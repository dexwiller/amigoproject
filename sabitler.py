#!/usr/bin/env python
# -*- coding: latin5 -*-
import os
import base64
import wx
simulation = True
wait_panel_id = 999
ana_button_size = 68
collapse_button_size=37
path_prefix  = os.path.dirname(os.path.realpath(__file__)).replace('library.zip','')
if not path_prefix.endswith('\\'):
        path_prefix += '\\'                    
main_process        = u'mainWindow.exe'
main_process_timeot = 3
control_process = u'control.exe'
control_process_timeout = 1
image_path  = path_prefix + "ico\\"
trans_path  = path_prefix + "trans\\"
allowed_key_code_list_number = [8,314,315,316,317]
CihazBeklemeSuresi    = 0.5#probe icin bu
except_bekleme_suresi = 0.3     
arabirimEkranBekleme  = 0.25
arabirimCevapsizBeklemeSuresi = 0.04
arabirimBeklemeSuresi = 0.1
similasyonbeklemesuresi = 0.1
arabirimSorguSuresi   = 0.2
cardReaderSorguSuresi   = 0.5
cardReaderBeklemeSuresi = 0.2
probeSorguSuresi      = 0.5
probeDBKayitSuresi    = 30
wsSysncSuresi         = 45
wsGunSonuBeklemeSuresi = 60
probeToplamKayitSayisi = 50
probeKararKayitSayisi  = 8
proveDolumKarakFark    = 10#lt
probeModSayisi         = 5
dolumaraKayitBeklemeSuresi = 30
rapor_sayfa_kayit_sayisi   = 25
satis_kayit_kontrol_zamani = 30
acilis_dolum_farki         = 255
acilista_400_kayit_kontrol   = False
satis_sonu_odeme_methodu_sor = False
dolum_rengi = wx.Colour(45,223,205)
hata_rengi  = wx.Colour(243,123,109)
call_rengi  = wx.Colour(30,166,94)
idle_rengi  = wx.WHITE

tank_yatay_mod  = 2
pompa_yatay_mod = 4

from local import locale,MESSAGES


copyright_text =  """Copyright � Firma ismi \n 
Address: Firma Adresi \n
Phone  :Firma telefonu\n
e-mail :Firma mail adres\n
Da��t�c� Ad�\n
Bayi Ad�
"""#%s\n%s"""
# stringteki bo�luklar direk aray�zde ��k�yo. ona g�re.
class dummy:
    pass
class ODEME_SEKILLERI:
    BELIRSIZ        = 0
    NAKIT           = 1
    KREDI_KARTI     = 2
    VERESIYE        = 3
    if satis_sonu_odeme_methodu_sor:
        DEFAULT = BELIRSIZ
    else:
        DEFAULT = NAKIT
    dict_values     = {
        1 : MESSAGES.nakit,
        0: MESSAGES.belirsiz,
        2 : MESSAGES.kredi_karti,
        3 : MESSAGES.veresiye
    }
class PROBE_ID_MARKA: 
    START_ITALIANA_XMT_SI_485 = 1
    TEOSIS                    = 2
    HSM                       = 3
    TSAV3                     = 4
    class TSAV3_FORMATTER:
        
        AlarmTresholds = 0x26,
        AutoNominal = 0x30,
        Configuration = 0x24,
        EndOfComm = 0,
        Nominal = 50,
        QueryCalibrationSet1 = 0x12,
        QueryCalibrationSet2 = 20,
        Reset = 0x90,
        SetAddress = 0xb2,
        SetAlarmTresholds = 0xb6,
        SetCalibrationSet2 = 0xa4,
        SetConfiguration = 180,
        Temp = 0x36,
        Version = 0x22
        query = 'FA000%s0032FFFFFFFFFFFFFFFFFFFFFFFF%s'
        start_byte = 0x25
        req_type = 'byte'
        yakit_start = 18
        yakit_end   = 24
        su_end      = 30
        isi_end     = 34
        yakit_bolen = 1000
        su_bolen    = 1000
        isi_bolen   = 100 

    class START_ITALIANA_XMT_SI_485_FORMATTER:
        ayrac    = '='
        adres    = 0
        durum    = 1
        sicaklik = 2
        sicaklik_multiplier = 10.0
        yakit_mm = 3
        yakit_multiplier    = 10.0
        su_mm    = 4
        su_multiplier       = 1.0
        chk_sum  = 5
        
        req_type = 'str'
    class HSM_FORMATTER:
        baslangic_bitis_byte     = 1
        response_type_index      = [4,6]
        response_all             = '80'
        response_sow             = '9C'
        response_sow_index       = [6,14]
        sow_multiplier           = 100
        fuel_sensor_index        = [6,12]
        water_sensor_index       = [12,18]
        temprature               = [26,28]
        status                   = [28,30]
        HSM_bolen                = float(100000000)
        req_type = 'byte'
        
    class TEOSIS_FORMATTER:
        ayrac    = ':'
        sicaklik = 2
        yakit_mm = 0
        su_mm    = 1
        query    = '%s2C'
        req_type = 'str'
        
    class SIMULATION:
        durum       = 10
        sicaklik    = 21
        yakit_sabit = 300
        su_sabit    = 10
        yakit_bolen = 10000
        su_bolen    = 100
    probe_id_formatter_dict = {START_ITALIANA_XMT_SI_485:START_ITALIANA_XMT_SI_485_FORMATTER(),
                                TEOSIS:TEOSIS_FORMATTER(),HSM:HSM_FORMATTER(),TSAV3:TSAV3_FORMATTER()}
class KART_OKUYUCU_MARKA:
        class MARKALAR: 
                BEST              = 1
                RFREADERV1        = 2

        class BEST_POOLING_FORMAT:
                AD               = 'best'
                ID               = 1
                
                return_idle      = '%s300100'
                pool             = '%s000101'
                pool_rf_id       = '%s000102'
                pool_w_rf_id     = '%s000103'
                read_w_card      = '%s300101'
                invalid_uid      = '%s300102'
                main_menu        = '%s300103'
                start_filling    = '%s300104'
                filling_progres  = '%s300105'
                filling_sum      = '%s300106'
                year_sum         = '%s300107'
                custom_sc        = '%s300108'
                clear_uid        = '%s300109'
                clear_seq        = '%s30010A'
                #01 30 01 00 E0 04
                #0130010004e

                status_resp_data_type = '30'
                status_resp_pump_st   = '80'
                status_resp_dict      = {
                        '00':[],
                        '01':[],
                        '02':[],
                        '03':['pool_rf_id','return_idle'],
                        '04':[],
                        '05':['pool_w_rf_id','return_idle'],
                        '06':['return_idle'],
                        '07':['return_idle'],
                        '08':['return_idle'],
                        '09':['return_idle'],
                        '0a':['return_idle'],
                        '0b':['return_idle'],
                        '0c':['return_idle'], 
                        '0d':['return_idle'],
                        '0e':['return_idle'],
                        '0f':['return_idle']
                }
                status_resp_pump_st_idle = '00'
                status_resp_pump_st_call = ''
                idle_list                = ['01','02']
        
        class RFIDV1_POOLING_FORMAT:
                AD               = 'rfid'
                ID               = 2
                
                return_idle      = '02%s0161'
                pool             = '02%s0161'
                pool_rf_id       = '02%s0162'
                pool_pass        = '02%s0164'
                clear_cache      = '02%s0163'
                card_data_resp   = '02%s0182'
                crc_end          = '%s03'
                card_id_resp     = '82'
                card_id_state    = '62'
                
                
                
                
                if locale.DIL    == 'tr_TR':
                        invalid_uid_cache      = '02%s4E6514321492020200000000000000000000000000000000190C476563657273697A204B61727400000004160000000000000000000000000000000002200000000000000000000000000000000042'
                        start_filling_cache    = '02%s4E6514321490020200000000000000000000000000000000190C446F6C756D61204261736C6179696E0002160000000000000000000000000000000002200000000000000000000000000000000041'
                        filling_progres_cache  = '02%s4E6514321490020200000000000000000000000000000000190C446F6C756D20596170696C69796F720002160000000000000000000000000000000002200000000000000000000000000000000040'
                        filling_sum_cache      = '02%s4E6514321490020200000000000000000000000000000000190C446F6C756D2054616D616D6C616E646902160000000000000000000000000000000002200000000000000000000000000000000042'
                        #read_w_card_cache      = '02%s4E6514321490020200000000000000000000000000000000140C506F6D70616369204B617274696E690028164F6B7574756E757A000000000000000002200000000000000000000000000000000041'
                        read_w_card_cache      = '02%s4E6514321490140C504F4D50414349204B415254494E490026164F4B5554554E555A000000000000000019204D55535445524920504C414B41000000262Fbasenumber000000000000000041'
                        final_payment_cache    = '02%s4E651432149019024F44454D45204D4554484F44550000000F0F46313A204E414B4954000000000000000F1946323A204B52454449204B41525449000F2346333A2056455245534959450000000043'
                else:
                        invalid_uid_cache      = '02%s4E6514321492020200000000000000000000000000000000190C496E76616C696420436172640000000002160000000000000000000000000000000002200000000000000000000000000000000042'
                        start_filling_cache    = '02%s4E6514321490020200000000000000000000000000000000190C53746172742046696C6C696E6700000002160000000000000000000000000000000002200000000000000000000000000000000041'
                        filling_progres_cache  = '02%s4E6514321490020200000000000000000000000000000000190C46696C6C696E6720696E0000000000001E1650726F6772726573730000000000000002200000000000000000000000000000000040'
                        filling_sum_cache      = '02%s4E6514321490020200000000000000000000000000000000190C46696C6C696E6720436F6D706C65746502160000000000000000000000000000000002200000000000000000000000000000000042'
                        #read_w_card_cache      = '02%s4E65143214900202000000000000000000000000000000001C0C57616974696E6720666F7200000000001E165374756666204361726400000000000002200000000000000000000000000000000041'
                        read_w_card_cache      = '02%s4E65143214901E0A57414954494E4720464F5200000000002216535455464620434152440000000000001220435553544F4D4552204C504C41544520282Fbasenumber000000000000000041'
                        final_payment_cache    = '02%s4E651432149019025041594D454E54204D4554484F4400000F0F46313A204341534800000000000000000F1946323A204352454449542043415244000F2346333A204F4E2054484520435546460043'
                        
                start_filling    = start_filling_cache
                filling_progres  = filling_progres_cache
                filling_sum      = filling_sum_cache
                read_w_card      = read_w_card_cache
                final_payment    = final_payment_cache
                
                invalid_uid = invalid_uid_cache
                status_resp_data_type = '30'
                status_resp_pump_st   = '80'
                status_resp_dict      = {
                        '00':[],
                        '01':[],
                        '02':['pool_rf_id','clear_cache'],
                        '03':['pool_pass','clear_cache'],
                        '99':[],
                        '82':['card_data_resp']
                }
                payment_dict = {
                        '44' : ODEME_SEKILLERI.NAKIT,
                        '2d' : ODEME_SEKILLERI.KREDI_KARTI,
                        '2b' : ODEME_SEKILLERI.VERESIYE
                        
                }
                status_resp_pump_st_idle = '00'
                status_resp_pump_st_call = ''
                idle_list                = ['01','02','63','30','62']
            
            
        card_reader_id_requset_dict  = {MARKALAR.BEST      :BEST_POOLING_FORMAT,
                                        MARKALAR.RFREADERV1:RFIDV1_POOLING_FORMAT}
class ARABIRIM_ID_MARKA:
    TEOSIS                    = 1
    ORPAK                     = 2
    class TEOSIS_RESPONSE_MAP:
        pass
    class TEOSIS_POOLING_FORMAT:
        AD               = 'teosis'
        ID               = 1
        pool             = '%s20FA'
        pool_resp        = '70'
        call_resp        = '01'
        paid_resp        = '08'
        pump_st_req      = '%s308001%s0'
        crc_end          = '%s03FA'
        pool_end         = '%sC0FA'
        autorize         = '%s308807%s%s000099999900'
        unautorize       = '%s308807%s%s000099999907'
        error            = '%s20FA'
        req_ecr_plate    = '%s309701%s%s'
        req_paid         = '%s308201%s%s'
        req_eof          = '%s308301%s%s'
        req_unpause      = '%s308402%s%s01'
        req_totalizer    = '%s308701%s%s'
        req_real_totalizer = req_totalizer
        req_sale_records = '%s308B02%s'
        req_update_ppu   = '%s308C04%s%s%s'
        req_update_time  = '%s309B06%s'
        
        status_resp_data_type          = '30'
        status_resp_pump_st            = '80'
        
        status_resp_dict      = {#_ecr_only
            '00':[],
            '01':['req_ecr_plate','autorize'],
            '02':['req_eof'],
            '03':['req_unpause'],
            '04':['req_real_totalizer','req_totalizer','req_paid','req_eof'],
            '05':[],
            '06':[],
            '07':[],
            '08':[],
            'uncall':['unautorize']
        }
        
        status_resp_pump_st_idle = '00'
        status_resp_pump_st_call = ''
    
    #hata_rengi   = wx.WHITE
    status_response_dict =  {
        '00':['idle',idle_rengi],
        '01':['call',call_rengi],
        '02':['busy',dolum_rengi],
        '03':['suspended',wx.YELLOW],
        '04':['unpaid',dolum_rengi],
        '05':['error',hata_rengi],
        '06':['autorized',dolum_rengi],
        '07':['wait',dolum_rengi],
        '08':['paid',dolum_rengi],
        '09':['plate',dolum_rengi]
        }
    status_response_dict_orpak = {
        '31':['idle',idle_rengi],
        '35':['call',call_rengi],
        '32':['busy',dolum_rengi],
        '38':['suspended',wx.YELLOW],
        '33':['unpaid',dolum_rengi],
        '37':['error',hata_rengi],
        '39':['autorized',dolum_rengi]
        }

    class ORPAK_POOLING_FORMAT:
        AD               = 'orpak'
        ID               = 2
        start            = 'FD'
        pool             = '%s80scumesajpirmesaj080000'
        pump_st_req      = '%s80scumesajpirmesaj2700050000303%s31'
        pool_resp        = '70'
        call_resp        = '35'
        paid_resp        = '08'
        crc_end          = '%sFE'
        pool_end         = '%s80scumesajpirmesaj060000'
        autorize         = '%s80scumesajpirmesaj2100220000303%s3%s3430303030302E30303000000020202020202020202020202020202030'
        unautorize       = '%s308807%s%s000099999907'
        error            = '%s20FA'
        req_ecr_plate    = '%s309701%s%s'
        req_paid         = '%s80scumesajpirmesaj2300060001303%s0001'
        req_eof          = '%s80scumesajpirmesaj2500050000303%s3%s'
        req_unpause      = '%s308402%s%s01'
        req_totalizer    = '%s308701%s%s'
        req_real_totalizer = req_totalizer
        req_sale_records = '%s308B02%s'
        req_update_ppu   = '%s80scumesajpirmesaj2D000E0000303%s3%s00303032%s'#3034363738
        req_update_time  = '%s309B06%s'
        req_clear        = '%s80scumesajpirmesaj2F00090001303%s000000003%s'
        status_resp_data_type          = '30'
        status_resp_pump_st            = '80'
        
        status_resp_dict= {#_ecr_only
                '3c':[],
                '31':[],
                '32':['req_eof'],
                '33':['req_eof','req_paid','req_clear'],
                '34':[],
                '35':['autorize'],
                '36':[],
                '37':[],
                'uncall':['req_clear']
        }
        
        status_resp_pump_st_idle = '00'
        status_resp_pump_st_call = ''
    
    
    
    #arabrim_id_formatter_dict = {TEOSIS:TEOSIS_FORMATTER}
    arabirim_id_requset_dict  = {TEOSIS:TEOSIS_POOLING_FORMAT,ORPAK:ORPAK_POOLING_FORMAT}
    class simulation:
        endeks = [['50', '30', '87', '0f', '51', '00', '00', '00', '00', '00', '00', '00', '00','00', '00', '00', '00', '00', '00', 'd9', '12', '03', 'fa'], ['50', '30', '87','0f', '11', '00', '59', '26', '72', '00', '00', '03', '12', '98', '99', '00', '00', '03', '81', 'de', 'f5', '03', 'fa']]

class TASIT_TANIMA_PK_MAP :
    pk_map_dict = {
        'CARI_HESAPLAR' : 'ID_CARI_HESAP_WEB',
        'KARTLAR'       : 'ID_KART_WEB',
        'ISKONTO'       : 'ID_ISKONTO_WEB',
        'ISKONTO_YAKIT' : 'ID_ISKONTO_YAKIT_WEB'
    }
    pk_sira_list = ['ISKONTO','ISKONTO_YAKIT', 'CARI_HESAPLAR', 'KARTLAR']#sira onemli, reference s�ras�na gore yaz�lmal�.

class TANK_MIN_MAX_ALARM_MAP:
    su_min_max_alarm_map_dict    = {'yuksek':{'text':'Su Seviyesi Yuksek'.decode('Latin5'),'rengi':wx.Colour(243,123,109)},
                                    'cok_yuksek':{'text':'Su Seviyesi �ok Y�ksek'.decode('Latin5'),'rengi':wx.Colour(243,123,109)}}
    yakit_min_max_alarm_map_dict = {'dusuk':{'text':'Yak�t Seviyesi D���k'.decode('Latin5'),'rengi':wx.Colour(243,123,109)},
                                    'cok_dusuk':{'text':'Yak�t Seviyesi �ok D���k'.decode('Latin5'),'rengi':wx.Colour(243,123,109)},
                                    'yuksek':{'text':'Yak�t Seviyesi Yuksek'.decode('Latin5'),'rengi':wx.Colour(243,123,109)},
                                    'cok_yuksek':{'text':'Yak�t Seviyesi �ok Y�ksek'.decode('Latin5'),'rengi':wx.Colour(243,123,109)}}
class CHART_COLORS:
        yakit_dict =  {
                1 :  0x800080,
                2 :  0x01DF3A,
                3 :  0x808000,
                4 :  0x008000,
                5 :  0x800080,
                6 :  0xC8FE2E,
                7 :  0x00FFFF,
                11 : 0x800000,
                12 : 0xA0522D,
                15 : 0x008080,
                16 : 0xB22222,
                17 : 0xCD5C5C,
                18 : 0xFFFF00,
                19 : 0xFFA500,
                20 : 0xFF6347,
                69 : 0x800080,
                70 : 0x800080,
                71 : 0x800080
                }
        
        SU_COLOR = 0x0000FF
        
class TANK_H_ACIKLAMA:
    OKUNAN_DEGER    = 1
    DOLUM_BASLANGIC = 2
    DOLUM_ARA_DEGER = 3
    DOLUM_BITIS     = 4
    TT_BASLANGIC    = 5
    TT_ARA_KAYIT    = 6
    TT_BITIS        = 7
    DALGA           = 8
    OTO_AC_FARK     = 9

    
class HATA_ACIKLAMA:
    PROBE_HATA             = MESSAGES.probe_hata_.decode('latin5')
    PORT_HATA              = MESSAGES.port_hata_.decode('latin5')
    PORT_ARABIRIM_HATA     = MESSAGES.port_arabirim_hata_.decode('latin5')
    ARABIRIM_ILETISIM_HATA = MESSAGES.arabirim_iletisi_hata.decode('latin5')
    POMPA_BEYIN_HATA       = MESSAGES.pompa_beyin_hata.decode('Latin5')
    
class EKRAN_ID_MAP:
    class ANA_EKRANLAR:
        id_e_otomasyon      = 9999
        id_e_islemler       = 8888
        id_e_vardiya        = 7777
        id_e_raporlar       = 6666
        id_e_ayarlar        = 5555
        id_e_about          = 4444
    
    class ANA_BUTONLAR:
        
        id_otomasyon      = 5
        id_islemler       = 10
        id_vardiya        = 15
        id_raporlar       = 20
        id_ayarlar        = 25
        id_about          = 30
    
    class OTOMASYON_EKRANLAR:
        id_e_otomasyon_ekrani = 199
        id_e_otomasyon_total_satislar = 195
        id_e_tank_otomasyonu  = 189
        id_e_birim_fiyatlar   = 179
        id_e_pompa_endeks     = 169
        id_e_tasit_tanima     = 159
        id_e_santiye          = 149
        

    class OTOMASYON_BUTONLAR:
        id_otomasyon_ekrani = 800
        id_tank_otomasyonu  = 805
        id_birim_fiyatlar   = 810
        id_pompa_ekdeks     = 815
        id_tasit_tanima     = 820
        id_santiye          = 825
    
    class ISLEMLER_EKRANLAR:
        id_e_iskonto_tanim      = 299
        id_e_cari_kartlar       = 289
        id_e_kart_tanimlama     = 279
        id_e_kart_listesi       = 269
        id_e_veresiye_islem     = 259
        id_e_servis_islem       = 249
        id_e_irsaliye_islem     = 239
        id_e_kalibrasyon_islem  = 229
        id_e_web_uygulamasi     = 219
        
    class ISLEMLER_BUTONLAR:
        id_b_iskonto_tanim      = 200
        id_b_cari_kartlar       = 205
        id_b_kart_tanimlama     = 210
        id_b_kart_listesi       = 215
        id_b_veresiye_islem     = 220
        id_b_servis_islem       = 225
        id_b_irsaliye_islem     = 230
        id_b_kalibrasyon_islem  = 235
        id_b_web_uygulamasi     = 245
    
    class VARDIYA_EKRANLAR:
        id_e_vardiya_duzeni           = 399
        id_e_vardiya_satis_izleme     = 389
        

    class ISKONTO_YAKIT_EKLE:
        id_iskonto_yakit_list  = 31
        id_check_box_aktif     = 32
        id_check_box_pasif     = 33
        

        
    class VARDIYA_BUTONLAR:
        id_b_vardiya_duzeni              = 300
        id_b_vardiya_satis_izleme        = 305
    
    class AYARLAR_BUTONLAR:
        
        id_b_otomasyon_ayarlar      = 500
        id_b_bayii_tanimlama        = 505
        id_b_yakit_tanimlama        = 510
        id_b_tank_tanimlama         = 515
        id_b_pompa_tanimlama        = 520
        id_b_tanimlama_gonder_al    = 525
        id_b_portlar                = 530
        #id_b_probe_tanimlama        = 535
        id_b_otomasyon_rapor        = 540
        
    class AYARLAR_EKRANLAR:
        
        id_e_otomasyon_ayarlar      = 599
        id_e_bayii_tanimlama        = 589
        id_e_yakit_tanimlama        = 579
        id_e_tank_tanimlama         = 569
        id_e_pompa_tanimlama        = 559
        id_e_tanimlama_gonder_al    = 549
        id_e_bayi_list              = 539
        id_e_portlar                = 529
        #id_e_probe_tanimlama        = 519
        id_e_otomasyon_rapor        = 509
    class RAPORLAR_BUTONLAR:
        id_b_tank_durum_raporlari   = 699
        id_b_satis_raporlari        = 689
        id_b_genel_durum            = 669
        id_b_vardiye_rapor          = 659
        id_b_gunsonu_rapor          = 649
    class RAPORLAR_EKRANLAR:
        id_e_tank_durum_raporlari     = 799
        id_e_satis_raporlari          = 789
        id_e_genel_durum              = 769
        id_e_vardiye_rapor            = 759
        id_e_genel_toplam_satis_rapor = 779
        id_e_gunsonu_rapor            = 749
        
EVENT_EKRAN_BINDING = {EKRAN_ID_MAP.ANA_BUTONLAR.id_otomasyon : EKRAN_ID_MAP.ANA_EKRANLAR.id_e_otomasyon,
                       EKRAN_ID_MAP.ANA_BUTONLAR.id_islemler  : EKRAN_ID_MAP.ANA_EKRANLAR.id_e_islemler,
                       EKRAN_ID_MAP.ANA_BUTONLAR.id_vardiya   : EKRAN_ID_MAP.ANA_EKRANLAR.id_e_vardiya,
                       EKRAN_ID_MAP.ANA_BUTONLAR.id_raporlar  : EKRAN_ID_MAP.ANA_EKRANLAR.id_e_raporlar,
                       EKRAN_ID_MAP.ANA_BUTONLAR.id_ayarlar   : EKRAN_ID_MAP.ANA_EKRANLAR.id_e_ayarlar,
                       EKRAN_ID_MAP.ANA_BUTONLAR.id_about     : EKRAN_ID_MAP.ANA_EKRANLAR.id_e_about,
                       EKRAN_ID_MAP.OTOMASYON_BUTONLAR.id_birim_fiyatlar : EKRAN_ID_MAP.OTOMASYON_EKRANLAR.id_e_birim_fiyatlar,
                       EKRAN_ID_MAP.OTOMASYON_BUTONLAR.id_tank_otomasyonu : EKRAN_ID_MAP.OTOMASYON_EKRANLAR.id_e_tank_otomasyonu,
                       EKRAN_ID_MAP.OTOMASYON_BUTONLAR.id_pompa_ekdeks : EKRAN_ID_MAP.OTOMASYON_EKRANLAR.id_e_pompa_endeks,
                       EKRAN_ID_MAP.OTOMASYON_BUTONLAR.id_otomasyon_ekrani:EKRAN_ID_MAP.OTOMASYON_EKRANLAR.id_e_otomasyon_ekrani,
                       EKRAN_ID_MAP.ISLEMLER_BUTONLAR.id_b_iskonto_tanim : EKRAN_ID_MAP.ISLEMLER_EKRANLAR.id_e_iskonto_tanim,
                       EKRAN_ID_MAP.ISLEMLER_BUTONLAR.id_b_kalibrasyon_islem : EKRAN_ID_MAP.ISLEMLER_EKRANLAR.id_e_kalibrasyon_islem,
                       EKRAN_ID_MAP.ISLEMLER_BUTONLAR.id_b_cari_kartlar : EKRAN_ID_MAP.ISLEMLER_EKRANLAR.id_e_cari_kartlar,
                       EKRAN_ID_MAP.ISLEMLER_BUTONLAR.id_b_kart_tanimlama : EKRAN_ID_MAP.ISLEMLER_EKRANLAR.id_e_kart_tanimlama,
                       EKRAN_ID_MAP.ISLEMLER_BUTONLAR.id_b_servis_islem : EKRAN_ID_MAP.ISLEMLER_EKRANLAR.id_e_servis_islem,
                       EKRAN_ID_MAP.VARDIYA_BUTONLAR.id_b_vardiya_duzeni : EKRAN_ID_MAP.VARDIYA_EKRANLAR.id_e_vardiya_duzeni,
                       EKRAN_ID_MAP.VARDIYA_BUTONLAR.id_b_vardiya_satis_izleme:EKRAN_ID_MAP.VARDIYA_EKRANLAR.id_e_vardiya_satis_izleme,
                       EKRAN_ID_MAP.AYARLAR_BUTONLAR.id_b_otomasyon_ayarlar : EKRAN_ID_MAP.AYARLAR_EKRANLAR.id_e_otomasyon_ayarlar,
                       EKRAN_ID_MAP.AYARLAR_BUTONLAR.id_b_bayii_tanimlama : EKRAN_ID_MAP.AYARLAR_EKRANLAR.id_e_bayii_tanimlama,
                       EKRAN_ID_MAP.AYARLAR_BUTONLAR.id_b_portlar : EKRAN_ID_MAP.AYARLAR_EKRANLAR.id_e_portlar ,
                       EKRAN_ID_MAP.AYARLAR_BUTONLAR.id_b_tank_tanimlama : EKRAN_ID_MAP.AYARLAR_EKRANLAR.id_e_tank_tanimlama,
                       EKRAN_ID_MAP.AYARLAR_BUTONLAR.id_b_yakit_tanimlama : EKRAN_ID_MAP.AYARLAR_EKRANLAR.id_e_yakit_tanimlama,
                       EKRAN_ID_MAP.AYARLAR_BUTONLAR.id_b_pompa_tanimlama : EKRAN_ID_MAP.AYARLAR_EKRANLAR.id_e_pompa_tanimlama,
                       EKRAN_ID_MAP.AYARLAR_BUTONLAR.id_b_tanimlama_gonder_al : EKRAN_ID_MAP.AYARLAR_EKRANLAR.id_e_tanimlama_gonder_al,
                       EKRAN_ID_MAP.AYARLAR_BUTONLAR.id_b_otomasyon_rapor:EKRAN_ID_MAP.AYARLAR_EKRANLAR.id_e_otomasyon_rapor,
                       EKRAN_ID_MAP.RAPORLAR_BUTONLAR.id_b_tank_durum_raporlari : EKRAN_ID_MAP.RAPORLAR_EKRANLAR.id_e_tank_durum_raporlari,
                       EKRAN_ID_MAP.RAPORLAR_BUTONLAR.id_b_genel_durum : EKRAN_ID_MAP.RAPORLAR_EKRANLAR.id_e_genel_durum,
                       EKRAN_ID_MAP.RAPORLAR_BUTONLAR.id_b_satis_raporlari : EKRAN_ID_MAP.RAPORLAR_EKRANLAR.id_e_satis_raporlari,
                       EKRAN_ID_MAP.RAPORLAR_BUTONLAR.id_b_vardiye_rapor : EKRAN_ID_MAP.RAPORLAR_EKRANLAR.id_e_vardiye_rapor,
                       EKRAN_ID_MAP.RAPORLAR_BUTONLAR.id_b_gunsonu_rapor : EKRAN_ID_MAP.RAPORLAR_EKRANLAR.id_e_gunsonu_rapor,
                       EKRAN_ID_MAP.ISLEMLER_BUTONLAR.id_b_web_uygulamasi : EKRAN_ID_MAP.ISLEMLER_EKRANLAR.id_e_web_uygulamasi
                       }
aylar              ={
                    1:'Ocak',
                    2:'�ubat',
                    3:'Mart',
                    4:'Nisan',
                    5:'May�s',
                    6:'Haziran',
                    7:'Temmuz',
                    8:'A�ustos',
                    9:'Eyl�l',
                    10:'Ekim',
                    11:'Kas�m',
                    12:'Aral�k'
}
aylar_object_list = []
for k,v in aylar.iteritems():
    aylar_object    = dummy()
    aylar_object.id = k
    aylar_object.ad = v
    aylar_object_list.append( aylar_object )
dolum_tipi_dict = { 0:'Otomatik Dolum',
                    1:'Manuel Dolum'}
                       
servis_cesitleri  =  {
                    0:[MESSAGES.sablon_iki_kelime%( MESSAGES.olusturma, MESSAGES.tarihi),'TALEP_TARIHI'],
                    1:[MESSAGES.sablon_iki_kelime%(MESSAGES.iletim, MESSAGES.tarihi),'MERKEZE_ILETIM_TARIHI'],
                    2:[MESSAGES.sablon_iki_kelime%(MESSAGES.islem, MESSAGES.tarihi),'ISLEME_ALINMA_TARIHI'],
                    3:[MESSAGES.sablon_iki_kelime%(MESSAGES.servise_bildirim, MESSAGES.tarihi),'SERVISE_BILDIRIM_TARIHI'],
                    4:[MESSAGES.sablon_iki_kelime%(MESSAGES.tamamlanma, MESSAGES.tarihi),'TAMAMLANMA_TARIHI']
                     }
class CARI_TIPLER:
    PERSONEL = 1  
    MUSTERI  = 2
class ISKONTO_UYG_TURU:
        ARTIRIM = 1
        INDIRIM = 2
class ISKONTO_UYG_CINSI:
        MIKTAR  = 1
        YUZDE   = 2
class param_threads:
        TANK            = 14
        POMPA           = 15
        WS              = 16
        TANK_PROBESUZ   = 17
pass_algoritm_default     = '''f14aae6a0e050b74e4b7b9a5b2ef1a60ceccbbca39b132ae3e8bf88d3a946c6d8687f3266fd2b626419d8b67dcf1d8d7c0fe72d4919d9bd05efbd37070cfb41a'''
pass_algoritm_product     = '''31d42894a17fa278666c6d8e6c2be8174bad9cc9d16eca967555975940f04169ad4923359caf8f98e6f4f803a9a7c2a921a88e097ecfc05562b2457197a4aca7'''   
pass_algoritm             = '''1f40fc92da241694750979ee6cf582f2d5d7d28e18335de05abc54d0560e0f5302860c652bf08d560252aa5e74210546f369fbbbce8c12cfc7957b2652fe9a75'''
data_bytes        = [1,5,6,7,8,123,134,254,253,150,75,89,34]
base64string      = base64.encodestring('%s:%s' % ('otoweb', 'manager')).replace('\n', '')
db_filename       = path_prefix + 'oto.db'
epdk_ws_url       = 'http://lisansws.epdk.org.tr/services/lisansPublicProxy/'
epdk_ws_wsdl_url  = 'http://lisansws.epdk.org.tr/services/lisansPublicProxy?wsdl'
#web_soap_url      = 'http://192.168.0.51:8080/OtoDeskSync/?WSDL'
#web_soap_wsdl_url = 'http://192.168.0.51:8080/OtoDeskSync/service.wsdl'

web_soap_url      = 'http://localhost:8000/OtoDeskSync/?WSDL'
web_soap_wsdl_url = 'http://localhost:8000/OtoDeskSync/service.wsdl'
web_tasit_tanima_soap_url = 'http://localhost:8000/OtoTasitTanima/?WSDL'
web_soap_tasit_tanima_wsdl_url = 'http://localhost:8000/OtoTasitTanima/service.wsdl'

imp_url           = "'http://www.w3.org/2001/XMLSchema',location='http://www.w3.org/2001/XMLSchema.xsd'"
