#!/usr/bin/env python
# -*- coding: latin5 -*-
from __future__ import generators
import pyodbc
import logging
import sys
from util import data_formatla
def ResultIter(cursor, arraysize=10):
    while True:
        results = cursor.fetchmany(arraysize)
        if not results:
            break
        for result in results:
            yield result
class Struct:
    def __init__(self, **entries):
        self.__dict__.update(entries)
class Child:
    def __init__(self, **entries): 
        self.__dict__.update(entries)
class dataObj:
    def __init__( self ):
        self.id = None
        self.ad = None
class conect:
    def __init__(self):
        self.conn = None
        self.cur  = None
	self.usr     = 'OtoDesk'
	self.pas     = 'OtoDesk'
	self.db_name = 'DunyaOtoDesk'

    def connect( self):

	#print usr, pas, schema
        #conn  = cx_Oracle.connect( "%s/%s@%s"%(usr,pas,schema))
	conn  = pyodbc.connect('DRIVER={SQL Server Native Client 10.0};SERVER=localhost;DATABASE=%s;UID=%s;PWD=%s'%(self.db_name,self.usr,self.pas))
        #conn = pyodbc.connect('DSN=asdad;UID=%s;PWD=%s'%(usr,pas))
	return conn
    
    def run( self, sqls ):

        conn = self.connect()
	logging.debug( "con run ------------------------- > ")
        cur  = conn.cursor()
	logging.debug( "cur run ------------------------- >")
	try:
            cur.execute(str(sqls))
            don  = cur.fetchall()
        except:
            logging.debug( str(sys.exc_info()[0]))
            logging.debug ( 'sql hatasi -> %s'% sqls)
	    print str(sys.exc_info()[0])
            don = [[]]
        cur.close()
        conn.close()
        return don
    
    def run_returns_as_dict( self, sqls ):
	conn = self.connect()
	logging.debug( "con run ------------------------- > ")
        cur  = conn.cursor()
	logging.debug( "cur run ------------------------- >")
        try:
            cur.execute(str(sqls))
	    columns = [column[0] for column in cur.description]
            don = []
	    for row in ResultIter( cur ):
	        don.append(dict(zip(columns, row)))
        except:
            logging.debug( str(sys.exc_info()[0]))
            logging.debug ( 'sql hatasi -> %s'% sqls)
	    print str(sys.exc_info()[0])
            don = [[]]
        cur.close()
        conn.close()
        return don
    
    def run_returns_as_object( self, sqls ):
	conn = self.connect()
	logging.debug( "con run ------------------------- > ")
        cur  = conn.cursor()
	logging.debug( "cur run ------------------------- >")
        #try:
	cur.execute(str(sqls))
	columns = [column[0] for column in cur.description]
	don = []
	for row in ResultIter( cur ):
	    don.append(Struct(**dict(zip(columns, row))))
        #except:
        #    print( str(sys.exc_info()[0]))
        #    print( 'sql hatasi -> %s'% sqls)
	#    print str(sys.exc_info()[0])
        #    don = [[]]
        cur.close()
        conn.close()
        return don
    
    def run_returns_as_parent_child( self, sqls, child_parameters,data_types ):
	conn = self.connect()
        cur  = conn.cursor()
	cur2 = conn.cursor()
        #try:
	cur.execute(str(sqls))
	columns = [column[0] for column in cur.description]
	don = []
	for row in  ResultIter(cur):
	    parent_dict        = dict(zip(columns, row))
	    parent_obje        = Struct(**parent_dict)
	    parent_mapper      = {}    
	    parent_mapper[0]   = row[0]
	    data_formatla(row[0], data_types[0])
	    setattr(parent_obje,'mapper',parent_mapper)
	    #setattr(parent_obje,'children',children)
	    setattr(parent_obje,'base_attr',parent_dict[child_parameters[1]])
	     
	    don.append( parent_obje )
        #except:
        #    logging.debug( str(sys.exc_info()[0]))
        #    logging.debug ( 'sql hatasi -> %s'% sqls)
	#    print str(sys.exc_info()[0])
        #    don = [[]]
        cur.close()
        conn.close()
	
        return don
    def get_children_by_parent( self, sqls, child_parameters , parent_paramters,data_types ):
	conn = self.connect()
	cur2 = conn.cursor()
	cur2.execute( sqls )
	columns2 = [column[0] for column in cur2.description]
	children = []
	
	for row2 in ResultIter(cur2):
	    ch_dict = dict(zip(columns2,row2))
	    ch = Child(**ch_dict)
	    mapper_child = {0:parent_paramters[0]}
	    for r in range( len(row2) ):
		if not mapper_child.has_key(r):
		    mapper_child[r] = data_formatla(row2[r], data_types[r])
	    setattr(ch,'base_attr',parent_paramters[1])
	    setattr(ch,'mapper',mapper_child)
	    children.append( ch )
	cur2.close()
        conn.close()
	return children
    
    def dict_fetch_all( self, curs ):
	desc = curs.description
	return [
		dict( zip([col[0] for col in desc], row))
		for row in curs.fetchall()
		]
    
    def run_dml_return_id( self, sqls ):
        logging.debug( ' run_dml ----------> geldi' )
        conn      = self.connect()
        cur       = conn.cursor()
	cur_scope = conn.cursor()
        scope  = ''
        try:
	    logging.debug( ' run_dml ----------> insert' )	    
	    cur.execute(sqls)
	    logging.debug( ' run_dml ----------> cur.execute ')
	    logging.debug( ' run_dml ----------> curdon '  )
	    conn.commit()
	    cur.close()
	    sc = cur_scope.execute('SELECT SCOPE_IDENTITY()')
	    scope = sc.fetchone()[0]
	    conn.commit()
	    cur_scope.close()
	    conn.close()
        except:
            print ( str(sys.exc_info()[0]))
            print  ( 'sql hatasi -> %s'% sqls)
            cur.close()
            conn.close()
        return scope

    def run_dml( self, sqls,type ):
        logging.debug( ' run_dml ----------> geldi' )
        conn = self.connect()
        cur  = conn.cursor()
        don  = 1
        try:
            if type == 'insert':
                logging.debug( ' run_dml ----------> insert' )
                
                cur.execute(sqls)
                logging.debug( ' run_dml ----------> cur.execute ')
		logging.debug( ' run_dml ----------> curdon '  )
            elif type == 'update':
		logging.debug( ' run_dml ----------> sqls %s'%sqls )
                cur.execute(str(sqls))
	    elif type == 'delete':
		logging.debug( ' run_dml ----------> sqls %s'%sqls )
                cur.execute(str(sqls))
            conn.commit()
            cur.close()
            conn.close()
        except:
            print ( str(sys.exc_info()[0]))
            print  ( 'sql hatasi -> %s'% sqls)
            don = 0
            cur.close()
            conn.close()
        return don
    pass

class DBObjects:
    def __init__( self ):
        self.object_list = []
    
    def get_raw_data_all(self, table_name, where="", order="",selects="",dict_mi = 0,child_parameters="",parent_parameters="",data_types=""):
	#dict mi 0 ise normal, 1 ise dict 2 ise parent child
	if where != "":
            where = ' Where %s'%where
        if order != "":
            order = ' ORDER BY %s'%order
        if selects == "":
            selects = '*'
        sq_  = conect()
        sqls = " Select %s from %s %s %s" %(selects,table_name,where,order)
	
        logging.debug('get_raw_data_all ----------------> sqls %s' %sqls)
        if dict_mi == 'dict':
	    return sq_.run_returns_as_dict( sqls )
	elif dict_mi == 'object':
	    return sq_.run_returns_as_object( sqls )
	elif dict_mi == 'parent':
	    return sq_.run_returns_as_parent_child( sqls, child_parameters,data_types )
	elif dict_mi == 'child':
	    return sq_.get_children_by_parent( sqls, child_parameters,parent_parameters,data_types )
	
	else:
	    data = sq_.run( sqls )
	
	    return data
    def get_parameters_all(self,table_name,where="",order="",selects = ""):
	self.object_list = []
        if where != "":
            where = ' Where %s'%where
        if order != "":
            order = ' ORDER BY %s'%order
        if selects == "":
            selects = '*'
        sq_  = conect()
        sqls = " Select %s from %s %s %s" %(selects,table_name,where,order)
        logging.debug('get_parameters_all ----------------> sqls %s' %sqls)
	
        data = sq_.run( sqls )
        for d in data:
            obj = dataObj()
            obj.id = d[0]
            obj.ad = d[1]
            self.object_list.append( obj )
            
        return self.object_list
    
    def get_parameters_all_with_id(self,table_name,where="",order="",selects = ""):
	self.object_list = []
        if where != "":
            where = ' Where %s'%where
        if order != "":
            order = ' ORDER BY %s'%order
        if selects == "":
            selects = '*'
        sq_  = conect()
        sqls = " Select %s from %s %s %s" %(selects,table_name,where,order)
        logging.debug('get_parameters_all ----------------> sqls %s' %sqls)
        data = sq_.run( sqls )
        for d in data:
            obj = dataObj()
            obj.id = d[0]
	    obj.ad = ''
	    i=0
	    for o in d:
		if i!= 0:
		    obj.ad += str(o)+'-'
		i += 1
	    obj.ad = obj.ad[:len(obj.ad)-1]
            self.object_list.append( obj )     
	return self.object_list
    def save_data ( self , table_name ,column_name, values ):
        #### t�rkce karakter problemi var !!!!#########
        logging.debug( 'Save Parameters -------- >' + str(table_name) +' ::  ' + str(column_name)+' ::  ')
        sq_  = conect()
        logging.debug( 'Save Parameters -------- > sq %s ' % sq_)
        sqls = '''insert into %s ( %s ) values ( %s )''' %(table_name,column_name,values)
	#print sqls 
        logging.debug( 'Save Parameters -------- > %s' %sqls )
        sqls.encode('latin5','ignore')
        data = sq_.run_dml(sqls,'insert') 
        logging.debug( 'Save Parameters -------- > %s'%str(data) )
        return data
    def save_data_and_get_id ( self , table_name ,column_name, values ):
        #### t�rkce karakter problemi var !!!!#########
        logging.debug( 'Save Parameters -------- >' + str(table_name) +' ::  ' + str(column_name)+' ::  ')
        sq_  = conect()
        logging.debug( 'Save Parameters -------- > sq %s ' % sq_)
        sqls = '''insert into %s ( %s ) values ( %s )''' %(table_name,column_name,values)
        logging.debug( 'Save Parameters -------- > %s' %sqls )
        sqls.encode('latin5','ignore')
        scope = sq_.run_dml_return_id(sqls) 
        logging.debug( 'Save Parameters -------- > %s'%str(scope) )
        return scope
    def update_data( self, table_name,column_name, values, where ):
	logging.debug( 'Save Parameters -------- >' + str(table_name) +' ::  ' + str(column_name)+' ::  ')
        sq_  = conect()
        logging.debug( 'Save Parameters -------- > sq %s ' % sq_)
	c_list = column_name.split(',')
	v_list = values.split(',')
	set_str = ""
	for c in c_list:
	    set_str += c + ' = ' +  v_list[c_list.index(c)] 
	    set_str += ' , '
	
	set_str = set_str[:len(set_str)-2]
        sqls = '''update %s set %s where %s''' %(table_name,set_str,where)
	#print sqls
        logging.debug( 'Save Parameters -------- > %s' %sqls )
        sqls.encode('latin5','ignore')
        data = sq_.run_dml(sqls,'update') 
        logging.debug( 'Save Parameters -------- > %s'%str(data) )
        return data  
    
    def delete_data( self, table_name,where ):
        sq_  = conect()
        sqls = '''delete from %s where %s''' %(table_name,where)
        sqls.encode('latin5','ignore')
        data = sq_.run_dml(sqls,'delete') 
        return data
    
    def widgetMaker(self, widget, objects):
	widget.Clear()
        for obj in objects:
            widget.Append(str(obj.ad), obj)
    def widgetMakerStr( self , widget , StrObjects ):
	widget.Clear()
        for obj in StrObjects:
            widget.Append( str( obj))
    def widgetMakerConcate( self , widget , objects):
	widget.Clear()
        for obj in objects:
            widget.Append(str(obj.id) +'-'+ str(obj.ad) )
    def widgetMakerConcateObject( self , widget , objects):
	widget.Clear()
        for obj in objects:
            widget.Append((str(obj.id) +'-'+ str(obj.ad)),obj )
    def getIlceObjectListByIl( self, il_id ):
	ilceler = self.get_parameters_all( table_name="P_ILCELER", where="sehir=%s"%il_id ,order="ilce" )
	return ilceler
    
