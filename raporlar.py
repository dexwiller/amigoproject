#!/usr/bin/env python
# -*- coding: latin5 -*-

import wx
import os
import  sys
import time
import logging
from DBConnectLite import dataObj
from printPage import Printer, HtmlPrinter, PrintTablePreview
import style
import model
from getGridDataList import IndexListPanel,PaginatingIndexPanel,createCariSatislar
param = model.Parameters()
import wx.lib.agw.aui as aui
import sabitler
import wx.lib.buttons as LibButtons
button_size  = sabitler.ana_button_size
image_path   = sabitler.image_path
#ekstra_idler = sabitler.EKRAN_ID_MAP.ISKONTO_YAKIT_EKLE 
frame_color     = wx.Colour(32,213,244)
from local import MESSAGES
from datetime import date, timedelta as td
id_b_tank_durum_raporlari = sabitler.EKRAN_ID_MAP.RAPORLAR_BUTONLAR.id_b_tank_durum_raporlari
id_b_satis_raporlari = sabitler.EKRAN_ID_MAP.RAPORLAR_BUTONLAR.id_b_satis_raporlari
id_b_genel_durum = sabitler.EKRAN_ID_MAP.RAPORLAR_BUTONLAR.id_b_genel_durum
id_b_vardiye_rapor=sabitler.EKRAN_ID_MAP.RAPORLAR_BUTONLAR.id_b_vardiye_rapor
id_b_gunsonu_rapor=sabitler.EKRAN_ID_MAP.RAPORLAR_BUTONLAR.id_b_gunsonu_rapor

    
class win_Raporlar(wx.Frame):
    def __init__(self, parent, id_, title):
        
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR | wx.MAXIMIZE
                                  ))
        self.O  = param.get_root( self ).O 
        self.GValuesDict = None
        self.SourceDataBase = None
        self.RS = None
        self.previousGValuesDict = None
        self.oncekiDeger = False
        #font1 = wx.Font(9, wx.SWISS, wx.NORMAL, wx.BOLD, False, u'Verdana')
        #self.CreateStatusBar() 
        #filemenu= wx.Menu()
        #aboutItem = filemenu.Append(wx.ID_ABOUT, "&Hakk�nda"," Program Hakk�nda Bilgi")
        #self.Bind(wx.EVT_MENU, self.OnAbout, aboutItem)
        #filemenu.AppendSeparator()
        #exitItem = filemenu.Append(wx.ID_EXIT,"&��k��"," Program� Kapat")
        #self.Bind(wx.EVT_MENU, self.OnExit, exitItem)
        
        #item = wx.MenuItem(filemenu, MENU_HIDE_X, "Hide X Button", "Hide X Button", wx.ITEM_CHECK)
        
        #self.Bind(wx.EVT_CLOSE, self.OnCloseWindow)

        #menuBar = wx.MenuBar()
        #menuBar.Append(filemenu,"&Dosya")
        #self.SetMenuBar(menuBar)
        #self.CreateRightClickMenu()
        self.LayoutItems()
        self.Centre()
        #icon1           = wx.Icon("buski.ico", wx.BITMAP_TYPE_ICO)
        #self.SetIcon(icon1)
        
        self.Show(True)
    
    def LayoutItems(self):
        mainSizer = wx.GridBagSizer(0,0)
        self.Freeze()
        self.SetSizer(mainSizer)
        winSize =  self.GetSize()
        self.top_panel = wx.Panel(self, wx.ID_ANY,size=(winSize[0],540))
        self.top_panel.SetBackgroundColour( wx.WHITE)
        self.top_sizer = wx.GridBagSizer(1,1)
        self.top_panel.SetSizer( self.top_sizer )
        
        self.btn_tank_raporu_pressed  =  wx.Image(MESSAGES.tank_rapor_accepted_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_btn_tank_raporu = wx.Image(MESSAGES.tank_rapor_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_tank_raporu  = wx.BitmapButton(self.top_panel, id=id_b_tank_durum_raporlari, bitmap=image_btn_tank_raporu,pos=(0,0),
        size = (image_btn_tank_raporu.GetWidth()+5, image_btn_tank_raporu.GetHeight()+5))
        self.btn_tank_raporu.Bind(wx.EVT_BUTTON, self.OnRaporlarMenuPressed)
        self.btn_tank_raporu.SetToolTipString(MESSAGES.sablon_uc_kelime%( MESSAGES.tank, MESSAGES.durum, MESSAGES.raporu))
        self.btn_tank_raporu.SetBitmapSelected(self.btn_tank_raporu_pressed)
        self.top_sizer.Add(self.btn_tank_raporu, (0,0),(1,1),wx.ALL)
        
        self.btn_satis_raporlari_pressed  =  wx.Image(MESSAGES.satis_rapor_accepted_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_btn_satis_raporlari = wx.Image(MESSAGES.satis_rapor_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_satis_raporlari  = wx.BitmapButton(self.top_panel, id=id_b_satis_raporlari, bitmap=image_btn_satis_raporlari,pos=(0,1),
        size = (image_btn_satis_raporlari.GetWidth()+5, image_btn_satis_raporlari.GetHeight()+5))
        self.btn_satis_raporlari.Bind(wx.EVT_BUTTON, self.OnRaporlarMenuPressed)
        self.btn_satis_raporlari.SetToolTipString(MESSAGES.sablon_iki_kelime%( MESSAGES.satis, MESSAGES.raporu))
        self.btn_satis_raporlari.SetBitmapSelected(self.btn_satis_raporlari_pressed)
        self.top_sizer.Add(self.btn_satis_raporlari,  (0,1),(1,1), wx.ALL)
        
        self.btn_gunsonu_rapor_pressed  =  wx.Image(MESSAGES.gunsonu_rapor_accepted_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_btn_gunsonu_rapor = wx.Image(MESSAGES.gunsonu_rapor_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_gunsonu_rapor  = wx.BitmapButton(self.top_panel, id=id_b_gunsonu_rapor, bitmap=image_btn_gunsonu_rapor,pos=(0,0),
        size = (image_btn_gunsonu_rapor.GetWidth()+5, image_btn_gunsonu_rapor.GetHeight()+5))
        self.btn_gunsonu_rapor.Bind(wx.EVT_BUTTON, self.OnRaporlarMenuPressed)
        self.btn_gunsonu_rapor.SetToolTipString(MESSAGES.sablon_iki_kelime%( MESSAGES.gunsonu, MESSAGES.raporu))
        self.btn_gunsonu_rapor.SetBitmapSelected(self.btn_gunsonu_rapor_pressed)
        self.top_sizer.Add(self.btn_gunsonu_rapor, (0,2),(1,1),wx.ALL)
          
        self.btn_genel_durum_pressed  =  wx.Image(MESSAGES.genel_rapor_accepted_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_btn_genel_durum = wx.Image(MESSAGES.genel_rapor_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_genel_durum  = wx.BitmapButton(self.top_panel, id=id_b_genel_durum, bitmap=image_btn_genel_durum,pos=(0,1),
        size = (image_btn_genel_durum.GetWidth()+5, image_btn_genel_durum.GetHeight()+5))
        self.btn_genel_durum.Bind(wx.EVT_BUTTON, self.OnRaporlarMenuPressed)
        self.btn_genel_durum.SetToolTipString(MESSAGES.sablon_uc_kelime%(MESSAGES.genel, MESSAGES.durum, MESSAGES.raporu))
        self.btn_genel_durum.SetBitmapSelected(self.btn_genel_durum_pressed)
        self.top_sizer.Add(self.btn_genel_durum,  (1,1),(1,1), wx.ALL)
        
        self.btn_vardiya_rapor_pressed  =  wx.Image(MESSAGES.vardiya_rapor_accepted_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_btn_vardiya_rapor = wx.Image(MESSAGES.vardiya_rapor_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_vardiya_rapor  = wx.BitmapButton(self.top_panel, id=id_b_vardiye_rapor, bitmap=image_btn_vardiya_rapor,pos=(0,1),
        size = (image_btn_vardiya_rapor.GetWidth()+5, image_btn_vardiya_rapor.GetHeight()+5))
        self.btn_vardiya_rapor.Bind(wx.EVT_BUTTON, self.OnRaporlarMenuPressed)
        self.btn_vardiya_rapor.SetToolTipString(MESSAGES.sablon_iki_kelime%( MESSAGES.vardiya, MESSAGES.raporu))
        self.btn_vardiya_rapor.SetBitmapSelected(self.btn_vardiya_rapor_pressed)
        self.top_sizer.Add(self.btn_vardiya_rapor,  (1,0),(1,1), wx.ALL)
        '''
        self.btn_vardiya_durum_pressed  =  wx.Image("%svardiya_rapor_accepted256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_btn_vardiya_durum = wx.Image("%svardiya_rapor256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_vardiya_durum  = wx.BitmapButton(self.top_panel, id=id_b_vardiye_rapor, bitmap=image_btn_vardiya_durum,pos=(0,1),
        size = (image_btn_vardiya_durum.GetWidth()+5, image_btn_vardiya_durum.GetHeight()+5))
        self.btn_vardiya_durum.Bind(wx.EVT_BUTTON, self.OnRaporlarMenuPressed)
        self.btn_vardiya_durum.SetToolTipString("Vardiya Raporu")
        self.btn_vardiya_durum.SetBitmapSelected(self.btn_vardiya_durum_pressed )
        self.top_sizer.Add(self.btn_vardiya_durum,  (0,2),(1,1), wx.ALL)
        '''
        
        self.middle_panel =  wx.Panel(self, wx.ID_ANY ,size=(winSize[0],winSize[1]-140))
        middle_sizer=wx.FlexGridSizer(cols=1,hgap=10,vgap=10)
        self.middle_panel.SetSizer(middle_sizer)
        self.middle_panel.SetBackgroundColour( wx.Colour(254,254,254))
        middle_panel_image=wx.Image(MESSAGES.yazilim_image_%image_path,wx.BITMAP_TYPE_ANY)
        sb1 = wx.StaticBitmap(self.middle_panel, -1, wx.BitmapFromImage(middle_panel_image))
        
        middle_sizer.Add(sb1,1,wx.ALL|wx.CENTER)      

        mainSizer.Add( self.top_panel,(0,0),(1,1),wx.ALL)
        mainSizer.Add( self.middle_panel,(1,0),(1,1),wx.ALL)
        self.Thaw()
    
    def OnExit(self,e):
        self.onay_message("��kmak �stedi�inizden Emin misiniz ?","Kapat")
    def OnCloseWindow(self,e):
        self.onay_message("��kmak �stedi�inizden Emin misiniz ?","Kapat")
   
    def onay_message( self,message,win_name):
        self.SetTransparent(150)
        dlg = wx.MessageDialog(self, message,
                               win_name,
                               wx.OK  |wx.CANCEL | wx.ICON_INFORMATION
                               )
        
        retCode = dlg.ShowModal()
        if (retCode == wx.ID_OK):
            self.Show( False )
        dlg.Destroy()
        self.SetTransparent(255)      
        
    def OnRaporlarMenuPressed( self , event ):
       
        id_  = event.GetEventObject().GetId()
        
        if id_ == id_b_tank_durum_raporlari:
            winClass = win_Tank_Durum
            title    = MESSAGES.sablon_uc_kelime%( MESSAGES.tank, MESSAGES.durum, MESSAGES.raporu)
            
        elif id_ == id_b_satis_raporlari:
            winClass = win_Satis_Raporu
            title    = MESSAGES.sablon_iki_kelime%( MESSAGES.satis, MESSAGES.raporu)
            
        elif id_ == id_b_genel_durum:
            winClass = win_Genel_Durum
            title    = MESSAGES.sablon_uc_kelime%(MESSAGES.genel, MESSAGES.durum, MESSAGES.raporu)
            
        elif id_ == id_b_vardiye_rapor:
            winClass = win_Vardiya_rapor
            title    = MESSAGES.sablon_iki_kelime%( MESSAGES.vardiya, MESSAGES.raporu)
        
        elif id_ == id_b_gunsonu_rapor:
            winClass = win_Gunsonu_rapor
            title    = MESSAGES.sablon_iki_kelime%( MESSAGES.gunsonu, MESSAGES.raporu)
            
        
        self.O.newWindow(self,event,winClass,title,True)
class win_Vardiya_rapor(wx.Frame):
    def __init__(self, parent, id_, title):
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR ))
        self.styl=style
        self.rapor_panel_id = 0
        self.print_where_query=''
        self.print_select=''
        self.print_headers=[]
        self.secilenyakitid=""
        self.db = param.get_root( self ).DB
        self.O  = param.get_root( self ).O
        self.Freeze()
        self.LayoutItems()
        style.setPanelListBg( self )
        self.printTitle=MESSAGES.sablon_iki_kelime%( MESSAGES.vardiya, MESSAGES.raporu)
        self.Thaw()
        self.Show()
    def LayoutItems(self):
        
        self.bs   = wx.BoxSizer( wx.VERTICAL )
        self.SetSizer ( self.bs )
        self.printer = HtmlPrinter()
           
        p = wx.Panel(self,-1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        p.PAINT_GRADIENTS = True
        panelStyle = (p.PAINT_GRADIENTS and [wx.BORDER_NONE] or [0])[0]
        p.startColour = wx.Colour( 140,140,140 )
        p.endColour   = wx.WHITE
       
        gbs = wx.FlexGridSizer(1, 20,3,3)
        '''
        #vardiya ID aratabiliriz
        lbl_Plaka   = self.styl.TransparentText(p, label="Plaka :  ")
        self.txt_Plaka      = wx.TextCtrl  (p, -1, "",size=(80,-1))
        self.styl.setStaticTextList( lbl_Plaka)
        
        lbl_bas_dpc =self.styl.TransparentText(p,label="Ba�lang�� Tarihi :")
        self.styl.setStaticTextList(lbl_bas_dpc)
        self.bas_dpc = wx.DatePickerCtrl(p, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )
        '''
       
        vardiya_adi_lbl    = self.styl.TransparentText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.vardiya, MESSAGES.adi))
        self.vardiya_adi_cmb    = wx.ComboBox(p,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.vardiya_adi_cmb.Bind(wx.EVT_COMBOBOX,self.onvardiyaidgetir)
        self.styl.setStaticTextList( vardiya_adi_lbl)
        
        personel_parametric_objects=[]
        personel_parametric_objects=self.db.get_parameters_all("CARI_HESAPLAR",order="CARI_ADI",selects="ID_CARI_HESAP,CARI_ADI",where="ID_CARI_TIPI=1")
        personel_lbl    = self.styl.TransparentText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.personel, MESSAGES.adi))
        self.personel_cmb=wx.ComboBox(p,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.db.widgetMaker( self.personel_cmb ,personel_parametric_objects)
        self.styl.setStaticTextList( personel_lbl)
        
        lbl_bit_dpc =self.styl.TransparentText(p,label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.bitis, MESSAGES.tarihi))
        self.styl.setStaticTextList(lbl_bit_dpc)
        self.bit_dpc = wx.DatePickerCtrl(p, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )
        self.bit_dpc.Bind(wx.EVT_DATE_CHANGED,self.VardiyaDoldur)
        
        self.vardiya_adi_parametric_objects = []
        self.tarih= self.bit_dpc.GetValue() #+ t
        self.tarih=self.tarih.Format('%Y-%m-%d')
        self.vardiya_adi_parametric_objects = self.db.get_parameters_all("VIEW_VARDIYA_ADI",order="ID_VARDIYA_AKTIF",selects="ID_VARDIYA_AKTIF,VARDIYA_ADI",where="strftime('%%Y-%%m-%%d', TARIH)='%s'"%(self.tarih))
        self.db.widgetMaker( self.vardiya_adi_cmb , self.vardiya_adi_parametric_objects)
        
        odeme_parametric_objects = []
        odeme_parametric_objects = self.db.get_parameters_all("P_ODEME_SEKLI",order="ID_ODEME",selects="ID_ODEME,ODEME_SEKLI")
        
        odeme_lbl    = self.styl.TransparentText(p, label=MESSAGES.odeme)
        self.odeme_cmb    = wx.ComboBox(p,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.styl.setStaticTextList( odeme_lbl)
        if odeme_parametric_objects != []:
            a=dataObj()
            a.id=-1
            a.ad=MESSAGES.tumu
            odeme_parametric_objects.append(a) 
        self.db.widgetMaker( self.odeme_cmb , odeme_parametric_objects)
        
        self.btn_search_pressed  =  wx.Image("%sdb_search_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sdb_search_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_search  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%( MESSAGES.vardiya, MESSAGES.raporu))
        self.btn_search.Bind(wx.EVT_BUTTON, self.OnRaporuGetir)
        self.btn_search.SetToolTipString(MESSAGES.sablon_iki_kelime%( MESSAGES.vardiya, MESSAGES.raporu))
        self.btn_search.SetBitmapSelected(self.btn_search_pressed)
        style.setButtonFont( self.btn_search)

        self.btn_print_preview_pressed  =  wx.Image("%sdb_search_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sdb_search_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_print_preview  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%( MESSAGES.yazdir, MESSAGES.onizleme))
        self.btn_print_preview.Bind(wx.EVT_BUTTON, self.OnPrintPreview)
        self.btn_print_preview.SetToolTipString(MESSAGES.sablon_iki_kelime%( MESSAGES.yazdir, MESSAGES.onizleme))
        self.btn_print_preview.SetBitmapSelected(self.btn_print_preview_pressed)
        style.setButtonFont( self.btn_print_preview)
        self.btn_print_preview.Enable(False)
        
        gbs.AddMany( [  ((20,20)),
                        (lbl_bit_dpc ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.bit_dpc,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((20,20)),
                        (vardiya_adi_lbl,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.vardiya_adi_cmb,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((20,20)),
                        (personel_lbl,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.personel_cmb,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((20,20)),
                        (odeme_lbl,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.odeme_cmb,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((80,20)),
                        (self.btn_search,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        ((80,20)),
                        (self.btn_print_preview,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )
                        ])
        
        #self.sumpanel.Add(lbl_toplam_litre,0,wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        gbs.AddGrowableRow(0)
        p.SetSizerAndFit( gbs )
        style.BindEvents( p )
        
        #
        self.bs.Add( p,0,wx.ALL|wx.EXPAND )
    def VardiyaDoldur(self,event):
        self.Freeze()
        self.tarih = self.bit_dpc.GetValue()
        self.tarih = self.tarih.Format('%Y-%m-%d')
        vardiya_adi_parametric_objects = self.db.get_parameters_all("VIEW_VARDIYA_ADI",order="ID_VARDIYA_AKTIF",selects="ID_VARDIYA_AKTIF,VARDIYA_ADI",where="strftime('%%Y-%%m-%%d', TARIH)='%s'"%(self.tarih))
        
        self.db.widgetMakerConcateObject( self.vardiya_adi_cmb , vardiya_adi_parametric_objects)
        self.Refresh()
        self.Thaw()
    def onvardiyaidgetir(self,event):
        if self.vardiya_adi_cmb.GetSelection()==-1:            
            self.secilenvardiyatid=""
        else :
            self.secilenvardiyaid=self.vardiya_adi_cmb.GetClientData(self.vardiya_adi_cmb.GetSelection()).id
  
    def OnPrintPreview(self, event):
        self.print_headers
        headers         = ['%s,digit'%MESSAGES.no,
                           '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.pompa[0:4],MESSAGES.no)),
                           '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.pompa[0:4],MESSAGES.adi)),
                           '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.beyin[0:3],MESSAGES.no)),
                           '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tabanca[0:3],MESSAGES.no)),
                           '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.personel[0:5],MESSAGES.adi)),
                           '%s,string'%MESSAGES.yakit,
                           '%s,digit'%MESSAGES.litre,
                           '%s,currency'%(MESSAGES.sablon_iki_kelime%( MESSAGES.birim[0:1],MESSAGES.fiyat)),
                           '%s,currency'%MESSAGES.tutar,
                           '%s,datetime'%MESSAGES.tarih,
                           '%s,string'%MESSAGES.plaka,
                           '%s,string'%MESSAGES.odeme,
                           '%s,string'%(MESSAGES.sablon_uc_kelime%( MESSAGES.vardiya,MESSAGES.endeks,MESSAGES.acilis)),
                           '%s,string'%(MESSAGES.sablon_uc_kelime%( MESSAGES.vardiya,MESSAGES.endeks,MESSAGES.kapanis)),
                           '%s,string'%(MESSAGES.sablon_uc_kelime%( MESSAGES.vardiya,MESSAGES.endeks,MESSAGES.toplam)),
                           '%s,string'%(MESSAGES.sablon_uc_kelime%( MESSAGES.vardiya,MESSAGES.endeks,MESSAGES.satis)),
                           '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.vardiya[0:5],MESSAGES.adi))]
        column_spaces = [0.4,0.6,0.6,0.5,0.5,0.9,0.6,0.6,0.65,0.8,0.9,0.9,0.7,0.7,0.7,0.7,0.7,0.6]
        dataset = self.db.get_raw_data_all(table_name = "VIEW_VARDIYA_RAPOR",
                                 where=self.print_where_query,
                                 order="ID_SATIS ASC",
                                 selects=self.print_select)
        if dataset==[]:
            style.uyari_message(self,MESSAGES.bos_rapor,self.printTitle)
        else:
            PrintTablePreview( self, dataset,headers,column_spaces, self.printTitle )

    def OnRaporuGetir(self,event):
        self.Freeze()
        #headers         = ['SATIS NO,digit','ADA NO,digit','BEYIN NO,digit','TABANCA NO,digit','YAKIT ADI,string','LITRE,digit','BIRIM FIYAT,digit','TUTAR,digit','TARIH,date','PLAKA,string','ODEME TURU,string']
        #data_history    = self.db.get_raw_data_all("VIEW_SATIS_RAPOR",order = "ID_SATIS",
        #                  selects="ID_SATIS,ADA_NO,BEYIN_NO,TABANCA_NO,YAKIT_ADI,SATIS_LITRE,BIRIM_FIYAT,TUTAR,TARIH,PLAKA,ODEME_SEKLI")
        #t =  wx.DateSpan(days=1) 
        #bas_tar=self.bas_dpc.GetValue().Format('%Y-%m-%d')
        bit_tar=bit_tar = self.bit_dpc.GetValue() #+ t
        bit_tar=bit_tar.Format('%Y-%m-%d')
        
        where_query = " 1=1 " #" TARIH = '%s' " % (bit_tar)
        
        #print self.cmb_tank_adi.GetClientData(self.cmb_tank_adi.GetSelection()).id
        if self.vardiya_adi_cmb.Value != MESSAGES.tumu and self.vardiya_adi_cmb.GetSelection() !=-1:
            where_query += 'AND ID_VARDIYA_AKTIF = %s' %self.vardiya_adi_cmb.GetClientData(self.vardiya_adi_cmb.GetSelection()).id
        if  self.personel_cmb.Value != '':
            where_query += " AND CARI_ADI='%s'"%self.personel_cmb.Value
        if self.odeme_cmb.Value != MESSAGES.tumu and self.odeme_cmb.GetSelection() !=-1:
            where_query += ' AND ID_ODEME = %s' %self.odeme_cmb.GetClientData(self.odeme_cmb.GetSelection()).id
        '''
        if self.txt_Plaka.GetValue() !="":
            where_query+=" AND PLAKA= '%s'"%self.txt_Plaka.GetValue()
        '''
        selects="ID_SATIS,POMPA_NO,POMPA_ADI,BEYIN_NO,TABANCA_NO,CARI_ADI,YAKIT_ADI,SATIS_LITRE,BIRIM_FIYAT,TUTAR,TARIH,PLAKA,ODEME_SEKLI,VARDIYA_ADI,VARDIYA_ACILIS_TUTAR,VARDIYA_KAPANIS_TUTAR,VARDIYA_EDEKS_TOPLAM,VARDIYA_SATIS_SAYISI"

        headers         = ['%s,digit'%MESSAGES.no,
                           '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.pompa[0:4],MESSAGES.no)),
                           '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.pompa[0:4],MESSAGES.adi)),
                           '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.beyin[0:3],MESSAGES.no)),
                           '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tabanca[0:3],MESSAGES.no)),
                           '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.personel[0:5],MESSAGES.adi)),
                           '%s,string'%MESSAGES.yakit,
                           '%s,digit'%MESSAGES.litre,
                           '%s,currency'%(MESSAGES.sablon_iki_kelime%( MESSAGES.birim[0:1],MESSAGES.fiyat)),
                           '%s,currency'%MESSAGES.tutar,
                           '%s,datetime'%MESSAGES.tarih,
                           '%s,string'%MESSAGES.plaka,
                           '%s,string'%MESSAGES.odeme,
                           '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.vardiya[0:4],MESSAGES.adi)),
                           '%s,string'%(MESSAGES.sablon_uc_kelime%( MESSAGES.vardiya,MESSAGES.endeks,MESSAGES.acilis)),
                           '%s,string'%(MESSAGES.sablon_uc_kelime%( MESSAGES.vardiya,MESSAGES.endeks,MESSAGES.kapanis)),
                           '%s,string'%(MESSAGES.sablon_uc_kelime%( MESSAGES.vardiya,MESSAGES.endeks,MESSAGES.toplam)),
                           '%s,string'%(MESSAGES.sablon_uc_kelime%( MESSAGES.vardiya,MESSAGES.endeks,MESSAGES.satis)),
                           
                           ]
        
        if self.rapor_panel_id != 0:
            wx.FindWindowById( self.rapor_panel_id ).Destroy()
        
        self.print_select=selects
        self.print_where_query=where_query
        self.print_headers=headers
        PaginatingIndexPanel( self,"VIEW_VARDIYA_RAPOR",
                                    selects = selects,
                                    where=where_query,
                                    order = "ID_SATIS ASC",
                                    headers = headers,
                                    parent_sizer = self.bs,
                                    totalcoloum=" sum(SATIS_LITRE) as '%s',sum(TUTAR) as '%s'"%( MESSAGES.sablon_iki_kelime%( MESSAGES.toplam, MESSAGES.litre),MESSAGES.sablon_iki_kelime%(MESSAGES.toplam,MESSAGES.tutar)))#bu framein mainsizeri-- boxsizer olmak zorunda
        
        self.btn_print_preview.Enable(True)
        self.Layout()
        self.Thaw()
class win_Satis_Raporu(wx.Frame):
   
    def __init__(self, parent, id_, title):
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR ))
        self.styl=style
        self.rapor_panel_id = 0
        self.print_where_query=''
        self.print_select=''
        self.print_headers=[]
        self.secilenyakitid=""
        self.db = param.get_root( self ).DB
        self.O  = param.get_root( self ).O
        self.Freeze()
        self.LayoutItems()
        style.setPanelListBg( self )
        self.printTitle=MESSAGES.sablon_iki_kelime%( MESSAGES.satis, MESSAGES.raporu)
        self.Thaw()
        self.Show()
    def LayoutItems(self):
        
        self.bs   = wx.BoxSizer( wx.VERTICAL )
        self.SetSizer ( self.bs )
        self.printer = HtmlPrinter()
        
        p = wx.Panel(self,-1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        p.PAINT_GRADIENTS = True
        panelStyle = (p.PAINT_GRADIENTS and [wx.BORDER_NONE] or [0])[0]
        p.startColour = wx.Colour( 140,140,140 )
        p.endColour   = wx.WHITE
        '''
        self.sumpanel=wx.BoxSizer(wx.HORIZONTAL)
        toplam_panel = wx.Panel(self,-1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        toplam_panel.PAINT_GRADIENTS = True
        panelStyle = (p.PAINT_GRADIENTS and [wx.BORDER_NONE] or [0])[0]
        toplam_panel.startColour = wx.Colour( 140,140,140 )
        toplam_panel.endColour   = wx.WHITE
        toplam_panel.SetSizer(self.sumpanel)
        
        lbl_toplam_litre=self.styl.TransparentText(p, label="Toplam Litre :  ")
        '''
        
        gbs = wx.FlexGridSizer(1, 20,3,3)
        
        lbl_Plaka   = self.styl.TransparentText(p, label="%s :  "%MESSAGES.plaka)
        self.txt_Plaka      = wx.TextCtrl  (p, -1, "",size=(80,-1))
        self.styl.setStaticTextList( lbl_Plaka)
        
        lbl_bas_dpc =self.styl.TransparentText(p,label=MESSAGES.sablon_iki_kelime_dot%(MESSAGES.baslangic, MESSAGES.tarihi))
        self.styl.setStaticTextList(lbl_bas_dpc)
        self.bas_dpc = wx.DatePickerCtrl(p, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )    
        yakit_adi_parametric_objects = []
        yakit_adi_parametric_objects = self.db.get_parameters_all("YAKIT",order="ID_YAKIT",selects="ID_YAKIT,YAKIT_ADI",where="YAKIT_DURUM=1")
        
        yakit_adi_lbl    = self.styl.TransparentText(p, label=MESSAGES.sablon_iki_kelime_dot%(MESSAGES.yakit, MESSAGES.turu))
        self.yakit_adi_cmb    = wx.ComboBox(p,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.yakit_adi_cmb.Bind(wx.EVT_COMBOBOX,self.onyakitidgetir)
        self.styl.setStaticTextList( yakit_adi_lbl)
        if yakit_adi_parametric_objects != []:
            a=dataObj()
            a.id=-1
            a.ad=MESSAGES.tumu
            yakit_adi_parametric_objects.append(a) 
        self.db.widgetMaker( self.yakit_adi_cmb , yakit_adi_parametric_objects)
        
        
        odeme_parametric_objects = []
        odeme_parametric_objects = self.db.get_parameters_all("P_ODEME_SEKLI",order="ID_ODEME",selects="ID_ODEME,ODEME_SEKLI")
        
        odeme_lbl    = self.styl.TransparentText(p, label=MESSAGES.odeme)
        self.odeme_cmb    = wx.ComboBox(p,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.styl.setStaticTextList( odeme_lbl)
        if odeme_parametric_objects != []:
            a=dataObj()
            a.id=-1
            a.ad=MESSAGES.tumu
            odeme_parametric_objects.append(a) 
        self.db.widgetMaker( self.odeme_cmb , odeme_parametric_objects)
        
        
        
        lbl_bit_dpc =self.styl.TransparentText(p,label=MESSAGES.sablon_iki_kelime_dot%(MESSAGES.bitis, MESSAGES.tarihi))
        self.styl.setStaticTextList(lbl_bit_dpc)
        self.bit_dpc = wx.DatePickerCtrl(p, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )
        
        
        self.btn_search_pressed  =  wx.Image("%sdb_search_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sdb_search_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_search  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%(MESSAGES.satis, MESSAGES.raporu))
        self.btn_search.Bind(wx.EVT_BUTTON, self.OnRaporuGetir)
        self.btn_search.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.satis, MESSAGES.raporu))
        self.btn_search.SetBitmapSelected(self.btn_search_pressed)
        style.setButtonFont( self.btn_search)
        '''
        self.btn_print_pressed  =  wx.Image("%sdb_search_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sdb_search_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_print  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label="Rapor Yazd�r")
        self.btn_print.Bind(wx.EVT_BUTTON, self.OnPrint)
        self.btn_print.SetToolTipString("Rapor Yazd�r")
        self.btn_print.SetBitmapSelected(self.btn_print_pressed)
        style.setButtonFont( self.btn_print)
        '''
        self.btn_print_preview_pressed  =  wx.Image("%sdb_search_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sdb_search_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_print_preview  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%( MESSAGES.yazdir, MESSAGES.onizleme))
        self.btn_print_preview.Bind(wx.EVT_BUTTON, self.OnPrintPreview)
        self.btn_print_preview.SetToolTipString(MESSAGES.sablon_iki_kelime%( MESSAGES.yazdir, MESSAGES.onizleme))
        self.btn_print_preview.SetBitmapSelected(self.btn_print_preview_pressed)
        style.setButtonFont( self.btn_print_preview)
        self.btn_print_preview.Enable(False)
        
        gbs.AddMany( [  ((15,20)),
                        (lbl_Plaka ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.txt_Plaka,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((20,20)),
                        (lbl_bas_dpc ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.bas_dpc,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((20,20)),
                        (lbl_bit_dpc,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.bit_dpc,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((20,20)),
                        (yakit_adi_lbl,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.yakit_adi_cmb,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((20,20)),
                        (odeme_lbl,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.odeme_cmb,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((20,20)),
                        (self.btn_search,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        ((80,20)),
                        (self.btn_print_preview,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )
                        ])
        
        #self.sumpanel.Add(lbl_toplam_litre,0,wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        gbs.AddGrowableRow(0)
        p.SetSizerAndFit( gbs )
        style.BindEvents( p )
        
        #
        self.bs.Add( p,0,wx.ALL|wx.EXPAND )
        
    def onyakitidgetir(self,event):
            if self.yakit_adi_cmb.GetSelection()==-1:            
                self.secilenyakitid=""
            else :
                self.secilenyakitid=self.yakit_adi_cmb.GetClientData(self.yakit_adi_cmb.GetSelection()).id
    '''
    def OnPrint(self, event):
        #replace self.editor.text and self.editor.title with appropriate values.
        
        self.printer.Print(self.liste.Text,self.printTitle)
    '''    
    def OnPrintPreview(self, event):
        self.print_headers
        headers       = ['%s,string'%MESSAGES.no,
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.pompa[0:3],MESSAGES.no)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.pompa[0:3],MESSAGES.adi)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.beyin[0:3],MESSAGES.no)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tabanca[0:3],MESSAGES.no)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.personel[0:3],MESSAGES.adi)),
                        '%s,string'%MESSAGES.tank,
                        '%s,string'%MESSAGES.yakit,
                        '%s,digit'%MESSAGES.litre,
                        '%s,currency'%(MESSAGES.sablon_iki_kelime%( MESSAGES.birim[0:1],MESSAGES.fiyat)),
                        '%s,currency'%MESSAGES.tutar,
                        '%s,datetime'%MESSAGES.tarih,
                        '%s,string'%MESSAGES.plaka,
                        '%s,string'%MESSAGES.odeme]
        
        
        column_spaces = [0.6,0.6,0.8,0.6,0.6,0.6,0.6,1.0,0.75,1.0,1.0,0.75,0.75,0.6]
        dataset = self.db.get_raw_data_all(table_name = "VIEW_POMPA_SATIS_RAPOR",
                                 where=self.print_where_query,
                                 order="ID_SATIS ASC",
                                 selects=self.print_select)
                
        if dataset==[]:
            style.uyari_message(self,MESSAGES.bos_rapor,self.printTitle)
        else:
            PrintTablePreview( self, dataset,headers,column_spaces, self.printTitle )
    def OnRaporuGetir(self,event):
        self.Freeze()
        #headers         = ['SATIS NO,digit','ADA NO,digit','BEYIN NO,digit','TABANCA NO,digit','YAKIT ADI,string','LITRE,digit','BIRIM FIYAT,digit','TUTAR,digit','TARIH,date','PLAKA,string','ODEME TURU,string']
        #data_history    = self.db.get_raw_data_all("VIEW_SATIS_RAPOR",order = "ID_SATIS",
        #                  selects="ID_SATIS,ADA_NO,BEYIN_NO,TABANCA_NO,YAKIT_ADI,SATIS_LITRE,BIRIM_FIYAT,TUTAR,TARIH,PLAKA,ODEME_SEKLI")
        t =  wx.DateSpan(days=1) 
        bas_tar=self.bas_dpc.GetValue().Format('%Y-%m-%d')
        bit_tar=bit_tar = self.bit_dpc.GetValue() + t
        bit_tar=bit_tar.Format('%Y-%m-%d')
        
        where_query = " TARIH >= '%s' and TARIH <= '%s' " % (bas_tar,bit_tar)
        
        #print self.cmb_tank_adi.GetClientData(self.cmb_tank_adi.GetSelection()).id
        if self.yakit_adi_cmb.Value != MESSAGES.tumu and self.yakit_adi_cmb.GetSelection() !=-1:
            where_query += ' AND ID_YAKIT = %s' %self.yakit_adi_cmb.GetClientData(self.yakit_adi_cmb.GetSelection()).id
        if self.odeme_cmb.Value != MESSAGES.tumu and self.odeme_cmb.GetSelection() !=-1:
            
            where_query += ' AND ID_ODEME = %s' %self.odeme_cmb.GetClientData(self.odeme_cmb.GetSelection()).id
        if self.txt_Plaka.GetValue() !="":
            where_query+=" AND PLAKA= '%s'"%self.txt_Plaka.GetValue()
        selects="ID_SATIS,POMPA_NO,POMPA_ADI,BEYIN_NO,TABANCA_NO,CARI_ADI,YAKIT_TANKI_ADI,YAKIT_ADI,SATIS_LITRE,BIRIM_FIYAT,TUTAR,TARIH,PLAKA,ODEME_SEKLI"

        #headers         = ['SATIS NO,digit','POMPA NO,digit','POMPA ADI,string','BEYIN NO,digit','TABANCA NO,digit','PERSONEL ADI,string','YAKIT TANKI,string','YAKIT ADI,string','LITRE,digit','BIRIM FIYAT,digit','TUTAR,digit','TARIH,datetime','PLAKA,string','ODEME TURU,string']
        headers       = ['%s,string'%MESSAGES.no,
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.pompa[0:3],MESSAGES.no)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.pompa[0:3],MESSAGES.adi)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.beyin[0:3],MESSAGES.no)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tabanca[0:3],MESSAGES.no)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.personel[0:3],MESSAGES.adi)),
                        '%s,string'%MESSAGES.tank,
                        '%s,string'%MESSAGES.yakit,
                        '%s,digit'%MESSAGES.litre,
                        '%s,currency'%(MESSAGES.sablon_iki_kelime%( MESSAGES.birim[0:1],MESSAGES.fiyat)),
                        '%s,currency'%MESSAGES.tutar,
                        '%s,datetime'%MESSAGES.tarih,
                        '%s,string'%MESSAGES.plaka,
                        '%s,string'%MESSAGES.odeme]
        if self.rapor_panel_id != 0:
            wx.FindWindowById( self.rapor_panel_id ).Destroy()
        
        self.print_select=selects
        self.print_where_query=where_query
        self.print_headers=headers
        PaginatingIndexPanel( self,"VIEW_POMPA_SATIS_RAPOR",
                                    selects = selects,
                                    where=where_query,
                                    order = "ID_SATIS ASC",
                                    headers = headers,
                                    parent_sizer = self.bs,totalcoloum=" sum(SATIS_LITRE) as '%s',sum(TUTAR) as '%s'"%( MESSAGES.sablon_iki_kelime%( MESSAGES.toplam, MESSAGES.litre),MESSAGES.sablon_iki_kelime%(MESSAGES.toplam,MESSAGES.tutar)))#bu framein mainsizeri-- boxsizer olmak zorunda
        
        self.btn_print_preview.Enable(True)
        self.Layout()
        self.Thaw()  
class win_Tank_Durum(wx.Frame):
    def __init__(self, parent, id_, title):
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR ))
        self.styl=style
        
        self.db = param.get_root( self ).DB
        self.O  = param.get_root( self ).O
        self.rapor_panel_id = 0
        self.print_select=''
        self.Freeze()
        self.LayoutItems()
        self.printTitle =MESSAGES.sablon_uc_kelime%( MESSAGES.tank, MESSAGES.durum, MESSAGES.raporu)
        style.setPanelListBg( self )
        self.Thaw()
        self.Show()
    def LayoutItems(self):
        
        self.bs   = wx.BoxSizer( wx.VERTICAL )
        self.SetSizer ( self.bs )
        
        p = wx.Panel(self,-1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        p.PAINT_GRADIENTS = True
        panelStyle = (p.PAINT_GRADIENTS and [wx.BORDER_NONE] or [0])[0]
        p.startColour = wx.Colour( 140,140,140 )
        p.endColour   = wx.WHITE
        
        
        gbs = wx.FlexGridSizer(1,13,3,3)
        
        tank_adi_parametric_objects = []
        tank_adi_parametric_objects = self.db.get_parameters_all("YAKIT_TANKI",order="ID_YAKIT_TANKI",selects="ID_YAKIT_TANKI,YAKIT_TANKI_ADI",where="ID_ACTIVE=1")
        lbl_tank_adi    = self.styl.TransparentText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.tank, MESSAGES.adi))
        self.cmb_tank_adi    = wx.ComboBox(p,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.cmb_tank_adi.Bind(wx.EVT_COMBOBOX,self.ontankidgetir)
        self.db.widgetMaker( self.cmb_tank_adi , tank_adi_parametric_objects)
        self.styl.setStaticTextList(lbl_tank_adi)
        
        lbl_bas_dpc =self.styl.TransparentText(p,label=MESSAGES.sablon_iki_kelime_dot%(MESSAGES.baslangic, MESSAGES.tarihi))
        self.styl.setStaticTextList(lbl_bas_dpc)
        self.bas_dpc = wx.DatePickerCtrl(p, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )
        
        lbl_bit_dpc =self.styl.TransparentText(p,label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.bitis, MESSAGES.tarihi))
        self.styl.setStaticTextList(lbl_bit_dpc)
        self.bit_dpc = wx.DatePickerCtrl(p, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )
        
        
        self.btn_search_pressed  =  wx.Image("%sdb_search_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sdb_search_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_search  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%(MESSAGES.tank, MESSAGES.raporu))
        self.btn_search.Bind(wx.EVT_BUTTON, self.OnRaporuGetir)
        self.btn_search.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.tank, MESSAGES.raporu))
        self.btn_search.SetBitmapSelected(self.btn_search_pressed)
        style.setButtonFont( self.btn_search)
        
        self.btn_print_preview_pressed  =  wx.Image("%sdb_search_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sdb_search_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_print_preview  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%(MESSAGES.yazdir, MESSAGES.onizleme))
        self.btn_print_preview.Bind(wx.EVT_BUTTON, self.OnPrintPreview)
        self.btn_print_preview.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.yazdir, MESSAGES.onizleme))
        self.btn_print_preview.SetBitmapSelected(self.btn_print_preview_pressed)
        style.setButtonFont( self.btn_print_preview)
        self.btn_print_preview.Enable(False)
        
        
        gbs.AddMany( [  ((80,20)),
                        (lbl_tank_adi ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.cmb_tank_adi,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((20,20)),
                        (lbl_bas_dpc ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.bas_dpc,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((20,20)),
                        (lbl_bit_dpc,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.bit_dpc,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((160,20)),
                        (self.btn_search,0, wx.ALIGN_CENTER|wx.ALIGN_CENTER_VERTICAL ),
                        ((80,20)),
                        (self.btn_print_preview,0, wx.ALIGN_CENTER|wx.ALIGN_CENTER_VERTICAL )
                        ])
        
        p.SetSizerAndFit( gbs )
        style.BindEvents( p )
        
        #
        self.bs.Add( p,0,wx.ALL|wx.EXPAND )
        
    def onyakitidgetir(self,event):
        if self.yakit_adi_cmb.GetSelection()==-1:            
            self.secilenyakitid=""
        else :
            self.secilenyakitid=self.yakit_adi_cmb.GetClientData(self.yakit_adi_cmb.GetSelection()).id
    def ontankidgetir(self,event):
        if self.cmb_tank_adi.GetSelection()==-1:            
            self.secilentankid=""
        else :
            self.secilentankid=self.cmb_tank_adi.GetClientData(self.cmb_tank_adi.GetSelection()).id
    def OnPrintPreview(self, event):
        self.print_headers
        '''
        headers       = ['KAYIT SAYISI',
                         'TANK ID',
                         'TANK ADI',
                         'YAKIT ADI',
                         'TARIH',
                         'YAKIT MIKTARI',
                         'SU MIKTARI',
                         'TANK DOLUM',
                         'POMPA SATIS',
                         'ACIKLAMA',
                         'DOLUM TIPI']
        '''
        headers       = ['%s,digit'%MESSAGES.no,
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tank,MESSAGES.no)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tank,MESSAGES.adi)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.yakit,MESSAGES.adi)),
                        '%s,datetime'%MESSAGES.tarih,
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.yakit,MESSAGES.miktar)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.su,MESSAGES.miktar)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tank,MESSAGES.dolum)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.pompa,MESSAGES.satis)),
                        '%s,string'%MESSAGES.aciklama,
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.dolum,MESSAGES.turu))]
                        
        selects="ID_TANK_KAYITLARI,ID_YAKIT_TANKI,YAKIT_TANKI_ADI,YAKIT_ADI,TARIH,OKUNAN_YAKIT,OKUNAN_SU,TANK_DOLUMU,POMPA_SATISI,H_ACIKLAMA,DOLUM_TIPI"
        
        column_spaces = [0.6,0.6,0.8,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0]
        dataset = self.db.get_raw_data_all(table_name = "VIEW_TANK_HRKT_RAPOR",
                                 where=self.print_where_query,
                                 order="ID_TANK_KAYITLARI ASC",
                                 selects=selects)
                

        if dataset==[]:
            style.uyari_message(self,MESSAGES.bos_rapor,self.printTitle)
        else:
            PrintTablePreview( self, dataset,headers,column_spaces, self.printTitle )
    def OnRaporuGetir(self,event):
        self.Freeze()
        
        t =  wx.DateSpan(days=1) 
        bit_tar = self.bit_dpc.GetValue() + t
        bas_tar=self.bas_dpc.GetValue().Format('%Y-%m-%d')
        bit_tar=bit_tar.Format('%Y-%m-%d')
        
        where_query = " TARIH >= '%s' and TARIH <= '%s' " % (bas_tar,bit_tar)
        #print self.cmb_tank_adi.GetClientData(self.cmb_tank_adi.GetSelection()).id
        if self.cmb_tank_adi.GetSelection() !=-1 and self.cmb_tank_adi.Value!=MESSAGES.tumu:
            where_query += ' AND ID_YAKIT_TANKI = %s' % self.cmb_tank_adi.GetClientData(self.cmb_tank_adi.GetSelection()).id
        
        #headers         = ['KAYIT_SAYISI,digit','TANK ID,digit','TANK ADI,string','YAKIT ADI,string','TARIH,datetime','YAKIT MIKTARI,digit','SU MIKTARI,string','TANK DOLUM,digit','POMPA SATIS,digit','ACIKLAMA,string','DOLUM TIPI,string']
        headers       = ['%s,digit'%MESSAGES.no,
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tank,MESSAGES.no)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tank,MESSAGES.adi)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.yakit,MESSAGES.adi)),
                        '%s,datetime'%MESSAGES.tarih,
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.yakit,MESSAGES.miktar)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.su,MESSAGES.miktar)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tank,MESSAGES.dolum)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.pompa,MESSAGES.satis)),
                        '%s,string'%MESSAGES.aciklama,
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.dolum,MESSAGES.turu))]
        selects="ID_TANK_KAYITLARI,ID_YAKIT_TANKI,YAKIT_TANKI_ADI,YAKIT_ADI,TARIH,OKUNAN_YAKIT,OKUNAN_SU,TANK_DOLUMU,POMPA_SATISI,H_ACIKLAMA,DOLUM_TIPI"
        
        self.print_select=selects
        self.print_where_query=where_query
        self.print_headers=headers
        
        #burdan sonras� generic. copy paste
        if self.rapor_panel_id != 0:
            wx.FindWindowById( self.rapor_panel_id ).Destroy()
        
        PaginatingIndexPanel( self,"VIEW_TANK_HRKT_RAPOR",
                                    selects = selects,
                                    where=where_query,
                                    order = "ID_TANK_KAYITLARI ASC",
                                    headers = headers,
                                    parent_sizer = self.bs)#bu framein mainsizeri-- boxsizer olmak zorunda
        
        self.btn_print_preview.Enable()
        self.Layout()
        self.Thaw()

class win_Gunsonu_rapor(wx.Frame):
    def __init__(self, parent, id_, title):
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR ))
        self.styl=style
        
        self.db = param.get_root( self ).DB
        self.O  = param.get_root( self ).O
        self.rapor_panel_id = 0
        self.print_select=''
        self.Freeze()
        self.LayoutItems()
        self.printTitle =MESSAGES.sablon_iki_kelime%( MESSAGES.gunsonu, MESSAGES.raporu)
        style.setPanelListBg( self )
        self.Thaw()
        self.Show()
    def LayoutItems(self):
        
        self.bs   = wx.BoxSizer( wx.VERTICAL )
        self.SetSizer ( self.bs )
        
        p = wx.Panel(self,-1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        p.PAINT_GRADIENTS = True
        panelStyle = (p.PAINT_GRADIENTS and [wx.BORDER_NONE] or [0])[0]
        p.startColour = wx.Colour( 140,140,140 )
        p.endColour   = wx.WHITE
        
        
        gbs = wx.FlexGridSizer(1,13,3,3)
        
        tank_adi_parametric_objects = []
        tank_adi_parametric_objects = self.db.get_parameters_all("YAKIT_TANKI",order="ID_YAKIT_TANKI",selects="ID_YAKIT_TANKI,YAKIT_TANKI_ADI",where="ID_ACTIVE=1")
        lbl_tank_adi    = self.styl.TransparentText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.tank, MESSAGES.adi))
        self.cmb_tank_adi    = wx.ComboBox(p,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.cmb_tank_adi.Bind(wx.EVT_COMBOBOX,self.ontankidgetir)
        self.db.widgetMaker( self.cmb_tank_adi , tank_adi_parametric_objects)
        self.styl.setStaticTextList(lbl_tank_adi)
        
        lbl_bas_dpc =self.styl.TransparentText(p,label=MESSAGES.sablon_iki_kelime_dot%(MESSAGES.baslangic, MESSAGES.tarihi))
        self.styl.setStaticTextList(lbl_bas_dpc)
        self.bas_dpc = wx.DatePickerCtrl(p, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )
        
        lbl_bit_dpc =self.styl.TransparentText(p,label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.bitis, MESSAGES.tarihi))
        self.styl.setStaticTextList(lbl_bit_dpc)
        self.bit_dpc = wx.DatePickerCtrl(p, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )
        
        
        self.btn_search_pressed  =  wx.Image("%sdb_search_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sdb_search_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_search  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%(MESSAGES.gunsonu, MESSAGES.raporu))
        self.btn_search.Bind(wx.EVT_BUTTON, self.OnRaporuGetir)
        self.btn_search.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.gunsonu, MESSAGES.raporu))
        self.btn_search.SetBitmapSelected(self.btn_search_pressed)
        style.setButtonFont( self.btn_search)
        
        self.btn_print_preview_pressed  =  wx.Image("%sdb_search_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sdb_search_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_print_preview  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%(MESSAGES.yazdir, MESSAGES.onizleme))
        self.btn_print_preview.Bind(wx.EVT_BUTTON, self.OnPrintPreview)
        self.btn_print_preview.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.yazdir, MESSAGES.onizleme))
        self.btn_print_preview.SetBitmapSelected(self.btn_print_preview_pressed)
        style.setButtonFont( self.btn_print_preview)
        self.btn_print_preview.Enable(False)
        
        
        gbs.AddMany( [  ((80,20)),
                        (lbl_tank_adi ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.cmb_tank_adi,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((20,20)),
                        (lbl_bas_dpc ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.bas_dpc,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((20,20)),
                        (lbl_bit_dpc,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.bit_dpc,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((160,20)),
                        (self.btn_search,0, wx.ALIGN_CENTER|wx.ALIGN_CENTER_VERTICAL ),
                        ((80,20)),
                        (self.btn_print_preview,0, wx.ALIGN_CENTER|wx.ALIGN_CENTER_VERTICAL )
                        ])
        
        p.SetSizerAndFit( gbs )
        style.BindEvents( p )
        
        #
        self.bs.Add( p,0,wx.ALL|wx.EXPAND )
        
    def onyakitidgetir(self,event):
        if self.yakit_adi_cmb.GetSelection()==-1:            
            self.secilenyakitid=""
        else :
            self.secilenyakitid=self.yakit_adi_cmb.GetClientData(self.yakit_adi_cmb.GetSelection()).id
    def ontankidgetir(self,event):
        if self.cmb_tank_adi.GetSelection()==-1:            
            self.secilentankid=""
        else :
            self.secilentankid=self.cmb_tank_adi.GetClientData(self.cmb_tank_adi.GetSelection()).id
    def OnPrintPreview(self, event):
        self.print_headers
        
        headers       = [
                        '%s,digit'%(MESSAGES.sablon_iki_kelime% ( MESSAGES.tank,MESSAGES.no)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tank,MESSAGES.adi)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.yakit,MESSAGES.adi)),
                        '%s,datetime'%MESSAGES.tarih,
                        '%s,digit'%(MESSAGES.sablon_iki_kelime% ( MESSAGES.baslangic,MESSAGES.miktar)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime% ( MESSAGES.toplam,MESSAGES.dolum)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime% ( MESSAGES.toplam,MESSAGES.satis)),
                        '%s,digit'%(MESSAGES.sablon_uc_kelime%  ( MESSAGES.son,MESSAGES.olculen,MESSAGES.miktar)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime% ( MESSAGES.olculen,MESSAGES.fark)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime% ( MESSAGES.olmasi_gereken,MESSAGES.miktar)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime% ( MESSAGES.olmasi_gereken,MESSAGES.fark))]
                        
        selects="ID_YAKIT_TANKI,YAKIT_TANKI_ADI,YAKIT_ADI,TARIH,BASLANGIC_YAKIT,TOTAL_DOLUM,TOTAL_SATIS,OKUNAN_YAKIT,PROBE_KALAN,SATIS_KALAN,GUN_SONU_FARKI"
        
        column_spaces = [0.6,0.6,0.8,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0]
        dataset = self.db.get_raw_data_all(table_name = "GUNSONU_RAPORLARI",
                                 where=self.print_where_query,
                                 order="TARIH ASC",
                                 selects=selects)
                

        if dataset==[]:
            style.uyari_message(self,MESSAGES.bos_rapor,self.printTitle)
        else:
            PrintTablePreview( self, dataset,headers,column_spaces, self.printTitle )
    def OnRaporuGetir(self,event):
        self.Freeze()
        
        t =  wx.DateSpan(days=1) 
        bit_tar = self.bit_dpc.GetValue() + t
        bas_tar = self.bas_dpc.GetValue().Format('%Y-%m-%d')
        bit_tar = bit_tar.Format('%Y-%m-%d')
        
        where_query = " TARIH >= '%s' and TARIH <= '%s' " % (bas_tar,bit_tar)
        #print self.cmb_tank_adi.GetClientData(self.cmb_tank_adi.GetSelection()).id
        if self.cmb_tank_adi.GetSelection() !=-1 and self.cmb_tank_adi.Value!=MESSAGES.tumu:
            where_query += ' AND ID_YAKIT_TANKI = %s' % self.cmb_tank_adi.GetClientData(self.cmb_tank_adi.GetSelection()).id
        
        headers       = [
                        '%s,digit'%(MESSAGES.sablon_iki_kelime% ( MESSAGES.tank,MESSAGES.no)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tank,MESSAGES.adi)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.yakit,MESSAGES.adi)),
                        '%s,datetime'%MESSAGES.tarih,
                        '%s,digit'%(MESSAGES.sablon_iki_kelime% ( MESSAGES.baslangic,MESSAGES.miktar)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime% ( MESSAGES.toplam,MESSAGES.dolum)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime% ( MESSAGES.toplam,MESSAGES.satis)),
                        '%s,digit'%(MESSAGES.sablon_uc_kelime%  ( MESSAGES.son,MESSAGES.olculen,MESSAGES.miktar)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime% ( MESSAGES.olculen,MESSAGES.fark)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime% ( MESSAGES.olmasi_gereken,MESSAGES.miktar)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime% ( MESSAGES.olmasi_gereken,MESSAGES.fark))]
        
        selects="ID_YAKIT_TANKI,YAKIT_TANKI_ADI,YAKIT_ADI,TARIH,BASLANGIC_YAKIT,TOTAL_DOLUM,TOTAL_SATIS,OKUNAN_YAKIT,PROBE_KALAN,SATIS_KALAN,GUN_SONU_FARKI"
        
        self.print_select=selects
        self.print_where_query=where_query
        self.print_headers=headers
        
        #burdan sonras� generic. copy paste
        if self.rapor_panel_id != 0:
            wx.FindWindowById( self.rapor_panel_id ).Destroy()
        
        PaginatingIndexPanel( self,"GUNSONU_RAPORLARI",
                                    selects      = selects,
                                    where        = where_query,
                                    order        = "TARIH",
                                    headers      = headers,
                                    parent_sizer = self.bs )#bu framein mainsizeri-- boxsizer olmak zorunda
        
        self.btn_print_preview.Enable()
        self.Layout()
        self.Thaw()

        
class win_Genel_Durum(wx.Frame):
    def __init__(self, parent, id_, title):
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR ))
        self.styl=style 
        self.db = param.get_root( self ).DB
        self.O  = param.get_root( self ).O 
        self.LayoutItems()
        self.rapor_panel_id = 0
    
    def LayoutItems(self):
        
        self.Freeze()
        self.bs   = wx.BoxSizer( wx.VERTICAL )
        self.SetSizer ( self.bs )
        self.check_list = []
       
        self.book  = aui.AuiNotebook(self,1,agwStyle=(aui.AUI_NB_TOP|aui.AUI_NB_SCROLL_BUTTONS))
        self.book.SetArtProvider(aui.AuiDefaultTabArt())
        self.bs.Add(self.book, 1,wx.ALL | wx.EXPAND)
        
        self.p  = CreateAylikBilgi( self.book ) 
        self.p2 = CreateSatisBilgi(self.book)
        self.p3 = CreateToplamSatisBilgi(self.book)
       

        self.book.AddPage( self.p,MESSAGES.sablon_uc_kelime%( MESSAGES.detayli, MESSAGES.tank, MESSAGES.bilgisi),True )
        self.book.AddPage( self.p2,MESSAGES.sablon_uc_kelime%( MESSAGES.detayli, MESSAGES.satis, MESSAGES.bilgisi) )
        self.book.AddPage( self.p3,MESSAGES.sablon_dort_kelime%( MESSAGES.detayli,MESSAGES.toplam, MESSAGES.satis, MESSAGES.bilgisi) )
    
        
        il = wx.ImageList(64, 64)
        img0 = il.Add(wx.Bitmap('%stank_list_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img1 = il.Add(wx.Bitmap('%spompa_list_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img2 = il.Add(wx.Bitmap('%spompa_list_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        
        self.book.AssignImageList(il)
        #self.book.Bind(aui.EVT_AUINOTEBOOK_PAGE_CHANGED, self.OnBookPageChanged)
 
        self.book.SetPageImage(0, img0)
        self.book.SetPageImage(1, self.img1)
        self.book.SetPageImage(2, self.img2)
        
        self.Thaw()
        self.Show()
        
class CreateToplamSatisBilgi(wx.Panel):
    
    def __init__( self, parent):
        
        wx.Panel.__init__(self, parent ,1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        self.db           = param.get_root( self ).DB 
        self.boxsizer   = wx.BoxSizer( wx.VERTICAL )
        self.SetSizer( self.boxsizer )
        self.rapor_panel_id = 0
        self.Freeze()
        self.createAboutToplamSatisBilgi()
        style.setPanelListBg( self )
        self.printTitle=MESSAGES.sablon_dort_kelime%( MESSAGES.detayli,MESSAGES.toplam, MESSAGES.satis, MESSAGES.bilgisi)
        self.Thaw()
    
    def createAboutToplamSatisBilgi(self ):

        self.p = wx.Panel(self,-1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        p=self.p
        p.SetSize((1300,100))
        p.PAINT_GRADIENTS = True
        panelStyle = (p.PAINT_GRADIENTS and [wx.BORDER_NONE] or [0])[0]
        p.style = panelStyle
        
        sizer        = wx.FlexGridSizer( 1,19,5,1)
        p.startColour = wx.Colour( 140,140,140 )
        #self.startColour =  wx.RED 
        p.endColour   = wx.WHITE
        
        self.cari_adi_parametric_objects = []
        self.cari_adi_parametric_objects = self.db.get_parameters_all("CARI_HESAPLAR",order="CARI_ADI",selects="ID_CARI_HESAP,CARI_ADI",where="ID_ACTIVE=1 AND ID_CARI_TIPI=2")
        
        cari_adi_lbl    = style.TransparentText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.cari,MESSAGES.adi))
        self.cari_adi_cmb    = wx.ComboBox(p,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.cari_adi_cmb.Bind(wx.EVT_COMBOBOX,self.oncariidgetir)
        style.setStaticTextList( cari_adi_lbl)
        if self.cari_adi_parametric_objects != []:
            a=dataObj()
            a.id=-1
            a.ad=MESSAGES.tumu
            self.cari_adi_parametric_objects.append(a) 
            self.db.widgetMaker( self.cari_adi_cmb , self.cari_adi_parametric_objects)
            
      
        lbl_bas_dpc = style.TransparentText(p,label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.baslangic,MESSAGES.tarihi))
        style.setStaticTextList(lbl_bas_dpc)
        self.bas_dpc = wx.DatePickerCtrl(p, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )
        
        lbl_bit_dpc =style.TransparentText(p,label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.bitis,MESSAGES.tarihi))
        style.setStaticTextList(lbl_bit_dpc)
        self.bit_dpc = wx.DatePickerCtrl(p, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )
        
        
        self.btn_search_pressed  =  wx.Image("%sdb_search_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sdb_search_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_search  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%( MESSAGES.satis,MESSAGES.raporu))
        self.btn_search.Bind(wx.EVT_BUTTON, self.OnRaporuGetir)
        self.btn_search.SetToolTipString(MESSAGES.sablon_iki_kelime%( MESSAGES.satis,MESSAGES.raporu))
        self.btn_search.SetBitmapSelected(self.btn_search_pressed)
        style.setButtonFont( self.btn_search)
        
        self.btn_print_preview_pressed  =  wx.Image("%sdb_search_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sdb_search_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_print_preview  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%( MESSAGES.yazdir,MESSAGES.onizleme))
        self.btn_print_preview.Bind(wx.EVT_BUTTON, self.OnPrintPreview)
        self.btn_print_preview.SetToolTipString(MESSAGES.sablon_iki_kelime%( MESSAGES.yazdir,MESSAGES.onizleme))
        self.btn_print_preview.SetBitmapSelected(self.btn_print_preview_pressed)
        style.setButtonFont( self.btn_print_preview)
        self.btn_print_preview.Enable(False)        
        
        sizer.AddMany( [((10,20)),
                        (cari_adi_lbl,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.cari_adi_cmb,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((10,20)),
                        (lbl_bas_dpc ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.bas_dpc,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((10,20)),
                        (lbl_bit_dpc,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.bit_dpc,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((10,20)),
                        (self.btn_search,0, wx.ALIGN_CENTER|wx.ALIGN_CENTER_VERTICAL ),
                        ((10,20)),
                        (self.btn_print_preview,0, wx.ALIGN_CENTER|wx.ALIGN_CENTER_VERTICAL ),
                       
                        ])

        
        
        p.SetSizerAndFit( sizer )
        style.BindEvents( p )
        self.boxsizer.Add( p,0, wx.ALL|wx.EXPAND )
        
    
    def oncariidgetir(self,event):
        if self.cari_adi_cmb.GetSelection()==-1:            
            self.secilencariid=""
        else :
            self.secilencariid=self.cari_adi_cmb.GetClientData(self.cari_adi_cmb.GetSelection()).id

    def OnPrintPreview(self, event):

        column_spaces = [0.6,3.0,1.0,1.0,1.0,1.0,1.0]
        dataset,headers  = createCariSatislar( self,self.print_selects,self.print_where_query,self.print_order,self.print_group_by,self.print_cari_hesaplar,self.print_cari_hesap,True)
        
        if dataset==[]:
            style.uyari_message(self,MESSAGES.bos_rapor,self.printTitle)
        else:
            PrintTablePreview( self.GrandParent, dataset,headers,column_spaces, self.printTitle )
    def OnRaporuGetir(self,event):
        self.Freeze()
        
        t =  wx.DateSpan(days=1) 
        bit_tar = self.bit_dpc.GetValue() + t
        bas_tar=self.bas_dpc.GetValue().Format('%Y-%m-%d')
        bit_tar=bit_tar.Format('%Y-%m-%d')
        cari_hesaplar = []
        cari_hesap    = []
        where_query = " TARIH >= '%s' and TARIH <= '%s' " % (bas_tar,bit_tar)
        if self.cari_adi_cmb.GetSelection() !=-1 and self.cari_adi_cmb.Value!=MESSAGES.tumu:
            where_query += ' AND ID_MUSTERI_CARI = %s' %self.cari_adi_cmb.GetClientData(self.cari_adi_cmb.GetSelection()).id
            cari_hesap = sabitler.dummy()
            cari_hesap.id = self.cari_adi_cmb.GetClientData(self.cari_adi_cmb.GetSelection()).id
            cari_hesap.ad =self.cari_adi_cmb.GetClientData(self.cari_adi_cmb.GetSelection()).ad
        else:
            #id_cari_hesaplar = self.db.get_raw_data_all("CARI_HESAPLAR",selects="ID_CARI_HESAP,CARI_ADI",where="ID_ACTIVE=1 AND ID_CARI_TIPI=2")
            cari_hesaplar = self.cari_adi_parametric_objects[:len(self.cari_adi_parametric_objects)]
            where_query += ' AND ID_MUSTERI_CARI = %s'

        selects  = "ID_MUSTERI_CARI,CARI_ADI,ID_ODEME,ODEME_SEKLI,SUM(TUTAR)"
        order    = "ID_MUSTERI_CARI"
        group_by = "ID_MUSTERI_CARI,ID_ODEME"
        if wx.FindWindowById( sabitler.EKRAN_ID_MAP.RAPORLAR_EKRANLAR.id_e_genel_toplam_satis_rapor ):
            wx.FindWindowById( sabitler.EKRAN_ID_MAP.RAPORLAR_EKRANLAR.id_e_genel_toplam_satis_rapor ).Destroy()
            
        self.print_selects       = selects
        self.print_where_query   = where_query
        self.print_order         = order
        self.print_group_by      = group_by
        self.print_cari_hesaplar = cari_hesaplar
        self.print_cari_hesap    = cari_hesap
        
        self.cari_satis_frame = createCariSatislar( self,selects,where_query,order,group_by,cari_hesaplar,cari_hesap,False)
        self.boxsizer.Add( self.cari_satis_frame,1,wx.EXPAND)
        self.btn_print_preview.Enable()
        self.Layout()
        self.Thaw()
class CreateAylikBilgi(wx.Panel ):
    def __init__( self, parent):
        
        wx.Panel.__init__(self, parent ,1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        self.db           = param.get_root( self ).DB 
        self.boxsizer   = wx.BoxSizer( wx.VERTICAL )
        self.SetSizer( self.boxsizer )
        self.rapor_panel_id = 0
        self.printTitle=MESSAGES.sablon_uc_kelime%( MESSAGES.detayli, MESSAGES.tank, MESSAGES.bilgisi)
        self.Freeze()
        self.createAboutAylikBilgi()
        style.setPanelListBg( self )
        self.Thaw()
    
    def createAboutAylikBilgi(self ):

        
        p = wx.Panel(self,-1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        
        p.PAINT_GRADIENTS = True
        panelStyle = (p.PAINT_GRADIENTS and [wx.BORDER_NONE] or [0])[0]
        p.style = panelStyle
        
        sizer        = wx.FlexGridSizer( 1,18,5,1)
        p.startColour = wx.Colour( 140,140,140 )
        #self.startColour =  wx.RED 
        p.endColour   = wx.WHITE
        

        tank_adi_parametric_objects = []
        tank_adi_parametric_objects = self.db.get_parameters_all("YAKIT_TANKI",order="ID_YAKIT_TANKI",selects="ID_YAKIT_TANKI,YAKIT_TANKI_ADI",where="ID_ACTIVE=1")
        lbl_tank_adi    = style.TransparentText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.tank,MESSAGES.adi))
        self.cmb_tank_adi    = wx.ComboBox(p,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.cmb_tank_adi.Bind(wx.EVT_COMBOBOX,self.ontankidgetir)
        #self.db.widgetMaker( self.cmb_tank_adi , tank_adi_parametric_objects)
        self.db.widgetMaker( self.cmb_tank_adi , tank_adi_parametric_objects)
        style.setStaticTextList(lbl_tank_adi)
        if tank_adi_parametric_objects != []:
            a=dataObj()
            a.id=-1
            a.ad=MESSAGES.tumu
            tank_adi_parametric_objects.append(a) 
            self.db.widgetMaker( self.cmb_tank_adi , tank_adi_parametric_objects)
            
        hareket_adi_parametric_objects = []
        hareket_adi_parametric_objects = self.db.get_parameters_all("P_TANK_H_CESIT",order="ID_THC",selects="ID_THC,H_ACIKLAMA")
        lbl_hareket_adi    = style.TransparentText(p, label="%s:  "%MESSAGES.aciklama)
        
        self.cmb_hareket_adi    = wx.CheckListBox(p,choices=[],size=(160, 50))#, style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        #self.cmb_hareket_adi.Bind(wx.EVT_CHECKLISTBOX,self.onhareketidgetir)
        #self.db.widgetMaker( self.cmb_tank_adi , tank_adi_parametric_objects)
        self.db.widgetMaker( self.cmb_hareket_adi , hareket_adi_parametric_objects)
        style.setStaticTextList(lbl_hareket_adi)
       
        '''
        lbl_ay_adi    = style.TransparentText(p, label="Ay:  ")
        self.cmb_ay_adi    = wx.ComboBox(p,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.cmb_ay_adi.Bind(wx.EVT_COMBOBOX,self.onayidgetir)
        #self.db.widgetMaker( self.cmb_tank_adi , tank_adi_parametric_objects)
        self.db.widgetMaker( self.cmb_ay_adi , sabitler.aylar_object_list)
        style.setStaticTextList(lbl_ay_adi)
        if lbl_ay_adi != []:
            a=dataObj()
            a.id=-1
            a.ad=MESSAGES.tumu
            sabitler.aylar_object_list.append(a) 
            self.db.widgetMaker( self.cmb_ay_adi , sabitler.aylar_object_list)
            lbl_bas_dpc =self.styl.TransparentText(p,label=MESSAGES.sablon_iki_kelime_dot%(MESSAGES.baslangic, MESSAGES.tarihi))
        self.styl.setStaticTextList(lbl_bas_dpc)
        self.bas_dpc = wx.DatePickerCtrl(p, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )
        
        lbl_bit_dpc =self.styl.TransparentText(p,label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.bitis, MESSAGES.tarihi))
        '''
        lbl_bas_dpc = style.TransparentText(p,label=MESSAGES.sablon_iki_kelime_dot%(MESSAGES.baslangic, MESSAGES.tarihi))
        style.setStaticTextList(lbl_bas_dpc)
        self.bas_dpc = wx.DatePickerCtrl(p, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )
        
        lbl_bit_dpc =style.TransparentText(p,label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.bitis, MESSAGES.tarihi))
        style.setStaticTextList(lbl_bit_dpc)
        self.bit_dpc = wx.DatePickerCtrl(p, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )
        
        
        self.btn_search_pressed  =  wx.Image("%sdb_search_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sdb_search_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_search  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%(MESSAGES.tank, MESSAGES.raporu))
        self.btn_search.Bind(wx.EVT_BUTTON, self.OnRaporuGetir)
        self.btn_search.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.tank, MESSAGES.raporu))
        self.btn_search.SetBitmapSelected(self.btn_search_pressed)
        style.setButtonFont( self.btn_search)
        
        self.btn_print_preview_pressed  =  wx.Image("%sdb_search_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sdb_search_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_print_preview  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%(MESSAGES.yazdir, MESSAGES.onizleme))
        self.btn_print_preview.Bind(wx.EVT_BUTTON, self.OnPrintPreview)
        self.btn_print_preview.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.yazdir, MESSAGES.onizleme))
        self.btn_print_preview.SetBitmapSelected(self.btn_print_preview_pressed)
        style.setButtonFont( self.btn_print_preview)
        self.btn_print_preview.Enable(False)
        
        
        sizer.AddMany( [ #(lbl_ay_adi ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        #(self.cmb_ay_adi,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((10,20)),
                        (lbl_tank_adi ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.cmb_tank_adi,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((10,20)),
                        (lbl_hareket_adi ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.cmb_hareket_adi,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((10,20)),
                        (lbl_bas_dpc ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.bas_dpc,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((10,20)),
                        (lbl_bit_dpc,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.bit_dpc,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((20,20)),
                        (self.btn_search,0, wx.ALIGN_CENTER|wx.ALIGN_CENTER_VERTICAL ),
                        ((10,20)),
                        (self.btn_print_preview,0, wx.ALIGN_CENTER|wx.ALIGN_CENTER_VERTICAL )
                        ])

        
        
        p.SetSizerAndFit( sizer )
        style.BindEvents( p )
        self.boxsizer.Add( p,0, wx.ALL|wx.EXPAND )
        
    def onhareketidgetir(self,event):
        
        if event.GetSelection()==-1:            
            self.secilenhareketid=""
        else :
            #print 'else'
            self.secilenhareketid=self.cmb_hareket_adi.GetClientData(self.cmb_hareket_adi.GetSelection()).id
            #print self.secilenhareketid
    def onyakitidgetir(self,event):
        if self.yakit_adi_cmb.GetSelection()==-1:            
            self.secilenyakitid=""
        else :
            self.secilenyakitid=self.yakit_adi_cmb.GetClientData(self.yakit_adi_cmb.GetSelection()).id
    def ontankidgetir(self,event):
        if self.cmb_tank_adi.GetSelection()==-1:            
            self.secilentankid=""
        else :
            self.secilentankid=self.cmb_tank_adi.GetClientData(self.cmb_tank_adi.GetSelection()).id
    '''
    def onayidgetir(self,event):
        if self.cmb_ay_adi.GetSelection()==-1:            
            self.secilenayid=""
        else :
            self.secilenayid=self.cmb_ay_adi.GetClientData(self.cmb_ay_adi.GetSelection()).id
            #print self.secilenayid
    '''
    def OnPrintPreview(self, event):
        self.print_headers
        '''
        headers       = ['KAYIT SAYISI',
                         'TANK ID',
                         'TANK ADI',
                         'YAKIT ADI',
                         'TARIH',
                         'YAKIT MIKTARI',
                         'SU MIKTARI',
                         'TANK DOLUM',
                         'POMPA SATIS',
                         'ACIKLAMA',
                         'DOLUM TIPI']
        '''
        headers       = ['%s,digit'%MESSAGES.no,
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tank,MESSAGES.no)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tank,MESSAGES.adi)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.yakit,MESSAGES.adi)),
                        '%s,datetime'%MESSAGES.tarih,
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.yakit,MESSAGES.miktar)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.su,MESSAGES.miktar)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tank,MESSAGES.dolum)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.pompa,MESSAGES.satis)),
                        '%s,string'%MESSAGES.aciklama,
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.dolum,MESSAGES.turu))]
        selects="ID_TANK_KAYITLARI,ID_YAKIT_TANKI,YAKIT_TANKI_ADI,YAKIT_ADI,TARIH,OKUNAN_YAKIT,OKUNAN_SU,TANK_DOLUMU,POMPA_SATISI,H_ACIKLAMA,DOLUM_TIPI"
        
        column_spaces = [0.6,0.6,0.8,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0]
        dataset = self.db.get_raw_data_all(table_name = "VIEW_TANK_HRKT_RAPOR",
                                 where=self.print_where_query,
                                 order="ID_TANK_KAYITLARI ASC",
                                 selects=selects)
                
        if dataset==[]:
            style.uyari_message(self,MESSAGES.bos_rapor,self.printTitle)
        else:
            
            PrintTablePreview( self.GrandParent, dataset,headers,column_spaces, self.printTitle )
    def OnRaporuGetir(self,event):
        self.Freeze()
        
        t =  wx.DateSpan(days=1) 
        bit_tar = self.bit_dpc.GetValue() + t
        bas_tar=self.bas_dpc.GetValue().Format('%Y-%m-%d')
        bit_tar=bit_tar.Format('%Y-%m-%d')
        
        where_query = " TARIH >= '%s' and TARIH <= '%s' " % (bas_tar,bit_tar)
        #print self.cmb_tank_adi.GetClientData(self.cmb_tank_adi.GetSelection()).id
        if self.cmb_tank_adi.GetSelection() !=-1 and self.cmb_tank_adi.Value!=MESSAGES.tumu:
            where_query += ' AND ID_YAKIT_TANKI = %s' % self.cmb_tank_adi.GetClientData(self.cmb_tank_adi.GetSelection()).id

        selected_thc_list = []
        for selected in self.cmb_hareket_adi.GetChecked():
            selected_thc_list.append( str(self.cmb_hareket_adi.GetClientData(selected).id))
        
        if selected_thc_list != []:
            joined_thc = '\',\''.join( selected_thc_list )
            where_query += ' AND ID_THC in (\'%s\')' % joined_thc
        '''        
        if self.cmb_ay_adi.Value==MESSAGES.tumu:
            where_query=" TARIH BETWEEN '%s' and '%s' " % (bas_tar,bit_tar)
        else:
            if self.cmb_ay_adi.GetSelection() !=-1:
                where_query += " AND TARIH like '*-%s-*' " % self.cmb_ay_adi.GetClientData(self.cmb_ay_adi.GetSelection()).id
        '''   
   
        #headers         = ['KAYIT_SAYISI,digit','TANK ID,digit','TANK ADI,string','YAKIT ADI,string','TARIH,datetime','YAKIT MIKTARI,digit','SU MIKTARI,string','TANK DOLUM,digit','POMPA SATIS,digit','ACIKLAMA,string']
        headers       = ['%s,digit'%MESSAGES.no,
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tank,MESSAGES.no)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tank,MESSAGES.adi)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.yakit,MESSAGES.adi)),
                        '%s,datetime'%MESSAGES.tarih,
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.yakit,MESSAGES.miktar)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.su,MESSAGES.miktar)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tank,MESSAGES.dolum)),
                        '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.pompa,MESSAGES.satis)),
                        '%s,string'%MESSAGES.aciklama,
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.dolum,MESSAGES.turu))]
        selects="ID_TANK_KAYITLARI,ID_YAKIT_TANKI,YAKIT_TANKI_ADI,YAKIT_ADI,TARIH,OKUNAN_YAKIT,OKUNAN_SU,TANK_DOLUMU,POMPA_SATISI,H_ACIKLAMA"
        
        self.print_select=selects
        self.print_where_query=where_query
        self.print_headers=headers
        #burdan sonras� generic. copy paste
        if self.rapor_panel_id != 0:
            wx.FindWindowById( self.rapor_panel_id ).Destroy()
        
        PaginatingIndexPanel( self,"VIEW_TANK_HRKT_RAPOR",
                                    selects = selects,
                                    where=where_query,
                                    order = "ID_TANK_KAYITLARI ASC",
                                    headers = headers,
                                    parent_sizer = self.boxsizer)#bu framein mainsizeri-- boxsizer olmak zorunda
        self.btn_print_preview.Enable()
        self.Layout()
        self.Thaw()
        
class CreateSatisBilgi(wx.Panel ):
    def __init__( self, parent):
        
        wx.Panel.__init__(self, parent ,1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        self.db           = param.get_root( self ).DB 
        self.boxsizer   = wx.BoxSizer( wx.VERTICAL )
        self.SetSizer( self.boxsizer )
        self.rapor_panel_id = 0
        self.Freeze()
        self.createAboutSatisBilgi()
        style.setPanelListBg( self )
        self.printTitle=MESSAGES.sablon_uc_kelime%( MESSAGES.detayli, MESSAGES.satis, MESSAGES.bilgisi)
        self.Thaw()
    
    def createAboutSatisBilgi(self ):

        self.p = wx.Panel(self,-1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        p=self.p
        p.SetSize((1300,100))
        p.PAINT_GRADIENTS = True
        panelStyle = (p.PAINT_GRADIENTS and [wx.BORDER_NONE] or [0])[0]
        p.style = panelStyle
        
        sizer        = wx.FlexGridSizer( 1,19,5,1)
        p.startColour = wx.Colour( 140,140,140 )
        #self.startColour =  wx.RED 
        p.endColour   = wx.WHITE
        
        yakit_adi_parametric_objects = []
        yakit_adi_parametric_objects = self.db.get_parameters_all("YAKIT",order="ID_YAKIT",selects="ID_YAKIT,YAKIT_ADI",where="YAKIT_DURUM=1")
        
        yakit_adi_lbl    = style.TransparentText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.yakit,MESSAGES.adi))
        self.yakit_adi_cmb    = wx.ComboBox(p,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.yakit_adi_cmb.Bind(wx.EVT_COMBOBOX,self.onyakitidgetir)
        style.setStaticTextList( yakit_adi_lbl)
        if yakit_adi_parametric_objects != []:
            a=dataObj()
            a.id=-1
            a.ad=MESSAGES.tumu
            yakit_adi_parametric_objects.append(a) 
            self.db.widgetMaker( self.yakit_adi_cmb , yakit_adi_parametric_objects)
            
        personel_adi_parametric_objects = []
        personel_adi_parametric_objects = self.db.get_parameters_all("CARI_HESAPLAR",order="ID_CARI_HESAP",selects="ID_CARI_HESAP,CARI_ADI",where="ID_CARI_TIPI=1")
        
        lbl_personel_adi    = style.TransparentText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.personel,MESSAGES.adi))
        self.cmb_personel_adi    = wx.ComboBox(p,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.cmb_personel_adi.Bind(wx.EVT_COMBOBOX,self.onpersonelidgetir)
        style.setStaticTextList( lbl_personel_adi)
        if personel_adi_parametric_objects != []:
            a=dataObj()
            a.id=-1
            a.ad=MESSAGES.tumu
            personel_adi_parametric_objects.append(a) 
            self.db.widgetMaker( self.cmb_personel_adi , personel_adi_parametric_objects)

        tank_adi_parametric_objects = []
        tank_adi_parametric_objects = self.db.get_parameters_all("YAKIT_TANKI",order="ID_YAKIT_TANKI",selects="ID_YAKIT_TANKI,YAKIT_TANKI_ADI",where="ID_ACTIVE=1")
        lbl_tank_adi    = style.TransparentText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.tank,MESSAGES.adi))
        self.cmb_tank_adi    = wx.ComboBox(p,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.cmb_tank_adi.Bind(wx.EVT_COMBOBOX,self.ontankidgetir)
        self.db.widgetMaker( self.cmb_tank_adi , tank_adi_parametric_objects)
        style.setStaticTextList(lbl_tank_adi)
        if tank_adi_parametric_objects != []:
            a=dataObj()
            a.id=-1
            a.ad=MESSAGES.tumu
            tank_adi_parametric_objects.append(a) 
            self.db.widgetMaker( self.cmb_tank_adi , tank_adi_parametric_objects)
            
        
        
        lbl_bas_dpc = style.TransparentText(p,label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.baslangic,MESSAGES.tarihi))
        style.setStaticTextList(lbl_bas_dpc)
        self.bas_dpc = wx.DatePickerCtrl(p, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )
        
        lbl_bit_dpc =style.TransparentText(p,label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.bitis,MESSAGES.tarihi))
        style.setStaticTextList(lbl_bit_dpc)
        self.bit_dpc = wx.DatePickerCtrl(p, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )
        
        
        self.btn_search_pressed  =  wx.Image("%sdb_search_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sdb_search_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_search  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%( MESSAGES.satis,MESSAGES.raporu))
        self.btn_search.Bind(wx.EVT_BUTTON, self.OnRaporuGetir)
        self.btn_search.SetToolTipString(MESSAGES.sablon_iki_kelime%( MESSAGES.satis,MESSAGES.raporu))
        self.btn_search.SetBitmapSelected(self.btn_search_pressed)
        style.setButtonFont( self.btn_search)
        
        self.btn_print_preview_pressed  =  wx.Image("%sdb_search_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sdb_search_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_print_preview  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%( MESSAGES.yazdir,MESSAGES.onizleme))
        self.btn_print_preview.Bind(wx.EVT_BUTTON, self.OnPrintPreview)
        self.btn_print_preview.SetToolTipString(MESSAGES.sablon_iki_kelime%( MESSAGES.yazdir,MESSAGES.onizleme))
        self.btn_print_preview.SetBitmapSelected(self.btn_print_preview_pressed)
        style.setButtonFont( self.btn_print_preview)
        self.btn_print_preview.Enable(False)        
        
        sizer.AddMany( [((10,20)),
                        (lbl_tank_adi ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.cmb_tank_adi,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((10,20)),
                        (yakit_adi_lbl,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.yakit_adi_cmb,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((10,20)),
                        (lbl_personel_adi,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.cmb_personel_adi,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((10,20)),
                        (lbl_bas_dpc ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.bas_dpc,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((10,20)),
                        (lbl_bit_dpc,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.bit_dpc,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((10,20)),
                        (self.btn_search,0, wx.ALIGN_CENTER|wx.ALIGN_CENTER_VERTICAL ),
                        ((10,20)),
                        (self.btn_print_preview,0, wx.ALIGN_CENTER|wx.ALIGN_CENTER_VERTICAL ),
                       
                        ])

        
        
        p.SetSizerAndFit( sizer )
        style.BindEvents( p )
        self.boxsizer.Add( p,0, wx.ALL|wx.EXPAND )
        
    
    def onyakitidgetir(self,event):
        if self.yakit_adi_cmb.GetSelection()==-1:            
            self.secilenyakitid=""
        else :
            self.secilenyakitid=self.yakit_adi_cmb.GetClientData(self.yakit_adi_cmb.GetSelection()).id
    def onpersonelidgetir(self,event):
        if self.cmb_personel_adi.GetSelection()==-1:            
            self.secilenpersonelid=""
        else :
            self.secilenpersonelid=self.cmb_personel_adi.GetClientData(self.cmb_personel_adi.GetSelection()).id
    def ontankidgetir(self,event):
        if self.cmb_tank_adi.GetSelection()==-1:            
            self.secilentankid=""
        else :
            self.secilentankid=self.cmb_tank_adi.GetClientData(self.cmb_tank_adi.GetSelection()).id
    def OnPrintPreview(self, event):
        self.print_headers
        headers       = ['%s,string'%MESSAGES.no,
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.pompa[0:3],MESSAGES.no)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.pompa[0:3],MESSAGES.adi)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.beyin[0:3],MESSAGES.no)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tabanca[0:3],MESSAGES.no)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.personel[0:3],MESSAGES.adi)),
                        '%s,string'%MESSAGES.tank,
                        '%s,string'%MESSAGES.yakit,
                        '%s,digit'%MESSAGES.litre,
                        '%s,currency'%(MESSAGES.sablon_iki_kelime%( MESSAGES.birim[0:1],MESSAGES.fiyat)),
                        '%s,currency'%MESSAGES.tutar,
                        '%s,datetime'%MESSAGES.tarih,
                        '%s,string'%MESSAGES.plaka,
                        '%s,string'%MESSAGES.odeme]
        column_spaces = [0.6,0.6,0.8,0.6,0.6,0.6,0.6,1.0,0.75,1.0,1.0,0.75,0.75,0.6]
        dataset = self.db.get_raw_data_all(table_name = "VIEW_POMPA_SATIS_RAPOR",
                                 where=self.print_where_query,
                                 order="ID_SATIS ASC",
                                 selects=self.print_select)
        if dataset==[]:
            style.uyari_message(self,MESSAGES.bos_rapor,self.printTitle)
        else:
            PrintTablePreview( self.GrandParent, dataset,headers,column_spaces, self.printTitle )
    def OnRaporuGetir(self,event):
        self.Freeze()
        
        t =  wx.DateSpan(days=1) 
        bit_tar = self.bit_dpc.GetValue() + t
        bas_tar=self.bas_dpc.GetValue().Format('%Y-%m-%d')
        bit_tar=bit_tar.Format('%Y-%m-%d')
        
        where_query = " TARIH >= '%s' and TARIH <= '%s' " % (bas_tar,bit_tar)
        #print self.cmb_tank_adi.GetClientData(self.cmb_tank_adi.GetSelection()).id
        if self.yakit_adi_cmb.GetSelection() !=-1 and self.yakit_adi_cmb.Value!=MESSAGES.tumu:
            where_query += ' AND ID_YAKIT = %s' %self.yakit_adi_cmb.GetClientData(self.yakit_adi_cmb.GetSelection()).id
            #print 'yakit girdi'    
        if self.cmb_personel_adi.GetSelection() !=-1 and self.cmb_personel_adi.Value!=MESSAGES.tumu:
            where_query += ' AND ID_CARI_HESAP = %s' %self.cmb_personel_adi.GetClientData(self.cmb_personel_adi.GetSelection()).id
            #print 'personel girdi'
        if self.cmb_tank_adi.GetSelection() !=-1 and self.cmb_tank_adi.Value!=MESSAGES.tumu:
            where_query += ' AND ID_YAKIT_TANKI = %s' %self.cmb_tank_adi.GetClientData(self.cmb_tank_adi.GetSelection()).id
            #print 'tank girdi'
        
        #print where_query
        
        selects="ID_SATIS,POMPA_NO,POMPA_ADI,BEYIN_NO,TABANCA_NO,CARI_ADI,YAKIT_TANKI_ADI,YAKIT_ADI,SATIS_LITRE,BIRIM_FIYAT,TUTAR,TARIH,PLAKA,ODEME_SEKLI"

        #headers         = ['SATIS NO,digit','POMPA_NO NO,digit','POMPA_ADI,string','BEYIN NO,digit','TABANCA NO,digit','PERSONEL ADI,string','YAKIT TANKI,string','YAKIT ADI,string','LITRE,digit','BIRIM FIYAT,digit','TUTAR,digit','TARIH,datetime','PLAKA,string','ODEME TURU,string']
        headers       = ['%s,string'%MESSAGES.no,
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.pompa[0:3],MESSAGES.no)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.pompa[0:3],MESSAGES.adi)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.beyin[0:3],MESSAGES.no)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tabanca[0:3],MESSAGES.no)),
                        '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.personel[0:3],MESSAGES.adi)),
                        '%s,string'%MESSAGES.tank,
                        '%s,string'%MESSAGES.yakit,
                        '%s,digit'%MESSAGES.litre,
                        '%s,currency'%(MESSAGES.sablon_iki_kelime%( MESSAGES.birim[0:1],MESSAGES.fiyat)),
                        '%s,currency'%MESSAGES.tutar,
                        '%s,datetime'%MESSAGES.tarih,
                        '%s,string'%MESSAGES.plaka,
                        '%s,string'%MESSAGES.odeme]
        if self.rapor_panel_id != 0:
            wx.FindWindowById( self.rapor_panel_id ).Destroy()
        self.print_select=selects
        self.print_where_query=where_query
        self.print_headers=headers
        
        PaginatingIndexPanel( self,"VIEW_POMPA_SATIS_RAPOR",
                                    selects = selects,
                                    where=where_query,
                                    order = "ID_SATIS ASC",
                                    headers = headers,
                                    parent_sizer = self.boxsizer,totalcoloum=" sum(SATIS_LITRE) as '%s',sum(TUTAR) as '%s'"%( MESSAGES.sablon_iki_kelime%( MESSAGES.toplam, MESSAGES.litre),MESSAGES.sablon_iki_kelime%(MESSAGES.toplam,MESSAGES.tutar)))#bu framein mainsizeri-- boxsizer olmak zorunda
        
        self.btn_print_preview.Enable()
        self.Layout()
        self.Thaw()
    
