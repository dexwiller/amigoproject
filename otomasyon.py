#!/usr/bin/env python
# -*- coding: latin5 -*-

import wx
import wx.animate
import os
import sys
import time
import wx.lib.agw.gradientbutton as GB
import wx.grid as gridlib
import logging
import style as s
from printPage import Printer
from printPage import HtmlPrinter
import sabitler
import util
from win32api import GetSystemMetrics
import model
import Queue
from iletisim import PPU
param = model.Parameters()
from getGridDataList import TreeListPanel, IndexListPanel, createSonSatislar
from process_manager import KThread
import wx.lib.buttons as LibButtons
import wx.lib.agw.aui as aui
from chart import bar_chart
from util_db import PompaRequestFormatla, ProbeOperations
from local import MESSAGES
from util import getTankYuzdeHesabi
button_size = sabitler.ana_button_size
image_path  = sabitler.image_path

id_otomasyon_ekrani = sabitler.EKRAN_ID_MAP.OTOMASYON_BUTONLAR.id_otomasyon_ekrani
id_tank_otomasyonu  = sabitler.EKRAN_ID_MAP.OTOMASYON_BUTONLAR.id_tank_otomasyonu
id_birim_fiyatlar   = sabitler.EKRAN_ID_MAP.OTOMASYON_BUTONLAR.id_birim_fiyatlar
id_pompa_ekdeks     = sabitler.EKRAN_ID_MAP.OTOMASYON_BUTONLAR.id_pompa_ekdeks
id_tasit_tanima     = sabitler.EKRAN_ID_MAP.OTOMASYON_BUTONLAR.id_tasit_tanima
id_santiye          = sabitler.EKRAN_ID_MAP.OTOMASYON_BUTONLAR.id_santiye

class win_Otomasyon(wx.Frame):
    def __init__(self, parent, id_, title):
        
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR | wx.MAXIMIZE))
        
        self.O  = param.get_root( self ).O 
        self.thread_params = param.get_root( self ).thread_params
        self.GValuesDict = None
        self.SourceDataBase = None
        self.RS = None
        self.previousGValuesDict = None
        self.oncekiDeger = False
        #font1 = wx.Font(9, wx.SWISS, wx.NORMAL, wx.BOLD, False, u'Verdana')
        #self.CreateStatusBar() 
        #filemenu= wx.Menu()
        #aboutItem = filemenu.Append(wx.ID_ABOUT, "&Hakk�nda"," Program Hakk�nda Bilgi")
        #self.Bind(wx.EVT_MENU, self.OnAbout, aboutItem)
        #filemenu.AppendSeparator()
        #exitItem = filemenu.Append(wx.ID_EXIT,"&��k��"," Program� Kapat")
        #self.Bind(wx.EVT_MENU, self.OnExit, exitItem)
        
        #item = wx.MenuItem(filemenu, MENU_HIDE_X, "Hide X Button", "Hide X Button", wx.ITEM_CHECK)
        
        self.Bind(wx.EVT_CLOSE, self.OnCloseWindow)

        #menuBar = wx.MenuBar()
        #menuBar.Append(filemenu,"&Dosya")
        #self.SetMenuBar(menuBar)
        #self.CreateRightClickMenu()
        self.LayoutItems()
        self.Centre()
        #icon1           = wx.Icon("buski.ico", wx.BITMAP_TYPE_ICO)
        #self.SetIcon(icon1)
        
        self.Show(True)
    
    def LayoutItems(self):
        mainSizer = wx.GridBagSizer(0,0)
        self.Freeze()
        self.SetSizer(mainSizer)
        winSize =  self.GetSize()
        self.top_panel = wx.Panel(self, wx.ID_ANY,size=(winSize[0],540))
        self.top_panel.SetBackgroundColour( wx.WHITE)
        self.top_sizer = wx.GridBagSizer(1,1)
        self.top_panel.SetSizer( self.top_sizer )
        
        self.otomasyon_ekrani_pressed  =  wx.Image("%sotomasyon_ekrani_accept_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_otomasyon_ekrani = wx.Image(MESSAGES.otomasyon_ekrani_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_otomasyon_ekrani  = wx.BitmapButton(self.top_panel, id=id_otomasyon_ekrani, bitmap=image_otomasyon_ekrani,pos=(0,0),
        size = (image_otomasyon_ekrani.GetWidth()+5, image_otomasyon_ekrani.GetHeight()+5))
        self.btn_otomasyon_ekrani.Bind(wx.EVT_BUTTON, self.OnOtomasyonMenuPressed)
        self.btn_otomasyon_ekrani.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.pompa, MESSAGES.otomasyonu ))
        self.btn_otomasyon_ekrani.SetBitmapSelected(self.otomasyon_ekrani_pressed)
        self.top_sizer.Add(self.btn_otomasyon_ekrani, (0,0),(1,1),wx.ALL)
        if not self.thread_params.POMPA_AKTIF:
            self.btn_otomasyon_ekrani.Disable()
        
        self.otomasyon_tank_pressed  =  wx.Image("%sotomasyon_tank_accept_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_otomasyon_tank = wx.Image(MESSAGES.otomasyon_tank_ekrani_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_otomasyon_tank  = wx.BitmapButton(self.top_panel, id=id_tank_otomasyonu, bitmap=image_otomasyon_tank,pos=(0,1),
        size = (image_otomasyon_tank.GetWidth()+5, image_otomasyon_tank.GetHeight()+5))
        self.btn_otomasyon_tank.Bind(wx.EVT_BUTTON, self.OnOtomasyonMenuPressed)
        self.btn_otomasyon_tank.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.tank, MESSAGES.otomasyonu ))
        self.btn_otomasyon_tank.SetBitmapSelected(self.otomasyon_tank_pressed)
        self.top_sizer.Add(self.btn_otomasyon_tank,  (0,1),(1,1), wx.ALL)
        self.btn_otomasyon_tank.Disable()
        if  self.thread_params.TANK_AKTIF or self.thread_params.TANK_PROBESUZ_AKTIF:
            self.btn_otomasyon_tank.Enable()
        self.otomasyon_br_fiyat_pressed  =  wx.Image("%sbirim_fiyat_accept_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_otomasyon_br = wx.Image(MESSAGES.birim_fiyat_ekrani_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_otomasyon_br  = wx.BitmapButton(self.top_panel, id=id_birim_fiyatlar, bitmap=image_otomasyon_br,pos=(0,1),
        size = (image_otomasyon_br.GetWidth()+5, image_otomasyon_br.GetHeight()+5))
        self.btn_otomasyon_br.Bind(wx.EVT_BUTTON, self.OnOtomasyonMenuPressed)
        self.btn_otomasyon_br.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.birim, MESSAGES.fiyatlar ))
        self.btn_otomasyon_br.SetBitmapSelected(self.otomasyon_br_fiyat_pressed)
        self.top_sizer.Add(self.btn_otomasyon_br,  (1,1),(1,1), wx.ALL)
        if not self.thread_params.POMPA_AKTIF:
            self.btn_otomasyon_br.Disable()
        
        
        self.otomasyon_pompa_endex_pressed  =  wx.Image("%spompa_endeks_accept_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_otomasyon_pompa_ekdex = wx.Image(MESSAGES.pompa_endeks_ekrani_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_otomasyon_pompa_endex  = wx.BitmapButton(self.top_panel, id=id_pompa_ekdeks, bitmap=image_otomasyon_pompa_ekdex,pos=(0,1),
        size = (image_otomasyon_pompa_ekdex.GetWidth()+5, image_otomasyon_pompa_ekdex.GetHeight()+5))
        self.btn_otomasyon_pompa_endex.Bind(wx.EVT_BUTTON, self.OnOtomasyonMenuPressed)
        self.btn_otomasyon_pompa_endex.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.pompa, MESSAGES.endeksleri ))
        self.btn_otomasyon_pompa_endex.SetBitmapSelected(self.otomasyon_pompa_endex_pressed)
        self.top_sizer.Add(self.btn_otomasyon_pompa_endex,  (1,0),(1,1), wx.ALL)
        if not self.thread_params.POMPA_AKTIF:
            self.btn_otomasyon_pompa_endex.Disable()
        '''
        self.otomasyon_tasit_tanima_pressed  =  wx.Image("%stasit_tanima_accept_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_otomasyon_tasit_tanima = wx.Image("%stasit_tanima_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_otomasyon_tasit_tanima  = wx.BitmapButton(self.top_panel, id=id_tasit_tanima, bitmap=image_otomasyon_tasit_tanima,pos=(0,1),
        size = (image_otomasyon_tasit_tanima.GetWidth()+5, image_otomasyon_tasit_tanima.GetHeight()+5))
        self.btn_otomasyon_tasit_tanima.Bind(wx.EVT_BUTTON, self.OnOtomasyonMenuPressed)
        self.btn_otomasyon_tasit_tanima.SetToolTipString("Ta��t Tan�ma")
        self.btn_otomasyon_tasit_tanima.SetBitmapSelected(self.otomasyon_tasit_tanima_pressed)
        self.top_sizer.Add(self.btn_otomasyon_tasit_tanima,  (1,1),(1,1), wx.ALL)
        
        self.otomasyon_santiye_pressed  =  wx.Image("%ssantiye_accept_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_otomasyon_santiye = wx.Image("%ssantiye_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_otomasyon_santiye  = wx.BitmapButton(self.top_panel, id=id_santiye, bitmap=image_otomasyon_santiye,pos=(0,1),
        size = (image_otomasyon_santiye.GetWidth()+5, image_otomasyon_santiye.GetHeight()+5))
        self.btn_otomasyon_santiye.Bind(wx.EVT_BUTTON, self.OnOtomasyonMenuPressed)
        self.btn_otomasyon_santiye.SetToolTipString("�antiye")
        self.btn_otomasyon_santiye.SetBitmapSelected(self.otomasyon_santiye_pressed)
        self.top_sizer.Add(self.btn_otomasyon_santiye,  (1,2),(1,1), wx.ALL)
        '''
       
        self.middle_panel =  wx.Panel(self, wx.ID_ANY ,size=(winSize[0],winSize[1]-140))
        middle_sizer=wx.FlexGridSizer(cols=1,hgap=10,vgap=10)
        self.middle_panel.SetSizer(middle_sizer)
        self.middle_panel.SetBackgroundColour( wx.Colour(254,254,254))
        middle_panel_image=wx.Image(MESSAGES.yazilim_image_%image_path,wx.BITMAP_TYPE_ANY)
        sb1 = wx.StaticBitmap(self.middle_panel, -1, wx.BitmapFromImage(middle_panel_image))
        
        middle_sizer.Add(sb1,1,wx.ALL|wx.CENTER)        
        mainSizer.Add( self.top_panel,(0,0),(1,1),wx.ALL)
        mainSizer.Add( self.middle_panel,(1,0),(1,1),wx.ALL)
        self.Thaw()
    def OnAbout(self, event):
        self.SetTransparent(150)
        dlg = wx.MessageDialog( self, "Copyright � ", wx.OK)
        dlg.ShowModal() 
        dlg.Destroy()
        self.SetTransparent(255)
    def OnExit(self,e):
        #self.onay_message("��kmak �stedi�inizden Emin misiniz ?","Kapat")
        self.Show(False)
        
    def OnCloseWindow(self,e):
        for kthred in self.thread_list:
            kthred.kill()
        self.Destroy()
        
    def onSelect(self, event):
        """"""
        #print dir( event )
        #print "You selected: " + self.isin_adi_cmb.GetStringSelection()
        obj = self.isin_adi_cmb.GetClientData(self.isin_adi_cmb.GetSelection())
        text = """
        The object's attributes are:
        %s  %s
 
        """ % (obj.id, obj.ad)
        #print text
    def onay_message( self,message,win_name):
        self.SetTransparent(150)
        dlg = wx.MessageDialog(self, message,
                               win_name,
                               wx.OK  |wx.CANCEL | wx.ICON_INFORMATION
                               )
        
        retCode = dlg.ShowModal()
        if (retCode == wx.ID_OK):
            self.Show( False )
        dlg.Destroy()
        self.SetTransparent(255)
    
    def OnOtomasyonMenuPressed( self , event ):
        
        id_  = event.GetEventObject().GetId()
        fullscreen = False
        if id_ == id_otomasyon_ekrani:
            winClass   = win_Otomasyon_Ekrani
            title      = MESSAGES.sablon_iki_kelime%(MESSAGES.otomasyon, MESSAGES.ekrani )
            fullscreen = True
        elif id_ == id_tank_otomasyonu:
            winClass = win_Tank_Otomasyonu
            title    = MESSAGES.sablon_iki_kelime%(MESSAGES.tank, MESSAGES.otomasyonu )
            fullscreen = True
        elif id_ == id_birim_fiyatlar:
            winClass = win_Birim_Fiyat
            title    = MESSAGES.sablon_iki_kelime%(MESSAGES.birim, MESSAGES.fiyatlar )
        elif id_ == id_pompa_ekdeks:
            winClass = win_Pompa_Endeks
            title    = MESSAGES.sablon_iki_kelime%(MESSAGES.pompa, MESSAGES.endeksleri )
            fullscreen = True
        elif id_   == id_tasit_tanima:
            winClass = win_Tasit_Tanima
            title    = MESSAGES.sablon_iki_kelime%(MESSAGES.tasit, MESSAGES.tanima )
        elif id_ == id_santiye:
            winClass = win_Santiye
            title    = MESSAGES.santiye
        self.O.newWindow(self,event,winClass,title,maximize=fullscreen)
         
class win_Birim_Fiyat( wx.Frame ):
    def __init__(self, parent, id_, title):
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (   wx.DEFAULT_FRAME_STYLE|wx.FRAME_NO_TASKBAR ))
        self.root  = param.get_root( self )
        self.db    = self.root.DB
        self.O     = self.root.O 
        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.mainSizer)
        self.createLayout()
        self.SetSize((800,600))
        self.Centre()
    
    def createLayout(self):
        
        self.Freeze()
        #self.book = wx.Notebook(self, style=wx.NB_TOP )
        self.book  = aui.AuiNotebook(self,1,agwStyle=(aui.AUI_NB_TOP|aui.AUI_NB_SCROLL_BUTTONS))
        self.book.SetArtProvider(aui.AuiDefaultTabArt())
        s.setPanelListBg( self.book )
        self.p = self.createBirimFiyatGiris()
        self.book.Bind(aui.EVT_AUINOTEBOOK_PAGE_CHANGED  , self.OnBookPageChanged )
        self.p2 = self.O.createNullPanel(self.book)
        self.book.AddPage( self.p,MESSAGES.sablon_uc_kelime%(MESSAGES.birim, MESSAGES.fiyat, MESSAGES.guncelle),True )
        self.book.AddPage( self.p2,MESSAGES.sablon_uc_kelime%(MESSAGES.birim, MESSAGES.fiyat, MESSAGES.listesi))
        
        il = wx.ImageList(64, 64)
        self.img0 = il.Add(wx.Bitmap('%sbirim_fiyat_guncelleme.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img1 = il.Add(wx.Bitmap('%sbirim_fiyat_list.png'%image_path, wx.BITMAP_TYPE_ANY))
  
        self.book.AssignImageList(il)
        self.book.SetPageImage(0, self.img0)
        self.book.SetPageImage(1, self.img1)
        self.mainSizer.Add(self.book, 1,wx.ALL | wx.EXPAND)
        self.Layout()
        self.Fit()
        
        if self.GetSize().height > GetSystemMetrics(1) - 200:
            self.SetSize( (self.GetSize().width, GetSystemMetrics(1) - 200))
        
        if self.GetSize().width > GetSystemMetrics(0) - 200:
            self.SetSize( (GetSystemMetrics(0) - 200,self.GetSize().height )) 
        
        self.Thaw()
    def OnBookPageChanged( self, event ):
        if self.book.GetSelection() == 1:
            self.Freeze()
            p = self.createBirimFiyatListe()
            self.book.SetSelection(0)
            self.book.RemovePage( 1 )
            self.book.AddPage( p,MESSAGES.sablon_uc_kelime%(MESSAGES.birim, MESSAGES.fiyat, MESSAGES.listesi))
            self.book.SetPageImage(1, self.img1)
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED )
            self.book.SetSelection(1)
            self.Layout()
            self.SetSize((800,600))
            self.Thaw()

    def createBirimFiyatGiris ( self ):
        
        self.p = wx.Panel(self.book,-1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        # wx.Colour(130,208,190)
        #p.SetBackgroundColour(  frame_color )
        gbs = self.gbs = wx.GridBagSizer(5, 5)
        
        #get_raw_data_all(self, table_name, where="", order="",selects=""):
        self.data_all       = self.db.get_raw_data_all(table_name = 'YAKIT', where="YAKIT_DURUM=1",order="ID_YAKIT",selects="ID_YAKIT,YAKIT_ADI,YAKIT_BIRIM_FIYAT" )
        #lbl_yakit_kodu       =  wx.StaticText(self.p, label=" Yak�t Kodu ",style=wx.ALIGN_CENTER)
        lbl_yakit_adi        =  wx.StaticText(self.p, label=MESSAGES.sablon_iki_kelime%( MESSAGES.yakit, MESSAGES.adi),style=wx.ALIGN_CENTER)
        lbl_eski_birim_fiyat =  wx.StaticText(self.p, label=MESSAGES.sablon_uc_kelime%( MESSAGES.guncel, MESSAGES.birim, MESSAGES.fiyat),style=wx.ALIGN_CENTER)
        lbl_yeni_birim_fiyat =  wx.StaticText(self.p, label=MESSAGES.sablon_uc_kelime%( MESSAGES.yeni, MESSAGES.birim, MESSAGES.fiyat),style=wx.ALIGN_CENTER)
        
        #s.setStaticTextHeader( lbl_yakit_kodu )
        s.setStaticTextHeader( lbl_yakit_adi )
        s.setStaticTextHeader( lbl_eski_birim_fiyat )
        s.setStaticTextHeader( lbl_yeni_birim_fiyat )
        
        #gbs.Add( lbl_yakit_kodu, (1,1),(1,1),  wx.ALL | wx.EXPAND,3)
        gbs.Add( lbl_yakit_adi, (1,1) ,(1,1),  wx.ALL | wx.EXPAND,3)
        gbs.Add( lbl_eski_birim_fiyat, (1,2) ,(1,1), wx.ALL | wx.EXPAND,3)
        gbs.Add( lbl_yeni_birim_fiyat, (1,3) ,(1,3),  wx.ALL ,3)
        
        line_num = 2
        self.widgetDict     = {}
        self.EskiFiyatDict  = {}
        for data in self.data_all:
            
            #lbl_data_yakit_kodu             =   wx.StaticText(p, label=str(data[0]).decode('Latin5'),style=wx.ALIGN_CENTER)
            #s.setStaticTextList( lbl_data_yakit_kodu )
            lbl_data_yakit_adi              =   wx.StaticText(self.p, label=str(data[1]),style=wx.ALIGN_LEFT)
            s.setStaticTextList( lbl_data_yakit_adi )
            lbl_data_yakit_eski_birim_fiyat =   wx.StaticText(self.p, label=str(util.data_formatla(data[2],'currency')).decode('Latin5') + ' ' + MESSAGES.para_birimi,style=wx.ALIGN_CENTER)
            s.setStaticTextList( lbl_data_yakit_eski_birim_fiyat )
            #txt_yeni_birim_fiyat            =   wx.TextCtrl  (self.p, -1, "",size=(180,-1))
            txt_yeni_birim_fiyat            = s.CheckNumberMasked(self.p, 'br_' , 1,2 )
            self.widgetDict   [ data[0] ]      = txt_yeni_birim_fiyat
            self.EskiFiyatDict[ data[0] ]      = data[2]
            
            #txt_yeni_birim_fiyat.Bind( wx.EVT_CHAR, s.CheckNumber )
            line_                           =   wx.StaticLine(self.p, -1, style=wx.LI_HORIZONTAL)
            lbl_tl                          =   wx.StaticText(self.p, label=MESSAGES.para_birimi)
            s.setStaticTextList( lbl_tl )
            
            #gbs.Add( lbl_data_yakit_kodu,             (line_num ,1 ),(1,1),wx.ALL | wx.EXPAND, 3 )
            gbs.Add( lbl_data_yakit_adi,              (line_num ,1 ),(1,1),wx.ALL | wx.EXPAND, 1 )
            gbs.Add( lbl_data_yakit_eski_birim_fiyat, (line_num ,2 ),(1,1),wx.ALL | wx.EXPAND, 1 )
            gbs.Add( txt_yeni_birim_fiyat,            (line_num ,4 ),(1,1),wx.ALL , 1 )
            gbs.Add( lbl_tl,                          (line_num ,5 ),(1,1),wx.ALL | wx.ALIGN_CENTER , 1 )
            
            line_num += 1
            gbs.Add( line_,            (line_num,0 ),(1,6),flag=wx.EXPAND|wx.BOTTOM, border=10)
            line_num += 1
        
        self.btn_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        #bitmap_save = wx.Bitmap( "%ssave.png"%image_path, wx.BITMAP_TYPE_ANY )
        
        self.btn_save  = LibButtons.ThemedGenBitmapTextButton(self.p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.fiyatlari_guncelle)
        #self.btn_save.SetBitmapLabel("Fiyatlar� Pompaya G�nder")
        
        self.btn_save.Bind(wx.EVT_BUTTON, self.OnBirimFiyatSave)
        self.btn_save.SetToolTipString(MESSAGES.fiyatlari_guncelle)
        self.btn_save.SetBitmapSelected(self.btn_save_pressed)
        s.setButtonFont( self.btn_save )
        gbs.Add(self.btn_save,  (line_num,1),(1,2))
        
        s.setPanelListBg( self.p )
        self.p.SetSizerAndFit(gbs)
        self.p.SetAutoLayout(True)
        
        return self.p
    
    def OnBirimFiyatSave( self, event ):
        dons = []
        for k,v in self.widgetDict.iteritems():
            if v.GetValue().strip() != '.':
                tabanca_object_list = self.db.getPumpOptionsByYakitID( k )
                #yakit_idlelerine gore pump nozzle number bul.
                #bunlar�n degerini set et, olumluysa veritaban�n� g�ncelle
                '''
                if not sabitler.simulation:
                    PPU().update_ppu( self.root.com_q, tabanca_object_list, v.GetValue().strip())
                '''
                history = self.setBirimFiyatHistory(k,v.GetValue().strip())
                if history != 0:
                    dons.append (self.db.update_data('YAKIT','YAKIT_BIRIM_FIYAT',v.GetValue().strip(),'ID_YAKIT=%s'%k))
                else:
                    dons.append(0)
        if dons.count( 0 ) > 0:
            s.uyari_message(self,MESSAGES.sablon_uc_kelime%( MESSAGES.birim, MESSAGES.fiyatlar, MESSAGES.hata),MESSAGES.fiyatlari_guncelle)
        else:
            s.ok_message(self,MESSAGES.sablon_uc_kelime%( MESSAGES.birim, MESSAGES.fiyatlar, MESSAGES.kaydedildi),MESSAGES.fiyatlari_guncelle)
            #s.ok_info_bar(self,self.mainSizer,'Veriler Kaydedildi')
        refresh_dict     = dict(book            = self.book,
                                temp_page           = 1,
                                create_panel_method = self.createBirimFiyatGiris,
                                cur_page            = 0,
                                label               = MESSAGES.fiyatlari_guncelle,
                                image               = self.img0)
        s.updateWindow(**refresh_dict)
        #s.updateWindow(self.book, self.createLayout)
        self.SetSize((800,600) )
        
    def setBirimFiyatHistory(self, yakit_id, yeni_fiyat ):
        
        str_cols = "ID_YAKIT,ESKI_FIYAT,YENI_FIYAT,TARIH"
        str_vals = str(yakit_id)+','+ str(self.EskiFiyatDict[yakit_id])+','+ str(yeni_fiyat) + ",datetime('now', 'localtime')"
        don = self.db.save_data( 'YAKIT_HISTORY',str_cols,str_vals )
        return don
    
    def createBirimFiyatListe ( self ):
        
        #headers          = ['Yak�t Ad�,string','G�ncel Fiyat�,currency','De�i�iklik Tarihi,datetime','Eski Fiyat�,currency','Yeni Fiyat�,currency']
        child_parameters = ['YAKIT_HISTORY','ID_YAKIT','ID_YAKIT,ID_YAKIT_HISTORY,ESKI_FIYAT,YENI_FIYAT,TARIH']
        headers_model    = ['%s,string'%MESSAGES.sablon_iki_kelime%( MESSAGES.yakit, MESSAGES.adi),
                            '%s,currency'%MESSAGES.sablon_uc_kelime%( MESSAGES.onceki, MESSAGES.birim, MESSAGES.fiyat),
                            '%s,currency'%MESSAGES.sablon_uc_kelime%( MESSAGES.yeni, MESSAGES.birim, MESSAGES.fiyat),
                            '%s,datetime'%MESSAGES.tarih]
        
        parent_childs  = self.db.get_raw_data_all(table_name = 'YAKIT',order="ID_YAKIT",selects="YAKIT_ADI,ID_YAKIT",where="YAKIT_DURUM=1",dict_mi='parent',child_parameters =child_parameters,data_types=map(lambda x:x.split(',')[1], headers_model) )
        mod            = model.TreeListModel(parent_childs,headers_model,child_parameters)
        tree_list_pane = TreeListPanel( self.book, mod )
        return tree_list_pane
        #data_history   = db.get_raw_data_all(table_name = 'VIEW_YAKIT_HISTORY',order="ID_YAKIT,TARIH",selects="YAKIT_ADI,YAKIT_BIRIM_FIYAT,TARIH,ESKI_FIYAT,YENI_FIYAT" )
        
        #p = O.createStaticList( self.book, headers, data_history)
        #return p
        
class win_Otomasyon_Ekrani(wx.Frame ):
    def __init__(self, parent, id_, title):
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= ( wx.DEFAULT_FRAME_STYLE | wx.FRAME_NO_TASKBAR ))
        self.root = param.get_root( self )
        self.db = self.root.DB
        self.O  = self.root.O
        self.root  = param.get_root( self ) 
        self.yatay_mod = sabitler.pompa_yatay_mod
        self.scroll = wx.ScrolledWindow(self, 1, style=(wx.SIMPLE_BORDER|wx.FULL_REPAINT_ON_RESIZE))
        self.scroll.SetScrollbars(1,1,600,400)
        self.ultramainSizer = wx.BoxSizer( wx.VERTICAL )
        self.mainSizer = wx.GridBagSizer(3,3)
        self.scroll.SetSizer( self.ultramainSizer )
        self.createLayout()
        
        
        s.setPanelListBg( self )
    
    def createLayout( self ):
        self.Freeze() 
        q = self.root.pompaQueueContainer 
        beyin_sayisi        = self.db.get_raw_data_all( table_name="VIEW_POMPA_TABANCALAR",selects = 'distinct( BEYIN_NO) , ARABIRIM_ADRESI ,POMPA_ADI, POMPA_NO' ,
                                              order="POMPA_NO",dict_mi="object")
        row_num = 0
        col_num = 0
        self.pompa_dict = {}
        self.aboutPanel = createAboutPompa( self.scroll )
        self.ultramainSizer.Add( self.aboutPanel,0,wx.EXPAND )    
        for beyin in beyin_sayisi:
            p = PompaOtomasyonPanel( self.scroll )
            pompa_no_l = util.tarih_sifir_ekle (str( beyin.POMPA_NO ))
            p.lbl_pompa_no.SetLabel( pompa_no_l)
            
            p.lbl_pompa_beyin.SetLabel( '%s/%s' % ( util.tarih_sifir_ekle (beyin.POMPA_ADI), util.tarih_sifir_ekle(beyin.BEYIN_NO)))
            #p.Enable( False )
            self.mainSizer.Add( p, (row_num, col_num ),(1,1),(wx.ALL|wx.EXPAND))
            
            
            self.pompa_dict[beyin.POMPA_NO] = p
            col_num +=1
            if col_num%self.yatay_mod == 0:
                col_num = 0
                row_num+=1
        
        self.son_satis_frame = createSonSatislar( self.scroll )
        
        self.ultramainSizer.Add( self.mainSizer,0,wx.ALIGN_TOP)
        self.ultramainSizer.Add( self.son_satis_frame,1,wx.EXPAND)
        
        
        self.Thaw()
        
        self.pompa_thread = KThread(target = self.PompaDegerleriniSetEt, kwargs={'q':q,
                                                                                 'p_dict':self.pompa_dict,
                                                                                 'son_satis_panel':self.son_satis_frame,
                                                                                 'son_satis_q':self.root.lastSaleRecordsQueueObject})
        self.pompa_thread.start()
        self.GetParent().thread_list.append( self.pompa_thread )
        self.Bind(wx.EVT_CLOSE, self.OnClose )

    def OnClose( self, event ):
        self.pompa_thread.kill()
        self.Destroy()
        
    def PompaDegerleriniSetEt( self, q, p_dict,son_satis_panel,son_satis_q):
        formatted_type  = {}
        pompa_formatter = PompaRequestFormatla()
        counter  = 0
        while 1:
            counter += 1
            formatted_type = q.get()
            q.put( formatted_type )
            #probe_degerleri = {}
            #pompa_props     = p_dict[1]
            #panel           = p_dict[0]
            
            #arabirimden gelen pompa numaras� ile bizdeki pompa no e�le�cek.
            
            #for tabanca_id,arabirim_donen in pompa_degerleri.iteritems():
            #formatted_type =  pompa_formatter.arabirim_donen_formatla( pompa_degerleri )  
            
            for k,v in formatted_type.iteritems():
                pompa_panel    =  p_dict[k]
                pompa_panel.Freeze()

                s_image_container = pompa_panel.static_image_container
                
                if not son_satis_q.empty():
                    last_record = son_satis_q.get()
                    son_satis_panel.Freeze()
                    if hasattr( last_record, 'payment_update' ):
                        pk_index = filter(lambda x:x[0] == last_record.id_, son_satis_panel.model.data)
                        if len( pk_index ) > 0:
                            inkes =  self.son_satis_frame.model.data.index( pk_index[0] )
                            son_satis_panel.model.SetValueByRow( last_record.payment_update, inkes, 6  )
                    else:
                        values   = [last_record.id_, 
                                   last_record.str_now,
                                   last_record.plate_number,
                                   last_record.filling_volume,
                                   last_record.unit_price,
                                   last_record.filling_amount,
                                   sabitler.ODEME_SEKILLERI.dict_values[ last_record.payment_method ]
                                   ]
                        
                        son_satis_panel.model.InsertRow(0, values )
    
                        if len( son_satis_panel.model.data ) >= 15:
                            son_satis_panel.model.DeleteRows( list(range(14, len(son_satis_panel.model.data))) )
                    son_satis_panel.Update()
                    son_satis_panel.Thaw()
                if v:
                    #print counter,k,v.status
                    '''
                    print counter ,' : **************************' 
                    for key, value in v.__dict__.iteritems():
                        print key , ' : ', value
                    print '**************************************'
                    counter += 1
                    '''
                    pompa_panel.SetBackgroundColour( v.color )
                    #print v.color
                    #print v.status
                    try:
                        pompa_panel.lbl_yakit_adi.SetLabel( v.yakit_adi)
                    except:
                        pass
                    try:
                        pompa_panel.lbl_plaka_txt.SetLabel( v.plate_number)
                    except:
                        pass
                    try:
                        pompa_panel.lbl_birim_fiyat_txt.SetLabel( str(v.unit_price) )
                        pompa_panel.lbl_verilen_litre_txt.SetLabel( str(v.filling_volume))
                        pompa_panel.lbl_tutar_txt.SetLabel( str(v.filling_amount) )
                        
                    except:
                        pass
                    
                    if v.status != 'idle' and v.status != 'error':
                        s_image_container.SetBitmap( pompa_panel.png_busy)
                    elif v.status == 'error':
                        s_image_container.SetBitmap( pompa_panel.png_error)
                        #pompa_panel.Enable( False )
                    else:
                        s_image_container.SetBitmap( pompa_panel.png_idle)
                    
                     
                       
                else:
                    pompa_panel.SetBackgroundColour( sabitler.hata_rengi )
                    s_image_container.SetBitmap( pompa_panel.png_error)
                pompa_panel.Update()
                pompa_panel.Thaw()
            time.sleep( sabitler.arabirimEkranBekleme ) 

class createAboutPompa( wx.Panel ):
    def __init__( self, parent ):
        self.PAINT_GRADIENTS = True
        panelStyle = (self.PAINT_GRADIENTS and [wx.BORDER_NONE] or [0])[0]
        wx.Panel.__init__(self, parent, style=wx.FULL_REPAINT_ON_RESIZE)
        self.sizer         = wx.GridSizer( 1,2,1,1)
        png_logo          = wx.Image("%slogo_gr_bg_64.png"%image_path,wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        png_dagitici_logo = wx.Image("%sdagitici_logo.png"%image_path,wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.static_image_container   = wx.StaticBitmap(self, 0, png_logo )
        self.static_image_container_d = wx.StaticBitmap(self, 0, png_dagitici_logo )
        dagitici_ = param.get_dagitici_adi( False )
        bayi_     = param.get_bayi_adi( False ) 
        
        #lbl_      = wx.StaticText(self,-1,'%s - %s' %(dagitici_,bayi_))
        self.lbl_  = [('%s' %(bayi_), 65, 20 )]
        #self.widget_list = [  lbl_]
        #s.setStaticTextPompaHeader( lbl_ )
       
        
        
        self.startColour = wx.Colour( 140,140,140 )
        #self.startColour =  wx.RED 
        self.endColour   = wx.WHITE
        #self.BindEvents ( static_image_container )
        s.BindEvents( self , self.lbl_ )
        
        self.sizer.AddMany( [(self.static_image_container_d ,0, wx.ALIGN_LEFT ),
                             (self.static_image_container ,0, wx.ALIGN_RIGHT )
                            #(lbl_  ,1 , wx.ALIGN_RIGHT|wx.ALIGN_BOTTOM)
                        ])
        
        self.SetSizer( self.sizer )
    
class PompaOtomasyonPanel( wx.Panel ):
    
    def __init__(self, parent ):
        wx.Panel.__init__(self, parent,style=wx.SIMPLE_BORDER)

        s.setPanelListBg( self )
        self.MBsizer  = wx.BoxSizer( wx.VERTICAL )
        self.topsizer = wx.GridSizer(1, 3, 5, 2)
        self.sizer   = wx.GridBagSizer(1,1)
        self.bottomsizer = wx.GridSizer(1, 2, 5, 2)
        self.root    = param.get_root( self ) 
        self.SetSizer( self.MBsizer )
        
        self.png_idle  = wx.Image(MESSAGES.pompa_idle2_%image_path,wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.png_error = wx.Image(MESSAGES.pompa_error2_%image_path,wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.png_busy  = wx.Image(MESSAGES.pompa_busy2_%image_path,wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.static_image_container = wx.StaticBitmap(self, -1, self.png_error )
        self.SetBackgroundColour( sabitler.hata_rengi )
        self.lbl_pompa_beyin              = wx.StaticText(self)
        self.lbl_yakit_adi                = wx.StaticText(self,style=wx.ALIGN_CENTRE)
        self.lbl_pompa_no                 = wx.StaticText(self)
        line_                         = wx.StaticLine(self, -1, style=wx.LI_HORIZONTAL)
        line_2                         = wx.StaticLine(self, -1, style=wx.LI_HORIZONTAL)
        s.setStaticTextPompaHeader     ( self.lbl_yakit_adi )
        s.setStaticTextPompaPanelNumber  ( self.lbl_pompa_beyin )
        s.setStaticTextPompaPanelNumber  ( self.lbl_pompa_no )
        
        self.lbl_birim_fiyat      = wx.StaticText(self,-1,'%s :'%MESSAGES.br_fiyat)
        self.lbl_birim_fiyat_txt  = wx.StaticText(self)
        s.setStaticTextPompaHeader( self.lbl_birim_fiyat )
        s.setStaticTextPompaPanelList  ( self.lbl_birim_fiyat_txt )
        
        self.lbl_verilen_litre     = wx.StaticText(self,-1,'%s     :'%MESSAGES.litre)
        self.lbl_verilen_litre_txt = wx.StaticText(self) 
        s.setStaticTextPompaHeader( self.lbl_verilen_litre )
        s.setStaticTextPompaPanelList  ( self.lbl_verilen_litre_txt )
        
        self.lbl_tutar_txt            = wx.StaticText(self)
        self.lbl_tutar                = wx.StaticText(self,-1,'%s    :'%MESSAGES.fiyat)
        s.setStaticTextPompaHeader( self.lbl_tutar )
        s.setStaticTextPompaPanelList( self.lbl_tutar_txt )
        
        self.lbl_plaka_txt            = wx.StaticText(self)
        self.lbl_plaka                = wx.StaticText(self,-1,'%s   :'%MESSAGES.plaka)
        s.setStaticTextPompaHeader( self.lbl_plaka )
        s.setStaticTextPompaPanelList( self.lbl_plaka_txt )
        
        yer_tutucu                    = '     '
        self.yer_tutucu               = wx.StaticText(self,-1,yer_tutucu)
        
        gridy = 0

        self.topsizer.AddMany( [(self.lbl_pompa_beyin ,0, wx.ALIGN_LEFT ),
                                (self.lbl_yakit_adi  ,0, wx.ALIGN_CENTER_VERTICAL),
                                (self.lbl_pompa_no  ,0 , wx.ALIGN_RIGHT)])
        
        self.MBsizer.Add( self.topsizer , 0, wx.EXPAND )
        
        self.sizer.Add ( line_ , (0,0),(1,25),flag=wx.EXPAND, border=10)
        
        self.sizer.Add( self.static_image_container ,(1,gridy),(4,3),5 )
        
        self.sizer.Add ( self.yer_tutucu, (2,gridy+3),(1,1),1)
        
        self.sizer.Add( self.lbl_tutar  ,(2,gridy+4),(1,1),10 )
        self.sizer.Add( self.lbl_tutar_txt  ,(2,gridy+5),(1,1),10 )
        
        self.sizer.Add( self.lbl_verilen_litre  ,(3,gridy+4),(1,1),10 )
        self.sizer.Add( self.lbl_verilen_litre_txt  ,(3,gridy+5),(1,1),10 )
        
        self.sizer.Add( self.lbl_birim_fiyat  ,(4,gridy+4),(1,1),10 )
        self.sizer.Add( self.lbl_birim_fiyat_txt      ,(4,gridy+5),(1,1),10 )

        
        self.sizer.Add ( line_2 , (5,0),(1,25),flag=wx.EXPAND, border=10)
        
        self.MBsizer.Add( self.sizer , 0, wx.EXPAND)
        
        self.bottomsizer.AddMany( [( self.lbl_plaka  ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                                   ( self.lbl_plaka_txt  ,0, wx.ALIGN_LEFT |wx.ALIGN_CENTER_VERTICAL)
                               ])
        
        self.MBsizer.Add( self.bottomsizer, 0,wx.EXPAND|wx.ALIGN_CENTER_VERTICAL)
        
        self.SetSizeHints(GetSystemMetrics(0) / sabitler.pompa_yatay_mod,230,700,300)
    
class win_Tank_Otomasyonu(wx.Frame ):
    def __init__(self, parent, id_, title):
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (   wx.DEFAULT_FRAME_STYLE|wx.FRAME_NO_TASKBAR|wx.FULL_REPAINT_ON_RESIZE ))
        self.db = param.get_root( self ).DB
        self.O  = param.get_root( self ).O
        self.yatay_mod = sabitler.tank_yatay_mod
        self.scroll = wx.ScrolledWindow(self, 1,style=wx.SIMPLE_BORDER)
        self.scroll.SetScrollbars(1,1,600,400)
        self.mainSizer = wx.GridBagSizer(3,3)
        
        self.scroll.SetSizer(self.mainSizer)
        self.root               = param.get_root( self )
        self.createLayout()

    def createLayout( self ):
        self.Freeze()
        
        q           = self.root.probeQueueContainer
        
        tanklar = self.db.get_raw_data_all( table_name="VIEW_YAKIT_TANKI",
                                           selects="""ID_YAKIT_TANKI,
                                                      YAKIT_TANKI_ADI,
                                                      YAKIT_TANKI_YUKSEKLIK, 
                                                      YAKIT_TANKI_KAPASITE,
                                                      ID_PROBE, 
                                                      PROBE_MARKA,
                                                      PROBE_UZUNLUK, 
                                                      PROBE_ADRES, 
                                                      ID_PORT, 
                                                      ID_YAKIT, 
                                                      PROBE_BASLANGIC_SEVIYE, 
                                                      PORT_ADI,
                                                      SPEED_OF_WIRE,
                                                      YAKIT_ADI""",order="ID_YAKIT_TANKI",dict_mi="object")
        row_num = 0
        col_num = 0
        self.probe_dict = {}
        
        p_about = createAboutTank( self.scroll )
        self.mainSizer.Add(p_about, (row_num, col_num ),(1,self.yatay_mod),(wx.ALL|wx.EXPAND))
        row_num += 1
        
        p_total = TankOtomasyonTotalPanel( self.scroll )
        self.mainSizer.Add(p_total, (row_num, col_num ),(1,self.yatay_mod),(wx.ALL|wx.EXPAND))
        row_num += 1
        
        for tank in tanklar:
            p = TankOtomasyonPanel( self.scroll )
            yakit_adi_1,yakit_adi_2 = util.get_uzun_str_bol_2_word( tank.YAKIT_ADI )

            p.yakit_tanki_adi.SetLabel( ': %s'%tank.YAKIT_TANKI_ADI)
            p.yakit_tanki_kapasite.SetLabel( ': %s lt'%util.data_formatla(tank.YAKIT_TANKI_KAPASITE,'digit'))
            p.probe_marka.SetLabel( ': %s'%tank.PROBE_MARKA)
            p.yakit_adi.SetLabel( ': %s'%yakit_adi_1)
            p.yakit_adi_2.SetLabel('  %s'%yakit_adi_2)

            s.setStaticTextList( p.yakit_yuzde )
            s.setStaticTextList( p.yakit_tanki_adi )
            s.setStaticTextList( p.yakit_tanki_kapasite )
            s.setStaticTextList( p.probe_marka )
            s.setStaticTextList( p.yakit_adi )
            s.setStaticTextList( p.yakit_adi_2 )
            s.setStaticTextList( p.yakit_lt )
            s.setStaticTextList( p.su_lt )
            s.setStaticTextList( p.su_mm )
            s.setStaticTextList( p.yakit_mm )
            s.setStaticTextList( p.yakit_sicaklik )
            s.setStaticTextList( p.yakit_alarm_text,color=(255,0,0))
            s.setStaticTextList( p.su_alarm_text,color=(255,0,0))
            self.probe_dict[ tank.ID_PROBE ] = [p,tank]
            
            
            self.mainSizer.Add(p, (row_num, col_num ),(1,1),(wx.ALL|wx.EXPAND))
            col_num +=1
            if col_num%self.yatay_mod == 0:
                col_num = 0
                row_num+=1
            
        self.Thaw()
        
        self.probe_thread = KThread(target = self.ProbeDegerleriniSetEt, kwargs={'q':q,'p_dict':self.probe_dict,'total_panel':p_total})
        self.probe_thread.start()
        self.GetParent().thread_list.append( self.probe_thread )
        self.Bind(wx.EVT_CLOSE, self.OnClose )
        
    def OnClose( self, event ):
        self.probe_thread.kill()
        self.Destroy()
        
    def ProbeDegerleriniSetEt( self, **kwargs ):
        title       = (MESSAGES.sablon_iki_kelime%(MESSAGES.yakit,MESSAGES.durumu)).decode('Latin5')
        title_total = MESSAGES.toplam_kalan_yakit_stogu .decode('Latin5')
        
        while 1:
            probe_degerleri = kwargs['q'].get()
            
            kwargs['q'].put( probe_degerleri )
            #probe_degerleri = {}
            probe_props     = kwargs['p_dict']
            p_total         = kwargs['total_panel']
            
            total_kapasite_by_yakit       = {}
            total_kalan_stok_by_yakit     = {}
            yakit_adlari                  = {}
                 
            for k,v in probe_degerleri.iteritems():
            
                tank_panel      = probe_props[k][0]
                tank_parameters = probe_props[k][1]
                #print v.su_mm
                tank_panel.Freeze()
                tank_panel.yakit_lt.SetLabel( ': %s lt'%util.data_formatla(v.yakit_litre,'digit'))
                tank_panel.su_lt.SetLabel( ': %s lt'%util.data_formatla(v.su_lt,'digit'))
                tank_panel.su_mm.SetLabel( ': %s mm'%util.data_formatla(v.su_mm,'digit'))
                tank_panel.yakit_mm.SetLabel( ': %s mm'%util.data_formatla(v.yakit_mm,'digit'))
                tank_panel.yakit_sicaklik.SetLabel( ': %s C'%util.data_formatla(v.yakit_sicaklik,'digit'))
                tank_panel.yakit_yuzde.SetLabel( '%%%s'%util.data_formatla(v.yakit_yuzde,'digit'))
                tank_panel.yakit_alarm_text.SetLabel(v.yakit_alarm.text)
                tank_panel.su_alarm_text.SetLabel(v.su_alarm.text)
                if v.yakit_alarm.text != '' or v.su_alarm.text != '':
                    tank_panel.SetBackgroundColour( sabitler.hata_rengi )
                else:
                    tank_panel.SetBackgroundColour( sabitler.idle_rengi )
                
                ch_img           = bar_chart([v.yakit_yuzde], [tank_parameters.YAKIT_ADI], title, str(k), stacked_data = [[v.yakit_by_total_yuzde], [v.su_yuzde]],stacked_color=sabitler.CHART_COLORS.yakit_dict[tank_parameters.ID_YAKIT], bg=True)
                #ch_img          = bar_chart([v.yakit_litre, v.su_lt], [tank_parameters.YAKIT_ADI], title, str(k))
                #tank_img        = wx.ImageFromStream( ch_img )
                tank_img_bitmap = ch_img.ConvertToBitmap()
                util.QClear( tank_panel.QImage)
                tank_panel.QImage.put( tank_img_bitmap )
                #tank_panel.Fit()
                tank_panel.Layout()
                tank_panel.Refresh()
                tank_panel.Thaw()
                
                
                total_by_yakit = 0
                if total_kapasite_by_yakit.has_key( tank_parameters.ID_YAKIT ):
                    total_by_yakit = total_kapasite_by_yakit[ tank_parameters.ID_YAKIT]
            
                total_by_yakit = total_by_yakit + tank_parameters.YAKIT_TANKI_KAPASITE - v.su_lt
                total_kapasite_by_yakit[ tank_parameters.ID_YAKIT ] = total_by_yakit
                
                kalan_by_yakit = 0
                if total_kalan_stok_by_yakit.has_key( tank_parameters.ID_YAKIT ):
                    kalan_by_yakit = total_kalan_stok_by_yakit[ tank_parameters.ID_YAKIT]
                
                if not yakit_adlari.has_key( tank_parameters.ID_YAKIT ):
                    yakit_adlari[ tank_parameters.ID_YAKIT ] = tank_parameters.YAKIT_ADI
            
                kalan_by_yakit = kalan_by_yakit + v.yakit_litre
                total_kalan_stok_by_yakit[ tank_parameters.ID_YAKIT ] = kalan_by_yakit
            
            p_total.Freeze()
            total_yuzde       = []
            yakit_adlari_list = []
            tot_colors        = []
            for k,v in total_kalan_stok_by_yakit.iteritems():
                
                p_total.widgetDict[k][0].SetLabel( ':  '  + util.data_formatla(v,'digit'))  
                p_total.widgetDict[k][1].SetLabel( '/   ' + util.data_formatla(total_kapasite_by_yakit[k],'digit') + ' Lt')
                yakit_adlari_list.append( yakit_adlari[k] )
                y,s,t = getTankYuzdeHesabi(total_kapasite_by_yakit[k],v)
                total_yuzde.append( y )
                if int(y) != 0:
                    tot_colors.append( sabitler.CHART_COLORS.yakit_dict[k] )
            tot_img_name = 'tot'
            #print total_yuzde
            #print tot_colors
            
            ch_img_total          = bar_chart(total_yuzde, yakit_adlari_list, title_total, tot_img_name,700,160,colours=tot_colors)
            #tank_img        = wx.ImageFromStream( ch_img )
            total_img_bitmap = ch_img_total.ConvertToBitmap()
            util.QClear( p_total.QImageTotal)
            p_total.QImageTotal.put( total_img_bitmap )
            p_total.scroll.SetSize( (p_total.GetSize().width,p_total.GetSize().height))
            p_total.Refresh()
            p_total.Thaw()
            time.sleep( sabitler.CihazBeklemeSuresi )

class TankOtomasyonPanel( wx.Panel ):
    
    def __init__(self, parent ):
        wx.Panel.__init__(self, parent,style=wx.SIMPLE_BORDER)

        s.setPanelListBg( self )
        self.sizer = wx.GridBagSizer(1,3)
        self.root  = param.get_root( self ) 
        self.SetSizer( self.sizer )
        self.yakit_tanki_adi          = wx.StaticText(self)
        self.lbl_yakit_tanki_adi      = wx.StaticText(self,-1,MESSAGES.sablon_iki_kelime%(MESSAGES.tank,MESSAGES.adi))
        s.setStaticTextHeader( self.lbl_yakit_tanki_adi )
        
        self.yakit_tanki_kapasite     = wx.StaticText(self)
        self.lbl_yakit_tanki_kapasite = wx.StaticText(self,-1,MESSAGES.kapasite)
        s.setStaticTextHeader( self.lbl_yakit_tanki_kapasite )
        
        self.probe_marka              = wx.StaticText(self)
        self.lbl_probe_marka          = wx.StaticText(self,-1,MESSAGES.probe)
        s.setStaticTextHeader( self.lbl_probe_marka )
        
        self.yakit_adi                = wx.StaticText(self)
        self.yakit_adi_2              = wx.StaticText(self)
        self.lbl_yakit_adi            = wx.StaticText(self,-1,MESSAGES.yakit)
        s.setStaticTextHeader( self.lbl_yakit_adi )
        
        self.yakit_lt                 = wx.StaticText(self)
        self.lbl_yakit_lt             = wx.StaticText(self,-1,'%s lt'%MESSAGES.yakit)
        s.setStaticTextHeader( self.lbl_yakit_lt )
        
        self.su_lt                    = wx.StaticText(self)
        self.lbl_su_lt                = wx.StaticText(self,-1, '%s lt'%MESSAGES.su)
        s.setStaticTextHeader( self.lbl_su_lt )
        
        self.su_mm                    = wx.StaticText(self)
        self.lbl_su_mm                = wx.StaticText(self,-1, '%s mm'%MESSAGES.su)
        s.setStaticTextHeader( self.lbl_su_mm )
        
        self.yakit_mm                 = wx.StaticText(self)
        self.lbl_yakit_mm             = wx.StaticText(self,-1,'%s mm'%MESSAGES.yakit)
        s.setStaticTextHeader( self.lbl_yakit_mm )
        
        self.yakit_yuzde              = wx.StaticText(self)
        
        self.yakit_sicaklik           = wx.StaticText(self)
        self.lbl_yakit_sicaklik       = wx.StaticText(self,-1,MESSAGES.sicaklik)
        
        self.yer_tutucu              = wx.StaticText(self,-1,'                                ')
        
        s.setStaticTextHeader( self.lbl_yakit_sicaklik )
        
        self.yakit_alarm_text        = wx.StaticText(self)
        self.su_alarm_text           = wx.StaticText(self)
        
        gridy = 17
        self.sizer.Add( self.yer_tutucu ,(0,gridy-1),(1,1),1 )
        self.sizer.Add( self.lbl_yakit_tanki_adi  ,(1,gridy),(1,1),1 )
        self.sizer.Add( self.yakit_tanki_adi  ,(1,gridy + 1 ),(1,1),1 )
        
        self.sizer.Add( self.lbl_yakit_adi  ,(2,gridy),(1,1),1 )
        self.sizer.Add( self.yakit_adi      ,(2,gridy + 1),(1,1),1 )
        self.sizer.Add( self.yakit_adi_2    ,(3,gridy + 1),(1,1),1 )
        
        self.sizer.Add( self.lbl_yakit_tanki_kapasite  ,(4,gridy),(1,1),1 )
        self.sizer.Add( self.yakit_tanki_kapasite  ,(4,gridy+1),(1,1),1 )
        
        self.sizer.Add( self.lbl_probe_marka  ,(5,gridy),(1,1),1 )
        self.sizer.Add( self.probe_marka  ,(5,gridy + 1),(1,1),1 )
        
        self.sizer.Add( self.lbl_yakit_mm  ,(6,gridy),(1,1),1 )
        self.sizer.Add( self.yakit_mm  ,(6,gridy+1),(1,1),1 )
        
        self.sizer.Add( self.lbl_yakit_lt  ,(7,gridy),(1,1),1 )
        self.sizer.Add( self.yakit_lt  ,(7,gridy + 1),(1,1),1 )
        
        self.sizer.Add( self.lbl_su_mm  ,(8,gridy),(1,1),1 )
        self.sizer.Add( self.su_mm  ,(8,gridy+1),(1,1),1 )
        
        self.sizer.Add( self.lbl_su_lt  ,(9,gridy),(1,1),1 )
        self.sizer.Add( self.su_lt  ,(9,gridy+1),(1,1),1 )
        
        self.sizer.Add( self.lbl_yakit_sicaklik  ,(10,gridy),(1,1),1 )
        self.sizer.Add( self.yakit_sicaklik  ,(10,gridy+1),(1,1),1 )
        
        self.sizer.Add( self.yakit_yuzde  ,(5,1),(1,1),1 )
        
        self.sizer.Add( self.yakit_alarm_text,(11,gridy),(1,2),1)
        self.sizer.Add( self.su_alarm_text,(12,gridy),(1,2),1)
        self.SetSizeHints(GetSystemMetrics(0) / sabitler.tank_yatay_mod ,280,700,300)
        self.QImage = Queue.Queue()
        
        self.Bind(wx.EVT_PAINT, self.OnPaint)
    
    def OnPaint( self, event ):
        event.Skip()
        dc = wx.PaintDC( self )
        if not self.QImage.empty():
            tank_img = self.QImage.get()
            if tank_img:
                dc.DrawBitmap( tank_img, 43, 15 )

class TankOtomasyonTotalPanel( wx.Panel ):
    
    def __init__(self, parent ):
        wx.Panel.__init__(self, parent)
        #self.Freeze()
        self.scroll = wx.ScrolledWindow(self, 1)
        self.scroll.SetScrollbars(1,1,600,400)
        s.setPanelListBg( self.scroll )
        s.setPanelListBg( self )
        self.sizer = wx.GridBagSizer(1,10)
        self.root  = param.get_root( self )
        self.db    = self.root.DB
        self.scroll.SetSizer( self.sizer )
        
        self.data_all       = self.db.get_raw_data_all(table_name = 'YAKIT', where="YAKIT_DURUM=1",order="ID_YAKIT",selects="ID_YAKIT,YAKIT_ADI" )
        lbl_yakit_adi        =  wx.StaticText(self.scroll, label=MESSAGES.sablon_iki_kelime%( MESSAGES.yakit, MESSAGES.adi ),style=wx.ALIGN_CENTER)
        lbl_miktar          =  wx.StaticText(self.scroll, label=MESSAGES.miktar,style=wx.ALIGN_CENTER)
        
        s.setStaticTextHeader( lbl_yakit_adi )
        s.setStaticTextHeader( lbl_miktar )
        
        self.sizer.Add( lbl_yakit_adi, (0,0) ,(1,1),  wx.ALL | wx.EXPAND,1)
        self.sizer.Add( lbl_miktar, (0,1) ,(1,2), wx.ALL | wx.EXPAND,1)
        
        line_num = 1
        self.widgetDict     = {}
        for data in self.data_all:
            lbl_data_yakit_adi              =   wx.StaticText(self.scroll, label=str(data[1]),style=wx.ALIGN_LEFT)
            s.setStaticTextHeader( lbl_data_yakit_adi )
            lbl_data_miktar                 =   wx.StaticText(self.scroll, label='',style=wx.ALIGN_CENTER)
            s.setStaticTextList( lbl_data_miktar )
            lbl_data_total                  =   wx.StaticText(self.scroll, label='',style=wx.ALIGN_CENTER)
            s.setStaticTextList( lbl_data_total )
            self.widgetDict   [ data[0] ]      = [lbl_data_miktar,lbl_data_total]
            
            self.sizer.Add( lbl_data_yakit_adi,              ( line_num ,0 ),(1,1),wx.ALL | wx.EXPAND, 1 )
            self.sizer.Add( lbl_data_miktar,                 ( line_num ,1 ),(1,1),wx.ALL | wx.EXPAND, 1 )
            self.sizer.Add( lbl_data_total,                  ( line_num ,2 ),(1,1),wx.ALL | wx.EXPAND, 1 )
            line_num += 1
        #print parent.GetParent().GetParent().GetSize()   
        self.SetSizeHints(parent.GetParent().GetParent().GetSize().width-20,160,parent.GetParent().GetParent().GetSize().width-20,300)
        self.QImageTotal = Queue.Queue()
        self.scroll.Bind(wx.EVT_PAINT, self.OnPaint)
        #self.Thaw()
        
    
    def OnPaint( self, event ):
        event.Skip()
        dc = wx.PaintDC( self.scroll )
        
        if not self.QImageTotal.empty():
            tank_total_img = self.QImageTotal.get()
            if tank_total_img:
                dc.DrawBitmap( tank_total_img, 600,0  )

class createAboutTank( wx.Panel ):
    def __init__( self, parent ):
        
        self.PAINT_GRADIENTS = True
        panelStyle = (self.PAINT_GRADIENTS and [wx.BORDER_NONE] or [0])[0]
        wx.Panel.__init__(self, parent, style=panelStyle)
        self.root          =  param.get_root( self )
        self.db            =  self.root.DB
        self.thread_params = self.root.thread_params
        self.p_ops        = ProbeOperations( self.db )
        self.sizer        = wx.GridSizer(1,4,1,1)
        self.SetSizeHints(  GetSystemMetrics(0)-20,64,GetSystemMetrics(0)-20,64)
        child_sizer       = wx.BoxSizer( wx.HORIZONTAL )
        child_sizer2       = wx.BoxSizer( wx.HORIZONTAL )
        png_logo          = wx.Image("%slogo_gr_bg_64.png"%image_path,wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        png_dagitici_logo = wx.Image("%sdagitici_logo.png"%image_path,wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.static_image_container   = wx.StaticBitmap(self, 0, png_logo )
        self.static_image_container_d = wx.StaticBitmap(self, 0, png_dagitici_logo )
        dagitici_ = param.get_dagitici_adi( False )
        bayi_     = param.get_bayi_adi( False ) 
        
        #lbl_      = wx.StaticText(self,-1,'%s - %s' %(dagitici_,bayi_))
        self.lbl_  = '%s' %(bayi_)
        #self.widget_list = [  lbl_]
        #s.setStaticTextPompaHeader( lbl_ )
        self.selected_tank = 0
        self.selected_irs  = 0
        
        self.startColour = wx.Colour( 140,140,140 )
        #self.startColour =  wx.RED 
        self.endColour   = wx.WHITE
        #self.BindEvents ( static_image_container )
        self.cmb_yakit_tanki  = wx.ComboBox(self,1,"", choices=[],
                                            style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY)) 
        yakit_tanki_parametric_objects = []
        yakit_tanki_parametric_objects = self.db.get_parameters_all("YAKIT_TANKI",where="ID_ACTIVE = 1",order="YAKIT_TANKI_ADI",selects="ID_YAKIT_TANKI,YAKIT_TANKI_ADI")
        if yakit_tanki_parametric_objects != [] :
            self.db.widgetMaker( self.cmb_yakit_tanki , yakit_tanki_parametric_objects)
        self.cmb_yakit_tanki.Bind(wx.EVT_COMBOBOX, self.OnYakitTankiSelect)
        
        
        irs_parametric_objects = []
        irs_parametric_objects = self.db.get_parameters_all("IRSALIYE",where="strftime('%Y-%m-%d',GONDERILDIGI_TARIH) >= date('now','-46 days')",order="ID_IRSALIYE DESC",selects="ID_IRSALIYE,IRSALIYE_SERI || IRSALIYE_NO")
        self.cmb_irsaliye    = wx.ComboBox(self,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        if irs_parametric_objects != [] :
            self.db.widgetMaker( self.cmb_irsaliye , irs_parametric_objects)
        self.cmb_irsaliye.Bind(wx.EVT_COMBOBOX, self.OnIrsSelect)
        
        lbl_irs_choice    = s.TransparentText(self, label = '%s : '%MESSAGES.irsaliye)
        s.setStaticTextList ( lbl_irs_choice )
        lbl_tank_choice   = s.TransparentText(self, label = '%s : '%MESSAGES.tank)
        s.setStaticTextList ( lbl_tank_choice )
        
        
        spacer       = '  '
        spacer_ctrl  = s.TransparentText(self, label = spacer )
        
        spacer_1       = '  '
        spacer_ctrl_1  = s.TransparentText(self, label = spacer_1 )
        
        s.BindEvents( self )
        
        if not self.thread_params.TANK_PROBESUZ_AKTIF:    
            btn_basla_pressed  = wx.Image("%sd_baslat_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
            image_basla        = wx.Image("%sd_baslat_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
            self.btn_basla     = LibButtons.ThemedGenBitmapTextButton(self, id=wx.NewId(), bitmap=image_basla,label=MESSAGES.dolum_baslat , style=wx.ALIGN_CENTER)
            self.btn_basla.Bind(wx.EVT_BUTTON, self.OnDolumBaslat )
            self.btn_basla.SetToolTipString(MESSAGES.dolum_baslat)
            self.btn_basla.SetBitmapSelected(btn_basla_pressed)
            s.setButtonFont( self.btn_basla )
            self.btn_basla.Enable( False )
            
            btn_bitir_pressed  =  wx.Image("%sd_bitir_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
            image_bitir        = wx.Image("%sd_bitir_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
            self.btn_bitir         = LibButtons.ThemedGenBitmapTextButton(self, id=wx.NewId(), bitmap=image_bitir,label=MESSAGES.dolum_bitir , style=wx.ALIGN_CENTER)
            self.btn_bitir.Bind(wx.EVT_BUTTON, self.OnDolumBitir )
            self.btn_bitir.SetToolTipString(MESSAGES.dolum_bitir)
            self.btn_bitir.SetBitmapSelected(btn_bitir_pressed)
            s.setButtonFont( self.btn_bitir )
            self.btn_bitir.Enable( False )
            
            child_sizer.AddMany( [
                                 (lbl_tank_choice ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                                 (self.cmb_yakit_tanki ,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                                 (spacer_ctrl ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL),
                                 (lbl_irs_choice ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                                 (self.cmb_irsaliye    ,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL )])
            child_sizer2.AddMany([
                (self.btn_basla            ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                (self.btn_bitir            ,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL )]
            )
        else:
            btn_basla_pressed  = wx.Image("%sd_baslat_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
            image_basla        = wx.Image("%sd_baslat_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
            self.btn_basla     = LibButtons.ThemedGenBitmapTextButton(self, id=wx.NewId(), bitmap=image_basla,label=MESSAGES.sablon_iki_kelime%(MESSAGES.dolum,MESSAGES.kaydet) , style=wx.ALIGN_CENTER)
            self.btn_basla.Bind(wx.EVT_BUTTON, self.OnDolumKaydet )
            self.btn_basla.SetToolTipString(MESSAGES.dolum_baslat)
            self.btn_basla.SetBitmapSelected(btn_basla_pressed)
            s.setButtonFont( self.btn_basla )
            self.btn_basla.Enable( False )

            lbl_dolum_txt   = s.TransparentText(self, label = '%s : '%MESSAGES.sablon_iki_kelime%( MESSAGES.dolum, MESSAGES.miktar))
            s.setStaticTextList ( lbl_dolum_txt )
            self.txt_dolum_deger = wx.TextCtrl(self, -1, "", size=(100,-1),style=(wx.EXPAND))
            
            
            
            child_sizer.AddMany( [
                                 (lbl_tank_choice ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                                 (self.cmb_yakit_tanki ,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                                 (spacer_ctrl ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL),
                                 (lbl_irs_choice ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                                 (self.cmb_irsaliye    ,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                                 ]),
            
            child_sizer2.AddMany([
                (lbl_dolum_txt ,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                (self.txt_dolum_deger ,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                (spacer_ctrl_1 ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL),
                (self.btn_basla            ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )]
            )
            
            
        self.sizer.AddMany( [(self.static_image_container_d ,0, wx.ALIGN_LEFT ),
                             (child_sizer ,0, wx.ALIGN_CENTER|wx.ALIGN_CENTER_VERTICAL ),
                             (child_sizer2 ,0, wx.ALIGN_CENTER|wx.ALIGN_CENTER_VERTICAL ),
                             (self.static_image_container ,0, wx.ALIGN_RIGHT )
                            #(lbl_  ,1 , wx.ALIGN_RIGHT|wx.ALIGN_BOTTOM)
                        ])
        
        
        self.SetSizer( self.sizer )
    def OnDolumKaydet( self, evet ):
        
        probe_q_object        = self.root.probeQueueContainer
        probe_values          = probe_q_object.get()
        probe_q_object.put( probe_values )
        self.tank_obj = self.db.getProbeByTank( self.selected_tank )[0]
        self.secili_tank_deger_obj  = probe_values[ self.tank_obj.ID_PROBE ]
        pre_scope, scope       = self.p_ops.getDolumScopebyTank( self.tank_obj.ID_YAKIT_TANKI )
        self.secili_tank_deger_obj.dolum_scope = scope
        self.dolum_tipi  =  sabitler.TANK_H_ACIKLAMA.DOLUM_BITIS
        self.dolum_deger = self.txt_dolum_deger.GetValue().strip()
        try:
            self.dolum_deger = int(self.dolum_deger)
            c = 1
        except:
            s.uyari_message( self, MESSAGES.miktar_gir , MESSAGES.hata )
            c = 0
        if c == 1:
            s.ok_cancel_message(self,MESSAGES.dolum_basla_onay.decode('latin5'),MESSAGES.dolum_baslat.decode('latin5'), self.save_tank_kayit_probesuz)
        
    def save_tank_kayit_probesuz( self ):
        probe_q_object        = self.root.probeQueueContainer
        probe_values          = probe_q_object.get()
        new_values = {}
        for p_id, p_v in probe_values.iteritems():
            
            if self.selected_tank == p_v.yakit_tank_id:
                
                new_lt = p_v.yakit_litre + self.dolum_deger
                tank_params_obj = self.db.getTankParametersByProbe( p_id )[0]
                yakit_yuzde, su_yuzde, yakit_by_total_yuzde = getTankYuzdeHesabi( tank_params_obj.YAKIT_TANKI_KAPASITE, new_lt, p_v.su_lt, print_ = 1)
                p_v.yakit_litre          = new_lt
                p_v.yakit_yuzde          = yakit_yuzde
                p_v.su_yuzde             = su_yuzde
                p_v.yakit_by_total_yuzde = yakit_by_total_yuzde
                
            new_values[ p_id ] = p_v
        probe_q_object.put( new_values )
        self.p_ops.save_tank_kayit( tank_params_obj = self.tank_obj, v = self.secili_tank_deger_obj , manual_dolum=1, islem_type = self.dolum_tipi, dolum_deger = self.dolum_deger)
    
    def OnDolumBaslat( self, event ):
        probe_q_object        = self.root.probeQueueContainer
        probe_values          = probe_q_object.get()
        probe_q_object.put( probe_values )
        self.tank_obj = self.db.getProbeByTank( self.selected_tank )[0]
        self.secili_tank_deger_obj  = probe_values[ self.tank_obj.ID_PROBE ]
        pre_scope, scope       = self.p_ops.getDolumScopebyTank( self.tank_obj.ID_YAKIT_TANKI )
        self.secili_tank_deger_obj.dolum_scope = scope
        self.dolum_tipi  =  sabitler.TANK_H_ACIKLAMA.DOLUM_BASLANGIC
        self.dolum_deger = 0
        self.action = 'basla'
        s.ok_cancel_message(self,MESSAGES.dolum_basla_onay.decode('latin5'),MESSAGES.dolum_baslat.decode('latin5'), self.save_tank_kayit)
    
    def save_tank_kayit( self )   :
        self.p_ops.save_tank_kayit( tank_params_obj = self.tank_obj, v = self.secili_tank_deger_obj , manual_dolum=1, islem_type = self.dolum_tipi, dolum_deger = self.dolum_deger, irsaliye_id = self.selected_irs)
        if self.action == 'basla':
            self.btn_bitir.Enable( True )
            self.btn_basla.Enable( False )
        elif self.action == 'bitir':
            self.btn_bitir.Enable( False )
            self.btn_basla.Enable( True )
        self.Refresh()
        
    def OnDolumBitir ( self, event ):
        probe_q_object        = self.root.probeQueueContainer
        probe_values          = probe_q_object.get()
        probe_q_object.put( probe_values )
        self.tank_obj = self.db.getProbeByTank( self.selected_tank )[0]
        self.secili_tank_deger_obj  = probe_values[ self.tank_obj.ID_PROBE ]
        pre_scope, scope       = self.p_ops.getDolumScopebyTank( self.tank_obj.ID_YAKIT_TANKI )
        self.secili_tank_deger_obj.dolum_scope = pre_scope
        self.dolum_tipi       =  sabitler.TANK_H_ACIKLAMA.DOLUM_BITIS
        self.dolum_deger      = self.p_ops.getDolumMiktar( self.tank_obj.ID_YAKIT_TANKI, pre_scope, self.secili_tank_deger_obj.yakit_litre )
        self.action = 'bitir'
        s.ok_cancel_message(self, MESSAGES.dolum_bitir_onay.decode('latin5'),MESSAGES.dolum_bitir.decode('latin5'), self.save_tank_kayit)
        
    def OnYakitTankiSelect( self, event ):
        if not self.thread_params.TANK_PROBESUZ_AKTIF:
            self.btn_basla.Enable( False )
            self.btn_bitir.Enable( False )
            obj = self.cmb_yakit_tanki.GetClientData(self.cmb_yakit_tanki.GetSelection())
            dolum_param, dolum_rtipi = self.p_ops.GetDolumBul( obj.id )
            if dolum_param:
                self.btn_bitir.Enable( True )
            else:
                self.btn_basla.Enable( True )
        else:
            self.btn_basla.Enable( False )
            obj = self.cmb_yakit_tanki.GetClientData(self.cmb_yakit_tanki.GetSelection())
            dolum_param, dolum_rtipi = self.p_ops.GetDolumBul( obj.id )
            self.btn_basla.Enable( True )
        
        self.selected_tank = obj.id
        self.Refresh()    
    def OnIrsSelect( self, event ):
        obj = self.cmb_irsaliye.GetClientData(self.cmb_irsaliye.GetSelection())
        self.selected_irs = obj.id
class win_Pompa_Endeks(wx.Frame):
    
    def __init__(self, parent, id_, title):
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (   wx.DEFAULT_FRAME_STYLE|wx.FRAME_NO_TASKBAR ))
        self.root  = param.get_root( self )
        self.db    = self.root.DB
        self.O     = self.root.O 
        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.mainSizer)
        self.createLayout()
        self.Centre()
    
    def createLayout(self):
        
        self.Freeze()
        self.printer = HtmlPrinter()
        #self.book = wx.Notebook(self, style=wx.NB_TOP )
        self.book  = aui.AuiNotebook(self,1,agwStyle=(aui.AUI_NB_TOP|aui.AUI_NB_SCROLL_BUTTONS))
        self.book.SetArtProvider(aui.AuiDefaultTabArt())
        s.setPanelListBg( self.book )
        
        self.pompa_tabancalar  = self.db.get_raw_data_all( table_name="VIEW_POMPA_TABANCALAR",
                                              order="ID_TABANCA",dict_mi="object")
        
        self.p = self.createEndeks(0)
        self.p2 = self.createEndeks(1)
        self.p3 = self.createEndeks(2)
        self.p4 = self.createEndeks(3)
        self.book.AddPage( self.p,MESSAGES.sablon_uc_kelime%(MESSAGES.arabirim,MESSAGES.tabanca,MESSAGES.endeks),True )
        self.book.AddPage( self.p2,"%s %s"%(MESSAGES.sablon_uc_kelime%(MESSAGES.arabirim,MESSAGES.yakit,MESSAGES.turu),MESSAGES.endeks))
        self.book.AddPage( self.p3,MESSAGES.sablon_uc_kelime%(MESSAGES.totalizer,MESSAGES.tabanca,MESSAGES.endeks) )
        self.book.AddPage( self.p4,"%s %s"%(MESSAGES.sablon_uc_kelime%(MESSAGES.totalizer,MESSAGES.yakit,MESSAGES.turu),MESSAGES.endeks))
        
        il = wx.ImageList(64, 64)
        self.img0 = il.Add(wx.Bitmap('%stabanca2_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img1 = il.Add(wx.Bitmap('%sfuel_icon.png'%image_path, wx.BITMAP_TYPE_ANY))
        
        self.book.AssignImageList(il)
        self.book.SetPageImage(0, self.img0)
        self.book.SetPageImage(1, self.img1)
        self.book.SetPageImage(2, self.img0)
        self.book.SetPageImage(3, self.img1)
        self.mainSizer.Add(self.book, 1,wx.ALL | wx.EXPAND)
        self.Layout()
        self.Fit()
        
        if self.GetSize().height > GetSystemMetrics(1) - 200:
            self.SetSize( (self.GetSize().width, GetSystemMetrics(1) - 200))
        
        if self.GetSize().width > GetSystemMetrics(0) - 200:
            self.SetSize( (GetSystemMetrics(0) - 200,self.GetSize().height )) 
        
        self.Thaw()
    
    def createEndeks(self, book_id ):
        
        endeksSizer = wx.BoxSizer( wx.VERTICAL )
        endeksPanel = wx.Panel(self.book,1, style = wx.TAB_TRAVERSAL
                         | wx.CLIP_CHILDREN
                         | wx.FULL_REPAINT_ON_RESIZE)
        endeksPanel.SetSizerAndFit( endeksSizer)
        data_list = []
        donen = {}
        counter = 1
        #print self.root.endeksQueueContainer.empty(), donen
        while not self.root.endeksQueueContainer.empty() and donen == {} and counter< 10000:
            donen = self.root.endeksQueueContainer.get()
            self.root.endeksQueueContainer.put( donen )
            counter+= 1
        if book_id == 0 or book_id == 2:
            for k,v_list in donen.iteritems():
                sub_list = [k,k]
                if len( v_list ) == 2:
                    v = v_list[0]
                    if book_id == 0:
                        v = v_list[1]
                    
                    sub_list.append( v.pompa_adi )
                    sub_list.append( v.beyin_no  )
                    sub_list.append( v.pompa_no  )
                    sub_list.append( v.yakit_adi )
                    
                    sub_list.append( v.total_volume  )
                    sub_list.append( v.total_amount  )
                    sub_list.append( v.sale_quantity )
               
                data_list.append( sub_list )
                    
            headers = ['%s.Id,digit'%MESSAGES.tabanca[0:1],
                       '%s.Id,digit'%MESSAGES.tabanca[0:1],
                       '%s,string'%MESSAGES.sablon_iki_kelime%( MESSAGES.ada, MESSAGES.no),
                       '%s,digit'%MESSAGES.sablon_iki_kelime%(MESSAGES.beyin, MESSAGES.no),
                       '%s,digit'%MESSAGES.sablon_iki_kelime%(MESSAGES.pompa, MESSAGES.no),
                       '%s,string'%MESSAGES.yakit,
                       '%s,digit'%MESSAGES.sablon_iki_kelime%(MESSAGES.toplam, MESSAGES.litre),
                       '%s,digit'%MESSAGES.sablon_iki_kelime%(MESSAGES.toplam, MESSAGES.tutar),
                       '%s,digit'%MESSAGES.sablon_iki_kelime%(MESSAGES.satis, MESSAGES.sayisi)]
            endeksPanel.liste = self.O.createStaticList(endeksPanel, headers, data_list )

        else:
            from collections import defaultdict
            v = defaultdict(list)
            for key, value in sorted(donen.iteritems()):
                v[value[0].yakit_id].append(value)
            
           
            for k_final, v_final in v.iteritems():
                
                sub_list = [k_final,k_final]
                sub_list.append( v_final[0][0].yakit_adi )
                total_total_volume  = 0
                total_total_amount  = 0
                total_sale_quantity = 0
                for deger in v_final:
                    if book_id == 1:
                        total_total_volume  = total_total_volume  + deger[1].total_volume 
                        total_total_amount  = total_total_amount  + deger[1].total_amount 
                        total_sale_quantity = total_sale_quantity + deger[1].sale_quantity
                    elif book_id == 3:
                        total_total_volume  = total_total_volume  + deger[0].total_volume 
                        total_total_amount  = total_total_amount  + deger[0].total_amount 
                        total_sale_quantity = total_sale_quantity + deger[0].sale_quantity
                sub_list.append( total_total_volume )
                sub_list.append( total_total_amount )
                sub_list.append( total_sale_quantity)
                data_list.append( sub_list )
            
            headers = ['%s Id,digit'%MESSAGES.yakit[0:1],
                       '%s Id,digit'%MESSAGES.yakit[0:1],
                       '%s,string'%MESSAGES.yakit,
                       '%s,digit'%MESSAGES.sablon_iki_kelime%(MESSAGES.toplam, MESSAGES.litre),
                       '%s,digit'%MESSAGES.sablon_iki_kelime%(MESSAGES.toplam, MESSAGES.tutar),
                       '%s,digit'%MESSAGES.sablon_iki_kelime%(MESSAGES.satis, MESSAGES.sayisi)]
            
            endeksPanel.liste = self.O.createStaticList(endeksPanel, headers, data_list )
        
        p_button  = wx.Panel(endeksPanel,1, style = wx.TAB_TRAVERSAL
                         | wx.CLIP_CHILDREN
                         | wx.FULL_REPAINT_ON_RESIZE)
        btn_sizer = wx.BoxSizer( wx.HORIZONTAL )
        p_button.SetSizer( btn_sizer )
        p_button.SetSize((100,100))
        '''
        btn_pageSettings_pressed  =  wx.Image("%sprinter_accept.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_pageSettings = wx.Image("%sprinter.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        btn_pageSettings  = LibButtons.ThemedGenBitmapTextButton(p_button, id=wx.ID_ANY, bitmap=image_pageSettings,label="Sayfa Ayarlar�",style=(wx.ALIGN_LEFT|wx.ALIGN_BOTTOM|wx.EXPAND))
        btn_pageSettings.Bind(wx.EVT_BUTTON,self.OnPageSetup)
        btn_pageSettings.SetToolTipString("Sayfa Ayarlar�")
        btn_pageSettings.SetBitmapSelected(btn_pageSettings_pressed)
        
        '''
        btn_preview_pressed  =  wx.Image("%sprinter_accept.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sprinter.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        btn_preview  = LibButtons.ThemedGenBitmapTextButton(p_button, id=wx.ID_ANY, bitmap=image_save,label=MESSAGES.sablon_iki_kelime%(MESSAGES.yazdir,MESSAGES.onizleme),style=(wx.ALIGN_CENTER|wx.ALIGN_BOTTOM|wx.EXPAND))
        btn_preview.Bind(wx.EVT_BUTTON,self.OnPrintPreview)
        btn_preview.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.yazdir,MESSAGES.onizleme))
        btn_preview.SetBitmapSelected(btn_preview_pressed)
        
        '''
        btn_print_pressed  =  wx.Image("%sprinter_accept.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sprinter.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        btn_print  = LibButtons.ThemedGenBitmapTextButton(p_button, id=wx.ID_ANY, bitmap=image_save,label="Yazd�r",style=(wx.ALIGN_RIGHT|wx.ALIGN_BOTTOM|wx.EXPAND))
        btn_print.Bind(wx.EVT_BUTTON,self.OnPrint)
        btn_print.SetToolTipString("Yazd�r")
        btn_print.SetBitmapSelected(btn_print_pressed)
        '''
        
        #s.setButtonFont(btn_print )
        #s.setButtonFont(btn_pageSettings)
        s.setButtonFont(btn_preview)
        
        #btn_sizer.Add(btn_pageSettings,0,wx.ALL|wx.EXPAND)
        btn_sizer.Add(btn_preview,0,wx.ALIGN_LEFT)
        #btn_sizer.Add(btn_print,0,wx.ALL|wx.EXPAND)
        
        
        endeksSizer.Add(endeksPanel.liste,1,wx.ALL|wx.GROW)
        endeksSizer.Add(p_button,0,wx.ALL|wx.GROW)
        
        return endeksPanel

    def OnPrint(self, event):
        #replace self.editor.text and self.editor.title with appropriate values.
        if self.book.GetSelection() == 0:
            liste = self.p.liste
            print_title = ('%s %s'%MESSAGES.sablon_uc_kelime%(MESSAGES.arabirim,MESSAGES.tabanca,MESSAGES.endeks),MESSAGES.listesi).decode('latin5')
        elif self.book.GetSelection() == 1:
            liste = self.p2.liste
            print_title = ("%s %s"%(MESSAGES.sablon_uc_kelime%(MESSAGES.arabirim,MESSAGES.yakit,MESSAGES.turu),MESSAGES.sablon_iki_kelime%(MESSAGES.endeks,MESSAGES.listesi))).decode('latin5')
        elif self.book.GetSelection() == 2:
            liste = self.p3.liste
            print_title = ('%s %s'%MESSAGES.sablon_uc_kelime%(MESSAGES.totalizer,MESSAGES.tabanca,MESSAGES.endeks),MESSAGES.listesi).decode('latin5')
        elif self.book.GetSelection() == 3:
            liste = self.p4.liste
            print_title = ("%s %s"%(MESSAGES.sablon_uc_kelime%(MESSAGES.totalizer,MESSAGES.yakit,MESSAGES.turu),MESSAGES.sablon_iki_kelime%(MESSAGES.endeks,MESSAGES.listesi))).decode('latin5')
        self.printer.Print(liste.Text, print_title)
        
    def OnPrintPreview(self, event):
        if self.book.GetSelection() == 0:
            liste = self.p.liste
            print_title = ('%s %s'%(MESSAGES.sablon_uc_kelime%(MESSAGES.arabirim,MESSAGES.tabanca,MESSAGES.endeks),MESSAGES.listesi)).decode('latin5')
        elif self.book.GetSelection() == 1:
            liste = self.p2.liste
            print_title = ("%s %s"%(MESSAGES.sablon_uc_kelime%(MESSAGES.arabirim,MESSAGES.yakit,MESSAGES.turu),MESSAGES.sablon_iki_kelime%(MESSAGES.endeks,MESSAGES.listesi))).decode('latin5')
        elif self.book.GetSelection() == 2:
            liste = self.p3.liste
            print_title = ('%s %s'%(MESSAGES.sablon_uc_kelime%(MESSAGES.totalizer,MESSAGES.tabanca,MESSAGES.endeks),MESSAGES.listesi)).decode('latin5')
        elif self.book.GetSelection() == 3:
            liste = self.p4.liste
            print_title = ("%s %s"%(MESSAGES.sablon_uc_kelime%(MESSAGES.totalizer,MESSAGES.yakit,MESSAGES.turu),MESSAGES.sablon_iki_kelime%(MESSAGES.endeks,MESSAGES.listesi))).decode('latin5')
        
        self.printer.PreviewText(liste.Text,print_title)
        
    def OnPageSetup(self, event):
        '''
        print 'OnPageSetup'
        self.printer.PageSetup()
        '''
        pass