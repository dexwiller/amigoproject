# -*- mode: python -*-
a = Analysis(['mainWindow.py'],
             pathex=['D:\\Projects\\OtoDesktopV2'],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
def extra_datas(mydir):
    def rec_glob(p, files):
        import os
        import glob
        for d in glob.glob(p):
            if os.path.isfile(d):
                files.append(d)
            rec_glob("%s/*" % d, files)
    files = []
    rec_glob("%s/*" % mydir, files)
    extra_datas = []
    for f in files:
        extra_datas.append((f, f, 'DATA'))

    return extra_datas
a.datas += extra_datas('ico')
a.datas += extra_datas('trans')
a.datas += [('dnzacn.yldz','dnzacn.yldz','DATA')]
a.datas += [('control.exe','dist/control/control.exe','DATA')]
a.datas += [('chartdir.dll','C:\Python27\Lib\site-packages\chartdir.dll','DATA')]
a.datas += [('pychartdir27.pyd','C:\Python27\Lib\site-packages\pychartdir27.pyd','DATA')]
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='mainWindow.exe',
          debug=False,
          strip=None,
          upx=True,
          console=False )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name='Oto')
