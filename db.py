#!/usr/bin/env python
# -*- coding: utf-8 -*-
from local import locale
from sabitler import db_filename
import collections
from local import MESSAGES
db_object_dict = dict(
		P_ACTIVE = 		'''
					CREATE TABLE P_ACTIVE(
						ID_ACTIVE integer primary key autoincrement NOT NULL,
						ACTIVE_NAME text NOT NULL
						);
					''',

                P_KIT_TIPI  =           '''CREATE TABLE P_KIT_TIPI(
                                                   ID_KIT_TIPI  integer primary key autoincrement NOT NULL,
                                                   KIT_TIPI_ADI text NOT NULL,
                                                   ID_ACTIVE    integer NOT NULL references P_ACTIVE( ID_ACTIVE )
                                                );
                                        ''',
                P_HATA_CESITLERI   =    '''
                                                CREATE TABLE P_HATA_CESITLERI(
						ID_HATA integer primary key autoincrement NOT NULL,
						HATA_ACIKLAMA text NOT NULL
						);
                                        ''',
		P_ISKONTO_UYG_CINSI =   '''
					CREATE TABLE P_ISKONTO_UYG_CINSI(
						ID_UYG_CINSI integer primary key autoincrement NOT NULL,
						UYG_CINSI_ADI text NOT NULL
						);
					''',
                P_DURUM            =    '''
                                                CREATE TABLE P_DURUM(
						ID_DURUM integer primary key autoincrement NOT NULL,
						DURUM_ACIKLAMA text NOT NULL
						);
                                        ''',
		P_ISKONTO_UYG_TURU =    '''
					CREATE TABLE P_ISKONTO_UYG_TURU(
						ID_UYG_TURU integer primary key autoincrement NOT NULL,
						UYG_TURU_ADI text NOT NULL
						);
					''',
		P_DAGITICI         =    '''
					CREATE TABLE P_DAGITICI(
						ID_DAGITICI integer primary key autoincrement NOT NULL,
						DAGITICI_NO text NOT NULL,
						DAGITICI_LISANS text NOT NULL,
						DAGITICI_ADI text NOT NULL
						);
					''',
		P_BAYI             =    '''
					CREATE TABLE P_BAYI(
						ID_BAYI integer primary key autoincrement NOT NULL,
						ID_DAGITICI integer NOT NULL references P_DAGITICI(ID_DAGITICI),
						BAYI_LISANS_NO text NOT NULL UNIQUE,
						BAYI_ADI text NOT NULL,
						BAYI_ADRES text NOT NULL,
						BAYI_SEHIR text NOT NULL,
						BAYI_TIPI text NOT NULL,
						BAYI_LISANS_BASLANGIC datetime NOT NULL,
						BAYI_ENLEM real NULL,
						BAYI_BOYLAM real NULL,
						BAYI_TANK_SAYISI integer NOT NULL,
						BAYI_POMPA_SAYISI integer NOT NULL,
						BAYI_TANK_KAPASITE real NOT NULL,
						BAYI_TABANCA_SAYISI integer NOT NULL
						);
					''',
                P_TANK_H_CESIT        = '''
                                        CREATE TABLE P_TANK_H_CESIT(
                                                ID_THC integer primary key autoincrement NOT NULL,
                                                H_ACIKLAMA text NOT NULL
                                                );
                                        ''',
		P_OTOMASYON_PARAMETRE = '''
					CREATE TABLE P_OTOMASYON_PARAMETRE(
						ID_PARAMETRE integer primary key autoincrement NOT NULL,
						PARAMETRE_ADI text NOT NULL,
						PARAMETRE_DEGER integer NOT NULL,
						PARAMETRE_KRITER text
                                                						);
					''',
		P_ILLER               = '''
					CREATE TABLE P_ILLER(
						id integer primary key autoincrement  NOT NULL,
						sehir text
						); 
					''',
		P_ILCELER             = '''
					CREATE TABLE P_ILCELER(
						id integer primary key autoincrement NOT NULL,
						ilce text,
						sehir integer  NOT NULL,
						FOREIGN KEY (sehir)  references P_ILLER(id)
						);
					''',
		P_CARI_TIPI            = '''
					CREATE TABLE P_CARI_TIPI(
						ID_CARI_TIPI integer primary key autoincrement NOT NULL,
						CARI_TIP_ADI text NOT NULL
						);
					''',
                P_ODEME_SEKLI      = '''
                                        CREATE TABLE P_ODEME_SEKLI(
                                                ID_ODEME integer primary key autoincrement NOT NULL,
                                                ODEME_SEKLI text NOT NULL
                                        );
                                        ''',
                P_BASAMAK_KAYDIRMA = '''
					CREATE TABLE P_BASAMAK_KAYDIRMA(
						ID_BASAMAK_KAYDIRMA integer primary key autoincrement NOT NULL,
						BASAMAK_KAYDIRMA_ADI text NOT NULL,
                                                ID_ACTIVE integer
						);
					''',
                P_ARABIRIM_MARKALAR = '''
                                      CREATE TABLE P_ARABIRIM_MARKALAR(
						ID_ARABIRIM_MARKA integer primary key autoincrement NOT NULL,
						ARABIRIM_MARKA_ADI text NOT NULL,
                                                ID_ACTIVE integer DEFAULT 1,
                                                FOREIGN KEY (ID_ACTIVE)  references P_ACTIVE(ID_ACTIVE)
						);
                                      ''',
		P_PROBE_MARKA            = '''
					CREATE TABLE P_PROBE_MARKA(
						ID_PROBE_MARKA integer primary key autoincrement NOT NULL,
						PROBE_MARKA_ADI text NOT NULL,
						ID_ACTIVE integer NOT NULL references P_ACTIVE( ID_ACTIVE )
						);
					''',
                P_LIMIT_CESIT            ='''
                                        CREATE TABLE P_LIMIT_CESIT(
						ID_LIMIT_CESIT integer primary key autoincrement NOT NULL,
						LIMIT_CESIT text NOT NULL
						);
                                           ''',
                P_LIMIT_DONEM            ='''
                                        CREATE TABLE P_LIMIT_DONEM(
						ID_LIMIT_DONEM integer primary key autoincrement NOT NULL,
						LIMIT_DONEM text NOT NULL
						);
                                           ''',
		ISKONTO                = '''
					CREATE TABLE ISKONTO(
						ID_ISKONTO integer primary key autoincrement NOT NULL,
						ISKONTO_ADI text NOT NULL,
						ID_ACTIVE integer DEFAULT 1 NOT NULL  references P_ACTIVE(ID_ACTIVE),
                                                ID_ISKONTO_WEB integer NULL UNIQUE
						);
		
					 ''',
		YAKIT                   = '''
					CREATE TABLE YAKIT(
						ID_YAKIT integer primary key autoincrement NOT NULL,
						YAKIT_ADI text NOT NULL,
						YAKIT_BIRIM_FIYAT real NOT NULL,
						YAKIT_DURUM integer NOT NULL
						);
					''',
		YAKIT_HISTORY           = '''
					CREATE TABLE YAKIT_HISTORY(
						ID_YAKIT_HISTORY integer primary key autoincrement NOT NULL,
						ID_YAKIT integer NOT NULL references YAKIT( ID_YAKIT ),
						ESKI_FIYAT real NOT NULL,
						YENI_FIYAT real NOT NULL,
						TARIH datetime NOT NULL,
                                                DURUM integer DEFAULT 2 NOT NULL references P_ACTIVE(ID_ACTIVE)
						);
					''',
		ISKONTO_YAKIT           = '''
					CREATE TABLE ISKONTO_YAKIT(
						ID_ISKONTO_YAKIT integer primary key autoincrement NOT NULL,
						ID_ISKONTO integer NULL references ISKONTO(ID_ISKONTO),
						ID_YAKIT integer NOT NULL references YAKIT(ID_YAKIT),
						ID_UYG_TURU integer NOT NULL references P_ISKONTO_UYG_TURU(ID_UYG_TURU),
						ID_UYG_CINSI integer NOT NULL references P_ISKONTO_UYG_CINSI(ID_UYG_CINSI),
						ORAN_MIKTAR real NOT NULL,
						ID_ACTIVE integer DEFAULT 1 NOT NULL references P_ACTIVE(ID_ACTIVE),
                                                ID_ISKONTO_YAKIT_WEB integer NULL,
                                                ID_ISKONTO_WEB integer NULL references ISKONTO(ID_ISKONTO_WEB)
						);
					''',
		PORTLAR                 = '''
					CREATE TABLE PORTLAR(
						ID_PORT integer primary key autoincrement NOT NULL,
						PORT_ADI text NOT NULL,
						PARITY text NOT NULL,
						STOP_BIT text NOT NULL,
						BAUND_RATE text NOT NULL,
						DATA_BIT text NOT NULL,
						BEK_SURESI text NOT NULL
						);
					''',
		PROBE                   = '''
					CREATE TABLE PROBE(
						ID_PROBE integer primary key autoincrement NOT NULL,
						PROBE_MARKA integer references P_PROBE_MARKA( ID_PROBE_MARKA ),
						PROBE_UZUNLUK integer,
						SPEED_OF_WIRE integer,
						PROBE_ADRES text,
						ID_ACTIVE integer DEFAULT 1 NOT NULL references P_ACTIVE( ID_ACTIVE )
						);
					''',
		VARDIYA                 = '''
					CREATE TABLE VARDIYA(
						ID_VARDIYA integer primary key autoincrement NOT NULL,
						VARDIYA_ADI text NOT NULL,
						BASLANGIC_SAATI time NOT NULL,
						BITIS_SAATI time NOT NULL,
						ID_ACTIVE integer DEFAULT 1 NOT NULL references P_ACTIVE( ID_ACTIVE )
						);
					''',
		YAKIT_TANKI             = '''
					CREATE TABLE YAKIT_TANKI(
						ID_YAKIT_TANKI integer primary key autoincrement NOT NULL,
						YAKIT_TANKI_ADI text NOT NULL,
						YAKIT_TANKI_YUKSEKLIK integer NOT NULL,
						YAKIT_TANKI_KAPASITE integer NOT NULL,
						ID_PROBE integer NOT NULL references PROBE( ID_PROBE),
						ID_PORT integer NOT NULL references PORTLAR( ID_PORT ),
						ID_YAKIT integer NOT NULL references YAKIT( ID_YAKIT ),
						ID_ACTIVE integer DEFAULT 1 NOT NULL references P_ACTIVE( ID_ACTIVE ),
						PROBE_BASLANGIC_SEVIYE integer NOT NULL
						);
					''',
		YAKIT_TANKI_MIN_MAX    = '''
					CREATE TABLE YAKIT_TANKI_MIN_MAX(
						ID_TANK_MIN_MAX integer primary key autoincrement NOT NULL,
						ID_YAKIT_TANKI integer NOT NULL references YAKIT_TANKI( ID_YAKIT_TANKI ),
						YAKIT_DUSUK_SEVIYE integer ,
						YAKIT_COK_DUSUK_SEVIYE integer NOT NULL ,
						YAKIT_YUKSEK_SEVIYE integer ,
						YAKIT_COK_YUKSEK_SEVIYE integer NOT NULL,
						SU_YUKSEK_SEVIYE integer ,
						SU_COK_YUKSEK_SEVIYE integer NOT NULL,
						YAKIT_IHTIYAC_MIKTARI integer
						);
					''',
                IRSALIYE      = '''
                                   CREATE TABLE IRSALIYE
                                                (
                                                "ID_IRSALIYE" integer primary key NOT NULL,
                                                "IRSALIYE_SERI" text NOT NULL,
                                                "IRSALIYE_NO" integer NOT NULL,
                                                "ID_BELGE_TIPI" integer NOT NULL,
                                                "ID_KDV_DURUM" integer NOT NULL,
                                                "SEVK_TARIHI" datetime NOT NULL,
                                                "IRSALIYE_TARIHI" datetime NOT NULL,
                                                "TOPLAM_TUTAR" real NOT NULL,
                                                "ISKONTO" real NOT NULL,
                                                "KDV_TUTAR" real NOT NULL,
                                                "GENEL_TOPLAM" real NOT NULL,
                                                "IRSALIYE_KALEM" integer NOT NULL,
                                                "GONDERILDIGI_TARIH" DEFAULT (datetime('now','localtime'))  NOT NULL
                                
                                );
                                ''',
		CARI_HESAPLAR          ='''
					CREATE TABLE CARI_HESAPLAR(
						ID_CARI_HESAP integer primary key autoincrement NOT NULL,
						CARI_ADI text NOT NULL,
						YETKILI_ADI text NOT NULL,
						ID_CARI_TIPI integer NOT NULL references P_CARI_TIPI( ID_CARI_TIPI ),
						VERGI_DAIRESI text NULL,
						VERGI_NO text NULL,
						ID_IL integer NULL references P_ILLER ( id ),
						ID_ILCE integer NULL references P_ILCELER ( id ),
						ADRES_1 text NULL,
						ADRES_2 text NULL,
						TEL text NULL,
						FAX text NULL,
                                                WS_DURUM  integer NULL,
                                                ID_ACTIVE integer DEFAULT 1 NOT NULL references P_ACTIVE (ID_ACTIVE),
                                                ID_CARI_HESAP_WEB integer NULL UNIQUE
						);
					''',
                ARACLAR                =''' CREATE TABLE "ARACLAR" (
                                                ID_ARAC integer primary key autoincrement NOT NULL,
                                                ID_CARI_HESAP integer NULL references CARI_HESAPLAR ( ID_CARI_HESAP ),
                                                DEPARTMAN text,
                                                PLAKA text NOT NULL UNIQUE(PLAKA COLLATE NOCASE),
                                                MARKA text,
                                                MODEL text,
                                                YILI integer,
                                                ID_YAKIT integer NOT NULL references YAKIT (ID_YAKIT),
                                                ID_KIT_TIPI integer references P_KIT_TIPI( ID_KIT_TIPI ),
                                                ID_ACTIVE integer DEFAULT 1 NOT NULL references P_ACTIVE (ID_ACTIVE),
                                                ID_CARI_HESAP_WEB integer NULL references CARI_HESAPLAR ( ID_CARI_HESAP_WEB ),
                                                UNIQUE(PLAKA COLLATE NOCASE)
                                                );
                                ''',
                
                KARTLAR                ='''
					CREATE TABLE KARTLAR(
						ID_KART integer primary key autoincrement NOT NULL,
						KART_ADI text NOT NULL,
						ID_CARI_HESAP integer NULL references CARI_HESAPLAR ( ID_CARI_HESAP ),
                                                KART_SERI_NO text NOT NULL,
                                                PLAKA text NULL,
                                                H_DONEMI integer NULL references P_LIMIT_DONEM ( ID_LIMIT_DONEM ),
                                                H_TARIH text NULL,
                                                U_LIMIT integer NULL,
                                                LIMIT_TUR integer NULL references P_LIMIT_CESIT(ID_LIMIT_CESIT),
						ID_ISKONTO integer NULL references ISKONTO( ID_ISKONTO ),
                                                ID_CARI_TIPI integer NOT NULL references P_CARI_TIPI( ID_CARI_TIPI ),
                                                WS_DURUM  integer NULL,
                                                POMPA_ACABILIR_MI integer, 
                                                ID_ACTIVE integer DEFAULT 1 NOT NULL references P_ACTIVE (ID_ACTIVE),
                                                ID_KART_WEB integer NULL,
                                                ID_CARI_HESAP_WEB integer NULL references CARI_HESAPLAR ( ID_CARI_HESAP_WEB ),
                                                ID_ISKONTO_WEB integer NULL references ISKONTO(ID_ISKONTO_WEB)
                                                );
					''',
                SERVIS                = '''
					CREATE TABLE SERVIS(
						ID_SERVIS integer primary key autoincrement NOT NULL,
						ID_HATA integer DEFAULT 1 NOT NULL  references P_HATA_CESITLERI(ID_HATA),
						ID_DURUM integer DEFAULT 1 NOT NULL  references P_DURUM(ID_DURUM),
						ID_BAYI integer DEFAULT 1 NOT NULL  references P_BAYI(ID_BAYI),
                                                ID_WEB_SERVIS integer,
                                                TANK_SAYISI integer NOT NULL,
                                                POMPA_SAYISI integer NOT NULL,
                                                ACIKLAMA text,
                                                TALEP_TARIHI datetime NOT NULL,  
                                                MERKEZE_ILETIM_TARIHI datetime,
                                                ISLEME_ALINMA_TARIHI datetime,
                                                SERVISE_BILDIRIM_TARIHI datetime,
                                                TAMAMLANMA_TARIHI datetime
                                                );
                                        ''',
                POMPA                 = '''
                                                CREATE TABLE POMPA(
                                                ID_POMPA integer primary key autoincrement NOT NULL,
                                                POMPA_ADI text NOT NULL,
                                                POMPA_NO_ON integer NOT NULL,
                                                POMPA_NO_ARKA integer NOT NULL,
                                                ARABIRIM_MARKASI integer NOT NULL,
                                                ARABIRIM_ADRESI integer NOT NULL,
                                                PORT_TIPI text NOT NULL,
                                                ID_PORT integer NOT NULL references PORTLAR( ID_PORT ),
                                                ARABIRIMDEKI_BEYIN_SAYISI integer NOT NULL,
                                                ARABIRIMDEKI_TABANCA_SAYISI integer NOT NULL
                                                );
                                        ''',
                TABANCALAR                 = '''
                                                CREATE TABLE TABANCALAR(
                                                ID_TABANCA integer primary key autoincrement NOT NULL,
                                                ID_POMPA integer NOT NULL references POMPA ( ID_POMPA ),
                                                POMPA_NO integer NOT NULL,
                                                BEYIN_NO integer NOT NULL,
                                                ID_BASAMAK_KAYDIRMA integer NOT NULL references P_BASAMAK_KAYDIRMA (ID_BASAMAK_KAYDIRMA),
                                                TABANCA_NO integer NOT NULL,
                                                ID_YAKIT_TANKI integer NOT NULL references YAKIT_TANKI ( ID_YAKIT_TANKI )
                                                );
                                         ''',
                KART_OKUYUCU             = '''
                                                CREATE TABLE KART_OKUYUCU(
                                                ID_KART_OKUYUCU integer primary key autoincrement NOT NULL,
                                                ID_POMPA integer NOT NULL references POMPA ( ID_POMPA ),
                                                POMPA_NO integer NOT NULL,
                                                KART_OKUYUCU_MARKA integer NOT NULL,
                                                ID_PORT integer NOT NULL references PORTLAR( ID_PORT ),
                                                KART_OKUYUCU_ADRES text NOT NULL
                                                );
                                         ''',
                POMPA_SATIS_KAYIT           ='''
                                                CREATE TABLE POMPA_SATIS_KAYIT(
                                                ID_SATIS integer primary key autoincrement NOT NULL,
                                                POMPA_NO integer NOT NULL,
						POMPA_ADI integer NOT NULL,
                                                BEYIN_NO integer NOT NULL,
                                                ID_TABANCA integer NOT NULL references TABANCALAR(ID_TABANCA),
						TABANCA_NO integer NOT NULL,
                                                ID_CARI_HESAP integer references CARI_HESAPLAR(ID_CARI_HESAP),
                                                ID_YAKIT integer NOT NULL references YAKIT (ID_YAKIT),
                                                SATIS_LITRE real NOT NULL,
                                                BIRIM_FIYAT real NOT NULL,
                                                TUTAR real NOT NULL,
                                                TARIH datetime NOT NULL,
                                                PLAKA text NOT NULL,
                                                ODEME_SEKLI integer NOT NULL DEFAULT 1 references P_ODEME_SEKLI(ID_ODEME),
                                                ID_VARDIYA_AKTIF integer  references VARDIYA_AKTIF(ID_VARDIYA_AKTIF),
						DURUM integer NOT NULL DEFAULT 0,
                                                ID_MUSTERI_CARI integer references CARI_HESAPLAR(ID_CARI_HESAP)
                                                );
                                             ''',
                TANK_KAYITLARI              ='''
                                                CREATE TABLE TANK_KAYITLARI(
                                                ID_TANK_KAYITLARI integer primary key autoincrement NOT NULL,
                                                TANK_ID integer NOT NULL references YAKIT_TANKI(ID_YAKIT_TANKI),
                                                TARIH datetime NOT NULL,
                                                ID_YAKIT integer NOT NULL references YAKIT (ID_YAKIT),
                                                OKUNAN_YAKIT real NOT NULL,
                                                OKUNAN_SU real NOT NULL,
                                                POMPA_SATISI real,
                                                TANK_DOLUMU real,
                                                KAYIT_KODU integer NOT NULL DEFAULT 1 references P_TANK_H_CESIT(ID_THC),
                                                DURUM integer NOT NULL DEFAULT 0,
						DOLUM_SCOPE integer,
						MANUAL_DOLUM integer,
                                                ID_IRSALIYE integer references IRSALIYE( ID_IRSALIYE ),
                                                SU_ALARM_TEXT text,
                                                YAKIT_ALARM_TEXT text,
                                                SICAKLIK integer,
                                                GUN_SONU text
                                                );
                                                ''',
                GUNSONU_RAPORLARI              ='''
                                                CREATE TABLE GUNSONU_RAPORLARI(
                                                ID_TANK_KAYITLARI integer NOT NULL references TANK_KAYITLARI(ID_TANK_KAYITLARI),
                                                TOTAL_DOLUM real,
                                                TOTAL_SATIS real,
                                                ID_YAKIT_TANKI integer NOT NULL references YAKIT_TANKI(ID_YAKIT_TANKI),
                                                YAKIT_TANKI_ADI text,
                                                TARIH datetime NOT NULL,
                                                YAKIT_ADI text,
                                                OKUNAN_YAKIT real NOT NULL,
                                                OKUNAN_SU real NOT NULL,
                                                POMPA_SATISI real,
                                                TANK_DOLUMU real,
                                                ID_THC integer NOT NULL DEFAULT 1 references P_TANK_H_CESIT(ID_THC),
                                                H_ACIKLAMA text,
                                                DURUM integer NOT NULL DEFAULT 0,
						DOLUM_TIPI text,
                                                BASLANGIC_YAKIT real,
                                                PROBE_KALAN real,
                                                SATIS_KALAN real,
                                                GUN_SONU_FARKI real,
                                                GUN_SONU text
                                                );
                                                ''',
		VARDIYA_CARI                 = '''
                                                CREATE TABLE VARDIYA_CARI(
                                                ID_VARDIYA_CARI integer primary key autoincrement NOT NULL,
                                                ID_CARI_HESAP integer NOT NULL references CARI_HESAPLAR ( ID_CARI_HESAP ),
                                                ID_VARDIYA integer NOT NULL references VARDIYA ( ID_VARDIYA ),
						ID_ACTIVE integer DEFAULT 1 NOT NULL references P_ACTIVE( ID_ACTIVE ),
						TARIH datetime NOT NULL
                                                );
					''',
		VARDIYA_AKTIF           = '''
                                                CREATE TABLE VARDIYA_AKTIF(
                                                ID_VARDIYA_AKTIF integer primary key autoincrement NOT NULL,
                                                ID_VARDIYA integer NOT NULL references VARDIYA ( ID_VARDIYA ),
						TARIH datetime NOT NULL,
                                                VARDIYA_ACILIS_TUTAR real,
                                                VARDIYA_KAPANIS_TUTAR real,
                                                VARDIYA_SATIS_SAYISI real
                                                );
					''',
                LOG_TABLO                ='''
                                                CREATE TABLE LOG_TABLO(
                                                ID_LOG integer primary key autoincrement NOT NULL,
                                                KISI text,
                                                ISLEM text,
                                                TARIH datetime DEFAULT (datetime('now','localtime')) NOT NULL
                                                );
                                          ''',
                                
                TANK_KALIBRASYON_TABLOSU  ='''
                                                CREATE TABLE TANK_KALIBRASYON_TABLOSU(
                                                ID_TKT integer primary key autoincrement NOT NULL,
                                                ID_YAKIT_TANKI integer NOT NULL references YAKIT_TANKI(ID_YAKIT_TANKI),
                                                YUKSEKLIK float NOT NULL,
                                                HACIM integer NOT NULL );
                                ''',
                TANK_KALIBRASYON_TABLOSU_HISTORY  ='''
                                                CREATE TABLE TANK_KALIBRASYON_TABLOSU_HISTORY(
                                                ID_TKT integer primary key autoincrement NOT NULL,
                                                ID_YAKIT_TANKI integer NOT NULL references YAKIT_TANKI(ID_YAKIT_TANKI),
                                                YUKSEKLIK float NOT NULL,
                                                HACIM integer NOT NULL,
                                                TARIH datetime NOT NULL);
                                ''',
                EPDK_TABLO8                 = '''
                                                CREATE TABLE EPDK_TABLO8(
                                                ID_ACIKLAMA_KODU integer primary key autoincrement NOT NULL,
                                                ACIKLAMA_KODU text DEFAULT 0 NOT NULL,
                                                LISANS_NO text NOT NULL references P_BAYI(BAYI_LISANS_NO),
                                                ID_YAKIT integer NOT NULL references YAKIT ( ID_YAKIT ),
                                                OZEL_DURUM_BASL_TAR_SAAT datetime NOT NULL,
                                                OZEL_DURUM_BIT_TAR_SAAT datetime NOT NULL,
                                                OZEL_DURUM_TURU text,
                                                AKARYAKIT_KAYIP real DEFAULT 0 NOT NULL,
                                                AKARYAKIT_KAZANC real DEFAULT 0 NOT NULL,
                                                DURUM integer,
                                                ACIKLAMA text,
                                                OLUSTURMA_TARIHI datetime DEFAULT (datetime('now','localtime')) NOT NULL,
                                                WEBE_ILETIM_TARIHI datetime
                                                );
                                         ''',
		
                #Açıklama Kodu: Diğer tablolarda bulunan açıklamalara verilen kod
                #Özel Durum Başlangıç Tarihi ve Saati:  Özel durumun başladığı tarih ve saat
                #Özel Durum Bitiş Tarihi ve Saati:  Özel durumun bittiği tarih ve saat
                #Özel Durum Türü: Özel durumun türü 
                #Akaryakıt Miktarındaki Kayıp:  Özel  durum  sonucunda  oluşan  toplam  akaryakıt  miktarındaki kayıp
                #Akaryakıt Miktarındaki Kazanç:   Özel  durum  sonucunda  oluşan  toplam  akaryakıt  miktarındaki kazanç
                #Durum: Özel durum kaydı kapatma işareti
                #Periyot: Oluşuma Bağlı
                #Son Gönderim Zamanı: Yok 
                #Gönderim Yöntemi: Web Servis
                #İlişkili Olduğu Tablolar: Tüm Tablolar
                #Durum kolonuna özel durum kaydını tamamlamak ve kesinleştirmek için “1”, kaydı kesinleştirmeden 
                #güncellemek için “0” girilecektir.
                #Tabloda  yer  alan  akaryakıt  miktarındaki  kayıp  ve  kazanç  ifadeleri  temel  olarak  akaryakıt 
                #istasyonundaki tanklarda yapılan temizlik işlemleri sonucunda tanktan çıkan ve sonradan tanka gire 
                #akaryakıt miktarı arasındaki farkın tabloya yansıtılabilmesi amacını taşımaktadır.
                                
                EPDK_TABLO1                 = '''
                                                CREATE TABLE EPDK_TABLO1(
                                                ID_TABLO1 integer primary key autoincrement NOT NULL,
                                                TARIH datetime NOT NULL,
                                                LISANS_NO text NOT NULL  references P_BAYI(BAYI_LISANS_NO),
                                                ID_YAKIT integer NOT NULL references YAKIT ( ID_YAKIT ),
                                                TANK_DONEM_BASI real NOT NULL,
                                                TANK_DOLUM real DEFAULT 0 NOT NULL,
                                                POMPA_SATIS real DEFAULT 0 NOT NULL,
                                                TANK_DONEM_SONU real NOT NULL,
                                                KOY_POMPA_DOLUM real DEFAULT 0,
                                                ACIKLAMA integer references EPDK_TABLO8(ID_ACIKLAMA_KODU),
                                                OLUSTURMA_TARIHI datetime DEFAULT (datetime('now','localtime')) NOT NULL,
                                                WEBE_ILETIM_TARIHI datetime
                                                );
                                         ''',
                #Dönem Başı Stok: 00:00 itibarıyla tanklarda bulunan akaryakıt miktarı. 
                #Saat 00:00 itibariyle  aynı ürün tipindeki tanklarda bulunan ürünlerin toplam miktarı. “Dönem  Başı 
                #Stok” miktarı bir önceki gün “Dönem Sonu Stok” miktarı ile eşit olmalıdır.
                #Tanka Dolum: 00:00-23:59 arası tanklara dolumu yapılan akaryakıt miktarı.
                #Gün içerisinde ayni ürün tipinde yapılan dolumların toplam miktarı.
                #Pompa Satış: 00:00-23:59 arası pompadan yapılan akaryakıt satış miktarı.
                #Söz konusu miktarın içinde dış satış için ve ayrıca köy  pompası ve tarımsal amaçlı satış tankeri için 
                #yapılan  transferler  dâhil  edildi  ise  toplam  hesapta  yanlışlık  olmaması  amacıyla  Köy  Pompaları  ve 
                #Tarımsal Satış Amaçlı Tankerlere Yapılan Dolum Miktarı dikkatli bir şekilde takip edilmelidir.
                #Dönem Sonu Stok: 23:59 itibarıyla tanklarda bulunan akaryakıt miktarı
                #Saat  23:59  itibariyle aynı ürün tipindeki tanklarda bulunan ürünlerin toplam miktarı. “Dönem  Sonu
                #Stok” miktarı bir sonraki gün “Dönem Başı Stok” miktarı ile eşit olmalıdır.
                #Köy Pompaları ve Tarımsal  Satış Amaçlı Tankerlere Yapılan Dolum Miktarı:  00:00-23:59 arası 
                #köy pompaları ve tarımsal satış amaçlı tankerlere dolum amacıyla çıkışı yapılan akaryakıt miktarı.
                #Periyot: Günlük
                #Son Gönderim Zamanı: İlgili günü takip eden gün saat 06:00
                #İlişkili Olduğu Tablolar: T2, T3, T4, T18
                #T1  verilerinin  belirlenen  son  gönderim  zamanından  sonra  gönderilmesi  mevzuat  kapsamında 
                #mümkün olmayıp, gönderilemeyen elektronik verilerin e-posta yoluyla gönderilmesi yükümlülüklerin 
                #yerine getirilmesini sağlamamaktadır.
                EPDK_TABLO2                 = '''
                                                CREATE TABLE EPDK_TABLO2(
                                                ID_TABLO2 integer primary key autoincrement NOT NULL,
                                                TARIH datetime NOT NULL,
                                                LISANS_NO text NOT NULL  references P_BAYI(BAYI_LISANS_NO),
                                                ID_YAKIT integer NOT NULL references YAKIT ( ID_YAKIT ),
                                                TANKER_PLAKA text NOT NULL,
                                                POMPA_SATIS real DEFAULT 0 NOT NULL,
                                                IST_YAPILAN_DOLUM real NOT NULL,
                                                ACIKLAMA integer references EPDK_TABLO8(ID_ACIKLAMA_KODU),
                                                OLUSTURMA_TARIHI datetime DEFAULT (datetime('now','localtime')) NOT NULL,
                                                WEBE_ILETIM_TARIHI datetime
                                                );
                                         ''',
                #Tanker Plaka No: Tarımsal satış amaçlı tanker plaka numarası
                #Pompa Satış: 00:00-23:59 arası pompadan yapılan akaryakıt satış miktarı
                #İstasyondan Yapılan  Dolum  Miktarı:  00:00-23:59  arası  istasyondan  tarımsal  satış  amaçlı  tankere 
                #ikmal edilen akaryakıt miktarı.
                #Periyot: Aylık
                #Son Gönderim Zamanı: Rapora esas dönemi izleyen ayın 4 üncü günü. 
                #İlişkili Olduğu Tablolar: T1, T3, T4, T12
                #Tarımsal  amaçlı  satış  tankerlerinin  verileri  günlük  bazda  aylık  dönemler  itibarı  ile  EPDK’ya 
                #gönderilecektir.  Tankerden satılan akaryakıt, istasyondan dolduruldu ise” İstasyondan Yapılan Dolum 
                #Miktarı” sütunu mutlaka doldurulmalıdır.
                                
                EPDK_TABLO3                 = '''
                                                CREATE TABLE EPDK_TABLO3(
                                                ID_TABLO3 integer primary key autoincrement NOT NULL,
                                                TARIH datetime NOT NULL,
                                                LISANS_NO text NOT NULL  references P_BAYI(BAYI_LISANS_NO),
                                                KOY_POMPASI_NO integer,
                                                ID_YAKIT integer NOT NULL references YAKIT ( ID_YAKIT ),
                                                TANK_DONEM_BASI real NOT NULL,
                                                TANK_DOLUM real DEFAULT 0 NOT NULL,
                                                POMPA_SATIS real DEFAULT 0 NOT NULL,
                                                TANK_DONEM_SONU real NOT NULL,
                                                IST_YAPILAN_DOLUM real NOT NULL DEFAULT 0,
                                                ACIKLAMA integer references EPDK_TABLO8(ID_ACIKLAMA_KODU),
                                                OLUSTURMA_TARIHI datetime DEFAULT (datetime('now','localtime')) NOT NULL,
                                                WEBE_ILETIM_TARIHI datetime
                                                );
                                         ''',
                #Köy Pompası No: Kurum tarafından sağlanan köy pompası belirleyici numarası
                #Dönem Başı Stok: 00:00 itibarıyla tanklarda bulunan akaryakıt miktarı
                #Tanka Dolum: 00:00-23:59 arası tanklara dolumu yapılan akaryakıt miktarı
                #Pompa Satış: 00:00-23:59 arası pompadan yapılan akaryakıt satış miktarı
                #Dönem Sonu Stok: 23:59 itibarıyla tanklarda bulunan akaryakıt miktarı
                #İstasyondan Yapılan  Dolum  Miktarı:  00:00-23:59  arası  istasyondan  köy  pompasına  ikmal  edilen 
                #akaryakıt miktarını
                #Periyot: Günlük 
                #Son Gönderim Zamanı: İlgili günü takip eden gün saat 06:00
                #İlişkili Olduğu Tablolar: T1, T2, T4, T13
                #Herhangi  bir  nedenle  bayilik  lisansı  altında  bulunan  köy  pompalarından  birine  ikmal  kesilmesi 
                #gereken bir durum ortaya çıkarsa, diğer köy pompalarına ikmalin durdurulması gerekmemektedir.

                                
                EPDK_TABLO4                 = '''
                                                CREATE TABLE EPDK_TABLO4(
                                                ID_TABLO4 integer primary key autoincrement NOT NULL,
                                                DONEM integer NOT NULL,
                                                LISANS_NO text NOT NULL  references P_BAYI(BAYI_LISANS_NO),
                                                ID_YAKIT integer NOT NULL references YAKIT ( ID_YAKIT ),
                                                TANK_DONEM_BASI real NOT NULL,
                                                TANK_DOLUM real DEFAULT 0 NOT NULL,
                                                POMPA_SATIS real DEFAULT 0 NOT NULL,
                                                TANK_DONEM_SONU real NOT NULL,
                                                KOY_YAPILAN_DOLUM real NOT NULL,
                                                ACIKLAMA integer references EPDK_TABLO8(ID_ACIKLAMA_KODU),
                                                OLUSTURMA_TARIHI datetime DEFAULT (datetime('now','localtime')) NOT NULL,
                                                WEBE_ILETIM_TARIHI datetime
                                                );
                                         ''',
                #Köy Pompası No: Kurum tarafından sağlanan köy pompası belirleyici numarası
                #Dönem Başı Stok: 00:00 itibarıyla tanklarda bulunan akaryakıt miktarı
                #Tanka Dolum: 00:00-23:59 arası tanklara dolumu yapılan akaryakıt miktarı
                #Pompa Satış: 00:00-23:59 arası pompadan yapılan akaryakıt satış miktarı
                #Dönem Sonu Stok: 23:59 itibarıyla tanklarda bulunan akaryakıt miktarı
                #İstasyondan Yapılan  Dolum  Miktarı:  00:00-23:59  arası  istasyondan  köy  pompasına  ikmal  edilen 
                #akaryakıt miktarını
                #Periyot: Günlük 
                #Son Gönderim Zamanı: İlgili günü takip eden gün saat 06:00
                #İlişkili Olduğu Tablolar: T1, T2, T4, T13
                #Herhangi  bir  nedenle  bayilik  lisansı  altında  bulunan  köy  pompalarından  birine  ikmal  kesilmesi 
                #gereken bir durum ortaya çıkarsa, diğer köy pompalarına ikmalin durdurulması gerekmemektedir

                                
                EPDK_TABLO5                 = '''
                                                CREATE TABLE EPDK_TABLO5(
                                                ID_TABLO5 integer primary key autoincrement NOT NULL,
                                                BAYI_BELGE_TARIHI datetime NOT NULL,
                                                BAYI_BELGE_NO text NOT NULL,
                                                LISANS_NO text NOT NULL  references P_BAYI(BAYI_LISANS_NO),
                                                ID_YAKIT integer NOT NULL references YAKIT ( ID_YAKIT ),
                                                SATIS_IL integer NOT NULL references P_ILLER (id),
                                                DIS_SATIS_MIKTARI real DEFAULT 0 NOT NULL,
                                                DAG_SEVK_IRS_TAR datetime NOT NULL,
                                                DAG_SEVK_IRS_NO text NOT NULL,
                                                ACIKLAMA integer references EPDK_TABLO8(ID_ACIKLAMA_KODU),
                                                OLUSTURMA_TARIHI datetime DEFAULT (datetime('now','localtime')) NOT NULL,
                                                WEBE_ILETIM_TARIHI datetime
                                                );
                                         ''',
                #Bayi  Belge  Tarihi:  Bayinin  istasyon  tankına  girmeden  dış  satış  olarak  yaptığı  satışa  ilişkin 
                #düzenlediği belgenin tarihi
                #Bayi Belge No:  Bayinin istasyon tankına girmeden dış satış olarak yaptığı satışa ilişkin düzenlediği 
                #belgenin numarası
                #Satış Yapılan İl: Satışın yapıldığı il
                #Belgelenen  Dış  Satış  Miktarı:  Bayi  tarafından  dağıtıcı  lisansı  sahibine  gönderilen  satış  belgesi 
                #örneğindeki miktar
                #Dağıtıcı  Sevk  İrsaliye  Tarihi:  Dağıtıcı  lisansı  sahibi  tarafından  düzenlenen  ve  bayilik  lisansı 
                #sahibinin dış satışı ile eşleşen sevk irsaliyesinin tarihi
                #Dağıtıcı Sevk İrsaliye No:  Dağıtıcı lisansı sahibi tarafından düzenlenen ve bayilik lisansı sahibinin 
                #dış satışı ile eşleşen sevk irsaliyesinin numarası
                #Periyot: Aylık,
                #Son Gönderim Zamanı: Dış satışa esas dönemi izleyen ayın 19 uncu günü
                #İlişkili Olduğu Tablolar: T6, T7
                #Dış satış bulunmasa da dış satış yapılmadığının beyan edilmesi açısından "0" değeri girilmelidir.
                #Bayilik  lisansı  sahipleri  yaptıkları  dış  satışlara  ilişkin  satış  belgelerini,  dağıtıcı  lisansı  sahibince 
                #düzenlenen irsaliyeler ile eşleştirmek ve dağıtıcı lisansı sahibine bildirmek ile yükümlüdür. 
                #Ancak, dış satış faturalarının dağıtıcı lisansı sahiplerine bildirimi esnasında ticari sır niteliğinde olan
                #bilgilerin  üçüncü  taraflar  ile  paylaşılması  gerekmemektedir.  Dağıtıcı  lisansı  sahibi  bayisinin 
                #gönderdiği satış belgelerini düzenlediği irsaliyelerle eşleştirmelidir.


                                
                EPDK_TABLO6                 = '''
                                                CREATE TABLE EPDK_TABLO6(
                                                ID_TABLO6 integer primary key autoincrement NOT NULL,
                                                BELGE_TANZIM_TARIHI datetime NOT NULL,
                                                LISANS_NO text NOT NULL  references P_BAYI(BAYI_LISANS_NO),
                                                ID_YAKIT integer NOT NULL references YAKIT ( ID_YAKIT ),
                                                DAG_SATIS_MIKTARI real DEFAULT 0 NOT NULL,
                                                DAG_SEVK_IRS_NO text NOT NULL,
                                                ISTASYON_DOLUM real DEFAULT 0 NOT NULL,
                                                KOY_POMPASI_DOLUM real DEFAULT 0 NOT NULL,
                                                TAR_AMAC_TANKER_SATIS real DEFAULT 0 NOT NULL,
                                                DIS_SATIS_MIKTARI real DEFAULT 0 NOT NULL,
                                                IADE_MIKTAR real DEFAULT 0 NOT NULL,
                                                ACIKLAMA integer references EPDK_TABLO8(ID_ACIKLAMA_KODU),
                                                OLUSTURMA_TARIHI datetime DEFAULT (datetime('now','localtime')) NOT NULL,
                                                WEBE_ILETIM_TARIHI datetime
                                                );
                                         ''',
                #Belge Tanzim Tarihi: Dağıtıcının satış esnasında bayisine düzenlediği belgenin tarihi
                #Dağıtıcı  Satış  Miktarı:  Dağıtıcının  bayiye  kestiği  her  bir  sevk  irsaliyesinde  o  akaryakıt  türü  için 
                #belirtilen miktar
                #Dağıtıcı  Sevk  İrsaliye  No:  Dağıtıcının  satış  esnasında  bayisine  düzenlediği  sevk  irsaliyesinin numarası
                #İstasyon  Dolum:  Bayinin  dağıtıcısından  aldığı,  ilgili  sevk  irsaliyesine  karşılık  istasyon  tankına 
                #koyduğu ve otomasyon sisteminden gelen istasyon tankına akaryakıt dolum miktarı
                #Köy  Pompası  Dolum:  Bayinin  dağıtıcısından  aldığı,  ilgili  sevk  irsaliyesi  karşılığı  köy  pompası 
                #deposuna koyduğu ve otomasyon sisteminden gelen köy pompası tankına akaryakıt dolum miktarı
                #Tarımsal  Satış  Amaçlı  Tanker  Satış:  Bayinin  dağıtıcıdan  aldığı,  ilgili  sevk  irsaliyesi  karşılığı 
                #tarımsal satış amaçlı tankerinden sattığı ve otomasyon sisteminden gelen akaryakıt satış miktarı
                #Dış Satış Miktarı:  Bayi tarafından dağıtıcı lisansı sahibine gönderilen  “satış belgesi”nde belirtilen 
                #akaryakıt miktarı
                #İade Edilen Miktar: Bayinin dağıtıcı lisansı sahibine iade ettiği akaryakıt miktarı
                #Periyot: Aylık,
                #Son Gönderim Zamanı: İlgili dönemi izleyen ayın 19 uncu günü  
                #İlişkili Olduğu Tablolar: T5, T7
                #Satış takip tablosu mutabakat tablosunun oluşturulmasında esas alınması gereken tablodur. Mutabakat 
                #tablosu T6’nın özet hali olup, her iki tablo arasında farklılık olmamalıdır.
                #T18  ve  T6  arasında  farklılık  olmamalıdır.  Ancak,  T18’de  bildirilmemiş  bir  bilginin  T6’da  da 
                #bildirilmemesi uygulaması yanlıştır. 
                #Her ne kadar tablo 4 ile tablo 6  verilerinin  birebir tutması beklenmese de birbirleri ile uyumlu olması 
                #beklenmektedir. Bunun temel nedeni tablo 4’ün ilgili aya ait istasyondaki tüm hareketleri gösteriyor 
                #olması  ancak  tablo  6’nın  ise  dağıtıcının  ilgili  ayda  bayisine  sattığı  satışların  toplamı  ve  bu  satılan 
                #akaryakıtın nereye gittiğinin tespitidir. Bu nedenle dağıtıcının,  bayisine kestiği her bir irsaliyeyi  bayi 
                #ile koordineli bir şekilde nereye gittiğini takip etmesi gerekmektedir
                EPDK_TABLO7                 = '''
                                                CREATE TABLE EPDK_TABLO7(
                                                ID_TABLO7 integer primary key autoincrement NOT NULL,
                                                MUTABAKAT_DONEMI datetime NOT NULL,
                                                LISANS_NO text NOT NULL  references P_BAYI(BAYI_LISANS_NO),
                                                ID_YAKIT integer NOT NULL references YAKIT ( ID_YAKIT ),
                                                DAG_SATIS_MIKTARI real DEFAULT 0 NOT NULL,
                                                OTO_SIS_GECEN_DOLUM real DEFAULT 0 NOT NULL,
                                                DIS_SATIS real DEFAULT 0 NOT NULL,
                                                MUTABAKAT_DURUMU text,
                                                ACIKLAMA integer references EPDK_TABLO8(ID_ACIKLAMA_KODU),
                                                OLUSTURMA_TARIHI datetime DEFAULT (datetime('now','localtime')) NOT NULL,
                                                WEBE_ILETIM_TARIHI datetime
                                                );
                                         ''',
                #Mutabakat Dönemi:  Aylık
                #Dağıtıcı  Tarafından  Satılan  Akaryakıt  Miktarı:  Dağıtıcının  ilgili  ay  içerisinde  bayisine  yaptığı 
                #akaryakıt satışlarının irsaliye bazında toplamı
                #Otomasyon  Sisteminden  Geçen Akaryakıt  Miktarı:  İrsaliye  bazında  istasyonun  deposuna  dolan, 
                #köy pompasına dolan, tarımsal amaçlı satış tankeri ile satışı yapılan akaryakıt miktarı
                #Dış Satış Miktarı: Bayilik lisansı sahibi tarafından yapılan toplam akaryakıt dış satış miktarı (irsaliye 
                #bazında)
                #Mutabakat: "Sağlandı" - "Sağlanmadı" ibaresinin bulunduğu alan
                #Aylık Akaryakıt  Stok Ve Alım  Satım  Raporu Tablosu  mutabakata  esas  dönemi  izleyen  ayın  20  nci
                #gününe  kadar  Kuruma  Petrol  Piyasası  Bilgi  Sistemi  üzerinden  elektronik  imza  ile  gönderilecek 
                #olduğundan web servis üzerinden bildirilmesine gerek bulunmamaktadır. 

                
                EPDK_TABLO9                 = '''
                                                CREATE TABLE EPDK_TABLO9(
                                                ID_TABLO9 integer primary key autoincrement NOT NULL,
                                                TARIH_SAAT datetime NOT NULL,
                                                LISANS_NO text NOT NULL  references P_BAYI(BAYI_LISANS_NO),
                                                ID_YAKIT integer NOT NULL references YAKIT ( ID_YAKIT ),
                                                GECERLI_FIYAT real NOT NULL,
                                                OLUSTURMA_TARIHI datetime DEFAULT (datetime('now','localtime')) NOT NULL,
                                                WEBE_ILETIM_TARIHI datetime
                                                );
                                         ''',
                #Tarih- Saat:  Anlık
                #Geçerli Satış Fiyatı: Geçerli olan fiyat(virgülden sonra iki haneye kadar girilecektir)
                #Akaryakıt türlerine göre bayinin  satış fiyatları günlük olarak gönderilecek, ayrıca akaryakıt fiyatının 
                #değişmesini müteakip yeni fiyat anlık olarak gönderilecektir.  
                #Periyot: Oluşuma Bağlı
                #Son Gönderim Zamanı: Anlık 
                #Gönderim Yöntemi: Web Servis
                #İlişkili Olduğu Tablolar: Yok
                #Fiyat bildirimlerinde kullanılacak olan akaryakıt türleri: 
                #  Kurşunsuz benzin 95 oktan 
                #  Katkılı kurşunsuz benzin 95 oktan
                #  Kurşunsuz benzin 95 oktan (diğer) = 97 Oktan Kurşunsuz Benzin
                #  Motorin
                #  Motorin (diğer) = Katkılı Motorin
                #  Motorin (diğer 2) = GTL
                #  Gazyağı
                #  Yakıt nafta
                #  Fuel oil 3 
                #  Fuel oil 4
                #  Fuel oil 5
                #  Fuel oil 6

                EPDK_TABLO10                 = '''
                                                CREATE TABLE EPDK_TABLO10(
                                                ID_TABLO10 integer primary key autoincrement NOT NULL,
                                                NUMUNE_ALIM_TARIH datetime NOT NULL,
                                                LISANS_NO text NOT NULL  references P_BAYI(BAYI_LISANS_NO),
                                                ID_YAKIT integer NOT NULL references YAKIT ( ID_YAKIT ),
                                                ANALIZ_TARIHI datetime NOT NULL,
                                                ANALIZ_YAP_KURUM text NOT NULL,
                                                AKREDITASYON_NO text NOT NULL,
                                                ANALIZ_NO text NOT NULL,
                                                ANALIZ_SONUCU text NOT NULL,
                                                ACIKLAMA text
                                                );
                                         ''',
                #Numune Alım Tarihi: Numunenin istasyondan alındığı tarih
                #Analiz Tarihi: Numunenin analizinin yapıldığı tarih
                #Analizi Yapan Kurum: Numune analizini yapan kurumun resmi adı
                #Akreditasyon No: Numune analizini yapan kurumun akreditasyon numarası
                #Analiz No: Numune analizini yapan kurumun verdiği analiz numarası
                #Analiz Sonucu:  Analiz raporunun sonucu (Geçerli veya Geçersiz)
                #Periyot: Oluşuma Bağlı
                #Son Gönderim Zamanı: Numune analiz tarihinden bir ay sonra
                #Gönderim Yöntemi:  Petrol Piyasası Bilgi Sistemi 
                #İlişkili Olduğu Tablolar: -Numune  Analiz  Bilgileri  Tablosu  elektronik  imza  ile  Petrol  Piyasası  Bilgi  Sistemi  üzerinden 
                #bildirilecektir.

                EPDK_TABLO11                 = '''
                                                CREATE TABLE EPDK_TABLO11(
                                                ID_TABLO11 integer primary key autoincrement NOT NULL,
                                                BAYI_BELGE_TANZIM_TAR datetime NOT NULL,
                                                BAYI_BELGE_NO text NOT NULL,
                                                LISANS_NO text NOT NULL  references P_BAYI(BAYI_LISANS_NO),
                                                ID_YAKIT integer NOT NULL references YAKIT ( ID_YAKIT ),
                                                DAG_SATIS_MIK real NOT NULL,
                                                DAG_SEVK_IRS_TAR datetime NOT NULL,
                                                DAG_SEVK_IRS_NO text NOT NULL,
                                                BELGELENEN_DIS_SATIS_MIKTARI double DEFAULT 0 NOT NULL,
                                                SATIS_IL text NOT NULL references P_ILLER (id),
                                                ACIKLAMA integer references EPDK_TABLO8(ID_ACIKLAMA_KODU),
                                                OLUSTURMA_TARIHI datetime DEFAULT (datetime('now','localtime')) NOT NULL,
                                                WEBE_ILETIM_TARIHI datetime
                                                );
                                         ''',
                #Bayi Belge Tanzim Tarihi:  Bayinin yaptığı satışa ilişkin düzenlediği belgenin tarihi
                #Bayi Belge No: Bayinin yaptığı satışa ilişkin düzenlediği belgenin numarası
                #Dağıtıcı  Satış:  Dağıtıcının  bayiye  kestiği  her  bir  sevk  irsaliyesinde  o  akaryakıt  türü  için  belirtilen 
                #miktar
                #Dağıtıcı  Sevk  İrsaliye  Tarihi:  Dağıtıcının  satış  esnasında  bayisine  düzenlediği  sevk  irsaliyesinin 
                #tarihi
                #Dağıtıcı Sevk İrsaliye No:  Dağıtıcının  satış  esnasında  bayisine  düzenlediği  sevk  irsaliyesinin 
                #numarası 
                #Belgelenen  Dış  Satış  Miktarı:  Bayi  tarafından  dağıtıcı  lisansı  sahibine  gönderilen  satış  belgesi 
                #örneği esasındaki miktar
                #Satış Yapılan İl: Bayinin akaryakıt satışını gerçekleştirdiği il
                #Periyot: Aylık
                #Son Gönderim Zamanı: İlgili dönemi izleyen ayın 19 uncu günü 
                #Gönderim Yöntemi:  Web Servis
                #İlişkili Olduğu Tablolar: BDS - TABLO - 07
                #Ölçü birimi kilogram’dır
                #İstasyonsuz  bayilere  ilişkin  BDS - TABLO - 07   formları  bildirilirken  dikkate  alınması  gereken 
                #tablodur.
                #Dış satış bulunmasa da dış satış yapılmadığının beyan edilmesi açısından "0" değeri girilmelidir

                EPDK_TABLO12                 = '''
                                                CREATE TABLE EPDK_TABLO12(
                                                ID_TABLO12 integer primary key autoincrement NOT NULL,
                                                DONEM integer NOT NULL,
                                                LISANS_NO text NOT NULL  references P_BAYI(BAYI_LISANS_NO),
                                                ID_YAKIT integer NOT NULL references YAKIT ( ID_YAKIT ),
                                                TANKER_PLAKA text NOT NULL,
                                                POMPA_SATIS real DEFAULT 0 NOT NULL,
                                                IST_YAPILAN_DOLUM real DEFAULT 0 NOT NULL,
                                                ACIKLAMA integer references EPDK_TABLO8(ID_ACIKLAMA_KODU),
                                                OLUSTURMA_TARIHI datetime DEFAULT (datetime('now','localtime')) NOT NULL,
                                                WEBE_ILETIM_TARIHI datetime
                                                );
                                         ''',
                #Tanker Plaka No: Tarımsal satış amaçlı tanker plaka numarası
                #Pompa Satış: İlgili ay içerisinde pompadan yapılan akaryakıt satış miktarı
                #İstasyondan Yapılan Dolum Miktarı:  İlgili ay içerisinde istasyondan tarımsal satış amaçlı tankere 
                #ikmal edilen akaryakıt miktarı
                #Periyot: Aylık
                #Son Gönderim Zamanı: Rapora esas dönemi izleyen ayın 4 üncü günü
                #Gönderim Yöntemi: Web servis
                #İlişkili Olduğu Tablolar: T1, T2, T3
                #Tarımsal amaçlı satış tankerleri verisinin düzeltilmiş olarak özet bir biçimde gönderileceği tablo. A y 
                #içerisinde tankerde hatalı ölçüm, verilerin alınamaması vb. sebeplerle Tablo 2’ye hatalı yansımış veya 
                #yansımamış verilerin düzeltilerek gönderileceği tablodur. Tablo 2’de gün bazlı olan elektronik veriler 
                #T12’de kümülatif olarak verilecektir.

                EPDK_TABLO13                    = '''
                                                CREATE TABLE EPDK_TABLO13(
                                                ID_TABLO13 integer primary key autoincrement NOT NULL,
                                                DONEM integer NOT NULL,
                                                LISANS_NO text NOT NULL  references P_BAYI(BAYI_LISANS_NO),
                                                KOY_POMPA_NO integer,
                                                ID_YAKIT integer NOT NULL references YAKIT ( ID_YAKIT ),
                                                TANK_DONEM_BASI real DEFAULT 0 NOT NULL,
                                                TANK_DOLUM real DEFAULT 0 NOT NULL,
                                                POMPA_SATIS real DEFAULT 0 NOT NULL,
                                                TANK_DONEM_SONU real DEFAULT 0 NOT NULL,
                                                IST_YAPILAN_DOLUM real DEFAULT 0 NOT NULL,
                                                ACIKLAMA integer references EPDK_TABLO8(ID_ACIKLAMA_KODU),
                                                OLUSTURMA_TARIHI datetime DEFAULT (datetime('now','localtime')) NOT NULL,
                                                WEBE_ILETIM_TARIHI datetime
                                                );
                                         ''',
                
                #Dönem:  Aylık
                #Bayi  Lisans  No:  Dağıtıcı  lisansı  sahibi  tarafından  bayilik  lisans  numarasına  ek  getirilerek  oluşan numara
                #Köy Pompası No:  Bayinin lisansına derç edilmiş köy pompaları için Kurum tarafından verilmiş olan belirleyici numara
                #Akaryakıt Türü: Bayiler tarafından satılabilecek, mevzuatın izin verdiği akaryakıt türü
                #Dönem Başı Stok:  Ayın başlangıcında tanklarda bulunan akaryakıt miktarı
                #Tanka Dolum:  Ay içerisinde tanklara dolumu yapılan akaryakıt miktarı
                #Pompa Satış:  Ay içerisinde pompadan yapılan akaryakıt satış miktarı
                #Dönem Sonu Stok:  Ay sonu itibarı ile tanklarda bulunan akaryakıt miktarı
                #İstasyondan Yapılan  Dolum  Miktarı:  İlgili  ay  içerisinde  akaryakıt  istasyonundan  köy  pompasına
                #ikmal edilen akaryakıt miktarını
                #Periyot: Aylık
                #Son Gönderim Zamanı: Rapora esas dönemi izleyen ayın 4 üncü günü
                #Gönderim Yöntemi: Web servis
                #İlişkili Olduğu Tablolar: T1, T2, T3
                #A y  içerisinde  köy  pompasında  hatalı  ölçüm,  verilerin  alınamaması  vb.  sebeplerle  Tablo  3’e  hatalı 
                #yansımış veya yansımamış verilerin düzeltilerek gönderileceği tablodur.
                
                EPDK_TABLO14                    = '''
                                                CREATE TABLE EPDK_TABLO14(
                                                ID_TABLO14 integer primary key autoincrement NOT NULL,
                                                BELGE_TANZIM_TAR datetime NOT NULL,
                                                LISANS_NO text NOT NULL  references P_BAYI(BAYI_LISANS_NO),
                                                ID_YAKIT integer NOT NULL references YAKIT ( ID_YAKIT ),
                                                DAG_SAT_MIKTARI real NOT NULL,
                                                DAG_SEVK_IRS_NO text NOT NULL,
                                                ACIKLAMA integer references EPDK_TABLO8(ID_ACIKLAMA_KODU),
                                                OLUSTURMA_TARIHI datetime DEFAULT (datetime('now','localtime')) NOT NULL,
                                                WEBE_ILETIM_TARIHI datetime
                                                );
                                         ''',
                #Belge Tanzim Tarihi: Dağıtıcının satış esnasında bayisine düzenlediği belgenin tarihi
                #Dağıtıcı  Satış  Miktarı:  Dağıtıcının  bayiye  kestiği  her  bir  sevk  irsaliyesinde  o  akaryakıt  türü  için 
                #belirtilen miktar
                #Dağıtıcı  Sevk  İrsaliye  No:  Dağıtıcının  satış  esnasında  bayisine  düzenlediği  sevk  irsaliyesinin 
                #numarası
                #Periyot: Oluşuma Bağlı
                #Son Gönderim Zamanı: Belge tanzim tarihinden itibaren 2 gün içerisinde
                #Gönderim Yöntemi:  Web Servis 
                #İlişkili Olduğu Tablolar: T5, T6, T7, T18
                #Dağıtıcı  lisansı  sahipleri  tarafından  bayisine  yapılan  tüm  satışların  düzenli  olarak  bildirilmesi 
                #gerekmektedir.
                EPDK_TABLO15                    = '''
                                                CREATE TABLE EPDK_TABLO15(
                                                ID_TABLO15 integer primary key autoincrement NOT NULL,
                                                LISANS_NO text NOT NULL  references P_BAYI(BAYI_LISANS_NO),
                                                BAYI_ENLEM real NOT NULL,
                                                BAYI_BOYLAM real NOT NULL,
                                                BAYI_TANK_SAYISI integer NOT NULL,
                                                BAYI_TOPLAM_KAPASITE real NOT NULL,
                                                BAYI_POMPA_SAYISI integer NOT NULL,
                                                BAYI_TABANCA_SAYISI integer NOT NULL,
                                                BAYI_MARKA text,
                                                OLUSTURMA_TARIHI datetime DEFAULT (datetime('now','localtime')) NOT NULL,
                                                WEBE_ILETIM_TARIHI datetime
                                                );
                                         ''',
                #Enlem: Bayinin coğrafi enlemi
                #Boylam: Bayinin coğrafi boylamı
                #Tank Sayısı: Bayinin toplam tank sayısı
                #Toplam Kapasite: Bayinin litre cinsinden toplam kapasitesi
                #Pompa Sayısı: Bayinin toplam pompa sayısı
                #Tabanca Sayısı: Bayinin toplam tabanca sayısı
                #Marka: İstasyonun faaliyet gösterdiği dağıtıcının ticari markası
                #Periyot: Oluşuma Bağlı
                #Son Gönderim Zamanı: Yok
                #Gönderim Yöntemi:  Petrol Piyasası Bilgi Sistemi, Web Servis 
                #İlişkili Olduğu Tablolar: -Coğrafi enlem ve boylam ondalık derece cinsinden girilecektir. Örn. EPDK [+39.89331,+ 32.81113]
                #Söz konusu Tablo Petrol Piyasası Bilgi Sistemi üzerinden de bildirilmelidir.
                EPDK_TABLO16                    = '''
                                                CREATE TABLE EPDK_TABLO16(
                                                ID_TABLO16 integer primary key autoincrement NOT NULL,
                                                LISANS_NO text NOT NULL  references P_BAYI(BAYI_LISANS_NO),
                                                KOY_POMPASI_NO integer NOT NULL,
                                                KOY_POMPASI_ENLEM real NOT NULL,
                                                KOY_POMPASI_BOYLAM real NOT NULL,
                                                KOY_POMPASI_TANK_SAYISI integer NOT NULL,
                                                KOY_POMPASI_TOPLAM_KAPASITE real NOT NULL,
                                                KOY_POMPASI_POMPA_SAYISI integer NOT NULL,
                                                KOY_POMPASI_TABANCA_SAYISI integer NOT NULL,
                                                OLUSTURMA_TARIHI datetime DEFAULT (datetime('now','localtime')) NOT NULL,
                                                WEBE_ILETIM_TARIHI datetime
                                                );
                                         ''',
                #Bayi Lisans No: Bayilik lisans numarası
                #Köy Pompası No:  Bayinin lisansına derç edilmiş köy pompaları için EPDK tarafından verilmiş olan 
                #belirleyici numara
                #Enlem: Bayinin köy pompasının coğrafi enlemi
                #Boylam: Bayinin köy pompasının coğrafi boylamı
                #Tank Sayısı: Bayinin köy pompasının toplam tank sayısı
                #Toplam Kapasite: Bayinin köy pompasının litre cinsinden toplam kapasitesi
                #Pompa Sayısı: Bayinin köy pompasının toplam pompa sayısı
                #Tabanca Sayısı: Bayinin köy pompasının toplam tabanca sayısı
                #Periyot: Oluşuma Bağlı
                #Son Gönderim Zamanı: Yok 
                #Gönderim Yöntemi:  Petrol Piyasası Bilgi Sistemi, Web Servis 
                EPDK_TABLO17                    = '''
                                                CREATE TABLE EPDK_TABLO17(
                                                ID_TABLO17 integer primary key autoincrement NOT NULL,
                                                LISANS_NO text NOT NULL  references P_BAYI(BAYI_LISANS_NO),
                                                TANKER_PLAKA_NO text NOT NULL,
                                                TOPLAM_KAPASITE real NOT NULL,
                                                OLUSTURMA_TARIHI datetime DEFAULT (datetime('now','localtime')) NOT NULL,
                                                WEBE_ILETIM_TARIHI datetime
                                                );
                                         ''',
                                         
                #Bayi Lisans No: Bayilik lisans numarası
                #Tanker Plaka No: Tarımsal satış amaçlı tanker plaka numarası
                #Toplam Kapasite: Tarımsal satış amaçlı tankerin toplam kapasitesi
                #Periyot: Oluşuma Bağlı
                #Son Gönderim Zamanı: Yok
                #Gönderim Yöntemi:  Petrol Piyasası Bilgi Sistemi, Web Servis 
                #İlişkili Olduğu Tablolar: -Söz konusu Tablo Petrol Piyasası Bilgi Sistemi üzerinden de bildirilmelidir.
                
                EPDK_TABLO18                    = '''
                                                CREATE TABLE EPDK_TABLO18(
                                                ID_TABLO18 integer primary key autoincrement NOT NULL,
                                                LISANS_NO text NOT NULL  references P_BAYI(BAYI_LISANS_NO),
                                                DOLUM_TARIHI datetime NOT NULL,
                                                ID_YAKIT integer NOT NULL references YAKIT ( ID_YAKIT ),
                                                DOLUM_MIKTARI real DEFAULT 0 NOT NULL,
                                                SATIS_BELGESI_NO text NOT NULL,
                                                SATIS_BELGESI_TARIH datetime NOT NULL,
                                                SAT_BEL_URUN_MIK real NOT NULL,
                                                IADE_BAKIM_TRANSFER text NOT NULL,
                                                IKMAL_DURUMU text NOT NULL,
                                                ACIKLAMA integer references EPDK_TABLO8(ID_ACIKLAMA_KODU),
                                                OLUSTURMA_TARIHI datetime DEFAULT (datetime('now','localtime')) NOT NULL,
                                                WEBE_ILETIM_TARIHI datetime
                                                );
                                         '''
                                
                #Dolum Tarihi: Dolumun gerçekleştirildiği gün.
                #Dolum  Miktarı:  00:00-23:59  arası  tanklara  dolumu  yapılan  akaryakıt  miktarı(Birden  fazla  satış 
                #belgesine ait dolum olması durumunda her bir satış belgesine ait dolum ayrı ayrı belirtilecektir.)
                #Satış Belgesi No:  Dolum ile eşleşen, dağıtıcının satış esnasında bayisine düzenlediği satış belgesinin 
                #numarası.
                #Satış Belgesi Tarihi: Satış belgesinin tarihi.
                #Satış Belgesindeki Ürün Miktarı: Doluma ait olan satış belgesindeki akaryakıt miktarı.
                #İade faturası, tank bakım işlemleri, istasyon-köy pompası-tarımsal satış amaçlı tanker arasında 
                #transfer işlemi var mı?  Evet / Hayır (Ödeme kaydedici cihaz fişi düzenlenerek istasyondan çıkarılan 
                #ancak  tamamı  veya  bir  kısmı  satılamadan  tekrar  akaryakıt  istasyonu  tankına  doldurulan  akaryakıt, 
                #tank bakım işlemleri kapsamında tanktan çıkartılarak tekrar tanka doldurulan akaryakıt, bayinin köy 
                #pompası ve/veya tarımsal satış amaçlı tankeri bulunması durumunda ilgili yerler arasında akaryakıt 
                #transferi  bulunmakta  ise  söz  konusu  dolumun  bu  hareketlerden  kaynaklanıp  kaynaklanmadığına 
                #incelenmelidir.)
                #Akaryakıt  İkmali  Durduruldu  mu?  Evet  /  Hayır  (Eşleştirme  ve  inceleme  sonucunda  kayıt  dışı 
                #ikmal ve/veya satış yapıldığının anlaşılması durumda "Evet", aksi takdirde "Hayır" girilmelidir.
                #Açıklama:  Açıklama, varsa iade faturası numarası.
                #Periyot: Oluşuma Bağlı
                #Son Gönderim Zamanı: Dolum tarihinden itibaren 2 iş günü
                #Gönderim Yöntemi:  Web Servis 
                #İlişkili Olduğu Tablolar: T1, T6, T14
                #Önemli:  Dolumun  yapıldığı  tarihte  istasyon  otomasyon  sisteminde  arıza  bulunması  durumunda 
                #dolum  eşleştirmesi  arızanın  giderildiği  tarihten  itibaren  2  iş  günü  içerisinde  Kurumun  erişimine 
                #sağlanmalıdır.
                #Dağıtıcı lisansı sahipleri akaryakıt istasyonuna yapılan dolumları günlük olarak takip etmelidir. Gün 
                #içerisinde  yapılan  dolumlar  irsaliyeler  ile  eşleştirilerek  T18’e  yansıtılmalıdır.  T18’de  eşleşmeme 
                #durumu  oluşması  durumunda,  eğer  kayıt  dışı  ikmal  ve/veya  satış  yapıldığı  anlaşılıyor  ise  bayiye 
                #akaryakıt  ikmali  durdurulmalıdır. Ardından  1240  sayılı  Kurul  Kararında  belirlenen  usul  ve  esaslar 
                #takip edilmelidir. 
                #Akaryakıt  istasyonunda  yapılan  tüm  dolumların  T18’de  eşleştirilmesi  gerekmektedir.  Akaryakıt 
                #istasyonundaki  dolum,  Tablo  1’e  yansımasa  veya  tam  miktarı  tespit  edilemese  bile,  Tablo  18’de 
                #eşleştirilmesi gerekmektedir. 
                #ÖRNEK 3. 
                #Dolum Tarihi: Mayıs 15, 
                #Tablo 1’de yer alan dolum miktarı: 10.000 litre, 
                #15  Mayıs  sonrasında  tam  dolum  miktarının  9.800  litre  olduğunun  anlaşılması  durumunda  T18’de 
                #10.000  litre  üzerinden  eşleştirme  yapılmalı  açıklama  kısmında  eksik/fazla  olan  miktara  ilişkin 
                #açıklama yapılmalıdır
                
)
db_object_view_dict = dict(
        VIEW_PROBE =  '''
                                CREATE VIEW VIEW_PROBE AS 
                                SELECT 
                                T2.ID_PROBE,
                                T2.PROBE_MARKA AS ID_PROBE_MARKA,
                                T1.PROBE_MARKA_ADI,
                                T2.PROBE_UZUNLUK,
                                T2.SPEED_OF_WIRE,
                                T2.PROBE_ADRES,
                                T2.ID_ACTIVE AS AKTIF_PROBE
                                FROM P_PROBE_MARKA T1,
                                PROBE T2
                                WHERE 
                                T1.ID_PROBE_MARKA = T2.PROBE_MARKA
                      ''',
	VIEW_VARDIYA_ADI =      '''
                                                CREATE VIEW VIEW_VARDIYA_ADI AS select 
                                                                                T1.ID_VARDIYA_AKTIF,T1.ID_VARDIYA,T1.TARIH,T2.VARDIYA_ADI,T2.ID_ACTIVE,T2.BITIS_SAATI,T2.BASLANGIC_SAATI,
                                                                                T1.VARDIYA_ACILIS_TUTAR, T1.VARDIYA_KAPANIS_TUTAR, T1.VARDIYA_SATIS_SAYISI
                                                                                FROM
                                                                                VARDIYA_AKTIF T1,VARDIYA T2 WHERE T1.ID_VARDIYA=T2.ID_VARDIYA
                                ''',      
	ISKONTO_VIEW           = '''
				CREATE VIEW ISKONTO_VIEW
					AS
					SELECT     ISKONTO.ISKONTO_ADI, ISKONTO_YAKIT.ID_ISKONTO_YAKIT, ISKONTO_YAKIT.ID_ISKONTO, ISKONTO_YAKIT.ID_YAKIT, ISKONTO_YAKIT.ORAN_MIKTAR, 
							      YAKIT.YAKIT_ADI, YAKIT.YAKIT_DURUM, P_ISKONTO_UYG_CINSI.UYG_CINSI_ADI AS UYGULAMA_CINSI_ADI, 
							      P_ISKONTO_UYG_TURU.UYG_TURU_ADI AS UYGULAMA_TURU_ADI
					FROM         ISKONTO INNER JOIN
							      ISKONTO_YAKIT ON ISKONTO.ID_ISKONTO = ISKONTO_YAKIT.ID_ISKONTO AND ISKONTO.ID_ISKONTO = ISKONTO_YAKIT.ID_ISKONTO INNER JOIN
							      YAKIT ON ISKONTO_YAKIT.ID_YAKIT = YAKIT.ID_YAKIT INNER JOIN
							      P_ISKONTO_UYG_TURU ON ISKONTO_YAKIT.ID_UYG_TURU = P_ISKONTO_UYG_TURU.ID_UYG_TURU INNER JOIN
							      P_ISKONTO_UYG_CINSI ON ISKONTO_YAKIT.ID_UYG_CINSI = P_ISKONTO_UYG_CINSI.ID_UYG_CINSI
					''',
	VIEW_YAKIT_HISTORY      = '''
				CREATE VIEW VIEW_YAKIT_HISTORY
					AS
					SELECT    t1.YAKIT_ADI, t1.YAKIT_BIRIM_FIYAT, t2.*
					FROM         YAKIT t1, 
								 YAKIT_HISTORY t2
					WHERE t1.ID_YAKIT = t2.ID_YAKIT
				''',
	VIEW_YAKIT_TANKI        = '''
				CREATE VIEW VIEW_YAKIT_TANKI
					AS
					SELECT     YAKIT_TANKI.ID_YAKIT_TANKI, YAKIT_TANKI.YAKIT_TANKI_ADI, YAKIT_TANKI.YAKIT_TANKI_YUKSEKLIK, YAKIT_TANKI.YAKIT_TANKI_KAPASITE, YAKIT_TANKI.ID_PROBE, 
							      P_PROBE_MARKA.ID_PROBE_MARKA,P_PROBE_MARKA.PROBE_MARKA_ADI AS PROBE_MARKA, PROBE.PROBE_UZUNLUK, PROBE.PROBE_ADRES, YAKIT_TANKI.ID_PORT, YAKIT_TANKI.ID_YAKIT, YAKIT_TANKI.PROBE_BASLANGIC_SEVIYE, 
							      PORTLAR.PORT_ADI, PROBE.SPEED_OF_WIRE, YAKIT.YAKIT_ADI
					FROM         YAKIT_TANKI INNER JOIN
							      PROBE ON YAKIT_TANKI.ID_PROBE = PROBE.ID_PROBE INNER JOIN
							      P_PROBE_MARKA ON P_PROBE_MARKA.ID_PROBE_MARKA = PROBE.PROBE_MARKA INNER JOIN
							      PORTLAR ON YAKIT_TANKI.ID_PORT = PORTLAR.ID_PORT INNER JOIN
							      YAKIT ON YAKIT_TANKI.ID_YAKIT = YAKIT.ID_YAKIT
					WHERE     YAKIT_TANKI.ID_ACTIVE = 1
				''',
	CARI_LISTE_VIEW         ='''CREATE VIEW CARI_LISTE_VIEW AS 
					SELECT T1.*,T2.ilce,T3.sehir,T4.CARI_TIP_ADI FROM CARI_HESAPLAR T1
					LEFT OUTER JOIN P_ILCELER T2 ON T1.ID_ILcE = T2.id
					LEFT OUTER JOIN P_ILLER T3 ON T1.ID_IL = T3.id,
					P_CARI_TIPI T4
					WHERE 
					T1.ID_CARI_TIPI = T4.ID_CARI_TIPI
				''',
        KART_LISTE_VIEW           ='''
                                CREATE VIEW KART_LISTE_VIEW AS 
					SELECT T2.*,T1.CARI_ADI,T3.ISKONTO_ADI,T4.CARI_TIP_ADI,T5.*,T6.* FROM CARI_HESAPLAR T1
					LEFT OUTER JOIN KARTLAR T2 ON T1.ID_CARI_HESAP = T2.ID_CARI_HESAP
					LEFT OUTER JOIN ISKONTO T3 ON T2.ID_ISKONTO = T3.ID_ISKONTO
                                        LEFT OUTER JOIN P_CARI_TIPI T4 ON T2.ID_CARI_TIPI=T4.ID_CARI_TIPI
                                        LEFT OUTER JOIN P_LIMIT_CESIT T5 ON T2.LIMIT_TUR=T5.ID_LIMIT_CESIT
                                        LEFT OUTER JOIN P_LIMIT_DONEM T6 ON T2.H_DONEMI=T6.ID_LIMIT_DONEM
                                ''',
        ARAC_LISTE_VIEW           ='''
                                CREATE VIEW ARAC_LISTE_VIEW AS 
					SELECT T2.*,T1.CARI_ADI,T5.YAKIT_ADI,T6.KIT_TIPI_ADI FROM CARI_HESAPLAR T1
					LEFT OUTER JOIN ARACLAR T2 ON T1.ID_CARI_HESAP = T2.ID_CARI_HESAP
                                        LEFT OUTER JOIN YAKIT T5 ON T2.ID_YAKIT=T5.ID_YAKIT
                                        LEFT OUTER JOIN P_KIT_TIPI T6 ON T2.ID_KIT_TIPI=T6.ID_KIT_TIPI
                                ''',
        SERVIS_VIEW               ='''
                                CREATE VIEW SERVIS_VIEW AS
                                SELECT T1.*,T2.HATA_ACIKLAMA,T3.DURUM_ACIKLAMA,T4.BAYI_LISANS_NO,
                                T4.BAYI_TANK_SAYISI,T4.BAYI_POMPA_SAYISI
                                FROM SERVIS T1
                                LEFT OUTER JOIN P_HATA_CESITLERI T2 ON T1.ID_HATA = T2.ID_HATA
                                LEFT OUTER JOIN P_DURUM T3 ON T1.ID_DURUM=T3.ID_DURUM
                                LEFT OUTER JOIN P_BAYI T4 ON T1.ID_BAYI=T4.ID_BAYI
                                ''',
        VIEW_POMPA_TABANCALAR   = '''
                                CREATE VIEW VIEW_POMPA_TABANCALAR AS
                                                SELECT T1.ID_POMPA AS ID_POMPA, T1.POMPA_ADI AS POMPA_ADI,
						T2.POMPA_NO AS POMPA_NO,T2.BEYIN_NO AS BEYIN_NO, T5.ARABIRIM_MARKA_ADI AS ARABIRIM_MARKASI,T5.ID_ARABIRIM_MARKA,
                                                T1.ARABIRIM_ADRESI AS ARABIRIM_ADRESI,T3.PORT_ADI AS ARABIRIM_PORT_ADI,T3.ID_PORT,
                                                T2.ID_TABANCA AS ID_TABANCA,T2.TABANCA_NO AS TABANCA_NO,T2.ID_YAKIT_TANKI AS ID_YAKIT_TANKI,
                                                T4.YAKIT_TANKI_ADI AS YAKIT_TANKI_ADI,T6.BASAMAK_KAYDIRMA_ADI AS BASAMAK_KAYDIRMA_ADI, T7.YAKIT_ADI,T7.ID_YAKIT,
                                                T8.KART_OKUYUCU_ADRES, T8.KART_OKUYUCU_MARKA, T8.ID_PORT AS KART_OKUYUCU_PORT, T8.ID_KART_OKUYUCU ,T7.YAKIT_BIRIM_FIYAT

                                                FROM POMPA T1,P_BASAMAK_KAYDIRMA T6,YAKIT T7,TABANCALAR T2                                               
                                                LEFT OUTER JOIN PORTLAR T3 ON T1.ID_PORT = T3.ID_PORT
                                                LEFT OUTER JOIN YAKIT_TANKI T4 ON T2.ID_YAKIT_TANKI = T4.ID_YAKIT_TANKI
                                                LEFT OUTER JOIN P_ARABIRIM_MARKALAR T5 ON T1.ARABIRIM_MARKASI = T5.ID_ARABIRIM_MARKA
                                                LEFT OUTER JOIN KART_OKUYUCU T8 ON T1.ID_POMPA = T8.ID_POMPA AND T2.POMPA_NO = T8.POMPA_NO
                                                WHERE
                                                T1.ID_POMPA = T2.ID_POMPA AND
                                                T2.ID_BASAMAK_KAYDIRMA = T6.ID_BASAMAK_KAYDIRMA AND
						T4.ID_YAKIT = T7.ID_YAKIT


                                                ORDER BY T1.POMPA_ADI,T2.BEYIN_NO,T2.TABANCA_NO
                                    ''',
        
	VIEW_TANK_HRKT_RAPOR    =  '''
						CREATE VIEW VIEW_TANK_HRKT_RAPOR AS
                                                                                SELECT T5.ID_YAKIT_TANKI,T5.YAKIT_TANKI_ADI,T1.TARIH,T4.YAKIT_ADI,T1.OKUNAN_YAKIT,
                                                                                T1.OKUNAN_SU,T1.TANK_DOLUMU,T2.H_ACIKLAMA,T1.ID_TANK_KAYITLARI,T1.POMPA_SATISI,T2.ID_THC,T1.GUN_SONU,
                                                                                ( CASE WHEN T1.MANUAL_DOLUM = 1 THEN 'Manuel Dolum' ELSE 'Otomatik Dolum' END ) AS DOLUM_TIPI
                                                                                FROM
                                                                                TANK_KAYITLARI T1,P_TANK_H_CESIT T2,YAKIT T4,YAKIT_TANKI T5
                                                                                WHERE
                                                                                T1.TANK_ID=T5.ID_YAKIT_TANKI AND T1.ID_YAKIT=T4.ID_YAKIT AND T1.KAYIT_KODU=T2.ID_THC
	
	''',
        VIEW_POMPA_SATIS_RAPOR    =  '''
						CREATE VIEW VIEW_POMPA_SATIS_RAPOR AS
                                                                                SELECT T1.ID_SATIS,T1.ID_YAKIT,T2.YAKIT_ADI,T1.POMPA_NO, T1.POMPA_ADI,T1.BEYIN_NO,T1.ID_TABANCA,T5.TABANCA_NO,T1.TARIH,T1.SATIS_LITRE,T1.ID_MUSTERI_CARI,T1.ID_VARDIYA_AKTIF,
                                                                                T1.BIRIM_FIYAT,T1.TUTAR,T1.PLAKA,T3.ID_ODEME,T3.ODEME_SEKLI,T1.ID_CARI_HESAP,T4.CARI_ADI,T5.ID_YAKIT_TANKI,T6.YAKIT_TANKI_ADI,T1.DURUM,T7.CARI_ADI AS MUSTERI_CARI_ADI
                                                                                FROM
                                                                                YAKIT T2,P_ODEME_SEKLI T3,TABANCALAR T5,YAKIT_TANKI T6,
                                                                                POMPA_SATIS_KAYIT T1 LEFT OUTER JOIN CARI_HESAPLAR T4 ON T1.ID_CARI_HESAP=T4.ID_CARI_HESAP
                                                                                LEFT OUTER JOIN CARI_HESAPLAR T7 ON T1.ID_MUSTERI_CARI=T7.ID_CARI_HESAP
                                                                                WHERE
                                                                                T1.ODEME_SEKLI=T3.ID_ODEME AND T1.ID_YAKIT=T2.ID_YAKIT AND T1.ID_TABANCA=T5.ID_TABANCA AND T5.ID_YAKIT_TANKI =T6.ID_YAKIT_TANKI

	''',
	
        VIEW_VARDIYA_RAPOR    =  '''
						CREATE VIEW VIEW_VARDIYA_RAPOR AS
                                                                                SELECT T1.ID_SATIS,T1.ID_YAKIT,T2.YAKIT_ADI,T1.POMPA_NO, T1.POMPA_ADI,T1.BEYIN_NO,T1.ID_TABANCA,T5.TABANCA_NO,T1.TARIH,T1.SATIS_LITRE,
										T1.BIRIM_FIYAT,T1.TUTAR,T1.PLAKA,T3.ID_ODEME,T3.ODEME_SEKLI,T1.ID_CARI_HESAP,T5.ID_YAKIT_TANKI,T6.YAKIT_TANKI_ADI,T1.DURUM,T4.CARI_ADI,T8.VARDIYA_ADI,T8.ID_VARDIYA_AKTIF,
                                                                                T8.VARDIYA_ACILIS_TUTAR, T8.VARDIYA_KAPANIS_TUTAR, T8.VARDIYA_SATIS_SAYISI, ( T8.VARDIYA_KAPANIS_TUTAR - T8.VARDIYA_ACILIS_TUTAR ) AS VARDIYA_EDEKS_TOPLAM
										FROM
										POMPA_SATIS_KAYIT T1  LEFT OUTER JOIN CARI_HESAPLAR T4 ON T1.ID_CARI_HESAP=T4.ID_CARI_HESAP,YAKIT T2,P_ODEME_SEKLI T3,TABANCALAR T5,YAKIT_TANKI T6,VIEW_VARDIYA_ADI T8
										WHERE
										T1.ODEME_SEKLI=T3.ID_ODEME AND 
										T1.ID_YAKIT=T2.ID_YAKIT AND 
										T1.ID_TABANCA=T5.ID_TABANCA AND 
										T5.ID_YAKIT_TANKI =T6.ID_YAKIT_TANKI AND 
										T1.ID_VARDIYA_AKTIF = T8.ID_VARDIYA_AKTIF

	'''
       
	
)

db_object_trigger_dict=dict(
                toplamlar_epdktablo1 =
                                '''
                                
					CREATE TRIGGER toplamlar_epdktablo1 AFTER INSERT ON EPDK_TABLO1
					BEGIN
						UPDATE EPDK_TABLO1 SET TANK_DOLUM =
                                                                              (SELECT (CASE WHEN sum(TANK_DOLUMU) is NULL THEN 0 ELSE sum(TANK_DOLUMU) END) AS total_dolum
                                                                                from TANK_KAYITLARI WHERE strftime('%d/%m/%Y', TARIH)=strftime('%d/%m/%Y', date(('now'),'-1 day')) and ID_YAKIT=NEW.ID_YAKIT),    
						
									POMPA_SATIS =
									      (SELECT  (CASE WHEN SUM(SATIS_LITRE) IS NULL THEN 0 ELSE SUM(SATIS_LITRE) END) AS total_litre
										from POMPA_SATIS_KAYIT
										WHERE strftime('%d/%m/%Y', TARIH)=strftime('%d/%m/%Y', date(('now'),'-1 day')) AND ID_YAKIT=NEW.ID_YAKIT )
						WHERE ID_TABLO1=NEW.ID_TABLO1;
					END;        
                                ''',
                KART_RELATION =
                '''
                CREATE TRIGGER KART_RELATION
                                AFTER INSERT ON KARTLAR
                                	BEGIN
                                            update KARTLAR set ID_ISKONTO = ( select ID_ISKONTO from ISKONTO where ID_ISKONTO_WEB = new.ID_ISKONTO_WEB )  where ID_ISKONTO_WEB = new.ID_ISKONTO_WEB  ;
                                            update KARTLAR set ID_CARI_HESAP = ( select ID_CARI_HESAP from CARI_HESAPLAR where ID_CARI_HESAP_WEB = new.ID_CARI_HESAP_WEB )  where ID_CARI_HESAP_WEB = new.ID_CARI_HESAP_WEB  ;
                                	END;
                ''',
                KART_RELATION_UPDATE =
                '''
                CREATE TRIGGER KART_RELATION_UPDATE
                                AFTER UPDATE ON KARTLAR
                                	BEGIN
                                            update KARTLAR set ID_ISKONTO = ( select ID_ISKONTO from ISKONTO where ID_ISKONTO_WEB = new.ID_ISKONTO_WEB )  where ID_ISKONTO_WEB = new.ID_ISKONTO_WEB  ;
                                            update KARTLAR set ID_CARI_HESAP = ( select ID_CARI_HESAP from CARI_HESAPLAR where ID_CARI_HESAP_WEB = new.ID_CARI_HESAP_WEB )  where ID_CARI_HESAP_WEB = new.ID_CARI_HESAP_WEB  ;
                                	END;
                ''',
                ISKONTO_RELATION =
                '''
                CREATE TRIGGER ISKOTO_RELATION 
                                AFTER INSERT ON ISKONTO_YAKIT
                                                BEGIN
                                                                update ISKONTO_YAKIT set ID_ISKONTO = ( select ID_ISKONTO from ISKONTO where ID_ISKONTO_WEB = new.ID_ISKONTO_WEB )  where ID_ISKONTO_WEB = new.ID_ISKONTO_WEB  ;
                                                END;

                ''',
                ISKONTO_RELATION_UPDATE =
                '''
                CREATE TRIGGER ISKOTO_RELATION_UPDATE 
                                AFTER UPDATE ON ISKONTO_YAKIT
                                                BEGIN
                                                                update ISKONTO_YAKIT set ID_ISKONTO = ( select ID_ISKONTO from ISKONTO where ID_ISKONTO_WEB = new.ID_ISKONTO_WEB )  where ID_ISKONTO_WEB = new.ID_ISKONTO_WEB  ;
                                                END;

                ''',
                ARACLAR_RELATION =
                '''
                CREATE TRIGGER ARACLAR_RELATION 
                                AFTER INSERT ON ARACLAR
                                                BEGIN
                                                                update ARACLAR set ID_CARI_HESAP = ( select ID_CARI_HESAP from CARI_HESAPLAR where ID_CARI_HESAP_WEB = new.ID_CARI_HESAP_WEB )  where ID_CARI_HESAP_WEB = new.ID_CARI_HESAP_WEB  ;
                                                END;

                ''',
                ARACLAR_RELATION_UPDATE =
                '''
                CREATE TRIGGER ARACLAR_RELATION_UPDATE 
                                AFTER UPDATE ON ARACLAR
                                                BEGIN
                                                                update ARACLAR set ID_CARI_HESAP = ( select ID_CARI_HESAP from CARI_HESAPLAR where ID_CARI_HESAP_WEB = new.ID_CARI_HESAP_WEB )  where ID_CARI_HESAP_WEB = new.ID_CARI_HESAP_WEB  ;
                                                END;

                ''',
                ARACLAR_PASIF_UPDATE = 
                '''
                CREATE TRIGGER ARACLAR_PASIF_UPDATE 
                                AFTER UPDATE ON ARACLAR
                                WHEN new.ID_ACTIVE = 2
                                BEGIN
                                     update KARTLAR set ID_ACTIVE = 2 WHERE PLAKA = new.PLAKA;       
                                END
                ''',
                CARI_HESAPLAR_UPDATE =
                '''
                CREATE TRIGGER CARI_HESAPLAR_UPDATE 
                                AFTER UPDATE ON CARI_HESAPLAR
                                WHEN new.ID_ACTIVE = 2
                                BEGIN
                                                update ARACLAR set ID_ACTIVE = 2 WHERE ID_CARI_HESAP = new.ID_CARI_HESAP;
                                                update KARTLAR set ID_ACTIVE = 2 WHERE ID_CARI_HESAP = new.ID_CARI_HESAP;
                                END
                '''

                
               
)

db_object_values  = dict(
	P_ACTIVE = [
		("insert into P_ACTIVE ( ID_ACTIVE,ACTIVE_NAME ) values (1,'%s');"%MESSAGES.aktif),
		("insert into P_ACTIVE ( ID_ACTIVE,ACTIVE_NAME ) values (2,'%s');"%MESSAGES.pasif),
		("insert into P_ACTIVE ( ID_ACTIVE,ACTIVE_NAME ) values (3,'%s');"%MESSAGES.silindi)
	],
	P_ISKONTO_UYG_CINSI = [
		("insert into P_ISKONTO_UYG_CINSI ( ID_UYG_CINSI,UYG_CINSI_ADI ) values  (1,'%s');"%MESSAGES.miktar),
		("insert into P_ISKONTO_UYG_CINSI ( ID_UYG_CINSI,UYG_CINSI_ADI ) values (2,'%s');"%MESSAGES.yuzde)
	],
	P_ISKONTO_UYG_TURU = [
		("insert into P_ISKONTO_UYG_TURU ( ID_UYG_TURU,UYG_TURU_ADI ) values (1,'%s');"%MESSAGES.artirim),
		("insert into P_ISKONTO_UYG_TURU ( ID_UYG_TURU,UYG_TURU_ADI ) values (2,'%s');"%MESSAGES.indirim)
	],
        P_LIMIT_CESIT = [
		("insert into P_LIMIT_CESIT ( ID_LIMIT_CESIT,LIMIT_CESIT ) values (1,'%s');"%MESSAGES.tutar),
		("insert into P_LIMIT_CESIT ( ID_LIMIT_CESIT,LIMIT_CESIT ) values (2,'%s');"%MESSAGES.litre)
	],
        P_LIMIT_DONEM = [
		("insert into P_LIMIT_DONEM ( ID_LIMIT_DONEM,LIMIT_DONEM ) values (1,'%s');"%MESSAGES.gunluk),
		("insert into P_LIMIT_DONEM ( ID_LIMIT_DONEM,LIMIT_DONEM ) values (2,'%s');"%MESSAGES.haftalik),
                ("insert into P_LIMIT_DONEM ( ID_LIMIT_DONEM,LIMIT_DONEM ) values (3,'%s');"%MESSAGES.aylik)
	],
        P_TANK_H_CESIT = [
		("insert into P_TANK_H_CESIT ( ID_THC,H_ACIKLAMA ) values (1,'%s');"%MESSAGES.okunan_deger),
		("insert into P_TANK_H_CESIT ( ID_THC,H_ACIKLAMA ) values (2,'%s');"%MESSAGES.dolum_baslangic),
                ("insert into P_TANK_H_CESIT ( ID_THC,H_ACIKLAMA ) values (3,'%s');"%MESSAGES.dolum_ara_kayit),
                ("insert into P_TANK_H_CESIT ( ID_THC,H_ACIKLAMA ) values (4,'%s');"%MESSAGES.dolum_bitis),
                ("insert into P_TANK_H_CESIT ( ID_THC,H_ACIKLAMA ) values (5,'%s');"%MESSAGES.tt_baslangic),
                ("insert into P_TANK_H_CESIT ( ID_THC,H_ACIKLAMA ) values (6,'%s');"%MESSAGES.tt_ara_kayit),
                ("insert into P_TANK_H_CESIT ( ID_THC,H_ACIKLAMA ) values (7,'%s');"%MESSAGES.tt_bitis),
                ("insert into P_TANK_H_CESIT ( ID_THC,H_ACIKLAMA ) values (8,'%s');"%MESSAGES.dalga_hareketi),
		("insert into P_TANK_H_CESIT ( ID_THC,H_ACIKLAMA ) values (9,'%s');"%MESSAGES.acilis_farki)
	],
        P_ODEME_SEKLI = [
                ("insert into P_ODEME_SEKLI ( ID_ODEME,ODEME_SEKLI ) values (0,'%s');"%MESSAGES.belirsiz),
                ("insert into P_ODEME_SEKLI ( ID_ODEME,ODEME_SEKLI ) values (1,'%s');"%MESSAGES.nakit),
		("insert into P_ODEME_SEKLI ( ID_ODEME,ODEME_SEKLI ) values (2,'%s');"%MESSAGES.kredi_karti),
                ("insert into P_ODEME_SEKLI ( ID_ODEME,ODEME_SEKLI ) values (3,'%s');"%MESSAGES.veresiye)
                ],
        P_ARABIRIM_MARKALAR = [
                "insert into P_ARABIRIM_MARKALAR ( ID_ARABIRIM_MARKA,ARABIRIM_MARKA_ADI ) values (1,'Teosis');",
		"insert into P_ARABIRIM_MARKALAR ( ID_ARABIRIM_MARKA,ARABIRIM_MARKA_ADI ) values (2,'Beko');"
                ],
        P_HATA_CESITLERI = [
		("insert into P_HATA_CESITLERI ( ID_HATA,HATA_ACIKLAMA ) values (1,'%s');"%MESSAGES.tank_seviye_gostermiyor),
		("insert into P_HATA_CESITLERI ( ID_HATA,HATA_ACIKLAMA ) values (2,'%s');"%MESSAGES.tank_seviye_yanlis),
                ("insert into P_HATA_CESITLERI ( ID_HATA,HATA_ACIKLAMA ) values (3,'%s');"%MESSAGES.arabirim_arizali),
                ("insert into P_HATA_CESITLERI ( ID_HATA,HATA_ACIKLAMA ) values (4,'%s');"%MESSAGES.otomasyon_satis_izni_yok),              
	],
        P_DURUM = [
		("insert into P_DURUM ( ID_DURUM,DURUM_ACIKLAMA ) values (1,'%s');"%MESSAGES.merkeze_gonderiliyor),
		("insert into P_DURUM ( ID_DURUM,DURUM_ACIKLAMA ) values (2,'%s');"%MESSAGES.merkeze_iletildi),
                ("insert into P_DURUM ( ID_DURUM,DURUM_ACIKLAMA ) values (3,'%s');"%MESSAGES.isleme_alindi),
                ("insert into P_DURUM ( ID_DURUM,DURUM_ACIKLAMA ) values (4,'%s');"%MESSAGES.servis_yonlendirildi),
                ("insert into P_DURUM ( ID_DURUM,DURUM_ACIKLAMA ) values (5,'%s');"%MESSAGES.cagri_tamamlandi),
                ("insert into P_DURUM ( ID_DURUM,DURUM_ACIKLAMA ) values (6,'%s');"%MESSAGES.cagri_tamamlanamadi),
                
	],
	YAKIT_TR = [
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (1,'Yakıt Nafta',4.5500,0);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (2,'K.Benzin 95',5.1000,1);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (3,'Kat. K.Benzin 95',5.3000,1);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (4,'K.Benzin 98',4.5000,0);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (5,'Gazyağı',3.5000,1);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (6,'Motorin',4.5000,1);",
                "insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (7,'Lpg',4.5000,0);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (11,'Otobiodizel',4.5000,0);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (12,'Yakıtbiodizel',4.5000,0);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (15,'K.Benzin 95(Diğer)',4.5000,0);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (16,'Motorin (Diğer)',4.5000,0);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (17,'Motorin (Diğer 2)',4.5000,0);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (18,'Uçak Benzini',4.5000,0);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (19,'Benzin Tipi Jet Y.',4.5000,0);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (20,'Jet Yakıtı',4.5000,0);",
                "insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (69,'Fuel Oil 4 (Kalorifer Yakıtı)',4.5000,0);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (70,'Fuel Oil',4.5000,0);",
                "insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (71,'Yük. Kükürtlü Fuel Oil',4.5000,0);"
	],
        YAKIT_EN = [
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (1,'Fuel Naphtha',4.5500,0);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (2,'Unl.Gasoline 95',5.1000,1);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (3,'Add. Unl.Gasoline 95',5.3000,1);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (4,'Unl.Gasoline 98',4.5000,0);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (5,'Kerosene',3.5000,1);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (6,'Diesel',4.5000,1);",
                "insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (7,'Lpg',4.5000,0);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (11,'Auto biodiesel',4.5000,0);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (12,'Fuel biodiesel',4.5000,0);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (18,'Aviation Gasoline',4.5000,0);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (20,'Jet Fuel',4.5000,0);",
                "insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (69,'Fuel Oil 4 (Heating Fuel)',4.5000,0);",
		"insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (70,'Fuel Oil',4.5000,0);",
                "insert into YAKIT ( ID_YAKIT,YAKIT_ADI, YAKIT_BIRIM_FIYAT,YAKIT_DURUM ) values (71,'High-Sulfur Fuel Oil',4.5000,0);"
	],
	P_OTOMASYON_PARAMETRE = [
		#"insert into P_OTOMASYON_PARAMETRE ( ID_PARAMETRE,PARAMETRE_ADI,PARAMETRE_DEGER ) values (1,'Tabanca Açılış Fiyat Tazele',1);",
		#"insert into P_OTOMASYON_PARAMETRE ( ID_PARAMETRE,PARAMETRE_ADI,PARAMETRE_DEGER ) values (2,'İndirimli Fiyat Göster',2);",
		#"insert into P_OTOMASYON_PARAMETRE ( ID_PARAMETRE,PARAMETRE_ADI,PARAMETRE_DEGER ) values (3,'Vardiya Sonu Toplamları Hesapla',2);",
		#"insert into P_OTOMASYON_PARAMETRE ( ID_PARAMETRE,PARAMETRE_ADI,PARAMETRE_DEGER ) values (4,'İrsaliyeleri Otomatik Eşleştir',2);",
		#"insert into P_OTOMASYON_PARAMETRE ( ID_PARAMETRE,PARAMETRE_ADI,PARAMETRE_DEGER ) values (5,'Vardiya İşlemlerini Şifre İle Yap',2);",
		#"insert into P_OTOMASYON_PARAMETRE ( ID_PARAMETRE,PARAMETRE_ADI,PARAMETRE_DEGER ) values (6,'Filo Uygulamasını Aktifleştir',2);",
		#"insert into P_OTOMASYON_PARAMETRE ( ID_PARAMETRE,PARAMETRE_ADI,PARAMETRE_DEGER ) values (7,'Otomatik Dolum Algıla',2);",
		#"insert into P_OTOMASYON_PARAMETRE ( ID_PARAMETRE,PARAMETRE_ADI,PARAMETRE_DEGER ) values (8,'Otomatik Endeks Al',2);",
		#"insert into P_OTOMASYON_PARAMETRE ( ID_PARAMETRE,PARAMETRE_ADI,PARAMETRE_DEGER ) values (9,'Kablosuz Haberleşme Aktif',2);",
		"insert into P_OTOMASYON_PARAMETRE ( ID_PARAMETRE,PARAMETRE_ADI,PARAMETRE_DEGER,PARAMETRE_KRITER ) values (10,'Kayıt Defterini Kapat',2,'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\System\DisableRegistryTools');",
		"insert into P_OTOMASYON_PARAMETRE ( ID_PARAMETRE,PARAMETRE_ADI,PARAMETRE_DEGER,PARAMETRE_KRITER ) values (11,'Görev Yöneticisini Kapat',2,'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\System\DisableTaskMgr');",
		"insert into P_OTOMASYON_PARAMETRE ( ID_PARAMETRE,PARAMETRE_ADI,PARAMETRE_DEGER,PARAMETRE_KRITER ) values (12,'Masaüstünü Kapat',2,'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\Shell');",
                "insert into P_OTOMASYON_PARAMETRE ( ID_PARAMETRE,PARAMETRE_ADI,PARAMETRE_DEGER,PARAMETRE_KRITER ) values (13,'Sunucu Adresi',3,'');",
                "insert into P_OTOMASYON_PARAMETRE ( ID_PARAMETRE,PARAMETRE_ADI,PARAMETRE_DEGER ) values (14,'Tank Otomasyonu',1);",
                "insert into P_OTOMASYON_PARAMETRE ( ID_PARAMETRE,PARAMETRE_ADI,PARAMETRE_DEGER ) values (15,'Pompa Otomasyonu',1);",
                "insert into P_OTOMASYON_PARAMETRE ( ID_PARAMETRE,PARAMETRE_ADI,PARAMETRE_DEGER ) values (16,'Web Servisleri',1);",
                "insert into P_OTOMASYON_PARAMETRE ( ID_PARAMETRE,PARAMETRE_ADI,PARAMETRE_DEGER ) values (17,'Tank Otomasyon Probesuz',2);"   
	],

        P_BASAMAK_KAYDIRMA = ["insert into P_BASAMAK_KAYDIRMA ( ID_BASAMAK_KAYDIRMA,BASAMAK_KAYDIRMA_ADI,ID_ACTIVE ) values (1,'Basamak Kaydırma',1);",
                              "insert into P_BASAMAK_KAYDIRMA ( ID_BASAMAK_KAYDIRMA,BASAMAK_KAYDIRMA_ADI,ID_ACTIVE ) values (2,'Anlık Satışta Litreyi Kaydır',1);",
                              "insert into P_BASAMAK_KAYDIRMA ( ID_BASAMAK_KAYDIRMA,BASAMAK_KAYDIRMA_ADI,ID_ACTIVE ) values (3,'Anlık Satışta TL yi Kaydır',1);",
                              "insert into P_BASAMAK_KAYDIRMA ( ID_BASAMAK_KAYDIRMA,BASAMAK_KAYDIRMA_ADI,ID_ACTIVE ) values (4,'Anlık Satışta Litre ve TL yi Kaydır',1);",
                              "insert into P_BASAMAK_KAYDIRMA ( ID_BASAMAK_KAYDIRMA,BASAMAK_KAYDIRMA_ADI,ID_ACTIVE ) values (5,'Dolum Raporunda Litreyi Kaydır',1);",
                              "insert into P_BASAMAK_KAYDIRMA ( ID_BASAMAK_KAYDIRMA,BASAMAK_KAYDIRMA_ADI,ID_ACTIVE ) values (6,'Dolum Raporunda TL yi Kaydır',1);",
                              "insert into P_BASAMAK_KAYDIRMA ( ID_BASAMAK_KAYDIRMA,BASAMAK_KAYDIRMA_ADI,ID_ACTIVE ) values (7,'Dolum Raporunda Litre ve TL yi Kaydır',1);",
                              "insert into P_BASAMAK_KAYDIRMA ( ID_BASAMAK_KAYDIRMA,BASAMAK_KAYDIRMA_ADI,ID_ACTIVE ) values (8,'Tüm Durumlarda Litreyi Kaydır',1);",
                              "insert into P_BASAMAK_KAYDIRMA ( ID_BASAMAK_KAYDIRMA,BASAMAK_KAYDIRMA_ADI,ID_ACTIVE ) values (9,'Tüm Durumlarda TL yi Kaydır',1);",
                              "insert into P_BASAMAK_KAYDIRMA ( ID_BASAMAK_KAYDIRMA,BASAMAK_KAYDIRMA_ADI,ID_ACTIVE ) values (10,'Tüm Durumlarda Litre ve TL yi Kaydır',1);"
                              ],
	P_PROBE_MARKA      = ["insert into P_PROBE_MARKA ( ID_PROBE_MARKA,PROBE_MARKA_ADI,ID_ACTIVE )   values (1,'Start Italiana XMT-SI-485',1);",
                              "insert into P_PROBE_MARKA ( ID_PROBE_MARKA,PROBE_MARKA_ADI,ID_ACTIVE )   values (2,'Teosis',1);",
			      "insert into P_PROBE_MARKA ( ID_PROBE_MARKA,PROBE_MARKA_ADI,ID_ACTIVE )   values (3,'HSM',1);",
                              "insert into P_PROBE_MARKA ( ID_PROBE_MARKA,PROBE_MARKA_ADI,ID_ACTIVE )   values (4,'TSAV3',1);",
                              ],
        CARI_HESAPLAR      = [("insert into CARI_HESAPLAR (ID_CARI_HESAP, CARI_ADI,YETKILI_ADI,ID_CARI_TIPI,ID_ACTIVE) values (1,'%s %s', '%s %s', 1, 1)"%( MESSAGES.varsayilan, MESSAGES.cari , MESSAGES.varsayilan,MESSAGES.yetkili))],
        VARDIYA            = [("insert into VARDIYA (ID_VARDIYA, VARDIYA_ADI,BASLANGIC_SAATI, BITIS_SAATI,ID_ACTIVE) values (1,'%s %s','08:00:00','18:00:00',1)"%(MESSAGES.varsayilan, MESSAGES.vardiya))],
	VARDIYA_AKTIF      = ["insert into VARDIYA_AKTIF (ID_VARDIYA_AKTIF, ID_VARDIYA,TARIH,VARDIYA_ACILIS_TUTAR,VARDIYA_KAPANIS_TUTAR,VARDIYA_SATIS_SAYISI) values (1,1,datetime('now', 'localtime'),0,0,0)"],
        VARDIYA_CARI       = ["insert into VARDIYA_CARI (ID_VARDIYA_CARI, ID_CARI_HESAP, ID_VARDIYA, ID_ACTIVE, TARIH) values (1,1,1,1,datetime('now', 'localtime'))"],
        P_KIT_TIPI         = ["insert into P_KIT_TIPI ( ID_KIT_TIPI, KIT_TIPI_ADI, ID_ACTIVE) VALUES (1, 'Kartlı Sistem',1);",
                              "insert into P_KIT_TIPI ( ID_KIT_TIPI, KIT_TIPI_ADI, ID_ACTIVE) VALUES (2, 'KMSiz Sistem',1);",
                              "insert into P_KIT_TIPI ( ID_KIT_TIPI, KIT_TIPI_ADI, ID_ACTIVE) VALUES (3, 'KMli Sistem',1);"],
        P_ILLER_KOS     = [
                                "insert into P_ILLER ( id, sehir) VALUES (1, 'Deçan')",
                                "insert into P_ILLER ( id, sehir) VALUES (2, 'Gjakova')",
                                "insert into P_ILLER ( id, sehir) VALUES (3, 'Gllogoc')",
                                "insert into P_ILLER ( id, sehir) VALUES (4, 'Gjilani')",
                                "insert into P_ILLER ( id, sehir) VALUES (5, 'Istogu')",
                                "insert into P_ILLER ( id, sehir) VALUES (6, 'Kaçanik')",
                                "insert into P_ILLER ( id, sehir) VALUES (7, 'Fushë Kosovë')",
                                "insert into P_ILLER ( id, sehir) VALUES (8, 'Kamenicë')",
                                "insert into P_ILLER ( id, sehir) VALUES (9, 'Mitrovica')",
                                "insert into P_ILLER ( id, sehir) VALUES (10, 'Leposaviq')",
                                "insert into P_ILLER ( id, sehir) VALUES (11, 'Lipjan')",
                                "insert into P_ILLER ( id, sehir) VALUES (12, 'Novobërdë')",
                                "insert into P_ILLER ( id, sehir) VALUES (13, 'Kastrioti')",
                                "insert into P_ILLER ( id, sehir) VALUES (14, 'Rahovec')",
                                "insert into P_ILLER ( id, sehir) VALUES (15, 'Peja')",
                                "insert into P_ILLER ( id, sehir) VALUES (16, 'Podujevë')",
                                "insert into P_ILLER ( id, sehir) VALUES (17, 'Prishtinë')",
                                "insert into P_ILLER ( id, sehir) VALUES (18, 'Prizren')",
                                "insert into P_ILLER ( id, sehir) VALUES (19, 'Ferizaj')",
                                "insert into P_ILLER ( id, sehir) VALUES (18, 'Vushtrri')",
                                "insert into P_ILLER ( id, sehir) VALUES (19, 'Zubin Potoku')"
                          ],
        
        P_ILLER_TR  = [
		"insert into P_ILLER ( id, sehir) VALUES (1, 'ADANA')",
		"insert into P_ILLER ( id, sehir) VALUES (2, 'ADIYAMAN')",
		"insert into P_ILLER ( id, sehir) VALUES (3, 'AFYON')",
		"insert into P_ILLER ( id, sehir) VALUES (4, 'AĞRI')",
		"insert into P_ILLER ( id, sehir) VALUES (5, 'AMASYA')",
		"insert into P_ILLER ( id, sehir) VALUES (6, 'ANKARA')",
		"insert into P_ILLER ( id, sehir) VALUES (7, 'ANTALYA')",
		"insert into P_ILLER ( id, sehir) VALUES (8, 'ARTVİN')",
		"insert into P_ILLER ( id, sehir) VALUES (9, 'AYDIN')",
		"insert into P_ILLER ( id, sehir) VALUES (10, 'BALIKESİR')",
		"insert into P_ILLER ( id, sehir) VALUES (11, 'BİLECİK')",
		"insert into P_ILLER ( id, sehir) VALUES (12, 'BİNGÖL')",
		"insert into P_ILLER ( id, sehir) VALUES (13, 'BİTLİS')",
		"insert into P_ILLER ( id, sehir) VALUES (14, 'BOLU')",
		"insert into P_ILLER ( id, sehir) VALUES (15, 'BURDUR')",
		"insert into P_ILLER ( id, sehir) VALUES (16, 'BURSA')",
		"insert into P_ILLER ( id, sehir) VALUES (17, 'ÇANAKKALE')",
		"insert into P_ILLER ( id, sehir) VALUES (18, 'ÇANKIRI')",
		"insert into P_ILLER ( id, sehir) VALUES (19, 'ÇORUM')",
		"insert into P_ILLER ( id, sehir) VALUES (20, 'DENİZLİ')",
		"insert into P_ILLER ( id, sehir) VALUES (21, 'DİYARBAKIR')",
		"insert into P_ILLER ( id, sehir) VALUES (22, 'EDİRNE')",
		"insert into P_ILLER ( id, sehir) VALUES (23, 'ELAZIĞ')",
		"insert into P_ILLER ( id, sehir) VALUES (24, 'ERZİNCAN')",
		"insert into P_ILLER ( id, sehir) VALUES (25, 'ERZURUM')",
		"insert into P_ILLER ( id, sehir) VALUES (26, 'ESKİŞEHİR')",
		"insert into P_ILLER ( id, sehir) VALUES (27, 'GAZİANTEP')",
		"insert into P_ILLER ( id, sehir) VALUES (28, 'GİRESUN')",
		"insert into P_ILLER ( id, sehir) VALUES (29, 'GÜMÜŞHANE')",
		"insert into P_ILLER ( id, sehir) VALUES (30, 'HAKKARİ')",
		"insert into P_ILLER ( id, sehir) VALUES (31, 'HATAY')",
		"insert into P_ILLER ( id, sehir) VALUES (32, 'ISPARTA')",
		"insert into P_ILLER ( id, sehir) VALUES (33, 'İÇEL')",
		"insert into P_ILLER ( id, sehir) VALUES (34, 'İSTANBUL')",
		"insert into P_ILLER ( id, sehir) VALUES (35, 'İZMİR')",
		"insert into P_ILLER ( id, sehir) VALUES (36, 'KARS')",
		"insert into P_ILLER ( id, sehir) VALUES (37, 'KASTAMONU')",
		"insert into P_ILLER ( id, sehir) VALUES (38, 'KAYSERİ')",
		"insert into P_ILLER ( id, sehir) VALUES (39, 'KIRKLARELİ')",
		"insert into P_ILLER ( id, sehir) VALUES (40, 'KIRŞEHİR')",
		"insert into P_ILLER ( id, sehir) VALUES (41, 'KOCAELİ')",
		"insert into P_ILLER ( id, sehir) VALUES (42, 'KONYA')",
		"insert into P_ILLER ( id, sehir) VALUES (43, 'KÜTAHYA')",
		"insert into P_ILLER ( id, sehir) VALUES (44, 'MALATYA')",
		"insert into P_ILLER ( id, sehir) VALUES (45, 'MANİSA')",
		"insert into P_ILLER ( id, sehir) VALUES (46, 'KAHRAMANMARAŞ')",
		"insert into P_ILLER ( id, sehir) VALUES (47, 'MARDİN')",
		"insert into P_ILLER ( id, sehir) VALUES (48, 'MUĞLA')",
		"insert into P_ILLER ( id, sehir) VALUES (49, 'MUŞ')",
		"insert into P_ILLER ( id, sehir) VALUES (50, 'NEVŞEHİR')",
		"insert into P_ILLER ( id, sehir) VALUES (51, 'NİĞDE')",
		"insert into P_ILLER ( id, sehir) VALUES (52, 'ORDU')",
		"insert into P_ILLER ( id, sehir) VALUES (53, 'RİZE')",
		"insert into P_ILLER ( id, sehir) VALUES (54, 'SAKARYA')",
		"insert into P_ILLER ( id, sehir) VALUES (55, 'SAMSUN')",
		"insert into P_ILLER ( id, sehir) VALUES (56, 'SİİRT')",
		"insert into P_ILLER ( id, sehir) VALUES (57, 'SİNOP')",
		"insert into P_ILLER ( id, sehir) VALUES (58, 'SİVAS')",
		"insert into P_ILLER ( id, sehir) VALUES (59, 'TEKİRDAĞ')",
		"insert into P_ILLER ( id, sehir) VALUES (60, 'TOKAT')",
		"insert into P_ILLER ( id, sehir) VALUES (61, 'TRABZON')",
		"insert into P_ILLER ( id, sehir) VALUES (62, 'TUNCELİ')",
		"insert into P_ILLER ( id, sehir) VALUES (63, 'ŞANLIURFA')",
		"insert into P_ILLER ( id, sehir) VALUES (64, 'UŞAK')",
		"insert into P_ILLER ( id, sehir) VALUES (65, 'VAN')",
		"insert into P_ILLER ( id, sehir) VALUES (66, 'YOZGAT')",
		"insert into P_ILLER ( id, sehir) VALUES (67, 'ZONGULDAK')",
		"insert into P_ILLER ( id, sehir) VALUES (68, 'AKSARAY')",
		"insert into P_ILLER ( id, sehir) VALUES (69, 'BAYBURT')",
		"insert into P_ILLER ( id, sehir) VALUES (70, 'KARAMAN')",
		"insert into P_ILLER ( id, sehir) VALUES (71, 'KIRIKKALE')",
		"insert into P_ILLER ( id, sehir) VALUES (72, 'BATMAN')",
		"insert into P_ILLER ( id, sehir) VALUES (73, 'ŞIRNAK')",
		"insert into P_ILLER ( id, sehir) VALUES (74, 'BARTIN')",
		"insert into P_ILLER ( id, sehir) VALUES (75, 'ARDAHAN')",
		"insert into P_ILLER ( id, sehir) VALUES (76, 'IĞDIR')",
		"insert into P_ILLER ( id, sehir) VALUES (77, 'YALOVA')",
		"insert into P_ILLER ( id, sehir) VALUES (78, 'KARABÜK')",
		"insert into P_ILLER ( id, sehir) VALUES (79, 'KİLİS')",
		"insert into P_ILLER ( id, sehir) VALUES (80, 'OSMANİYE')",
		"insert into P_ILLER ( id, sehir) VALUES (81, 'DÜZCE')"
	],
	P_ILCELER_TR = [
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (1, 'SEYHAN', 1)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (2, 'CEYHAN', 1)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (3, 'FEKE', 1)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (4, 'KARAİSALI', 1)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (5, 'KARATAŞ', 1)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (6, 'KOZAN', 1)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (7, 'POZANTI', 1)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (8, 'SAİMBEYLİ', 1)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (9, 'TUFANBEYLİ', 1)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (10,'YUMURTALIK', 1)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (11, 'YÜREĞİR', 1)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (12, 'ALADAĞ', 1)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (13, 'İMAMOĞLU', 1)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (14, 'ADIYAMAN MERKEZ', 2)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (15, 'BESNİ', 2)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (16, 'ÇELİKHAN', 2)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (17, 'GERGER', 2)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (18, 'GÖLBAŞI', 2)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (19, 'KAHTA', 2)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (20, 'SAMSAT', 2)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (21, 'SİNCİK', 2)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (22, 'TUT', 2)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (23, 'AFYONMERKEZ', 3)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (24, 'BOLVADİN', 3)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (25, 'ÇAY', 3)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (26, 'DAZKIRI', 3)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (27, 'DİNAR', 3)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (28, 'EMİRDAĞ', 3)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (29, 'İHSANİYE', 3)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (30, 'SANDIKLI', 3)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (31, 'SİNANPAŞA', 3)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (32, 'SULDANDAĞI', 3)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (33, 'ŞUHUT', 3)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (34, 'BAŞMAKÇI', 3)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (35, 'BAYAT', 3)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (36, 'İŞCEHİSAR', 3)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (37, 'ÇOBANLAR', 3)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (38, 'EVCİLER', 3)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (39, 'HOCALAR', 3)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (40, 'KIZILÖREN', 3)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (41, 'AKSARAY MERKEZ', 68)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (42, 'ORTAKÖY', 68)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (43, 'AĞAÇÖREN', 68)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (44, 'GÜZELYURT', 68)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (45, 'SARIYAHŞİ', 68)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (46, 'ESKİL', 68)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (47, 'GÜLAĞAÇ', 68)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (48, 'AMASYA MERKEZ', 5)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (49, 'GÖYNÜÇEK', 5)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (50, 'GÜMÜŞHACIKÖYÜ', 5)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (51, 'MERZİFON', 5)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (52, 'SULUOVA', 5)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (53, 'TAŞOVA', 5)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (54, 'HAMAMÖZÜ', 5)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (55, 'ALTINDAĞ', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (56, 'AYAS', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (57, 'BALA', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (58, 'BEYPAZARI', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (59, 'ÇAMLIDERE', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (60, 'ÇANKAYA', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (61, 'ÇUBUK', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (62, 'ELMADAĞ', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (63, 'GÜDÜL', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (64, 'HAYMANA', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (65, 'KALECİK', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (66, 'KIZILCAHAMAM', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (67, 'NALLIHAN', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (68, 'POLATLI', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (69, 'ŞEREFLİKOÇHİSAR', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (70, 'YENİMAHALLE', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (71, 'GÖLBAŞI', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (72, 'KEÇİÖREN', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (73, 'MAMAK', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (74, 'SİNCAN', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (75, 'KAZAN', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (76, 'AKYURT', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (77, 'ETİMESGUT', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (78, 'EVREN', 6)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (79, 'ANSEKİ', 7)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (80, 'ALANYA', 7)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (81, 'ANTALYA MERKEZİ', 7)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (82, 'ELMALI', 7)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (83, 'FİNİKE', 7)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (84, 'GAZİPAŞA', 7)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (85, 'GÜNDOĞMUŞ', 7)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (86, 'KAŞ', 7)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (87, 'KORKUTELİ', 7)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (88, 'KUMLUCA', 7)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (89, 'MANAVGAT', 7)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (90, 'SERİK', 7)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (91, 'DEMRE', 7)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (92, 'İBRADI', 7)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (93, 'KEMER', 7)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (94, 'ARDAHAN MERKEZ', 75)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (95, 'GÖLE', 75)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (96, 'ÇILDIR', 75)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (97, 'HANAK', 75)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (98, 'POSOF', 75)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (99, 'DAMAL', 75)",
		"insert into P_ILCELER (id, ilce, sehir ) VALUES (100, 'ARDANUÇ', 8)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (101, 'ARHAVİ', 8)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (102, 'ARTVİN MERKEZ', 8)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (103, 'BORÇKA', 8)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (104, 'HOPA', 8)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (105, 'ŞAVŞAT', 8)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (106, 'YUSUFELİ', 8)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (107, 'MURGUL', 8)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (108, 'AYDIN MERKEZ', 9)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (109, 'BOZDOĞAN', 9)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (110, 'ÇİNE', 9)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (111, 'GERMENCİK', 9)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (112, 'KARACASU', 9)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (113, 'KOÇARLI', 9)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (114, 'KUŞADASI', 9)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (115, 'KUYUCAK', 9)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (116, 'NAZİLLİ', 9)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (117, 'SÖKE', 9)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (118, 'SULTANHİSAR', 9)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (119, 'YENİPAZAR', 9)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (120, 'BUHARKENT', 9)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (121, 'İNCİRLİOVA', 9)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (122, 'KARPUZLU', 9)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (123, 'KÖŞK', 9)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (124, 'DİDİM', 9)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (125, 'AĞRI MERKEZ', 4)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (126, 'DİYADİN', 4)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (127, 'DOĞUBEYAZIT', 4)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (128, 'ELEŞKİRT', 4)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (129, 'HAMUR', 4)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (130, 'PATNOS', 4)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (131, 'TAŞLIÇAY', 4)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (132, 'TUTAK', 4)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (133, 'AYVALIK', 10)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (134, 'BALIKESİR MERKEZ', 10)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (135, 'BALYA', 10)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (136, 'BANDIRMA', 10)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (137, 'BİGADİÇ', 10)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (138, 'BURHANİYE', 10)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (139, 'DURSUNBEY', 10)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (140, 'EDREMİT', 10)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (141, 'ERDEK', 10)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (142, 'GÖNEN', 10)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (143, 'HAVRAN', 10)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (144, 'İVRİNDİ', 10)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (145, 'KEPSUT', 10)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (146, 'MANYAS', 10)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (147, 'SAVAŞTEPE', 10)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (148, 'SINDIRGI', 10)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (149, 'SUSURLUK', 10)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (150, 'MARMARA', 10)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (151, 'GÖMEÇ', 10)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (152, 'BARTIN MERKEZ', 74)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (153, 'KURUCAŞİLE', 74)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (154, 'ULUS', 74)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (155, 'AMASRA', 74)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (156, 'BATMAN MERKEZ', 72)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (157, 'BEŞİRİ', 72)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (158, 'GERCÜŞ', 72)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (159, 'KOZLUK', 72)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (160, 'SASON', 72)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (161, 'HASANKEYF', 72)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (162, 'BAYBURT MERKEZ', 69)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (163, 'AYDINTEPE', 69)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (164, 'DEMİRÖZÜ', 69)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (165, 'BOLU MERKEZ', 14)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (166, 'GEREDE', 14)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (167, 'GÖYNÜK', 14)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (168, 'KIBRISCIK', 14)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (169, 'MENGEN', 14)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (170, 'MUDURNU', 14)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (171, 'SEBEN', 14)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (172, 'DÖRTDİVAN', 14)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (173, 'YENİÇAĞA', 14)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (174, 'AĞLASUN', 15)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (175, 'BUCAK', 15)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (176, 'BURDUR MERKEZ', 15)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (177, 'GÖLHİSAR', 15)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (178, 'TEFENNİ', 15)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (179, 'YEŞİLOVA', 15)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (180, 'KARAMANLI', 15)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (181, 'KEMER', 15)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (182, 'ALTINYAYLA', 15)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (183, 'ÇAVDIR', 15)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (184, 'ÇELTİKÇİ', 15)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (185, 'GEMLİK', 16)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (186, 'İNEGÖL', 16)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (187, 'İZNİK', 16)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (188, 'KARACABEY', 16)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (189, 'KELES', 16)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (190, 'MUDANYA', 16)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (191, 'MUSTAFA K. PAŞA', 16)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (192, 'ORHANELİ', 16)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (193, 'ORHANGAZİ', 16)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (194, 'YENİŞEHİR', 16)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (195, 'BÜYÜK ORHAN', 16)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (196, 'HARMANCIK', 16)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (197, 'NÜLİFER', 16)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (198, 'OSMAN GAZİ', 16)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (199, 'YILDIRIM', 16)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (200, 'GÜRSU', 16)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (201, 'KESTEL', 16)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (202, 'BİLECİK MERKEZ', 11)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (203, 'BOZÜYÜK', 11)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (204, 'GÖLPAZARI', 11)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (205, 'OSMANELİ', 11)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (206, 'PAZARYERİ', 11)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (207, 'SÖĞÜT', 11)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (208, 'YENİPAZAR', 11)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (209, 'İNHİSAR', 11)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (210, 'BİNGÖL MERKEZ', 12)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (211, 'GENÇ', 12)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (212, 'KARLIOVA', 12)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (213, 'KİGI', 12)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (214, 'SOLHAN', 12)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (215, 'ADAKLI', 12)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (216, 'YAYLADERE', 12)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (217, 'YEDİSU', 12)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (218, 'ADİLCEVAZ', 13)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (219, 'AHLAT', 13)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (220, 'BİTLİS MERKEZ', 13)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (221, 'HİZAN', 13)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (222, 'MUTKİ', 13)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (223, 'TATVAN', 13)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (224, 'GÜROYMAK', 13)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (225, 'DENİZLİ MERKEZ', 20)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (226, 'ACIPAYAM', 20)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (227, 'BULDAN', 20)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (228, 'ÇAL', 20)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (229, 'ÇAMELİ', 20)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (230, 'ÇARDAK', 20)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (231, 'ÇİVRİL', 20)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (232, 'GÜNEY', 20)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (233, 'KALE', 20)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (234, 'SARAYKÖY', 20)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (235, 'TAVAS', 20)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (236, 'BABADAĞ', 20)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (237, 'BEKİLLİ', 20)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (238, 'HONAZ', 20)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (239, 'SERİNHİSAR', 20)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (240, 'AKKÖY', 20)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (241, 'BAKLAN', 20)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (242, 'BEYAĞAÇ', 20)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (243, 'BOZKURT', 20)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (244, 'DÜZCE MERKEZ', 81)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (245, 'AKÇAKOCA', 81)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (246, 'YIĞILCA', 81)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (247, 'CUMAYERİ', 81)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (248, 'GÖLYAKA', 81)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (249, 'ÇİLİMLİ', 81)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (250, 'GÜMÜŞOVA', 81)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (251, 'KAYNAŞLI', 81)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (252, 'DİYARBAKIR MERKEZ', 21)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (253, 'BİSMİL', 21)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (254, 'ÇERMİK', 21)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (255, 'ÇINAR', 21)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (256, 'ÇÜNGÜŞ', 21)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (257, 'DİCLE', 21)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (258, 'ERGANİ', 21)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (259, 'HANİ', 21)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (260, 'HAZRO', 21)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (261, 'KULP', 21)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (262, 'LİCE', 21)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (263, 'SİLVAN', 21)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (264, 'EĞİL', 21)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (265, 'KOCAKÖY', 21)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (266, 'EDİRNE MERKEZ', 22)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (267, 'ENEZ', 22)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (268, 'HAVSA', 22)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (269, 'İPSALA', 22)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (270, 'KEŞAN', 22)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (271, 'LALAPAŞA', 22)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (272, 'MERİÇ', 22)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (273, 'UZUNKÖPRÜ', 22)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (274, 'SÜLOĞLU', 22)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (275, 'ELAZIĞ MERKEZ', 23)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (276, 'AĞIN', 23)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (277, 'BASKİL', 23)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (278, 'KARAKOÇAN', 23)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (279, 'KEBAN', 23)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (280, 'MADEN', 23)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (281, 'PALU', 23)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (282, 'SİVRİCE', 23)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (283, 'ARICAK', 23)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (284, 'KOVANCILAR', 23)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (285, 'ALACAKAYA', 23)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (286, 'ERZURUM MERKEZ', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (287, 'PALANDÖKEN', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (288, 'AŞKALE', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (289, 'ÇAT', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (290, 'HINIS', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (291, 'HORASAN', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (292, 'OLTU', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (293, 'İSPİR', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (294, 'KARAYAZI', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (295, 'NARMAN', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (296, 'OLUR', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (297, 'PASİNLER', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (298, 'ŞENKAYA', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (299, 'TEKMAN', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (300, 'TORTUM', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (301, 'KARAÇOBAN', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (302, 'UZUNDERE', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (303, 'PAZARYOLU', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (304, 'ILICA', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (305, 'KÖPRÜKÖY', 25)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (306, 'ÇAYIRLI', 24)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (307, 'ERZİNCAN MERKEZ', 24)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (308, 'İLİÇ', 24)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (309, 'KEMAH', 24)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (310, 'KEMALİYE', 24)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (311, 'REFAHİYE', 24)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (312, 'TERCAN', 24)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (313, 'OTLUKBELİ', 24)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (314, 'ESKİŞEHİR MERKEZ', 26)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (315, 'ÇİFTELER', 26)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (316, 'MAHMUDİYE', 26)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (317, 'MİHALIÇLIK', 26)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (318, 'SARICAKAYA', 26)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (319, 'SEYİTGAZİ', 26)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (320, 'SİVRİHİSAR', 26)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (321, 'ALPU', 26)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (322, 'BEYLİKOVA', 26)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (323, 'İNÖNÜ', 26)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (324, 'GÜNYÜZÜ', 26)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (325, 'HAN', 26)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (326, 'MİHALGAZİ', 26)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (327, 'ARABAN', 27)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (328, 'İSLAHİYE', 27)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (329, 'NİZİP', 27)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (330, 'OĞUZELİ', 27)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (331, 'YAVUZELİ', 27)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (332, 'ŞAHİNBEY', 27)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (333, 'ŞEHİT KAMİL', 27)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (334, 'KARKAMIŞ', 27)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (335, 'NURDAĞI', 27)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (336, 'GÜMÜŞHANE MERKEZ', 29)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (337, 'KELKİT', 29)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (338, 'ŞİRAN', 29)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (339, 'TORUL', 29)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (340, 'KÖSE', 29)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (341, 'KÜRTÜN', 29)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (342, 'ALUCRA', 28)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (343, 'BULANCAK', 28)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (344, 'DERELİ', 28)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (345, 'ESPİYE', 28)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (346, 'EYNESİL', 28)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (347, 'GİRESUN MERKEZ', 28)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (348, 'GÖRELE', 28)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (349, 'KEŞAP', 28)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (350, 'ŞEBİNKARAHİSAR', 28)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (351, 'TİREBOLU', 28)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (352, 'PİPAZİZ', 28)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (353, 'YAĞLIDERE', 28)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (354, 'ÇAMOLUK', 28)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (355, 'ÇANAKÇI', 28)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (356, 'DOĞANKENT', 28)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (357, 'GÜCE', 28)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (358, 'HAKKARİ MERKEZ', 30)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (359, 'ÇUKURCA', 30)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (360, 'ŞEMDİNLİ', 30)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (361, 'YÜKSEKOVA', 30)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (362, 'ALTINÖZÜ', 31)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (363, 'DÖRTYOL', 31)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (364, 'HATAY MERKEZ', 31)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (365, 'HASSA', 31)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (366, 'İSKENDERUN', 31)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (367, 'KIRIKHAN', 31)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (368, 'REYHANLI', 31)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (369, 'SAMANDAĞ', 31)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (370, 'YAYLADAĞ', 31)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (371, 'ERZİN', 31)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (372, 'BELEN', 31)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (373, 'KUMLU', 31)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (374, 'ISPARTA MERKEZ', 32)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (375, 'ATABEY', 32)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (376, 'KEÇİBORLU', 32)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (377, 'EĞİRDİR', 32)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (378, 'GELENDOST', 32)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (379, 'SİNİRKENT', 32)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (380, 'ULUBORLU', 32)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (381, 'YALVAÇ', 32)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (382, 'AKSU', 32)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (383, 'GÖNEN', 32)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (384, 'YENİŞAR BADEMLİ', 32)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (385, 'IĞDIR MERKEZ', 76)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (386, 'ARALIK', 76)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (387, 'TUZLUCA', 76)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (388, 'KARAKOYUNLU', 76)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (389, 'AFŞİN', 46)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (390, 'ANDIRIN', 46)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (391, 'ELBİSTAN', 46)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (392, 'GÖKSUN', 46)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (393, 'KAHRAMANMARAŞ MERKEZ', 46)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (394, 'PAZARCIK', 46)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (395, 'TÜRKOĞLU', 46)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (396, 'ÇAĞLAYANCERİT', 46)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (397, 'EKİNÖZÜ', 46)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (398, 'NURHAK', 46)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (399, 'EFLANİ', 78)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (400, 'ESKİPAZAR', 78)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (401, 'KARABÜK MERKEZ', 78)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (402, 'OVACIK', 78)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (403, 'SAFRANBOLU', 78)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (404, 'YENİCE', 78)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (405, 'ERMENEK', 70)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (406, 'KARAMAN MERKEZ', 70)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (407, 'AYRANCI', 70)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (408, 'KAZIMKARABEKİR', 70)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (409, 'BAŞYAYLA', 70)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (410, 'SARIVELİLER', 70)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (411, 'KARS MERKEZ', 36)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (412, 'ARPAÇAY', 36)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (413, 'DİGOR', 36)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (414, 'KAĞIZMAN', 36)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (415, 'SARIKAMIŞ', 36)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (416, 'SELİM', 36)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (417, 'SUSUZ', 36)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (418, 'AKYAKA', 36)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (419, 'ABANA', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (420, 'KASTAMONU MERKEZ', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (421, 'ARAÇ', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (422, 'AZDAVAY', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (423, 'BOZKURT', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (424, 'CİDE', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (425, 'ÇATALZEYTİN', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (426, 'DADAY', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (427, 'DEVREKANİ', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (428, 'İNEBOLU', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (429, 'KÜRE', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (430, 'TAŞKÖPRÜ', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (431, 'TOSYA', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (432, 'İHSANGAZİ', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (433, 'PINARBAŞI', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (434, 'ŞENPAZAR', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (435, 'AĞLI', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (436, 'DOĞANYURT', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (437, 'HANÖNÜ', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (438, 'SEYDİLER', 37)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (439, 'BÜNYAN', 38)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (440, 'DEVELİ', 38)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (441, 'FELAHİYE', 38)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (442, 'İNCESU', 38)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (443, 'PINARBAŞI', 38)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (444, 'SARIOĞLAN', 38)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (445, 'SARIZ', 38)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (446, 'TOMARZA', 38)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (447, 'YAHYALI', 38)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (448, 'YEŞİLHİSAR', 38)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (449, 'AKKIŞLA', 38)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (450, 'TALAS', 38)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (451, 'KOCASİNAN', 38)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (452, 'MELİKGAZİ', 38)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (453, 'HACILAR', 38)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (454, 'ÖZVATAN', 38)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (455, 'DERİCE', 71)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (456, 'KESKİN', 71)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (457, 'KIRIKKALE MERKEZ', 71)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (458, 'SALAK YURT', 71)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (459, 'BAHŞİLİ', 71)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (460, 'BALIŞEYH', 71)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (461, 'ÇELEBİ', 71)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (462, 'KARAKEÇİLİ', 71)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (463, 'YAHŞİHAN', 71)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (464, 'KIRKKLARELİ MERKEZ', 39)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (465, 'BABAESKİ', 39)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (466, 'DEMİRKÖY', 39)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (467, 'KOFÇAY', 39)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (468, 'LÜLEBURGAZ', 39)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (469, 'VİZE', 39)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (470, 'KIRŞEHİR MERKEZ', 40)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (471, 'ÇİÇEKDAĞI', 40)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (472, 'KAMAN', 40)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (473, 'MUCUR', 40)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (474, 'AKPINAR', 40)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (475, 'AKÇAKENT', 40)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (476, 'BOZTEPE', 40)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (477, 'KOCAELİ MERKEZ', 41)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (478, 'GEBZE', 41)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (479, 'GÖLCÜK', 41)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (480, 'KANDIRA', 41)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (481, 'KARAMÜRSEL', 41)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (482, 'KÖRFEZ', 41)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (483, 'DERİNCE', 41)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (484, 'KONYA MERKEZ', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (485, 'AKŞEHİR', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (486, 'BEYŞEHİR', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (487, 'BOZKIR', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (488, 'CİHANBEYLİ', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (489, 'ÇUMRA', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (490, 'DOĞANHİSAR', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (491, 'EREĞLİ', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (492, 'HADİM', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (493, 'ILGIN', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (494, 'KADINHANI', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (495, 'KARAPINAR', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (496, 'KULU', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (497, 'SARAYÖNÜ', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (498, 'SEYDİŞEHİR', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (499, 'YUNAK', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (500, 'AKÖREN', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (501, 'ALTINEKİN', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (502, 'DEREBUCAK', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (503, 'HÜYÜK', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (504, 'KARATAY', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (505, 'MERAM', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (506, 'SELÇUKLU', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (507, 'TAŞKENT', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (508, 'AHIRLI', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (509, 'ÇELTİK', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (510, 'DERBENT', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (511, 'EMİRGAZİ', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (512, 'GÜNEYSINIR', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (513, 'HALKAPINAR', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (514, 'TUZLUKÇU', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (515, 'YALIHÜYÜK', 42)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (516, 'KÜTAHYA  MERKEZ', 43)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (517, 'ALTINTAŞ', 43)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (518, 'DOMANİÇ', 43)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (519, 'EMET', 43)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (520, 'GEDİZ', 43)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (521, 'SİMAV', 43)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (522, 'TAVŞANLI', 43)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (523, 'ASLANAPA', 43)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (524, 'DUMLUPINAR', 43)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (525, 'HİSARCIK', 43)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (526, 'ŞAPHANE', 43)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (527, 'ÇAVDARHİSAR', 43)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (528, 'PAZARLAR', 43)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (529, 'KİLİS MERKEZ', 79)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (530, 'ELBEYLİ', 79)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (531, 'MUSABEYLİ', 79)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (532, 'POLATELİ', 79)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (533, 'MALATYA MERKEZ', 44)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (534, 'AKÇADAĞ', 44)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (535, 'ARAPGİR', 44)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (536, 'ARGUVAN', 44)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (537, 'DARENDE', 44)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (538, 'DOĞANŞEHİR', 44)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (539, 'HEKİMHAN', 44)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (540, 'PÜTÜRGE', 44)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (541, 'YEŞİLYURT', 44)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (542, 'BATTALGAZİ', 44)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (543, 'DOĞANYOL', 44)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (544, 'KALE', 44)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (545, 'KULUNCAK', 44)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (546, 'YAZIHAN', 44)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (547, 'AKHİSAR', 45)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (548, 'ALAŞEHİR', 45)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (549, 'DEMİRCİ', 45)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (550, 'GÖRDES', 45)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (551, 'KIRKAĞAÇ', 45)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (552, 'KULA', 45)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (553, 'MANİSA MERKEZ', 45)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (554, 'SALİHLİ', 45)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (555, 'SARIGÖL', 45)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (556, 'SARUHANLI', 45)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (557, 'SELENDİ', 45)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (558, 'SOMA', 45)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (559, 'TURGUTLU', 45)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (560, 'AHMETLİ', 45)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (561, 'GÖLMARMARA', 45)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (562, 'KÖPRÜBAŞI', 45)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (563, 'DERİK', 47)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (564, 'KIZILTEPE', 47)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (565, 'MARDİN MERKEZ', 47)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (566, 'MAZIDAĞI', 47)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (567, 'MİDYAT', 47)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (568, 'NUSAYBİN', 47)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (569, 'ÖMERLİ', 47)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (570, 'SAVUR', 47)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (571, 'YEŞİLLİ', 47)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (572, 'MERSİN MERKEZ', 33)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (573, 'ANAMUR', 33)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (574, 'ERDEMLİ', 33)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (575, 'GÜLNAR', 33)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (576, 'MUT', 33)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (577, 'SİLİFKE', 33)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (578, 'TARSUS', 33)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (579, 'AYDINCIK', 33)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (580, 'BOZYAZI', 33)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (581, 'ÇAMLIYAYLA', 33)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (582, 'BODRUM', 48)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (583, 'DATÇA', 48)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (584, 'FETHİYE', 48)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (585, 'KÖYCEĞİZ', 48)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (586, 'MARMARİS', 48)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (587, 'MİLAS', 48)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (588, 'MUĞLA MERKEZ', 48)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (589, 'ULA', 48)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (590, 'YATAĞAN', 48)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (591, 'DALAMAN', 48)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (592, 'KAVAKLI DERE', 48)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (593, 'ORTACA', 48)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (594, 'BULANIK', 49)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (595, 'MALAZGİRT', 49)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (596, 'MUŞ MERKEZ', 49)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (597, 'VARTO', 49)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (598, 'HASKÖY', 49)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (599, 'KORKUT', 49)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (600, 'NEVŞEHİR MERKEZ', 50)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (601, 'AVANOS', 50)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (602, 'DERİNKUYU', 50)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (603, 'GÜLŞEHİR', 50)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (604, 'HACIBEKTAŞ', 50)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (605, 'KOZAKLI', 50)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (606, 'ÜRGÜP', 50)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (607, 'ACIGÖL', 50)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (608, 'NİĞDE MERKEZ', 51)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (609, 'BOR', 51)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (610, 'ÇAMARDI', 51)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (611, 'ULUKIŞLA', 51)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (612, 'ALTUNHİSAR', 51)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (613, 'ÇİFTLİK', 51)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (614, 'AKKUŞ', 52)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (615, 'AYBASTI', 52)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (616, 'FATSA', 52)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (617, 'GÖLKÖY', 52)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (618, 'KORGAN', 52)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (619, 'KUMRU', 52)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (620, 'MESUDİYE', 52)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (621, 'ORDU MERKEZ', 52)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (622, 'PERŞEMBE', 52)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (623, 'ULUBEY', 52)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (624, 'ÜNYE', 52)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (625, 'GÜLYALI', 52)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (626, 'GÜRGENTEPE', 52)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (627, 'ÇAMAŞ', 52)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (628, 'ÇATALPINAR', 52)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (629, 'ÇAYBAŞI', 52)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (630, 'İKİZCE', 52)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (631, 'KABADÜZ', 52)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (632, 'KABATAŞ', 52)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (633, 'BAHÇE', 80)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (634, 'KADİRLİ', 80)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (635, 'OSMANİYE MERKEZ', 80)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (636, 'DÜZİÇİ', 80)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (637, 'HASANBEYLİ', 80)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (638, 'SUMBAŞ', 80)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (639, 'TOPRAKKALE', 80)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (640, 'RİZE MERKEZ', 53)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (641, 'ARDEŞEN', 53)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (642, 'ÇAMLIHEMŞİN', 53)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (643, 'ÇAYELİ', 53)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (644, 'FINDIKLI', 53)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (645, 'İKİZDERE', 53)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (646, 'KALKANDERE', 53)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (647, 'PAZAR', 53)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (648, 'GÜNEYSU', 53)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (649, 'DEREPAZARI', 53)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (650, 'HEMŞİN', 53)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (651, 'İYİDERE', 53)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (652, 'AKYAZI', 54)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (653, 'GEYVE', 54)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (654, 'HENDEK', 54)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (655, 'KARASU', 54)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (656, 'KAYNARCA', 54)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (657, 'SAKARYA MERKEZ', 54)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (658, 'PAMUKOVA', 54)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (659, 'TARAKLI', 54)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (660, 'FERİZLİ', 54)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (661, 'KARAPÜRÇEK', 54)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (662, 'SÖĞÜTLÜ', 54)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (663, 'ALAÇAM', 55)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (664, 'BAFRA', 55)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (665, 'ÇARŞAMBA', 55)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (666, 'HAVZA', 55)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (667, 'KAVAK', 55)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (668, 'LADİK', 55)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (669, 'SAMSUN MERKEZ', 55)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (670, 'TERME', 55)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (671, 'VEZİRKÖPRÜ', 55)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (672, 'ASARCIK', 55)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (673, 'ONDOKUZMAYIS', 55)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (674, 'SALIPAZARI', 55)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (675, 'TEKKEKÖY', 55)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (676, 'AYVACIK', 55)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (677, 'YAKAKENT', 55)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (678, 'AYANCIK', 57)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (679, 'BOYABAT', 57)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (680, 'SİNOP MERKEZ', 57)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (681, 'DURAĞAN', 57)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (682, 'ERGELEK', 57)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (683, 'GERZE', 57)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (684, 'TÜRKELİ', 57)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (685, 'DİKMEN', 57)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (686, 'SARAYDÜZÜ', 57)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (687, 'DİVRİĞİ', 58)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (688, 'GEMEREK', 58)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (689, 'GÜRÜN', 58)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (690, 'HAFİK', 58)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (691, 'İMRANLI', 58)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (692, 'KANGAL', 58)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (693, 'KOYUL HİSAR', 58)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (694, 'SİVAS MERKEZ', 58)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (695, 'SU ŞEHRİ', 58)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (696, 'ŞARKIŞLA', 58)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (697, 'YILDIZELİ', 58)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (698, 'ZARA', 58)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (699, 'AKINCILAR', 58)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (700, 'ALTINYAYLA', 58)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (701, 'DOĞANŞAR', 58)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (702, 'GÜLOVA', 58)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (703, 'ULAŞ', 58)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (704, 'BAYKAN', 56)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (705, 'ERUH', 56)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (706, 'KURTALAN', 56)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (707, 'PERVARİ', 56)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (708, 'SİİRT MERKEZ', 56)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (709, 'ŞİRVARİ', 56)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (710, 'AYDINLAR', 56)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (711, 'TEKİRDAĞ MERKEZ', 59)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (712, 'ÇERKEZKÖY', 59)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (713, 'ÇORLU', 59)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (714, 'HAYRABOLU', 59)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (715, 'MALKARA', 59)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (716, 'MURATLI', 59)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (717, 'SARAY', 59)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (718, 'ŞARKÖY', 59)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (719, 'MARAMARAEREĞLİSİ', 59)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (720, 'ALMUS', 60)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (721, 'ARTOVA', 60)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (722, 'TOKAT MERKEZ', 60)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (723, 'ERBAA', 60)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (724, 'NİKSAR', 60)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (725, 'REŞADİYE', 60)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (726, 'TURHAL', 60)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (727, 'ZİLE', 60)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (728, 'PAZAR', 60)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (729, 'YEŞİLYURT', 60)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (730, 'BAŞÇİFTLİK', 60)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (731, 'SULUSARAY', 60)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (732, 'TRABZON MERKEZ', 61)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (733, 'AKÇAABAT', 61)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (734, 'ARAKLI', 61)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (735, 'ARŞİN', 61)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (736, 'ÇAYKARA', 61)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (737, 'MAÇKA', 61)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (738, 'OF', 61)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (739, 'SÜRMENE', 61)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (740, 'TONYA', 61)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (741, 'VAKFIKEBİR', 61)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (742, 'YOMRA', 61)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (743, 'BEŞİKDÜZÜ', 61)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (744, 'ŞALPAZARI', 61)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (745, 'ÇARŞIBAŞI', 61)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (746, 'DERNEKPAZARI', 61)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (747, 'DÜZKÖY', 61)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (748, 'HAYRAT', 61)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (749, 'KÖPRÜBAŞI', 61)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (750, 'TUNCELİ MERKEZ', 62)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (751, 'ÇEMİŞGEZEK', 62)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (752, 'HOZAT', 62)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (753, 'MAZGİRT', 62)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (754, 'NAZİMİYE', 62)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (755, 'OVACIK', 62)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (756, 'PERTEK', 62)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (757, 'PÜLÜMÜR', 62)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (758, 'BANAZ', 64)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (759, 'EŞME', 64)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (760, 'KARAHALLI', 64)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (761, 'SİVASLI', 64)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (762, 'ULUBEY', 64)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (763, 'UŞAK MERKEZ', 64)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (764, 'BAŞKALE', 65)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (765, 'VAN MERKEZ', 65)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (766, 'EDREMİT', 65)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (767, 'ÇATAK', 65)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (768, 'ERCİŞ', 65)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (769, 'GEVAŞ', 65)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (770, 'GÜRPINAR', 65)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (771, 'MURADİYE', 65)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (772, 'ÖZALP', 65)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (773, 'BAHÇESARAY', 65)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (774, 'ÇALDIRAN', 65)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (775, 'SARAY', 65)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (776, 'YALOVA MERKEZ', 77)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (777, 'ALTINOVA', 77)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (778, 'ARMUTLU', 77)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (779, 'ÇINARCIK', 77)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (780, 'ÇİFTLİKKÖY', 77)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (781, 'TERMAL', 77)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (782, 'AKDAĞMADENİ', 66)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (783, 'BOĞAZLIYAN', 66)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (784, 'YOZGAT MERKEZ', 66)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (785, 'ÇAYIRALAN', 66)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (786, 'ÇEKEREK', 66)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (787, 'SARIKAYA', 66)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (788, 'SORGUN', 66)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (789, 'ŞEFAATLI', 66)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (790, 'YERKÖY', 66)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (791, 'KADIŞEHRİ', 66)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (792, 'SARAYKENT', 66)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (793, 'YENİFAKILI', 66)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (794, 'ÇAYCUMA', 67)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (795, 'DEVREK', 67)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (796, 'ZONGULDAK MERKEZ', 67)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (797, 'EREĞLİ', 67)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (798, 'ALAPLI', 67)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (799, 'GÖKÇEBEY', 67)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (800, 'ÇANAKKALE MERKEZ', 17)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (801, 'AYVACIK', 17)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (802, 'BAYRAMİÇ', 17)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (803, 'BİGA', 17)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (804, 'BOZCAADA', 17)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (805, 'ÇAN', 17)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (806, 'ECEABAT', 17)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (807, 'EZİNE', 17)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (808, 'LAPSEKİ', 17)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (809, 'YENİCE', 17)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (810, 'ÇANKIRI MERKEZ', 18)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (811, 'ÇERKEŞ', 18)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (812, 'ELDİVAN', 18)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (813, 'ILGAZ', 18)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (814, 'KURŞUNLU', 18)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (815, 'ORTA', 18)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (816, 'ŞABANÖZÜ', 18)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (817, 'YAPRAKLI', 18)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (818, 'ATKARACALAR', 18)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (819, 'KIZILIRMAK', 18)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (820, 'BAYRAMÖREN', 18)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (821, 'KORGUN', 18)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (822, 'ALACA', 19)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (823, 'BAYAT', 19)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (824, 'ÇORUM MERKEZ', 19)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (825, 'İKSİPLİ', 19)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (826, 'KARGI', 19)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (827, 'MECİTÖZÜ', 19)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (828, 'ORTAKÖY', 19)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (829, 'OSMANCIK', 19)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (830, 'SUNGURLU', 19)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (831, 'DODURGA', 19)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (832, 'LAÇİN', 19)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (833, 'OĞUZLAR', 19)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (834, 'ADALAR', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (835, 'BAKIRKÖY', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (836, 'BEŞİKTAŞ', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (837, 'BEYKOZ', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (838, 'BEYOĞLU', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (839, 'ÇATALCA', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (840, 'EMİNÖNÜ', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (841, 'EYÜP', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (842, 'FATİH', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (843, 'GAZİOSMANPAŞA', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (844, 'KADIKÖY', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (845, 'KARTAL', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (846, 'SARIYER', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (847, 'SİLİVRİ', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (848, 'ŞİLE', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (849, 'ŞİŞLİ', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (850, 'ÜSKÜDAR', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (851, 'ZEYTİNBURNU', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (852, 'BÜYÜKÇEKMECE', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (853, 'KAĞITHANE', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (854, 'KÜÇÜKÇEKMECE', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (855, 'PENDİK', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (856, 'ÜMRANİYE', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (857, 'BAYRAMPAŞA', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (858, 'AVCILAR', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (859, 'BAĞCILAR', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (860, 'BAHÇELİEVLER', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (861, 'GÜNGÖREN', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (862, 'MALTEPE', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (863, 'SULTANBEYLİ', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (864, 'TUZLA', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (865, 'ESENLER', 34)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (866, 'ALİAĞA', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (867, 'BAYINDIR', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (868, 'BERGAMA', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (869, 'BORNOVA', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (870, 'ÇEŞME', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (871, 'DİKİLİ', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (872, 'FOÇA', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (873, 'KARABURUN', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (874, 'KARŞIYAKA', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (875, 'KEMALPAŞA', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (876, 'KINIK', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (877, 'KİRAZ', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (878, 'MENEMEN', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (879, 'ÖDEMİŞ', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (880, 'SEFERİHİSAR', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (881, 'SELÇUK', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (882, 'TİRE', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (883, 'TORBALI', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (884, 'URLA', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (885, 'BEYDAĞ', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (886, 'BUCA', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (887, 'KONAK', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (888, 'MENDERES', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (889, 'BALÇOVA', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (890, 'ÇİGLİ', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (891, 'GAZİEMİR', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (892, 'NARLIDERE', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (893, 'GÜZELBAHÇE', 35)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (894, 'ŞANLIURFA MERKEZ', 63)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (895, 'AKÇAKALE', 63)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (896, 'BİRECİK', 63)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (897, 'BOZOVA', 63)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (898, 'CEYLANPINAR', 63)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (899, 'HALFETİ', 63)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (900, 'HİLVAN', 63)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (901, 'SİVEREK', 63)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (902, 'SURUÇ', 63)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (903, 'VİRANŞEHİR', 63)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (904, 'HARRAN', 63)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (905, 'BEYTÜŞŞEBAP', 73)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (906, 'ŞIRNAK MERKEZ', 73)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (907, 'CİZRE', 73)",
		"insert into P_ILCELER (id, ilce, sehir )  VALUES (908, 'İDİL', 73)",	
	],
	P_CARI_TIPI = [
		("insert into P_CARI_TIPI ( ID_CARI_TIPI,CARI_TIP_ADI ) values (1,'%s');"%MESSAGES.personel),
		("insert into P_CARI_TIPI ( ID_CARI_TIPI,CARI_TIP_ADI ) values (2,'%s');"%MESSAGES.musteri)
	],
	########################### TEST VERILERI SILINECEK ###################################3
	#########################################################################################
	
)
def check_db():
	import os
	import sqlite3
	#print dir( sqlite3 )
	
	db_is_new   = not os.path.exists(db_filename)
	
	with sqlite3.connect(db_filename) as conn:
		conn.execute("PRAGMA KEY=manager")
		for k,v in db_object_dict.iteritems():
                      check_table_sql  =  '''SELECT name FROM sqlite_master WHERE type='table' AND name='%s';'''%k
                      cursor = conn.cursor()
                      cursor.execute( check_table_sql )
                      rows = cursor.fetchone()
                      cursor.close()
                      if not rows:
                          conn.execute( v )
                      if db_object_values.has_key( k ):
                            for v_ in db_object_values[k]:
                                try:
                                     conn.execute( v_ )
                                except:
                                     #print v_
                                     continue
                      else:
                                if db_object_values.has_key( locale.iller ):
                                      for v_iller in db_object_values[locale.iller] :
                                          try:    
                                                conn.execute( v_iller )
                                          except:
                                                continue
                                if db_object_values.has_key( locale.ilceler ):
                                      for v_ilceler in db_object_values[locale.ilceler] :
                                          try:    
                                                conn.execute( v_ilceler )
                                          except:
                                                continue
                                if db_object_values.has_key( locale.yakitlar ):
                                      for v_yakitlar in db_object_values[locale.yakitlar] :
                                          try:    
                                                conn.execute( v_yakitlar )
                                          except:
                                                continue
                                                                
                                
                od = collections.OrderedDict(sorted(db_object_view_dict.items()))
		for kv, vv in od.iteritems ():
			check_table_sql  =  '''SELECT name FROM sqlite_master WHERE type='view' AND name='%s';'''%kv
			cursor3 = conn.cursor()
			cursor3.execute( check_table_sql )
			rows = cursor3.fetchone()
			cursor3.close()
			if not rows:
				conn.execute( vv )
				conn.commit()
	
		for k,v in db_object_trigger_dict.iteritems():
                               
                                try:
                                        conn.execute(v)
                                except:
                                        pass

	conn.close()	

