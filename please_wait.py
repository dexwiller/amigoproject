import wx
import sabitler
from local import MESSAGES
import style as s
class waitPanel(wx.Panel):
    def __init__(self, parent):

        wx.Panel.__init__(self, parent, 1)
        sizer = wx.BoxSizer( wx.VERTICAL )
        
        wait_ani = wx.animate.Animation( sabitler.image_path +  MESSAGES.yukleniyor )
        wait_ctrl = wx.animate.AnimationCtrl(self, 1, wait_ani)
        wait_ctrl.SetUseWindowBackgroundColour()
        wait_ctrl.Play()
        
        sizer.Add( wait_ctrl,1,wx.EXPAND)

        self.SetSizerAndFit(sizer)

class waitFrame(wx.Frame):
    def __init__(self,parent):
        wx.Frame.__init__(self, parent,sabitler.wait_panel_id, '',
        style= wx.CLIP_CHILDREN|wx.STAY_ON_TOP|wx.FRAME_NO_TASKBAR| wx.FRAME_SHAPED)

        panel = waitPanel(self)
        sizer=wx.BoxSizer(wx.VERTICAL)
        sizer.Add(panel,1,wx.EXPAND)
        self.SetSizerAndFit(sizer)
        self.Layout()
        self.CenterOnScreen()
        self.Show()
        
class thiefPanel(wx.Panel):
    def __init__(self, parent):

        wx.Panel.__init__(self, parent, 1)
        sizer = wx.BoxSizer( wx.VERTICAL )
        
        st_thief = wx.StaticText(self,-1,MESSAGES.lisans_hatasi)
        s.setStaticTextPompaHeader( st_thief )
        
        sizer.Add( st_thief,1,wx.EXPAND)

        self.SetSizerAndFit(sizer)

class thiefFrame(wx.Frame):
    def __init__(self,parent):
        wx.Frame.__init__(self, parent,wx.ID_ANY, '',
        style= wx.CLIP_CHILDREN|wx.STAY_ON_TOP|wx.FRAME_NO_TASKBAR| wx.FRAME_SHAPED  )

        panel = thiefPanel(self)
        sizer=wx.BoxSizer(wx.VERTICAL)
        sizer.Add(panel,1,wx.EXPAND)
        self.SetSizerAndFit(sizer)
        self.Layout()
        self.CenterOnScreen()
        self.Show()


