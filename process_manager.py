import sys,os
from _winreg import *
import psutil
import time
from sabitler import path_prefix, control_process, main_process
import trace
import threading

class KThread(threading.Thread):
 
    def __init__(self, *args, **keywords):
        threading.Thread.__init__(self, *args, **keywords)
        self.killed = False
  
    def start(self):
        self.__run_backup = self.run
        self.run = self.__run  
        threading.Thread.start(self)
  
    def __run(self):
        sys.settrace(self.globaltrace)
        self.__run_backup()
        self.run = self.__run_backup
  
    def globaltrace(self, frame, why, arg):
        if why == 'call':
          return self.localtrace
        else:
          return None
  
    def localtrace(self, frame, why, arg):
        if self.killed:
          if why == 'line':
            raise SystemExit()
        return self.localtrace
  
    def kill(self):
        self.killed = True
def process_control( process_name ):
    p_list =  psutil.get_process_list()
    param_process_is_alive = False
    for p in p_list:
        try:
            if p.name() == process_name:
                param_process_is_alive = True
                break
        except:
            pass
    return param_process_is_alive
def process_start( process_name ):
    path = path_prefix + '\\' + process_name
    try:
        #print path
        os.startfile(path)
    except:
        #print 'Process Path Hatali !!!'
        #print path
        pass
def process_end( process_name ):
    for proc in psutil.process_iter():
        try:
            if str(proc.name()) == process_name:
                proc.kill()
        except:
            pass
def otoControl():
    process_name = main_process
    param_process_is_alive = process_control( process_name )
    if not param_process_is_alive:
        process_start ( process_name )
def selfControl():
    #you take myself, you take my self-control...
    process_name = control_process
    param_process_is_alive = process_control( process_name )
    if not param_process_is_alive:
        process_start ( process_name )
