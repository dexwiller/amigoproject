#!/usr/bin/env python
# -*- coding: latin5 -*-

import wx
import os
import sys
import time
import logging
import style as s
import sabitler
import util
import wx.lib.agw.aui as aui
from win32api import GetSystemMetrics
from getGridDataList import TreeListPanel
from getGridDataList import IndexListPanel,PaginatingIndexPanel
import wx.lib.buttons as LibButtons
import model  
import serial
from urllib2 import URLError
from socket import error as socketError
from exceptions import Exception as exceptionException
from  ws_sonuc import wsSonuc
from iletisim import EpdkSoap
from iletisim import WebSoap
WebSoapObj          = WebSoap()
EpdkSoapObj         = EpdkSoap()
from serial.tools import list_ports
from process_manager import KThread

from wx.lib import delayedresult
param         = model.Parameters()


button_size = sabitler.ana_button_size
image_path  = sabitler.image_path
from process_manager import process_end, process_start, process_control

id_b_otomasyon_ayarlari_ekrani = sabitler.EKRAN_ID_MAP.AYARLAR_BUTONLAR.id_b_otomasyon_ayarlar
id_b_bayii_tanimlama           = sabitler.EKRAN_ID_MAP.AYARLAR_BUTONLAR.id_b_bayii_tanimlama
id_b_yakit_tanimlama           = sabitler.EKRAN_ID_MAP.AYARLAR_BUTONLAR.id_b_yakit_tanimlama
id_b_tank_tanimlari            = sabitler.EKRAN_ID_MAP.AYARLAR_BUTONLAR.id_b_tank_tanimlama
id_b_pompa_tanimlari           = sabitler.EKRAN_ID_MAP.AYARLAR_BUTONLAR.id_b_pompa_tanimlama     
id_b_tanimlama_gonder_al       = sabitler.EKRAN_ID_MAP.AYARLAR_BUTONLAR.id_b_tanimlama_gonder_al
id_b_port_tanimlama            = sabitler.EKRAN_ID_MAP.AYARLAR_BUTONLAR.id_b_portlar
id_b_otomasyon_raporu          = sabitler.EKRAN_ID_MAP.AYARLAR_BUTONLAR.id_b_otomasyon_rapor
class win_Ayarlar(wx.Frame):
    def __init__(self, parent, id_, title):
	wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE|
                                  wx.FRAME_NO_TASKBAR | wx.MAXIMIZE))
	
	self.O  = param.get_root( self ).O 
	self.GValuesDict = None
	self.SourceDataBase = None
	self.RS = None
	self.previousGValuesDict = None
	self.oncekiDeger = False
	#self.CreateStatusBar() 
	self.LayoutItems()
	self.Centre()
	self.Bind(wx.EVT_CLOSE, self.OnCloseWindow)
	self.Show(True)
    
    def LayoutItems(self):
        mainSizer = wx.GridBagSizer(0,0)
        self.Freeze()
        self.SetSizer(mainSizer)
        winSize =  self.GetSize()
        self.top_panel = wx.Panel(self, wx.ID_ANY,size=(winSize[0],540))
        self.top_panel.SetBackgroundColour( wx.Colour(254,254,254))
        self.top_sizer = wx.GridBagSizer(1,1)
        self.top_panel.SetSizer( self.top_sizer )
        
        self.otomasyon_ayarlari_pressed  =  wx.Image("%stool_accepted_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_otomasyon_ayarlari         = wx.Image("%stool_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_otomasyon_ayarlari        = wx.BitmapButton(self.top_panel, id=id_b_otomasyon_ayarlari_ekrani, bitmap=image_otomasyon_ayarlari,pos=(0,0),
        size = (image_otomasyon_ayarlari.GetWidth()+5, image_otomasyon_ayarlari.GetHeight()+5))
        self.btn_otomasyon_ayarlari.Bind(wx.EVT_BUTTON, self.OnAyarlarMenuPressed)
        self.btn_otomasyon_ayarlari.SetToolTipString("Otomasyon Ayarlar�")
        self.btn_otomasyon_ayarlari.SetBitmapSelected(self.otomasyon_ayarlari_pressed)
        self.top_sizer.Add(self.btn_otomasyon_ayarlari, (0,0),(1,1),wx.ALL)
        
        self.bayii_tanimlari_pressed  =  wx.Image("%sbayi_tanimlama_accepted_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_bayii_tanimlari = wx.Image("%sbayi_tanimlama_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_bayii_tanimlari = wx.BitmapButton(self.top_panel, id=id_b_bayii_tanimlama, bitmap=image_bayii_tanimlari,pos=(0,1),
        size = (image_bayii_tanimlari.GetWidth()+5, image_bayii_tanimlari.GetHeight()+5))
        self.btn_bayii_tanimlari.Bind(wx.EVT_BUTTON, self.OnAyarlarMenuPressed)
        self.btn_bayii_tanimlari.SetToolTipString("Bayii Tan�mlama")
        self.btn_bayii_tanimlari.SetBitmapSelected(self.bayii_tanimlari_pressed)
        self.top_sizer.Add(self.btn_bayii_tanimlari,  (0,1),(1,1), wx.ALL)
        
        self.yakit_tanimlari_pressed  =  wx.Image("%syakit_tanimlama_accepted_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_yakit_tanimlari = wx.Image("%syakit_tanimlama_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_yakit_tanimlari  = wx.BitmapButton(self.top_panel, id=id_b_yakit_tanimlama, bitmap=image_yakit_tanimlari,pos=(0,1),
        size = (image_yakit_tanimlari.GetWidth()+5, image_yakit_tanimlari.GetHeight()+5))
        self.btn_yakit_tanimlari.Bind(wx.EVT_BUTTON, self.OnAyarlarMenuPressed)
        self.btn_yakit_tanimlari.SetToolTipString("Yak�t Tan�mlama")
        self.btn_yakit_tanimlari.SetBitmapSelected(self.yakit_tanimlari_pressed)
        self.top_sizer.Add(self.btn_yakit_tanimlari,  (0,2),(1,1), wx.ALL)
        
        self.port_tanimlari_pressed  =  wx.Image("%sserialport_icon_accepted_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_port_tanimlari = wx.Image("%sserialport_icon_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_port_tanimlari  = wx.BitmapButton(self.top_panel, id=id_b_port_tanimlama, bitmap=image_port_tanimlari,pos=(0,1),
        size = (image_port_tanimlari.GetWidth()+5, image_port_tanimlari.GetHeight()+5))
        self.btn_port_tanimlari.Bind(wx.EVT_BUTTON, self.OnAyarlarMenuPressed)
        self.btn_port_tanimlari.SetToolTipString("Port Tan�mlama")
        self.btn_port_tanimlari.SetBitmapSelected(self.port_tanimlari_pressed)
        self.top_sizer.Add(self.btn_port_tanimlari,  (0,3),(1,1), wx.ALL)
        
        self.restart_pressed  =  wx.Image("%srestart_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        restart_ters = wx.Image("%srestart_gray_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_restart  = wx.BitmapButton(self.top_panel, id=wx.ID_ANY, bitmap=restart_ters,pos=(0,1),
        size = (restart_ters.GetWidth()+5, restart_ters.GetHeight()+5))
        self.btn_restart.Bind(wx.EVT_BUTTON, self.OnRestart)
        self.btn_restart.SetToolTipString("Yeniden Ba�lat")
        self.btn_restart.SetBitmapSelected(self.restart_pressed)
        self.top_sizer.Add(self.btn_restart,  (0,4),(1,1), wx.ALL)
        
        self.btn_otomasyon_rapor_pressed  =  wx.Image("%sotomasyon_rapor_accepted256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_btn_otomasyon_rapor = wx.Image("%sotomasyon_rapor256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_otomasyon_rapor  = wx.BitmapButton(self.top_panel, id=id_b_otomasyon_raporu, bitmap=image_btn_otomasyon_rapor,pos=(0,0),
        size = (image_btn_otomasyon_rapor.GetWidth()+5, image_btn_otomasyon_rapor.GetHeight()+5))
        self.btn_otomasyon_rapor.Bind(wx.EVT_BUTTON, self.OnAyarlarMenuPressed)
        self.btn_otomasyon_rapor.SetToolTipString("Otomasyon Raporu")
        self.btn_otomasyon_rapor.SetBitmapSelected(self.btn_otomasyon_rapor_pressed)
        self.top_sizer.Add(self.btn_otomasyon_rapor, (1,3),(1,1),wx.ALL)
        
        self.tank_tanimlari_pressed  =  wx.Image("%stank_tanimlama_accepted_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_tank_tanimlari = wx.Image("%stank_tanimlama_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_tank_tanimlari  = wx.BitmapButton(self.top_panel, id=id_b_tank_tanimlari, bitmap=image_tank_tanimlari,pos=(0,1),
        size = (image_tank_tanimlari.GetWidth()+5, image_tank_tanimlari.GetHeight()+5))
        self.btn_tank_tanimlari.Bind(wx.EVT_BUTTON, self.OnAyarlarMenuPressed)
        self.btn_tank_tanimlari.SetToolTipString("Tank Tan�mlama")
        self.btn_tank_tanimlari.SetBitmapSelected(self.tank_tanimlari_pressed)
        self.top_sizer.Add(self.btn_tank_tanimlari,  (1,0),(1,1), wx.ALL)
        
        
        self.pompa_tanimlari_pressed  =  wx.Image("%spompa_endeks_accept_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_pompa_tanimlari = wx.Image("%spompa_endeks_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_otomasyon_tasit_tanima  = wx.BitmapButton(self.top_panel, id=id_b_pompa_tanimlari, bitmap=image_pompa_tanimlari,pos=(0,1),
        size = (image_pompa_tanimlari.GetWidth()+5, image_pompa_tanimlari.GetHeight()+5))
        self.btn_otomasyon_tasit_tanima.Bind(wx.EVT_BUTTON, self.OnAyarlarMenuPressed)
        self.btn_otomasyon_tasit_tanima.SetToolTipString("Pompa Tan�mlama")
        self.btn_otomasyon_tasit_tanima.SetBitmapSelected(self.pompa_tanimlari_pressed)
        self.top_sizer.Add(self.btn_otomasyon_tasit_tanima,  (1,1),(1,1), wx.ALL)
        
        self.tanimlama_gonder_al_pressed  =  wx.Image("%sgonder_al_accepted_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_tanimlama_gonder_al = wx.Image("%sgonder_al_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_tanimlama_gonder_al  = wx.BitmapButton(self.top_panel, id=id_b_tanimlama_gonder_al, bitmap=image_tanimlama_gonder_al,pos=(0,1),
        size = (image_tanimlama_gonder_al.GetWidth()+5, image_tanimlama_gonder_al.GetHeight()+5))
        self.btn_tanimlama_gonder_al.Bind(wx.EVT_BUTTON, self.OnAyarlarMenuPressed)
        self.btn_tanimlama_gonder_al.SetToolTipString("Tan�mlamalar� G�nder Al")
        self.btn_tanimlama_gonder_al.SetBitmapSelected(self.tanimlama_gonder_al_pressed)
        self.top_sizer.Add(self.btn_tanimlama_gonder_al,  (1,2),(1,1), wx.ALL)
        
        self.kilitle_pressed  =  wx.Image("%slock_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_kilitle = wx.Image("%sunlock_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_kilitle  = wx.BitmapButton(self.top_panel, id=wx.ID_ANY, bitmap=image_kilitle,pos=(0,1),
        size = (image_kilitle.GetWidth()+5, image_kilitle.GetHeight()+5))
        self.btn_kilitle.Bind(wx.EVT_BUTTON, self.OnAyarlariKilitle)
        self.btn_kilitle.SetToolTipString("Ayarlar� Kilitle")
        self.btn_kilitle.SetBitmapSelected(self.kilitle_pressed)
        self.top_sizer.Add(self.btn_kilitle,  (1,4),(1,1), wx.ALL)
        
        
        self.middle_panel =  wx.Panel(self, wx.ID_ANY ,size=(winSize[0],winSize[1]-140))
        middle_sizer=wx.FlexGridSizer(cols=1,hgap=10,vgap=10)
        self.middle_panel.SetSizer(middle_sizer)
        self.middle_panel.SetBackgroundColour( wx.Colour(254,254,254))
        middle_panel_image=wx.Image("%syazilim_image.png"%image_path,wx.BITMAP_TYPE_ANY)
        sb1 = wx.StaticBitmap(self.middle_panel, -1, wx.BitmapFromImage(middle_panel_image))
        
        middle_sizer.Add(sb1,1,wx.ALL|wx.CENTER)      

        mainSizer.Add( self.top_panel,(0,0),(1,1),wx.ALL)
        mainSizer.Add( self.middle_panel,(1,0),(1,1),wx.ALL)
        self.Thaw()
    
    def OnRestart(self,event):
        #os.system("taskkill /IM mainWindow.exe /f")
        r = param.get_root( self )
        t = r.c_thread.kill()
        process_end ( sabitler.main_process )
        r.Destroy()
    def OnAyarlariKilitle( self, event ):
        mainW = param.get_root( self )
	self.GetParent().login = False
	for kthred in self.thread_list:
            kthred.kill()
        self.Destroy()
        mainW.btn_ayarlar.Destroy()
        mainW.SetSize(( mainW.GetSize().width, mainW.GetSize().height - sabitler.ana_button_size))
        mainW.Layout()
    def OnAbout(self, event):
        self.SetTransparent(150)
        dlg = wx.MessageDialog( self, "Copyright � ", wx.OK)
        dlg.ShowModal() 
        dlg.Destroy()
        self.SetTransparent(255)
        
    def OnExit(self,e):
        #self.onay_message("��kmak �stedi�inizden Emin misiniz ?","Kapat")
        self.Show(False)
        
    def OnCloseWindow(self,e):
	mainW = param.get_root( self )
        for kthred in self.thread_list:
            kthred.kill()
	self.GetParent().login = False
	mainW.btn_ayarlar.Destroy()
        mainW.SetSize(( mainW.GetSize().width, mainW.GetSize().height - sabitler.ana_button_size))
        mainW.Layout()
        self.Destroy()
        
    def onSelect(self, event):
        """"""
        #print dir( event )
        #print "You selected: " + self.isin_adi_cmb.GetStringSelection()
        obj = self.isin_adi_cmb.GetClientData(self.isin_adi_cmb.GetSelection())
        text = """
        The object's attributes are:
        %s  %s
 
        """ % (obj.id, obj.ad)
        #print text
        
    def onay_message( self,message,win_name):
        self.SetTransparent(150)
        dlg = wx.MessageDialog(self, message,
                               win_name,
                               wx.OK  |wx.CANCEL | wx.ICON_INFORMATION
                               )
        
        retCode = dlg.ShowModal()
        if (retCode == wx.ID_OK):
            self.Show( False )
        dlg.Destroy()
        self.SetTransparent(255)
    
    def OnAyarlarMenuPressed( self , event ):
        
        id_  = event.GetEventObject().GetId()
        fullscreen = False
        if id_ == id_b_otomasyon_ayarlari_ekrani:
            winClass   = win_Otomasyon_Ayarlari
            title      = 'Otomasyon Ayarlar�'
        elif id_ == id_b_bayii_tanimlama:
            winClass   = win_Bayii_Tanimlama
            title      = 'Bayi Tan�mlama'
        elif id_ == id_b_yakit_tanimlama:
            winClass = win_Yakit_Tanimlama
            title    = 'Yak�t Tan�mlama'
            fullscreen = True
        elif id_ == id_b_pompa_tanimlari:
            winClass = win_Pompa_Tanimlama
            title    = 'Pompa Tan�mlama'
        elif id_   == id_b_tank_tanimlari:
            winClass = win_Tank_Tanimlama
            title    = 'Tank Tan�mlama'
        elif id_ == id_b_tanimlama_gonder_al:
            winClass = win_Tanimlamalari_Gonder_Al
            title    = 'Tan�mlamalar� G�nder / Al'
        elif id_ == id_b_port_tanimlama:
            winClass = win_port_tanimla
            title    = 'Port Tan�mlama'
            fullscreen = True
        elif id_==id_b_otomasyon_raporu:
            winClass = win_Otomasyon_Raporu
            title    = 'Otomasyon Raporu'
	    fullscreen = True
            
        self.O.newWindow(self,event,winClass,title,maximize=fullscreen)
class win_Tank_Tanimlama(wx.Frame) :
    
    def __init__(self, parent, id_, title):

        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR|wx.EXPAND ))
        self.root = param.get_root( self )
        self.db = self.root.DB
        self.O  = self.root.O
        self.selected_item = None
        self.dummy = sabitler.dummy()
        self.LayoutItems()
        
    def LayoutItems(self):

        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.mainSizer)
        #self.book = wx.Notebook(self, style=wx.NB_TOP )
        self.book  = aui.AuiNotebook(self,1,agwStyle=(aui.AUI_NB_TOP|aui.AUI_NB_SCROLL_BUTTONS))
        self.book.SetArtProvider(aui.AuiDefaultTabArt())
        s.setPanelListBg( self.book )
        self.mainSizer.Add(self.book, 1,wx.ALL | wx.EXPAND)
        
        self.p  = self.createAddProbe()
        s.setPanelListBg( self.p )
        self.p2 = self.createProbeList()
        s.setPanelListBg( self.p2 )
        self.p3 = self.createAddTank()
        s.setPanelListBg( self.p3 )
        self.p4 = self.createTankList()
        s.setPanelListBg( self.p4 )
        self.p5 = self.createAddMinMax()
        s.setPanelListBg( self.p5 )
        self.p6 = self.createTankKalibrasyonList()
        s.setPanelListBg( self.p6 )
        
        self.book.AddPage( self.p, "Probe Ekle",True )
        self.book.AddPage( self.p2,"Probe Listesi" )
        self.book.AddPage( self.p3,"Yak�t Tank Ekle" )
        self.book.AddPage( self.p4,"Tank Listesi" )
        self.book.AddPage( self.p5,"Min Max Ekle" )
        self.book.AddPage( self.p6,"Kalibrasyon Listesi" )
        self.book.Bind(aui.EVT_AUINOTEBOOK_PAGE_CHANGED, self.OnBookPageChanged )
        
        il   = wx.ImageList(64, 64)
        self.img0 = il.Add(wx.Bitmap('%sprobe_ekleme_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img1 = il.Add(wx.Bitmap('%sprobe_liste_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img2 = il.Add(wx.Bitmap('%stank_ekle_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img3 = il.Add(wx.Bitmap('%stank_list_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img4 = il.Add(wx.Bitmap('%sminmax.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img5 = il.Add(wx.Bitmap('%skalibrasyon_list_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.book.AssignImageList(il)
 
        self.book.SetPageImage(0, self.img0)
        self.book.SetPageImage(1, self.img1)
        self.book.SetPageImage(2, self.img2)
        self.book.SetPageImage(3, self.img3)
        self.book.SetPageImage(4, self.img4)
        self.book.SetPageImage(5, self.img5)
 
    def createAddMinMax(self):
        self.panel_minmax = wx.Panel(self.book,1, style = wx.TAB_TRAVERSAL
             | wx.CLIP_CHILDREN
             | wx.FULL_REPAINT_ON_RESIZE
             |wx.EXPAND)
        
        self.gbs_add_minmax =  wx.GridBagSizer(10, 0)
        
        lbl_yakit_tanki       = wx.StaticText(self.panel_minmax, label="Yak�t Tank� :  ")
        s.setStaticTextList( lbl_yakit_tanki )
        self.cmb_yakit_tanki  = wx.ComboBox(self.panel_minmax,1,"", choices=[],
                                            style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY)) 
        self.cmb_yakit_tanki.Bind( wx.EVT_COMBOBOX , self.OnYakitTankiChanged )
        yakit_tanki_parametric_objects = []
        yakit_tanki_parametric_objects = self.db.get_parameters_all("YAKIT_TANKI",where="ID_ACTIVE = 1",order="YAKIT_TANKI_ADI",selects="ID_YAKIT_TANKI,YAKIT_TANKI_ADI")
        if yakit_tanki_parametric_objects != [] :
            self.db.widgetMaker( self.cmb_yakit_tanki , yakit_tanki_parametric_objects)   

        lbl_yakit_dusuk_seviye       = wx.StaticText(self.panel_minmax, label="Yak�t D���k Seviyesi :  ")
        s.setStaticTextList( lbl_yakit_dusuk_seviye )
        self.txt_yakit_dusuk_seviye  = s.CheckNumberMasked(self.panel_minmax,'yakit_dusuk_seviye',4)#wx.TextCtrl  (self.panel_minmax, 1, "",size=(180,-1))
        txt_yakit_dusuk_seviye_cins = wx.StaticText(self.panel_minmax,label="mm")
        s.setStaticTextList( txt_yakit_dusuk_seviye_cins )

        lbl_yakit_cok_dusuk_seviye  = wx.StaticText(self.panel_minmax, label="Yak�t �ok D���k Seviye :  ")
        s.setStaticTextList( lbl_yakit_cok_dusuk_seviye )
        self.txt_yakit_cok_dusuk_seviye  = s.CheckNumberMasked(self.panel_minmax,'yakit_cok_dusuk_seviye',4)#wx.TextCtrl  (self.panel_minmax, 1, "",size=(180,-1))
        txt_yakit_cok_dusuk_seviye_cins = wx.StaticText(self.panel_minmax,label="mm")
        s.setStaticTextList( txt_yakit_cok_dusuk_seviye_cins )
        txt_yakit_cok_dusuk_seviye_zorunlu = wx.StaticText(self.panel_minmax,label="* Zorunlu Alan",style=wx.ALIGN_LEFT)
        s.setStaticTextList( txt_yakit_cok_dusuk_seviye_zorunlu,color=(255,0,0) )

        lbl_yakit_yuksek_seviye       = wx.StaticText(self.panel_minmax, label="Yak�t Y�ksek Seviye :  ")
        s.setStaticTextList( lbl_yakit_yuksek_seviye )
        self.txt_yakit_yuksek_seviye  = s.CheckNumberMasked(self.panel_minmax,'yakit_y�ksek_seviye',4)#wx.TextCtrl  (self.panel_minmax, 1, "",size=(180,-1))
        txt_yakit_yuksek_seviye_cins = wx.StaticText(self.panel_minmax,label="mm")
        s.setStaticTextList( txt_yakit_yuksek_seviye_cins )
                        
        lbl_yakit_cok_yuksek       = wx.StaticText(self.panel_minmax, label="Yak�t �ok Y�ksek Seviye :  ")
        s.setStaticTextList( lbl_yakit_cok_yuksek )
        self.txt_yakit_cok_yuksek_seviye  = s.CheckNumberMasked(self.panel_minmax,'yakit_cok_yuksek_seviye',4)#wx.TextCtrl  (self.panel_minmax, 1, "",size=(180,-1))
        txt_yakit_cok_yuksek_seviye = wx.StaticText(self.panel_minmax,label="mm")
        s.setStaticTextList( txt_yakit_cok_yuksek_seviye )
        txt_yakit_cok_yuksek_seviye_zorunlu = wx.StaticText(self.panel_minmax,label="* Zorunlu Alan",style=wx.ALIGN_LEFT)
        s.setStaticTextList( txt_yakit_cok_yuksek_seviye_zorunlu,color=(255,0,0) )
        
        lbl_su_yuksek_seviye       = wx.StaticText(self.panel_minmax, label="Su Y�ksek Seviye :  ")
        s.setStaticTextList( lbl_su_yuksek_seviye )
        self.txt_su_yuksek_seviye  = s.CheckNumberMasked(self.panel_minmax,'su_yuksek_seviye',4)#wx.TextCtrl  (self.panel_minmax, 1, "",size=(180,-1))
        txt_su_yuksek_seviye_cins = wx.StaticText(self.panel_minmax,label="mm")
        s.setStaticTextList( txt_su_yuksek_seviye_cins )
        
        lbl_su_cok_yuksek_seviye       = wx.StaticText(self.panel_minmax, label="Su �ok Y�ksek Seviyesi :  ")
        s.setStaticTextList( lbl_su_cok_yuksek_seviye )
        self.txt_su_cok_yuksek_seviye  = s.CheckNumberMasked(self.panel_minmax,'su_cok_yuksek_seviye',4)#wx.TextCtrl  (self.panel_minmax, 1, "",size=(180,-1))
        txt_su_cok_yuksek_seviye_cins = wx.StaticText(self.panel_minmax,label="mm")
        s.setStaticTextList( txt_su_cok_yuksek_seviye_cins )
        txt_su_cok_yuksek_seviye_zorunlu = wx.StaticText(self.panel_minmax,label="* Zorunlu Alan",style=wx.ALIGN_LEFT)
        s.setStaticTextList( txt_su_cok_yuksek_seviye_zorunlu,color=(255,0,0) )
        
        lbl_yakit_ihtiyac_miktari       = wx.StaticText(self.panel_minmax, label="Yak�t �htiya� Miktar� :  ")
        s.setStaticTextList( lbl_yakit_ihtiyac_miktari )
        self.txt_yakit_ihtiyac_miktari  = s.CheckNumberMasked(self.panel_minmax,'yakit_ihtiyac_miktar',6)#wx.TextCtrl  (self.panel_minmax, 1, "",size=(180,-1))
        txt_yakit_ihtiyac_miktari_cins = wx.StaticText(self.panel_minmax,label="lt")
        s.setStaticTextList( txt_yakit_ihtiyac_miktari_cins )
        
        self.btn_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()        
        self.btn_save  = LibButtons.ThemedGenBitmapTextButton(self.panel_minmax, id=wx.NewId(), bitmap=image_save,label="Verileri Kaydet")
        self.btn_save.Bind(wx.EVT_BUTTON, self.OnMinMaxSave)
        self.btn_save.SetToolTipString("Max-Min Bilgilerini Kaydet")
        self.btn_save.SetBitmapSelected(self.btn_save_pressed)
        s.setButtonFont( self.btn_save )
        
        self.btn_delete_pressed  =  wx.Image("%sdelete_green.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.image_delete = wx.Image("%sdelete.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_delete  = LibButtons.ThemedGenBitmapTextButton(self.panel_minmax, id=wx.NewId(), bitmap=self.image_delete,label="Verileri Sil" , style=wx.ALIGN_LEFT)
        self.btn_delete.Bind(wx.EVT_BUTTON, self.OnDeleteMinMaxCheck)
        self.btn_delete.SetToolTipString("Se�ili Yak�t Tank� ��in Min-Max De�erlerini Sil")
        self.btn_delete.SetBitmapSelected(self.btn_delete_pressed)
        s.setButtonFont( self.btn_delete )
        
        
        self.gbs_add_minmax.Add( lbl_yakit_tanki, (1,1),(1,1),  wx.ALL,1)
        self.gbs_add_minmax.Add( self.cmb_yakit_tanki, (1,2) ,(1,1),  wx.ALL,1)
        
        self.gbs_add_minmax.Add( lbl_yakit_dusuk_seviye, (2,1),(1,1),  wx.ALL,1)
        self.gbs_add_minmax.Add( self.txt_yakit_dusuk_seviye , (2,2) ,(1,1),  wx.ALL,1)
        self.gbs_add_minmax.Add( txt_yakit_dusuk_seviye_cins, (2,3),(1,1),  wx.ALL,1)
        
        self.gbs_add_minmax.Add( lbl_yakit_cok_dusuk_seviye, (3,1),(1,1),  wx.ALL,1)
        self.gbs_add_minmax.Add( self.txt_yakit_cok_dusuk_seviye, (3,2) ,(1,1),  wx.ALL,1)
        self.gbs_add_minmax.Add( txt_yakit_cok_dusuk_seviye_cins, (3,3),(1,1),  wx.ALL,1)
        self.gbs_add_minmax.Add( txt_yakit_cok_dusuk_seviye_zorunlu, (3,4),(1,1),wx.ALL,1)

        self.gbs_add_minmax.Add( lbl_yakit_yuksek_seviye, (4,1),(1,1),  wx.ALL,1)
        self.gbs_add_minmax.Add( self.txt_yakit_yuksek_seviye, (4,2) ,(1,1),  wx.ALL,1)
        self.gbs_add_minmax.Add( txt_yakit_yuksek_seviye_cins, (4,3),(1,1),  wx.ALL,1)
        
        self.gbs_add_minmax.Add( lbl_yakit_cok_yuksek, (5,1),(1,1),  wx.ALL,1)
        self.gbs_add_minmax.Add( self.txt_yakit_cok_yuksek_seviye, (5,2) ,(1,1),  wx.ALL,1)
        self.gbs_add_minmax.Add( txt_yakit_cok_yuksek_seviye, (5,3),(1,1),  wx.ALL,1)
        self.gbs_add_minmax.Add( txt_yakit_cok_yuksek_seviye_zorunlu, (5,4),(1,1),wx.ALL,1)
        
        self.gbs_add_minmax.Add( lbl_su_yuksek_seviye, (6,1),(1,1),  wx.ALL,1)
        self.gbs_add_minmax.Add( self.txt_su_yuksek_seviye, (6,2) ,(1,1),  wx.ALL,1)
        self.gbs_add_minmax.Add( txt_su_yuksek_seviye_cins, (6,3),(1,1),  wx.ALL,1)

        self.gbs_add_minmax.Add( lbl_su_cok_yuksek_seviye, (7,1),(1,1),  wx.ALL,1)
        self.gbs_add_minmax.Add( self.txt_su_cok_yuksek_seviye, (7,2) ,(1,1),  wx.ALL,1)
        self.gbs_add_minmax.Add( txt_su_cok_yuksek_seviye_cins, (7,3),(1,1),  wx.ALL,1)
        self.gbs_add_minmax.Add( txt_su_cok_yuksek_seviye_zorunlu, (7,4),(1,1),wx.ALL,1)
        
        self.gbs_add_minmax.Add( lbl_yakit_ihtiyac_miktari, (8,1),(1,1),  wx.ALL,1)
        self.gbs_add_minmax.Add( self.txt_yakit_ihtiyac_miktari, (8,2) ,(1,1),  wx.ALL,1)
        self.gbs_add_minmax.Add( txt_yakit_ihtiyac_miktari_cins, (8,3),(1,1),  wx.ALL,1)
        
        self.gbs_add_minmax.Add(self.btn_save,  (9,1),(1,1),wx.ALL,1)        
        self.gbs_add_minmax.Add(self.btn_delete, (9,2),(1,2),wx.ALL,1)
        
        self.panel_minmax.SetSizerAndFit(self.gbs_add_minmax)
        self.panel_minmax.SetAutoLayout(True)
        return self.panel_minmax

    def createProbeList( self ):

        headers        = ['ID,digit','Probe Marka,string','Probe Adres,string','Probe Uzunluk,digit','Speed OF Wire,digit']
        data_history   = self.db.get_raw_data_all("VIEW_PROBE",where = "AKTIF_PROBE=1" ,order = "ID_PROBE",selects="ID_PROBE,PROBE_MARKA_ADI,PROBE_ADRES,PROBE_UZUNLUK,SPEED_OF_WIRE")  
        p_list = IndexListPanel( self.book, headers=headers, data = data_history,table_name='PROBE',where='ID_PROBE',delete_button=True,guncelle_button=True,guncellemede_gidilecek_sayfa=0)

        return p_list

    
    def createAddProbe (self ):
        
        selected_item             = self.selected_item
        self.dummy.PROBE_ADRES    = ''
        self.dummy.PROBE_MARKA    = ''
        self.dummy.PROBE_UZUNLUK  = ''
        self.dummy.SPEED_OF_WIRE  = ''
        data_probe                = self.dummy

        if selected_item :
            data_probe = self.db.get_raw_data_all("PROBE",selects="ID_PROBE,PROBE_ADRES,PROBE_MARKA,PROBE_UZUNLUK,SPEED_OF_WIRE",where="ID_PROBE=%s"%selected_item,dict_mi='object')[0]

        self.panel_probe = wx.Panel(self.book,1, style = wx.TAB_TRAVERSAL
             | wx.CLIP_CHILDREN
             | wx.FULL_REPAINT_ON_RESIZE
             |wx.EXPAND)
        
        self.gbs_add_probe =  wx.GridBagSizer(10, 0)

        lbl_probe_adres       = wx.StaticText(self.panel_probe, label="Probe Adresi :  ")
        s.setStaticTextList( lbl_probe_adres )
        self.txt_probe_adres  = wx.TextCtrl  (self.panel_probe, 1, str(data_probe.PROBE_ADRES),size=(180,-1))
        #self.txt_probe_adres.Bind(wx.EVT_CHAR,s.CheckNumber)
        
        lbl_probe_marka       = wx.StaticText(self.panel_probe, label="Probe Markas� :  ")
        s.setStaticTextList( lbl_probe_marka )
        choices_probe_marka = []
        
        self.cmb_probe_marka  = wx.ComboBox(self.panel_probe,1,"", choices=choices_probe_marka,
                                            style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY))
        choices_probe_marka = self.db.get_parameters_all("P_PROBE_MARKA",where="ID_ACTIVE = 1",order="PROBE_MARKA_ADI",selects="ID_PROBE_MARKA,PROBE_MARKA_ADI")
        if choices_probe_marka != [] :
            self.db.widgetMaker( self.cmb_probe_marka , choices_probe_marka)
        
        if data_probe.PROBE_MARKA == '':
            probe_index = -1
        else:
            probe_index = choices_probe_marka.index(filter(lambda x:  x.id==data_probe.PROBE_MARKA,choices_probe_marka)[0])
            
        self.cmb_probe_marka.SetSelection(probe_index)
        
        lbl_probe_uzunlugu       = wx.StaticText(self.panel_probe, label="Probe Uzunlu�u :  ")
        s.setStaticTextList( lbl_probe_uzunlugu )
        self.txt_probe_uzunlugu  = s.CheckNumberMasked(self.panel_probe,'probe_uzunluk',4)#wx.TextCtrl  (self.panel_probe, 1,str(data_probe.PROBE_UZUNLUK),size=(180,-1))
        self.txt_probe_uzunlugu.SetValue(str(data_probe.PROBE_UZUNLUK))       
        txt_probe_uzunlugu_cinsi = wx.StaticText(self.panel_probe,label="mm")
        s.setStaticTextList( txt_probe_uzunlugu_cinsi )
                 
        lbl_speed_of_wire       = wx.StaticText(self.panel_probe, label="Speed Of Wire :  ")
        s.setStaticTextList( lbl_speed_of_wire )
        self.txt_speed_of_wire  = wx.TextCtrl  (self.panel_probe, 1, str(data_probe.SPEED_OF_WIRE),size=(180,-1))        
        self.txt_speed_of_wire.Bind(wx.EVT_CHAR,s.CheckNumber)
        self.btn_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        
        self.btn_save  = LibButtons.ThemedGenBitmapTextButton(self.panel_probe, id=wx.NewId(), bitmap=image_save,label="Verileri Kaydet")
        self.btn_save.Bind(wx.EVT_BUTTON,lambda event: self.OnProbeSave(event,selected_item))
        self.btn_save.SetToolTipString("Probe Bilgilerini Kaydet")
        self.btn_save.SetBitmapSelected(self.btn_save_pressed)
        s.setButtonFont( self.btn_save )
       
        self.gbs_add_probe.Add( lbl_probe_adres, (1,1),(1,1),  wx.ALL,3)
        self.gbs_add_probe.Add( self.txt_probe_adres, (1,2) ,(1,1),  wx.ALL,3)
        
        self.gbs_add_probe.Add( lbl_probe_marka, (2,1),(1,1),  wx.ALL,3)
        self.gbs_add_probe.Add( self.cmb_probe_marka, (2,2) ,(1,1),  wx.ALL,3)
        
        self.gbs_add_probe.Add( lbl_probe_uzunlugu, (3,1),(1,1),  wx.ALL,3)
        self.gbs_add_probe.Add( self.txt_probe_uzunlugu, (3,2) ,(1,1),  wx.ALL,3)
        self.gbs_add_probe.Add( txt_probe_uzunlugu_cinsi, (3,3),(1,2),  wx.ALL,3)

        self.gbs_add_probe.Add( lbl_speed_of_wire, (4,1),(1,1),  wx.ALL,3)
        self.gbs_add_probe.Add( self.txt_speed_of_wire, (4,2) ,(1,1),  wx.ALL,3)       

        self.gbs_add_probe.Add(self.btn_save,  (5,1),(1,1),wx.ALL,1)        
        
        self.panel_probe.SetSizerAndFit(self.gbs_add_probe)
        #self.panel_probe.SetAutoLayout(True)    
        return self.panel_probe

    def OnYakitTankiChanged(self,event ):
        if self.cmb_yakit_tanki.GetSelection() != -1:
            yakit_tanki = self.cmb_yakit_tanki.GetClientData(self.cmb_yakit_tanki.GetSelection()).id
            data = self.db.get_raw_data_all(table_name="YAKIT_TANKI_MIN_MAX",
                                                        where="ID_YAKIT_TANKI=%s"%yakit_tanki,
                        selects='ID_YAKIT_TANKI,YAKIT_DUSUK_SEVIYE,YAKIT_COK_DUSUK_SEVIYE,YAKIT_YUKSEK_SEVIYE,YAKIT_COK_YUKSEK_SEVIYE,SU_YUKSEK_SEVIYE,SU_COK_YUKSEK_SEVIYE,YAKIT_IHTIYAC_MIKTARI',dict_mi='object')
            if data != []:
                #self.cmb_yakit_tanki.GetClientData(self.cmb_yakit_tanki.GetSelection()).id 
                self.txt_yakit_dusuk_seviye.SetValue(str(data[0].YAKIT_DUSUK_SEVIYE))
                self.txt_yakit_cok_dusuk_seviye.SetValue(str(data[0].YAKIT_COK_DUSUK_SEVIYE)) 
                self.txt_yakit_yuksek_seviye.SetValue(str(data[0].YAKIT_YUKSEK_SEVIYE) )
                self.txt_yakit_cok_yuksek_seviye.SetValue(str(data[0].YAKIT_COK_YUKSEK_SEVIYE)) 
                self.txt_su_yuksek_seviye.SetValue(str(data[0].SU_YUKSEK_SEVIYE) )
                self.txt_su_cok_yuksek_seviye.SetValue(str(data[0].SU_COK_YUKSEK_SEVIYE) )
                self.txt_yakit_ihtiyac_miktari.SetValue(str(data[0].YAKIT_IHTIYAC_MIKTARI) )
            else:
                self.TemizleMinMax()
            
    def OnMinMaxSave(self,event ):
        message = ''
        hata = 0
        message_=''
        
        if self.cmb_yakit_tanki.GetSelection() == -1:
            s.uyari_message(self,"L�tfen Yak�t Tank�n� Se�iniz. ","Min-Max Tan�mlama")
            hata = 1
            
        if self.txt_yakit_cok_dusuk_seviye.GetValue().strip() == '':
            message_ += 'Yak�t �ok D���k Seviye,'
            hata = 2
        if self.txt_yakit_cok_yuksek_seviye.GetValue().strip() == '':
            message_ += ' Yak�t �ok Y�ksek Seviye,'
            hata = 2
        if self.txt_su_cok_yuksek_seviye.GetValue().strip() == '':
            message_ += ' Su �ok Y�ksek Seviye,'
            hata = 2
        if hata == 2:
            message_ = message_[0:len(message_)-1]
            s.uyari_message(self,"L�tfen %s Alanlar�n� Doldurunuz. "%message_,"Min-Max Tan�mlama")
        if hata == 0:
            yakit_tanki              = self.cmb_yakit_tanki.GetClientData(self.cmb_yakit_tanki.GetSelection()).id 
            yakit_dusuk_seviye       = self.txt_yakit_dusuk_seviye.GetValue().strip()
            yakit_cok_dusuk_seviye   = self.txt_yakit_cok_dusuk_seviye.GetValue().strip()
            yakit_yuksek_seviye      = self.txt_yakit_yuksek_seviye.GetValue().strip()
            yakit_cok_yuksek_seviye  = self.txt_yakit_cok_yuksek_seviye.GetValue().strip()
            su_yuksek_seviye         = self.txt_su_yuksek_seviye.GetValue().strip()
            su_cok_yuksek_seviye     = self.txt_su_cok_yuksek_seviye.GetValue().strip()
            yakit_ihtiyac_miktari    = self.txt_yakit_ihtiyac_miktari.GetValue().strip()
            
            if yakit_dusuk_seviye=='' and yakit_cok_dusuk_seviye=='' and yakit_yuksek_seviye=='' and yakit_cok_yuksek_seviye=='' and su_yuksek_seviye=='' and su_cok_yuksek_seviye=='' and yakit_ihtiyac_miktari=='':
                s.uyari_message(self,"L�tfen Yak�t Tank� Alan� D���ndaki Verilerden En Az Birini Giriniz."  ,"Min-Max Tan�mlama")       
                hata = 1
            if hata ==0:
                if yakit_dusuk_seviye == '':
                    yakit_dusuk_seviye ='null'
                if yakit_cok_dusuk_seviye == '':
                    yakit_cok_dusuk_seviye ='null'
                if yakit_yuksek_seviye == '':
                    yakit_yuksek_seviye ='null'
                if yakit_cok_yuksek_seviye == '':
                    yakit_cok_yuksek_seviye ='null'
                if su_yuksek_seviye == '':
                    su_yuksek_seviye ='null'
                if su_cok_yuksek_seviye == '':
                    su_cok_yuksek_seviye ='null'
                if yakit_ihtiyac_miktari == '':
                    yakit_ihtiyac_miktari='null'
        
                ##veri tabaninda olup olmadigini kontrol edelim##
                check_var_mi = self.db.get_raw_data_all(table_name="YAKIT_TANKI_MIN_MAX",
                                                        where="ID_YAKIT_TANKI=%s"%yakit_tanki,selects='ID_TANK_MIN_MAX')
                self.table_name_min_max = "YAKIT_TANKI_MIN_MAX"
                self.column_name_min_max="ID_YAKIT_TANKI,YAKIT_DUSUK_SEVIYE,YAKIT_COK_DUSUK_SEVIYE,YAKIT_YUKSEK_SEVIYE,YAKIT_COK_YUKSEK_SEVIYE,SU_YUKSEK_SEVIYE,SU_COK_YUKSEK_SEVIYE,YAKIT_IHTIYAC_MIKTARI"
                self.values_min_max = "%s,%s,%s,%s,%s,%s,%s,%s"%(yakit_tanki,yakit_dusuk_seviye,yakit_cok_dusuk_seviye,yakit_yuksek_seviye,
                                 yakit_cok_yuksek_seviye,su_yuksek_seviye,su_cok_yuksek_seviye,yakit_ihtiyac_miktari   ) 

                if check_var_mi == []:
                    data_id = self.db.save_data_and_get_id ( table_name= self.table_name_min_max,column_name = self.column_name_min_max,values = self.values_min_max)           
                                
                    if data_id !='' :                    
                        s.uyari_message(self,"Eklemek �stedi�iniz Min-Max De�erleri %s �simli Yak�t Tank� ��in Sisteme Ba�ar�yla Kaydedilmi�tir."%str(self.cmb_yakit_tanki.GetValue()),"Min-Max Tan�mlama")    
                    else:
                        s.uyari_message(self,"Eklemek �stedi�iniz Min-Max Sisteme Kaydedilemedi. Sistem Y�neticiniz ile G�r���n�z.","Min-Max Tan�mlama")
                else:
                    self.where_min_max = "ID_TANK_MIN_MAX = %s"%check_var_mi[0][0]
                    s.ok_cancel_message(self,"Eklemek �stedi�iniz Min-Max De�erleri %s �simli Yak�t Tank� ��in Sistemde Mevcuttur."%str(self.cmb_yakit_tanki.GetValue())+
                                        "Tamam Tu�una Basarsan�z Min-Max G�ncellenecektir. "+
                                        "�ptal Tu�una Basarak �ptal Edebilirsiniz"
                        ,"Min-Max Tan�mlama",ok_function = self.MinMaxGuncelle,cancel_function=self.MinMaxGuncelleme)
                  
    def OnDeleteMinMaxCheck(self,event ):
        
        if self.cmb_yakit_tanki.GetSelection() != -1:
            
            self.yakit_tanki_id = self.cmb_yakit_tanki.GetClientData(self.cmb_yakit_tanki.GetSelection()).id
            yakit_tanki_adi     = self.cmb_yakit_tanki.GetClientData(self.cmb_yakit_tanki.GetSelection()).ad
            check_var_mi        = self.db.get_raw_data_all(table_name="YAKIT_TANKI_MIN_MAX",
                                                    where="ID_YAKIT_TANKI=%s"%self.yakit_tanki_id,selects='ID_TANK_MIN_MAX')
            
            if check_var_mi != []:
                self.yakit_tanki_min_max_id = check_var_mi[0][0]
                s.ok_cancel_message(self,"%s �simli Yak�t Tank� ��in Sistemde Tan�ml� Min-Max De�erlerini Silmek �stiyormusunuz"%yakit_tanki_adi,
                        "Min-Max Tan�mlama",ok_function = self.OnDeleteMinMax,cancel_function=self.OnNoDeleteMinMax)
                     
    def OnDeleteMinMax(self):
        user=self.root.user
        data_delete = self.db.delete_data(table_name="YAKIT_TANKI_MIN_MAX",where="ID_TANK_MIN_MAX=%s"%self.yakit_tanki_min_max_id,user=user )
        if data_delete !='' :                    
            s.uyari_message(self,"Silme ��lemi Ba�ar�yla Ger�ekle�tirilmi�tir.","Min-Max Tan�mlama")
            self.TemizleMinMax()
        else:
            s.uyari_message(self,"Silmek �stedi�iniz Min-Max Sisteme Kaydedilemedi. Sistem Y�neticiniz ile G�r���n�z.","Min-Max Tan�mlama")

    def OnNoDeleteMinMax(self):
        pass

    def TemizleMinMax(self):
        
        #self.cmb_yakit_tanki.GetClientData(self.cmb_yakit_tanki.GetSelection()).id 
        self.txt_yakit_dusuk_seviye.SetValue('')
        self.txt_yakit_cok_dusuk_seviye.SetValue('') 
        self.txt_yakit_yuksek_seviye.SetValue('')
        self.txt_yakit_cok_yuksek_seviye.SetValue('') 
        self.txt_su_yuksek_seviye.SetValue('')
        self.txt_su_cok_yuksek_seviye.SetValue('')
        self.txt_yakit_ihtiyac_miktari.SetValue('')
            
    def OnProbeSave(self,event,update=None):
        user=self.root.user
        message = ''
        probe_adres            = self.txt_probe_adres.GetValue().strip()
        probe_marka_select     = self.cmb_probe_marka.GetSelection()
        probe_uzunluk          = self.txt_probe_uzunlugu.GetValue().strip()
        speed_of_wire          = self.txt_speed_of_wire.GetValue().strip()
        if speed_of_wire == '':
            speed_of_wire = 0 
        if probe_adres == '':
            message = "L�tfen Probe Adresini Giriniz.\n"         
        if probe_marka_select == -1 :
            message += "L�tfen Probe Markas�n� Se�iniz.\n"
        if probe_uzunluk == '':
            message += "L�tfen Probe Uzunlu�unu Giriniz.\n"
        if message != '' :
            s.uyari_message(self,message,"Probe Tan�mlama")
        else:
            probe_marka            = self.cmb_probe_marka.GetClientData(self.cmb_probe_marka.GetSelection()).id
            #print 0
            if update:
                #print 1
                ##veri tabaninda olup olmadigini kontrol edelim##
                check_var_mi = self.db.get_raw_data_all(table_name="PROBE",
                                                        where="PROBE_ADRES=%s AND ID_PROBE !=%s"%(probe_adres,update),selects='PROBE_ADRES')
                if check_var_mi ==[]:
                    #print 2
                    data_id = self.db.update_data(table_name="PROBE",where="ID_PROBE=%s"%update,column_name="PROBE_MARKA,PROBE_UZUNLUK,PROBE_ADRES,SPEED_OF_WIRE",values="'%s',%s,%s,%s"%(probe_marka,probe_uzunluk,probe_adres,speed_of_wire),user=user)
                    if data_id !='' :
                        #print 6
                        s.uyari_message(self,"G�ncellemek �stedi�iniz Probe Sisteme Ba�ar�yla Kaydedildi.","Probe Tan�mlama")    
                    else:
                        #print 7
                        s.uyari_message(self,"G�ncellemek �stedi�iniz Probe Sisteme Kaydedilemedi. Sistem Y�neticiniz ile G�r���n�z.","Probe Tan�mlama")
                else:
                    #print 3
                    s.uyari_message(self,"G�ncellemek �stedi�iniz Probe Adresi: %s Sistemde Mevcuttur."%(probe_adres),"Probe Tan�mlama")
            else:
                #print 4
                check_var_mi = self.db.get_raw_data_all( table_name="PROBE",selects="ID_PROBE",where="PROBE_ADRES='%s' and PROBE_MARKA=%s"%(probe_adres,probe_marka) ) 
                if check_var_mi == []:
                    #print 5
                    data_id = self.db.save_data_and_get_id ( table_name="PROBE" ,column_name="PROBE_MARKA,PROBE_UZUNLUK,PROBE_ADRES,SPEED_OF_WIRE",
                                                            values="%s,%s,'%s',%s"%(probe_marka,probe_uzunluk,probe_adres,speed_of_wire),user=user )
                    if data_id !='' :
                        s.uyari_message(self,"Eklemek �stedi�iniz Probe Sisteme Ba�ar�yla Kaydedildi.","Probe Tan�mlama")    
                    else:
                        s.uyari_message(self,"Eklemek �stedi�iniz Probe Sisteme Kaydedilemedi. Sistem Y�neticiniz ile G�r���n�z.","Probe Tan�mlama")
                else:
                    #print 8
                    s.uyari_message(self,"Eklemek �stedi�iniz Probe Adresi: %s Sistemde Mevcuttur."%probe_adres,"Probe Tan�mlama")              
     
    def createTankList( self ):
       
        headers        = ['ID,digit','Yak�t Tank� Ad�,string','Yak�t Tank� Y�kseklik,digit','Yak�t Tank� Kapasitesi,digit','Probe Adres,string','Probe Markas�,string','Port Ad�,string','Yak�t Ad�,string','Probe Ba�lang�� Seviye,digit']
        data_history   = self.db.get_raw_data_all("VIEW_YAKIT_TANKI",order = "YAKIT_TANKI_ADI",
                         selects="ID_YAKIT_TANKI,YAKIT_TANKI_ADI,YAKIT_TANKI_YUKSEKLIK,YAKIT_TANKI_KAPASITE,PROBE_ADRES,PROBE_MARKA,PORT_ADI,YAKIT_ADI,PROBE_BASLANGIC_SEVIYE")  

        list_pane = IndexListPanel( self.book, data = data_history, headers= headers,table_name='YAKIT_TANKI',where='ID_YAKIT_TANKI',delete_button=True,guncelle_button=True,guncellemede_gidilecek_sayfa=2 )   
        return list_pane

    def createAddTank (self ):
        selected_item = self.selected_item
        if selected_item :
            data_ = self.db.get_raw_data_all("YAKIT_TANKI", selects="ID_YAKIT_TANKI,YAKIT_TANKI_ADI,YAKIT_TANKI_YUKSEKLIK,YAKIT_TANKI_KAPASITE,ID_PROBE,ID_PORT,ID_YAKIT,PROBE_BASLANGIC_SEVIYE",where='ID_YAKIT_TANKI=%s'%selected_item,dict_mi='object')[0]  
        else:
            self.dummy.YAKIT_TANKI_ADI       = ''
            self.dummy.YAKIT_TANKI_YUKSEKLIK = ''
            self.dummy.YAKIT_TANKI_KAPASITE = ''
            self.dummy.ID_PROBE = -1
            self.dummy.ID_PORT=-1
            self.dummy.ID_YAKIT=-1
            self.dummy.PROBE_BASLANGIC_SEVIYE=''
            data_ = self.dummy
            #self.dummy = None
        self.panel_yakit_tanki = wx.Panel(self.book,1, style = wx.TAB_TRAVERSAL
             | wx.CLIP_CHILDREN
             | wx.FULL_REPAINT_ON_RESIZE
             |wx.EXPAND)
        
        self.gbs_add_tank =  wx.GridBagSizer(10, 0)

        lbl_tank_adi       = wx.StaticText(self.panel_yakit_tanki, label="Tank Ad� :  ")
        s.setStaticTextList( lbl_tank_adi )
        self.txt_tank_adi  = wx.TextCtrl  (self.panel_yakit_tanki, 1, data_.YAKIT_TANKI_ADI,size=(180,-1))
        
        lbl_tank_yuksekligi   = wx.StaticText(self.panel_yakit_tanki, label="Tank Y�ksekli�i :  ")
        s.setStaticTextList( lbl_tank_yuksekligi )
        self.txt_tank_yuksekligi  = s.CheckNumberMasked(self.panel_yakit_tanki,'tank_yuksekligi',4)#wx.TextCtrl  (self.panel_yakit_tanki, 1, str(data_.YAKIT_TANKI_YUKSEKLIK),size=(180,-1))    
        self.txt_tank_yuksekligi.SetValue(str(data_.YAKIT_TANKI_YUKSEKLIK))
        txt_tank_yuksekligi_cinsi = wx.StaticText(self.panel_yakit_tanki,label="mm")
        s.setStaticTextList( txt_tank_yuksekligi_cinsi )
        
        lbl_tank_kapasitesi   = wx.StaticText(self.panel_yakit_tanki, label="Tank Kapasitesi :  ")
        s.setStaticTextList( lbl_tank_kapasitesi )
        self.txt_tank_kapasitesi  = s.CheckNumberMasked(self.panel_yakit_tanki,'tank_kapasitesi',6)#wx.TextCtrl  (self.panel_yakit_tanki, 1,str(data_.YAKIT_TANKI_KAPASITE) ,size=(180,-1))
        self.txt_tank_kapasitesi.SetValue(str(data_.YAKIT_TANKI_KAPASITE))
        txt_tank_kapasite_cinsi   = wx.StaticText (self.panel_yakit_tanki,label="lt")
        s.setStaticTextList( txt_tank_kapasite_cinsi )
        
        lbl_probe                = wx.StaticText(self.panel_yakit_tanki, label="Probe :  ")
        s.setStaticTextList( lbl_probe )
        txt_tank_kapasite_cinsi_aciklama = wx.StaticText(self.panel_yakit_tanki,label="* Probe : Probe Adres - Probe Marka - Probe Uzunluk")
        s.setUyariText(txt_tank_kapasite_cinsi_aciklama)
        self.cmb_probe           = wx.ComboBox(self.panel_yakit_tanki,1,"", choices=[],style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY)) 
        probe_parametric_objects = []
        probe_parametric_objects = self.db.get_parameters_all_with_id("VIEW_PROBE",where="AKTIF_PROBE=1",order="ID_PROBE",selects="ID_PROBE,PROBE_ADRES,PROBE_MARKA_ADI,PROBE_UZUNLUK")
        if probe_parametric_objects != [] :
            self.db.widgetMaker( self.cmb_probe , probe_parametric_objects)
        
        if data_.ID_PROBE != -1:
            index_ = probe_parametric_objects.index(filter(lambda x:  x.id==data_.ID_PROBE,probe_parametric_objects)[0])
            self.cmb_probe.SetSelection(index_)
       
        #txt_probe_cmb_aciklama_yildiz    = wx.StaticText (self.panel_yakit_tanki, label = "*")
        #s.setUyariText( txt_probe_cmb_aciklama_yildiz )
        
        lbl_probe_baslangic_seviye   = wx.StaticText(self.panel_yakit_tanki, label="Probe Ba�lang�� Seviyesi :  ")
        s.setStaticTextList( lbl_probe_baslangic_seviye )
        self.txt_probe_baslangic_seviye  = s.CheckNumberMasked(self.panel_yakit_tanki,'probe_bas_seviye',4)#wx.TextCtrl  (self.panel_yakit_tanki, 1,str(data_.PROBE_BASLANGIC_SEVIYE),size=(180,-1))
        self.txt_probe_baslangic_seviye.SetValue(str(data_.PROBE_BASLANGIC_SEVIYE))
        txt_probe_baslangic_seviye_cinsi = wx.StaticText(self.panel_yakit_tanki,label="mm")
        s.setStaticTextList( txt_probe_baslangic_seviye_cinsi )
        
        lbl_port_tipi = wx.StaticText(self.panel_yakit_tanki, label="Port Tipi :")
        s.setStaticTextList( lbl_port_tipi )
        txt_port_tipi = wx.TextCtrl  (self.panel_yakit_tanki, 1, "Serial",size=(70,-1),style=wx.TE_READONLY)

        lbl_port_adi = wx.StaticText(self.panel_yakit_tanki, label="Port Ad� :")
        s.setStaticTextList( lbl_port_adi )
        self.cmb_port_adi  = wx.ComboBox(self.panel_yakit_tanki,1,"", choices=[],style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY)) 
        port_adi_parametric_objects = []
        port_adi_parametric_objects = self.db.get_parameters_all("PORTLAR",order="PORT_ADI",selects="ID_PORT,PORT_ADI")
        
        if port_adi_parametric_objects != [] :
            self.db.widgetMaker( self.cmb_port_adi , port_adi_parametric_objects)           
        if data_.ID_PORT != -1:
            index_port = port_adi_parametric_objects.index(filter(lambda x:  x.id==data_.ID_PORT,port_adi_parametric_objects)[0])
            self.cmb_port_adi.SetSelection(index_port)
        lbl_yakit_turu = wx.StaticText(self.panel_yakit_tanki, label="Yak�t T�r� :")
        s.setStaticTextList( lbl_yakit_turu )
        self.cmb_yakit_turu  = wx.ComboBox(self.panel_yakit_tanki,1,"", choices=[],style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY)) 
        yakit_turu_parametric_objects = []
        yakit_turu_parametric_objects = self.db.get_parameters_all("YAKIT",where="YAKIT_DURUM=1",order="YAKIT_ADI",selects="ID_YAKIT,YAKIT_ADI")
        
        if yakit_turu_parametric_objects != [] :
            self.db.widgetMaker( self.cmb_yakit_turu , yakit_turu_parametric_objects)
        if data_.ID_YAKIT != -1:
            index_yakit = yakit_turu_parametric_objects.index(filter(lambda x:  x.id==data_.ID_YAKIT,yakit_turu_parametric_objects)[0])
            self.cmb_yakit_turu.SetSelection(index_yakit)
        self.btn_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        
        self.btn_save  = LibButtons.ThemedGenBitmapTextButton(self.panel_yakit_tanki, id=wx.NewId(), bitmap=image_save,label="Verileri Kaydet")
        self.btn_save.Bind(wx.EVT_BUTTON,lambda event: self.OnTankSave(event,selected_item))
        self.btn_save.SetToolTipString("Tank Bilgilerini Kaydet")
        self.btn_save.SetBitmapSelected(self.btn_save_pressed)
        s.setButtonFont( self.btn_save )
        
        self.gbs_add_tank.Add( lbl_tank_adi, (1,1),(1,1),  wx.ALL,1)
        self.gbs_add_tank.Add( self.txt_tank_adi, (1,2) ,(1,2),  wx.ALL,1)
        
        self.gbs_add_tank.Add( lbl_tank_yuksekligi, (2,1) ,(1,1),  wx.ALL,1)        
        self.gbs_add_tank.Add( self.txt_tank_yuksekligi, (2,2),(1,1),  wx.ALL,1)
        self.gbs_add_tank.Add( txt_tank_yuksekligi_cinsi, (2,3),(1,1),  wx.ALL,1)
        
        self.gbs_add_tank.Add( lbl_tank_kapasitesi, (3,1),(1,1),  wx.ALL,1)
        self.gbs_add_tank.Add( self.txt_tank_kapasitesi, (3,2) ,(1,1),  wx.ALL,1)
        self.gbs_add_tank.Add(txt_tank_kapasite_cinsi,(3,3),(1,1),wx.ALL,1)
        
        self.gbs_add_tank.Add( lbl_probe, (4,1),(1,1),  wx.ALL,1)
        self.gbs_add_tank.Add( self.cmb_probe, (4,2) ,(1,5),  wx.ALL,1)
        #self.gbs_add_tank.Add(txt_probe_cmb_aciklama_yildiz,(4,7),(1,1),wx.ALL,1)
        self.gbs_add_tank.Add( txt_tank_kapasite_cinsi_aciklama,(5,1),(1,3),wx.ALL,1)
        
        self.gbs_add_tank.Add( lbl_probe_baslangic_seviye, (6,1),(1,1),  wx.ALL,1)
        self.gbs_add_tank.Add( self.txt_probe_baslangic_seviye, (6,2) ,(1,1),  wx.ALL,1)
        self.gbs_add_tank.Add( txt_probe_baslangic_seviye_cinsi, (6,3) ,(1,1),  wx.ALL,1)
        
        self.gbs_add_tank.Add( lbl_port_tipi, (7,1),(1,1),  wx.ALL,1)
        self.gbs_add_tank.Add( txt_port_tipi, (7,2) ,(1,1),  wx.ALL,1)
        
        self.gbs_add_tank.Add( lbl_port_adi, (8,1),(1,1),  wx.ALL,1)
        self.gbs_add_tank.Add( self.cmb_port_adi, (8,2) ,(1,1),  wx.ALL,1)
               
        self.gbs_add_tank.Add( lbl_yakit_turu, (9,1),(1,1),  wx.ALL,1)
        self.gbs_add_tank.Add( self.cmb_yakit_turu, (9,2) ,(1,2),  wx.ALL,1)
        
        self.gbs_add_tank.Add(self.btn_save,  (10,1),(1,1),wx.ALL,3)        
        
        self.panel_yakit_tanki.SetSizerAndFit(self.gbs_add_tank)    
        return self.panel_yakit_tanki

    def OnTankSave(self,event,update=None):
        
        user=self.root.user
        message = ''
        tank_adi        = self.txt_tank_adi.GetValue().strip()
        tank_yukseklik  = self.txt_tank_yuksekligi.GetValue().strip()
        tank_kapasitesi = self.txt_tank_kapasitesi.GetValue().strip()
        if tank_adi == '':
            message = "L�tfen Yak�t Tank�n�n Ad�n� Giriniz.\n"
        if tank_yukseklik == '' :
            message += "L�tfen Yak�t Tank�n�n Y�ksekli�ini Giriniz.\n"
        if tank_kapasitesi == '':
            message += "L�tfen Yak�t Tank�n�n Kapasitesini Giriniz.\n"
        if self.cmb_probe.GetSelection() == -1:
            message += "L�tfen Probe Se�iniz.\n"
        if self.cmb_yakit_turu.GetSelection() == -1 :
            message += "L�tfen Yak�t T�r�n� Giriniz.\n"
        if self.cmb_port_adi.GetSelection() == -1:
            message += "L�tfen Port Ad�n� Se�iniz."
        if message != '' :
            s.uyari_message(self,message,"Yak�t Tank� Tan�mlama")
            
        else:
            id_probe        = self.cmb_probe.GetClientData(self.cmb_probe.GetSelection()).id
            yakit_turu      = self.cmb_yakit_turu.GetClientData(self.cmb_yakit_turu.GetSelection()).id
            id_port         = self.cmb_port_adi.GetClientData(self.cmb_port_adi.GetSelection()).id
            probe_baslangic_seviye  = self.txt_probe_baslangic_seviye.GetValue()
            if not probe_baslangic_seviye or probe_baslangic_seviye.strip()== '':
                probe_baslangic_seviye = 0
            
            ##update degeri tank listesinde guncelleme butonuna basilirsa ordan gelen ID_YAKIT_TANKI degeri olup
            ##bu deger disinda secilen probe baska kayidda var mi kontrolunu yapiyoruz yoksa guncelliyoruz
            ##eger update None ise guncellemeden gelmiyor direkt arayuzden kaydet butonundan geliyor
            ##checkini yapalim ayni probe den kayit yoksa sisteme kaydededilim
           
            if update:
                check_var_mi = self.db.get_raw_data_all(table_name="YAKIT_TANKI",
                                                    where="ID_ACTIVE = 1 AND ID_PROBE=%s AND ID_YAKIT_TANKI!=%s "%(id_probe,update),selects='YAKIT_TANKI_ADI')           
                if check_var_mi == []:
                    data_id = self.db.update_data ( table_name="YAKIT_TANKI" ,column_name="YAKIT_TANKI_ADI,YAKIT_TANKI_YUKSEKLIK,YAKIT_TANKI_KAPASITE,ID_PROBE,ID_PORT,ID_YAKIT,PROBE_BASLANGIC_SEVIYE",where = 'ID_YAKIT_TANKI=%s'%update,
                                                            values="'%s',%s,%s,%s,%s,%s,%s"%(tank_adi,tank_yukseklik,tank_kapasitesi,id_probe,id_port,yakit_turu,probe_baslangic_seviye),user=user )
                    if data_id !='' :
                        s.uyari_message(self,"G�ncellemek �stedi�iniz Yak�t Tank� Sisteme Ba�ar�yla G�ncellendi.","Yak�t Tank� Tan�mlama")    
                    else:
                        s.uyari_message(self,"Eklemek �stedi�iniz Yak�t Tank� Sisteme Kaydedilemedi. Sistem Y�neticiniz ile G�r���n�z.","Yak�t Tank� Tan�mlama")
                else:
                    s.uyari_message(self,"%s Probe ile Sistemde Mevcuttur."%str(id_probe),"Yak�t Tank� Tan�mlama") 
            else:
                check_var_mi = self.db.get_raw_data_all(table_name="YAKIT_TANKI",
                                                    where="ID_ACTIVE = 1 AND ID_PROBE=%s  "%(id_probe),selects='YAKIT_TANKI_ADI')
                if check_var_mi == []:
                    data_id = self.db.save_data_and_get_id ( table_name="YAKIT_TANKI" ,column_name="YAKIT_TANKI_ADI,YAKIT_TANKI_YUKSEKLIK,YAKIT_TANKI_KAPASITE,ID_PROBE,ID_PORT,ID_YAKIT,PROBE_BASLANGIC_SEVIYE",
                                                            values="'%s',%s,%s,%s,%s,%s,%s"%(tank_adi,tank_yukseklik,tank_kapasitesi,id_probe,id_port,yakit_turu,probe_baslangic_seviye) )
                    if data_id !='' :
                        s.uyari_message(self,"Eklemek �stedi�iniz Yak�t Tank� Sisteme Ba�ar�yla Kaydedildi.","Yak�t Tank� Tan�mlama")    
                    else:
                        s.uyari_message(self,"Eklemek �stedi�iniz Yak�t Tank� Sisteme Kaydedilemedi. Sistem Y�neticiniz ile G�r���n�z.","Yak�t Tank� Tan�mlama")
                else:
                    s.uyari_message(self,"%s Probe ile Sistemde Mevcuttur."%str(id_probe),"Yak�t Tank� Tan�mlama") 

    def OnBookPageChanged( self, event ):
        if self.book.GetSelection() == 0:
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.book.Freeze()
            self.book.SetSelection(1)
            p_ = self.createAddProbe()
            self.book.RemovePage(0)
            self.book.InsertPage( 0,p_,"Probe Tan�mlama")
            self.book.SetPageImage(0, self.img0)
            self.book.SetSelection(0)
            self.book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.book.Thaw()
        
        elif self.book.GetSelection() == 1:
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.book.Freeze()
            self.book.SetSelection(0)
            p_ = self.createProbeList()
            self.book.RemovePage(1)
            self.book.InsertPage( 1,p_,"Probe Listesi")
            self.book.SetPageImage(1, self.img1)
            self.book.SetSelection(1)
            self.book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.book.Thaw()
            
        elif self.book.GetSelection() == 3:
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.book.Freeze()
            self.book.SetSelection(0)
            p_ = self.createTankList()
            self.book.RemovePage(3)
            self.book.InsertPage( 3,p_,"Yak�t Tank Listesi")
            self.book.SetPageImage(3, self.img3)
            self.book.SetSelection(3)
            self.book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.book.Thaw()
            
        elif self.book.GetSelection() == 2:
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.book.Freeze()
            self.book.SetSelection(0)
            p_ = self.createAddTank()
            self.book.RemovePage(2)
            self.book.InsertPage( 2,p_,"Yak�t Tank� Ekle")
            self.book.SetPageImage(2, self.img2)
            self.book.SetSelection(2)
            self.book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.book.Thaw()
        elif self.book.GetSelection() == 4:
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.book.Freeze()
            self.book.SetSelection(0)
            p_ = self.createAddMinMax()
            self.book.RemovePage(4)
            self.book.InsertPage( 4,p_,"Min-Max Tan�mlama")
            self.book.SetPageImage(4, self.img4)
            self.book.SetSelection(4)
            self.book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.book.Thaw()
        elif self.book.GetSelection() == 5:
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.book.Freeze()
            self.book.SetSelection(0)
            p_ = self.createTankKalibrasyonList()
            self.book.RemovePage(5)
            self.book.InsertPage( 5,p_,"Kalibrasyon Listesi")
            self.book.SetPageImage(5, self.img5)
            self.book.SetSelection(5)
            self.book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.book.Thaw()
            
        self.selected_item = None
    
    def MinMaxGuncelle(self):
        user=self.root.user
        data = self.db.update_data(self.table_name_min_max,self.column_name_min_max,self.values_min_max,self.where_min_max,user=user)
        if data == 1:
            s.uyari_message(self,"Se�ti�iniz Yak�t Tank� ��in Min-Max De�erleri Ba�ar�yla G�ncellenmi�tir.","Min-Max Tan�mlama")
    def MinMaxGuncelleme(self):
        pass

    def createTankKalibrasyonList(self):
        
        self.panel_kal_list = wx.Panel(self.book,1, style = wx.TAB_TRAVERSAL
             | wx.CLIP_CHILDREN
             | wx.FULL_REPAINT_ON_RESIZE
             |wx.EXPAND)
        self.panel_button = wx.Panel(self.panel_kal_list,1, style = wx.TAB_TRAVERSAL
             | wx.CLIP_CHILDREN
             | wx.FULL_REPAINT_ON_RESIZE
             |wx.EXPAND)
    
        self.panel_kal_list.kalib_panel_id = 0
        self.ondalik_kisim_var = False
        self.gbs_kal_list =  wx.GridBagSizer(0, 10)
        self.kal_list_box   = wx.BoxSizer( wx.VERTICAL )
        lbl_yakit_tanki   = wx.StaticText(self.panel_button, label="Yak�t Tank� :  ")
        s.setStaticTextList( lbl_yakit_tanki )
        choices_yakit_tank = []

        self.cmb_tank  = wx.ComboBox(self.panel_button,1,"", choices=choices_yakit_tank,
                                            style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY))
        choices_yakit_tank     = self.db.get_parameters_all("YAKIT_TANKI",where="ID_ACTIVE = 1",order="YAKIT_TANKI_ADI",selects="ID_YAKIT_TANKI,YAKIT_TANKI_ADI")
        if choices_yakit_tank != [] :
            self.db.widgetMaker( self.cmb_tank , choices_yakit_tank)
            
        self.btn_kalibrasyon_pressed  =  wx.Image("%sdatabase_down_16.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_kalibrasyon = wx.Image("%sdatabase_16.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        
        self.btn_kalibrasyon  = LibButtons.ThemedGenBitmapTextButton(self.panel_button, id=wx.NewId(), bitmap=image_kalibrasyon,label="Kalibrasyonu Getir")
        self.btn_kalibrasyon.Bind(wx.EVT_BUTTON,lambda event: self.OnKalibrasyonGetir(event))
        self.btn_kalibrasyon.SetToolTipString("Tank�n Kalibrasyon Cetvelini Getir")
        self.btn_kalibrasyon.SetBitmapSelected(self.btn_kalibrasyon_pressed)
        s.setButtonFont( self.btn_kalibrasyon,font = 10 )
        
        self.lbl_ondalik_kisim   = wx.StaticText(self.panel_button, label="Ondal�k K�s�m :  ")
        s.setStaticTextList( self.lbl_ondalik_kisim )
        self.cmb_ondalik_kisim = wx.ComboBox(self.panel_button,1,"", choices=['0','1','2','3','4','5','6','7','8','9'],
                                             style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY))
        
        self.btn_kalibrasyon_olustur_pressed  =  wx.Image("%sdatabase_down_16.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_kalibrasyon_olustur = wx.Image("%sdatabase_16.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        
        self.btn_kalibrasyon_olustur  = LibButtons.ThemedGenBitmapTextButton(self.panel_button, id=wx.NewId(), bitmap=image_kalibrasyon_olustur,label="Kalibrasyon Olu�tur")
        self.btn_kalibrasyon_olustur.Bind(wx.EVT_BUTTON,lambda event: self.OnKalibrasyonOlustur(event))
        self.btn_kalibrasyon_olustur.SetToolTipString("Tank�n Kalibrasyon Cetvelini Olu�turur")
        self.btn_kalibrasyon_olustur.SetBitmapSelected(self.btn_kalibrasyon_olustur_pressed)
        s.setButtonFont( self.btn_kalibrasyon_olustur,font = 10 )
        self.gbs_kal_list.Add(lbl_yakit_tanki,(0,0),span=wx.GBSpan(1,1))
        self.gbs_kal_list.Add(self.cmb_tank,(0,1),span=wx.GBSpan(1,1))
        self.gbs_kal_list.Add(self.lbl_ondalik_kisim,(0,2),span=wx.GBSpan(1,1))       
        self.gbs_kal_list.Add(self.cmb_ondalik_kisim,(0,3),span=wx.GBSpan(1,1))
        self.gbs_kal_list.Add(self.btn_kalibrasyon,(0,4),span=wx.GBSpan(1,6))
        self.gbs_kal_list.Add(self.btn_kalibrasyon_olustur,(0,10),wx.GBSpan(1,1),wx.ALIGN_RIGHT)
        
        self.panel_button.SetSizer(self.gbs_kal_list)
        self.kal_list_box.Add(self.panel_button)
        self.panel_kal_list.SetSizer( self.kal_list_box )

        #self.kal_list_box_sizer.Add(self.panel_button ,0, wx.ALIGN_CENTER|wx.ALIGN_CENTER_VERTICAL )
        #self.panel_probe.SetAutoLayout(True)
        
        return self.panel_kal_list
    
    def OnKalibrasyonGetir(self,event):
        self.panel_kal_list.Freeze()
        message = ''
        if self.cmb_tank.GetSelection() == -1:
            message = "L�tfen Yak�t Tank�n� Se�iniz."
        if message != '' :
            s.uyari_message(self,message,"Kalibrasyon Tablosu")
        else:
            if self.panel_kal_list.kalib_panel_id != 0:
                try:
                    wx.FindWindowById( self.panel_kal_list.kalib_panel_id ).Destroy()
                except:
                    pass
            id_yakit_tank      = self.cmb_tank.GetClientData(self.cmb_tank.GetSelection()).id
            headers            = ['ID,digit','Yak�t Tank� ID,digit','Y�kseklik,digit','Hacim,digit']
            data_history       = self.db.get_raw_data_all("TANK_KALIBRASYON_TABLOSU",where = 'ID_YAKIT_TANKI=%s'%id_yakit_tank,order = "YUKSEKLIK")  
            if data_history != []:
                hassasiyet         = data_history[0][2]
                hassasiyet_tam_mi  = hassasiyet.is_integer()
                hassasiyet_ondalik = 0
                if not hassasiyet_tam_mi:
                    hassasiyet_ondalik = int (str(hassasiyet-int(hassasiyet))[2:3])
                self.cmb_ondalik_kisim.SetSelection(hassasiyet_ondalik)
                self.list_pane = IndexListPanel( self.panel_kal_list, headers=headers, data = data_history,cell_editable = 'DATAVIEW_CELL_EDITABLE',save_button = True,save_method = self.OnSaveKalibrasyon)
                self.panel_kal_list.kalib_panel_id = self.list_pane.GetId()
                ekran_orani = 1
                self.kal_list_box.Add( self.list_pane,ekran_orani,wx.ALL|wx.EXPAND )
        self.panel_kal_list.Layout()
            #list_pane = PaginatingIndexPanel( self.panel_kal_list,table_name='TANK_KALIBRASYON_TABLOSU',where='ID_YAKIT_TANKI=%s'%id_yakit_tank,order='YUKSEKLIK', headers= headers,parent_sizer = self.kal_list_box,cell_editable = 'DATAVIEW_CELL_EDITABLE' )   
        self.panel_kal_list.Update()
        self.panel_kal_list.Thaw()

    def OnKalibrasyonOlustur(self,event):
        self.panel_kal_list.Freeze()
        
        if self.panel_kal_list.kalib_panel_id != 0:
            try:
                wx.FindWindowById( self.panel_kal_list.kalib_panel_id ).Destroy()
            except:
                pass
        hassasiyet_get = self.cmb_ondalik_kisim.GetSelection()
        id_yakit_tank  = self.cmb_tank.GetClientData(self.cmb_tank.GetSelection()).id
        tank_kayit = self.db.get_raw_data_all("YAKIT_TANKI", where = 'ID_YAKIT_TANKI=%s'%id_yakit_tank)
        list_dizi = []
        if tank_kayit :
            for i in range(0,(tank_kayit[0][2]/10)):
                list_dizi.append((0,id_yakit_tank,i+hassasiyet_get/float(10),0))
        self.cmb_ondalik_kisim.SetSelection(hassasiyet_get)
        headers   = ['ID,digit','Yak�t Tank� ID,digit','Y�kseklik,digit','Hacim,digit']
        self.list_pane = IndexListPanel( self.panel_kal_list, headers=headers, data = list_dizi,cell_editable = 'DATAVIEW_CELL_EDITABLE',save_button = True,save_method = self.OnSaveKalibrasyon)
        self.panel_kal_list.kalib_panel_id = self.list_pane.GetId()
        ekran_orani = 1
        self.kal_list_box.Add( self.list_pane,ekran_orani,wx.ALL|wx.EXPAND )
        self.panel_kal_list.Layout()
        self.panel_kal_list.Update()
        self.panel_kal_list.Thaw()
        
        
    def OnSaveKalibrasyon(self,event=None):
	component = event.GetEventObject()
	component.Disable()
        data_kalibrasyon        = self.list_pane.model.data
        id_yakit_tanki          = data_kalibrasyon[0][1]
        check_kayit_id          = data_kalibrasyon[0][0]
	user=self.root.user
	return_ = 0
        if check_kayit_id != 0:
            return_ = self.db.select_insert(table_insert='TANK_KALIBRASYON_TABLOSU_HISTORY',column_name_insert='ID_YAKIT_TANKI,YUKSEKLIK,HACIM,TARIH',
                                            column_name_select="ID_YAKIT_TANKI,YUKSEKLIK,HACIM,strftime('%Y-%m-%d %H:%M:%S','now')",
                                            table_select='TANK_KALIBRASYON_TABLOSU',where='ID_YAKIT_TANKI = %s'%id_yakit_tanki,
					    user=user)
            
           
        #print return_
        if check_kayit_id == 0 or  return_ == 1:
	    
            for kalib_data in data_kalibrasyon:
                if kalib_data[3] != 0:
		    delete_kalib_data = self.db.delete_data(table_name = 'TANK_KALIBRASYON_TABLOSU',where = 'ID_TKT= %s'%kalib_data[0])
		    if delete_kalib_data == 1:
			for_erebor = str(kalib_data[3]).replace(',','')
			self.db.save_data(table_name='TANK_KALIBRASYON_TABLOSU' ,column_name='ID_YAKIT_TANKI,YUKSEKLIK,HACIM', values='%s,%s,%s'%(kalib_data[1],kalib_data[2],for_erebor))
		    #break
	    tankKalibrasyonQueueContainer = self.root.tankKalibrasyonQueueContainer
            if not tankKalibrasyonQueueContainer.empty():
                tankKalibrasyonQueueContainer.get()
            tank_kalibrasyon_cetveli = self.db.getKalibrasyonDataAll()
            tankKalibrasyonQueueContainer.put(tank_kalibrasyon_cetveli)
class win_port_tanimla(wx.Frame):
    def __init__(self, parent, id_, title):
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR ))
        self.check_list = []
        self.root= param.get_root( self )
        self.db = self.root.DB
        self.O  = self.root.O
        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.mainSizer)
        self.LayoutItems()
        #self.Centre()
    def LayoutItems(self):
        
        self.Freeze()
       
        #self.book = wx.Notebook(self, style=wx.NB_TOP)
        self.book  = aui.AuiNotebook(self,1,agwStyle=(aui.AUI_NB_TOP|aui.AUI_NB_SCROLL_BUTTONS))
        self.book.SetArtProvider(aui.AuiDefaultTabArt())
        s.setPanelListBg( self.book )        
        self.p = self.PortKaydet()
        s.setPanelListBg( self.p )
        self.p2 = self.KayitliPortlar()
        s.setPanelListBg( self.p2 )
        self.book.AddPage( self.p,"Port Kaydet" )
        self.book.AddPage( self.p2,"Kay�tl� Port Listesi" )
        
        il = wx.ImageList(64, 64)
        self.img0 = il.Add(wx.Bitmap('%sserial_port_add.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img1 = il.Add(wx.Bitmap('%swindow_info.png'%image_path, wx.BITMAP_TYPE_ANY))
        
        self.book.Bind(aui.EVT_AUINOTEBOOK_PAGE_CHANGED, self.OnBookPageChanged )
        
        self.book.AssignImageList(il)
 
        self.book.SetPageImage(0, self.img0)
        self.book.SetPageImage(1, self.img1)
        self.mainSizer.Add(self.book, 1,wx.ALL | wx.EXPAND)
        self.Layout()
        self.SetSize((600,600))
        self.Thaw()
    def PortKaydet( self ):             
        p = wx.Panel(self.book,-1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        # wx.Colour(130,208,190)
        #p.SetBackgroundColour(  frame_color )
        gbs = self.gbs = wx.GridBagSizer(5, 5)
        portlist=[]
        if os.name == 'nt':
        # windows
            i = 0
            for i in range(256):
                try:
                    seri = serial.Serial(i)
                    seri.close()
                    portlist.append( 'COM' + str(i + 1))
                except serial.SerialException:
                    pass
        kayitli= self.db.get_raw_data_all(table_name = 'PORTLAR',
                                                   selects="ID_PORT,PORT_ADI,PARITY,STOP_BIT,BAUND_RATE,DATA_BIT,BEK_SURESI",dict_mi='object' )
        if len(kayitli)!=0:
            for x in kayitli:
                if x.PORT_ADI not in portlist:
                    portlist.append(x.PORT_ADI)
                    
        port_adi_lbl    = wx.StaticText(p, label="Port Ad�:  ")
        self.port_adi_cmb    = wx.ComboBox(p,  size=(180, -1),choices=portlist, style=wx.CB_DROPDOWN|wx.CB_READONLY)
        self.port_adi_cmb.Bind(wx.EVT_TEXT,self.PortAdiChange)
        parity_lbl    = wx.StaticText(p, label="Parity:  ")
        self.parity_cmb    = wx.ComboBox(p,  size=(180, -1), choices=['Odd','None','Mark','Even','Space'],
                                         style=wx.CB_DROPDOWN|wx.CB_READONLY)
        
        stop_bit_lbl    = wx.StaticText(p, label="Stop Bit:  ")
        self.stop_bit_cmb    = wx.ComboBox(p,  size=(180, -1), choices=['0','1','1.5','2'],
                                           style=wx.CB_DROPDOWN|wx.CB_READONLY)
        
        baud_rate_lbl    = wx.StaticText(p, label="Baund Rate:  ")
        self.baud_rate_cmb    = wx.ComboBox(p,  size=(180, -1), choices=['110','300','600','1200','2400','4800','9600','19200','38400','57600','115200'],
                                            style=wx.CB_DROPDOWN|wx.CB_READONLY)
        
        data_biti_lbl   = wx.StaticText(p, label="Data Bit:  ")
        self.data_biti_txt      =s.CheckNumberMasked(p,'data_biti',1)
        self.data_biti_txt.Value='8'
        
        bekleme_suresi_lbl   = wx.StaticText(p, label="Bekleme S�resi  ")
        self.bekleme_suresi_txt      = s.CheckNumberMasked(p,'data_biti',1)
        self.bekleme_suresi_txt.Value='1'
        
        self.btn_port_kaydet_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_port_kaydet  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label="Port Kaydet")
        self.btn_port_kaydet.Bind(wx.EVT_BUTTON, self.OnPortKaydet)
        self.btn_port_kaydet.SetToolTipString("Port Kaydet")
        self.btn_port_kaydet.SetBitmapSelected(self.btn_port_kaydet_pressed)
        s.setButtonFont( self.btn_port_kaydet )
        
        
        gbs.Add( port_adi_lbl, (1,1),(1,1),  wx.ALL,3)
        gbs.Add( self.port_adi_cmb, (1,2) ,(1,2),  wx.ALL,3)
        
        gbs.Add( parity_lbl, (2,1),(1,1),  wx.ALL,3)
        gbs.Add( self.parity_cmb,(2,2) ,(1,2),  wx.ALL,3)
        
        gbs.Add( stop_bit_lbl, (3,1),(1,1),  wx.ALL,3)
        gbs.Add( self.stop_bit_cmb, (3,2) ,(1,2),  wx.ALL,3)
        
        gbs.Add( baud_rate_lbl, (4,1),(1,1),  wx.ALL,3)
        gbs.Add( self.baud_rate_cmb, (4,2) ,(1,2),  wx.ALL,3)
        
        gbs.Add( data_biti_lbl, (5,1),(1,1),  wx.ALL,3)
        gbs.Add( self.data_biti_txt, (5,2) ,(1,1),  wx.ALL,3)
        
        gbs.Add( bekleme_suresi_lbl, (6,1),(1,1),  wx.ALL,3)
        gbs.Add( self.bekleme_suresi_txt, (6,2) ,(1,1),  wx.ALL,3)
        
        gbs.Add( self.btn_port_kaydet, (7,1) ,(1,1),  wx.ALL,3)
        
        p.SetSizerAndFit(gbs)
        p.SetAutoLayout(True)
        #self.Thaw()
        #self.Show()
        return p
    def PortAdiChange(self,event ):
        var_mi = self.db.get_raw_data_all(table_name = 'PORTLAR',
                                                   selects="ID_PORT,PORT_ADI,PARITY,STOP_BIT,BAUND_RATE,DATA_BIT,BEK_SURESI",where="PORT_ADI='%s'"%self.port_adi_cmb.GetValue(),dict_mi='object' )
        if var_mi != []:
            self.parity_cmb.SetStringSelection(var_mi[0].PARITY)
            self.stop_bit_cmb.SetStringSelection(var_mi[0].STOP_BIT)
            self.baud_rate_cmb.SetStringSelection(var_mi[0].BAUND_RATE)
            self.data_biti_txt.SetValue(var_mi[0].DATA_BIT)
            self.bekleme_suresi_txt.SetValue(var_mi[0].BEK_SURESI)
        else:
            self.parity_cmb.SetSelection = -1
            self.stop_bit_cmb.SetSelection = -1
            self.baud_rate_cmb.SetSelection = -1
            self.data_biti_txt.Value='8'
            self.bekleme_suresi_txt.Value='1'
    def OnPortKaydet(self,event):
        var = self.db.get_raw_data_all( table_name="PORTLAR",where="PORT_ADI='%s'"%self.port_adi_cmb.GetValue(),selects="count(*)" )
        user=self.root.user
        message=''
        port_adi=self.port_adi_cmb.GetValue()
        parity_id=self.parity_cmb.GetValue()
        stop_bit_id=self.stop_bit_cmb.GetValue()
        baud_rate_id=self.baud_rate_cmb.GetValue()
        data_bit=self.data_biti_txt.GetValue()
        bekleme=self.bekleme_suresi_txt.GetValue()
        
        if port_adi=="":
            message="L�tfen Port Ad� Alan�n� Doldurun\n"
            
        if parity_id=="":
            message+="L�tfen Parity Alan�n� Doldurun\n"
            
        if stop_bit_id=="":
            message+="L�tfen Stop Bit Alan�n� Doldurun\n"
            
        if baud_rate_id=="":
            message+="L�tfen Baud Rate Alan�n� Doldurun\n"
            
        if data_bit=="":
            message+="L�tfen Data Bit Alan�n� Doldurun\n"
            
        if bekleme=="":
            message+="L�tfen Bekleme S�resi Alan�n� Doldurun"
        if message != '' :
            s.uyari_message(self,message,"Uyar�")            
        else:
            if int(var[0][0]) >0:     
                s.ok_cancel_message(parent=self, message="Kay�tl� Port Bulundu.Port G�ncellensin Mi?"
                                          ,win_name="Port G�ncelle", ok_function=self.guncelle, cancel_function=None)
            else:
                donen=self.db.save_data(table_name = 'PORTLAR',
                         column_name='PORT_ADI,PARITY,STOP_BIT,BAUND_RATE,DATA_BIT,BEK_SURESI',
                         values=("'%s','%s','%s','%s','%s','%s'"%(self.port_adi_cmb.GetValue(),self.parity_cmb.GetValue(),self.stop_bit_cmb.GetValue(),
                                                                  self.baud_rate_cmb.GetValue(),self.data_biti_txt.GetValue(),self.bekleme_suresi_txt.GetValue() )),user=user)
                if donen==1 :
                    s.ok_message(self,"Veriler Kaydedildi","Port G�ncelleme")
                else:
                    s.ok_message(self,"Verilerin Kaydedilmesi S�ras�nda Bir Hata Olu�tu.","Port G�ncelleme")
                
    def guncelle(self ):
        user=self.root.user
        donen=self.db.update_data( table_name='PORTLAR',
                        column_name='PORT_ADI,PARITY,STOP_BIT,BAUND_RATE,DATA_BIT,BEK_SURESI',
                        values=("'%s','%s','%s','%s','%s','%s'"%(self.port_adi_cmb.GetValue(),self.parity_cmb.GetValue(),self.stop_bit_cmb.GetValue(),
                                                                  self.baud_rate_cmb.GetValue(),self.data_biti_txt.GetValue(),self.bekleme_suresi_txt.GetValue() )),
                        where="PORT_ADI='%s'"%self.port_adi_cmb.GetValue(),user=user)
        if donen==1 :
            s.ok_message(self,"Veriler Kaydedildi","Port G�ncelleme")
        else:
            s.ok_message(self,"Verilerin Kaydedilmesi S�ras�nda Bir Hata Olu�tu.","Port G�ncelleme")
    def getCheckedItems( self, item_id, item_state  ):
        if item_state == True:
            self.check_list.append( item_id )
        else:
            if item_id in self.check_list:
                self.check_list.remove( item_id )
                
            
    def OnPortDelete( self, event ):
        deleted_error = []
        user=self.root.user
        for item_id in self.check_list:
            delete=self.db.delete_data(table_name="PORTLAR",where="ID_PORT='"+str(item_id)+"'",user=user)
            #delete = 1
            if delete!=1 :
                deleted_error.append(1)
            
              
        if len(deleted_error) == 0:
            s.updateWindow(self.book, self.LayoutItems)
            s.ok_message(self,"Se�ilen port(lar) Silindi","Port Silme")
        else:
            s.uyari_message(self," Comport Kullan�mda G�r�n�yor! Tank,Arabirim,Kart Okuyucu Tan�mlar�n�z� Kontrol Edin","Port Silme")
        #self.Layout()                        
                 
    def KayitliPortlar( self ):
        
        
        self.data_all       = self.db.get_raw_data_all(table_name = 'PORTLAR',
                                                   selects="ID_PORT,PORT_ADI,PARITY,STOP_BIT,BAUND_RATE,DATA_BIT,BEK_SURESI" )
        headers = ['Se�,digit','Port Ad�,string','Parity,string','Stop Bit,string','Baund Rate,string'
                    ,'Data Bit,string','Bekleme S�resi,string']
        
        self.liste = self.O.createStaticList(self.book, headers, self.data_all  ,lineCheck=True ,checked_set_method = self.getCheckedItems)
        
        #self.liste.SetPosition((0,300),wx.EXPAND|wx.ALL)
        self.btn_port_delete_pressed  =  wx.Image("%strash_black.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%strash.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_port_delete  = LibButtons.ThemedGenBitmapTextButton(self.liste, id=wx.NewId(), bitmap=image_save,label="Port Sil")
        self.btn_port_delete.Bind( wx.EVT_BUTTON, self.OnPortDelete)
        self.btn_port_delete.SetToolTipString("Port Sil")
        self.btn_port_delete.SetBitmapSelected(self.btn_port_delete_pressed)
        s.setButtonFont( self.btn_port_delete )        
        sizer = self.liste.GetSizer()
        line_number = sizer.Rows + 1
        col_number  = sizer.Cols - 2
        
        sizer.Add(  self.btn_port_delete , ( line_number,col_number  ), (1,1), wx.ALL|wx.EXPAND, 3 )
        self.liste.Layout()
        
        return self.liste
    def OnBookPageChanged( self, event ):
        
        if self.book.GetSelection() == 0:
            
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.book.Freeze()
            self.book.SetSelection(1)
            p_ = self.PortKaydet()
            self.book.RemovePage(0)
            self.book.InsertPage( 0,p_,"Port Kaydet")
            self.book.SetPageImage(0, self.img0)
            self.book.SetSelection(0)
            self.book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.book.Thaw()
        
        elif self.book.GetSelection() == 1:
            
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.book.Freeze()
            self.book.SetSelection(0)
            p_ = self.KayitliPortlar()
            self.book.RemovePage(1)
            self.book.InsertPage( 1,p_,"Kay�tl� Port Listesi")
            self.book.SetPageImage(1, self.img1)
            self.book.SetSelection(1)
            self.book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.book.Thaw()
            
        
class win_Otomasyon_Ayarlari( wx.Frame ):
    def __init__(self, parent, id_, title):
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR ))
        
	self.root = param.get_root( self )
        self.db   = self.root.DB
        self.O    = self.root.O 
        self.LayoutItems()
        self.Centre()  

    def LayoutItems(self):
        mainSizer = wx.BoxSizer( wx.VERTICAL )
        self.Freeze()
        self.SetSizer(mainSizer)
        self.check_list    = []
        self.uncheck_list  = []
        self.p = self.createList()
        self.p_button  = wx.Panel(self,-1, style = wx.TAB_TRAVERSAL
                         | wx.CLIP_CHILDREN
                         | wx.FULL_REPAINT_ON_RESIZE)
        btn_sizer = wx.BoxSizer( wx.HORIZONTAL )
        
        self.p_text  = wx.Panel(self,-1, style = wx.TAB_TRAVERSAL
                         | wx.CLIP_CHILDREN
                         | wx.FULL_REPAINT_ON_RESIZE)
        text_sizer = wx.GridBagSizer( wx.HORIZONTAL )
        
        self.p_text.SetSizer(text_sizer)
        self.p_button.SetSizer( btn_sizer )
        self.SetSize((700,600))
        
        self.btn_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_save  = LibButtons.ThemedGenBitmapTextButton(self.p_button, id=wx.NewId(), bitmap=image_save,label="Kaydet")
        self.btn_save.Bind(wx.EVT_BUTTON, self.OnParametreSave)
        self.btn_save.SetToolTipString("Kaydet")
        self.btn_save.SetBitmapSelected(self.btn_save_pressed)
        s.setPanelListBg( self )
        s.setButtonFont( self.btn_save )
        
        self.btn_oto_kapa_pressed  =  wx.Image("%sdelete_green.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.image_oto_kapa = wx.Image("%sdelete.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_oto_kapa  = LibButtons.ThemedGenBitmapTextButton(self.p_button, id=wx.NewId(), bitmap=self.image_oto_kapa,label="Otomasyonu Kapat" , style=(wx.ALIGN_LEFT|wx.ALIGN_BOTTOM))
        self.btn_oto_kapa.Bind(wx.EVT_BUTTON, self.OtomasyonKapa)
        self.btn_oto_kapa.SetToolTipString("Otomasyonu Kapat")
        self.btn_oto_kapa.SetBitmapSelected(self.btn_oto_kapa_pressed)
        s.setButtonFont( self.btn_oto_kapa )
        
        lbl_sunucu_adresi   = wx.StaticText(self.p_text, label="Sunucu Adres :  ")
        self.txt_sunucu_adresi = wx.TextCtrl  (self.p_text, -1, "",size=(180,-1))
        s.setStaticTextList( lbl_sunucu_adresi )
        #space= wx.StaticText(self.p_text,label="   ")
        
        mainSizer.Add(self.p,1,wx.EXPAND)
        btn_sizer.Add(self.btn_save,1,wx.ALL| wx.EXPAND)
        btn_sizer.Add(self.btn_oto_kapa,0,wx.ALL| wx.EXPAND)
        text_sizer.Add( lbl_sunucu_adresi, (1,1),(3,1),  wx.EXPAND,3)
        text_sizer.Add( self.txt_sunucu_adresi, (1,2) ,(1,1),  wx.EXPAND,3)
        #text_sizer.Add(space,(2,1),(1,1),  wx.EXPAND,3)
        mainSizer.Add(self.p_text,0,wx.ALL|wx.EXPAND)
        mainSizer.Add(self.p_button,0,wx.ALL|wx.EXPAND)

        
        self.Thaw()
    
    def OtomasyonKapa (self,event ):
        s.ok_cancel_message(self,"Otomasyon Kapanacak !!!! Emin misiniz? ", "Otomasyonu Kapat", self.KillEmAll)
    
    def KillEmAll(self):
        r = param.get_root( self )
        t = r.c_thread.kill()
        process_end ( sabitler.control_process )
        process_end ( sabitler.main_process )
        r.Destroy()
       
    
    def createList(self):
        self.data_all   = self.db.get_raw_data_all(table_name = 'P_OTOMASYON_PARAMETRE',where="PARAMETRE_DEGER= 2 OR PARAMETRE_DEGER=1",order="ID_PARAMETRE",selects="ID_PARAMETRE,PARAMETRE_ADI,PARAMETRE_DEGER" )
        header = ['Se�,digit', 'Parametre Ad�,string', 'Parametre De�eri,digit']
        p = self.O.createStaticList(self,header,self.data_all,lineCheck=True,checked_set_method=self.OnChecked,checked_index=2 )
        return p
    
    def OnChecked( self,item_id, item_state):
        if item_state == True:
            if item_id in self.uncheck_list:
                self.uncheck_list.remove( item_id )
            self.check_list.append( item_id )
        else:
            if item_id in self.check_list:
                self.check_list.remove( item_id )
            self.uncheck_list.append( item_id )
  
    def OnParametreSave( self, event ):
	user = self.root.user
        for item_id in self.check_list:
            self.db.update_data( table_name='P_OTOMASYON_PARAMETRE',column_name='PARAMETRE_DEGER',values='1',where='ID_PARAMETRE=%s'%item_id, user=user)
            self.regedit_ayarla( item_id, 'enable' )
	    
        for item_id in self.uncheck_list:
            self.db.update_data( table_name='P_OTOMASYON_PARAMETRE',column_name='PARAMETRE_DEGER',values='2',where='ID_PARAMETRE=%s'%item_id,user=user)
            self.regedit_ayarla( item_id, 'disable' )
        
        if self.txt_sunucu_adresi.GetValue()!='':
            sunucu_adresi_txt = self.txt_sunucu_adresi.GetValue().strip()
            self.db.update_data( table_name='P_OTOMASYON_PARAMETRE',column_name='PARAMETRE_KRITER',values="'%s'"%sunucu_adresi_txt ,where='PARAMETRE_DEGER=3',user=user)
            
        s.ok_message(self, "Veriler Kaydedildi","Otomasyon Parametre Kaydet")
        #self.p.Destroy()
        #self.p = self.createList()
        self.Layout()
    def regedit_ayarla( self,item_id, parameter ):
        if parameter=='enable':
            reg_val = 1
        elif parameter == 'disable':
            reg_val = 0
        reg_key = self.db.get_raw_data_all(table_name = 'P_OTOMASYON_PARAMETRE',where="ID_PARAMETRE=%s"%item_id,selects="PARAMETRE_KRITER",dict_mi = 'object' )[0]
        if reg_key.PARAMETRE_KRITER.strip() != '':
            reg_key = reg_key.PARAMETRE_KRITER
            if reg_key.startswith('HKEY_LOCAL_MACHINE\\'):
                reg_key = reg_key.replace( 'HKEY_LOCAL_MACHINE\\','' )
                if reg_key.find('Shell') > 0:
                    if reg_val == 1:
                        util.regKeySetREG_SZ_LOCAL( reg_key, sabitler.path_prefix + 'mainWindow.exe' )
                    else:
                        util.regKeySetREG_SZ_LOCAL( reg_key, 'explorer.exe' )
                    
                    s.ok_cancel_message(self, "De�i�ikliklerin Etkili Olmas� ��in Sisteminizi Yeniden Ba�lat�n�z.", "Otomasyon Parametre Kaydet", util.restartPC )
                    
                else:
                    util.regKeySetREG_DWORD_LOCAL( reg_key, reg_val)
            elif reg_key.startswith('HKEY_CURRENT_USER\\'):
                reg_key = reg_key.replace( 'HKEY_CURRENT_USER\\','' )
                util.regKeySetREG_DWORD_USER( reg_key, reg_val)
            elif reg_key.startswith('process'):
                process_name = reg_key.replace('process_', '' )
                if reg_val == 1:
                    if not process_control ( process_name ):
                        process_start ( process_name )
                else:
                    process_end( process_name )
class win_Bayii_Tanimlama( wx.Frame ):
    def __init__(self, parent, id_, title):
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (   wx.DEFAULT_FRAME_STYLE|wx.FRAME_NO_TASKBAR ))
        
        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.root = param.get_root( self )
        self.db   = self.root.DB
        self.O    = self.root.O
        
        self.createLayout()
        self.Centre()
    def createLayout(self):
        
        self.Freeze()
        
        self.book  = aui.AuiNotebook(self,1,agwStyle=(aui.AUI_NB_TOP|aui.AUI_NB_SCROLL_BUTTONS))
        
        self.book.SetArtProvider(aui.AuiDefaultTabArt())
        s.setPanelListBg( self.book )

        il = wx.ImageList(64, 64)
        self.img0 = il.Add(wx.Bitmap('%sdagitici_girisi.png'%image_path, wx.BITMAP_TYPE_PNG))
        self.img1 = il.Add(wx.Bitmap('%sbayi_girisi.png'%image_path, wx.BITMAP_TYPE_ANY))
        
        self.p = self.createDagiticiGiris()
        self.p2 = self.createBayiGiris()
        
        self.book.AddPage( self.p,"Da��t�c� Giri�i",True )
        check_dagitici = param.get_dagitici_object()
        self.book.AddPage( self.p2,"Bayi Giri�i")
        if not check_dagitici:
            self.book.EnableTab(1, False)
  
        self.book.AssignImageList(il)
        self.book.SetPageImage(0, self.img0)
        self.book.SetPageImage(1, self.img1)
        
        self.mainSizer.Add(self.book, 1,wx.ALL| wx.EXPAND)
        self.SetSizer(self.mainSizer)

        if self.GetSize().height > GetSystemMetrics(1) - 100:
            self.SetSize( (self.GetSize().width, GetSystemMetrics(1) - 100))
        
        if self.GetSize().width > GetSystemMetrics(0) - 200:
            self.SetSize( (GetSystemMetrics(0) - 200,self.GetSize().height )) 
        self.SetSize( (1100, 620 ))
        self.Layout()
        self.Thaw()

        self.Bind(wx.EVT_CLOSE, self.OnClose )
        self.abortEvent = delayedresult.AbortEvent()
    def createDagiticiGiris ( self ):
        
        self.p = wx.Panel(self.book,0, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)

        gbs  = wx.GridBagSizer(5, 5)
        lbl_dagitici_sec       =  wx.StaticText(self.p, label="Da��t�c� Se�       :",style=wx.ALIGN_CENTER)
        lbl_dagitici_vergi_no  =  wx.StaticText(self.p, label="Vergi No           :",style=wx.ALIGN_CENTER)
        lbl_dagitici_lisans    =  wx.StaticText(self.p, label="Da��t�c� Lisans No :",style=wx.ALIGN_CENTER)
        lbl_dagitici_adi       =  wx.StaticText(self.p, label="Da��t�c� Ad�       :",style=wx.ALIGN_CENTER)
        
        static_line_1          =  wx.StaticLine(self.p, -1, style=wx.LI_HORIZONTAL)
        
        s.setStaticTextHeader( lbl_dagitici_sec )
        s.setStaticTextHeader( lbl_dagitici_vergi_no )
        s.setStaticTextHeader( lbl_dagitici_lisans )
        s.setStaticTextHeader( lbl_dagitici_adi )
        
        self.default_coices_dagiticilar = []
        self.cmb_dagitici_sec      = wx.ComboBox(self.p,  1, "",size=(700,-1), choices=self.default_coices_dagiticilar,style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY))#bind data veri tabanindan gelecek...
        self.cmb_dagitici_sec.Bind( wx.EVT_COMBOBOX, self.OnDagiticiSec)
        self.lbl_uyari_dagitici_bekleniyor = wx.StaticText(self.p, 1,label="Combobox dolduruluyor, L�tfen Bekleyiniz...",style=wx.ALIGN_CENTER)
        s.setUyariText(self.lbl_uyari_dagitici_bekleniyor)
        
        self.txt_dagitici_vergi_no = wx.TextCtrl(self.p, -1, "", size=(180,-1),style=(wx.TE_READONLY|wx.EXPAND))
        self.txt_dagitici_lisans   = wx.TextCtrl(self.p, -1, "", size=(180,-1),style=(wx.TE_READONLY|wx.EXPAND))
        self.txt_dagitici_adi      = wx.TextCtrl(self.p, -1, "", size=(180,200),style=(wx.TE_READONLY|wx.TE_MULTILINE|wx.EXPAND))
        mevcut_dagitici  = param.get_dagitici_object()
        if mevcut_dagitici:
            self.txt_dagitici_vergi_no.SetValue( mevcut_dagitici.DAGITICI_NO)
            self.txt_dagitici_lisans.SetValue ( mevcut_dagitici.DAGITICI_LISANS)
            self.txt_dagitici_adi.SetValue( mevcut_dagitici.DAGITICI_ADI)
        gbs.Add( self.lbl_uyari_dagitici_bekleniyor, (1,1) ,(1,9),  wx.ALL | wx.EXPAND,3) 
        gbs.Add( lbl_dagitici_sec, (2,1) ,(1,1),  wx.ALL | wx.EXPAND,3)
        gbs.Add( self.cmb_dagitici_sec ,(2,2),(1,1),  wx.ALL | wx.EXPAND,5)
        gbs.Add( static_line_1, (3,0) ,(1,10), wx.ALL | wx.EXPAND,3)

        gbs.Add( lbl_dagitici_vergi_no, (4,1) ,(1,1),  wx.ALL | wx.EXPAND,3)
        gbs.Add( self.txt_dagitici_vergi_no, (4,2) ,(1,2),  wx.ALL | wx.EXPAND,3)
        
        gbs.Add( lbl_dagitici_lisans, (5,1) ,(1,1),  wx.ALL | wx.EXPAND,3)
        gbs.Add( self.txt_dagitici_lisans, (5,2) ,(1,2),  wx.ALL | wx.EXPAND,3)
        
        gbs.Add( lbl_dagitici_adi, (6,1) ,(1,1),  wx.ALL | wx.EXPAND,3)
        gbs.Add( self.txt_dagitici_adi, (6,2) ,(5,2),  wx.ALL | wx.EXPAND,3)
        
        
        self.btn_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        
        self.btn_save  = LibButtons.ThemedGenBitmapTextButton(self.p, id=wx.NewId(), bitmap=image_save,label="Da��t�c� Kaydet")
        
        self.btn_save.Bind(wx.EVT_BUTTON, self.OnDagiticiKaydet)
        self.btn_save.SetToolTipString("Da��t�c� Kaydet")
        self.btn_save.SetBitmapSelected(self.btn_save_pressed)
        s.setButtonFont( self.btn_save )
        gbs.Add(self.btn_save,  (11,2),(1,2), wx.ALIGN_RIGHT)
        
        s.setPanelListBg( self.p )
        self.p.SetSizer(gbs)

        delayedresult.startWorker(self.dagiticilarComboDoldur,self.getDagiticilarFromWs)
        return self.p

    def OnClose( self, event ):
        '''
        print self.tread_ws_dagitici
        self.tread_ws_dagitici.kill()
        print 'After Kill ',self.tread_ws_dagitici.is_alive()
        '''
        self.Destroy()
  
    def getDagiticilarFromWs(self):

        self.dagitici_list_from_ws = EpdkSoapObj.getDagiticiYururlukte()
        dagiticilar_ = map(lambda x: x.lisansGenelBilgi.lisansSahibiUnvani,self.dagitici_list_from_ws)
        dagiticilar = list(set(dagiticilar_))
        dagiticilar.sort()

        return dagiticilar 

    def dagiticilarComboDoldur(self,delayedResulted ):
        if self :
            db_obj   = self.db
            combo_obj = self.cmb_dagitici_sec
            data = []
                     
            try:
                data = delayedResulted.get()
            except:
                s.uyari_message(self,'Web Servisten Da��t�c�lar �ekilemedi. �nternet Ba�lant�n�z� Kontrol Ediniz.','Da��t�c� Giri�i')
                data = []

            combo_obj.Freeze()

            self.default_coices_dagiticilar = data
            db_obj.widgetMakerStr( combo_obj , data)
            self.lbl_uyari_dagitici_bekleniyor.SetLabel('')
            self.lbl_uyari_dagitici_bekleniyor.Refresh()
            combo_obj.Refresh()
            combo_obj.Thaw()
    
    
    def sayfayiKapat(self ):
        self.Destroy()
    def OnDagiticiSec( self, event ):
        
        self.p.Freeze()
        combo_secilen_dagitici_adi = self.cmb_dagitici_sec.GetStringSelection()
        dagitici_firma_ws_degerler = filter(lambda x: x.lisansGenelBilgi.lisansSahibiUnvani == combo_secilen_dagitici_adi,self.dagitici_list_from_ws)
        self.txt_dagitici_vergi_no.SetValue( dagitici_firma_ws_degerler[0].lisansGenelBilgi.vergiNo)
        self.txt_dagitici_lisans.SetValue(dagitici_firma_ws_degerler[0].lisansGenelBilgi.lisansNo)
        self.txt_dagitici_adi.SetValue(dagitici_firma_ws_degerler[0].lisansGenelBilgi.lisansSahibiUnvani) 
        self.p.Thaw()

    def OnDagiticiKaydet( self, event ):
        
        #varsa update, yoksa insert, bir kerede sadece bir dagitici olabilir.
        #dagitici_no     = self.txt_dagitici_id.GetValue().strip()
        dagitici_no     = self.txt_dagitici_vergi_no.GetValue().strip()
        dagitici_lisans = self.txt_dagitici_lisans.GetValue().strip()
        dagitici_adi    = self.txt_dagitici_adi.GetValue().strip()
        user=self.root.user
        if dagitici_lisans != '' and dagitici_no != '' and dagitici_adi != '':
            columns = "DAGITICI_NO,DAGITICI_LISANS,DAGITICI_ADI"
            values  = "'%s','%s','%s'"%(dagitici_no,dagitici_lisans,dagitici_adi )
            table   = 'P_DAGITICI' 
            dagitici_var_mi = param.get_dagitici_object()
            if dagitici_var_mi:
                don = self.db.update_data(table_name=table,
                                          column_name=columns,
                                          values=values,
                                          where='1=1',user=user)
            else:
                
                don = self.db.save_data(  table ,
                                          columns,
                                          values,user=user)
            
            if don != 1:
                s.uyari_message(self,'Kaydetme S�ras�nda Bir Hata Olu�tu','Da��t�c� Giri�i')
            else:
                s.ok_message(self,'Veriler Kaydedildi','Da��t�c� Giri�i')
                
                #s.ok_info_bar(self,self.mainSizer,'Veriler Kaydedildi')
            #s.updateWindow(self.book, self.createLayout)
            refresh_dict     = dict(book            = self.book,
                                temp_page           = 1,
                                create_panel_method = self.createDagiticiGiris,
                                cur_page            = 0,
                                label               = 'Da��t�c� Giri�i',
                                image               = self.img0)
            s.updateWindow(**refresh_dict)
            self.book.EnableTab(1, True)
        else:
            s.uyari_message(self,'Girilen Verileri Kontrol Ediniz.','Da��t�c� Giri�i')
    
    def createBayiGiris ( self ):
        
        self.p2 = wx.Panel(self.book,1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        # wx.Colour(130,208,190)
        #p.SetBackgroundColour(  frame_color )
        gbs = wx.GridBagSizer(5, 5)
        lbl_bayi_adi           =  wx.StaticText(self.p2, label="Bayi Ad�       :",style=wx.ALIGN_LEFT)
        lbl_bayi_adres         =  wx.StaticText(self.p2, label="Bayi Adres     :",style=wx.ALIGN_RIGHT)
        lbl_bayi_lisans        =  wx.StaticText(self.p2, label="Bayi Lisans No :",style=wx.ALIGN_LEFT)
        lbl_bayi_lisans_tarih  =  wx.StaticText(self.p2, label="Bayi Lisans Ba�lama Tarihi :",style=wx.ALIGN_RIGHT)
        lbl_bayi_sehir         =  wx.StaticText(self.p2, label="Bayi �ehir     :",style=wx.ALIGN_LEFT)
        lbl_marka              =  wx.StaticText(self.p2, label="Bayi Tipi :",style=wx.ALIGN_RIGHT)
        lbl_bayi_enlem         =  wx.StaticText(self.p2, label="Bayi Enlem     :",style=wx.ALIGN_LEFT)
        lbl_bayi_boylam        =  wx.StaticText(self.p2, label="Bayi Boylam :",style=wx.ALIGN_RIGHT)
        lbl_tank_sayisi        =  wx.StaticText(self.p2, label="Tank Say�s�    :",style=wx.ALIGN_LEFT)
        lbl_pompa_sayisi       =  wx.StaticText(self.p2, label="Pompa Say�s� :",style=wx.ALIGN_RIGHT)
        lbl_tabanca_sayisi     =  wx.StaticText(self.p2, label="Tabanca Say�s� :",style=wx.ALIGN_LEFT)
        lbl_toplam_kapasite    =  wx.StaticText(self.p2, label="Toplam Kapasite :",style=wx.ALIGN_RIGHT)
        #self.cb_koy_pompasi    = wx.CheckBox(self.p2,32 , MESSAGES.pasif,style=wx.ALIGN_RIGHT)
        
        
        static_line_1          =  wx.StaticLine(self.p2, -1, style=wx.LI_HORIZONTAL)
        
        s.setStaticTextHeader( lbl_bayi_adi )
        s.setStaticTextHeader( lbl_bayi_adres )
        s.setStaticTextHeader( lbl_bayi_lisans )
        s.setStaticTextHeader( lbl_bayi_lisans_tarih )
        s.setStaticTextHeader( lbl_bayi_sehir )
        s.setStaticTextHeader( lbl_marka )
        s.setStaticTextHeader( lbl_bayi_enlem )
        s.setStaticTextHeader( lbl_bayi_boylam )
        s.setStaticTextHeader( lbl_tank_sayisi )
        s.setStaticTextHeader( lbl_pompa_sayisi )
        s.setStaticTextHeader( lbl_tabanca_sayisi )
        s.setStaticTextHeader( lbl_toplam_kapasite )
        
        self.txt_bayi_adi          = wx.TextCtrl(self.p2, -1, "", size=(600,50),style=(wx.TE_MULTILINE|wx.EXPAND))
        self.txt_bayi_adres        = wx.TextCtrl(self.p2, -1, "", size=(600,50),style=(wx.TE_READONLY|wx.TE_MULTILINE|wx.EXPAND))
        
        self.txt_bayi_lisans             = wx.TextCtrl(self.p2, -1, "", size=(180,-1),style=(wx.TE_READONLY|wx.EXPAND))
        self.txt_bayi_lisans_tarih       = wx.TextCtrl(self.p2, -1, "", size=(180,-1),style=(wx.TE_READONLY|wx.EXPAND))
        self.txt_bayi_sehir              = wx.TextCtrl(self.p2, -1, "", size=(180,-1),style=(wx.EXPAND))
        self.txt_marka                   = wx.TextCtrl(self.p2, -1, "", size=(180,-1),style=(wx.EXPAND|wx.TE_READONLY))
        self.txt_bayi_enlem              = wx.TextCtrl(self.p2, -1, "", size=(180,-1),style=(wx.EXPAND))
        self.txt_bayi_enlem.Bind( wx.EVT_CHAR, s.CheckNumber )
        self.txt_bayi_boylam             = wx.TextCtrl(self.p2, -1, "", size=(180,-1),style=(wx.EXPAND))
        self.txt_bayi_boylam.Bind( wx.EVT_CHAR, s.CheckNumber )
        self.txt_tank_sayisi             = wx.TextCtrl(self.p2, -1, "", size=(180,-1),style=(wx.EXPAND))
        self.txt_tank_sayisi.Bind( wx.EVT_CHAR, s.CheckNumber )
        self.txt_pompa_sayisi            = wx.TextCtrl(self.p2, -1, "", size=(180,-1),style=(wx.EXPAND))
        self.txt_pompa_sayisi.Bind( wx.EVT_CHAR, s.CheckNumber )
        self.txt_tabanca_sayisi          = wx.TextCtrl(self.p2, -1, "", size=(180,-1),style=(wx.EXPAND))
        self.txt_tabanca_sayisi.Bind( wx.EVT_CHAR, s.CheckNumber )
        self.txt_toplam_kapasite         = wx.TextCtrl(self.p2, -1, "", size=(180,-1),style=(wx.EXPAND))
        self.txt_toplam_kapasite.Bind( wx.EVT_CHAR, s.CheckNumber )
        
        mevcut_bayi  = param.get_bayi_object()
        if mevcut_bayi:
            if str(mevcut_bayi.BAYI_ENLEM) == 'None':
                mevcut_bayi.BAYI_ENLEM = ""
            if str(mevcut_bayi.BAYI_BOYLAM) == 'None':
                mevcut_bayi.BAYI_BOYLAM = ""
            self.txt_bayi_adi.SetValue(  mevcut_bayi.BAYI_ADI )
            self.txt_bayi_adres.SetValue(  mevcut_bayi.BAYI_ADRES )
            self.txt_bayi_lisans.SetValue(  mevcut_bayi.BAYI_LISANS_NO )
            self.txt_bayi_lisans_tarih.SetValue(  str(mevcut_bayi.BAYI_LISANS_BASLANGIC) ) 
            self.txt_bayi_sehir.SetValue(  mevcut_bayi.BAYI_SEHIR )   
            self.txt_marka.SetValue(  mevcut_bayi.BAYI_TIPI )  
            self.txt_bayi_enlem.SetValue(  str(mevcut_bayi.BAYI_ENLEM) )
            self.txt_bayi_boylam.SetValue(  str(mevcut_bayi.BAYI_BOYLAM) )         
            self.txt_tank_sayisi.SetValue(  str(mevcut_bayi.BAYI_TANK_SAYISI) )
            self.txt_pompa_sayisi.SetValue(  str(mevcut_bayi.BAYI_POMPA_SAYISI) )
            self.txt_tabanca_sayisi.SetValue(  str(mevcut_bayi.BAYI_TABANCA_SAYISI) )
            self.txt_toplam_kapasite.SetValue(  str(mevcut_bayi.BAYI_TANK_KAPASITE) )

        
        gbs.Add( lbl_bayi_adi, (1,1) ,(1,1),  wx.ALL | wx.EXPAND,3)
        gbs.Add( self.txt_bayi_adi ,(1,2) ,(1,2),  wx.ALL | wx.EXPAND,5)
        
        gbs.Add( lbl_bayi_adres, (2,1) ,(1,1),  wx.ALL | wx.EXPAND,3)
        gbs.Add( self.txt_bayi_adres ,(2,2) ,(1,2),  wx.ALL | wx.EXPAND,5)
        
        gbs.Add( lbl_bayi_lisans, (3,1) ,(1,1),  wx.ALL | wx.EXPAND,3)
        gbs.Add( self.txt_bayi_lisans ,(3,2) ,(1,1),  wx.ALL | wx.EXPAND,5)
        
        gbs.Add( lbl_bayi_lisans_tarih, (3,3) ,(1,1),  wx.ALL | wx.EXPAND,3)
        gbs.Add( self.txt_bayi_lisans_tarih ,(3,4) ,(1,1),  wx.ALL | wx.EXPAND,5)
        
        gbs.Add( lbl_bayi_sehir, (4,1) ,(1,1),  wx.ALL | wx.EXPAND,3)
        gbs.Add( self.txt_bayi_sehir ,(4,2) ,(1,1),  wx.ALL | wx.EXPAND,5)
        
        gbs.Add( lbl_marka, (4,3) ,(1,1),  wx.ALL | wx.EXPAND,3)
        gbs.Add( self.txt_marka ,(4,4) ,(1,1),  wx.ALL | wx.EXPAND,5)
        
        gbs.Add( static_line_1, (5,0) ,(1,10), wx.ALL | wx.EXPAND,3)
        
        gbs.Add( lbl_bayi_enlem, (6,1) ,(1,1),  wx.ALL | wx.EXPAND,3)
        gbs.Add( self.txt_bayi_enlem ,(6,2) ,(1,1),  wx.ALL | wx.EXPAND,5)
        
        gbs.Add( lbl_bayi_boylam, (6,3) ,(1,1),  wx.ALL | wx.EXPAND,3)
        gbs.Add( self.txt_bayi_boylam ,(6,4) ,(1,1),  wx.ALL | wx.EXPAND,5)
        
        gbs.Add( lbl_tank_sayisi, (7,1) ,(1,1),  wx.ALL | wx.EXPAND,3)
        gbs.Add( self.txt_tank_sayisi ,(7,2) ,(1,1),  wx.ALL | wx.EXPAND,5)
        
        gbs.Add( lbl_pompa_sayisi, (7,3) ,(1,1),  wx.ALL | wx.EXPAND,3)
        gbs.Add( self.txt_pompa_sayisi ,(7,4) ,(1,1),  wx.ALL | wx.EXPAND,5)
        
        gbs.Add( lbl_tabanca_sayisi, (8,1) ,(1,1),  wx.ALL | wx.EXPAND,3)
        gbs.Add( self.txt_tabanca_sayisi ,(8,2) ,(1,1),  wx.ALL | wx.EXPAND,5)
        
        gbs.Add( lbl_toplam_kapasite, (8,3) ,(1,1),  wx.ALL | wx.EXPAND,3)
        gbs.Add( self.txt_toplam_kapasite ,(8,4) ,(1,1),  wx.ALL | wx.EXPAND,5)
        
        
        self.btn_list_pressed  =  wx.Image("%snotebook_accept.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_list = wx.Image("%snotebook.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        #bitmap_save = wx.Bitmap( "%ssave.png"%image_path, wx.BITMAP_TYPE_ANY )
        
        self.btn_list  = LibButtons.ThemedGenBitmapTextButton(self.p2, id=wx.NewId(), bitmap=image_list,label="Bayi Listesi")
        #self.btn_save.SetBitmapLabel("Fiyatlar� Pompaya G�nder")
        
        self.btn_list.Bind(wx.EVT_BUTTON, self.OnBayiListesi)
        self.btn_list.SetToolTipString("Bayi Listesi")
        self.btn_list.SetBitmapSelected(self.btn_list_pressed)
        s.setButtonFont( self.btn_list)
        
        self.btn_temizle_pressed  =  wx.Image("%strash_black.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_temizle = wx.Image("%strash.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        #bitmap_save = wx.Bitmap( "%ssave.png"%image_path, wx.BITMAP_TYPE_ANY )
        
        self.btn_temizle  = LibButtons.ThemedGenBitmapTextButton(self.p2, id=wx.NewId(), bitmap=image_temizle,label="  Temizle")
        #self.btn_save.SetBitmapLabel("Fiyatlar� Pompaya G�nder")
        
        self.btn_temizle.Bind(wx.EVT_BUTTON, self.OnTemizle)
        self.btn_temizle.SetToolTipString("Temizle")
        self.btn_temizle.SetBitmapSelected(self.btn_temizle_pressed)
        s.setButtonFont( self.btn_temizle )
        gbs.Add( self.btn_list ,(1,4) ,(1,1),  wx.ALL | wx.EXPAND,5)
        gbs.Add( self.btn_temizle ,(2,4) ,(1,1),  wx.ALL | wx.EXPAND,5)
        
        self.btn_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        #bitmap_save = wx.Bitmap( "%ssave.png"%image_path, wx.BITMAP_TYPE_ANY )
        
        self.btn_save  = LibButtons.ThemedGenBitmapTextButton(self.p2, id=wx.NewId(), bitmap=image_save,label="Bayi Kaydet")
        #self.btn_save.SetBitmapLabel("Fiyatlar� Pompaya G�nder")
        
        self.btn_save.Bind(wx.EVT_BUTTON, self.OnBayiKaydet)
        self.btn_save.SetToolTipString("Bayi Kaydet")
        self.btn_save.SetBitmapSelected(self.btn_save_pressed)
        s.setButtonFont( self.btn_save )
        gbs.Add(self.btn_save,  (9,3),(1,2), wx.ALIGN_RIGHT)
        
        self.p2.SetSizer( gbs )
        s.setPanelListBg( self.p2 )
        return self.p2
    def OnTemizle(self,event):
        self.p2.Layout()
        self.txt_bayi_adi.SetValue("")
        self.txt_bayi_adres.SetValue("")
        self.txt_bayi_lisans.SetValue("")
        self.txt_bayi_lisans_tarih.SetValue("")
        self.txt_bayi_sehir.SetValue("")
        self.txt_marka.SetValue("")
        self.txt_bayi_enlem.SetValue("")
        self.txt_bayi_boylam.SetValue("")
        self.txt_tank_sayisi.SetValue("")
        self.txt_pompa_sayisi.SetValue("")
        self.txt_tabanca_sayisi.SetValue("")
        self.txt_toplam_kapasite.SetValue("")
        
        self.txt_bayi_adi.SetEditable( True )
        self.txt_bayi_sehir.SetEditable( True )
        
    def OnBayiListesi(self,event):
        self.txt_bayi_adi.SetEditable( False )
        self.txt_bayi_sehir.SetEditable( False )
        createBayiiList(self, sabitler.EKRAN_ID_MAP.AYARLAR_EKRANLAR.id_e_bayi_list,'Bayi Listesi')
    def OnBayiKaydet( self,event):
        user=self.root.user
        get_aktif_dagitici = param.get_dagitici_object()
        dagitici_id     = get_aktif_dagitici.ID_DAGITICI
        
        bayi_adi          = self.txt_bayi_adi.GetValue().strip()
        bayi_adres        = self.txt_bayi_adres.GetValue().strip().replace(',','-')
        bayi_lisans       = self.txt_bayi_lisans.GetValue().strip()
        bayi_lisans_tarih = self.txt_bayi_lisans_tarih.GetValue().strip()
        bayi_sehir        = self.txt_bayi_sehir.GetValue().strip()
        bayi_tipi        = self.txt_marka.GetValue().strip()
        bayi_enlem        = self.txt_bayi_enlem.GetValue().strip()
        bayi_boylam       = self.txt_bayi_boylam.GetValue().strip()
        if bayi_enlem == '':
            bayi_enlem = 0.1
        if bayi_boylam == '':
            bayi_boylam = 0.1
        tank_sayisi       = self.txt_tank_sayisi.GetValue().strip()
        pompa_sayisi      = self.txt_pompa_sayisi.GetValue().strip()
        tabanca_sayisi    = self.txt_tabanca_sayisi.GetValue().strip()
        toplam_kapasite   = self.txt_toplam_kapasite.GetValue().strip()
        
        bayi_dict         = {}
        bayi_dict['dagitici_id']          = dagitici_id
        bayi_dict['bayi_adres']           = bayi_adres
        bayi_dict['bayi_lisans_no']       = bayi_lisans
        bayi_dict['bayi_adi']             = bayi_adi
        bayi_dict['bayi_lisans_tarih']    = bayi_lisans_tarih
        bayi_dict['bayi_sehir']           = bayi_sehir
        bayi_dict['bayi_tipi']            = bayi_tipi
        bayi_dict['bayi_enlem']           = bayi_enlem
        bayi_dict['bayi_boylam']          = bayi_boylam
        bayi_dict['tank_sayisi']          = tank_sayisi
        bayi_dict['pompa_sayisi']         = pompa_sayisi
        bayi_dict['tabanca_sayisi']       = tabanca_sayisi
        bayi_dict['toplam_kapasite']      = toplam_kapasite
        
        if tank_sayisi != '' and pompa_sayisi != '' and tabanca_sayisi != ''  and toplam_kapasite != '':
            table_name = "P_BAYI"
            column_str = """
                            [ID_DAGITICI],
                            [BAYI_LISANS_NO],
                            [BAYI_ADI],
                            [BAYI_ADRES],
                            [BAYI_SEHIR],
                            [BAYI_TIPI],
                            [BAYI_LISANS_BASLANGIC],
                            [BAYI_ENLEM],
                            [BAYI_BOYLAM],
                            [BAYI_TANK_SAYISI],
                            [BAYI_POMPA_SAYISI],
                            [BAYI_TANK_KAPASITE],
                            [BAYI_TABANCA_SAYISI]
                        """
            values  =   """
                            %(dagitici_id)s,
                            '%(bayi_lisans_no)s',
                            '%(bayi_adi)s',
                            '%(bayi_adres)s',
                            '%(bayi_sehir)s',
                            '%(bayi_tipi)s',
                            '%(bayi_lisans_tarih)s',
                            %(bayi_enlem)s,
                            %(bayi_boylam)s,
                            %(tank_sayisi)s,
                            %(pompa_sayisi)s,
                            %(toplam_kapasite)s,
                            %(tabanca_sayisi)s
                        """% bayi_dict
            bayi_var_mi = param.get_bayi_object()
            if bayi_var_mi:
                don = self.db.update_data(table_name  = table_name,
                                          column_name = column_str,
                                          values      = values,where='1=1',user=user)
            else:
                don = self.db.save_data( table_name,
                                         column_str,
                                         values,user=user)
            
            if don != 1:
                s.uyari_message(self,'Kaydetme S�ras�nda Bir Hata Olu�tu','Bayi Kaydet')
            else:
                s.ok_message(self,'Veriler Kaydedildi','Bayi Kaydet')
                
                #s.ok_info_bar(self,self.mainSizer,'Veriler Kaydedildi')
                refresh_dict = dict(book            = self.book,
                                temp_page           = 0,
                                create_panel_method = self.createBayiGiris,
                                cur_page            = 1,
                                label               = 'Bayi Giri�i',
                                image               = self.img1)
            s.updateWindow(**refresh_dict)
            self.book.EnableTab(1, True)
        else:
            s.uyari_message(self,'Girilen Verileri Kontrol Ediniz.','Bayi Kaydet') 
class createBayiiList( wx.Frame ):
    def __init__(self, parent, id_, title):
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR ))
        self.db = param.get_root( self ).DB
        self.O  = param.get_root( self ).O
        self.Freeze()
        self.p = self.O.createNullPanel( self )
        self.mainSizer = wx.BoxSizer( wx.VERTICAL )
        self.SetSizer(self.mainSizer)
        self.lbl_uyari_bayi_bekleniyor = wx.StaticText(self.p, 1,label="Bayi listesi dolduruluyor, L�tfen Bekleyiniz...",style=wx.ALIGN_CENTER)
        s.setUyariText(self.lbl_uyari_bayi_bekleniyor)
        self.mainSizer.Add(self.p,-1,wx.ALL|wx.EXPAND)
        
        self.Centre()
        self.p.SetSize((700,600))
        self.SetSize((700,600)) 
        self.Show()
        self.Thaw()
        self.LayoutItems()
    def LayoutItems(self):
        
        
        self.checked_bayi    = None
        self.bayi_adi   = self.GetParent().txt_bayi_adi.GetValue()
        self.bayi_sehir = self.GetParent().txt_bayi_sehir.GetValue()
        self.jobID = 0
        dagitici                 = self.db.get_raw_data_all("P_DAGITICI",selects="DAGITICI_LISANS,DAGITICI_ADI",dict_mi='object')
        dagitici_adi             = dagitici[0].DAGITICI_ADI
        dagitici_lisans_no       = dagitici[0].DAGITICI_LISANS
        bayi_adi_                = util.uppercaseTr(self.bayi_adi).strip()
        bayi_sehir_              = util.uppercaseTr(self.bayi_sehir).strip()

        if bayi_sehir_ == '':
            s.uyari_message(self,'L�tfen Bayinin �ehrini Giriniz',"Bayi Tan�mlama")
            self.Freeze()
            self.Destroy()
            self.Thaw()
        else:
            delayedresult.startWorker( self.createList, self.getDagiticiyaBagliBayilikFromWs, 
                                       wargs = (bayi_adi_,bayi_sehir_,dagitici_lisans_no), jobID=self.jobID)
    
        
    def getDagiticiyaBagliBayilikFromWs(self,bayi_adi,bayi_sehir,dagitici_lisans_no ):
        
        dagiticiya_bagli_bayilik_list_from_ws = EpdkSoapObj.getDagiticiyaBagliBayilikListesi(dagitici_lisans_no)
    
        if bayi_sehir and bayi_adi :
            bayiler = filter(lambda x: x.il == '%s'%bayi_sehir and x.lisansSahibi.find(bayi_adi)>0,dagiticiya_bagli_bayilik_list_from_ws)
        elif bayi_sehir and not bayi_adi:
            bayiler = filter(lambda x: x.il == '%s'%bayi_sehir,dagiticiya_bagli_bayilik_list_from_ws)
        elif not bayi_sehir and bayi_adi:
            bayiler = filter(lambda x: x.lisansSahibi.find(str(bayi_adi))>0,dagiticiya_bagli_bayilik_list_from_ws)
        elif not bayi_adi and not bayi_sehir:
            bayiler = dagiticiya_bagli_bayilik_list_from_ws

        bayiler_attr_list = ['lisansNo','lisansSahibi','tesisAdresi','il','lisansNo','lisansBaslangicTarihi','kategorisi']
        self.bayiler_formatted = map(lambda x:[getattr(x,a) for a in bayiler_attr_list],bayiler)
    
        return self.bayiler_formatted
    def createList(self, delayedResult ):
        #self.lbl_uyari_dagitici_bekleniyor = wx.StaticText(self.p, 1,label="Combobox dolduruluyor, L�tfen Bekleyiniz...",style=wx.ALIGN_CENTER)
        #s.setUyariText(self.lbl_uyari_dagitici_bekleniyor)
        if self:
            data = []
            try:
                data = delayedResult.get()
            except:
                s.uyari_message(self,'Web Servisten Da��t�c�ya Ba�l� Bayiler �ekilemedi. �nternet Ba�lant�n�z� Kontrol Ediniz.','Bayi Giri�i')
                data = []
               
            self.Freeze()
            btn_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
            image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
            btn_save  = LibButtons.ThemedGenBitmapTextButton(self, id=wx.NewId(), bitmap=image_save,label="Bayi Se�")
            btn_save.Bind(wx.EVT_BUTTON, self.OnBayiGonder)
            btn_save.SetToolTipString("Bayi Se�")
            btn_save.SetBitmapSelected(btn_save_pressed)
            s.setPanelListBg( self )
            s.setButtonFont( btn_save )
    
            bayiler_header    = ['Se�,digit','Bayi Ad�,string','Bayi Adres,string','Bayi �ehir,string','Lisans No,string','Lisans Ba�lama,datetime','Bayi Tipi,string']
           
            
            self.p_list = self.O.createStaticList(self,bayiler_header,data,lineRadio=True,radio_set_method=self.OnChecked )
            
            self.p.Destroy()
            self.mainSizer.Add(self.p_list,1,wx.EXPAND)
            self.mainSizer.Add(btn_save,0,wx.ALL|wx.EXPAND)
            self.Refresh()
            self.Layout()
            self.Thaw()  
    def OnChecked( self,item_id):
        self.checked_bayi = item_id
        
    def OnBayiGonder( self, event ):
        selectted_data =  self.bayiler_formatted[self.checked_bayi -1 ]
        
        #0 id, #1 ad #2 adres #3 sehir #4 lisas #5 lisans tarihi #6 tip
        self.GetParent().txt_bayi_adi.SetValue   ( selectted_data[1] )  
        self.GetParent().txt_bayi_adres.SetValue ( selectted_data[2] )
        self.GetParent().txt_bayi_sehir.SetValue ( selectted_data[3] )
        self.GetParent().txt_bayi_lisans.SetValue ( selectted_data[4] )
        self.GetParent().txt_bayi_lisans_tarih.SetValue ( util.get_date_formatted(selectted_data[5]) )
        #self.GetParent().txt_bayi_lisans_tarih.SetValue ( str(selectted_data[5]) )
        self.GetParent().txt_marka.SetValue( selectted_data[6])
        self.Destroy()
class win_Yakit_Tanimlama(wx.Frame):
    
    def __init__(self,parent,id_,title):
        wx.Frame.__init__(self, parent,id_, title=title,
                style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR ))
        self.root = param.get_root( self )
        self.db   = self.root.DB
        self.O    = self.root .O 
        self.LayoutItems()
        #self.Centre()
        self.Show()
        
    def LayoutItems(self):
        mainSizer = wx.BoxSizer( wx.VERTICAL )
        self.Freeze()
        self.SetSizer(mainSizer)
        self.SetSize((800,600))
        self.check_list    = []
        self.uncheck_list  = []
        self.liste = self.createYakitListesi()

        self.btn_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_save  = LibButtons.ThemedGenBitmapTextButton(self, id=wx.NewId(), bitmap=image_save,label="Kaydet")
        self.btn_save.Bind(wx.EVT_BUTTON, self.OnYakitSave)
        self.btn_save.SetToolTipString("Aktif Se�ilen Yak�tlar� Kaydet")
        self.btn_save.SetBitmapSelected(self.btn_save_pressed)
        s.setPanelListBg( self )
        s.setButtonFont( self.btn_save )
        mainSizer.Add(self.liste,1,wx.EXPAND)
        mainSizer.Add(self.btn_save,0,wx.ALL|wx.EXPAND)
        self.Layout()
        self.Thaw()
        
    def OnYakitSave(self,event):
        user=self.root.user
        for item_id in self.check_list:
            self.db.update_data( table_name='YAKIT',column_name='YAKIT_DURUM',values='1',where='ID_YAKIT=%s'%item_id,user=user)
        for item_id in self.uncheck_list:
            self.db.update_data( table_name='YAKIT',column_name='YAKIT_DURUM',values='2',where='ID_YAKIT=%s'%item_id,user=user)
        s.ok_message(self, "Veriler Kaydedildi","Yak�t Durumu Kaydet")
        self.Layout()   
        
        
    def createYakitListesi( self ):
        
        self.data_all   = self.db.get_raw_data_all(table_name = 'YAKIT',
                                                   selects="ID_YAKIT,YAKIT_ADI,YAKIT_BIRIM_FIYAT,YAKIT_DURUM" )
        headers = ['Yak�t Aktif Mi,digit','Yak�t Ad�,string','Yak�t Birim Fiyat�,currency','Yak�t Durumu,digit']     
        self.liste = self.O.createStaticList(self, headers, self.data_all,lineCheck=True,checked_set_method=self.OnChecked,checked_index=3)  
        self.liste.Layout()
        
        return self.liste
        
    def OnChecked( self,item_id, item_state):
        if item_state == True:
            if item_id in self.uncheck_list:
                self.uncheck_list.remove( item_id )
            self.check_list.append( item_id )
        else:
            if item_id in self.check_list:
                self.check_list.remove( item_id )
            self.uncheck_list.append( item_id )
class win_Pompa_Tanimlama(wx.Frame):

    def __init__(self, parent, id_, title):

        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR|wx.EXPAND ))
        self.root            = param.get_root( self )
        self.db              = self.root.DB
        self.O               = self.root.O
        self.selected_item   = {}
        self.dummy           = sabitler.dummy()
        self.LayoutItems()
        
    def LayoutItems(self):

        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.mainSizer)
        self.book  = aui.AuiNotebook(self,1,agwStyle=(aui.AUI_NB_TOP|aui.AUI_NB_SCROLL_BUTTONS))
        self.book.SetArtProvider(aui.AuiDefaultTabArt())
        s.setPanelListBg( self.book )
        self.mainSizer.Add(self.book, 1,wx.ALL | wx.EXPAND)
        
        il   = wx.ImageList(64, 64)
        self.img0 = il.Add(wx.Bitmap('%spompa_ekle_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img1 = il.Add(wx.Bitmap('%stabanca_ekle_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img2 = il.Add(wx.Bitmap('%spompa_list_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img3 = il.Add(wx.Bitmap('%skart_okuyucu_add_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img4 = il.Add(wx.Bitmap('%skart_okuyucu_list_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        
        self.p  = self.createAddPompa()
        s.setPanelListBg( self.p )
        self.p2 = self.createAddTabanca()
        s.setPanelListBg( self.p2 )
        self.p3 = self.createPompaList()
        s.setPanelListBg( self.p3 )
        self.p4 = self.createAddKartOkuyucu()
        s.setPanelListBg( self.p4 )
        self.p5 = self.createKartOkuyucuList()
        s.setPanelListBg(self.p5)
        self.book.AddPage( self.p, "Pompa Tan�mlama",True )
        self.book.AddPage( self.p2,"Tabanca Tan�mlama" )
        self.book.AddPage( self.p3,"Pompa Listesi" )
        self.book.AddPage( self.p4,"Kart Okuyucu Tan�mlama" )
        self.book.AddPage( self.p5,"Kart Okuyucu Listesi" )
        self.book.Bind(aui.EVT_AUINOTEBOOK_PAGE_CHANGED, self.OnBookPageChanged )

        self.book.AssignImageList(il)
 
        self.book.SetPageImage(0, self.img0)
        self.book.SetPageImage(1, self.img1)
        self.book.SetPageImage(2, self.img2)
        self.book.SetPageImage(3, self.img3)
        self.book.SetPageImage(4, self.img4)

    def createAddPompa(self):
        if self.selected_item.has_key(0):
            selected_item                       = self.selected_item[0]
        else:
            selected_item = None
        #selected_item                           = self.selected_item
        self.dummy.ID_POMPA                     = -1
        self.dummy.POMPA_ADI                    = ''
        self.dummy.POMPA_NO_ON                  = ''
        self.dummy.POMPA_NO_ARKA                = ''
        self.dummy.ARABIRIM_MARKASI             = -1
        self.dummy.ARABIRIM_ADRESI              = ''
        self.dummy.PORT_TIPI                    = ''
        self.dummy.ID_PORT                      = -1
        self.dummy.ARABIRIMDEKI_BEYIN_SAYISI    = ''
        self.dummy.ARABIRIMDEKI_TABANCA_SAYISI  = ''
        data_pompa                              = self.dummy

        if selected_item :
            data_pompa_ = self.db.get_raw_data_all("POMPA",selects="ID_POMPA,POMPA_ADI,POMPA_NO_ON,POMPA_NO_ARKA,ARABIRIM_MARKASI,ARABIRIM_ADRESI,PORT_TIPI,ID_PORT,ARABIRIMDEKI_BEYIN_SAYISI,ARABIRIMDEKI_TABANCA_SAYISI",where="ID_POMPA=%s"%selected_item,dict_mi='object')
            if len(data_pompa_)>0:
                data_pompa = data_pompa_[0]
        self.panel_add_pompa = wx.Panel(self.book,1, style = wx.TAB_TRAVERSAL
             | wx.CLIP_CHILDREN
             | wx.FULL_REPAINT_ON_RESIZE
             |wx.EXPAND)
        
        self.gbs_add_pompa =  wx.GridBagSizer(0, 0)

        lbl_pompa_adi       = wx.StaticText(self.panel_add_pompa, label="Pompa Ad� :  ")
        s.setStaticTextList( lbl_pompa_adi )
        #self.txt_pompa_adi  = wx.TextCtrl  (self.panel_add_pompa, -1, str(data_pompa.POMPA_ADI),size=(180,-1))
        self.txt_pompa_adi  = wx.TextCtrl  (self.panel_add_pompa, -1, "",size=(80,-1))
        self.txt_pompa_adi.SetMaxLength(6)
        if data_pompa.POMPA_ADI:
            self.txt_pompa_adi.SetValue(str(data_pompa.POMPA_ADI))
        lbl_pompa_no_on       = wx.StaticText(self.panel_add_pompa, label="Pompa No(�n) :  ")
        s.setStaticTextList( lbl_pompa_no_on )
        self.txt_pompa_no_on  = s.CheckNumberMasked(self.panel_add_pompa,'pompa no(�n)',2)
        if data_pompa.POMPA_NO_ON:
            self.txt_pompa_no_on.SetValue(str(data_pompa.POMPA_NO_ON))
        lbl_pompa_no_arka       = wx.StaticText(self.panel_add_pompa, label="Pompa No(Arka) :  ")
        s.setStaticTextList( lbl_pompa_no_arka )
        self.txt_pompa_no_arka  = s.CheckNumberMasked(self.panel_add_pompa,'pompa no(arka)',2)
        if data_pompa.POMPA_NO_ARKA:
            self.txt_pompa_no_arka.SetValue(str(data_pompa.POMPA_NO_ARKA))
        lbl_arabirim_marka       = wx.StaticText(self.panel_add_pompa, label="Arabirim Markas� :  ")
        s.setStaticTextList( lbl_arabirim_marka )
        #choices_arabirim_marka = ['Beko','Teosis']
        self.cmb_arabirim_marka  = wx.ComboBox(self.panel_add_pompa,-1,"", choices=[],
                                            style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY))
        arabirim_marka_parametric_objects = []
        arabirim_marka_parametric_objects = self.db.get_parameters_all("P_ARABIRIM_MARKALAR",order="ARABIRIM_MARKA_ADI",where="ID_ACTIVE=1",selects="ID_ARABIRIM_MARKA,ARABIRIM_MARKA_ADI")
        if arabirim_marka_parametric_objects != [] :
            self.db.widgetMaker( self.cmb_arabirim_marka , arabirim_marka_parametric_objects)   
        if data_pompa.ARABIRIM_MARKASI != -1:
            index_arabirim_markasi = arabirim_marka_parametric_objects.index(filter(lambda x:  x.id==int(data_pompa.ARABIRIM_MARKASI),arabirim_marka_parametric_objects)[0])
            self.cmb_arabirim_marka.SetSelection(index_arabirim_markasi)

        lbl_arabirim_adres       = wx.StaticText(self.panel_add_pompa, label="Arabirim Adresi :  ")
        s.setStaticTextList( lbl_arabirim_adres )
        self.txt_arabirim_adres  = wx.TextCtrl  (self.panel_add_pompa, -1, str(data_pompa.ARABIRIM_ADRESI),size=(180,-1))
        self.txt_arabirim_adres.Bind(wx.EVT_CHAR,s.CheckNumber)     
        
        lbl_port_tipi = wx.StaticText(self.panel_add_pompa, label="Port Tipi :")
        s.setStaticTextList( lbl_port_tipi )
        self.txt_port_tipi = wx.TextCtrl  (self.panel_add_pompa, -1, "Serial",size=(70,-1),style=wx.TE_READONLY)

        lbl_port_adi = wx.StaticText(self.panel_add_pompa, label="Port Ad� :")
        s.setStaticTextList( lbl_port_adi )
        self.cmb_add_pompa_port_adi  = wx.ComboBox(self.panel_add_pompa,-1,"", choices=[],style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY)) 
        port_adi_parametric_objects = []
        port_adi_parametric_objects = self.db.get_parameters_all("PORTLAR",order="PORT_ADI",selects="ID_PORT,PORT_ADI")
        
        if port_adi_parametric_objects != [] :
            self.db.widgetMaker( self.cmb_add_pompa_port_adi , port_adi_parametric_objects)           
        if data_pompa.ID_PORT != -1:
            index_port = port_adi_parametric_objects.index(filter(lambda x:  x.id==data_pompa.ID_PORT,port_adi_parametric_objects)[0])
            self.cmb_add_pompa_port_adi.SetSelection(index_port)

        lbl_beyin_sayisi       = wx.StaticText(self.panel_add_pompa, label="Arabirimdeki Beyin Say�s� :  ")
        s.setStaticTextList( lbl_beyin_sayisi )
        self.txt_beyin_sayisi  = s.CheckNumberMasked(self.panel_add_pompa,'beyin say�s�',1)
        if data_pompa.ARABIRIMDEKI_BEYIN_SAYISI:           
            self.txt_beyin_sayisi.SetValue(str(data_pompa.ARABIRIMDEKI_BEYIN_SAYISI))

        lbl_tabanca_sayisi       = wx.StaticText(self.panel_add_pompa, label="Arabirimdeki Tabanca Say�s� :  ")
        s.setStaticTextList( lbl_tabanca_sayisi )
        self.txt_tabanca_sayisi  = s.CheckNumberMasked(self.panel_add_pompa,'tabanca say�s�',1)
        if data_pompa.ARABIRIMDEKI_TABANCA_SAYISI:           
            self.txt_tabanca_sayisi.SetValue(str(data_pompa.ARABIRIMDEKI_TABANCA_SAYISI))
        self.btn_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_save  = LibButtons.ThemedGenBitmapTextButton(self.panel_add_pompa, id=wx.NewId(), bitmap=image_save,label="Verileri Kaydet")
        self.btn_save.Bind(wx.EVT_BUTTON,lambda event: self.OnPompaSave(event,selected_item))
        self.btn_save.SetToolTipString("Pompa Bilgilerini Kaydet")
        self.btn_save.SetBitmapSelected(self.btn_save_pressed)
        s.setButtonFont( self.btn_save )
       
        self.gbs_add_pompa.Add( lbl_pompa_adi, (1,1),(1,1),  wx.ALL,1)
        self.gbs_add_pompa.Add( self.txt_pompa_adi, (1,2) ,(1,1),  wx.ALL,1)
        
        self.gbs_add_pompa.Add( lbl_pompa_no_on, (2,1),(1,1),  wx.ALL,1)
        self.gbs_add_pompa.Add( self.txt_pompa_no_on, (2,2) ,(1,1),  wx.ALL,1)
        
        self.gbs_add_pompa.Add( lbl_pompa_no_arka, (2,3),(1,1),  wx.ALL,1)
        self.gbs_add_pompa.Add( self.txt_pompa_no_arka, (2,4) ,(1,1),  wx.ALL,1)
        
        self.gbs_add_pompa.Add( lbl_arabirim_marka, (3,1),(1,1),  wx.ALL,1)
        self.gbs_add_pompa.Add( self.cmb_arabirim_marka, (3,2) ,(1,1),  wx.ALL,1)

        self.gbs_add_pompa.Add( lbl_arabirim_adres, (4,1),(1,1),  wx.ALL,1)
        self.gbs_add_pompa.Add( self.txt_arabirim_adres, (4,2) ,(1,1),  wx.ALL,1)
        
        self.gbs_add_pompa.Add( lbl_port_tipi, (5,1),(1,1),  wx.ALL,1)
        self.gbs_add_pompa.Add( self.txt_port_tipi, (5,2) ,(1,1),  wx.ALL,1)       

        self.gbs_add_pompa.Add( lbl_port_adi, (6,1),(1,1),  wx.ALL,1)
        self.gbs_add_pompa.Add( self.cmb_add_pompa_port_adi, (6,2) ,(1,1),  wx.ALL,1)       

        self.gbs_add_pompa.Add( lbl_beyin_sayisi, (7,1),(1,1),  wx.ALL,1)
        self.gbs_add_pompa.Add( self.txt_beyin_sayisi, (7,2) ,(1,1),  wx.ALL,1)       

        self.gbs_add_pompa.Add( lbl_tabanca_sayisi, (8,1),(1,1),  wx.ALL,1)
        self.gbs_add_pompa.Add( self.txt_tabanca_sayisi, (8,2) ,(1,1),  wx.ALL,1)       


        self.gbs_add_pompa.Add(self.btn_save,  (9,1),(1,1),wx.ALL,1)        
        
        self.panel_add_pompa.SetSizerAndFit(self.gbs_add_pompa)   
        return self.panel_add_pompa

    def createAddTabanca(self):
        if self.selected_item.has_key(1):
            selected_item                   = self.selected_item[1]
        else:
            selected_item = None
        self.dummy.ID_POMPA             = -1
        self.dummy.POMPA_NO             = ''
        self.dummy.BEYIN_NO             = ''
        self.dummy.ID_BASAMAK_KAYDIRMA  = -1
        self.dummy.TABANCA_NO           = ''
        self.dummy.ID_YAKIT_TANKI       = -1
        self.dummy.ID_PORT              = -1
        self.dummy.MODUL_NO             = ''
        data_tabanca               = self.dummy

        if selected_item :
            data_tabanca = self.db.get_raw_data_all("TABANCALAR",selects="ID_TABANCA,ID_POMPA,POMPA_NO,BEYIN_NO,ID_BASAMAK_KAYDIRMA,TABANCA_NO,ID_YAKIT_TANKI",where="ID_TABANCA=%s"%selected_item,dict_mi='object')[0]

        self.panel_add_tabanca = wx.Panel(self.book,1, style = wx.TAB_TRAVERSAL
             | wx.CLIP_CHILDREN
             | wx.FULL_REPAINT_ON_RESIZE
             |wx.EXPAND)
        
        self.gbs_add_tabanca =  wx.GridBagSizer(10,5)

        lbl_pompa_adi       = wx.StaticText(self.panel_add_tabanca, label="Pompa Ad� :  ")
        s.setStaticTextList( lbl_pompa_adi )
        self.cmb_pompa_adi  = wx.ComboBox(self.panel_add_tabanca,1,"", choices=[],style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY)) 
        pompa_adi_parametric_objects = []
        pompa_adi_parametric_objects = self.db.get_parameters_all("POMPA",order="POMPA_ADI",selects="ID_POMPA,POMPA_ADI")
        self.cmb_pompa_adi.Bind( wx.EVT_COMBOBOX , self.OnPompaChanged )
        if pompa_adi_parametric_objects != [] :
            self.db.widgetMaker( self.cmb_pompa_adi , pompa_adi_parametric_objects)
        
        if data_tabanca.ID_POMPA != -1:
            index_ = pompa_adi_parametric_objects.index(filter(lambda x:  x.id==data_tabanca.ID_POMPA,pompa_adi_parametric_objects)[0])
            self.cmb_pompa_adi.SetSelection(index_)
            
        self.choices_pompa_no = []
        self.lbl_pompa_no       = wx.StaticText(self.panel_add_tabanca, label="Pompa No :  ")
        s.setStaticTextList( self.lbl_pompa_no )
        self.lbl_pompa_no.Disable()
        self.cmb_pompa_no  = wx.ComboBox  (self.panel_add_tabanca, 1,choices = self.choices_pompa_no ,style=(wx.CB_DROPDOWN|wx.CB_READONLY))
        self.cmb_pompa_no.Disable()
        self.choices_beyin_no = []
        self.lbl_beyin_no       = wx.StaticText(self.panel_add_tabanca, label="Beyin No :  ")
        s.setStaticTextList( self.lbl_beyin_no )
        self.lbl_beyin_no.Disable()
        self.cmb_beyin_no  = wx.ComboBox  (self.panel_add_tabanca, 1,choices = self.choices_beyin_no ,style=(wx.CB_DROPDOWN|wx.CB_READONLY))
        self.cmb_beyin_no.Disable()
        self.lbl_basamak_operation       = wx.StaticText(self.panel_add_tabanca, label="Basamak Operasyonu :  ")
        s.setStaticTextList( self.lbl_basamak_operation )
        self.lbl_basamak_operation.Disable()
        self.cmb_basamak_operation  = wx.ComboBox(self.panel_add_tabanca,1,"", choices=[],
                                            style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY))

        basamak_operation_parametric_objects = []
        basamak_operation_parametric_objects = self.db.get_parameters_all("P_BASAMAK_KAYDIRMA",where="ID_ACTIVE=1",order="ID_BASAMAK_KAYDIRMA",selects="ID_BASAMAK_KAYDIRMA,BASAMAK_KAYDIRMA_ADI")
        if basamak_operation_parametric_objects != []:
            self.db.widgetMaker( self.cmb_basamak_operation , basamak_operation_parametric_objects)   
            self.cmb_basamak_operation.SetSelection(0)
        if data_tabanca.ID_BASAMAK_KAYDIRMA != -1:
            index_bas_kaydirma = basamak_operation_parametric_objects.index(filter(lambda x:  x.id==data_tabanca.ID_BASAMAK_KAYDIRMA,basamak_operation_parametric_objects)[0])
            self.cmb_basamak_operation.SetSelection(index_bas_kaydirma)
        self.cmb_basamak_operation.Disable()
        
        self.choices_tabanca_no = []
        self.lbl_tabanca_no       = wx.StaticText(self.panel_add_tabanca, label="Tabanca No :  ")
        s.setStaticTextList( self.lbl_tabanca_no )
        self.lbl_tabanca_no.Disable()
        self.cmb_tabanca_no  = wx.ComboBox  (self.panel_add_tabanca, 1,choices = self.choices_tabanca_no ,style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY))
        self.cmb_tabanca_no.Disable()
        if data_tabanca.TABANCA_NO:
            self.cmb_tabanca_no.SetValue(str(data_tabanca.TABANCA_NO))
        self.lbl_tank_adi       = wx.StaticText(self.panel_add_tabanca, label="Yak�t Tank� :  ")
        s.setStaticTextList( self.lbl_tank_adi )
        self.lbl_tank_adi.Disable()
        
        self.cmb_tank_adi  = wx.ComboBox(self.panel_add_tabanca,1,"", choices=[],
                                            style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY))

        tank_adi_parametric_objects = []
        tank_adi_parametric_objects = self.db.get_parameters_all("YAKIT_TANKI",where="ID_ACTIVE=1",order="YAKIT_TANKI_ADI",selects="ID_YAKIT_TANKI,YAKIT_TANKI_ADI")
        if tank_adi_parametric_objects != []:
            self.db.widgetMaker( self.cmb_tank_adi , tank_adi_parametric_objects)  
        if data_tabanca.ID_YAKIT_TANKI != -1:
            index_yakit_tanki = tank_adi_parametric_objects.index(filter(lambda x:  x.id==data_tabanca.ID_YAKIT_TANKI,tank_adi_parametric_objects)[0])
            self.cmb_tank_adi.SetSelection(index_yakit_tanki)
        self.cmb_tank_adi.Disable()

        if data_tabanca.ID_POMPA != -1:
            self.OnPompaChanged( wx.EVT_COMBOBOX)
        if data_tabanca.TABANCA_NO:
            tabanca_index = self.choices_tabanca_no.index(data_tabanca.TABANCA_NO)
            self.cmb_tabanca_no.SetSelection(tabanca_index)
        if data_tabanca.BEYIN_NO:
            beyin_index = self.choices_beyin_no.index(data_tabanca.BEYIN_NO)
            self.cmb_beyin_no.SetSelection(beyin_index)
        if data_tabanca.POMPA_NO:
            choice_new_pompa_dizi = []
            for choice in self.choices_pompa_no:
                choice_ = int(choice.split('-')[1].strip())
                choice_new_pompa_dizi.append(choice_)
            pompa_index = choice_new_pompa_dizi.index(data_tabanca.POMPA_NO)
            self.cmb_pompa_no.SetSelection(pompa_index)
        
        self.btn_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_save  = LibButtons.ThemedGenBitmapTextButton(self.panel_add_tabanca, id=wx.NewId(), bitmap=image_save,label="Verileri Kaydet")
        self.btn_save.Bind(wx.EVT_BUTTON,lambda event: self.OnBeyinSave(event,selected_item))
        self.btn_save.SetToolTipString("Verileri Kaydet")
        self.btn_save.SetBitmapSelected(self.btn_save_pressed)
        s.setButtonFont( self.btn_save )
       
        self.gbs_add_tabanca.Add( lbl_pompa_adi, (1,1),(1,1),  wx.ALL,1)
        self.gbs_add_tabanca.Add( self.cmb_pompa_adi, (1,2) ,(1,1),  wx.ALL,1)
        
        self.gbs_add_tabanca.Add( self.lbl_pompa_no, (1,3),(1,1),  wx.ALL,1)
        self.gbs_add_tabanca.Add( self.cmb_pompa_no, (1,4) ,(1,1),  wx.ALL,1)
        
        self.gbs_add_tabanca.Add( self.lbl_beyin_no, (2,1),(1,1),  wx.ALL,1)
        self.gbs_add_tabanca.Add( self.cmb_beyin_no, (2,2) ,(1,1),  wx.ALL,1)
        
        self.gbs_add_tabanca.Add( self.lbl_basamak_operation, (2,3),(1,1),  wx.ALL,1)
        self.gbs_add_tabanca.Add( self.cmb_basamak_operation, (2,4) ,(1,2),  wx.ALL,1)

        self.gbs_add_tabanca.Add( self.lbl_tabanca_no, (3,1),(1,1),  wx.ALL,1)
        self.gbs_add_tabanca.Add( self.cmb_tabanca_no, (3,2) ,(1,1),  wx.ALL,1)
        
        self.gbs_add_tabanca.Add( self.lbl_tank_adi, (3,3),(1,1),  wx.ALL,1)
        self.gbs_add_tabanca.Add( self.cmb_tank_adi, (3,4) ,(1,1),  wx.ALL,1)
    
        self.gbs_add_tabanca.Add(self.btn_save,  (5,1),(1,2),wx.ALL,1)        
     
        self.panel_add_tabanca.SetSizerAndFit(self.gbs_add_tabanca)
   
        return self.panel_add_tabanca
    
    def createAddKartOkuyucu(self):
        if self.selected_item.has_key(3):
            selected_item                   = self.selected_item[3]
        else:
            selected_item = None
        self.dummy.ID_POMPA             = -1
        self.dummy.POMPA_NO             = ''
        self.dummy.KART_OKUYUCU_MARKA   = ''
        self.dummy.ID_PORT              = -1
        self.dummy.KART_OKUYUCU_ADRES   = ''
        data_kart_okuyucu               = self.dummy

        if selected_item :
            data_kart_okuyucu = self.db.get_raw_data_all("KART_OKUYUCU",selects="ID_KART_OKUYUCU,ID_POMPA,POMPA_NO,KART_OKUYUCU_MARKA,ID_PORT,KART_OKUYUCU_ADRES",where="ID_KART_OKUYUCU=%s"%selected_item,dict_mi='object')[0]

        self.panel_add_kart_okuyucu = wx.Panel(self.book,1, style = wx.TAB_TRAVERSAL
             | wx.CLIP_CHILDREN
             | wx.FULL_REPAINT_ON_RESIZE
             |wx.EXPAND)
        
        self.gbs_add_kart_okuyucu =  wx.GridBagSizer(5,5)

        lbl_kart_okuyucu_pompa_adi       = wx.StaticText(self.panel_add_kart_okuyucu, label="Pompa Ad� :  ")
        s.setStaticTextList( lbl_kart_okuyucu_pompa_adi )
        self.cmb_kart_okuyucu_pompa_adi  = wx.ComboBox(self.panel_add_kart_okuyucu,1,"", choices=[],style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY)) 
        kart_okuyucu_pompa_adi_parametric_objects = []
        kart_okuyucu_pompa_adi_parametric_objects = self.db.get_parameters_all("POMPA",order="POMPA_ADI",selects="ID_POMPA,POMPA_ADI")
        self.cmb_kart_okuyucu_pompa_adi.Bind( wx.EVT_COMBOBOX , self.OnPompaChangedKartOkuyucu )
        if kart_okuyucu_pompa_adi_parametric_objects != [] :
            self.db.widgetMaker( self.cmb_kart_okuyucu_pompa_adi , kart_okuyucu_pompa_adi_parametric_objects)
        
        if data_kart_okuyucu.ID_POMPA != -1:
            index_ = kart_okuyucu_pompa_adi_parametric_objects.index(filter(lambda x:  x.id==data_kart_okuyucu.ID_POMPA,kart_okuyucu_pompa_adi_parametric_objects)[0])
            self.cmb_kart_okuyucu_pompa_adi.SetSelection(index_)
            
        self.choices_pompa_no_kart_okuyucu = []
        self.lbl_pompa_no_kart_okuyucu       = wx.StaticText(self.panel_add_kart_okuyucu, label="Pompa No :  ")
        s.setStaticTextList( self.lbl_pompa_no_kart_okuyucu )
        self.lbl_pompa_no_kart_okuyucu.Disable()
        self.cmb_pompa_no_kart_okuyucu  = wx.ComboBox  (self.panel_add_kart_okuyucu, 1,choices = self.choices_pompa_no_kart_okuyucu ,style=(wx.CB_DROPDOWN|wx.CB_READONLY))
        self.cmb_pompa_no_kart_okuyucu.Disable()
        
        self.lbl_kart_okuyucu_marka       = wx.StaticText(self.panel_add_kart_okuyucu, label="Kart Okuyucu Marka :  ")
        s.setStaticTextList( self.lbl_kart_okuyucu_marka )
        self.lbl_kart_okuyucu_marka.Disable()
        
	choices_kart_okuyucu_marka =  dict((name, getattr(sabitler.KART_OKUYUCU_MARKA.MARKALAR, name)) for name in dir(sabitler.KART_OKUYUCU_MARKA.MARKALAR) if not name.startswith('__'))
        self.cmb_kart_okuyucu_marka  = wx.ComboBox  (self.panel_add_kart_okuyucu, 1,choices = [] ,style=(wx.CB_DROPDOWN|wx.CB_READONLY))
        self.db.widgetMakerDict( self.cmb_kart_okuyucu_marka , choices_kart_okuyucu_marka) 
        self.cmb_kart_okuyucu_marka.Disable()
        
	
        self.lbl_kart_okuyucu_port_adi       = wx.StaticText(self.panel_add_kart_okuyucu, label="Kart Okuyucu Portu :  ")
        s.setStaticTextList( self.lbl_kart_okuyucu_port_adi )
        self.lbl_kart_okuyucu_port_adi.Disable()
        self.cmb_kart_okuyucu_port_adi  =  wx.ComboBox(self.panel_add_kart_okuyucu,1,"", choices=[],
                                            style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY))
        kart_okuyucu_port_adi_parametric_objects = []
        kart_okuyucu_port_adi_parametric_objects = self.db.get_parameters_all("PORTLAR",order="PORT_ADI",selects="ID_PORT,PORT_ADI")
        if kart_okuyucu_port_adi_parametric_objects != []:
            self.db.widgetMaker( self.cmb_kart_okuyucu_port_adi , kart_okuyucu_port_adi_parametric_objects)

        self.lbl_kart_okuyucu_adres       = wx.StaticText(self.panel_add_kart_okuyucu, label="Kart Okuyucu Adres :  ")
        s.setStaticTextList( self.lbl_kart_okuyucu_adres )
        self.lbl_kart_okuyucu_adres.Disable()
        self.txt_kart_okuyucu_adres  =  wx.TextCtrl  (self.panel_add_kart_okuyucu, 1, str(data_kart_okuyucu.KART_OKUYUCU_ADRES),size=(100,-1)) 
        self.txt_kart_okuyucu_adres.Disable()
        
        if data_kart_okuyucu.KART_OKUYUCU_MARKA:
            kart_okuyucu_marka_index = choices_kart_okuyucu_marka.values().index(data_kart_okuyucu.KART_OKUYUCU_MARKA)
            self.cmb_kart_okuyucu_marka.SetSelection(kart_okuyucu_marka_index)
            
        if data_kart_okuyucu.KART_OKUYUCU_ADRES:           
            self.txt_kart_okuyucu_adres.SetValue(str(data_kart_okuyucu.KART_OKUYUCU_ADRES))

        if data_kart_okuyucu.ID_PORT :
            if data_kart_okuyucu.ID_PORT != -1:
                index_kart_okuyucu_port_adi = kart_okuyucu_port_adi_parametric_objects.index(filter(lambda x:  x.id==data_kart_okuyucu.ID_PORT,kart_okuyucu_port_adi_parametric_objects)[0])
                self.cmb_kart_okuyucu_port_adi.SetSelection(index_kart_okuyucu_port_adi)
        self.cmb_kart_okuyucu_port_adi.Disable()
       
        if data_kart_okuyucu.ID_POMPA != -1:
            self.OnPompaChangedKartOkuyucu( wx.EVT_COMBOBOX)
 
        if data_kart_okuyucu.POMPA_NO:
            choice_kart_okuyucu_new_pompa_dizi = []
            for choice in self.choices_pompa_no_kart_okuyucu:
                choice_ = int(choice.split('-')[1].strip())
                choice_kart_okuyucu_new_pompa_dizi.append(choice_)
            pompa_index = choice_kart_okuyucu_new_pompa_dizi.index(data_kart_okuyucu.POMPA_NO)
            self.cmb_pompa_no_kart_okuyucu.SetSelection(pompa_index)
        
        
        self.btn_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_save  = LibButtons.ThemedGenBitmapTextButton(self.panel_add_kart_okuyucu, id=wx.NewId(), bitmap=image_save,label="Verileri Kaydet")
        self.btn_save.Bind(wx.EVT_BUTTON,lambda event: self.OnKartOkuyucuSave(event,selected_item))
        self.btn_save.SetToolTipString("Verileri Kaydet")
        self.btn_save.SetBitmapSelected(self.btn_save_pressed)
        s.setButtonFont( self.btn_save )
       
        self.gbs_add_kart_okuyucu.Add( lbl_kart_okuyucu_pompa_adi, (1,1),(1,1),  wx.ALL,1)
        self.gbs_add_kart_okuyucu.Add( self.cmb_kart_okuyucu_pompa_adi, (1,2) ,(1,1),  wx.ALL,1)
        
        self.gbs_add_kart_okuyucu.Add( self.lbl_pompa_no_kart_okuyucu, (1,3),(1,1),  wx.ALL,1)
        self.gbs_add_kart_okuyucu.Add( self.cmb_pompa_no_kart_okuyucu, (1,4) ,(1,1),  wx.ALL,1)
        
        self.gbs_add_kart_okuyucu.Add( self.lbl_kart_okuyucu_marka, (2,1),(1,1),  wx.ALL,1)
        self.gbs_add_kart_okuyucu.Add( self.cmb_kart_okuyucu_marka, (2,2) ,(1,1),  wx.ALL,1)
        
        self.gbs_add_kart_okuyucu.Add( self.lbl_kart_okuyucu_port_adi, (2,3),(1,1),  wx.ALL,1)
        self.gbs_add_kart_okuyucu.Add( self.cmb_kart_okuyucu_port_adi, (2,4) ,(1,2),  wx.ALL,1)

        self.gbs_add_kart_okuyucu.Add( self.lbl_kart_okuyucu_adres, (3,1),(1,1),  wx.ALL,1)
        self.gbs_add_kart_okuyucu.Add( self.txt_kart_okuyucu_adres, (3,2) ,(1,1),  wx.ALL,1)
        
        
        self.gbs_add_kart_okuyucu.Add(self.btn_save,  (4,1),(1,2),wx.ALL,1)        
     
        self.panel_add_kart_okuyucu.SetSizerAndFit(self.gbs_add_kart_okuyucu)
        
        return self.panel_add_kart_okuyucu
        
    def createKartOkuyucuList(self):
        headers        = ['ID,digit','Kart Okuyucu Marka,string','Ada,string','Ada Y�z,string','Kart Okuyucu Port Ad�,string','Kart Okuyucu Adres,string']
        data_history   = self.db.get_raw_data_all("VIEW_POMPA_TABANCALAR",order = "ID_KART_OKUYUCU",
                         selects="ID_KART_OKUYUCU,KART_OKUYUCU_MARKA,POMPA_ADI,POMPA_NO,KART_OKUYUCU_PORT,KART_OKUYUCU_ADRES",where =" KART_OKUYUCU_ADRES is not null ",group_by = "ID_KART_OKUYUCU")  
        data_history_   = map(list,data_history )
        portlar        = self.db.get_raw_data_all("PORTLAR",selects="ID_PORT,PORT_ADI",dict_mi = 'object')
        for data in data_history_:

            if data[1]:
                dict_markalar = sabitler.KART_OKUYUCU_MARKA.MARKALAR.__dict__
                data[1] = dict_markalar.keys()[dict_markalar.values().index(data[1])]
                
            if data[4]:
                kart_okuyucu_port_adi_ =  filter(lambda x:  x.ID_PORT==data[4],portlar)[0].PORT_ADI
                data[4] = kart_okuyucu_port_adi_

        list_pane = IndexListPanel( self.book, data = data_history_, headers= headers,table_name='KART_OKUYUCU',where='ID_KART_OKUYUCU',delete_button=True,guncelle_button=True,guncellemede_gidilecek_sayfa=3 )   
        return list_pane

    def createPompaList(self):

        refresh_dict     = dict(book                = self.book,
                                temp_page           = 3,
                                create_panel_method = self.createPompaList,
                                cur_page            = 2,
                                label               = 'Kart Okuyucu Tan�mlama',
                                image               = self.img4)
        child_parameters = ['VIEW_POMPA_TABANCALAR','ID_POMPA','ID_POMPA,ID_TABANCA,BEYIN_NO,TABANCA_NO,POMPA_NO,ARABIRIM_MARKASI,ARABIRIM_ADRESI,ARABIRIM_PORT_ADI,YAKIT_TANKI_ADI,BASAMAK_KAYDIRMA_ADI']
        headers_model    = ['POMPA ADI,string','BEYIN NO,digit','TABANCA NO,digit','POMPA NO,digit','ARA BIRIM MARKASI,string','ARABIRIM ADRESI,digit','ARABIRIM PORT ADI,string','YAKIT TANKI ADI,string','BASAMAK KAYDIRMA ADI,string']
        
        parent_childs  = self.db.get_raw_data_all(table_name = 'POMPA',order="POMPA_ADI",selects="POMPA_ADI,ID_POMPA",dict_mi='parent',child_parameters =child_parameters,data_types=map(lambda x:x.split(',')[1], headers_model) )
        mod            = model.TreeListModel(parent_childs,headers_model,child_parameters)
        tree_list_pane = TreeListPanel( self.book, mod,guncelle_button=True,delete_button=True,child_guncellemede_gidilecek_sayfa=1,parent_guncellemede_gidilecek_sayfa=0,child_table_name='TABANCALAR',parent_table_name='POMPA',
                                       parent_delete_where='ID_POMPA',child_delete_where='ID_TABANCA',refresh_dict = refresh_dict)
        
        return tree_list_pane
    
    def OnPompaSave(self,event,selected_item=None ):
        user=self.root.user
        message                      = ''
        pompa_adi                    = self.txt_pompa_adi.GetValue().strip()
        pompa_no_on                  = self.txt_pompa_no_on.GetValue().strip()
        pompa_no_arka                = self.txt_pompa_no_arka.GetValue().strip()
        arabirim_adres               = self.txt_arabirim_adres.GetValue().strip()
        arabirimdeki_beyin_sayisi    = self.txt_beyin_sayisi.GetValue().strip()
        arabirimdeki_tabanca_sayisi  = self.txt_tabanca_sayisi.GetValue().strip()
        port_tipi                    = self.txt_port_tipi.GetValue()
        if pompa_adi == '' :
            message+= 'L�tfen Pompa Ad�n� Giriniz.\n'

        if pompa_no_on == ''  :
            message += 'L�tfen Pompa No(�n) ��in  De�er Giriniz.\n'
        
        if pompa_no_arka == '':
            pompa_no_arka = 'null'
        
        if self.cmb_arabirim_marka.GetSelection() == -1 :
            message += 'L�tfen Arabirim Markas�n� Se�iniz.\n'
        else:
            arabirim_marka_id = self.cmb_arabirim_marka.GetClientData(self.cmb_arabirim_marka.GetSelection()).id
            
        if arabirim_adres == '' or arabirim_adres < 1 :
            message += 'L�tfen Arabirim Adresi ��in Girdi�iniz De�eri Kontrol Ediniz.\n'
        else:
            self.txt_arabirim_adres.SetValue(arabirim_adres)
        #print self.cmb_add_pompa_port_adi.GetSelection()
        if self.cmb_add_pompa_port_adi.GetSelection() == -1 :
            message +='L�tfen Port Ad�n� Se�iniz.\n'
        else:
            port_id = self.cmb_add_pompa_port_adi.GetClientData(self.cmb_add_pompa_port_adi.GetSelection()).id
            
        if arabirimdeki_beyin_sayisi == '':
            message +='L�tfen Arabirimdeki Toplam Beyin Say�s�n� Giriniz.\n'
            
        if arabirimdeki_tabanca_sayisi == '':
            message +='L�tfen Arabirimdeki Toplam Tabanca Say�s�n� Giriniz.'
        if message == '':
            self.txt_pompa_adi.SetValue(pompa_adi)
            self.table_name_update_pompa = "POMPA"
            self.value_update_pompa       = "'%s',%s,%s,%s,%s,'%s',%s,%s,%s"%(pompa_adi,pompa_no_on,pompa_no_arka,arabirim_marka_id,arabirim_adres,port_tipi,port_id,arabirimdeki_beyin_sayisi,arabirimdeki_tabanca_sayisi)
            where_check   = "UPPER (POMPA_ADI) like UPPER('" + pompa_adi+"')"
            selects = "ID_POMPA,POMPA_ADI"
            self.column_name_update_pompa = "POMPA_ADI,POMPA_NO_ON,POMPA_NO_ARKA,ARABIRIM_MARKASI,ARABIRIM_ADRESI,PORT_TIPI,ID_PORT,ARABIRIMDEKI_BEYIN_SAYISI,ARABIRIMDEKI_TABANCA_SAYISI"
            var_pompa = self.db.get_raw_data_all( self.table_name_update_pompa,where_check,selects,dict_mi='object' )
            ### selected_item varsa tabanca listesinden guncelle butonuyla bu sayfaya gelinmis demektir. Yani update yapilmak isteniyor###               
            if selected_item :
                self.selected_item_update = selected_item
                pompa_adi = var_pompa[0].POMPA_ADI
                s.ok_cancel_message(self,"%s �simli Pompan�n Verilerini G�ncellemek �stedi�inize Emin misiniz?"%pompa_adi,
                    "Pompa Tan�mlama",ok_function = self.pompaGuncelle,cancel_function=self.pompaGuncelleme)
            else:           
                if var_pompa == []:
                    var_pompa_yon = self.db.get_raw_data_all("POMPA",selects="POMPA_NO_ON,POMPA_NO_ARKA",where="POMPA_NO_ON=%s OR POMPA_NO_ON=%s OR POMPA_NO_ARKA=%s OR POMPA_NO_ARKA=%s"%(pompa_no_on,pompa_no_arka,pompa_no_on,pompa_no_arka))
                    if var_pompa_yon == []:
                        ##veritabaninda yokmus o zaman veritabanina kaydedelim
                        data_id = self.db.save_data_and_get_id ( self.table_name_update_pompa ,self.column_name_update_pompa, self.value_update_pompa,user=user )
                        
                        if data_id !='' :                    
                            s.uyari_message(self,"Eklemek �stedi�iniz Pompa Sisteme Ba�ar�yla Kaydedildi.","Pompa Tan�mlama")
                        else:
                            s.uyari_message(self,"Bir Hata Olu�tu. Eklemek �stedi�iniz Pompa Sisteme Kaydedilemedi.\nL�tfen Sistem Y�neticinizle G�r���n�z.","Pompa Tan�mlama")
                    else:
                        s.uyari_message(self.panel_add_pompa,"Eklemek �stedi�iniz Pompa No(Y�n) Sistemde Mevcuttur.","Pompa Tan�mlama")    
                else :
                    s.uyari_message(self.panel_add_pompa,"Eklemek �stedi�iniz Pompa Ad� Sistemde Mevcuttur.","Pompa Tan�mlama")
        else:
            s.uyari_message(self.panel_add_pompa,message,"Pompa Tan�mlama")
            
    def OnBeyinSave(self,event,selected_item=None):
        user=self.root.user
        if self.cmb_pompa_adi.GetSelection()==-1:
            s.uyari_message(self.panel_add_pompa,'L�tfen Pompa Ad�n� Se�iniz.',"Pompa Tan�mlama")
        else:
            message                      = ''
            pompa_id                     = self.cmb_pompa_adi.GetClientData(self.cmb_pompa_adi.GetSelection()).id
            basamak_operation            = self.cmb_basamak_operation.GetClientData(self.cmb_basamak_operation.GetSelection()).id         
            '''
            if self.txt_modul_no.GetValue():
                modul_no = self.txt_modul_no.GetValue()
            else:
                modul_no = 'null'
               
            if self.cmb_port_adi.GetSelection() != -1:
                port_id = self.cmb_port_adi.GetClientData(self.cmb_port_adi.GetSelection()).id
            else:
                port_id = 'null'
            '''    
            if self.cmb_pompa_no.GetSelection() == -1  :
                message += 'L�tfen Pompa No Se�iniz.\n'
            else:
                pompa_no = self.cmb_pompa_no.GetValue().split('-')[1].strip()
            
            if self.cmb_beyin_no.GetSelection() == -1 :
                message += 'L�tfen Beyin Numaras�n� Se�iniz.\n'
            else:
                beyin_no = self.cmb_beyin_no.GetValue()
                
            if self.cmb_tabanca_no.GetSelection() == -1 :
                message += 'L�tfen Tabanca Numaras�n� Se�iniz.\n'
            else:
                tabanca_no = self.cmb_tabanca_no.GetValue()
                
            if self.cmb_tank_adi.GetSelection() == -1:
                message += 'L�tfen Yak�t Tank�n� Se�iniz.\n'
            else:
                yakit_tanki_id = self.cmb_tank_adi.GetClientData(self.cmb_tank_adi.GetSelection()).id
                
            if message == '':
                self.table_name_update_tabanca = "TABANCALAR"
                self.value_update_tabanca       = "%s,%s,%s,%s,%s,%s"%(pompa_id,pompa_no,beyin_no,basamak_operation,tabanca_no,yakit_tanki_id)#,port_id,modul_no)
                self.where_check_tabanca   = "ID_POMPA=%s AND BEYIN_NO=%s AND TABANCA_NO=%s"%(pompa_id,beyin_no,tabanca_no)
                selects = "ID_TABANCA"
                self.column_name_update_tabanca = "ID_POMPA,POMPA_NO,BEYIN_NO,ID_BASAMAK_KAYDIRMA,TABANCA_NO,ID_YAKIT_TANKI"#,ID_PORT,MODUL_NO"
                ### selected_item varsa tabanca listesinden guncelle butonuyla bu sayfaya gelinmis demektir. Yani update yapilmak isteniyor###
                
                if selected_item :
                    self.selected_item_update = selected_item
                    pompa_adi = self.cmb_pompa_adi.GetValue()
                    s.ok_cancel_message(self,"%s �simli Pompan�n %s Numaral� Tabancan�n Verilerini G�ncellemek �stedi�inize Emin misiniz?"%(pompa_adi,tabanca_no),
                        "Pompa Tan�mlama",ok_function = self.tabancaGuncelle,cancel_function=self.tabancaGuncelleme)
                else:
                    var_tabanca = self.db.get_raw_data_all( self.table_name_update_tabanca,self.where_check_tabanca,selects )
                    if var_tabanca == []:
                        
                        ##veritabaninda yokmus o zaman veritabanina kaydedelim
                        data_id = self.db.save_data_and_get_id ( self.table_name_update_tabanca ,self.column_name_update_tabanca, self.value_update_tabanca ,user=user)
                        if data_id !='' :                    
                            s.uyari_message(self,"Eklemek �stedi�iniz Tabanca Sisteme Ba�ar�yla Kaydedildi.","Pompa Tan�mlama")
                           
                            ##uyaridan sonra iskontaya eklenecek yakit turleri ekleme ekranina gecelim
                        else:
                            s.uyari_message(self,"Bir Hata Olu�tu. Eklemek �stedi�iniz Tabanca Sisteme Kaydedilemedi.\nL�tfen Sistem Y�neticinizle G�r���n�z.","Pompa Tan�mlama")
                            
                    else :
                        
                        s.uyari_message(self.panel_add_pompa,"Eklemek �stedi�iniz Tabanca Sistemde Mevcuttur.","Pompa Tan�mlama")
            else:
                s.uyari_message(self.panel_add_pompa,message,"Pompa Tan�mlama")
                
    def OnKartOkuyucuSave(self,event,selected_item=None ):
        user = self.root.user
        if self.cmb_kart_okuyucu_pompa_adi.GetSelection()==-1:
            s.uyari_message(self.panel_add_kart_okuyucu,'L�tfen Pompa Ad�n� Se�iniz.',"Kart Okuyucu Tan�mlama")
        else:
            message   = ''
            pompa_id  = self.cmb_kart_okuyucu_pompa_adi.GetClientData(self.cmb_kart_okuyucu_pompa_adi.GetSelection()).id
            if self.cmb_pompa_no_kart_okuyucu.GetSelection() == -1  :
                message += 'L�tfen Pompa No Se�iniz.\n'
            else:
                pompa_no = self.cmb_pompa_no_kart_okuyucu.GetValue().split('-')[1].strip()
            if self.cmb_kart_okuyucu_marka.GetSelection() == -1  :
                message += 'L�tfen Kart Okuyucu Markas�n� Se�iniz.\n'
            else:
                kart_okuyucu_marka = self.cmb_kart_okuyucu_marka.GetClientData(self.cmb_kart_okuyucu_marka.GetSelection()).id
            if self.cmb_kart_okuyucu_port_adi.GetSelection() == -1:
                message += 'L�tfen Kart Okuyucu Portunu Se�iniz.\n'
            else:
                port_id = self.cmb_kart_okuyucu_port_adi.GetClientData(self.cmb_kart_okuyucu_port_adi.GetSelection()).id
            if self.txt_kart_okuyucu_adres.GetValue().strip() == '':
                message += 'L�tfen Kart Okuyucu Adresini Giriniz.'
            else:
                kart_okuyucu_adres = self.txt_kart_okuyucu_adres.GetValue().strip()
            
            if message == '':
                self.table_name_update_kart_okuyucu = "KART_OKUYUCU"
                self.value_update_kart_okuyucu  = "%s,%s,%s,%s,%s"%(pompa_id,pompa_no,kart_okuyucu_marka,port_id,kart_okuyucu_adres)#,port_id,modul_no)
                self.where_check_kart_okuyucu   = "ID_POMPA=%s AND POMPA_NO=%s"%(pompa_id,pompa_no)
                selects = "ID_KART_OKUYUCU"
                self.column_name_update_kart_okuyucu = "ID_POMPA,POMPA_NO,KART_OKUYUCU_MARKA,ID_PORT,KART_OKUYUCU_ADRES"#,ID_YAKIT_TANKI",ID_PORT,MODUL_NO"
                ### selected_item varsa tabanca listesinden guncelle butonuyla bu sayfaya gelinmis demektir. Yani update yapilmak isteniyor###
                
                if selected_item :
                    self.selected_item_update = selected_item
                    pompa_adi = self.cmb_kart_okuyucu_pompa_adi.GetValue()
                    s.ok_cancel_message(self,"%s �simli Adan�n %s Nolu Pompan�n Kart Okuyucu Verilerini G�ncellemek �stedi�inize Emin misiniz?"%(pompa_adi,kart_okuyucu_adres),
                        "Kart Okuyucu Tan�mlama",ok_function = self.kartOkuyucuGuncelle,cancel_function=self.kartOkuyucuGuncelleme)
                else:
                    var_kart_okuyucu = self.db.get_raw_data_all( self.table_name_update_kart_okuyucu,self.where_check_kart_okuyucu,selects )
                    if var_kart_okuyucu == []:
                        data_id = self.db.save_data_and_get_id ( self.table_name_update_kart_okuyucu ,self.column_name_update_kart_okuyucu, self.value_update_kart_okuyucu ,user=user)
                        if data_id !='' :                    
                            s.uyari_message(self,"Eklemek �stedi�iniz Kart Okuyucu Sisteme Ba�ar�yla Kaydedildi.","Kart Okuyucu Tan�mlama")
                        else:
                            s.uyari_message(self,"Bir Hata Olu�tu. Eklemek �stedi�iniz Kart Okuyucu Sisteme Kaydedilemedi.\nL�tfen Sistem Y�neticinizle G�r���n�z.","Kart Okuyucu Tan�mlama")
                            
                    else :
                        
                        s.uyari_message(self.panel_add_kart_okuyucu,"Eklemek �stedi�iniz Adada Kart Okuyucu Sistemde Mevcuttur.","Kart Okuyucu Tan�mlama")
            else:
                s.uyari_message(self.panel_add_kart_okuyucu,message,"Kart Okuyucu Tan�mlama")
            
    '''
    def onCheckedKartOkuyucu (self,event):
        
        if self.cb_kart_okuyucu.IsChecked():
            self.lbl_modul_no.Enable(True)
            self.txt_modul_no.Enable(True)     
            self.lbl_port_adi.Enable(True)
            self.cmb_port_adi.Enable(True)
        else:
            self.lbl_modul_no.Disable()
            self.txt_modul_no.Disable()     
            self.lbl_port_adi.Disable()
            self.cmb_port_adi.Disable()
            self.cmb_port_adi.SetSelection(-1)
    '''
    def OnPompaChanged(self, event):
        
        self.choices_pompa_no   = []
        self.choices_beyin_no   = []
        self.choices_tabanca_no = []
        if self.cmb_pompa_adi.GetSelection() != -1 :
            pompa_id = self.cmb_pompa_adi.GetClientData(self.cmb_pompa_adi.GetSelection()).id
            data = self.db.get_raw_data_all(table_name = "POMPA",where="ID_POMPA=%s"%pompa_id,selects="POMPA_NO_ON,POMPA_NO_ARKA,ARABIRIMDEKI_BEYIN_SAYISI,ARABIRIMDEKI_TABANCA_SAYISI")
            if data != []:
                if data[0][0]:                    
                    self.choices_pompa_no.append( "Pompa �n - "+str(data[0][0]))
                if data[0][1]:                    
                    self.choices_pompa_no.append( "Pompa Arka - "+str(data[0][1]))
                self.db.widgetMakerStr( self.cmb_pompa_no , self.choices_pompa_no) 
                if data[0][2]:                    
                    for i in range (data[0][2]):
                        self.choices_beyin_no.append(i+1)
                    self.db.widgetMakerStr( self.cmb_beyin_no , self.choices_beyin_no)
                if data[0][3]:                    
                    for i in range ((data[0][3])/data[0][2]):
                        self.choices_tabanca_no.append(i+1)
                    self.db.widgetMakerStr( self.cmb_tabanca_no , self.choices_tabanca_no)
                    
                self.cmb_pompa_no.Enable(True)
                self.lbl_pompa_no.Enable(True)
                self.cmb_beyin_no.Enable(True)
                self.lbl_beyin_no.Enable(True)
                self.cmb_basamak_operation.Enable(True)
                self.lbl_basamak_operation.Enable(True)
                self.cmb_tabanca_no.Enable(True)
                self.lbl_tabanca_no.Enable(True)
                self.cmb_tank_adi.Enable(True)
                self.lbl_tank_adi.Enable(True)
                #self.cb_kart_okuyucu.Enable(True)
                self.Refresh()
        
    def OnPompaChangedKartOkuyucu(self, event):
        
        self.choices_pompa_no_kart_okuyucu   = []
        if self.cmb_kart_okuyucu_pompa_adi.GetSelection() != -1 :
            pompa_id = self.cmb_kart_okuyucu_pompa_adi.GetClientData(self.cmb_kart_okuyucu_pompa_adi.GetSelection()).id
            data = self.db.get_raw_data_all(table_name = "POMPA",where="ID_POMPA=%s"%pompa_id,selects="POMPA_NO_ON,POMPA_NO_ARKA,ARABIRIMDEKI_BEYIN_SAYISI,ARABIRIMDEKI_TABANCA_SAYISI")
            if data != []:
                if data[0][0]:                    
                    self.choices_pompa_no_kart_okuyucu.append( "Pompa �n - "+str(data[0][0]))
                if data[0][1]:                    
                    self.choices_pompa_no_kart_okuyucu.append( "Pompa Arka - "+str(data[0][1]))
                self.db.widgetMakerStr( self.cmb_pompa_no_kart_okuyucu , self.choices_pompa_no_kart_okuyucu) 

                self.cmb_pompa_no_kart_okuyucu.Enable(True)
                self.lbl_pompa_no_kart_okuyucu.Enable(True)
                self.cmb_kart_okuyucu_port_adi.Enable(True)
                self.lbl_kart_okuyucu_port_adi.Enable(True)
                self.cmb_kart_okuyucu_marka.Enable(True)
                self.lbl_kart_okuyucu_marka.Enable(True)
                self.txt_kart_okuyucu_adres.Enable(True)
                self.lbl_kart_okuyucu_adres.Enable(True)

                self.Refresh()
    def OnBookPageChanged( self, event ):
        
        if self.book.GetSelection() == 0:
            
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.book.Freeze()
            self.book.SetSelection(1)
            p_ = self.createAddPompa()
            self.book.RemovePage(0)
            self.book.InsertPage( 0,p_,"Pompa Tan�mlama")
            self.book.SetPageImage(0, self.img0)
            self.book.SetSelection(0)
            self.book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.book.Thaw()
        
        elif self.book.GetSelection() == 1:
            
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.book.Freeze()
            self.book.SetSelection(0)
            p_ = self.createAddTabanca()
            self.book.RemovePage(1)
            self.book.InsertPage( 1,p_,"Tabanca Tan�mlama")
            self.book.SetPageImage(1, self.img1)
            self.book.SetSelection(1)
            self.book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.book.Thaw()
            
        elif self.book.GetSelection() == 2:
            
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.book.Freeze()
            self.book.SetSelection(0)
            p_ = self.createPompaList()
            self.book.RemovePage(2)
            self.book.InsertPage( 2,p_,"Pompa Listesi")
            self.book.SetPageImage(2, self.img2)
            self.book.SetSelection(2)
            self.book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.book.Thaw()

        elif self.book.GetSelection() == 3:
            
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.book.Freeze()
            self.book.SetSelection(0)
            p_ = self.createAddKartOkuyucu()
            self.book.RemovePage(3)
            self.book.InsertPage( 3,p_,"Kart Okuyucu Tan�mlama")
            self.book.SetPageImage(3, self.img3)
            self.book.SetSelection(3)
            self.book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.book.Thaw()
            
        elif self.book.GetSelection() == 4:
            
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.book.Freeze()
            self.book.SetSelection(0)
            p_ = self.createKartOkuyucuList()
            self.book.RemovePage(4)
            self.book.InsertPage( 4,p_,"Kart Okuyucu Listesi")
            self.book.SetPageImage(4, self.img4)
            self.book.SetSelection(4)
            self.book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.book.Thaw()

    def pompaGuncelle(self):
        user=self.root.user
        update = self.db.update_data(table_name=self.table_name_update_pompa,column_name=self.column_name_update_pompa,values=self.value_update_pompa,where='ID_POMPA=%s'%self.selected_item_update,user=user)
        
        if update == 1:
             s.uyari_message(self,"G�ncellemek �stedi�iniz Tabanca Verileri Ba�ar�yla Kaydedildi.","Pompa Tan�mlama")  
        else:
            s.uyari_message(self,"Bir Hata Olu�tu. G�ncellemek �stedi�iniz Tabanca Verileri Kaydedilemedi.\nL�tfen Sistem Y�neticinizle G�r���n�z.","Pompa Tan�mlama") 

    def pompaGuncelleme(self):
        pass
    
    def tabancaGuncelle(self):
        user=self.root.user
        update = self.db.update_data(table_name=self.table_name_update_tabanca,column_name=self.column_name_update_tabanca,values=self.value_update_tabanca,where='ID_TABANCA=%s'%self.selected_item_update,user=user)
        
        if update == 1:
             s.uyari_message(self,"G�ncellemek �stedi�iniz Tabanca Verileri Ba�ar�yla Kaydedildi.","Pompa Tan�mlama")  
        else:
            s.uyari_message(self,"Bir Hata Olu�tu. G�ncellemek �stedi�iniz Tabanca Verileri Kaydedilemedi.\nL�tfen Sistem Y�neticinizle G�r���n�z.","Pompa Tan�mlama") 
    def tabancaGuncelleme(self):
        pass
    
    def kartOkuyucuGuncelle(self):
        user=self.root.user
        update = self.db.update_data(table_name=self.table_name_update_kart_okuyucu,column_name=self.column_name_update_kart_okuyucu,values=self.value_update_kart_okuyucu,where='ID_KART_OKUYUCU=%s'%self.selected_item_update,user=user)
        
        if update == 1:
             s.uyari_message(self,"G�ncellemek �stedi�iniz Kart Okuyucu Verileri Ba�ar�yla Kaydedildi.","Kart Okuyucu Tan�mlama")  
        else:
            s.uyari_message(self,"Bir Hata Olu�tu. G�ncellemek �stedi�iniz Kart Okuyucu Verileri Kaydedilemedi.\nL�tfen Sistem Y�neticinizle G�r���n�z.","Kart Okuyucu Tan�mlama") 
    def kartOkuyucuGuncelleme(self):
        pass
   
class win_Tanimlamalari_Gonder_Al(wx.Frame):
    

    def __init__(self,parent,id_,title):
        wx.Frame.__init__(self, parent,id_, title=title,
                style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR ))
        self.root = param.get_root( self )
        self.db = self.root.DB
        self.O  = self.root.O 
        self.LayoutItems()
        self.Show()
        
    def LayoutItems(self):
        mainSizer = wx.BoxSizer( wx.VERTICAL )
        altSizer  = wx.BoxSizer( wx.HORIZONTAL )
        self.Freeze()
        self.SetSizer(mainSizer)
        self.SetSize((800,600))
        self.check_list    = []
        self.jobID = 0
        self.liste = self.createTanimlamaListesi()

        self.btn_tanimlamalari_al_pressed  =  wx.Image("%sDown_red_64.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_tanimlamalari_al = wx.Image("%sDown_green_64.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_tanimlamalari_al  = LibButtons.ThemedGenBitmapTextButton(self, id=wx.NewId(), bitmap=image_tanimlamalari_al,label="Tan�mlamalar� Al           ")
        self.btn_tanimlamalari_al.Bind(wx.EVT_BUTTON, self.OnTanimlamalariAl)
        self.btn_tanimlamalari_al.SetToolTipString("Parametre Tan�mlamalar�n� Webden Al")
        self.btn_tanimlamalari_al.SetBitmapSelected(self.btn_tanimlamalari_al_pressed)
        s.setPanelListBg( self )
        s.setButtonFont( self.btn_tanimlamalari_al )
        
        self.btn_tanimlamalari_gonder_pressed  =  wx.Image("%sUp_red_64.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_tanimlamalari_gonder = wx.Image("%sUp_green_64.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_tanimlamalari_gonder  = LibButtons.ThemedGenBitmapTextButton(self, id=wx.NewId(), bitmap=image_tanimlamalari_gonder,label="Tan�mlamalar� G�nder")
        self.btn_tanimlamalari_gonder.Bind(wx.EVT_BUTTON, self.OnTanimlamalariGonder)
        self.btn_tanimlamalari_gonder.SetToolTipString("Parametre Tan�mlamalar�n� Webe G�nder")
        self.btn_tanimlamalari_gonder.SetBitmapSelected(self.btn_tanimlamalari_gonder_pressed)
        s.setPanelListBg( self )
        s.setButtonFont( self.btn_tanimlamalari_gonder )
        
        mainSizer.Add(self.liste,1,wx.EXPAND)
        altSizer.Add(self.btn_tanimlamalari_al,0,wx.ALL)
        altSizer.Add(self.btn_tanimlamalari_gonder,0,wx.ALL)
        mainSizer.Add(altSizer,0,wx.ALL)
        self.Layout()
        self.Thaw()
        
    def OnTanimlamalariAl(self,event):
        
        tanimlama_al_dizi = []
        try:
            bayii_no = str((self.db.get_raw_data_all ( table_name = 'P_BAYI' ,selects = 'BAYI_LISANS_NO' ))[0][0])
        except:
            uyari_message(self.parent,'Veritaban�n�zda Bayi Bilgisi Bulunmamaktad�r. L�tfen Bayi Giri�i Yap�n�z.',self.win_name)
        tanimlama_al_dizi.append(bayii_no)
        #if 1 in self.check_list or self.check_list==[]:
            #tanimlama_al_dizi.append('P_Dagitici')  
        if 2 in self.check_list or self.check_list==[]:
            tanimlama_al_dizi.append('P_Bayi')
        if 3 in self.check_list or self.check_list==[]:
            tanimlama_al_dizi.append('Tabancalar')
        if 4 in self.check_list or self.check_list==[]:
            tanimlama_al_dizi.append('Yakit')
        if 5 in self.check_list or self.check_list==[]:
            tanimlama_al_dizi.append('Yakit_Tanki')
        if 6 in self.check_list or self.check_list==[]:
            tanimlama_al_dizi.append('Probe')
        if 7 in self.check_list or self.check_list==[]:
            tanimlama_al_dizi.append('Portlar')
        if 8 in self.check_list or self.check_list==[]:
            tanimlama_al_dizi.append('Pompa')
        if 9 in self.check_list or self.check_list==[]:
            tanimlama_al_dizi.append('P_Otomasyon_Parametre')
            
        kwargs = tanimlama_al_dizi
   
        yon    = 'al'
        delayedresult.startWorker( self.WsDonenDegerIncele, WebSoapObj.getParameterDeger, 
                                   wargs = (yon,kwargs), jobID=self.jobID)
        #self.web_dagitici_ws_al = WebSoap().getParameterDeger(yon, kwargs)
        

    def WsDonenDegerIncele(self,delayedResult):

        if self:
            try:
                
                data = []
                data = delayedResult.get()
                if data =='bayi_404':
                    hata_oge = 'Bayi Bilgileri Hatas�!'
                    hata_msg = 'Bayi Bilgileri Verisi �le Web Sunucusundaki Veriler Uyu�mad�!'
                    
                elif data.startswith('HATA'):
                    hata_oge = 'WS Server Hatas�!'
                    hata_msg = data
                    
                elif data == 'OK':
                    hata_oge = 'Veriler Ba�ar�yla Kaydedildi.'
                    hata_msg = ''
                elif data.startswith('GENEL'):
                    hata_oge = 'Genel Bir Hata Olu�tu'
                    hata_msg = data
                else:
                    message = ''
                    donen_delete    = 0
                    update_tables   = ['P_OTOMASYON_PARAMETRE,ID_PARAMETRE','YAKIT,ID_YAKIT','P_BAYI,ID_BAYI']
                    update_keys     = ['p_otomasyon_parametres','yakits','p_bayis']
                    #update_sub_keys = ['P_Otomasyon_Parametre_','Yakit_','P_Bayi_']
                    
                    delete_tables   = ['TABANCALAR','TANK_KAYITLARI','YAKIT_TANKI','POMPA','PROBE','PORTLAR']
                    delete_keys     = ['tabancalars',0,'yakit_tankis','pompas','probes','ports']
                    #delete_sub_keys = ['Port_']
                    delete_returns  = {}
                    
                    for table_name_con in update_tables:
                        table_name_sp = table_name_con.split(',')
                        table_name    = table_name_sp[0]
                        pk_col_key    = table_name_sp[1]
                        
                        cons_data_u   = data.__dict__[update_keys[update_tables.index( table_name_con )]]
                        list_keys_u   = [key_u for key_u in cons_data_u.__dict__.keys() if not key_u.startswith('__')]
                        for k in list_keys_u:#bu hep tek kayit olur ama list diye loop yaptik. nolur noolmaz.
                            for kayit in cons_data_u[ k ]:
                                kayit_keys   = [_key_ for _key_ in kayit.__dict__.keys() if not _key_.startswith('__')]
                                column_names_ = ','.join( kayit_keys )
                                values       = ''
                                for k_k in kayit_keys:
                                    values += '\'%%(%s)s\','%k_k
                                
                                values = values%kayit
                                values = values[:len(values)-1]
                                where  = '%s=%s'%(pk_col_key, kayit.__dict__[ pk_col_key ])
                                #def update_data( self, table_name,column_name, values, where ):
                                donen = self.db.update_data ( table_name = table_name ,column_name=column_names_
                                                              ,values=values,where=where)
                                #print table_name_con, ' : ', donen 
                        
                    for table_name in delete_tables:
                        index_     = delete_tables.index( table_name )
                        data_index = delete_keys[ index_ ]
                        if data_index != 0:
                            cons_data = data.__dict__[ delete_keys[ index_ ] ]
                        else:
                            cons_data = 1
                        if cons_data:
                            donen = self.db.delete_data(  table_name = table_name,where= '1=1' )
                            if cons_data != 1:
                                delete_returns[ delete_keys[index_] ] = donen
                    #prepare for insert
                    delete_tables.reverse()
                    delete_keys.reverse()
                    
                    for table_name in delete_tables:
                        index_ = delete_tables.index( table_name )
                        data_index = delete_keys[ index_ ]
                        if data_index != 0:
                            key = delete_keys[ index_ ]
                            if delete_returns.has_key( key ) and delete_returns[ key ] == 1:
                                cons_data = data.__dict__[ key ]
                                list_keys = [key_ for key_ in cons_data.__dict__.keys() if not key_.startswith('__')]
                                for k in list_keys:#bu hep tek kayit olur ama list diye loop yaptik. nolur noolmaz.
                                    for kayit in cons_data[ k ]:
                                        kayit_keys   = [_key_ for _key_ in kayit.__dict__.keys() if not _key_.startswith('__')]
                                        column_names_ = ','.join( kayit_keys )
                                        values       = ''
                                        for k_k in kayit_keys:
                                            values += '\'%%(%s)s\','%k_k
                                        
                                        values = values%kayit
                                        values = values[:len(values)-1]
                                        donen = self.db.save_data ( table_name = table_name ,column_name=column_names_
                                                                  ,values=values)
                                        
                                        #print table_name, ' : ', donen
                        
            except Exception, e:                
                if type(e) == URLError:
                    
                    if str(e[0]).startswith('[Errno 10060]'):
                        hata_oge = '�nternet Ba�lant� Hatas�!'
                        hata_msg = 'Web Servisin Sunucusu �nternete Ba�l� De�il veya Web Servis �al��m�yor!'

                    elif str(e[0]).startswith('[Errno 10065]') or str(e[0]).startswith('[Errno 11001]'):
                        hata_oge = '�nternet Ba�lant� Hatas�!'
                        hata_msg = 'Bilgisayar�n�z�n �nternet Ba�lant�s�n� Kontrol Ediniz!'
                    elif str(e[0]).startswith('[Errno 10061]'):
                        hata_oge = 'Hedef WS Url Hatas�'
                        hata_msg = 'Ayarlarda Tan�mlanm�� Olan Ws Urllerini Kontrol Ediniz!'
                        
                elif type(e) == socketError:
                    
                    if str(e).startswith('[Errno 10053]'):
                        hata_oge = 'Kullan�c� Ad� veya �ifre Hatas�!'
                        hata_msg = 'Web Servise Ba�lanmaya �al��t���n�z Kullan�c� Ad� veya �ifre Yanl��!'
                        
                elif type (e) == exceptionException:
                    
                    if str(e[0]).startswith('HTTP Error 403') or e[0][0] == 403:
                            hata_oge = 'Kullan�c� Ad� veya �ifre Hatas�!'
                            hata_msg = 'Web Servise Ba�lanmaya �al��t���n�z Kullan�c� Ad� veya �ifre Yanl��!'
                        
                    elif str(e[0]).startswith('HTTP Error 401') or e[0][0] == 401:
                        hata_oge = 'Kullan�c� Ad� veya �ifre Hatas�!'
                        hata_msg = 'Web Servise Ba�lanmaya �al���rken Kullan�c� Ad� veya �ifresi Giriniz!'
    
                    elif str(e[0]).startswith('HTTP Error 405') or e[0][0] == 405:
                        hata_oge = 'Metod Hatas�!'
                        hata_msg = 'Web Serviste Olmayan Bir Metodu �a��rd�n�z.!'

            wsSonuc(data,self,'%s'%hata_oge,'%s'%hata_msg,'Tan�mlamalar� G�nder/Al')
            
    def OnTanimlamalariGonder(self,event):
        
        #P_Dagitici  = []
        P_Bayi      = []
        Tabancalar  = []
        Yakit       = []
        Yakit_Tanki = []
        Probe       = []
        Portlar     = []
        Pompa       = []
        P_Otomasyon_Parametre = []
        
        ###item_id = 1:Da��t�c� Bilgileri ; item_id = 2:Bayi Bilgileri ; item_id = 3:Tabanca Bilgileri
        ###item_id = 4:Yak�t Bilgileri ; item_id = 5:Yak�t Tank� Bilgileri ; item_id = 6:Probe Bilgileri ; item_id = 7:Port Bilgileri
        ###item_id = 8:Pompa Bilgileri ; item_id = 9:Otomasyon Parametre Bilgileri ; check_list = [] ise hepsini gonder
        Bayi_no      = str(self.db.get_raw_data_all ( table_name = 'P_BAYI',selects = 'BAYI_LISANS_NO' )[0][0])
        #if 1 in self.check_list or self.check_list==[]:
            #P_Dagitici  = self.db.get_raw_data_all ( table_name = 'P_DAGITICI' ,selects = 'ID_DAGITICI,DAGITICI_NO,DAGITICI_LISANS,DAGITICI_ADI',dict_mi = 'dict')    
        if 2 in self.check_list or self.check_list==[]:
            P_Bayi      = self.db.get_raw_data_all ( table_name = 'P_BAYI'     ,selects = 'ID_BAYI,ID_DAGITICI,BAYI_LISANS_NO,BAYI_ADI,BAYI_ADRES,BAYI_SEHIR,BAYI_TIPI,BAYI_LISANS_BASLANGIC,BAYI_ENLEM,BAYI_BOYLAM,BAYI_TANK_SAYISI,BAYI_POMPA_SAYISI,BAYI_TANK_KAPASITE,BAYI_TABANCA_SAYISI',dict_mi = 'dict' )
        if 3 in self.check_list or self.check_list==[]:
            Tabancalar  = self.db.get_raw_data_all ( table_name = 'TABANCALAR' ,selects = 'ID_TABANCA,ID_POMPA,POMPA_NO,BEYIN_NO,ID_BASAMAK_KAYDIRMA,TABANCA_NO,ID_YAKIT_TANKI,ID_PORT,MODUL_NO',dict_mi='dict' )
        if 4 in self.check_list or self.check_list==[]:
            Yakit       = self.db.get_raw_data_all ( table_name = 'YAKIT'      ,selects = 'ID_YAKIT,YAKIT_ADI,YAKIT_BIRIM_FIYAT,YAKIT_DURUM',dict_mi = 'dict' )
        if 5 in self.check_list or self.check_list==[]:
            Yakit_Tanki = self.db.get_raw_data_all ( table_name = 'YAKIT_TANKI',selects = 'ID_YAKIT_TANKI,YAKIT_TANKI_ADI,YAKIT_TANKI_YUKSEKLIK,YAKIT_TANKI_KAPASITE,ID_PROBE,ID_PROBE,ID_PORT,ID_YAKIT,ID_ACTIVE,PROBE_BASLANGIC_SEVIYE',dict_mi = 'dict' )
        if 6 in self.check_list or self.check_list==[]:
            Probe       = self.db.get_raw_data_all ( table_name = 'PROBE'      ,selects = 'ID_PROBE,PROBE_MARKA,PROBE_UZUNLUK,SPEED_OF_WIRE,PROBE_ADRES,ID_ACTIVE',dict_mi = 'dict')
        if 7 in self.check_list or self.check_list==[]:
            Portlar     = self.db.get_raw_data_all ( table_name = 'PORTLAR'    ,selects =  'ID_PORT,PORT_ADI,PARITY,STOP_BIT,BAUND_RATE,DATA_BIT,BEK_SURESI',dict_mi = 'dict')
        if 8 in self.check_list or self.check_list==[]:
            Pompa       = self.db.get_raw_data_all ( table_name = 'POMPA'      ,selects = 'ID_POMPA,POMPA_ADI,POMPA_NO_ON,POMPA_NO_ARKA,ARABIRIM_MARKASI,ARABIRIM_ADRESI,PORT_TIPI,ID_PORT,ARABIRIMDEKI_BEYIN_SAYISI,ARABIRIMDEKI_TABANCA_SAYISI',dict_mi = 'dict')
        if 9 in self.check_list or self.check_list==[]:
            P_Otomasyon_Parametre = self.db.get_raw_data_all (table_name = 'P_OTOMASYON_PARAMETRE',selects = 'ID_PARAMETRE,PARAMETRE_ADI,PARAMETRE_DEGER,PARAMETRE_KRITER',dict_mi='dict')

        kwargs = {#'P_Dagitici':P_Dagitici, 
                  'P_Bayi':P_Bayi,
                  'Tabancalar':Tabancalar,
                  'Yakit':Yakit,
                  'Yakit_Tanki':Yakit_Tanki,
                  'Probe':Probe,
                  'Portlar':Portlar,
                  'Pompa':Pompa,
                  'P_Otomasyon_Parametre':P_Otomasyon_Parametre,
                  'Bayi_No':Bayi_no}
        yon = 'gonder'
        delayedresult.startWorker( self.WsDonenDegerIncele, WebSoapObj.getParameterDeger, 
                                   wargs = (yon,kwargs), jobID=self.jobID)
        #self.web_dagitici_ws_gonder = WebSoap().getParameterDeger(yon,kwargs)
        #print  self.web_dagitici_ws_gonder
        
    def createTanimlamaListesi( self ):
        
        self.data_all   = [(2,'Bayi Bilgileri',1),(3,'Tabanca Bilgileri',1),(4,'Yak�t Bilgileri',1),
            (5,'Yak�t Tank� Bilgileri',1),(6,'Probe Bilgileri',1),(7,'Port Bilgileri',1),(8,'Pompa Bilgileri',1),(9,'Otomasyon Parametre Bilgileri',1)]
        headers = ['Se�,digit','Tan�mlama Ad�,string']     
        self.liste = self.O.createStaticList(self, headers, self.data_all,lineCheck=True,checked_set_method=self.OnChecked,checked_index=2)  
        for child in self.liste.GetChildren():
            for i in range(2,10):
                if child.GetId() == i :
                    #print dir(child)
                    child.Disable()
        self.liste.Layout()
        return self.liste
        
    def OnChecked( self,item_id, item_state):
        if item_state == True:
            self.check_list.append( item_id )
        else:
            if item_id in self.check_list:
                self.check_list.remove( item_id )
class win_Otomasyon_Raporu(wx.Frame):
    def __init__(self, parent, id_, title):
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= wx.DEFAULT_FRAME_STYLE| wx.TAB_TRAVERSAL| wx.FRAME_NO_TASKBAR
                                | wx.CLIP_CHILDREN
                                | wx.FULL_REPAINT_ON_RESIZE)
	self.styl=s
        self.rapor_panel_id=0
        self.db = param.get_root( self ).DB
        self.O  = param.get_root( self ).O 
        self.LayoutItems()
        self.Thaw()
        self.Show()
    def LayoutItems(self):
        self.bs   = wx.BoxSizer( wx.VERTICAL )
        self.SetSizer ( self.bs )
        
        self.Freeze()
        p = wx.Panel(self,-1, style = wx.TAB_TRAVERSAL
                                    | wx.CLIP_CHILDREN
                                    | wx.FULL_REPAINT_ON_RESIZE)
        p.PAINT_GRADIENTS = True
        panelStyle = (p.PAINT_GRADIENTS and [wx.BORDER_NONE] or [0])[0]
        p.startColour = wx.Colour( 140,140,140 )
        p.endColour   = wx.WHITE
        
        gbs = wx.FlexGridSizer(1,9,3,3)
        
        lbl_bas_dpc =self.styl.TransparentText(p,label="Ba�lang�� Tarihi :")
        self.styl.setStaticTextList(lbl_bas_dpc)
        self.dpc_tarih = wx.DatePickerCtrl(p, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )
        lbl_bas_dpc2 =self.styl.TransparentText(p,label="Biti� Tarihi :")
        self.styl.setStaticTextList(lbl_bas_dpc2)
        self.dpc_tarih2 = wx.DatePickerCtrl(p, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )
        
        self.btn_search_pressed  =  wx.Image("%sdb_search_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sdb_search_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_search  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label="Rapor Haz�rla")
        self.btn_search.Bind(wx.EVT_BUTTON, self.OnRaporuGetir)
        self.btn_search.SetToolTipString("Rapor Haz�rla")
        self.btn_search.SetBitmapSelected(self.btn_search_pressed)
        self.styl.setButtonFont( self.btn_search)
        
        gbs.AddMany( [  ((80,20)),
                        (lbl_bas_dpc ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.dpc_tarih,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((20,20)),
                        (lbl_bas_dpc2 ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.dpc_tarih2,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((20,20)),
                        (self.btn_search,0, wx.ALIGN_CENTER|wx.ALIGN_CENTER_VERTICAL ),
                        ((80,20))
                        ])
        
        p.SetSizerAndFit( gbs )
        self.styl.BindEvents( p )
        self.bs.Add( p,0,wx.ALL|wx.EXPAND )
    def OnRaporuGetir(self,event):
        self.Freeze()
        
        tarih=self.dpc_tarih.GetValue()
        self.format_tarih=tarih.Format('%Y-%m-%d')
        
        t =  wx.DateSpan(days=1) 
        tarih2=self.dpc_tarih2.GetValue()+t
        self.format_tarih2=tarih2.Format('%Y-%m-%d')
            
        where_query="strftime('%%Y-%%m-%%d', TARIH)>='%s' and strftime('%%Y-%%m-%%d', TARIH)<'%s'"%(self.format_tarih,self.format_tarih2)
        
        if self.rapor_panel_id != 0:
            wx.FindWindowById(self.rapor_panel_id ).Destroy()
            
        headers         = ['KAYIT NO,digit','K���,string','��LEM,string','TAR�H,datetime']
        selects         = "ID_LOG,KISI,ISLEM,TARIH"
        PaginatingIndexPanel( self,"LOG_TABLO",
                                    selects = selects,
                                    where=where_query,
                                    order = "ID_LOG ASC",
                                    headers = headers,
                                    parent_sizer = self.bs)
        
        
        self.Layout()
        self.Thaw()
 