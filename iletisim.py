#!/usr/bin/env python
# -*- coding: latin5 -*-
import serial
import sys
import time as time_
import urllib2
from util import *
from util_db import ariza,PompaRequestFormatla
from suds.xsd.doctor import Import, ImportDoctor
from suds.client import Client
#from pysimplesoap.client import SoapClient
import suds
import sabitler
import logging
logging.basicConfig(filename='wss.log',level=logging.ERROR)
#logging.getLogger('suds.client').setLevel(logging.INFO)
from kalibrasyon import Calibration
cal = Calibration()
dolum_flag = 0
satis_flag = 0
from model import Parameters
from util import QClear
from datetime import datetime
class Dummy:
    pass
def set_last_tank_values_for_probesuz(  probe_property_list , q, db ):
	#print probe_property_list
	formatted_response = {}
	
	for probe_property in probe_property_list:
	    #print dir( probe_property )
	    
	    probe_object = Dummy()
	    
	    probe_object.yakit_litre          = 0
	    probe_object.su_mm                = 0
	    probe_object.su_lt                = 0
	    probe_object.yakit_mm             = 0
	    probe_object.yakit_sicaklik       = 0
	    probe_object.yakit_yuzde          = 0
	    probe_object.su_yuzde             = 0
	    probe_object.yakit_by_total_yuzde = 0
	    probe_object.yakit_tank_id        = probe_property.ID_YAKIT_TANKI
	    probe_object.islem_type           = [sabitler.TANK_H_ACIKLAMA.OKUNAN_DEGER]
	    
	    last_data = db.get_raw_data_all(" TANK_KAYITLARI ",where=" TANK_ID = %s "%probe_property.ID_YAKIT_TANKI,order = "ID_TANK_KAYITLARI DESC LIMIT 1", dict_mi = 'object')
	    
	    yakit_lt = 0
	    su_lt    = 0
	    
	    if last_data != []:
		
		yakit_lt = last_data[0].OKUNAN_YAKIT
		su_lt    = last_data[0].OKUNAN_SU
		
	    yakit_yuzde, su_yuzde, yakit_by_total_yuzde = getTankYuzdeHesabi( probe_property.YAKIT_TANKI_KAPASITE, yakit_lt, su_lt)
	    probe_object.yakit_litre          = yakit_lt
	    probe_object.su_lt                = su_lt
	    probe_object.yakit_yuzde          = yakit_yuzde
	    probe_object.su_yuzde             = su_yuzde
	    probe_object.yakit_by_total_yuzde = yakit_by_total_yuzde
	    
	    formatted_response[probe_property.ID_PROBE] = probe_object
	    
	QClear( q )
        q.put ( formatted_response )    
class ProbeHistoryFormatter:
    def __init__( self ):
        self.probe_deger_mem_cont = {}
        self.read_type_dict       = {}
        #self.basilacak_deger      = 0
        #self.resetted             = False
        self.dolum_time            = {}
        

    def getIslemTipiByHistory_old( self, probe_id, probe_degerleri ):
        last_islem_type = 1
        if self. probe_deger_mem_cont.has_key( probe_id ):

            p_deger_list_yakit = self. probe_deger_mem_cont[probe_id][0]
            p_deger_list_su    = self. probe_deger_mem_cont[probe_id][1]
        else:
            p_deger_list_yakit = []
            p_deger_list_su    = []

        p_deger_list_su.append( probe_degerleri.su_lt )
         
        prev_last_value      = -1
        prev_prev_last_value = -1
        if len(p_deger_list_yakit) > 2 :
            prev_last_value      = p_deger_list_yakit[-1]
            prev_prev_last_value = p_deger_list_yakit[-2]
        ##print  probe_id, '-', probe_degerleri.yakit_litre, len ( p_deger_list_yakit )
        
        p_deger_list_yakit.append( probe_degerleri.yakit_litre )
        
        islem_type = [sabitler.TANK_H_ACIKLAMA.OKUNAN_DEGER]
        
        if (probe_degerleri.yakit_litre - prev_last_value > sabitler.proveDolumKarakFark) and (prev_last_value - prev_prev_last_value > sabitler.proveDolumKarakFark):
            if self.basilacak_deger == 0:
                self.basilacak_deger = prev_prev_last_value
            ##print self.resetted
            if self.resetted == False:
                ##print '*************************'
                ##print p_deger_list_yakit
                p_deger_list_yakit   = p_deger_list_yakit[ len(p_deger_list_yakit) - p_deger_list_yakit[::-1].index(prev_prev_last_value) - 1 :len( p_deger_list_yakit ) ]
                ##print p_deger_list_yakit
                ##print '*************************'
                self.resetted = True
        if len( p_deger_list_yakit ) > sabitler.probeToplamKayitSayisi:
            
            mod_15_yakit_list = p_deger_list_yakit[0::sabitler.probeModSayisi]
            type_int_artis  = 0
            #type_int_azalma = 0
            prev_kayit_var  = False
            
            if self. read_type_dict.has_key( probe_id ):
                prev_kayit_var = True
            for mod in range ( 1, len(mod_15_yakit_list) ):
                if mod_15_yakit_list[ mod ] - mod_15_yakit_list [ mod - 1 ] > sabitler.proveDolumKarakFark:
                    type_int_artis += 1
                '''
                elif  mod_15_yakit_list [ mod - 1 ] > mod_15_yakit_list[ mod ]:
                    type_int_azalma += 1
                '''
            
            if type_int_artis >= sabitler.probeKararKayitSayisi:
                ##print 'mod 7 �zeri, dolum ya da ara kayit '
                #tankta artis var, bak bakalim dolum mu ara kayit mi
                islem_type = [ sabitler.TANK_H_ACIKLAMA.DOLUM_BASLANGIC ]
                if self.basilacak_deger == 0:
                    self.basilacak_deger = mod_15_yakit_list[0]
                if prev_kayit_var:
                    ##print 'prev_kayit_var 7 DEN BUYUK'
                    if self.read_type_dict[probe_id] == sabitler.TANK_H_ACIKLAMA.DOLUM_BASLANGIC or self.read_type_dict[probe_id] == sabitler.TANK_H_ACIKLAMA.DOLUM_ARA_DEGER:
                        ##print 'prev_kayit_var dolum ara'
                        islem_type = [ sabitler.TANK_H_ACIKLAMA.DOLUM_ARA_DEGER ]
                        self.basilacak_deger = mod_15_yakit_list[-1]
                
            else:
                #dolum_bitti ya da  olay yok.
                ##print 'mod 7 alt�, dolum bitis ya da okunan deger'
                if prev_kayit_var:
                    ##print 'prev_kayit_var 7 DEN kucuk'
                    if self.read_type_dict[probe_id] == sabitler.TANK_H_ACIKLAMA.DOLUM_BASLANGIC or self.read_type_dict[probe_id] == sabitler.TANK_H_ACIKLAMA.DOLUM_ARA_DEGER:
                        islem_type       = [ sabitler.TANK_H_ACIKLAMA.DOLUM_BITIS ]
                        self.basilacak_deger  = mod_15_yakit_list[-1]
                        self.resetted = False
                        ##print 'dolum bitis'
                    else:
                        islem_type = [sabitler.TANK_H_ACIKLAMA.OKUNAN_DEGER]
                        ##print 'okunan deger'
                        self.basilacak_deger  = mod_15_yakit_list[-1]
                    
                
            p_deger_list_yakit = []#bu �nemli
            
            ##print '******************'
            ##print islem_type, ' : ', self.basilacak_deger
            ##print '******************'

        
            p_deger_list_su = []
            self.basilacak_deger = 0
   
            last_islem_type = islem_type[-1]
            self.read_type_dict[ probe_id ]       = last_islem_type
        
        self.probe_deger_mem_cont[ probe_id ] = [p_deger_list_yakit, p_deger_list_su]
        return_list = [last_islem_type, self.basilacak_deger ]
        return return_list
    def getIslemTipiByHistory( self, probe_id, probe_degerleri ):
        last_islem_type = 1
        dolum_db_kayit  = True
        if self. probe_deger_mem_cont.has_key( probe_id ):

            p_deger_list_yakit = self. probe_deger_mem_cont[probe_id][0]
            p_deger_list_su    = self. probe_deger_mem_cont[probe_id][1]
        else:
            p_deger_list_yakit = []
            p_deger_list_su    = []
        
        if not self. dolum_time.has_key( probe_id ):
            self. dolum_time[ probe_id ] = datetime.now()
        
        
        p_deger_list_su.append( probe_degerleri.su_lt )
        basilacak_deger = -1
        
        islem_type = sabitler.TANK_H_ACIKLAMA.OKUNAN_DEGER
        
        if len(p_deger_list_yakit) < sabitler.probeToplamKayitSayisi:
            p_deger_list_yakit.append( probe_degerleri.yakit_litre )
        else :
            
            mod_15_yakit_list = p_deger_list_yakit[0::sabitler.probeModSayisi]
            type_int_artis  = 0
            prev_kayit_var  = False
            if self. read_type_dict.has_key( probe_id ):
                prev_kayit_var = True
            
            for mod in range ( 1, len(mod_15_yakit_list) ):
                if mod_15_yakit_list[ mod ] - mod_15_yakit_list [ mod - 1 ] > sabitler.proveDolumKarakFark:
                    type_int_artis += 1
                
            if type_int_artis >= sabitler.probeKararKayitSayisi:
                ##print 'mod 7 �zeri, dolum ya da ara kayit '
                #tankta artis var, bak bakalim dolum mu ara kayit mi
                islem_type = sabitler.TANK_H_ACIKLAMA.DOLUM_BASLANGIC
                basilacak_deger = p_deger_list_yakit[0]
                if prev_kayit_var:
                    ##print 'prev_kayit_var 7 DEN BUYUK'
                    if self.read_type_dict[probe_id] == sabitler.TANK_H_ACIKLAMA.DOLUM_BASLANGIC or self.read_type_dict[probe_id] == sabitler.TANK_H_ACIKLAMA.DOLUM_ARA_DEGER:
                        ##print 'prev_kayit_var dolum ara'
                        islem_type = sabitler.TANK_H_ACIKLAMA.DOLUM_ARA_DEGER
                        basilacak_deger = p_deger_list_yakit[-1]
                        ##print self.dolum_time
                        
                        if float((datetime.now() - self.dolum_time[probe_id]).total_seconds()) < float(sabitler.dolumaraKayitBeklemeSuresi):
                            dolum_db_kayit = False
                        else:
                            self.dolum_time[ probe_id ] = datetime.now()
                        
                        
                if islem_type == sabitler.TANK_H_ACIKLAMA.DOLUM_BASLANGIC:
                    self.dolum_time[ probe_id ]   = datetime.now()

            else:
                #dolum_bitti ya da  olay yok.
                ##print 'mod 7 alt�, dolum bitis ya da okunan deger'
                if prev_kayit_var:
                    ##print 'prev_kayit_var 7 DEN kucuk'
                    if self.read_type_dict[probe_id] == sabitler.TANK_H_ACIKLAMA.DOLUM_BASLANGIC or self.read_type_dict[probe_id] == sabitler.TANK_H_ACIKLAMA.DOLUM_ARA_DEGER:
                        islem_type       = sabitler.TANK_H_ACIKLAMA.DOLUM_BITIS
                        basilacak_deger  = p_deger_list_yakit[-1]
                        ##print p_deger_list_yakit
                    else:
                        islem_type = sabitler.TANK_H_ACIKLAMA.OKUNAN_DEGER
                        ##print 'okunan deger'
                        basilacak_deger  = p_deger_list_yakit[-1]
                    
                
            p_deger_list_yakit = p_deger_list_yakit[1:len(p_deger_list_yakit)]#bu �nemli
            p_deger_list_yakit.append ( probe_degerleri.yakit_litre )
            
            ##print '******************'
            ##print islem_type, ' : ', self.basilacak_deger
            ##print '******************'

        
            p_deger_list_su = []
   
            last_islem_type = islem_type
            self.read_type_dict[ probe_id ]       = last_islem_type
        
        self.probe_deger_mem_cont[ probe_id ] = [p_deger_list_yakit, p_deger_list_su]
        return_list = [last_islem_type, basilacak_deger, dolum_db_kayit ]
        return return_list
    
class PortOperations:
    def __init__(self, q = None, db=None):
        self.q                 = q
        self.ser               = {}
        self.com_property_dict = {}
        self.ariza_            = ariza()
        self.card_counter      = 0
        self.rf_counter        = 0
        self.PPU               = PPU()
        self.DB                = db
        self.musteri_cari      = {}
        self.personel_cari     = {}
        self.orpak_pir_mesaj   = {}
        self.orpak_scu_mesaj   = {}
        self.orpak_trans_mesaj = {}
        self.orpak_arabirim_start = {}
        self.prev_next_step      = {}
        self.payment_by_pompa    = {}
    def setDeafults(self):
        self.musteri_cari      = {}
        self.personel_cari     = {}
        self.orpak_pir_mesaj   = {}
        self.orpak_scu_mesaj   = {}
        self.orpak_trans_mesaj = {}
        self.orpak_arabirim_start = {}
    def open_port ( self, com_property_list, com_q = None, try_again = True ):
        for com_properties in com_property_list:
            if try_again:
                try:
                    if self.ser[ com_properties.ID_PORT ] == 0:
                        ser = serial.Serial(    port      = com_properties.PORT_ADI,
                        baudrate  = com_properties.BAUND_RATE,
                        parity    = str(com_properties.PARITY)[0],
                        bytesize  = int(com_properties.DATA_BIT),
                        timeout   = int(com_properties.BEK_SURESI),
                        stopbits  = int(com_properties.STOP_BIT))
                    
                    else:
                        ser = self.ser[ com_properties.ID_PORT ]

                except:
                    ser = 0
                
            else:
                ser = serial.Serial(    port      = com_properties.PORT_ADI,
                                    baudrate  = com_properties.BAUND_RATE,
                                    parity    = str(com_properties.PARITY)[0],
                                    bytesize  = int(com_properties.DATA_BIT),
                                    timeout   = int(com_properties.BEK_SURESI),
                                    stopbits  = int(com_properties.STOP_BIT))
            
            self.ser[com_properties.ID_PORT] = ser
            if com_q:
                com_port_dict = {}
                if not com_q.empty():
                    com_port_dict = com_q.get()
                com_port_dict = dict( com_port_dict.items() + self.ser.items())
                com_q.put( com_port_dict )
            
            self.com_property_dict[ com_properties.ID_PORT ] = com_properties 
    def make_probe_request(self, probe_property_list=None, pHistoryFormatter=None, responseFormatter=None, com_q = None ):
        
        formatted_response = {}
        
        for probe_property in probe_property_list:
            port_ = self.ser[probe_property.ID_PORT]
            if port_ != 0:
                string_query_list = self.get_probe_query_by_type(  probe_property  )
                
                query_counter     = 0
                
                for string_query in string_query_list:
                        try:
			    make_action       = False
			    
			    if query_counter == 0:
				make_action = True
			    ##print string_query
			   
			    if  string_query == '':
				pass
				#probe tanimlama hatasi ( protokol bulunamadi )
			    
			    req_type = sabitler.PROBE_ID_MARKA.probe_id_formatter_dict[ probe_property.ID_PROBE_MARKA ].req_type
			    ##print string_query
			    if req_type == 'str':
				donen = com_port_req_and_return_answer_probe( port_, string_query)
			    elif req_type == 'byte':
				donen = com_port_byte_req_and_return_answer_probe( port_, string_query)
			    ##print donen
			    if donen != 0 and donen != [0] :
				probe_object = responseFormatter.ProbeResponseFormat ( donen, probe_property )
				if make_action:
				    donen = pHistoryFormatter.getIslemTipiByHistory(probe_property.ID_PROBE, probe_object)
				    probe_object.islem_type = donen
			    else:
				probe_object = responseFormatter.ProbeResponseFormat ( None, probe_property )
	
			    query_counter += 1
                        
                        except:
                            print ( str(sys.exc_info()[0]))
                            self.ser[probe_property.ID_PORT ] = 0
                            probe_object = responseFormatter.ProbeResponseFormat ( None, probe_property )
                            self.open_port([self.com_property_dict[ probe_property.ID_PORT ]], com_q)
                        
                        if probe_object and make_action == True:
                            formatted_response[probe_property.ID_PROBE] = probe_object
                            self.ariza_.arizaProbeformatla( probe_property,probe_object )
                      
            else:
                
                if sabitler.simulation:
                    probe_object = responseFormatter.ProbeResponseFormat ( None, probe_property, sabitler.simulation )
                    formatted_response[probe_property.ID_PROBE] = probe_object
                self.ariza_.arizaPortformatla ( probe_property )
                self.ser[probe_property.ID_PORT ] = 0
                self.open_port([self.com_property_dict[ probe_property.ID_PORT ]], com_q)
         
        QClear( self.q )
        self.q.put ( formatted_response )
        
        ##print 'formatted_request : ', formatted_response

        responseFormatter   = None
        probe_object        = None
        formatted_response  = None
        probe_property_list = None   
    def make_pompa_request( self, pompa_arabirim_object, pompa_tabancalar, pompa_formatla, com_q, endeks_q, card_q, card_req_q ):
        formatted_response_pompa     = {}
        
        for arabirim_base_object, arabirim_tabanca_sub_object in pompa_arabirim_object.iteritems():
            ilk_birim_fiyat = None
            port_ = self.ser[arabirim_base_object.port]
            if port_ != 0:
                donen                  = []
                try:
                    if arabirim_base_object.ad == 'teosis':
                        satis_donen            = []
                        status_donen           = []
                        #print ' GIDEN', arabirim_base_object.pump_st_req
                        get_byte_request_by_query ( arabirim_base_object.pump_st_req  , port_ )    
                        status_donen     = get_byte_request_by_query( arabirim_base_object.pool, port_ )
                        get_byte_request ( arabirim_base_object.pool_end, port_)
                        #print 'STATUS ',status_donen
    
                        if len( status_donen ) > 5:
                            
                            active_nozzle_number    = int(status_donen[4][1])
                            arabirim                = arabirim_tabanca_sub_object[ active_nozzle_number ]
                            tabanca_id              = arabirim.ID_TABANCA
                            next_step               = []
                            card_object             = None
                            
                            if arabirim.status_resp_dict.has_key( status_donen[5] ) and arabirim_base_object.pump_beyin_no == status_donen[4][0] and int(arabirim.TABANCA_NO) == int(active_nozzle_number):
                                if status_donen[5] == arabirim_base_object.call_resp:
                                    if arabirim_base_object.card_reader:
                                        #car reader var m�
                                        continue_, birim_fiyat = self.getCardReaderOps( arabirim_base_object,arabirim, card_q, card_req_q )
                    
                                        if continue_ == 1:
                                            if (self.musteri_cari.has_key( arabirim_base_object.pompa_no ) and self.musteri_cari[ arabirim_base_object.pompa_no ]) and ( self.personel_cari.has_key( arabirim_base_object.pompa_no ) and self.personel_cari[ arabirim_base_object.pompa_no ]):
                                                birim_fiyat = str(self.musteri_cari[ arabirim_base_object.pompa_no ].birim_fiyat)
                                            
                                            self.PPU.update_ppu( port_, [arabirim], str(birim_fiyat)   )
                                            next_step    = arabirim.status_resp_dict[ status_donen[5]  ]
                                        
                                        elif continue_ == 0:
                                            #print 'uncall'
                                            next_step    = arabirim.status_resp_dict[ 'uncall' ] #bu olmal�
                                            #next_step    = arabirim.status_resp_dict[ status_donen[5]  ] # process kalmas�n diye yapt�k
                                            birim_fiyat  = self.DB.getBirimFiyatByTabancaID( arabirim.ID_TABANCA  )
                                            self.PPU.update_ppu( port_, [arabirim], str(birim_fiyat[0].YAKIT_BIRIM_FIYAT) )
                                    else:
                                        next_step    = arabirim.status_resp_dict[ status_donen[5]  ]
                                        birim_fiyat  = self.DB.getBirimFiyatByTabancaID( arabirim.ID_TABANCA  )
                                        self.personel_cari[ arabirim_base_object.pompa_no ] = self.DB.getActiveVardiaDefaultUser()
                                        self.musteri_cari [ arabirim_base_object.pompa_no ] = None
                                        self.PPU.update_ppu( port_, [arabirim], str(birim_fiyat[0].YAKIT_BIRIM_FIYAT) )
                                else:
                                    
                                    next_step    = arabirim.status_resp_dict[ status_donen[5]  ]
                                    ##print next_step
                                    #print port_ops_object.musteri_cari[ pompa_no ]
                                    
                                endeks_donen = [] 
                                for step in next_step:
                                    #print 'GIDEN      ', arabirim.__dict__[ step ]
                                    get_byte_request_by_query ( arabirim.__dict__[ step ], port_ )
                                    satis_donen     = get_byte_request_by_query( arabirim_base_object.pool, port_ )
                                    endeks_donen.append ( satis_donen )
                                    get_byte_request ( arabirim_base_object.pool_end, port_)
                        #print endeks_donen        
                        last_donen = donen
                        if status_donen != []:
                            last_donen = status_donen
                        if satis_donen != []:
                            last_donen = satis_donen
                        #print 'DONEN  ',last_donen
                        try:
                            if len( last_donen ) > 4:
                                if arabirim_base_object.pump_beyin_no == last_donen[4][0] and int(arabirim.TABANCA_NO) == active_nozzle_number:
                                    formatted_response_pompa[ arabirim_base_object.pompa_no  ] = [last_donen, arabirim_base_object, arabirim, endeks_donen, status_donen[5] ]
                                
                        except:
                            print 'except 1'
                            print ( str(sys.exc_info()[0]))
                            pass
                        
                        try:
    
                            cari_obj = [ self.musteri_cari,self.personel_cari  ]
                            ##print 1
                            formatted_response, formatted_endeks = pompa_formatla.arabirim_donen_formatla( formatted_response_pompa, self.ariza_, card_req_q = card_req_q, cari_obj = cari_obj, port_ops_object=self ) 
                        
                            QClear( self.q )
                            self.q.put   (  formatted_response )
                            endeks_q.put (  formatted_endeks )
                        
                        except:
                            print 'except_in'
                            print ( str(sys.exc_info()[0]))
                            self.ariza_.arizaArabirimformatla ( arabirim_base_object,iletisim_var_mi = False )
                        
                    elif arabirim_base_object.ad == 'orpak':
                        
                        satis_donen            = []
                        status_donen           = []
                        
                        if not self.orpak_scu_mesaj.has_key( arabirim_base_object.arabirim_adres ):
                            self.orpak_scu_mesaj[ arabirim_base_object.arabirim_adres ] = '01'
                        if not self.orpak_pir_mesaj.has_key( arabirim_base_object.arabirim_adres ):
                            self.orpak_pir_mesaj[ arabirim_base_object.arabirim_adres ] = '01'
      
                        pump_st_req      = arabirim_base_object.final_formatter( arabirim_base_object.pump_st_req, self.orpak_scu_mesaj, self.orpak_pir_mesaj,arabirim_base_object  )
                        pump_donen       = get_byte_request_by_query ( pump_st_req  , port_ )
                        #print pump_st_req
                        #print pump_donen
                        self.set_scu_pir_id_by_arabirim( pump_donen , arabirim_base_object )
    
                        pool             = arabirim_base_object.final_formatter( arabirim_base_object.pool, self.orpak_scu_mesaj, self.orpak_pir_mesaj,arabirim_base_object  )
                        status_donen     = get_byte_request_by_query( pool, port_ )
                        
                        print pool, status_donen
                        self.set_scu_pir_id_by_arabirim( status_donen , arabirim_base_object )
                        
                        pool_end               = arabirim_base_object.final_formatter( arabirim_base_object.pool_end, self.orpak_scu_mesaj, self.orpak_pir_mesaj,arabirim_base_object  )
                        ack_donen              = get_byte_request_by_query ( pool_end, port_)
                        self.set_scu_pir_id_by_arabirim( ack_donen , arabirim_base_object )
                        #print pump_st_req
                        #print 'first_ status   : ',status_donen
                        
                        if len( status_donen ) > 13 and status_donen[ 5 ] == '28':
                            self.orpak_arabirim_start [ arabirim_base_object.arabirim_adres ] = 1 
                            active_nozzle_number    = int(status_donen[12][1])
                            if active_nozzle_number == 0:
                                active_nozzle_number = 1
                            #print 'acn:',active_nozzle_number
                            active_beyin_number     = status_donen[11][1]
                            #print 'acbeyin',active_beyin_number
                            if not self.orpak_trans_mesaj.has_key( str(arabirim_base_object.arabirim_adres) + '_' + active_beyin_number ):
                                self.orpak_trans_mesaj[ str(arabirim_base_object.arabirim_adres) + '_' + active_beyin_number  ] = ['00','00','00','00']
                            arabirim                = arabirim_tabanca_sub_object[ active_nozzle_number ]
                            tabanca_id              = arabirim.ID_TABANCA
                            next_step               = []
                            card_object             = None
                            
                            if arabirim.status_resp_dict.has_key( status_donen[13] ) and arabirim_base_object.pump_beyin_no == active_beyin_number and int(arabirim.TABANCA_NO) == int(active_nozzle_number):
                                if status_donen[13] == arabirim_base_object.call_resp:
                                    if arabirim_base_object.card_reader:
                                        #print 'card var'
                                        #car reader var m�
                                        continue_, birim_fiyat = self.getCardReaderOps( arabirim_base_object,arabirim, card_q, card_req_q )
                                        
                                        if continue_ == 1:
                                            if (self.musteri_cari.has_key( arabirim_base_object.pompa_no ) and self.musteri_cari[ arabirim_base_object.pompa_no ]) and ( self.personel_cari.has_key( arabirim_base_object.pompa_no ) and self.personel_cari[ arabirim_base_object.pompa_no ]):
                                                birim_fiyat = str(self.musteri_cari[ arabirim_base_object.pompa_no ].birim_fiyat)
                                            
                                            donen_ppu = self.PPU.update_ppu( port_, [arabirim], str(birim_fiyat), self.orpak_scu_mesaj, self.orpak_pir_mesaj )
                                            self.set_scu_pir_id_by_arabirim( donen_ppu , arabirim_base_object )
                                            
                                            
                                            next_step    = arabirim.status_resp_dict[ status_donen[13]  ]
                                        
                                        elif continue_ == 0:
                                             
                                            next_step    = arabirim.status_resp_dict[ 'uncall' ] #bu olmal�
                                            #next_step    = arabirim.status_resp_dict[ status_donen[5]  ] # process kalmas�n diye yapt�k
                                            birim_fiyat  = self.DB.getBirimFiyatByTabancaID( arabirim.ID_TABANCA  )
                                            donen_ppu = self.PPU.update_ppu( port_, [arabirim], str(birim_fiyat[0].YAKIT_BIRIM_FIYAT), self.orpak_scu_mesaj, self.orpak_pir_mesaj )
                                            self.set_scu_pir_id_by_arabirim( donen_ppu , arabirim_base_object )
                                    else:
                                        next_step    = arabirim.status_resp_dict[ status_donen[13]  ]
                                        birim_fiyat  = self.DB.getBirimFiyatByTabancaID( arabirim.ID_TABANCA  )
                                        self.personel_cari[ arabirim_base_object.pompa_no ] = self.DB.getActiveVardiaDefaultUser()
                                        self.musteri_cari [ arabirim_base_object.pompa_no ] = None
                                        donen_ppu = self.PPU.update_ppu( port_, [arabirim], str(birim_fiyat[0].YAKIT_BIRIM_FIYAT), self.orpak_scu_mesaj, self.orpak_pir_mesaj )
                                        self.set_scu_pir_id_by_arabirim( donen_ppu , arabirim_base_object )
                                else:
                                    #print status_donen[13]
                                    next_step    = arabirim.status_resp_dict[ status_donen[13]  ]
                                    #print status_donen[13], next_step
                                    #print port_ops_object.musteri_cari[ pompa_no ]
                                    
                                endeks_donen = [] 
                                for step in next_step:
                                    #print 'GIDEN      ', arabirim.__dict__[ step ]
                                    if arabirim.__dict__[ step ].count('%s') == 2:
                                        ns               = arabirim_base_object.final_formatter( arabirim.__dict__[ step ], self.orpak_scu_mesaj, self.orpak_pir_mesaj,arabirim_base_object  )
                                    elif arabirim.__dict__[ step ].count('%s') == 3:
                                        ns               = arabirim_base_object.final_formatter( arabirim.__dict__[ step ], self.orpak_scu_mesaj, self.orpak_pir_mesaj,arabirim_base_object, self.orpak_trans_mesaj, active_beyin_number  )
                                    
                                    if arabirim.__dict__[ step ].count('%s') == 3 and self.orpak_trans_mesaj[ str(arabirim_base_object.arabirim_adres) + '_' + active_beyin_number ] == ['00','00','00','00']:
                                        pass
                                    else:
                                        #print ns
                                        donen            = get_byte_request_by_query ( ns, port_ )
                                        self.set_scu_pir_id_by_arabirim( donen , arabirim_base_object )
                                    
                                    pool             = arabirim_base_object.final_formatter( arabirim_base_object.pool, self.orpak_scu_mesaj, self.orpak_pir_mesaj,arabirim_base_object  )
                                    satis_donen     = get_byte_request_by_query( pool, port_ )
                                    
                                    self.set_scu_pir_id_by_arabirim( satis_donen , arabirim_base_object )
                                    
                                    if len(satis_donen) >5 and satis_donen[5] == 24:
                                        self.set_trans_mesaj_by_arabirim_beyin( satis_donen, arabirim_base_object )
                                        #arabirim_base_object.final_formatter( arabirim_base_object.pool, self.orpak_scu_mesaj, self.orpak_pir_mesaj,arabirim_base_object  )
                                    if len(satis_donen) > 5 and satis_donen[5] != '28':
                                        endeks_donen.append ( satis_donen )
                                    pool_end               = arabirim_base_object.final_formatter( arabirim_base_object.pool_end, self.orpak_scu_mesaj, self.orpak_pir_mesaj,arabirim_base_object  )
                                    ack_donen              = get_byte_request_by_query ( pool_end, port_)
                                    self.set_scu_pir_id_by_arabirim( ack_donen , arabirim_base_object )
                        
                        #print endeks_donen        
                        last_donen = donen
                        if status_donen != []:
                            last_donen = status_donen
                        if satis_donen != []:
                            last_donen = satis_donen
                        #print 'DONEN  ',last_donen
                        try:
                            #print '***************************************'
                            #print last_donen[11][1], active_nozzle_number
                            #print arabirim_base_object.pump_beyin_no, int(arabirim.TABANCA_NO)
                            if arabirim_base_object.pump_beyin_no == last_donen[11][1] and int(arabirim.TABANCA_NO) == active_nozzle_number:
                                #print arabirim_base_object.pompa_no
                                formatted_response_pompa[ arabirim_base_object.pompa_no  ] = [last_donen, arabirim_base_object, arabirim, endeks_donen, status_donen[13] ]
                            #print "------------------------------------"
                            #time.sleep( 1 )
                        except:
                            #print 'except 1'
                            print ( str(sys.exc_info()[0]))
                            pass
                        
                        try:
    
                            cari_obj = [ self.musteri_cari,self.personel_cari  ]
                            ##print 1
                            formatted_response, formatted_endeks = pompa_formatla.arabirim_donen_formatla( formatted_response_pompa, self.ariza_, card_req_q = card_req_q, cari_obj = cari_obj, port_ops_object=self ) 
                            
                            QClear( self.q )
                            self.q.put   (  formatted_response )
                            endeks_q.put (  formatted_endeks )
                        
                        except:
                            print 'except_in'
                            print ( str(sys.exc_info()[0]))
                            self.ariza_.arizaArabirimformatla ( arabirim_base_object,iletisim_var_mi = False )
                    
                except:
                    print ( str(sys.exc_info()[0]))
                    QClear( self.q )
                    self.q.put ( {arabirim_base_object.pompa_no : None} )
                    self.ariza_.arizaPortformatla ( arabirim_base_object,probe_port = False )
                    self.ser[arabirim_base_object.port ] = 0
                    #print 'excep'
                    self.open_port([self.com_property_dict[ arabirim_base_object.port ]],com_q)
                    
                
            else:
               
                QClear( self.q )
                if sabitler.simulation:
                    formatted_response_pompa     = {}
                    endeks_donen = sabitler.ARABIRIM_ID_MARKA.simulation.endeks
                    #print arabirim_base_object.pompa_no
                    formatted_response_pompa[ arabirim_base_object.pompa_no ] = [None, arabirim_base_object, arabirim_tabanca_sub_object[ arabirim_base_object.pompa_no ], endeks_donen, None ]
                    formatted_response, formatted_endeks = pompa_formatla.arabirim_donen_formatla( formatted_response_pompa, self.ariza_, sabitler.simulation )
                    endeks_q.put (  formatted_endeks )
                    
                    self.q.put   ( formatted_response )
                    time_.sleep( sabitler.similasyonbeklemesuresi )
                else:
                    
                    self.q.put ( {arabirim_base_object.pompa_no : None} )
                    self.ariza_.arizaPortformatla ( arabirim_base_object,probe_port = False )
                self.ser[arabirim_base_object.port ] = 0
                self.open_port([self.com_property_dict[ arabirim_base_object.port]],com_q)

        responseFormatter      = None
        arabirim_object        = None
        formatted_response     = None
        probe_property_list    = None   
    def set_scu_pir_id_by_arabirim( self, donen, arabirim_base_object ):
        try:
            scu_id = donen[3]
            #print 'scu_id_donen: ', scu_id
            if scu_id == 'ff':
                scu_id = '01'
            scu_id = int(str(scu_id),16)
            scu_id += 1
            scu_id = hex( scu_id )[2:]
            
            if len(scu_id) == 1:
        	scu_id = '0'+ scu_id
            
            pir_id = donen[4]
            
            self.orpak_scu_mesaj[ arabirim_base_object.arabirim_adres ] = scu_id
            self.orpak_pir_mesaj[ arabirim_base_object.arabirim_adres ] = pir_id
            
            #print self.orpak_scu_mesaj
            #print self.orpak_pir_mesaj
        except:
            #print donen
            print ( str(sys.exc_info()[0]))
            print 'pir scu except'  
    def set_trans_mesaj_by_arabirim_beyin( self, donen, arabirim_base_object ):
        try:
            beyin_no    = donen[11][1]
            trans_list  = []
            trans_list.appned( donen [72] )
            trans_list.appned( donen [74] )
            trans_list.appned( donen [75] )
            trans_list.appned( donen [75] )
            self.orpak_trans_mesaj[str(arabirim_base_object.arabirim_adres)+'_'+ beyin_no] = trans_list
        except:
            
            print ( str(sys.exc_info()[0]))
            print 'trans except'
    def getCardReaderOps( self, arabirim_base_obj,arabirim_tabanca_sub_object, card_q, card_req_q ):
        continue_       = -1
        birim_fiyat     = None
        #print card_q.empty()
        if not card_q.empty():
            card_q_object    = card_q.get()
            card_q.put     ( card_q_object )
            try:
                rf_id          = card_q_object[ arabirim_base_obj.pompa_no].RF_ID
            except:
                return continue_, birim_fiyat
            #print rf_id
            kart_object    = card_q_object[ arabirim_base_obj.pompa_no].kart_object
	    #print dir( arabirim_tabanca_sub_object )
            birim_fiyat    = self.DB.getBirimFiyatByTabancaID( arabirim_tabanca_sub_object.ID_TABANCA  )[0].YAKIT_BIRIM_FIYAT
            
	    if rf_id:
                if kart_object and kart_object.ID_ACTIVE==1:
                    if kart_object.ID_CARI_TIPI == sabitler.CARI_TIPLER.MUSTERI:
                        #burada danan�n kuyru�unu tuttuk.
                        #bak bakal�m iskonto var m�, bakiyesi yeterli mi vs, bakiye sonra,�nce iskonto, plaka tutuyor mu?
                        if kart_object.iskonto_info != [] and kart_object.iskonto_info:
                            donen_birim_fiyat = self.setPPU_byCard( kart_object ,arabirim_tabanca_sub_object )
                            if donen_birim_fiyat:
                                birim_fiyat = donen_birim_fiyat
                        kart_object.birim_fiyat = birim_fiyat
                        self.musteri_cari[ arabirim_base_obj.pompa_no ] = kart_object
                        if kart_object.POMPA_ACABILIR_MI == 1:
                            continue_ = 1
                        else:
                            if card_q_object[ arabirim_base_obj.pompa_no].card_base_object.ad == 'rfid':
                                card_req_q.put( {arabirim_base_obj.pompa_no:['read_w_card','plate_%s'%kart_object.PLAKA]} )
                            else:
                                card_req_q.put( {arabirim_base_obj.pompa_no:['read_w_card']})
                    elif kart_object.ID_CARI_TIPI == sabitler.CARI_TIPLER.PERSONEL:
                        card_req_q.put( {arabirim_base_obj.pompa_no:['start_filling']} )
                        self.personel_cari[ arabirim_base_obj.pompa_no ] = kart_object
                        continue_ = 1
                    self.card_counter = 0
                else:
                    #'kart db de yok'
                    card_req_q.put( {arabirim_base_obj.pompa_no:['invalid_uid']} )
                    continue_ = 0
                    self.personel_cari[ arabirim_base_obj.pompa_no ] = None
                    self.musteri_cari[ arabirim_base_obj.pompa_no ]  = None
                    QClear( card_q )
                    self.card_counter = 0
            else:
                self.card_counter +=1
                ##print self.card_counter
                if self.card_counter > 30:
                    self.personel_cari[ arabirim_base_obj.pompa_no ] = None
                    self.musteri_cari[ arabirim_base_obj.pompa_no ] = None
                    card_req_q.put( {arabirim_base_obj.pompa_no:['invalid_uid']} )
                    continue_ = 0
                    self.card_counter = 0
        return continue_ , birim_fiyat
    def setPPU_byCard( self,kart_object, arabirim_tabanca_sub_object):
        iskonto_list = kart_object.iskonto_info
        if iskonto_list:
            for iskonto in iskonto_list:
                #paket yap, tabanca_obj, yak�t_id, birim fiyat �eklinde, acik tabancada indirim var m�
                
                if arabirim_tabanca_sub_object.ID_YAKIT == iskonto.ID_YAKIT:
                    birim_fiyat  = self.DB.getBirimFiyatByTabancaID( arabirim_tabanca_sub_object.ID_TABANCA  )[0].YAKIT_BIRIM_FIYAT
                    #islec degistirerek daha elegant yaz�labilir, ugrasamam aq
                    
                    if iskonto.ID_UYG_TURU == sabitler.ISKONTO_UYG_TURU.INDIRIM:
                        if iskonto.ID_UYG_CINSI == sabitler.ISKONTO_UYG_CINSI.MIKTAR:
                            birim_fiyat = birim_fiyat - iskonto.ORAN_MIKTAR
                        elif iskonto.ID_UYG_CINSI == sabitler.ISKONTO_UYG_CINSI.YUZDE:
                            birim_fiyat = birim_fiyat - ( (birim_fiyat * iskonto.ORAN_MIKTAR) / 100 )
                    
                    if iskonto.ID_UYG_TURU == sabitler.ISKONTO_UYG_TURU.ARTIRIM:
                        if iskonto.ID_UYG_CINSI == sabitler.ISKONTO_UYG_CINSI.MIKTAR:
                            birim_fiyat = birim_fiyat + iskonto.ORAN_MIKTAR
                        elif iskonto.ID_UYG_CINSI == sabitler.ISKONTO_UYG_CINSI.YUZDE:
                            birim_fiyat = birim_fiyat + ( (birim_fiyat * iskonto.ORAN_MIKTAR) / 100 )
                    birim_fiyat = round( birim_fiyat, 2)
                    return birim_fiyat
        return None
    def make_card_request( self, card_arabirim_object, pompa_tabancalar, card_formatla, com_q, req_q, pay_q ):
        formatted_response_card     = {}
        crad_reader_count = len(card_arabirim_object.keys())
        for pompa_no, card_base_object in card_arabirim_object.iteritems():
            self.payment_by_pompa[ pompa_no ] = sabitler.ODEME_SEKILLERI.DEFAULT
            port_ = self.ser[card_base_object.port]
            if port_ != 0:
                donen                  = []
                try:
                    if card_base_object.ad == 'best':
                        status_donen           = []
                        deger_donen            = []
                        status_donen     = get_byte_request_by_query_card ( card_base_object.pool  , port_ )
                        
                        return_idle  = False 
                        if len(status_donen) > 1: 
                            next_step    = card_base_object.status_resp_dict[ status_donen[2]  ]
                            
                            if not req_q.empty():
                                req_obj      = req_q.get()
                                if req_obj.has_key( pompa_no ):
                                    if req_obj[ pompa_no ]:
                                        next_step = req_obj[ pompa_no ]
                            
                            if next_step.count('return_idle') > 0:
                                return_idle = True
                            
                            for step in next_step:
                                if step != 'return_idle':
                                    deger_donen = get_byte_request_by_query_card ( card_base_object.__dict__[ step ], port_ )
                                    ##print deger_donen
    
                            last_donen = donen
                            if status_donen != []:
                                last_donen = status_donen
                            if deger_donen != []:
                                last_donen = deger_donen
                    
                    
                            ##print last_donen
                    
                            formatted_response_card[ pompa_no ] = [last_donen, card_base_object, status_donen[2] ]
                        else:
                            #print '***********************************'
                            #return_idle = True
                            formatted_response_card[ pompa_no ] = []
                        try:
                            formatted_response = card_formatla.card_donen_formatla( formatted_response_card, self.ariza_ ) 
                            #print formatted_response
                            #if len( formatted_response.keys() ) == crad_reader_count:
                            QClear( self.q )
                            self.q.put   ( formatted_response )
    
                        except:
                            #self.ariza_.arizaArabirimformatla ( arabirim_base_object,iletisim_var_mi = False )    
                            print 'except inner '
                            print ( str(sys.exc_info()[0]))
                            QClear( self.q )
                        ##print return_idle
                    
                       
                        if return_idle:
                            return_idle     = get_byte_request_by_query_card ( card_base_object.return_idle  , port_ )
                    
                    if card_base_object.ad == 'rfid':
                        status_donen           = []
                        deger_donen            = []
                        status_donen     = get_byte_request_by_query_card ( card_base_object.pool  , port_ )
                        #print dir( card_base_object)
                        return_idle  = False 
                        if len(status_donen) > 1:
                            if status_donen[3] != card_base_object.card_id_resp:
                                if card_base_object.status_resp_dict.has_key( status_donen[4] ):
                                    next_step    = card_base_object.status_resp_dict[ status_donen[4]  ]
                                else:
                                    next_step    = []
                            else:
                                next_step    = card_base_object.status_resp_dict[ status_donen[3]  ]
                            
                            if status_donen[3] == '69':
                                if self.prev_next_step.has_key( pompa_no ):
                                    obj     = self.prev_next_step[ pompa_no ]
                                    obj.go  = False
                                    self.prev_next_step[ pompa_no] = obj
                                
                                
                            if not req_q.empty():
                                req_obj      = req_q.get()
                                if req_obj.has_key( pompa_no ):
                                    if req_obj[ pompa_no ]:
                                        next_step = req_obj[ pompa_no ]
                                        
                                        if len(next_step) > 0  and next_step[0] == 'final_payment':
                                            card_final_paymen_object = Dummy()
                                            card_final_paymen_object.counter = 0
                                            card_final_paymen_object.go      = True
                                            self.prev_next_step[ pompa_no] = card_final_paymen_object
    
                            if self.prev_next_step.has_key( pompa_no ):
                                if self.prev_next_step[pompa_no].go == True and  self.prev_next_step[pompa_no].counter < 15  and status_donen[3] != 69:
                                
                                    next_step = ['final_payment']
                                    obj = self.prev_next_step[ pompa_no ]
                                    obj.counter +=1
                                    self.prev_next_step[ pompa_no] = obj
                                else:
                                    if card_base_object.payment_dict.has_key( status_donen[4] ):
                                        payment   = card_base_object.payment_dict[ status_donen[4] ]
                                        self.payment_by_pompa[ pompa_no ] = payment
                                        if not pay_q.empty():
                                            self.payment_by_pompa = pay_q.get()
                                            self.payment_by_pompa[ pompa_no ] = payment
                                            
                                        pay_q.put(  self.payment_by_pompa )
                                    
                                    next_step = ['filling_sum','clear_cache']
                                     
                                    del (self.prev_next_step[ pompa_no] )
                                    
                            if next_step.count('clear_cache') > 0:
                                return_idle = True
                            
                            if len(filter( lambda x:x.find('plate') != -1, next_step  )) > 0:
                                #plaka ve read W kart gelmi�
                                read_w_kart  = card_base_object.__dict__[ next_step[0] ]
                                plate_number = next_step[1].split('_')[1]
                                if  len( plate_number ) != 8:
                                    plate_number = '0'* (8 - len( plate_number )) + plate_number
                                plate_number_hex = ''
                                for p in plate_number:
                                    plate_number_hex += p.encode('hex')
                                plate_number  = plate_number_hex
                                final_req     = read_w_kart % plate_number
                                final_req     = final_req + card_base_object.crc_end%getCRC16( final_req, True, 'xmodem' )
                                deger_donen = get_byte_request_by_query_card ( final_req, port_ )
                            else:
                                for step in next_step:
                                    if step != 'clear_cache':
                                        deger_donen = get_byte_request_by_query_card ( card_base_object.__dict__[ step ], port_ )
        
                            last_donen = donen
                            if status_donen != []:
                                last_donen = status_donen
                            if deger_donen != []:
                                last_donen = deger_donen
                            
                            #print last_donen
                            
                            formatted_response_card[ pompa_no ] = [last_donen, card_base_object, status_donen[4] ]
                        else:
                            #print '***********************************'
                            #return_idle = True
                            formatted_response_card[ pompa_no ] = []
                        try:
                            formatted_response = card_formatla.card_donen_formatla( formatted_response_card, self.ariza_ ) 
                            #print formatted_response
                            #if len( formatted_response.keys() ) == crad_reader_count:
                            QClear( self.q )
                            self.q.put   ( formatted_response )
    
                        except:
                            #self.ariza_.arizaArabirimformatla ( arabirim_base_object,iletisim_var_mi = False )    
                            print 'except card inner '
                            print ( str(sys.exc_info()[0]))
                            QClear( self.q )
                        ##print return_idle
                    
                       
                        if return_idle:
                            return_idle     = get_byte_request_by_query_card ( card_base_object.clear_cache  , port_ )
                    
                except:
                
                        print 'except card outer'
                        print ( str(sys.exc_info()[0]))
                        QClear( self.q )
                        self.q.put ( {card_base_object.pompa_no : None} )
                        ##print 'except'
                        #self.ariza_.arizaPortformatla ( arabirim_base_object,probe_port = False )
                        self.ser[card_base_object.port ] = 0
                        ##print 'excep'
                        self.open_port([self.com_property_dict[ card_base_object.port ]],com_q)   
                        
            else:
                #print '-----------------------------'
                self.ser[card_base_object.port ] = 0
                self.open_port([self.com_property_dict[ card_base_object.port]],com_q)
                if not self.q.empty():
                    yakabuu = self.q.get()
                    if isinstance( yakabuu, dict ):
                        yakabuu[card_base_object.pompa_no] = None 
                        self.q.put ( yakabuu )
                '''
                #self.ariza_.arizaPortformatla ( arabirim_base_object,probe_port = False )
                #time.sleep( sabitler.except_bekleme_suresi )
                QClear( self.q )
                if sabitler.simulation:
                    formatted_response_pompa     = {}
                    for tabanca_id, arabirim in arabirim_tabanca_sub_object.iteritems():
                        
                        endeks_donen = sabitler.ARABIRIM_ID_MARKA.simulation.endeks
                        formatted_response_pompa[ tabanca_id ] = [None, arabirim_base_object, arabirim, endeks_donen, None ]
                        formatted_response, formatted_endeks = pompa_formatla.arabirim_donen_formatla( formatted_response_pompa, self.ariza_, sabitler.simulation )
                        endeks_q.put (  formatted_endeks )
                        self.q.put   ( formatted_response )
                else:
                    self.q.put ( {arabirim_base_object.pompa_no : None} )
                '''
        
        responseFormatter      = None
        arabirim_object        = None
        formatted_response     = None
        probe_property_list    = None
    def get_manual_satis_request(self,  pompa_arabirim_object, pompa_tabancalar, pompa_formatla ):
        
        for arabirim_base_object, arabirim_tabanca_sub_object in pompa_arabirim_object.iteritems():
            port_ = self.ser[arabirim_base_object.port]
            if port_ != 0:
                donen                  = []
                try:
                    if arabirim_base_object.ad == 'teosis':
                        for i in range(400):
                            ##print i
                            i_str = str(i)
                            s_eklenecek = (4-len( i_str ))*'0'
                            final_i_str = s_eklenecek + i_str
                            req_sorgu = arabirim_base_object.req_sale_records%( hex(arabirim_base_object.arabirim_adres)[2:], final_i_str)
                            req_sorgu = req_sorgu + arabirim_base_object.crc_end % getCRC16( req_sorgu )
                            #get_byte_request ( arabirim_base_object.pool_end, port_)
                            get_byte_request_by_query( req_sorgu, port_ )
                            status_donen = []
                            status_donen.extend (get_byte_request_by_query( arabirim_base_object.pool, port_ ))
                            #status_donen.extend (get_byte_request_by_query( arabirim_base_object.pool, port_ ))
                            c  = pompa_formatla.manuel_satis_formatla( status_donen, pompa_tabancalar )
                            get_byte_request ( arabirim_base_object.pool_end, port_)
                            if c:
                                break
                except:
                    pass
        return True
    def arabirim_saat_guncelle(self, pompa_arabirim_object, pompa_tabancalar, pompa_formatla):
        c_time = time.strftime('%y%m%d%H%M%S')
        status_donen = []
        for arabirim_base_object, arabirim_tabanca_sub_object in pompa_arabirim_object.iteritems():
            
            port_ = self.ser[arabirim_base_object.port]
            
            if port_ != 0:
                #donen                  = []
                #try:
                if arabirim_base_object.ad == 'teosis':
                    
                    req_sorgu = arabirim_base_object.req_update_time % ( hex(arabirim_base_object.arabirim_adres)[2:], c_time )
                    req_sorgu = req_sorgu + arabirim_base_object.crc_end % getCRC16( req_sorgu )
                    #get_byte_request ( arabirim_base_object.pool_end, port_)
                    get_byte_request_by_query( req_sorgu, port_ )
                    #print req_sorgu,'************************'
                    
                    status_donen.append (get_byte_request_by_query( arabirim_base_object.pool, port_ ))
                    #status_donen.extend (get_byte_request_by_query( arabirim_base_object.pool, port_ ))
                    
                    get_byte_request ( arabirim_base_object.pool_end, port_)
                #except:
        #print status_donen
        return True
    def get_probe_query_by_type(self, probe_property ):
        
        string_query = []
        if int(probe_property.ID_PROBE_MARKA) == sabitler.PROBE_ID_MARKA.START_ITALIANA_XMT_SI_485:
            string_query.append('M' + str( probe_property.PROBE_ADRES ))
            
        if int(probe_property.ID_PROBE_MARKA) == sabitler.PROBE_ID_MARKA.TEOSIS:
            probe_query_formatter = sabitler.PROBE_ID_MARKA.probe_id_formatter_dict[ probe_property.ID_PROBE_MARKA ]
            query_final           = probe_query_formatter.query%( probe_property.PROBE_ADRES )
            byte_query_final      = get_bytearray_str( query_final )
            string_query.append( byte_query_final )
            
        if int(probe_property.ID_PROBE_MARKA) == sabitler.PROBE_ID_MARKA.HSM:
            hex_str         = 'A54' + str( probe_property.PROBE_ADRES )  +'00'
            valid_crc       = getCRC16( '0D' + hex_str, False )
            string_query.append('\r' + str(hex_str) + str(valid_crc) + '\n')
            
            hex_str_sow         = 'A50' + str( probe_property.PROBE_ADRES )  +'90'
            valid_crc_sow       = getCRC16( '0D' + hex_str_sow, False )
            string_query.append('\r' + str(hex_str_sow) + str(valid_crc_sow) + '\n')
            
            hex_str_emp         = 'A50' + str( probe_property.PROBE_ADRES )  +'01'
            valid_crc_emp       = getCRC16( '0D' + hex_str_emp, False )
            string_query.append('\r' + str(hex_str_emp) + str(valid_crc_emp) + '\n')
            
            hex_str_emp         = 'A50' + str( probe_property.PROBE_ADRES )  +'01'
            valid_crc_emp       = getCRC16( '0D' + hex_str_emp, False )
            string_query.append('\r' + str(hex_str_emp) + str(valid_crc_emp) + '\n')
            
            hex_str_emp         = 'A50' + str( probe_property.PROBE_ADRES )  +'01'
            valid_crc_emp       = getCRC16( '0D' + hex_str_emp, False )
            string_query.append('\r' + str(hex_str_emp) + str(valid_crc_emp) + '\n')
            
            
        if int( probe_property.ID_PROBE_MARKA ) == sabitler.PROBE_ID_MARKA.TSAV3:
            probe_query_formatter = sabitler.PROBE_ID_MARKA.probe_id_formatter_dict[ probe_property.ID_PROBE_MARKA ]
            query_final = probe_query_formatter.query%( probe_property.PROBE_ADRES,probe_query_formatter.start_byte - int( probe_property.PROBE_ADRES)  )
            byte_qeury_final = get_bytearray_str( query_final )
            string_query.append( byte_qeury_final )
            
        ##print string_query
        return string_query
    
class PPU:
    def update_ppu(self, port_, tabanca_object_list, value, orpak_scu_mesaj = None, orpak_pir_mesaj=None):
        #print value
        request_formatter    = PompaRequestFormatla()
        pooling_request_dict = request_formatter.pooling_request_by_tabanca( tabanca_object_list, value )
        donen = 0
    
        for tabanca_id, req in pooling_request_dict.iteritems():
            if req.ad == 'teosis':
                get_byte_request(req.req_ppu, port_)
            elif req.ad == 'orpak':
                
                req_str = req.final_formatter( req.req_ppu, orpak_scu_mesaj, orpak_pir_mesaj,req  )
                #print req_str
                donen = get_byte_request_by_query(req_str, port_)
                    
                    
                #time.sleep( sabitler.arabirimSorguSuresi )
        else:
            print ' q empty'
        
        return donen
class Records:
    def get_satis_record_by_tabanca(self, tabanca_object ):
        pass
'''
class Endeks:
    def get_endeks(self, com_q, tabanca_object_list):
           
        request_formatter    = PompaRequestFormatla()
        pooling_request_dict = request_formatter.pooling_request_by_tabanca( tabanca_object_list )
        totalizer_donen_dict = {}
        if not com_q.empty():
            port_dict = com_q.get()
            com_q.put( port_dict )
            for tabanca_id, req in pooling_request_dict.iteritems():
                #print req.req_totalizer
                port_ = port_dict[ req.ID_PORT ]
                get_byte_request(req.pool_end, port_)
                get_byte_request(req.req_totalizer, port_)
                totalizer_donen = get_byte_request_by_query(req.pool, port_)
                get_byte_request(req.pool_end, port_)
                totalizer_donen_dict[ tabanca_id ] = totalizer_donen
        else:
            #print ' q empty'
            
        return totalizer_donen_dict
'''
class ResponseFormat:
    def __init__(self,tankKalibrasyonQueueContainer = None,tankMinMaxQueueContainer = None):
        self.HSM_speed_of_wire_dict = {}
        self.tankKalibrasyonQueueContainer = tankKalibrasyonQueueContainer
        self.tankMinMaxQueueContainer      = tankMinMaxQueueContainer
    def ProbeResponseFormat( self, response, probe_property, simulation = False):
        yakit_litre    = 0
        su_lt          = 0
        su_mm          = 0
        yakit_sicaklik = 0
        yakit_mm       = 0
        yakit_yuzde    = 0
        su_yuzde       = 0
        offset         = probe_property.PROBE_BASLANGIC_SEVIYE
        ###buraya bir bak###
        su_alarm          = Dummy()
        su_alarm.text     = ""
        su_alarm.rengi    = wx.Colour(255,255,255)
        yakit_alarm       = Dummy()
        yakit_alarm.text  = ""
        yakit_alarm.rengi = wx.Colour(255,255,255)
       
        if not offset:
            offset     = 0
        yakit_by_total_yuzde = 0
        formatted_response = Dummy()
        formatted_response.su_mm = 0
        formatted_response.yakit_mm = 0
        formatted_response.yakit_sicaklik = 0
        if response or simulation:
            formatted_response_donen = self.string_formatter_by_probe_marka( response,probe_property,simulation )
            if formatted_response_donen:
                formatted_response = formatted_response_donen
                if formatted_response.yakit_mm != 0:
                    yakit_mm = formatted_response.yakit_mm + offset
                    if formatted_response.su_mm != 0:
                        su_mm = formatted_response.su_mm + offset
                    
                    yakit_litre_yukseklige_bagli = cal.getCalibrationByTankMm(probe_property.ID_YAKIT_TANKI  , yakit_mm,self.tankKalibrasyonQueueContainer )
                    su_lt                        = cal.getCalibrationByTankMm(probe_property.ID_YAKIT_TANKI  , su_mm,self.tankKalibrasyonQueueContainer   )
                    yakit_litre                  = yakit_litre_yukseklige_bagli - su_lt
                    yakit_yuzde, su_yuzde, yakit_by_total_yuzde = getTankYuzdeHesabi(probe_property.YAKIT_TANKI_KAPASITE, yakit_litre, su_lt )
                    su_alarm,yakit_alarm         = getTankMinMaxAlarmHesabi(su_mm,yakit_mm,probe_property.ID_YAKIT_TANKI,self.tankMinMaxQueueContainer)
        probe_object = Dummy()

        probe_object.yakit_litre          = yakit_litre
        probe_object.su_mm                = su_mm
        probe_object.su_lt                = su_lt
        probe_object.yakit_mm             = yakit_mm
        probe_object.yakit_sicaklik       = formatted_response.yakit_sicaklik
        probe_object.yakit_yuzde          = yakit_yuzde
        probe_object.su_yuzde             = su_yuzde
        probe_object.yakit_by_total_yuzde = yakit_by_total_yuzde
        probe_object.yakit_tank_id        = probe_property.ID_YAKIT_TANKI
        probe_object.su_alarm             = su_alarm
        probe_object.yakit_alarm          = yakit_alarm
        probe_object.islem_type           = []
        

        return probe_object
    def string_formatter_by_probe_marka( self, response, probe_property, simulation ):
        formatter_obj     = sabitler.PROBE_ID_MARKA.probe_id_formatter_dict[ int(probe_property.ID_PROBE_MARKA) ]
        formatted_object  = Dummy()
        formatted_object.durum           = 0
        formatted_object.yakit_sicaklik  = 0
        formatted_object.yakit_mm        = 0
        formatted_object.su_mm           = 0
        if simulation:
            carpan                           = probe_property.YAKIT_TANKI_KAPASITE 
            formatted_object.durum           = sabitler.PROBE_ID_MARKA.SIMULATION.durum
            formatted_object.yakit_sicaklik  = sabitler.PROBE_ID_MARKA.SIMULATION.sicaklik
            formatted_object.yakit_mm        = float(probe_property.PROBE_UZUNLUK) - (float(sabitler.PROBE_ID_MARKA.SIMULATION.yakit_sabit)*(float(carpan))) / float(sabitler.PROBE_ID_MARKA.SIMULATION.yakit_bolen)
            formatted_object.su_mm           = float(probe_property.PROBE_UZUNLUK) - (float(sabitler.PROBE_ID_MARKA.SIMULATION.su_sabit)*(float(carpan))) / float(sabitler.PROBE_ID_MARKA.SIMULATION.su_bolen)
        
        elif probe_property.ID_PROBE_MARKA == sabitler.PROBE_ID_MARKA.START_ITALIANA_XMT_SI_485:
            response_splitted = response.split( formatter_obj.ayrac )
            formatted_object.durum           = response_splitted[ formatter_obj.durum ]
            formatted_object.yakit_sicaklik  = float(response_splitted[ formatter_obj.sicaklik ]) / formatter_obj.sicaklik_multiplier 
            formatted_object.yakit_mm        = float(response_splitted[ formatter_obj.yakit_mm ]) / formatter_obj.yakit_multiplier 
            formatted_object.su_mm           = float(response_splitted[ formatter_obj.su_mm ])    / formatter_obj.su_multiplier
            
        elif probe_property.ID_PROBE_MARKA == sabitler.PROBE_ID_MARKA.TEOSIS:
            response_splitted = response.split( formatter_obj.ayrac )
            formatted_object.yakit_sicaklik  = float(response_splitted[ formatter_obj.sicaklik ].strip()[0:2]+'.'+response_splitted[ formatter_obj.sicaklik ][2].strip()[3:4])
            formatted_object.yakit_mm        = float(response_splitted[ formatter_obj.yakit_mm ].strip()[5:12])
            formatted_object.su_mm           = float(response_splitted[ formatter_obj.su_mm ].strip())
            
        elif probe_property.ID_PROBE_MARKA == sabitler.PROBE_ID_MARKA.HSM:
            check_str = ''.join( response[formatter_obj.baslangic_bitis_byte:len(response)-formatter_obj.baslangic_bitis_byte] ).decode( 'hex' )
            response_type = check_str[formatter_obj.response_type_index[0]:formatter_obj.response_type_index[1]]
            ##print check_str
            if response_type == formatter_obj.response_sow:
                speed_of_wire = int(check_str[formatter_obj.response_sow_index[0]:formatter_obj.response_sow_index[1]].decode('hex')) * formatter_obj.sow_multiplier
                self.HSM_speed_of_wire_dict[ probe_property.ID_PROBE] = speed_of_wire

            if response_type == formatter_obj.response_all:
                if self.HSM_speed_of_wire_dict.has_key( probe_property.ID_PROBE  ):
                    speed_of_wire = self.HSM_speed_of_wire_dict[ probe_property.ID_PROBE ]
                    
                    formatted_object.durum           = int(check_str[formatter_obj.status[0]:formatter_obj.status[1]], 16)
                    ##print 'durum  : ', formatted_object.durum
                    formatted_object.yakit_sicaklik  = int(check_str[formatter_obj.temprature[0]:formatter_obj.temprature[1]], 16)
                    
                    ##print 'isi  : ', formatted_object.yakit_sicaklik
                    formatted_object.yakit_mm        = float(probe_property.PROBE_UZUNLUK) - (float(int(check_str[formatter_obj.fuel_sensor_index[0]:formatter_obj.fuel_sensor_index[1]], 16))*(float(speed_of_wire))) / formatter_obj.HSM_bolen
                    ##print ' yakit_mm  : ',formatted_object.yakit_mm
                    formatted_object.su_mm           = float(probe_property.PROBE_UZUNLUK) - (float(int(check_str[formatter_obj.water_sensor_index[0]:formatter_obj.water_sensor_index[1]], 16))*(float(speed_of_wire))) / formatter_obj.HSM_bolen
                    ##print 'su_mm  : ',formatted_object.su_mm
                
        elif probe_property.ID_PROBE_MARKA == sabitler.PROBE_ID_MARKA.TSAV3:
            
            str_donen = ''.join( response )
            if len( str_donen ) >= formatter_obj.isi_end:
                formatted_object.yakit_mm = int(str_donen[formatter_obj.yakit_start : formatter_obj.yakit_end ], 16)  / float( formatter_obj.yakit_bolen )
                formatted_object.su_mm    = int(str_donen[formatter_obj.yakit_end : formatter_obj.su_end ], 16)  / float( formatter_obj.su_bolen )
                formatted_object.yakit_sicaklik    = int(str_donen[formatter_obj.su_end : formatter_obj.isi_end ], 16)  / float( formatter_obj.isi_bolen )
        return formatted_object
class HTTPSudsPreprocessor(urllib2.BaseHandler):

    def http_request(self, req):
        base64string = sabitler.base64string
        req.add_header('Content-Type', 'text/xml; charset=utf-8')
        req.add_header('TAURUS',"Basic %s" % base64string)
        return req

    https_request = http_request

class EpdkSoap:
   
    def makeRequest(self,wsdl_url,url,kwargs,attr):
        
        imp = Import(sabitler.imp_url)
        imp.filter.add(url)
        client = Client(wsdl_url, doctor=ImportDoctor(imp))
        return getattr(client.service,attr)(**kwargs)
    
    def getDagiticiYururlukte( self ):
        
        url      = sabitler.epdk_ws_url
        wsdl_url = sabitler.epdk_ws_wsdl_url
        kwargs   = {
                     'lisansDurumu':'ONAYLANDI'              
                   }
        attr     = 'petrolDagiticiLisansSorgula'
        data     = self.makeRequest(wsdl_url = wsdl_url, url = url, kwargs = kwargs, attr = attr)
        
        return data
    
    def getDagiticiyaBagliBayilikListesi (self,dagitici_lisans_no_):

        dagitici_lisans_no = dagitici_lisans_no_#).decode('utf-8')
        url      = sabitler.epdk_ws_url
        wsdl_url = sabitler.epdk_ws_wsdl_url
        kwargs   = {
                     'lisansDurumu':'ONAYLANDI' ,
                     'lisansNo':'%s'%dagitici_lisans_no
                   }
        attr     = 'getPetrolDagiticiyaBagliBayilikListesi'
        data     = self.makeRequest(wsdl_url = wsdl_url, url = url, kwargs = kwargs, attr = attr)

        return data
        
    def epdk_ws_sifre_ile_giris_test (self,dagitici_lisans_no_):
        
        dagitici_lisans_no = dagitici_lisans_no_
        url      = 'https://ppdbo.epdk.org.tr/test/Data.asmx/'
        wsdl_url = 'https://ppdbo.epdk.org.tr/test/Data.asmx?WSDL'
        kwargs   = {
                     'DagiticiLisansNo':'DA�/4241-1/32067' ,
                     'Sifre':'32067'
                   }
        attr     = 'SLogin'
        data     = self.makeRequest(wsdl_url = wsdl_url, url = url, kwargs = kwargs, attr = attr)
        #print data

class WebSoap:
    def __init__(self):
        
        self.webClient = None
        self.url      = sabitler.web_soap_url
        self.wsdl_url = sabitler.web_soap_wsdl_url
        
    def getClient(self,url ,wsdl_url):
        imp = Import(sabitler.imp_url)
        imp.filter.add(url)    
        https  = suds.transport.https.HttpTransport()
        opener = urllib2.build_opener(HTTPSudsPreprocessor)
        https.urlopener = opener
        #self.webClient                  = Client(wsdl_url, doctor=ImportDoctor(imp), transport = https)
        
        if not self.webClient:
            #print 'no cache'
            self.webClient                  = Client(wsdl_url, doctor=ImportDoctor(imp), transport = https)
        else:
            #print 'cache'
            pass
        #print self.webClient
        return self.webClient  
    
    def makeRequestParameterDegerGonder(self,wsdl_url,url,kwargs,attr):

        client = self.getClient(url ,wsdl_url)
        client.options.cache.clear()

        master                  = client.factory.create("ns0:MasterArgs")
        p_dagiticis_            = client.factory.create("ns0:P_Dagitici_Array")
        p_bayis_                = client.factory.create("ns0:P_Bayi_Array")
        tabancalars_            = client.factory.create("ns0:Tabancalar_Array")
        yakits_                 = client.factory.create("ns0:Yakit_Array")
        yakit_tankis_           = client.factory.create("ns0:Yakit_Tanki_Array")
        probes_                 = client.factory.create("ns0:Probe_Array")
        ports_                  = client.factory.create("ns0:Port_Array")
        pompas_                 = client.factory.create("ns0:Pompa_Array")
        p_otomasyon_parametres_ = client.factory.create("ns0:P_Otomasyon_Parametre_Array")
        
        dagitici_ws_array              = []
        bayi_ws_array                  = []
        tabancalar_ws_array            = []
        yakitlar_ws_array              = []
        yakit_ws_array                 = []
        yakit_tanki_ws_array           = []
        probe_ws_array                 = []
        port_ws_array                  = []
        pompa_ws_array                 = []
        p_otomasyon_parametre_ws_array = []
        stringArray  = client.factory.create("ns0:SetClientParameters")
        for key,value in kwargs.iteritems():
        
            if key == 'P_Dagitici':
                for dagitici in value:
                    p_dagitici = client.factory.create("ns0:P_Dagitici_")
                    for k,v in dagitici.iteritems():
                        setattr(p_dagitici,k,v)
                    dagitici_ws_array.append( p_dagitici )
                    
            if key == 'P_Bayi':
                for bayi in value:
                    p_bayi = client.factory.create("ns0:P_Bayi_")
                    for k,v in bayi.iteritems():
                        setattr(p_bayi,k,v)
                    bayi_ws_array.append( p_bayi )
                    
            if key == 'Tabancalar':
                for tabanca in value:
                    tabancalar = client.factory.create("ns0:Tabancalar_")
                    for k,v in tabanca.iteritems():
                        setattr(tabancalar,k,v)
                    tabancalar_ws_array.append( tabancalar )
                    
            if key == 'Yakit':
                for yakit in value:
                    yakitlar = client.factory.create("ns0:Yakit_")
                    for k,v in yakit.iteritems():
                        setattr(yakitlar,k,v)
                    yakitlar_ws_array.append( yakitlar )
                        
            if key == 'Yakit_Tanki':
                for yakit_tanki_ in value:
                    yakit_tanki = client.factory.create("ns0:Yakit_Tanki_")
                    for k,v in yakit_tanki_.iteritems():
                        setattr(yakit_tanki,k,v)
                    yakit_tanki_ws_array.append( yakit_tanki )
                    
            if key == 'Probe':
                for probe_ in value:
                    probe = client.factory.create("ns0:Probe_")
                    for k,v in probe_.iteritems():
                        setattr(probe,k,v)
                    probe_ws_array.append( probe )           
            
            if key == 'Portlar':
                for port_ in value:
                    port = client.factory.create("ns0:Port_")
                    for k,v in port_.iteritems():
                        setattr(port,k,v)
                    port_ws_array.append( port )
                    
            if key == 'Pompa':
                for pompa_ in value:
                    pompa = client.factory.create("ns0:Pompa_")
                    for k,v in pompa_.iteritems():
                        setattr(pompa,k,v)
                    pompa_ws_array.append( pompa )
                    
            if key == 'P_Otomasyon_Parametre':
                for otomasyon_parametre in value:
                    p_otomasyon_parametre = client.factory.create("ns0:P_Otomasyon_Parametre_")
                    for k,v in otomasyon_parametre.iteritems():
                        setattr(p_otomasyon_parametre,k,v)
                    p_otomasyon_parametre_ws_array.append( p_otomasyon_parametre )
            
            if key == 'Bayi_No':
                bayi_no_tag = client.factory.create("xs:string")
                bayi_no_tag = value
                stringArray.item  = {'string':[bayi_no_tag]}
                
        p_dagiticis_.P_Dagitici_                       = dagitici_ws_array
        p_bayis_.P_Bayi_                               = bayi_ws_array
        tabancalars_.Tabancalar_                       = tabancalar_ws_array
        yakits_.Yakit_                                 = yakitlar_ws_array
        yakit_tankis_.Yakit_Tanki_                     = yakit_tanki_ws_array
        probes_.Probe_                                 = probe_ws_array
        ports_.Port_                                   = port_ws_array
        pompas_.Pompa_                                 = pompa_ws_array
        p_otomasyon_parametres_.P_Otomasyon_Parametre_ = p_otomasyon_parametre_ws_array
        
        master.p_dagiticis            = p_dagiticis_
        master.p_bayis                = p_bayis_
        master.tabancalars            = tabancalars_
        master.yakits                 = yakits_
        master.yakit_tankis           = yakit_tankis_
        master.probes                 = probes_
        master.ports                  = ports_
        master.pompas                 = pompas_
        master.p_otomasyon_parametres = p_otomasyon_parametres_
        try :
            return getattr(client.service,attr)(master,stringArray)
        except suds.WebFault, hata:
            hata_message =  'HATA:"%s" hatas�.\nHata Kodu:"%s"'%(hata.fault.faultcode,hata.fault.faultstring)
            return hata_message
        except:
            hata_message =  'GENEL HATA:"%s" hatas�.'%( sys.exc_info()[0])
            return hata_message
       
    def makeRequestParameterDegerAl(self,wsdl_url,url,kwargs,attr):
        
       
        client = self.getClient( url ,wsdl_url )
        stringArray  = client.factory.create("ns0:SetClientParameters")
        array_ws_tanimlamalar = []

        for key in kwargs:
            ws_tag = client.factory.create("xs:string")
            ws_tag = key
            array_ws_tanimlamalar.append(ws_tag)
            
        stringArray.item        = {'string':array_ws_tanimlamalar}   
        
        try :
            return getattr(client.service,attr)(stringArray)
        except suds.WebFault, hata:
            hata_message =  'HATA:"%s" hatas�.\nHata Kodu:"%s"'%(hata.fault.faultcode,hata.fault.faultstring)
            return hata_message
        except:
            hata_message =  'GENEL HATA:"%s" hatas�.'%( sys.exc_info()[0])
            return hata_message
    
    def getParameterDeger( self,yon,kwargs = '' ):
        
        if yon == 'gonder':
            attr     = 'GetOtoDeskSettingsPackage'
            data     = self.makeRequestParameterDegerGonder(wsdl_url = self.wsdl_url, url = self.url, kwargs = kwargs, attr = attr)
            
        elif yon == 'al':
            attr     = 'SetOtoDeskSettingsPackage'
            data     = self.makeRequestParameterDegerAl(wsdl_url = self.wsdl_url, url = self.url, kwargs = kwargs, attr = attr)
        
        return data
    
    def tankKayitlariGonder (self,kwargs='',bayi_lisans=''):
        
        #ns12  = "Tank_Kayitlari_"
        client = self.getClient(self.url ,self.wsdl_url)
        
        #client.options.cache.clear()
        tank_kayit_array = []
        master                  = client.factory.create("ns0:TankKayitlariArgs")
        tank_kayit_ws_array     = client.factory.create("ns0:Tank_Kayitlari_Array")
        
        for tank_kayit in kwargs:
            tank_kayit_ws = client.factory.create("ns0:Tank_Kayitlari_")
            for k,v in tank_kayit.iteritems():
                setattr(tank_kayit_ws,k,v)
            tank_kayit_array.append( tank_kayit_ws )
        
        stringArray  = client.factory.create("ns0:SetClientParameters")
        
        bayi_no_tag = client.factory.create("xs:string")
        bayi_no_tag = bayi_lisans
        stringArray.item  = {'string':[bayi_no_tag]}
        
        tank_kayit_ws_array.Tank_Kayitlari_ = tank_kayit_array
        master.get_tank_kayitlari           = tank_kayit_ws_array
        attr = 'GetTankKayitlari' 
        return_object = getattr(client.service,attr)( master, stringArray)
        return return_object
    
    
    def yakithistoryKayitlariGonder (self,kwargs='',bayi_lisans=''):
        
        #ns12  = "Tank_Kayitlari_"
        client = self.getClient(self.url ,self.wsdl_url)
        
        #client.options.cache.clear()
        history_kayitlari = []
        master                     = client.factory.create("ns0:YakitHistoryArgs")
        history_kayit_ws_array     = client.factory.create("ns0:Yakit_History_Array")
        
        for history_kayit in kwargs:
            history_kayit_ws = client.factory.create("ns0:Yakit_History_")
            for k,v in history_kayit.iteritems():
                setattr(history_kayit_ws,k,v)
            history_kayitlari.append( history_kayit_ws )
        
        stringArray  = client.factory.create("ns0:SetClientParameters")
        
        bayi_no_tag = client.factory.create("xs:string")
        bayi_no_tag = bayi_lisans
        stringArray.item  = {'string':[bayi_no_tag]}
        
        history_kayit_ws_array.Yakit_History_ = history_kayitlari
        master.get_yakit_history = history_kayit_ws_array
        attr = 'GetYakitHistory' 
        
        return_object = getattr(client.service,attr)( master, stringArray)
        
        return return_object

    def servisKayitlariGonder (self,kwargs='',bayi_lisans=''):
        
        client = self.getClient(self.url ,self.wsdl_url)     
        client.options.cache.clear()
        servis_kayit_array = []
        master                  = client.factory.create("ns0:ServisKayitlariArgs")
        servis_kayit_ws_array     = client.factory.create("ns0:Servis_Kayitlari_Array")
        
        for servis_kayit in kwargs:
            servis_kayit_ws = client.factory.create("ns0:Servis_Kayitlari_")
            for k,v in servis_kayit.iteritems():
                setattr(servis_kayit_ws,k,v)
            servis_kayit_array.append( servis_kayit_ws )
        
        stringArray  = client.factory.create("ns0:SetClientParameters")
        
        bayi_no_tag = client.factory.create("xs:string")
        bayi_no_tag = bayi_lisans
        stringArray.item  = {'string':[bayi_no_tag]}
        
        servis_kayit_ws_array.Servis_Kayitlari_ = servis_kayit_array
        master.get_servis_kayitlari = servis_kayit_ws_array
        attr = 'GetServisKayitlari'       
        return_object = getattr(client.service,attr)( master, stringArray)
        
        return return_object
    ####cari hesap kayitlarini gonder su an icin iptal edildi####
    def cariHesaplariGonder(self,kwargs='',bayi_lisans=''):
        
        client = self.getClient(self.url ,self.wsdl_url)     
        client.options.cache.clear()
        cari_hesap_kayit_array    = []
        master                    = client.factory.create("ns0:CariHesapKayitlariArgs")
        cari_hesap_kayit_ws_array = client.factory.create("ns0:Cari_Hesap_Kayitlari_Array")

        for cari_hesap_kayit in kwargs:
            cari_hesap_kayit_ws = client.factory.create("ns0:Cari_Hesap_Kayitlari_")
            for k,v in cari_hesap_kayit.iteritems():
                setattr(cari_hesap_kayit_ws,k,v)
            cari_hesap_kayit_array.append( cari_hesap_kayit_ws )
        stringArray  = client.factory.create("ns0:SetClientParameters")
        bayi_no_tag                                     = client.factory.create("xs:string")
        bayi_no_tag                                     = bayi_lisans
        stringArray.item                                = {'string':[bayi_no_tag]}
        cari_hesap_kayit_ws_array.Cari_Hesap_Kayitlari_ = cari_hesap_kayit_array
        master.get_cari_hesap_kayitlari                 = cari_hesap_kayit_ws_array
        ##print master
        attr = 'GetCariHesapKayitlari' 
        
        return_object = getattr(client.service,attr)( master, stringArray)
        return return_object
    ####kart kayitlarini gonder su an icin iptal edildi####
    def kartKayitlariGonder(self,kwargs='',bayi_lisans='' ):

        client = self.getClient(self.url ,self.wsdl_url)     
        client.options.cache.clear()
        kart_kayit_array    = []
        master              = client.factory.create("ns0:KartKayitlariArgs")
        kart_kayit_ws_array = client.factory.create("ns0:Kart_Kayitlari_Array")

        for kart_kayit in kwargs:
            kart_kayit_ws = client.factory.create("ns0:Kart_Kayitlari_")
            for k,v in kart_kayit.iteritems():
                setattr(kart_kayit_ws,k,v)
            kart_kayit_array.append( kart_kayit_ws )

        stringArray  = client.factory.create("ns0:SetClientParameters")
        
        bayi_no_tag = client.factory.create("xs:string")
        bayi_no_tag = bayi_lisans
        stringArray.item  = {'string':[bayi_no_tag]}
        kart_kayit_ws_array.Kart_Kayitlari_ = kart_kayit_array
        master.get_kart_kayitlari = kart_kayit_ws_array
        ##print master
        attr = 'GetKartKayitlari' 
        
        return_object = getattr(client.service,attr)( master, stringArray)
        return return_object

    def irsaliyeKayitlariAl(self,bayi_lisans=''):
        client = self.getClient( self.url ,self.wsdl_url )
        stringArray  = client.factory.create("ns0:SetClientParameters")
        bayi_no_tag = client.factory.create("xs:string")
        bayi_no_tag = bayi_lisans
        stringArray.item  = {'string':[bayi_no_tag]}
        attr = 'SetIrsaliyeKayitlari'  
        return  getattr(client.service,attr)(stringArray)
    
    def iskontoKayitlariAl(self,bayi_lisans=''):
        
        client = self.getClient( self.url ,self.wsdl_url )
        stringArray  = client.factory.create("ns0:SetClientParameters")
        bayi_no_tag = client.factory.create("xs:string")
        bayi_no_tag = bayi_lisans
        stringArray.item  = {'string':[bayi_no_tag]}
        attr = 'SetIskontoKayitlari'  
        return  getattr(client.service,attr)(stringArray)
    
    def iskontoYakitKayitlariAl(self,bayi_lisans=''):
        
        client = self.getClient( self.url ,self.wsdl_url )
        stringArray  = client.factory.create("ns0:SetClientParameters")
        bayi_no_tag = client.factory.create("xs:string")
        bayi_no_tag = bayi_lisans
        stringArray.item  = {'string':[bayi_no_tag]}
        attr = 'SetYakitIskontoKayitlari'  
        return  getattr(client.service,attr)(stringArray)
    
    def cariHesaplariAl(self,bayi_lisans=''):
        client = self.getClient( self.url ,self.wsdl_url )
        stringArray  = client.factory.create("ns0:SetClientParameters")
        bayi_no_tag = client.factory.create("xs:string")
        bayi_no_tag = bayi_lisans
        stringArray.item  = {'string':[bayi_no_tag]}
        attr = 'SetCariHesapKayitlari'  
        return  getattr(client.service,attr)(stringArray)

    def cariKartlariAl(self,bayi_lisans=''):
        client = self.getClient( self.url ,self.wsdl_url )
        stringArray  = client.factory.create("ns0:SetClientParameters")
        bayi_no_tag = client.factory.create("xs:string")
        bayi_no_tag = bayi_lisans
        stringArray.item  = {'string':[bayi_no_tag]}
        attr = 'SetKartKayitlari'  
        return  getattr(client.service,attr)(stringArray)
    
    def TasitTanimaKayitlariAl(self,bayi_lisans=''):
        client = self.getClient( self.url ,self.wsdl_url )
        stringArray  = client.factory.create("ns0:SetClientParameters")
        bayi_no_tag = client.factory.create("xs:string")
        bayi_no_tag = bayi_lisans
        stringArray.item  = {'string':[bayi_no_tag]}
        attr = 'SetTasitTanimaKayitlari'  
        return  getattr(client.service,attr)(stringArray)
    
    def tasitTanimaOkGonder(self,kwargs='',bayi_lisans='' ):

        client = self.getClient(self.url ,self.wsdl_url)     
        client.options.cache.clear()
        tasit_tanima_ok_kayit_array = []
        master                      = client.factory.create("ns0:TasitTanimaKayitlariOkArgs")
        tasit_tanima_ok_kayit_ws_array = client.factory.create("ns0:Tasit_Tanima_Kayitlari_Ok_Array")

        for tasit_tanima_id,ws_durum in kwargs.iteritems():
            tasit_tanima_ok_kayit_ws = client.factory.create("ns0:Tasit_Tanima_Kayitlari_Ok_")
            tasit_tanima_ok_kayit_ws.ID_TASIT_TANIMA_HISTORY = int(tasit_tanima_id)
            tasit_tanima_ok_kayit_ws.WS_DURUM                = int(ws_durum)     
            tasit_tanima_ok_kayit_array.append( tasit_tanima_ok_kayit_ws )

        stringArray  = client.factory.create("ns0:SetClientParameters")
        
        bayi_no_tag = client.factory.create("xs:string")
        bayi_no_tag = bayi_lisans
        stringArray.item  = {'string':[bayi_no_tag]}
        
        tasit_tanima_ok_kayit_ws_array.Tasit_Tanima_Kayitlari_Ok_ = tasit_tanima_ok_kayit_array
        master.get_tasit_tanima_ok = tasit_tanima_ok_kayit_ws_array
        attr = 'GetTasitTanimaKayitlariOkKayitlari'       
        gonder_tasit_tanima_ok = getattr(client.service,attr)( master, stringArray)        
        
    def irsaliyeOkGonder(self,kwargs='',bayi_lisans='' ):

        client = self.getClient(self.url ,self.wsdl_url)     
        client.options.cache.clear()
        irsaliye_ok_kayit_array = []
        master                     = client.factory.create("ns0:IrsaliyeKayitlariOkArgs")
        irsaliye_ok_kayit_ws_array = client.factory.create("ns0:Irsaliye_Kayitlari_Ok_Array")

        for irsaliye_id,gond_tarih in kwargs.iteritems():
            irsaliye_ok_kayit_ws = client.factory.create("ns0:Irsaliye_Kayitlari_Ok_")
            irsaliye_ok_kayit_ws.ID_IRSALIYE        = int(irsaliye_id)
            irsaliye_ok_kayit_ws.GONDERILDIGI_TARIH = str(gond_tarih)     
            irsaliye_ok_kayit_array.append( irsaliye_ok_kayit_ws )

        stringArray  = client.factory.create("ns0:SetClientParameters")
        
        bayi_no_tag = client.factory.create("xs:string")
        bayi_no_tag = bayi_lisans
        stringArray.item  = {'string':[bayi_no_tag]}
        
        irsaliye_ok_kayit_ws_array.Irsaliye_Kayitlari_Ok_ = irsaliye_ok_kayit_array
        master.get_irsaliye_ok = irsaliye_ok_kayit_ws_array
        attr = 'GetIrsaliyeOkKayitlari'       
        gonder_irsaliye_ok = getattr(client.service,attr)( master, stringArray)

    def satisKayitlariGonder (self,kwargs='',bayi_lisans=''):
        
        client = self.getClient(self.url ,self.wsdl_url) 
        client.options.cache.clear()
        satis_kayit_array = []
        master               = client.factory.create("ns0:SatisKayitlariArgs")
        satis_kayit_ws_array = client.factory.create("ns0:Satis_Kayitlari_Array")
        for satis_kayit in kwargs:
            satis_kayit_ws = client.factory.create("ns0:Satis_Kayitlari_")
            ##print dir( satis_kayit_ws )
            for k,v in satis_kayit.iteritems():
                setattr(satis_kayit_ws,k,v)
            satis_kayit_array.append( satis_kayit_ws )
        
        stringArray  = client.factory.create("ns0:SetClientParameters")
        
        bayi_no_tag = client.factory.create("xs:string")
        bayi_no_tag = bayi_lisans
        stringArray.item  = {'string':[bayi_no_tag]}
        satis_kayit_ws_array.Satis_Kayitlari_ = satis_kayit_array
        master.get_satis_kayitlari = satis_kayit_ws_array
        ##print master
        attr = 'GetSatisKayitlari' 
        
        return_object = getattr(client.service,attr)( master, stringArray)
        
        return return_object
    
    def EpdkTablolariWebeGonder(self,kwargs='',bayi_lisans=''):

        client = self.getClient(self.url ,self.wsdl_url)
        client.options.cache.clear()
       
        epdk_tables   = client.factory.create("ns0:EpdkTablesArgs")
        tablo_1_      = client.factory.create("ns0:Epdk_Tablo1_Array")
        tablo_2_      = client.factory.create("ns0:Epdk_Tablo2_Array")
        tablo_3_      = client.factory.create("ns0:Epdk_Tablo3_Array")
        tablo_4_      = client.factory.create("ns0:Epdk_Tablo4_Array")
        tablo_5_      = client.factory.create("ns0:Epdk_Tablo5_Array")
        tablo_6_      = client.factory.create("ns0:Epdk_Tablo6_Array")
        tablo_7_      = client.factory.create("ns0:Epdk_Tablo7_Array")
        tablo_8_      = client.factory.create("ns0:Epdk_Tablo8_Array")
        tablo_9_      = client.factory.create("ns0:Epdk_Tablo9_Array")
        tablo_11_     = client.factory.create("ns0:Epdk_Tablo11_Array")
        tablo_12_     = client.factory.create("ns0:Epdk_Tablo12_Array")
        tablo_13_     = client.factory.create("ns0:Epdk_Tablo13_Array")
        tablo_14_     = client.factory.create("ns0:Epdk_Tablo14_Array")
        tablo_15_     = client.factory.create("ns0:Epdk_Tablo15_Array")
        tablo_16_     = client.factory.create("ns0:Epdk_Tablo16_Array")
        tablo_17_     = client.factory.create("ns0:Epdk_Tablo17_Array")
        tablo_18_     = client.factory.create("ns0:Epdk_Tablo18_Array")
        
        tablo1_ws_array   = []
        tablo2_ws_array   = []
        tablo3_ws_array   = []
        tablo4_ws_array   = []
        tablo5_ws_array   = []
        tablo6_ws_array   = []
        tablo7_ws_array   = []
        tablo8_ws_array   = []
        tablo9_ws_array   = []
        tablo11_ws_array  = []
        tablo12_ws_array  = []
        tablo13_ws_array  = []
        tablo14_ws_array  = []
        tablo15_ws_array  = []
        tablo16_ws_array  = []
        tablo17_ws_array  = []
        tablo18_ws_array  = []
        
        stringArray  = client.factory.create("ns0:SetClientParameters")
        for key,value in kwargs.iteritems():
            
            if key == 'EPDK_TABLO1':
                for tablo1 in value:
                    epdk_tablo1 = client.factory.create("ns0:Epdk_Tablo1_")
                    for k,v in tablo1.iteritems():
                        setattr(epdk_tablo1,k,v)
                    tablo1_ws_array.append( epdk_tablo1 )
             
            if key == 'EPDK_TABLO2':
                for tablo2 in value:
                    epdk_tablo2 = client.factory.create("ns0:Epdk_Tablo2_")
                    for k,v in tablo2.iteritems():
                        setattr(epdk_tablo2,k,v)
                    tablo2_ws_array.append( epdk_tablo2 )
                    
            if key == 'EPDK_TABLO3':
                for tablo3 in value:
                    epdk_tablo3 = client.factory.create("ns0:Epdk_Tablo3_")
                    for k,v in tablo3.iteritems():
                        setattr(epdk_tablo3,k,v)
                    tablo3_ws_array.append( epdk_tablo3 )
                    
            if key == 'EPDK_TABLO4':
                for tablo4 in value:
                    epdk_tablo4 = client.factory.create("ns0:Epdk_Tablo4_")
                    for k,v in tablo4.iteritems():
                        setattr(epdk_tablo4,k,v)
                    tablo4_ws_array.append( epdk_tablo4 )
                    
            if key == 'EPDK_TABLO5':
                for tablo5 in value:
                    epdk_tablo5 = client.factory.create("ns0:Epdk_Tablo5_")
                    for k,v in tablo5.iteritems():
                        setattr(epdk_tablo5,k,v)
                    tablo5_ws_array.append( epdk_tablo5 )
                    
            if key == 'EPDK_TABLO6':
                for tablo6 in value:
                    epdk_tablo6 = client.factory.create("ns0:Epdk_Tablo6_")
                    for k,v in tablo6.iteritems():
                        setattr(epdk_tablo6,k,v)
                    tablo6_ws_array.append( epdk_tablo6 )
                        
            if key == 'EPDK_TABLO7':
                for tablo7 in value:
                    epdk_tablo7 = client.factory.create("ns0:Epdk_Tablo7_")
                    for k,v in tablo7.iteritems():
                        setattr(epdk_tablo7,k,v)
                    tablo7_ws_array.append( epdk_tablo7 )
                  
            if key == 'EPDK_TABLO8':                
                for tablo8 in value:
                    epdk_tablo8 = client.factory.create("ns0:Epdk_Tablo8_")
                    for k,v in tablo8.iteritems():
                        setattr(epdk_tablo8,k,v)
                    tablo8_ws_array.append( epdk_tablo8 )       
            
            if key == 'EPDK_TABLO9':
                for tablo9 in value:
                    epdk_tablo9 = client.factory.create("ns0:Epdk_Tablo9_")
                    for k,v in tablo9.iteritems():
                        setattr(epdk_tablo9,k,v)
                    tablo9_ws_array.append( epdk_tablo9 )
                   
            if key == 'EPDK_TABLO11':
                for tablo11 in value:
                    epdk_tablo11 = client.factory.create("ns0:Epdk_Tablo11_")
                    for k,v in tablo11.iteritems():
                        setattr(epdk_tablo11,k,v)
                    tablo11_ws_array.append( epdk_tablo11 )
             
            if key == 'EPDK_TABLO12':
                for tablo12 in value:
                    epdk_tablo12 = client.factory.create("ns0:Epdk_Tablo12_")
                    for k,v in tablo12.iteritems():
                        setattr(epdk_tablo12,k,v)
                    tablo12_ws_array.append( epdk_tablo12 )

            if key == 'EPDK_TABLO13':
                for tablo13 in value:
                    epdk_tablo13 = client.factory.create("ns0:Epdk_Tablo13_")
                    for k,v in tablo13.iteritems():
                        setattr(epdk_tablo13,k,v)
                    tablo13_ws_array.append( epdk_tablo13 )
                  
            if key == 'EPDK_TABLO14':
                for tablo14 in value:
                    epdk_tablo14 = client.factory.create("ns0:Epdk_Tablo14_")
                    for k,v in tablo14.iteritems():
                        setattr(epdk_tablo14,k,v)
                    tablo14_ws_array.append( epdk_tablo14 )
                    
            if key == 'EPDK_TABLO15':
                for tablo15 in value:
                    epdk_tablo15 = client.factory.create("ns0:Epdk_Tablo15_")
                    for k,v in tablo15.iteritems():
                        setattr(epdk_tablo15,k,v)
                    tablo15_ws_array.append( epdk_tablo15 )
                    
            if key == 'EPDK_TABLO16':
                for tablo16 in value:
                    epdk_tablo16 = client.factory.create("ns0:Epdk_Tablo16_")
                    for k,v in tablo16.iteritems():
                        setattr(epdk_tablo16,k,v)
                    tablo16_ws_array.append( epdk_tablo16 )
            
            if key == 'EPDK_TABLO17':
                for tablo17 in value:
                    epdk_tablo17 = client.factory.create("ns0:Epdk_Tablo17_")
                    for k,v in tablo17.iteritems():
                        setattr(epdk_tablo17,k,v)
                    tablo17_ws_array.append( epdk_tablo17 )
                    
            if key == 'EPDK_TABLO18':
                for tablo18 in value:
                    epdk_tablo18 = client.factory.create("ns0:Epdk_Tablo18_")
                    for k,v in tablo18.iteritems():
                        setattr(epdk_tablo18,k,v)
                    tablo18_ws_array.append( epdk_tablo18 )
            
        bayi_no_tag = client.factory.create("xs:string")
        bayi_no_tag = bayi_lisans
        stringArray.item  = {'string':[bayi_no_tag]}
                
        tablo_1_.Epdk_Tablo1_    = tablo1_ws_array
        tablo_2_.Epdk_Tablo2_    = tablo2_ws_array
        tablo_3_.Epdk_Tablo3_    = tablo3_ws_array
        tablo_4_.Epdk_Tablo4_    = tablo4_ws_array
        tablo_5_.Epdk_Tablo5_    = tablo5_ws_array
        tablo_6_.Epdk_Tablo6_    = tablo6_ws_array
        tablo_7_.Epdk_Tablo7_    = tablo7_ws_array
        tablo_8_.Epdk_Tablo8_    = tablo8_ws_array
        tablo_9_.Epdk_Tablo9_    = tablo9_ws_array
        #tablo_10_.Epdk_Tablo10_  = tablo10_ws_array
        tablo_11_.Epdk_Tablo11_  = tablo11_ws_array
        tablo_12_.Epdk_Tablo12_  = tablo12_ws_array
        tablo_13_.Epdk_Tablo13_  = tablo13_ws_array
        tablo_14_.Epdk_Tablo14_  = tablo14_ws_array
        tablo_15_.Epdk_Tablo15_  = tablo15_ws_array
        tablo_16_.Epdk_Tablo16_  = tablo16_ws_array
        tablo_17_.Epdk_Tablo17_  = tablo17_ws_array
        tablo_18_.Epdk_Tablo18_  = tablo18_ws_array
              
        epdk_tables.epdk_table1s  = tablo_1_
        epdk_tables.epdk_table2s  = tablo_2_
        epdk_tables.epdk_table3s  = tablo_3_
        epdk_tables.epdk_table4s  = tablo_4_
        epdk_tables.epdk_table5s  = tablo_5_
        epdk_tables.epdk_table6s  = tablo_6_
        epdk_tables.epdk_table7s  = tablo_7_
        epdk_tables.epdk_table8s  = tablo_8_
        epdk_tables.epdk_table9s  = tablo_9_
        epdk_tables.epdk_table11s = tablo_11_
        epdk_tables.epdk_table12s = tablo_12_
        epdk_tables.epdk_table13s = tablo_13_
        epdk_tables.epdk_table14s = tablo_14_
        epdk_tables.epdk_table15s = tablo_15_
        epdk_tables.epdk_table16s = tablo_16_
        epdk_tables.epdk_table17s = tablo_17_
        epdk_tables.epdk_table18s = tablo_18_
        
        attr = 'EpdkTablesPackage'
        ##print epdk_tables
        ##print  getattr(client.service,attr)(epdk_tables,stringArray)
        return getattr(client.service,attr)(epdk_tables,stringArray)
        ##print donen
       
        ##print donen_json_format['EPDK_Tablo_1']
        '''
        except suds.WebFault, hata:
            #print "1111111111"
            hata_message =  'HATA:"%s" hatas�.\nHata Kodu:"%s"'%(hata.fault.faultcode,hata.fault.faultstring)
            #print hata_message
            return hata_message
        except:
            #print "222222222"
            hata_message =  'GENEL HATA:"%s" hatas�.'%( sys.exc_info()[0])
            return hata_message
        '''

#a=EpdkSoap()
#a.epdk_ws_sifre_ile_giris_test('ali')

class WebOtoTasitTanimaSoap:
    def __init__(self):
        
        self.webClientTasitTanima = None
        self.url      = sabitler.web_tasit_tanima_soap_url
        self.wsdl_url = sabitler.web_soap_tasit_tanima_wsdl_url
        
    def getClient(self,url ,wsdl_url):
        imp = Import(sabitler.imp_url)
        imp.filter.add(url)    
        https  = suds.transport.https.HttpTransport()
        opener = urllib2.build_opener(HTTPSudsPreprocessor)
        https.urlopener = opener
        #self.webClient                  = Client(wsdl_url, doctor=ImportDoctor(imp), transport = https)
        
        if not self.webClientTasitTanima:
            #print 'no cache'
            self.webClientTasitTanima   = Client(wsdl_url, doctor=ImportDoctor(imp), transport = https)
        else:
            #print 'cache'
            pass
        #print self.webClient
        return self.webClientTasitTanima
    
    def tasitTanimaKartBilgileriAl(self,bayi_lisans=''):
        bayi_lisans = 'BAY/467-220/10050'
        client = self.getClient( self.url ,self.wsdl_url )
        stringArray  = client.factory.create("ns0:SetClientParameters")
        bayi_no_tag  = client.factory.create("xs:string")
        bayi_no_tag  = bayi_lisans
        kart_seri_no ='1212313'
        stringArray.item  = {'string':[bayi_no_tag,kart_seri_no]}
        print "0000000000"
        print stringArray
        attr = 'getTasitTanimaKartBilgi'  
        #return  getattr(client.service,attr)(stringArray)
        
        print getattr(client.service,attr)(stringArray)
        
WebOtoTasitTanimaSoap().tasitTanimaKartBilgileriAl()
        
        
    