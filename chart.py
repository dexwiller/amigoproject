# -*- coding: latin5 -*-
import pychartdir
from PIL import Image, ImageDraw
from sabitler import image_path, CHART_COLORS
import wx
import cStringIO
import os
import util
def pie_chart(data, labels, title,resim_ad):
    #print '====',len(data),'====',len(labels)
    c = pychartdir.PieChart(850, 450)
    box = (0,0,840,440)
    c.setPieSize(430, 200, 150)
    c.addTitle(title)
    c.set3D(40)
    c.setLabelLayout(SideLayout)
    t = c.setLabelStyle()
    t.setBackground(pychartdir.SameAsMainColor, pychartdir.Transparent, pychartdir.glassEffect())
    t.setRoundedCorners(5)
    c.setLineColor(pychartdir.SameAsMainColor, 0x000000)
    c.setStartAngle(135)
    j = 0
    i = 0
    if 0 in data:
        for i in range(len(data)):
            if data[j] == 0:         
                del data[j]
                del labels[j]
                j -= 1
            j += 1
    c.setData(data, labels)
    c.setExplode(0)
    
    image_source = image_path  + "/"+resim_ad+".png"
  
    c.makeChart(image_source)
    
    resize_img(box,image_source)
   

def bar_chart(data, labels, title, resim_ad, x=280, y=240, stacked_data = [], colours = [], stacked_color=[], bg=False  ):
    j = 0
    i = 0
    if stacked_data != []:
	data   = stacked_data[0]
	data_1 = stacked_data[1]
    if 0 in data:
        for i in range(len(data)):
            if data[j] == 0:         
                del data[j]
                del labels[j]
                j -= 1
            j += 1
    c = pychartdir.XYChart(x,y )
    box = (0, 0, x-10, y-10)
    c.addTitle(title, "arialbd.ttf", 9)
    #if len(data)>1 :
    #	x_baslangic = x/len(data)
    #else:
    #x_baslangic = x/4
    #c.setPlotArea(x_baslangic, 20, birim_uzunluk*len(data),y-40 , -1, -1, Transparent, 0x000000)
    
    if bg:
	c.setPlotArea(25, 40,x-68,y-95 , pychartdir.Transparent, pychartdir.Transparent,pychartdir.Transparent, pychartdir.Transparent)#, Transparent, 0x000000)
	c.setWallpaper(image_path + "/tank_back.png")
    else:
	c.setPlotArea(25, 25,x-70,y-65 , 0xf8f8f8, 0xffffff)#, Transparent, 0x000000)#, Transparent, 0x000000)
    #plotarea.setBackground2("bg.png")
    #c.setRoundedFrame(0xffffff, 20)
    #c.addLegend(50, 20, 0, "arialbd.ttf", 8).setBackground(Transparent)
    if stacked_data == []:
	layer = c.addBarLayer3( data, colours )
    else:
	layer = c.addBarLayer2(pychartdir.Stack)
	layer.addDataSet(data_1, CHART_COLORS.SU_COLOR , "Su")
	layer.addDataSet(data,   stacked_color, "Yak�t")
	layer.setAggregateLabelStyle()
	layer.setDataLabelStyle()
    layer.setBarShape(pychartdir.CircleShape)
    layer.setBorderColor(pychartdir.Transparent, pychartdir.barLighting(0.8, 1.3))
    #layer.setRoundedCorners()
    #layer.set3D(10)
    new_labels = []
    for l in labels:
	l_1 , l_2 = util.get_uzun_str_bol_2_word( l , '')
	l = l_1 + '\n' + l_2    
	new_labels.append( l )
	    
    c.xAxis().setLabels( new_labels )
    
    c.yAxis().setColors(pychartdir.Transparent)
    c.yAxis().setLinearScale(0, 100, 10)
    c.yAxis().setLabelFormat("{value}")
    c.xAxis().setTickColor(pychartdir.Transparent)
    c.xAxis().setLabelStyle("arialbd.ttf", 8)#.setFontAngle(70)
    c.yAxis().setLabelStyle("arialbd.ttf", 7)
    if not bg: 
	c.syncYAxis()
	c.yAxis2().setColors(pychartdir.Transparent)
	c.yAxis2().setLabelFormat("{value|0}")
	c.yAxis2().setLabelStyle("arialbd.ttf", 7)

    img     = c.makeChart2( pychartdir.PNG )
    img_son = resize_img(box,img)
    return img_son
    
def resize_img(box,img):
	
    img_buffer = cStringIO.StringIO( img )
    im         = Image.open(img_buffer)
    im_son     = im.crop(box)
    im_son.load()
    return PilImageToWxImage( im_son )

def PilImageToWxImage( myPilImage, copyAlpha=True ) :

    hasAlpha = myPilImage.mode[ -1 ] == 'A'
    if copyAlpha and hasAlpha :  # Make sure there is an alpha layer copy.

        myWxImage = wx.EmptyImage( *myPilImage.size )
        myPilImageCopyRGBA = myPilImage.copy()
        myPilImageCopyRGB = myPilImageCopyRGBA.convert( 'RGB' )    # RGBA --> RGB
        myPilImageRgbData =myPilImageCopyRGB.tostring()
        myWxImage.SetData( myPilImageRgbData )
        myWxImage.SetAlphaData( myPilImageCopyRGBA.tostring()[3::4] )  # Create layer and insert alpha values.

    else :    # The resulting image will not have alpha.

        myWxImage = wx.EmptyImage( *myPilImage.size )
        myPilImageCopy = myPilImage.copy()
        myPilImageCopyRGB = myPilImageCopy.convert( 'RGB' )    # Discard any alpha from the PIL image.
        myPilImageRgbData =myPilImageCopyRGB.tostring()
        myWxImage.SetData( myPilImageRgbData )

    return myWxImage

    