#!/usr/bin/env python
# -*- coding: latin5 -*-
import wx
#'ICON_ASTERISK', 'ICON_ERROR', 'ICON_EXCLAMATION', 'ICON_HAND', 'ICON_INFORMATION', 'ICON_MASK', 'ICON_NONE', 'ICON_QUESTION', 'ICON_STOP', 'ICON_WARNING', '
import wx.lib.agw.infobar as IB
import  wx.lib.masked             as  masked
import util
#fonts = wx.DECORATIVE, wx.DEFAULT,wx.MODERN, wx.ROMAN, wx.SCRIPT or wx.SWISS.
from sabitler import allowed_key_code_list_number
default_list_text_color = wx.Colour( 0,0,64 )
default_list_bg_color   = wx.Colour(113,207,213) 
def setStaticTextHeader( widget):
    font  = wx.Font(12, wx.MODERN,wx.NORMAL, wx.BOLD )
    color = wx.Colour( 128,0,0 )
    widget.SetFont(font)
    widget.SetForegroundColour( color )
def setStaticTextLogo( widget):
    font  = wx.Font(8, wx.MODERN,wx.NORMAL, wx.BOLD )
    color = wx.Colour( 128,0,0 )
    widget.SetFont(font)
    widget.SetForegroundColour( color )  
def setStaticTextList( widget,zorunlu=False,color=None ):
    font  = wx.Font(10, wx.SCRIPT,wx.ITALIC, wx.BOLD)
    if color == None:
        color = default_list_text_color
    widget.SetFont(font)
    widget.SetForegroundColour( color )
    if zorunlu:
        widget.SetLabel('* '+widget.GetLabel())
def setStaticTextPompaHeader( widget, font=16):
    font  = wx.Font(font, wx.SWISS,wx.NORMAL, wx.BOLD )
    color = wx.Colour( 128,0,0 )
    widget.SetFont(font)
    try:
        widget.SetForegroundColour( color )
    except:
        widget.SetTextForeground(color)
def setStaticTextPompaPanelList( widget,zorunlu=False,font=16 ):
    font  = wx.Font(font, wx.SWISS,wx.ITALIC, wx.BOLD)
    color = default_list_text_color
    widget.SetFont(font)
    widget.SetForegroundColour( color )
    if zorunlu:
        widget.SetLabel('* '+widget.GetLabel())
def setStaticTextPompaPanelNumber( widget,zorunlu=False ):
    font  = wx.Font(20, wx.SWISS,wx.NORMAL, wx.BOLD)
    color = default_list_text_color
    widget.SetFont(font)
    widget.SetForegroundColour( color )
    if zorunlu:
        widget.SetLabel('* '+widget.GetLabel())

def setStaticTextListBg( widget ):
    color = default_list_bg_color
    rect  = widget.GetRect ()
    widget.SetBackgroundColour( color )
def setUyariText( widget ):
    font  = wx.Font(10, wx.SCRIPT,wx.ITALIC, wx.BOLD)
    color = wx.Colour( 255,0,0 )
    widget.SetFont(font)
    widget.SetForegroundColour( color )    
def setPanelListBg( widget ):
    color = wx.Colour(255,255,255)
    widget.SetBackgroundColour ( color )
def setButtonFont( widget,font=12 ):
    font  = wx.Font(font, wx.SWISS, wx.NORMAL, wx.BOLD)
    widget.SetFont( font )
def CheckNumber( event ):
        keycode = event.GetKeyCode()
        if keycode in allowed_key_code_list_number or chr(keycode).isdigit() or chr(keycode) == ".":
            event.Skip()

def CheckNumberMasked( parent, name,tam_kisim, ondalik_kisim=0,value_="" ):
    mask = ""
    for i in range(tam_kisim):
        mask += '#'
    if ondalik_kisim != 0:
        mask += '.'
        for o in range( ondalik_kisim ):
            mask += '#'

    newControl  = masked.TextCtrl( parent, -1 , 
                                                mask         = mask,
                                                #formatcodes  = 'F',
                                                includeChars = "",
                                                demo         = True,
                                                name         = name,
                                                value        = str(value_)) 
    return newControl

def CheckTelFaxMasked( parent, name,id_ = wx.NewId(),value_="" ):
    newControl  = masked.TextCtrl( parent, -1 , 
                                                mask         = '(###)#######',
                                                #formatcodes  = 'F',
                                                includeChars = '',
                                                demo         = True,
                                                name         = name,
                                                value        = value_) 
    return newControl
def CheckNumberMinMax( event,minmaxTuple):
        keycode = event.GetKeyCode()
        #backspace 8
        if keycode in allowed_key_code_list_number or chr(keycode).isdigit() or chr(keycode) == ".":
            event.Skip()
            val = event.GetEventObject().GetValue()
            if keycode != 8:
                val = val  + chr(keycode)
            else:
                val = val[:len(val)-1]
def uyari_message( parent, message, win_name):
        parent.SetTransparent(150)
        dlg = wx.MessageDialog(parent, message,
                               win_name,
                               wx.OK   | wx.ICON_WARNING
                               )
        
        retCode = dlg.ShowModal()
        if (retCode == wx.ID_OK):
            dlg.Destroy()
        parent.SetTransparent(255)
def scrool_message( parent, message,win_name):
    parent.SetTransparent(150)
    dlg = wx.lib.dialogs.ScrolledMessageDialog(parent, message,
                           win_name
                           )
    
    retCode = dlg.ShowModal()
    if (retCode == wx.ID_OK):
        dlg.Destroy()
    parent.SetTransparent(255)

def ok_message( parent, message, win_name):
        parent.SetTransparent(150)
        dlg = wx.MessageDialog(parent, message,
                               win_name,
                               wx.OK   | wx.ICON_INFORMATION
                               )
        
        retCode = dlg.ShowModal()
        if (retCode == wx.ID_OK):
            dlg.Destroy()
        parent.SetTransparent(255)
def ok_cancel_message( parent, message, win_name, ok_function=None, cancel_function=None):
        parent.SetTransparent(150)
        dlg = wx.MessageDialog(parent, message,
                               win_name,
                               wx.OK   | wx.ICON_INFORMATION |wx.CANCEL
                               )
        
        retCode = dlg.ShowModal()
        if (retCode == wx.ID_OK):
            if ok_function != None:
                ok_function()
            dlg.Destroy()
        elif (retCode == wx.ID_CANCEL):
            if cancel_function != None:
                cancel_function()
            dlg.Destroy()
    
        parent.SetTransparent(255)
        
def ok_info_bar(parent,sizer,message):
    
    parent.info = IB.InfoBar(parent)
    sizer.Add(parent.info, 0, wx.EXPAND)
    #parent.info.SetSize( (parent.GetSize().width-15, 100))
    parent.info.ShowMessage(message, wx.ICON_INFORMATION)
    
def updateWindow(MainContainer=None, createLayOutMethod=None,**kwargs ):
    if MainContainer:
        
        if isinstance( MainContainer, list):
            for c in MainContainer:
                if c:
                    c.Destroy()
        else:        
            if MainContainer:
                MainContainer.Destroy()
        createLayOutMethod()
    else:
        if kwargs.has_key('book'):
            book = kwargs['book']
            book.Freeze()
            book.SetSelection(kwargs['temp_page'])
            p_ = kwargs['create_panel_method']()
            book.RemovePage(kwargs['cur_page'])
            book.InsertPage( kwargs['cur_page'],p_,kwargs['label'])
            book.SetPageImage(kwargs['cur_page'], kwargs['image'])
            book.SetSelection(kwargs['cur_page'])
            #book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            book.Thaw()

def OnSize(event):
        component = event.GetEventObject()
        event.Skip()
        component.Refresh()

def OnPaint( event, text=''):
    
    component = event.GetEventObject()
    
    dc = wx.AutoBufferedPaintDCFactory(component)
    rect = list(component.GetRect())
    if rect[0] < 0:
        rect[0] = 0
    if rect[1] < 0:
        rect[1] = 0
    #rect = (0, 0, 1360, 63)
    dc.SetBackgroundMode(wx.TRANSPARENT)

    setStaticTextPompaHeader( dc )
    dc.GradientFillLinear(rect, component.startColour, component.endColour, wx.SOUTH)
    #print rect
    if text != '':
        for t in text:
            dc.DrawText(t[0], t[1], t[2])

def BindEvents(component, text=''):

        if not component.PAINT_GRADIENTS:
            return
        if text != '':
            component.Bind(wx.EVT_PAINT, lambda event: OnPaint(event, text ) )
        else:
            component.Bind(wx.EVT_PAINT, OnPaint)
        
        component.Bind(wx.EVT_ERASE_BACKGROUND, lambda event: None)
        component.Bind(wx.EVT_SIZE, OnSize)
   
class TransparentText(wx.StaticText):
  def __init__(self, parent, id=wx.ID_ANY, label='', pos=wx.DefaultPosition,
             size=wx.DefaultSize, style=wx.TRANSPARENT_WINDOW, name='transparenttext'):
    wx.StaticText.__init__(self, parent, id, label, pos, size, style, name)

    self.Bind(wx.EVT_PAINT, self.on_paint)
    self.Bind(wx.EVT_ERASE_BACKGROUND, lambda event: None)
    self.Bind(wx.EVT_SIZE, self.on_size)

  def on_paint(self, event):
    
    bdc = wx.PaintDC(self)
    dc = wx.GCDC(bdc)

    font_face = self.GetFont()
    font_color = self.GetForegroundColour()

    dc.SetFont(font_face)
    dc.SetTextForeground(font_color)
    dc.DrawText(self.GetLabel(), 0, 0)

  def on_size(self, event):
    self.Refresh()
    event.Skip()
class LoginDialog(wx.Dialog):
    #----------------------------------------------------------------------
    def __init__(self):
        """Constructor"""
        wx.Dialog.__init__(self, None, title="Giri�",size=(305,150))
        self.logged_in = False
        self.Center()
        setPanelListBg( self )
        # user info
        user_sizer = wx.BoxSizer(wx.HORIZONTAL)
 
        user_lbl = wx.StaticText(self, label="Kullan�c� Ad� :")
        
        setStaticTextList( user_lbl)
        user_sizer.Add(user_lbl, 0, wx.ALL|wx.CENTER, 5)
        self.user = wx.TextCtrl(self)
        user_sizer.Add(self.user, 0, wx.ALL, 5)
 
        # pass info
        p_sizer = wx.BoxSizer(wx.HORIZONTAL)
 
        p_lbl = wx.StaticText(self, label="�ifre         :")
        setStaticTextList( p_lbl )
        p_sizer.Add(p_lbl, 0, wx.ALL|wx.CENTER, 5)
        self.password = wx.TextCtrl(self, style=wx.TE_PASSWORD|wx.TE_PROCESS_ENTER)
        self.password.Bind(wx.EVT_TEXT_ENTER, self.onLogin)
        p_sizer.Add(self.password, 0, wx.ALL, 5)
 
        main_sizer = wx.BoxSizer(wx.VERTICAL)
        main_sizer.Add(user_sizer, 0, wx.ALL, 5)
        main_sizer.Add(p_sizer, 0, wx.ALL, 5)
 
        btn = wx.Button(self, label="Giri�")
        btn.Bind(wx.EVT_BUTTON, self.onLogin)
        main_sizer.Add(btn, 0, wx.ALL|wx.CENTER, 5)
 
        self.SetSizer(main_sizer)
 
    #----------------------------------------------------------------------
    def onLogin(self, event):
        stupid_password = "manager"
        user_password = self.password.GetValue()
        if user_password == stupid_password:
            #print "You are now logged in!"
            self.logged_in = True
            self.Close()
        else:
            uyari_message(self, 'Kullan�c� Ad� veya �ifre Yanl�� !!!','Hatal� Giri� ')


