#!/usr/bin/env python
# -*- coding: latin5 -*-
import wx
import wx.dataview as dv
import util
import sabitler as s
from DBConnectLite import DBObjects,Struct, Child
db = DBObjects()
class TreeListModel(dv.PyDataViewModel):
    def __init__(self, data,headers, child_parameters ):
        dv.PyDataViewModel.__init__(self)
        self.data                 = data
        self.headers              = map(lambda x:x.split(',')[0], headers)
        self.data_types           = map(lambda x:x.split(',')[1], headers)
        self.child_parameters     = child_parameters
      
        # The objmapper is an instance of DataViewItemObjectMapper and is used
        # to help associate Python objects with DataViewItem objects. Normally
        # a dictionary is used so any Python object can be used as data nodes.
        # If the data nodes are weak-referencable then the objmapper can use a
        # WeakValueDictionary instead. Each PyDataViewModel automagically has
        # an instance of DataViewItemObjectMapper preassigned. This
        # self.objmapper is used by the self.ObjectToItem and
        # self.ItemToObject methods used below.
        
        #self.objmapper.UseWeakRefs(True)
        #bu am�na kodumunun �eyi y�z�nden python on the fly child set etmiyordu. 

                
    # Report how many columns this model provides data for.
    def GetColumnCount(self):
        return len(self.headers)

    # Map the data column numbers to the data type
    def GetColumnType(self, col):
        mapper = {}
        for c in range( len(self.data_types)):
            mapper[c] = self.data_types[ c ]
        
        return mapper[col]
    def GetChildren(self, parent, children):  
        # The view calls this method to find the children of any node in the
        # control. There is an implicit hidden root node, and the top level
        # item(s) should be reported as children of this node. A List view
        # simply provides all items as children of this hidden root. A Tree
        # view adds additional items as children of the other items, as needed,
        # to provide the tree hierachy.
        ##self.log.write("GetChildren\n")
        
        # If the parent item is invalid then it represents the hidden root
        # item, so we'll use the genre objects as its children and they will
        # end up being the collection of visible roots in our tree.
        if not parent:
            for p in self.data:
                children.append(self.ObjectToItem(p))
            return len(self.data)
        
        # Otherwise we'll fetch the python object associated with the parent
        # item and make DV items for each of it's child objects.
        node              = self.ItemToObject(parent)
        parent_parameters = [node.mapper[0],node.base_attr]
        ch                = db.get_raw_data_all( table_name    = self.child_parameters[0],
                                          selects              = self.child_parameters[2],
                                          where                = "%s=%s"%( self.child_parameters[1],node.base_attr),
                                          dict_mi              = 'child',
                                          child_parameters     = self.child_parameters,
                                          parent_parameters    = parent_parameters,
                                          data_types           = self.data_types                                        
                                         )
        
        map( lambda x:children.append( self.ObjectToItem(x)), ch)
        return len(children)
        return 0
    

    def IsContainer(self, item):
        # Return True if the item has children, False otherwise.
        ##self.log.write("IsContainer\n")
        
        # The hidden root is a container
        if not item:
            return True
        # and in this model the genre objects are containers
        node = self.ItemToObject(item)
        if isinstance(node, Struct):
            return True
        # but everything else (the song objects) are not
        return False    


    #def HasContainerColumns(self, item):
    #    self.log.write('HasContainerColumns\n')
    #    return True

    
    def GetParent(self, item):
        # Return the item which is this item's parent.
        ##self.log.write("GetParent\n")
        if not item:
            return dv.NullDataViewItem

        node = self.ItemToObject(item)        
        if isinstance(node, Struct):
            return dv.NullDataViewItem
        elif isinstance(node, Child):
            for p in self.data:
                if p.base_attr == node.base_attr:
                    return self.ObjectToItem(p)
            
        
    def GetValue(self, item, col):
        # Return the value to be displayed for this item and column. For this
        # example we'll just pull the values from the data objects we
        # associated with the items in GetChildren.
        
        # Fetch the data object for this item.
        node = self.ItemToObject(item)
        
        if isinstance(node, Struct):
            mapper = node.mapper
            try:
                return mapper[col]
                
            except:
                return ""
        elif isinstance(node, Child):
            mapper = node.mapper
            return mapper[col]
        
        else:
            raise RuntimeError("unknown node type")

    def GetAttr(self, item, col, attr):
        ##self.log.write('GetAttr')
        node = self.ItemToObject(item)
        if isinstance(node, Struct):
            attr.SetColour('blue')
            attr.SetBold(True)
            return True
        return False
    
    '''
    def SetValue(self, value, item, col):
        self.log.write("SetValue: %s\n" % value)
        
        # We're not allowing edits in column zero (see below) so we just need
        # to deal with Song objects and cols 1 - 5
        
        node = self.ItemToObject(item)
        if isinstance(node, Song):
            if col == 1:
                node.artist = value
            elif col == 2:
                node.title = value
            elif col == 3:
                node.id = value
            elif col == 4:
                node.date = value
            elif col == 5:
                node.like = value
    '''
class Parameters:
    def __init__(self):
        self.total_key_code = ''
    def get_root( self, object_ ):
        while object_.GetParent():
            object_ = object_.GetParent()
        
        return object_
        
    def get_dagitici_object( self ):
        dagitici =  db.get_raw_data_all(table_name = "P_DAGITICI",dict_mi = 'object' )
        if len( dagitici ) > 0:
            dagitici = dagitici[0]
        else:
            dagitici = None
        
        return dagitici
    def get_bayi_object( self ):
        bayi =  db.get_raw_data_all(table_name = "P_BAYI",dict_mi = 'object' )
        if len( bayi ) > 0:
            bayi = bayi[0]
        else:
            bayi = None
        
        return bayi
    def get_bayi_adi( self, satir_formatla = True ):
        bayi_adi = 'Taurus Otomasyon'
        bayi = self.get_bayi_object()
        if bayi:
            bayi_adi   = bayi.BAYI_ADI
            bayi_split = bayi_adi.split( ' ' )
            if satir_formatla and len( bayi_split) > 4:
                bayi_adi_1 = bayi_split[:4]
                bayi_adi_2 = bayi_split[4:]
                bayi_adi = ' '.join( bayi_adi_1) + '\n' + ' '.join( bayi_adi_2 )
        
        return bayi_adi
    
    def get_dagitici_adi( self, satir_formatla = True):
        dagitici_adi = 'Taurus Otomasyon'
        dagitici = self.get_dagitici_object()
        if dagitici:
            dagitici_adi = dagitici.DAGITICI_ADI
            dagitici_split = dagitici_adi.split( ' ' )
            if satir_formatla and len( dagitici_split) > 4:
                dagitici_adi_1 = dagitici_split[:4]
                dagitici_adi_2 = dagitici_split[4:]
                dagitici_adi = ' '.join( dagitici_adi_1) + '\n' + ' '.join( dagitici_adi_2 )
        return dagitici_adi
    
    def enable_settings( self, event ):
        self.total_key_code += chr(event.GetKeyCode())
        pass_algoritm = s.pass_algoritm
        if util.hasmap( self.total_key_code ) == pass_algoritm:
            self.get_root(wx.FindWindowById( s.EKRAN_ID_MAP.ANA_EKRANLAR.id_e_about)).createAyarlar()
        
    def bindSettings( self, object_ ):
        object_.SetFocus()
        object_.Bind( wx.EVT_CHAR, self.enable_settings )
        '''
        bindings = []
        for c in list(map(chr, range(48, 123))):
            bindings.append( ( wx.ACCEL_NORMAL, ord(c), c.lower()))
            bindings.append( ( wx.ACCEL_SHIFT, ord(c), c.upper()))
        accelEntries = []
        for binding in bindings:
            eventId = wx.NewId()
            accelEntries.append( (binding[0], binding[1], eventId) )
            object_.Bind(wx.EVT_MENU, lambda evt, temp=binding[2]: self.enable_settings(evt, temp), id=eventId)
        accelTable  = wx.AcceleratorTable(accelEntries)
        object_.SetAcceleratorTable(accelTable )
        '''
    
class IndexListModel(dv.PyDataViewIndexListModel):
    def __init__(self, data, types):
        dv.PyDataViewIndexListModel.__init__(self, len(data))
        self.types = types
        self.data = data
        #self.log = log
        #self.headers          = map(lambda x:x.split(',')[0], self.data)
        #self.data_types       = map(lambda x:x.split(',')[1], self.data)
    # All of our columns are strings.  If the model or the renderers
    # in the view are other types then that should be reflected here.
    def GetColumnType(self, col):
        return "string"

    # This method is called to provide the data object for a
    # particular row,col
    def GetValueByRow(self, row, col):
        data_ = self.data[row][col]       
        if data_ == None:
            data_=''
        format_ = self.types[col]
        
        if data_=='' and format_=='datetime':
            return '-------------'
        else:
            #print data_
            return util.data_formatla( data_,format_)
        

    # This method is called when the user edits a data item in the view.
    def SetValueByRow(self, value, row, col):
        #self.log.write("SetValue: (%d,%d) %s\n" % (row, col, value))
        if self.data[row]:
            self.data[row] = list(self.data[row])
        self.data[row][col] = value

    # Report how many columns this model provides data for.
    def GetColumnCount(self):
        return len(self.data[0])

    # Report the number of rows in the model
    def GetCount(self):
        #self.log.write('GetCount')
        return len(self.data)
    
    # Called to check if non-standard attributes should be used in the
    # cell at (row, col)
    def GetAttrByRow(self, row, col, attr):
        ##self.log.write('GetAttrByRow: (%d, %d)' % (row, col))
        if col == 0:
            attr.SetColour('blue')
            attr.SetBold(True)
            return True
        return False


    # This is called to assist with sorting the data in the view.  The
    # first two args are instances of the DataViewItem class, so we
    # need to convert them to row numbers with the GetRow method.
    # Then it's just a matter of fetching the right values from our
    # data set and comparing them.  The return value is -1, 0, or 1,
    # just like Python's cmp() function.
    def Compare(self, item1, item2, col, ascending):
        if not ascending: # swap sort order?
            item2, item1 = item1, item2
        row1 = self.GetRow(item1)
        row2 = self.GetRow(item2)
        if col == 0:
            return cmp(int(self.data[row1][col]), int(self.data[row2][col]))
        else:
            return cmp(self.data[row1][col], self.data[row2][col])

        
    def DeleteRows(self, rows):
        # make a copy since we'll be sorting(mutating) the list
        rows = list(rows)
        # use reverse order so the indexes don't change as we remove items
        rows.sort(reverse=True)
        
        for row in rows:
            # remove it from our data structure
            del self.data[row]
            # notify the view(s) using this model that it has been removed
            self.RowDeleted(row)
            
            
    def AddRow(self, value):
        # update data structure
        self.data.append(value)
        # notify views
        self.RowAppended()
    def InsertRow( self, index, value ):
        
        self.data.insert( index, value )
        try:
            self.RowInserted(before=1)
        except:
            self.RowInserted(before=0)
            