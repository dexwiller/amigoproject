#!/usr/bin/env python
# -*- coding: latin5 -*-

import wx
import logging
import sabitler

from win32api import GetSystemMetrics

import style as s
import wx.lib.masked as masked
import wx.lib.buttons as LibButtons
import util
from model import Parameters
from util_db import PompaRequestFormatla
import time 
import wx.lib.agw.aui as aui
param = Parameters()
image_path=sabitler.image_path
from process_manager import KThread
id_vardiya_duzen_button        = sabitler.EKRAN_ID_MAP.VARDIYA_BUTONLAR.id_b_vardiya_duzeni
id_vardiya_satis_izleme_button = sabitler.EKRAN_ID_MAP.VARDIYA_BUTONLAR.id_b_vardiya_satis_izleme

from local import MESSAGES
from DBConnectLite import DBObjects

class win_Vardiya(wx.Frame):
    def __init__(self, parent, id_, title):
        
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR | wx.MAXIMIZE))
        
        self.O  = param.get_root( self ).O 
        self.Bind(wx.EVT_CLOSE, self.OnCloseWindow)
        #self.CreateStatusBar()       
        #self.Bind(wx.EVT_CLOSE, self.OnCloseWindow)
        self.LayoutItems()
        self.Centre()
        
        self.Show(True)
    
    def LayoutItems(self):
       
        mainSizer = wx.GridBagSizer(0,0)
        self.Freeze()
        self.SetSizer(mainSizer)
        winSize =  self.GetSize()
        self.top_panel = wx.Panel(self, wx.ID_ANY,size=(winSize[0],540))
        self.top_panel.SetBackgroundColour( wx.WHITE)
        self.top_sizer = wx.GridBagSizer(1,1)
        self.top_panel.SetSizer( self.top_sizer )
       
        self.vardiya_duzen_ekrani_pressed  =  wx.Image("%svardiya_accepted_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_vardiya_duzen = wx.Image(MESSAGES.vardiya_ekrani_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_vardiya_duzen_ekrani  = wx.BitmapButton(self.top_panel, id=id_vardiya_duzen_button, bitmap=image_vardiya_duzen,pos=(0,0),
        size = (image_vardiya_duzen.GetWidth()+5, image_vardiya_duzen.GetHeight()+5))
        self.btn_vardiya_duzen_ekrani.Bind(wx.EVT_BUTTON, self.OnVardiyaMenuPressed)
        self.btn_vardiya_duzen_ekrani.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.vardiya,MESSAGES.islemleri))
        self.btn_vardiya_duzen_ekrani.SetBitmapSelected(self.vardiya_duzen_ekrani_pressed)
        self.top_sizer.Add(self.btn_vardiya_duzen_ekrani, (0,0),(1,1),wx.ALL)
        
        
        self.vardiya_satis_ekrani_pressed  =  wx.Image("%svardiya_kasa_accepted_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_vardiya_satis_ekrani = wx.Image(MESSAGES.vardiya_kasa_ekrani_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_vardiya_satis_ekrani  = wx.BitmapButton(self.top_panel, id=id_vardiya_satis_izleme_button, bitmap=image_vardiya_satis_ekrani,pos=(0,0),
        size = (image_vardiya_satis_ekrani.GetWidth()+5, image_vardiya_satis_ekrani.GetHeight()+5))
        self.btn_vardiya_satis_ekrani.Bind(wx.EVT_BUTTON, self.OnVardiyaMenuPressed)
        self.btn_vardiya_satis_ekrani.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.vardiya,MESSAGES.satislari))
        self.btn_vardiya_satis_ekrani.SetBitmapSelected(self.vardiya_satis_ekrani_pressed)
        self.top_sizer.Add(self.btn_vardiya_satis_ekrani, (0,1),(1,1),wx.ALL)
        
        self.middle_panel =  wx.Panel(self, wx.ID_ANY ,size=(winSize[0],winSize[1]-140))
        middle_sizer=wx.FlexGridSizer(cols=1,hgap=10,vgap=10)
        self.middle_panel.SetSizer(middle_sizer)
        self.middle_panel.SetBackgroundColour( wx.Colour(254,254,254))
        middle_panel_image=wx.Image(MESSAGES.yazilim_image_%image_path,wx.BITMAP_TYPE_ANY)
        sb1 = wx.StaticBitmap(self.middle_panel, -1, wx.BitmapFromImage(middle_panel_image))
        
        middle_sizer.Add(sb1,1,wx.ALL|wx.CENTER)      

        mainSizer.Add( self.top_panel,(0,0),(1,1),wx.ALL)
        mainSizer.Add( self.middle_panel,(1,0),(1,1),wx.ALL)
        param.bindSettings( self )
        self.Thaw()
       
    def OnExit(self,e):
        self.onay_message("��kmak �stedi�inizden Emin misiniz ?","Kapat")
    def OnCloseWindow(self,e):
        self.onay_message("��kmak �stedi�inizden Emin misiniz ?","Kapat")
    def OnCloseWindow(self,e):
        for kthred in self.thread_list:
            kthred.kill()
        self.Destroy()
    def onSelect(self, event):
        """"""
        #print dir( event )
        #print "You selected: " + self.isin_adi_cmb.GetStringSelection()
        obj = self.isin_adi_cmb.GetClientData(self.isin_adi_cmb.GetSelection())
        text = """
        The object's attributes are:
        %s  %s
 
        """ % (obj.id, obj.ad)
        #print text
    def onay_message( self,message,win_name):
        self.SetTransparent(150)
        dlg = wx.MessageDialog(self, message,
                               win_name,
                               wx.OK  |wx.CANCEL | wx.ICON_INFORMATION
                               )
        
        retCode = dlg.ShowModal()
        if (retCode == wx.ID_OK):
            self.Show( False )
        dlg.Destroy()
        self.SetTransparent(255)
    def OnVardiyaMenuPressed( self , event ):
        maximize = False
        id_  = event.GetEventObject().GetId()
        if id_ == id_vardiya_duzen_button:
            winClass = win_Vardiya_Duzen_Ekrani
            title    = MESSAGES.sablon_iki_kelime%(MESSAGES.vardiya, MESSAGES.islemleri )
        elif id_ == id_vardiya_satis_izleme_button:
            winClass = win_Vardiya_Satis_Ekrani
            title    = MESSAGES.sablon_iki_kelime%(MESSAGES.vardiya, MESSAGES.satislari )
            maximize = True
        self.O.newWindow(self,event,winClass,title,maximize)
class win_Vardiya_Duzen_Ekrani(wx.Frame):        
    def __init__(self,parent,id_,title):
        wx.Frame.__init__(self,parent,id_,title=title,
                          style=(wx.DEFAULT_FRAME_STYLE
                                |wx.FRAME_NO_TASKBAR))
        self.root =  param.get_root( self )
        self.db = self.root.DB
        self.O  = self.root.O
        self.CreateLayout()
        self.Centre()
        self.last_checked_item_id = None
    
    def getCheckedItems( self, item_id  ):
        self.last_checked_item_id = item_id
        #vardiya_obj = db.get_raw_data_all(table_name='VARDIYA', where='ID_VARDIYA=%s'%self.last_checked_item_id,dict_mi='object',selects='VARDIYA_ADI,BASLANGIC_SAATI,BITIS_SAATI')
        vardiya_obj = filter(lambda x:x[0] == self.last_checked_item_id ,self.data_all)
        vardiya_obj = vardiya_obj[0]
        
        self.Vardiya_adi_txt.SetValue(vardiya_obj[1])
        self.Vardiya_bas_saat_txt.SetValue(util.data_formatla(vardiya_obj[2],'time'))
        self.Vardiya_bit_saat_txt.SetValue(util.data_formatla(vardiya_obj[3],'time'))
        if vardiya_obj[4] == MESSAGES.aktif:
            self.cb_aktif.SetValue( True )
            self.selected_aktif = True
        else:
            self.cb_aktif.SetValue( False )
            self.selected_aktif = False
        personel_object_list = self.db.get_raw_data_all(table_name='VARDIYA_CARI',
                                                   where='ID_VARDIYA=%s AND ID_ACTIVE=%s'%(self.last_checked_item_id,
                                                                                           1),dict_mi='object',
                                                   selects='ID_CARI_HESAP')
        for selected in self.cmb_cari_adi.GetChecked():
            self.cmb_cari_adi.Check( selected, False )
        for personel in personel_object_list:
            index_cari = self.cari_adi_parametric_objects.index(filter(lambda x:  x.id==personel.ID_CARI_HESAP,self.cari_adi_parametric_objects)[0])
            self.cmb_cari_adi.Check(index_cari, True )

    def CreateLayout( self ):
        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.mainSizer)
        self.Freeze()
        self.p      = self.VardiyaDuzenPanel()
        s.setPanelListBg( self.p )
        
        self.mainSizer.Add(self.p, 1,wx.ALL | wx.EXPAND)
        self.CreateVardiyaListe()
        self.Layout()
        self.SetSize((850,600))
        
        
        if self.GetSize().height > GetSystemMetrics(1) - 100:
            self.SetSize( (self.GetSize().width, GetSystemMetrics(1) - 100))
        
        if self.GetSize().width > GetSystemMetrics(0) - 100:
            self.SetSize( (GetSystemMetrics(0) - 100,self.GetSize().height )) 
        
        self.Thaw()
    def CreateVardiyaListe( self ):
        self.data_all       = self.db.get_raw_data_all(table_name = 'VARDIYA',where="ID_ACTIVE=1 OR ID_ACTIVE=2",selects="""ID_VARDIYA,VARDIYA_ADI,BASLANGIC_SAATI,BITIS_SAATI,
                                                       (CASE WHEN ID_ACTIVE=1 THEN '%s' ELSE '%s' END) as VARDIYA_DURUMU"""%( MESSAGES.aktif, MESSAGES.pasif)  )
        headers = ['%s,digit'%MESSAGES.sec,
                   '%s,string'%MESSAGES.sablon_iki_kelime%( MESSAGES.vardiya,MESSAGES.adi),
                   '%s,time'%MESSAGES.sablon_iki_kelime%( MESSAGES.baslangic,MESSAGES.saati),
                   '%s,time'%MESSAGES.sablon_iki_kelime%( MESSAGES.bitis,MESSAGES.saati),
                   '%s,string'%MESSAGES.sablon_iki_kelime%( MESSAGES.vardiya,MESSAGES.durumu)]
        
        self.liste = self.O.createStaticList(self, headers, self.data_all , lineRadio=True , radio_set_method = self.getCheckedItems)
        self.liste.SetPosition((0,0))
        self.mainSizer.Add(self.liste,1,wx.ALL | wx.EXPAND)
        #self.liste.Refresh()
        self.Layout()
        #return liste
        
        
    def VardiyaDuzenPanel (self) :
        p = wx.Panel(self,1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE
                     | wx.DOUBLE_BORDER)
        
        gbs = self.gbs =wx.GridBagSizer(10,10)
        
        
        Vardiya_adi_lbl   = wx.StaticText(p, label="%s"%( MESSAGES.sablon_iki_kelime_dot%(MESSAGES.vardiya, MESSAGES.adi)))
        self.Vardiya_adi_txt      = wx.TextCtrl  (p, -1, "",size=(180,-1))
        s.setStaticTextList( Vardiya_adi_lbl )
        
        Vardiya_bas_saat_lbl   = wx.StaticText(p, label="%s"%( MESSAGES.sablon_iki_kelime_dot%(MESSAGES.vardiya, MESSAGES.baslangic)))# giri� bi�imi saat �eklinde olacak
        #self.Vardiya_bas_saat_txt      = wx.TextCtrl  (p, -1, "",size=(180,-1))
        self.btn_spn_bas           = wx.SpinButton( p, -1, wx.DefaultPosition, (-1,-1), wx.SP_VERTICAL )
        self.Vardiya_bas_saat_txt  = masked.TimeCtrl(
                                     p, -1, name="24 hour control", fmt24hr=True,
                                     spinButton = self.btn_spn_bas
                                    )
        
        s.setStaticTextList( Vardiya_bas_saat_lbl )
        s.setButtonFont( self.btn_spn_bas )
        Vardiya_bit_saat_lbl       = wx.StaticText(p, label=MESSAGES.sablon_iki_kelime_dot%(MESSAGES.vardiya, MESSAGES.bitis)) #giri� bi�imi saat �eklinde olacak
        self.btn_spn_bit           = wx.SpinButton( p, -1, wx.DefaultPosition, (-1,-1), wx.SP_VERTICAL )
        self.Vardiya_bit_saat_txt  = masked.TimeCtrl(
                                     p, -1, name="24 hour control", fmt24hr=True,
                                     spinButton = self.btn_spn_bit
                                    )
        s.setButtonFont( self.btn_spn_bit )
        s.setStaticTextList( Vardiya_bit_saat_lbl )
        
        
        self.cari_adi_parametric_objects = []
        self.cari_adi_parametric_objects = self.db.get_parameters_all("CARI_HESAPLAR",order="CARI_ADI",selects="ID_CARI_HESAP,CARI_ADI",where='ID_CARI_TIPI=%s'%sabitler.CARI_TIPLER.PERSONEL)
        lbl_cari_adi                = wx.StaticText(p, label="%s :"%MESSAGES.personel)
        
        self.cmb_cari_adi           = wx.CheckListBox(p,choices=[],size=(160, 50))#, style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        #self.cmb_hareket_adi.Bind(wx.EVT_CHECKLISTBOX,self.onhareketidgetir)
        #self.db.widgetMaker( self.cmb_tank_adi , tank_adi_parametric_objects)
        self.db.widgetMaker( self.cmb_cari_adi , self.cari_adi_parametric_objects)
        s.setStaticTextList(lbl_cari_adi)
        
        self.cb_aktif = wx.CheckBox(p,wx.NewId() , "%s"%( MESSAGES.sablon_ayrac%(MESSAGES.aktif,MESSAGES.pasif)),style=wx.ALIGN_RIGHT)

        self.btn_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_vardiya_kaydet  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%( MESSAGES.vardiya, MESSAGES.kaydet))
        self.btn_vardiya_kaydet.Bind(wx.EVT_BUTTON, self.OnVardiyaKaydet)
        self.btn_vardiya_kaydet.SetToolTipString("%s"%MESSAGES.sablon_iki_kelime%( MESSAGES.vardiya, MESSAGES.kaydet))
        self.btn_vardiya_kaydet.SetBitmapSelected(self.btn_save_pressed)
        s.setButtonFont( self.btn_vardiya_kaydet )
        
        self.btn_delete_pressed  =  wx.Image("%sdelete_green.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_delete = wx.Image("%sdelete.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_vardiya_delete  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_delete,label="         %s"%( MESSAGES.sablon_iki_kelime%(MESSAGES.vardiya, MESSAGES.sil)) , style=wx.ALIGN_LEFT)
        self.btn_vardiya_delete.Bind(wx.EVT_BUTTON, self.OnVardiyaSil)
        self.btn_vardiya_delete.SetToolTipString("%s"%( MESSAGES.sablon_iki_kelime%(MESSAGES.vardiya, MESSAGES.sil)))
        self.btn_vardiya_delete.SetBitmapSelected(self.btn_delete_pressed)
        s.setButtonFont( self.btn_vardiya_delete )
        #bin.jpg kullan ve temizle butonuna �evir
        
        self.btn_bin_pressed  =  wx.Image("%strash_black.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_bin = wx.Image("%strash.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_bin = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_bin,label=MESSAGES.temizle)
        self.btn_bin.Bind(wx.EVT_BUTTON, self.VardiyaVeriReset)
        self.btn_bin.SetToolTipString(MESSAGES.temizle)
        self.btn_bin.SetBitmapSelected(self.btn_bin_pressed)
        s.setButtonFont( self.btn_bin )
        line_                           =   wx.StaticLine(p, -1, style=wx.LI_HORIZONTAL)
         
        gbs.Add( Vardiya_adi_lbl, (1,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.Vardiya_adi_txt, (1,2) ,(1,1),  wx.EXPAND,3)
        
        gbs.Add( Vardiya_bas_saat_lbl, (2,1),(1,1),  wx.EXPAND ,3)
        gbs.Add( self.Vardiya_bas_saat_txt, (2,2) ,(1,1),  wx.EXPAND,3)
        gbs.Add( self.btn_spn_bas,(2,3),(1,1))
        
        gbs.Add( Vardiya_bit_saat_lbl, (3,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.Vardiya_bit_saat_txt, (3,2) ,(1,1),  wx.EXPAND,3)
        gbs.Add( self.btn_spn_bit,(3,3),(1,1))
        
        gbs.Add( lbl_cari_adi, (1,3),(1,1),  wx.EXPAND,3)
        gbs.Add( self.cmb_cari_adi, (1,4) ,(3,1),  wx.EXPAND,3)
        
        gbs.Add( self.cb_aktif,(1,5),(1,1), wx.EXPAND,3)
        gbs.Add( line_,  (4,0 ),(1,5),flag=wx.EXPAND|wx.BOTTOM, border=10)

        gbs.Add( self.btn_vardiya_kaydet, (6,3) ,(1,2),  wx.ALL,3)
        gbs.Add( self.btn_vardiya_delete, (6,2) ,(1,1),  wx.ALL,3)
        gbs.Add( self.btn_bin, (6,1) ,(1,1),  wx.ALL,3)
        
        p.SetSizerAndFit(gbs)
        p.SetAutoLayout(True)
        s.setPanelListBg( p )
        return p
    
    def OnVardiyaKaydet (self, event ):
        anlik_endeks = {}
        counter      = 0
        while not self.root.endeksQueueContainer.empty() and anlik_endeks == {} and counter< 10000:
            anlik_endeks      = self.root.endeksQueueContainer.get()
            self.root.endeksQueueContainer.put( anlik_endeks )
            counter+= 1

        vardiya_adi       = self.Vardiya_adi_txt.GetValue()
        selected_cari_list = []
        for selected in self.cmb_cari_adi.GetChecked():
            selected_cari_list.append( str(self.cmb_cari_adi.GetClientData(selected).id))
            
        if vardiya_adi.strip() == '':
            s.uyari_message(self,MESSAGES.once_vardiya_adi_yaz,MESSAGES.sablon_iki_kelime%( MESSAGES.vardiya,MESSAGES.kaydet))
        else:
            vardiya_bas_saat = self.Vardiya_bas_saat_txt.GetValue()
            vardiya_bit_saat = self.Vardiya_bit_saat_txt.GetValue()
            if vardiya_bas_saat == vardiya_bit_saat:
                s.uyari_message(self, MESSAGES.baslangic_bitis_zamani_ayni,MESSAGES.sablon_iki_kelime%( MESSAGES.vardiya,MESSAGES.kaydet))
            else:
                if len(selected_cari_list) != 0:
                    if self.last_checked_item_id == None:
                        yeni_vardiya_id = self.db.save_data_and_get_id(table_name = 'VARDIYA',
                             column_name='VARDIYA_ADI,BASLANGIC_SAATI,BITIS_SAATI,ID_ACTIVE',
                             values=("'%s','%s','%s','%s'"%(vardiya_adi,vardiya_bas_saat,vardiya_bit_saat,2 )))
                        
                        for cari in selected_cari_list:
                            self.db.save_data(table_name = 'VARDIYA_CARI',
                                 column_name='ID_CARI_HESAP,ID_VARDIYA,ID_ACTIVE,TARIH',
                                 values=("%s,%s,1,datetime('now', 'localtime')"%(cari,yeni_vardiya_id)))
                        
                    else:
                        vardiya_cari_list = self.db.get_raw_data_all( selects='ID_CARI_HESAP', table_name='VARDIYA_CARI',
                                                                where='ID_VARDIYA=%s AND ID_ACTIVE=1' %self.last_checked_item_id)
                        db_cari_list      = map( lambda x:str(x[0]), vardiya_cari_list)
                        
                        pasif_cari_list_str = ','.join ( list( set( db_cari_list )       - set(selected_cari_list)))
                        aktif_cari_list = list( set( selected_cari_list ) - set(db_cari_list))
                       
                        self.db.update_data(table_name='VARDIYA_CARI',column_name='ID_ACTIVE',
                                       values=('2'),where='ID_VARDIYA=%s AND ID_CARI_HESAP in (%s)'%(self.last_checked_item_id, pasif_cari_list_str))
                        for cari in aktif_cari_list:
                            self.db.save_data(table_name = 'VARDIYA_CARI',
                                     column_name='ID_CARI_HESAP,ID_VARDIYA,ID_ACTIVE,TARIH',
                                     values=("%s,%s,1,datetime('now', 'localtime')"%(cari,self.last_checked_item_id)))    
                        checked_value = 2
                        if self.cb_aktif.IsChecked():
                            vardiya_total_para  = 0
                            vardiya_total_litre = 0
                            total_satis_sayisi  = 0
                            for k,v in anlik_endeks.iteritems():
                                vardiya_total_para  += float( v[1].total_amount  )
                                vardiya_total_litre += float( v[1].total_volume  )
                                total_satis_sayisi  += float( v[1].sale_quantity )
                            aktif_vardiya = self.db.getAktifVardiya()
                            self.db.update_data( table_name = 'VARDIYA_AKTIF',column_name='VARDIYA_KAPANIS_TUTAR,VARDIYA_SATIS_SAYISI',
                                                values=('%s,%s'%(vardiya_total_para, total_satis_sayisi)), where="ID_VARDIYA_AKTIF=%s"%aktif_vardiya.ID_VARDIYA_AKTIF)
                            checked_value = 1
                            self.db.update_data(table_name='VARDIYA',column_name='ID_ACTIVE',values=("2"),where="ID_ACTIVE != 3")
                            if not  self.selected_aktif :
                                self.db.save_data(table_name = 'VARDIYA_AKTIF',
                                         column_name='ID_VARDIYA,TARIH,VARDIYA_ACILIS_TUTAR,VARDIYA_KAPANIS_TUTAR,VARDIYA_SATIS_SAYISI',
                                         values=("%s,datetime('now', 'localtime'),%s,0,0"%(self.last_checked_item_id,vardiya_total_para)))  
                        self.db.update_data(table_name='VARDIYA',column_name='VARDIYA_ADI,BASLANGIC_SAATI,BITIS_SAATI,ID_ACTIVE',
                                       values=("'%s','%s','%s','%s'"%(vardiya_adi,vardiya_bas_saat,vardiya_bit_saat,checked_value) ),where='ID_VARDIYA=%s'%self.last_checked_item_id)
                        
                    self.VardiyaVeriReset(None)
                    s.ok_message(self,MESSAGES.sablon_iki_kelime%( MESSAGES.vardiya,MESSAGES.kaydedildi),MESSAGES.sablon_iki_kelime%( MESSAGES.vardiya,MESSAGES.kaydet))
                else:
                    s.uyari_message(self,MESSAGES.once_vardiya_personeli_sec,MESSAGES.sablon_iki_kelime%( MESSAGES.vardiya,MESSAGES.kaydet))
                
    def OnVardiyaSil(self, event ):
        if self.last_checked_item_id != None:
            self.db.update_data(table_name='VARDIYA',column_name='ID_ACTIVE',values='3',where='ID_VARDIYA=%s'%self.last_checked_item_id)    
            s.ok_message(self,MESSAGES.sablon_iki_kelime%(MESSAGES.vardiya,MESSAGES.silindi),MESSAGES.sablon_iki_kelime%( MESSAGES.vardiya,MESSAGES.sil))
            self.VardiyaVeriReset(None)
        else:
            s.uyari_message(self,MESSAGES.once_silinecek_sec,MESSAGES.sablon_iki_kelime%( MESSAGES.vardiya,MESSAGES.sil))
    
    def VardiyaVeriReset( self,event ):
        self.last_checked_item_id = None
        self.Vardiya_adi_txt.SetValue("")
        self.Vardiya_bas_saat_txt.SetValue("00:00:00")
        self.Vardiya_bit_saat_txt.SetValue("00:00:00")
        self.cmb_cari_adi.DeselectAll()
        self.cb_aktif.SetValue( False )
        for selected in self.cmb_cari_adi.GetChecked():
            self.cmb_cari_adi.Check( selected, False )
        s.updateWindow(self.liste, self.CreateVardiyaListe)
class win_Vardiya_Satis_Ekrani(wx.Frame ):
    def __init__(self, parent, id_, title):
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= ( wx.DEFAULT_FRAME_STYLE | wx.FRAME_NO_TASKBAR ))
        self.db = param.get_root( self ).DB
        self.O  = param.get_root( self ).O
        self.root  = param.get_root( self ) 
        self.yatay_mod = 1
        self.scroll = wx.ScrolledWindow(self, 1, style=wx.SIMPLE_BORDER)
        self.scroll.SetScrollbars(1,1,600,400)
        self.ultramainSizer = wx.BoxSizer( wx.VERTICAL )
        self.mainSizer = wx.GridBagSizer(3,3)
        self.scroll.SetSizer( self.ultramainSizer )
        self.createLayout()
        self.image_start = 20001
        self.image_count = self.image_start + 86

        s.setPanelListBg( self )
    
    def createLayout( self ):
        self.Freeze() 
        q = self.root.pompaQueueContainer
        self.aktif_vardiya = self.db.get_raw_data_all(table_name="VIEW_VARDIYA_ADI",
                                                         where="ID_VARDIYA_AKTIF = (select MAX(ID_VARDIYA_AKTIF) from VIEW_VARDIYA_ADI)",
                                                         dict_mi = 'object')
        if len( self.aktif_vardiya ) == 0:
            s.uyari_message(self, MESSAGES.aktif_vardiya_yok, MESSAGES.sablon_iki_kelime( MESSAGES.vardiya, MESSAGES.fault))
        else:
            self.aktif_vardiya = self.aktif_vardiya[0]
            cari_sayisi        = self.db.get_raw_data_all( table_name="VARDIYA_CARI",
                                where="ID_VARDIYA=%s AND ID_ACTIVE=1"%self.aktif_vardiya.ID_VARDIYA,dict_mi="object",order="ID_CARI_HESAP")
            cari_totals        = self.db.get_raw_data_all( table_name="POMPA_SATIS_KAYIT", selects="ID_CARI_HESAP,sum (TUTAR) AS TOTAL",
                                                          where = "ID_VARDIYA_AKTIF = %s GROUP BY ID_CARI_HESAP" % self.aktif_vardiya.ID_VARDIYA_AKTIF,
                                                          dict_mi = 'object')
            
            row_num = 0
            col_num = 0
            self.vardiya_dict = {}
            self.aboutPanel = createAboutVardiya( self.scroll, self.aktif_vardiya,self.db )
            self.ultramainSizer.Add( self.aboutPanel,0,wx.EXPAND )    
            for cari in cari_sayisi:
                p = VardiyaCariPanel( self.scroll )
                cari_prop    = self.db.get_raw_data_all(table_name='CARI_HESAPLAR',
                                                     where='ID_CARI_HESAP=%s'%cari.ID_CARI_HESAP,
                                                     dict_mi = 'object')[0]
                total        = filter(lambda x:x.ID_CARI_HESAP == cari.ID_CARI_HESAP, cari_totals)
                
                if len(total) == 1:
                    p.lbl_cari_toplam_txt.SetLabel( util.data_formatla(str(total[0].TOTAL),'currency') )
                
                p.lbl_cari_adi_txt.SetLabel( cari_prop.CARI_ADI)
                self.mainSizer.Add( p, (row_num, col_num ),(1,1),(wx.ALL|wx.EXPAND))
                self.vardiya_dict[cari.ID_CARI_HESAP] = p
                col_num +=1
                if col_num%self.yatay_mod == 0:
                    col_num = 0
                    row_num+=1
            self.ultramainSizer.Add( self.mainSizer,0,wx.ALIGN_TOP)    
            self.Thaw()
            
            self.vardiya_thread = KThread(target = self.VardiyaSatisDegerleriniSetEt, kwargs={'q':q,
                                                                                     'p_dict':self.vardiya_dict,
                                                                                     'aktif_vardiya_id' : self.aktif_vardiya.ID_VARDIYA_AKTIF,
                                                                                     'ust_panel':self.aboutPanel
                                                                                     })
            self.vardiya_thread.start()
            self.GetParent().thread_list.append( self.vardiya_thread )
            self.Bind(wx.EVT_CLOSE, self.OnClose )

    def OnClose( self, event ):
        self.vardiya_thread.kill()
        self.Destroy()
        
    def VardiyaSatisDegerleriniSetEt( self, q, p_dict, aktif_vardiya_id,ust_panel):
        formatted_type  = {}
        pompa_formatter = PompaRequestFormatla()
        db              = DBObjects() 
        while 1:
            
            formatted_type = q.get()
            q.put( formatted_type )
            try:
                for k,v in formatted_type.iteritems():
                    if v and hasattr(v,'cari_hesap_id'):
                        vardiya_panel    =  p_dict[v.cari_hesap_id]
                        vardiya_panel.Freeze()
                        vardiya_panel.SetBackgroundColour( v.color )
                        try:
                            vardiya_panel.lbl_yakit_adi_txt.SetLabel( v.yakit_adi)
                        except:
                            pass
                        try:
                            vardiya_panel.lbl_plaka_txt.SetLabel( v.plate_number)
                        except:
                            pass
                        try:
                            vardiya_panel.lbl_birim_fiyat_txt.SetLabel( str(util.data_formatla(str(v.unit_price),'currency')) + ' %s'%MESSAGES.para_birimi )
                            vardiya_panel.lbl_verilen_litre_txt.SetLabel( util.data_formatla(str(v.filling_volume),'digit') )
                            vardiya_panel.lbl_tutar_txt.SetLabel( str(util.data_formatla(str(v.filling_amount),'currency')) + ' %s'%MESSAGES.para_birimi  )
                        except:
                            pass
                        if v.satis_db_basilacak == 1:
                            next_toplam          = db.get_raw_data_all( table_name="POMPA_SATIS_KAYIT", selects="sum (TUTAR) AS TOTAL",
                                                              where   = "ID_VARDIYA_AKTIF = %s AND ID_CARI_HESAP=%s" % (aktif_vardiya_id, v.cari_hesap_id ),
                                                              dict_mi = 'object')[0].TOTAL
                            vardiya_panel.lbl_cari_toplam_txt.SetLabel( util.data_formatla(str(next_toplam),'currency') )
                            
                            next_toplam_t          = db.get_raw_data_all( table_name="POMPA_SATIS_KAYIT", selects="sum (TUTAR) AS TOTAL",
                                                              where   = "ID_VARDIYA_AKTIF = %s" % (aktif_vardiya_id ),
                                                              dict_mi = 'object')[0].TOTAL
                            ust_panel.lbl_total_txt.SetLabel( util.data_formatla(str(next_toplam_t),'currency') )
                            
                            
                        vardiya_panel.Update()
                        vardiya_panel.Thaw()
            except:
                pass
            
            time.sleep( sabitler.arabirimEkranBekleme ) 
class createAboutVardiya( wx.Panel ):
    def __init__( self, parent, aktif_vardiya, db ):
        
        wx.Panel.__init__(self, parent)
        cari_totals        = db.get_raw_data_all( table_name="POMPA_SATIS_KAYIT", selects="sum (TUTAR) AS TOTAL",
                                                          where = "ID_VARDIYA_AKTIF = %s" % aktif_vardiya.ID_VARDIYA_AKTIF,
                                                          dict_mi = 'object')[0]
        self.sizer         = wx.FlexGridSizer( 1,19,1,1)
        
        self.lbl_vardiya_adi      = wx.StaticText(self,-1,MESSAGES.sablon_iki_kelime%( MESSAGES.vardiya, MESSAGES.adi ))
        self.lbl_vardiya_adi_txt  = wx.StaticText(self,-1,aktif_vardiya.VARDIYA_ADI)

        self.lbl_baslangic        = wx.StaticText(self,-1,'%s :'%MESSAGES.baslangic)
        self.lbl_baslangic_txt    = wx.StaticText(self,-1,aktif_vardiya.BASLANGIC_SAATI)
        
        self.lbl_bitis            = wx.StaticText(self,-1,'%s :'%MESSAGES.bitis)
        self.lbl_bitis_txt        = wx.StaticText(self,-1,aktif_vardiya.BITIS_SAATI)
        
        self.lbl_total            = wx.StaticText(self,-1,MESSAGES.sablon_iki_kelime_dot%( MESSAGES.vardiya,MESSAGES.toplam ))
        self.lbl_total_txt        = wx.StaticText(self,-1,util.data_formatla(str(cari_totals.TOTAL),'currency'))

        s.setStaticTextPompaHeader        ( self.lbl_vardiya_adi )
        s.setStaticTextPompaPanelList     ( self.lbl_vardiya_adi_txt )
        
        s.setStaticTextPompaHeader   ( self.lbl_baslangic )
        s.setStaticTextPompaPanelList     ( self.lbl_baslangic_txt )
        
        s.setStaticTextPompaHeader   ( self.lbl_bitis )
        s.setStaticTextPompaPanelList     ( self.lbl_bitis_txt )
        
        s.setStaticTextPompaHeader   ( self.lbl_total )
        s.setStaticTextPompaPanelList     ( self.lbl_total_txt )
        #s.setPanelListBg ( self )
        self.sizer.AddMany( [
                             ((10,75)),
                             (self.lbl_vardiya_adi ,-1, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                             (self.lbl_vardiya_adi_txt ,-1, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                             ((30,20)),
                             (self.lbl_baslangic ,-1, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                             (self.lbl_baslangic_txt ,-1, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                             ((30,20)),
                             (self.lbl_bitis ,-1, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                             (self.lbl_bitis_txt ,-1, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                             ((30,20)),
                             (self.lbl_total ,-1, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                             (self.lbl_total_txt ,-1, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ])
        
        self.SetSizer( self.sizer )
    
class VardiyaCariPanel( wx.Panel ):
    
    def __init__(self, parent ):
        wx.Panel.__init__(self, parent,style=wx.SIMPLE_BORDER)

        s.setPanelListBg( self )
        self.topsizer = wx.GridSizer(1, 16, 5, 2)
        self.root    = param.get_root( self ) 
        self.SetSizer( self.topsizer )

        self.SetBackgroundColour( sabitler.idle_rengi )
        self.lbl_yakit_adi      = wx.StaticText(self,-1,'%s:'%MESSAGES.yakit)
        self.lbl_yakit_adi_txt  = wx.StaticText(self)

        self.lbl_cari_adi       = wx.StaticText(self,-1,MESSAGES.sablon_iki_kelime_dot%( MESSAGES.cari[0:1], MESSAGES.adi ))
        self.lbl_cari_adi_txt   = wx.StaticText(self)

        s.setStaticTextHeader   ( self.lbl_yakit_adi )
        s.setStaticTextList     ( self.lbl_yakit_adi_txt )
        
        s.setStaticTextHeader   ( self.lbl_cari_adi )
        s.setStaticTextList     ( self.lbl_cari_adi_txt )
        
        
        self.lbl_birim_fiyat      = wx.StaticText(self,-1,MESSAGES.sablon_iki_kelime_dot%( MESSAGES.birim[0:1], MESSAGES.fiyat ))
        self.lbl_birim_fiyat_txt  = wx.StaticText(self,-1,'0.0')
        s.setStaticTextHeader( self.lbl_birim_fiyat )
        s.setStaticTextList  ( self.lbl_birim_fiyat_txt )
        
        self.lbl_verilen_litre     = wx.StaticText(self,-1,'%s :'%MESSAGES.litre)
        self.lbl_verilen_litre_txt = wx.StaticText(self,-1,'0.0')
        s.setStaticTextHeader( self.lbl_verilen_litre )
        s.setStaticTextList  ( self.lbl_verilen_litre_txt )
        
        self.lbl_tutar_txt            = wx.StaticText(self,-1,'0.0 %s' %MESSAGES.para_birimi)
        self.lbl_tutar                = wx.StaticText(self,-1,'%s :'%MESSAGES.tutar)
        s.setStaticTextHeader( self.lbl_tutar )
        s.setStaticTextList( self.lbl_tutar_txt )
        
        self.lbl_plaka_txt            = wx.StaticText(self,-1,'---')
        self.lbl_plaka                = wx.StaticText(self,-1,'%s:'%MESSAGES.plaka)
        s.setStaticTextHeader( self.lbl_plaka )
        s.setStaticTextList( self.lbl_plaka_txt )
        
        self.lbl_cari_toplam = wx.StaticText(self,-1,MESSAGES.sablon_iki_kelime%( MESSAGES.cari[0:1],MESSAGES.tutar))
        self.lbl_cari_toplam_txt = wx.StaticText(self,-1,'0.0 %s' %MESSAGES.para_birimi)
        s.setStaticTextHeader( self.lbl_cari_toplam )
        s.setStaticTextList( self.lbl_cari_toplam_txt )
        
        self.topsizer.AddMany( [#((20,75)),
                                (self.lbl_cari_adi ,0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_LEFT ),
                                (self.lbl_cari_adi_txt ,0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_LEFT ),
                                #((50,75)),
                                (self.lbl_yakit_adi  ,0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT),
                                (self.lbl_yakit_adi_txt  ,0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_LEFT),
                                ((2,10)),
                                (self.lbl_plaka ,0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_LEFT ),
                                (self.lbl_plaka_txt ,0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_LEFT ),
                                #((10,10)),
                                (self.lbl_tutar ,0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_LEFT ),
                                (self.lbl_tutar_txt ,0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_LEFT ),
                                #((50,75)),
                                (self.lbl_verilen_litre ,0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_LEFT ),
                                (self.lbl_verilen_litre_txt ,0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_LEFT ),
                                #((50,75)),
                                (self.lbl_birim_fiyat ,0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_LEFT ),
                                (self.lbl_birim_fiyat_txt ,0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_LEFT ),
                                #((50,75)),
                                (self.lbl_cari_toplam ,0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_LEFT ),
                                (self.lbl_cari_toplam_txt ,0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_LEFT )])
                                
        self.SetSizeHints(GetSystemMetrics(0)-30,50,1700,100)
        
        
        