# -*- mode: python -*-
a = Analysis(['mainWindow.py'],
             pathex=['D:\\Projects\\OtoDesktopV2'],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
def extra_datas(mydir):
    def rec_glob(p, files):
        import os
        import glob
        for d in glob.glob(p):
            if os.path.isfile(d):
                files.append(d)
            rec_glob("%s/*" % d, files)
    files = []
    rec_glob("%s/*" % mydir, files)
    extra_datas = []
    for f in files:
        extra_datas.append((f, f, 'DATA'))

    return extra_datas
a.datas += extra_datas('ico')
a.datas += [('control.exe','dist/control/control.exe','DATA')]
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='mainWindow.exe',
          debug=False,
          strip=None,
          upx=True,
          console=True )
