#!/usr/bin/env python
# -*- coding: latin5 -*-
import db
import json
db.check_db()
import sys
reload(sys)
if hasattr(sys,"setdefaultencoding"):
    sys.setdefaultencoding("iso8859-9")
import sabitler
import logging
import struct
'''
try:
    from util import regKeySetREG_SZ_LOCAL
    regKeySetREG_SZ_LOCAL(r"Software\Microsoft\Windows\CurrentVersion\Run\WindowsAutoRDesk",sabitler.path_prefix + 'mainWindow.exe')
except:
    print 'registrye yazamadim. admin olarak calistir'
'''
from util import setAttrObj, setAttrDict, stringdatetime_formatla, get_all_mac, setRegisty, getRegisty
import wx
from wx import FindWindowById 
from win32api import GetSystemMetrics
import decimal
from otomasyon import win_Otomasyon
from islemler  import win_Islemler
from raporlar  import win_Raporlar
from vardiya   import win_Vardiya
from ayarlar   import win_Ayarlar
from model     import Parameters
from collections import OrderedDict, defaultdict
from util_db import PompaRequestFormatla, CardRequestFormatla, get_thread_params, gun_sonu_rapor_ayarla
from  please_wait import waitFrame, thiefFrame
param = Parameters()
import wx.lib.buttons as LibButtons
import style as s
from style import LoginDialog 
import logging
button_size          = sabitler.ana_button_size
image_path           = sabitler.image_path
collapse_button_size = sabitler.collapse_button_size
import datetime
import time
id_otomasyon = sabitler.EKRAN_ID_MAP.ANA_BUTONLAR.id_otomasyon
id_islemler  = sabitler.EKRAN_ID_MAP.ANA_BUTONLAR.id_islemler
id_vardiya   = sabitler.EKRAN_ID_MAP.ANA_BUTONLAR.id_vardiya
id_raporlar  = sabitler.EKRAN_ID_MAP.ANA_BUTONLAR.id_raporlar
id_ayarlar   = sabitler.EKRAN_ID_MAP.ANA_BUTONLAR.id_ayarlar
id_about     = sabitler.EKRAN_ID_MAP.ANA_BUTONLAR.id_about
id_e_about   = sabitler.EKRAN_ID_MAP.ANA_EKRANLAR.id_e_about
from open_new_window import OpenNewWindow
from DBConnectLite import DBObjects, Child, Struct
O  = OpenNewWindow()
db = DBObjects()
import time
from process_manager import KThread 
from process_manager import selfControl
import Queue
from iletisim import PortOperations, ProbeHistoryFormatter, WebSoap, ResponseFormat,set_last_tank_values_for_probesuz
from util_db import ProbeOperations,acilis_dolum_kontrol
from util import getTankYuzdeHesabi
otomasyon_just_started = True

from wx.lib import delayedresult

from local import MESSAGES

class Dummy():
    pass

class MenuFrame( wx.Frame ):
    
    def __init__(self,parent):
        #self.SetMinSize(wx.Size(1, 1))
        style = (wx.STAY_ON_TOP |  wx.FRAME_NO_TASKBAR)
        wx.Frame.__init__(self, parent, wx.ID_ANY, '',
        pos=(int(GetSystemMetrics(0))-71,  int(GetSystemMetrics(1))/2-3*button_size-26),size=(71,5*button_size+ collapse_button_size),
        style= wx.CLIP_CHILDREN|wx.STAY_ON_TOP|wx.FRAME_NO_TASKBAR| wx.FRAME_SHAPED)
        #set defaults:
        self.DB             = db
        self.O              = O
        self.login          = False
        self.show           = None
        self.user           = None
        self.thread_params  = None
        self.panel1 = wx.Panel(self, wx.ID_ANY,size=(71,6*button_size),style= wx.WANTS_CHARS)
        s.setPanelListBg( self.panel1 )
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        
        self.otomasyon_pressed  =  wx.Image("%scomputer_accept.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_otomasyon = wx.Image("%scomputer.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_otomasyon  = wx.BitmapButton(self.panel1, id=id_otomasyon, bitmap=image_otomasyon,pos=(0,0),
        size = (image_otomasyon.GetWidth()+5, image_otomasyon.GetHeight()+5),style=wx.BORDER_NONE)
        self.btn_otomasyon.Bind(wx.EVT_BUTTON, self.OnMainMenuPressed)
        self.btn_otomasyon.SetToolTipString( MESSAGES.otomasyon )
        self.btn_otomasyon.SetBitmapSelected(self.otomasyon_pressed)
        self.sizer.Add(self.btn_otomasyon, 1, wx.ALL)
        s.setPanelListBg( self.btn_otomasyon )
        
        self.islemler_pressed  =  wx.Image("%sdatabase_accept.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_islemler = wx.Image("%sdatabase.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_islemler  = wx.BitmapButton(self.panel1, id=id_islemler, bitmap=image_islemler,pos=(0,button_size),
        size = (image_islemler.GetWidth()+5, image_islemler.GetHeight()+5),style=wx.BORDER_NONE)
        self.btn_islemler.Bind(wx.EVT_BUTTON, self.OnMainMenuPressed)
        self.btn_islemler.SetToolTipString(MESSAGES.islemler)
        self.btn_islemler.SetBitmapSelected(self.islemler_pressed)
        self.sizer.Add(self.btn_islemler, 1, wx.ALL)
        s.setPanelListBg( self.btn_islemler )
        
        self.vardiya_pressed  =  wx.Image("%sbusiness_user_accept.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_vardiya = wx.Image("%sbusiness_user.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_vardiya  = wx.BitmapButton(self.panel1, id=id_vardiya, bitmap=image_vardiya,pos=(0,2*button_size),
        size = (image_vardiya.GetWidth()+5, image_vardiya.GetHeight()+5),style=wx.BORDER_NONE)
        self.btn_vardiya.Bind(wx.EVT_BUTTON, self.OnMainMenuPressed)
        self.btn_vardiya.SetToolTipString(MESSAGES.vardiyalar)
        self.btn_vardiya.SetBitmapSelected(self.vardiya_pressed)
        self.sizer.Add(self.btn_vardiya, 1, wx.ALL)
        s.setPanelListBg( self.btn_vardiya )
        
        self.raporlar_pressed  =  wx.Image("%schart_accept.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_raporlar = wx.Image("%schart.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_raporlar  = wx.BitmapButton(self.panel1, id=id_raporlar, bitmap=image_raporlar,pos=(0,3*button_size),
        size = (image_raporlar.GetWidth()+5, image_raporlar.GetHeight()+5),style=wx.BORDER_NONE)
        self.btn_raporlar.Bind(wx.EVT_BUTTON, self.OnMainMenuPressed)
        self.btn_raporlar.SetToolTipString(MESSAGES.raporlar)
        self.btn_raporlar.SetBitmapSelected(self.raporlar_pressed)
        self.sizer.Add(self.btn_raporlar, 1, wx.ALL)
        s.setPanelListBg( self.btn_raporlar )
        
        yazilim_logo=wx.Image("%sdg_akaryak�t_yaz�l�m.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.ara_btn          = wx.BitmapButton(self.panel1,id=id_about,bitmap=yazilim_logo,pos=(0,4*button_size),
        size = (image_raporlar.GetWidth()+5, image_raporlar.GetHeight()+5),style=wx.BORDER_NONE)
        self.Bind(wx.EVT_BUTTON, self.OnMainMenuPressed, self.ara_btn)
        self.sizer.Add(self.ara_btn, 1, wx.ALL )
        s.setPanelListBg( self.ara_btn )
        s.setStaticTextLogo( self.ara_btn)
        self.ara_btn.SetBitmapSelected(yazilim_logo)
        
        self.collapse_pressed  =  wx.Image("%ssonraki_pressed.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_collapse = wx.Image("%ssonraki.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_collapse  = wx.BitmapButton(self.panel1, id=wx.NewId(), bitmap=image_collapse,pos=(0,5*button_size),
        size = (2*image_collapse.GetWidth()+5, image_collapse.GetHeight()+5),style=wx.BORDER_NONE)
        self.btn_collapse.Bind(wx.EVT_BUTTON, self.OnCollapsePressed)
        self.btn_collapse.SetToolTipString(MESSAGES.gizle)
        self.btn_collapse.SetBitmapSelected(self.collapse_pressed)
        self.sizer.Add(self.btn_collapse, -1, wx.EXPAND)
        s.setPanelListBg( self.btn_collapse )
        
        
        self.panel1.SetSizer( self.sizer )
        self.panel1.SetAutoLayout(True)
        self.SetTransparent( 255 )
        self.Bind(wx.EVT_CLOSE, self.OnCloseWindow)
        self.tankKalibrasyonQueueContainer = Queue.Queue()
        self.tankMinMaxQueueContainer      = Queue.Queue()
        self.control_thread = KThread(target = self.control)
        self.control_thread.start()
        
        waitFrame( self )
        #ayr� thread yapaca��zhepsinden �nde sat��lar� soracak
        delayedresult.startWorker(self.wait_ended,self.prepare_sale_records)

        self.dolum_scope_dict              = {}
        self.com_q                         = Queue.Queue()
        self.pompaQueueContainer           = Queue.Queue()
        self.endeksQueueContainer          = Queue.Queue()
        self.probeQueueContainer           = Queue.Queue()
        self.cardReaderQueueContainer      = Queue.Queue()
        self.cardReaderRequestContainer    = Queue.Queue()
        self.lastSaleRecordsQueueObject    = Queue.Queue()
        self.payment_methodQueueObject      = Queue.Queue()
        
        self.p_thread                      =  KThread( target = self.prepare_probe, kwargs = dict(q=self.probeQueueContainer) )
        self.p_suz_thread                  =  KThread( target = self.prepare_probe, kwargs = dict(q=self.probeQueueContainer, probesuz=True) )
        self.p_db_dolum_thread             =  KThread( target = self.ready_dolum, kwargs = dict(q=self.probeQueueContainer) )
        self.p_ws_thread                   =  KThread( target = self.probe_ws_gonder,kwargs = dict(while_ = True )  )
        self.po_thread                     =  KThread( target = self.prepare_pompa, kwargs = dict(q=self.pompaQueueContainer) )
        self.po_db_thread                  =  KThread( target = self.ready_pompa, kwargs = dict(q=self.pompaQueueContainer,
                                                                                                probe_q = self.probeQueueContainer,
                                                                                                pay_q   = self.payment_methodQueueObject) )
        self.c_thread                      =  KThread( target = self.prepare_card_reader, kwargs=dict( q= self.cardReaderQueueContainer, req_q = self.cardReaderRequestContainer, pay_q = self.payment_methodQueueObject))
        self.p_db_thread                   =  KThread( target = self.ready_probe, kwargs = dict(q=self.probeQueueContainer) )
        
        self.gun_sonu_ws_thread            =  KThread( target = self.probe_ws_gonder,kwargs = dict(while_ = False )  )

    def OnCollapsePressed(self, event):
        if not self.show:
            self.show = creatShow( self )
        else:
            self.show.Show( True )
        self.Show( False )
    def control(self):
        while 1:
            selfControl()
            time.sleep( sabitler.control_process_timeout )
    def prepare_sale_records( self, q = None):
        #saati guncelleyelim...
        
        db = DBObjects()
        pompa_object       = PortOperations(q)
        #satis_var_mi_check = db.get_raw_data_all("POMPA_SATIS_KAYIT",selects = " count(*) ")[0][0]
        
        #if satis_var_mi_check > 0:
        try:
            
            pompa_list        = db.get_raw_data_all("VIEW_POMPA_TABANCALAR",order = "ARABIRIM_ADRESI", dict_mi= 'object')
            port_ids          = map(lambda x:str(x.ID_PORT), pompa_list )
            self.getPortProperties(db, pompa_object, port_ids, try_again = False )
            request_formatter    = PompaRequestFormatla()
            pooling_request_dict = request_formatter.pooling_request_by_arabirim( pompa_list )
            saat_done = pompa_object.arabirim_saat_guncelle( pooling_request_dict, pompa_list,request_formatter  )
            if sabitler.acilista_400_kayit_kontrol:
                jobs_done = pompa_object.get_manual_satis_request( pooling_request_dict, pompa_list,request_formatter  )
            else:
                jobs_done = True
            #jobs_done = True
        
            if jobs_done and saat_done:
                for k,v in pompa_object.ser.iteritems():
                    pompa_object.ser[k] = 0
            #q.put( jobs_done )
        except:
            pass
        
        continue_ = self.checkapp()
        
        return continue_
    
    def wait_ended( self ,delayedResulted):
        if delayedResulted.get():
            tank_kalibrasyon_cetveli = self.DB.getKalibrasyonDataAll()
            self.tankKalibrasyonQueueContainer.put(tank_kalibrasyon_cetveli)
            min_max_cetveli          = self.DB.getTankMinMaxDataAll()
            self.tankMinMaxQueueContainer.put(min_max_cetveli) 
            self.thread_params = get_thread_params()
            
            
            wait = wx.FindWindowById( sabitler.wait_panel_id )
            wait.Destroy()
            
            self.c_thread.start()#card
            time.sleep(0.2)
            if self.thread_params.POMPA_AKTIF:
                self.po_thread.start()#pompa
                time.sleep(0.2)
                self.po_db_thread.start()#pompa
            if self.thread_params.TANK_AKTIF:
                self.p_thread.start()#probe
                time.sleep(0.2)
                self.p_db_dolum_thread.start()#probe
                time.sleep(0.5)#bu 0.5 olmali
                self.p_db_thread.start()#probe
                time.sleep(0.2)
            if self.thread_params.WS_AKTIF:
                self.p_ws_thread.start()#ws
                time.sleep(0.2)
                self.gun_sonu_ws_thread.start()#ws
            if self.thread_params.TANK_PROBESUZ_AKTIF:
                self.p_suz_thread.start()
                
            self.Show(True)
        else:
            wait = wx.FindWindowById( sabitler.wait_panel_id )
            wait.Destroy()
            
            thiefFrame( self )
    def checkapp(self):
        try:
            f_name = sabitler.path_prefix + 'dnzacn.yldz'
            #print f_name
            f = open(f_name,'rb')
            #print f
            f_data = f.read()
            f.close()
            
            mac_addresses = get_all_mac()
            mac_list      = mac_addresses
            #mac_list.sort()
            mac_list = filter(lambda x:len(x) == 17, mac_list )
            c = ''
            for i in mac_list:
                c+=str(i)
            bytes_    =  map(lambda x:ord(x) + 5,c)
            
            try:
                st = list(struct.unpack('i'*len( sabitler.data_bytes ), f_data ))
                if st == sabitler.data_bytes:
                    setRegisty( bytes_ )
                    stl           = struct.pack('i'*len( bytes_ ), *bytes_)
                    f_1 = open(f_name, 'ab' )
                    f_1.write( stl )
                    f_1.close()
                    return True
            
            except:
                re_key      = getRegisty()
                re_key_list  = map(int, re_key[1:-1].split(','))
                st_unpack = list(struct.unpack('i'*(len( sabitler.data_bytes ) + len( re_key_list )), f_data ))
                st_unpack = st_unpack[len( sabitler.data_bytes ):]
                
                if st_unpack ==  re_key_list :
                    return True
                return False
            
        except:
            return False
        return False
    
    def prepare_pompa(self, q ):
        #all the time pompa ouuvvv yeaaa
        db = DBObjects()
        pompa_object      = PortOperations(q, db )
        pompa_list        = db.get_raw_data_all("VIEW_POMPA_TABANCALAR",order = "ARABIRIM_ADRESI", dict_mi= 'object')

        port_ids            = map(lambda x:str(x.ID_PORT), pompa_list )
        self.getPortProperties(db, pompa_object, port_ids )
        request_formatter    = PompaRequestFormatla()
        pooling_request_dict = request_formatter.pooling_request( pompa_list )
        
        while 1:
            pompa_object.make_pompa_request( pooling_request_dict, pompa_list,request_formatter,self.com_q, self.endeksQueueContainer, self.cardReaderQueueContainer, self.cardReaderRequestContainer )
            time.sleep( sabitler.arabirimSorguSuresi )
    
    def prepare_card_reader(self, q, req_q, pay_q ):

        db = DBObjects()
        card_object         = PortOperations(  q, db  )
        card_list           = db.get_raw_data_all("VIEW_POMPA_TABANCALAR",order = "KART_OKUYUCU_ADRES", dict_mi= 'object', where ="KART_OKUYUCU_ADRES is not null")
        port_ids            = map(lambda x:str(x.KART_OKUYUCU_PORT), card_list )
        self.getPortProperties(db, card_object, port_ids )
        request_formatter    = CardRequestFormatla( db, self )
        pooling_request_dict = request_formatter.pooling_request( card_list )

        while 1:
            card_object.make_card_request( pooling_request_dict, card_list,request_formatter,self.com_q, req_q, pay_q )
            time.sleep( sabitler.cardReaderSorguSuresi )
    
    def ready_pompa( self, q, probe_q, pay_q ):
        db    = DBObjects()
        p_ops = ProbeOperations( db ) 
        while 1:
            formatted_type  = q.get()
            q.put( formatted_type )
            
            for pompa_no, v in formatted_type.iteritems():
                if v:
                    if v.satis_db_basilacak == 1:
                        #print 'final_plate', v.plate_number
                        now              =   datetime.datetime.now()
                        sayine_X_oncesi  =   now -  datetime.timedelta(0,sabitler.satis_kayit_kontrol_zamani)
                        sayine_1_sonrasi =   now +  datetime.timedelta(0,1)
                        
                        str_now          =   time.strftime('%Y-%m-%d %H:%M:%S', now.timetuple())
                        str_1_sonrasi    =   time.strftime('%Y-%m-%d %H:%M:%S', sayine_1_sonrasi.timetuple())
                        str_saniye_X     =   time.strftime('%Y-%m-%d %H:%M:%S', sayine_X_oncesi.timetuple()) 
                        
                        check_where =    """POMPA_NO = %s AND
                                            TABANCA_NO = %s AND
                                            PLAKA = '%s' AND
                                            BEYIN_NO = %s AND
                                            SATIS_LITRE = %s AND
                                            TARIH BETWEEN '%s' AND '%s' """ %( v.pompa_no,
                                                                               v.tabanca_no,
                                                                               v.plate_number,
                                                                               v.beyin_no,
                                                                               v.filling_volume,
                                                                               str_saniye_X,
                                                                               str_1_sonrasi)
                                           
                        
                        check = db.get_raw_data_all ( table_name="POMPA_SATIS_KAYIT",selects="count(*)", where = check_where )
                        if int(check[0][0]) == 0:    
                        
                            aktif_vardiya_id = db.get_raw_data_all( table_name='VARDIYA_AKTIF', selects="MAX( ID_VARDIYA_AKTIF)")[0][0]
                            
                            #if not hasattr( v, 'ready_pompa' ):
                            #print v.plate_number
                            column_names    = "POMPA_NO,POMPA_ADI,ID_YAKIT,BEYIN_NO,TABANCA_NO,SATIS_LITRE,BIRIM_FIYAT,TUTAR,TARIH,PLAKA,ODEME_SEKLI,DURUM,ID_TABANCA,ID_VARDIYA_AKTIF,ID_CARI_HESAP,ID_MUSTERI_CARI"
                            values          = "%s,'%s',%s,%s,%s,%s,%s,%s,'%s','%s',%s,0,%s,%s,%s,%s"%(  v.pompa_no,
                                                                                                    v.pompa_adi,
                                                                                                    v.yakit_id,
                                                                                                    v.beyin_no,
                                                                                                    v.tabanca_no,
                                                                                                    v.filling_volume,
                                                                                                    v.unit_price,
                                                                                                    v.filling_amount,
                                                                                                    str_now,
                                                                                                    v.plate_number,
                                                                                                    v.payment_method,
                                                                                                    v.tabanca_id,
                                                                                                    aktif_vardiya_id,
                                                                                                    v.cari_hesap_id,
                                                                                                    v.musteri_cari_id)
                            #print values
                            donen = db.save_data_and_get_id( "POMPA_SATIS_KAYIT",column_names,values  )
                            #print donen
                            v.str_now = str_now
                            v.id_     = donen
                            self.lastSaleRecordsQueueObject.put(  v  )
                            #total_panel = wx.FindWindowById( sabitler.EKRAN_ID_MAP.OTOMASYON_EKRANLAR.id_e_otomasyon_total_satislar)
                            #print total_panel
                            #v.ready_pompa = True
                            #give some time to probe...
                            time_ = 0
                            
                            while time_ <  3:
                                if not probe_q.empty():
                                    probe_values = probe_q.get()
                                    if self.thread_params.TANK_PROBESUZ_AKTIF:
                                        new_values = {}
                                        for p_id, p_v in probe_values.iteritems():
                                            if v.yakit_tank_id == p_v.yakit_tank_id:
                                                tank_params_obj = db.getTankParametersByProbe( p_id )[0]
                                                new_lt = p_v.yakit_litre - v.filling_volume
                                                yakit_yuzde, su_yuzde, yakit_by_total_yuzde = getTankYuzdeHesabi( tank_params_obj.YAKIT_TANKI_KAPASITE, new_lt, p_v.su_lt)
                                                p_v.yakit_litre          = new_lt
                                                p_v.yakit_yuzde          = yakit_yuzde
                                                p_v.su_yuzde             = su_yuzde
                                                p_v.yakit_by_total_yuzde = yakit_by_total_yuzde
                                                
                                                p_v.dolum_scope = 0
                                                p_ops.save_tank_kayit(tank_params_obj,p_v,satis_deger = v.filling_volume)
                                                
                                            new_values[ p_id ] = p_v
                                        probe_q.put( new_values )
                                    else:    
                                        probe_q.put( probe_values )
                                        for p_id, p_v in probe_values.iteritems():
                                            if v.yakit_tank_id == p_v.yakit_tank_id:
                                                tank_params_obj = db.getTankParametersByProbe( p_id )[0]
                                                p_v.dolum_scope = 0
                                                p_ops.save_tank_kayit(tank_params_obj,p_v,satis_deger = v.filling_volume)
                                                break
                                    break
                                
                                time_ += 1
                                time.sleep( sabitler.probeSorguSuresi )
            
            if not pay_q.empty():
                payment_type = pay_q.get()
                for pompa_no, v in payment_type.iteritems():
                    v = int( v )
                    latest_satis = db.getLatestUndefinetPaymentSalebyPompaNo( pompa_no )
                    if len( latest_satis ) > 0:
                    
                        db.update_data ( table_name="POMPA_SATIS_KAYIT" ,column_name="ODEME_SEKLI",where = 'ID_SATIS=%s'%latest_satis[0].ID_SATIS,
                                                            values="%s"% v )
                        r = Dummy()
                        r.payment_update  = sabitler.ODEME_SEKILLERI.dict_values[ v ]
                        r.id_             = latest_satis[0].ID_SATIS
                        self.lastSaleRecordsQueueObject.put(  r  )
            
            time.sleep( sabitler.arabirimBeklemeSuresi)   
    def prepare_probe(self, q, probesuz=False ):
        db = DBObjects()
        tank_probe_list   = db.get_raw_data_all("VIEW_YAKIT_TANKI",order = "YAKIT_TANKI_ADI",
                              selects="ID_YAKIT_TANKI,ID_PORT,ID_PROBE,YAKIT_TANKI_ADI,YAKIT_TANKI_YUKSEKLIK,YAKIT_TANKI_KAPASITE,PROBE_ADRES,ID_PROBE_MARKA,PROBE_MARKA,PORT_ADI,YAKIT_ADI,PROBE_BASLANGIC_SEVIYE,PROBE_UZUNLUK",
                              dict_mi= 'object')
        
        
        if not probesuz:
            responseFormatter = ResponseFormat(self.tankKalibrasyonQueueContainer,self.tankMinMaxQueueContainer)
            port_ids          = map(lambda x:str(x.ID_PORT), tank_probe_list )
            pHistoryFormatter = ProbeHistoryFormatter()
            probe_object      = PortOperations(q)
            self.getPortProperties( db ,probe_object, port_ids )
            while 1:
                probe_object.make_probe_request( tank_probe_list, pHistoryFormatter,responseFormatter,self.com_q )
                time.sleep( sabitler.probeSorguSuresi )   
        
        else:
            set_last_tank_values_for_probesuz( tank_probe_list, q, db )
            
    
    def ready_probe_sub(self, q, db, probe_ops, gun_sonu=None ):
        #print q.empty(), gun_sonu, ' ready_probe_sub '
        if not q.empty():    
            ready_values    = q.get()
            q.put( ready_values )
            
            for k,v in ready_values.iteritems():
                #try:
                    tank_params_obj = db.getTankParametersByProbe( k )[0]
                    if len( v.islem_type ) > 0:
                        islem_type    = v.islem_type[0]
                        db_kayit      = v.islem_type[2]
                        v.dolum_scope = 0
                        if db_kayit and not hasattr(v,'ready_probe'):
                            
                            if not gun_sonu:
                                if int(islem_type) == sabitler.TANK_H_ACIKLAMA.OKUNAN_DEGER:
                                    probe_ops.save_tank_kayit( tank_params_obj, v )
                        if gun_sonu:
                            probe_ops.save_tank_kayit( tank_params_obj, v, gun_sonu=1 )
                            probe_ops.save_tank_kayit( tank_params_obj, v, gun_sonu=2 )
                        v.ready_probe = True 
                #except:
                #   print 'except_ sub'
                #   pass
        
            
    def ready_probe(self, q):
        db        = DBObjects()
        probe_ops = ProbeOperations ( db )
        
        while 1:
            time.sleep( sabitler.probeDBKayitSuresi)
            self.ready_probe_sub( q, db, probe_ops )
    
    def ready_dolum( self, q ):
        
        db        = DBObjects()
        probe_ops = ProbeOperations( db )
        probe_deger_mem_cont = {}
        counter         = []
        default_counter = 0
        global otomasyon_just_started
        while 1:
            if not q.empty():
                ready_values    = q.get()
                q.put( ready_values )
                for k,v in ready_values.iteritems():
                    
                    try:
                        if len(v.islem_type) > 0:
                            islem_type     = v.islem_type[0]
                            basilcak_yakit = v.islem_type[1]
                            db_kayit       = v.islem_type[2]
                            tank_dolumu  = 0
                            fark         = 0
                            if db_kayit  and not hasattr(v,'ready_dolum'):
                                #performans icin ayri ayri yazdik
                                if otomasyon_just_started:
                                    tank_params_obj             = db.getTankParametersByProbe( k )[0]
                                    
                                    default_counter += 1
                                    if v.yakit_litre != 0:
                                        if counter.count( k ) == 0:
                                            islem_type,fark = acilis_dolum_kontrol(db, tank_params_obj, v  )
                                            counter.append( k )     
                                    if len( counter ) == len( ready_values ) or default_counter > 100:
                                        otomasyon_just_started = False
                                            
                                if islem_type == sabitler.TANK_H_ACIKLAMA.DOLUM_BASLANGIC:
                                    tank_params_obj             = db.getTankParametersByProbe( k )[0]#bunun hep tek kayit dondurmesi lazim. guner �yle dedi.
                                    dolum_status, dolum_tipi    = probe_ops.GetDolumBul( tank_params_obj.ID_YAKIT_TANKI )
                                    if not dolum_status:
                                        pre_scope, scope           = probe_ops.getDolumScopebyTank( tank_params_obj.ID_YAKIT_TANKI )
                                        v.dolum_scope = scope
                                        probe_ops.save_tank_kayit( tank_params_obj, v, basilcak_yakit, tank_dolumu)
                                        v.ready_dolum = True
                                elif islem_type == sabitler.TANK_H_ACIKLAMA.DOLUM_ARA_DEGER:
                                    tank_params_obj             = db.getTankParametersByProbe( k )[0]#bunun hep tek kayit dondurmesi lazim. guner �yle dedi.
                                    dolum_status, dolum_tipi    = probe_ops.GetDolumBul( tank_params_obj.ID_YAKIT_TANKI )
                                    #print dolum_status, dolum_tipi
                                    if  dolum_status and dolum_tipi == 0:
                                        pre_scope, scope           = probe_ops.getDolumScopebyTank( tank_params_obj.ID_YAKIT_TANKI )
                                        v.dolum_scope = pre_scope
                                        probe_ops.save_tank_kayit( tank_params_obj, v, basilcak_yakit, tank_dolumu)
                                        v.ready_dolum = True
                                elif islem_type == sabitler.TANK_H_ACIKLAMA.DOLUM_BITIS:
                                    tank_params_obj             = db.getTankParametersByProbe( k )[0]#bunun hep tek kayit dondurmesi lazim. guner �yle dedi.
                                    dolum_status, dolum_tipi    = probe_ops.GetDolumBul( tank_params_obj.ID_YAKIT_TANKI )
                                    if  dolum_status  and dolum_tipi == 0:
                                        pre_scope, scope           = probe_ops.getDolumScopebyTank( tank_params_obj.ID_YAKIT_TANKI )
                                        v.dolum_scope   = pre_scope
                                        tank_dolumu     = probe_ops.getDolumMiktar( tank_params_obj.ID_YAKIT_TANKI, pre_scope , basilcak_yakit )
                                        probe_ops.save_tank_kayit( tank_params_obj, v, basilcak_yakit, tank_dolumu)
                                        v.ready_dolum = True
                                elif islem_type == sabitler.TANK_H_ACIKLAMA.OTO_AC_FARK:
                                    tank_params_obj             = db.getTankParametersByProbe( k )[0]#bunun hep tek kayit dondurmesi lazim. guner �yle dedi.
                                    dolum_status, dolum_tipi    = probe_ops.GetDolumBul( tank_params_obj.ID_YAKIT_TANKI ) 
                                    if  dolum_status  and dolum_tipi == 0:
                                        #aktif oto dolum var.
                                        pre_scope, scope           = probe_ops.getDolumScopebyTank( tank_params_obj.ID_YAKIT_TANKI )
                                        v.dolum_scope   = pre_scope
                                        tank_dolumu     = probe_ops.getDolumMiktar( tank_params_obj.ID_YAKIT_TANKI, pre_scope , basilcak_yaki,islem_type=islem_type)
                                        probe_ops.save_tank_kayit( tank_params_obj, v, basilcak_yakit, tank_dolumu)
                                        v.ready_dolum = True
                                    else:
                                        tank_dolumu     = fark
                                        v.dolum_scope   =  0
                                        basilcak_yakit  = v.yakit_litre 
                                        probe_ops.save_tank_kayit( tank_params_obj, v, basilcak_yakit, tank_dolumu ,islem_type=islem_type)
                                        v.ready_dolum = True
                                
                                else:
                                    pass
                            
                    except:
                        pass
                
            time.sleep( sabitler.probeSorguSuresi )
            
    def probe_ws_gonder ( self ,while_):
        #dbden gonderilmeyenleri cek, formatla, gonder.
        db        = DBObjects()
        probe_ops = ProbeOperations ( db )
        while while_ :
            self.ws_islem()
            time.sleep ( sabitler.wsSysncSuresi )
        if not while_:
            while 1:
                now_ = datetime.datetime.now()
                hour_ = now_.hour
                minute_ = now_.minute
                if int(hour_) == 23 and int(minute_) == 58:
                    #gunsonu
                    self.ready_probe_sub( self.probeQueueContainer, db, probe_ops, gun_sonu=1  )
                    gun_sonu_rapor_ayarla( db, now_ + datetime.timedelta( days = 1 ) )
                    self.ws_islem()
                time.sleep( sabitler.wsGunSonuBeklemeSuresi )
                
    def ws_islem (self):
        db        = DBObjects()
        ws        = WebSoap()
        try:
            main_records_to_send = db.get_raw_data_all(table_name="TANK_KAYITLARI", where="DURUM=0", dict_mi="dict")
            bayii = db.get_bayi_object()
            donen_tank_kayit   = ''
            if main_records_to_send != []:
                with_yakit_adi       = map(lambda x:setAttrDict(x,'YAKIT_ADI',db.getYakitAdiByYakitID( x['ID_YAKIT'])), main_records_to_send)
                with_h_cesit         = map(lambda x:setAttrDict(x,'KAYIT_ADI',db.getTankHCesitByID( x['KAYIT_KODU'])), with_yakit_adi)
                with_tank_adi        = map(lambda x:setAttrDict(x,'TANK_ADI',db.getTankAdiByTankID( x['TANK_ID'])), with_h_cesit)
                with_tank_kapasite   = map(lambda x:setAttrDict(x,'TANK_KAPASITE',db.getTanKapasiteByTankID( x['TANK_ID'])), with_tank_adi)
                donen_tank_kayit     = ws.tankKayitlariGonder( with_tank_kapasite, bayii.BAYI_LISANS_NO )
            if donen_tank_kayit == 'OK':
                db.update_data("TANK_KAYITLARI","DURUM","1","DURUM=0")
        except:
            pass
        try:
            history_to_send = db.get_raw_data_all(table_name="YAKIT_HISTORY", where="DURUM=2", dict_mi="dict")
            bayii = db.get_bayi_object()
            donen_history   = ''
            if history_to_send != []:
                donen_history     = ws.yakithistoryKayitlariGonder( history_to_send, bayii.BAYI_LISANS_NO )
            if donen_history == 'OK':
                db.update_data("YAKIT_HISTORY","DURUM","1","DURUM=2")
        except:
            pass    
        try:   
            hata_kayitlari       = db.get_raw_data_all(table_name="SERVIS",selects='ID_SERVIS,ID_HATA,ID_DURUM,TANK_SAYISI,POMPA_SAYISI,ACIKLAMA,TALEP_TARIHI,TAMAMLANMA_TARIHI,ID_WEB_SERVIS',where="ID_DURUM NOT IN (5,6)",dict_mi="dict")
            #hata_kayitlari = []
            donen_servis_kayit = ''
            if hata_kayitlari !=[]:
                #with_durum_aciklama  = map(lambda x:setAttrDict(x,'DURUM_ACIKLAMA',db.getDurumAdiByDurumID( x['ID_DURUM'])), hata_kayitlari)
                #with_hata_aciklama   = map(lambda x:setAttrDict(x,'HATA_ACIKLAMA',db.getHataAciklamaHataID( x['ID_HATA'])), with_durum_aciklama)
                #with_hata_aciklama   = map(lambda x:x if not x['TAMAMLANMA_TARIHI'] == '' else  setAttrDict(x,'TAMAMLANMA_TARIHI',None)  , hata_kayitlari)             
                with_hata_aciklama = hata_kayitlari
                donen_servis_kayit = ws.servisKayitlariGonder( with_hata_aciklama, bayii.BAYI_LISANS_NO )
            if donen_servis_kayit != '':
                self.servisKayitlariDonenWsIncele(donen_servis_kayit,db)
        except:
            pass
        
        try:
            donen_satis_kayit  = ''
            satis_kayitlari      = db.get_raw_data_all(table_name="VIEW_POMPA_SATIS_RAPOR",where="DURUM=0",dict_mi="dict")              
            if satis_kayitlari !=[]:
                donen_satis_kayit = ws.satisKayitlariGonder( satis_kayitlari, bayii.BAYI_LISANS_NO )                  
            if donen_satis_kayit != '':                   
                self.satisKayitlariDonenWsIncele(donen_satis_kayit,db)
        except:
            pass
        '''
        try:
            donen_cari_hesap_kayit  = ''
            cari_hesap_kayitlari      = db.get_raw_data_all(table_name="CARI_HESAPLAR",where="WS_DURUM is null",dict_mi="dict")              
            if cari_hesap_kayitlari !=[]:
                donen_cari_hesap_kayit = ws.cariHesaplariGonder( cari_hesap_kayitlari, bayii.BAYI_LISANS_NO )                  
            if donen_cari_hesap_kayit != '':                   
                self.cariHesapKayitlariDonenWsIncele(donen_cari_hesap_kayit,db)
        except:
            pass
        
        try:
            donen_kart_kayitlari = ''
            kart_kayitlari       = db.get_raw_data_all(table_name="KARTLAR",where="WS_DURUM is null and ID_CARI_WEB is not null",dict_mi="dict")
            if kart_kayitlari !=[]:
                donen_kart_kayitlari = ws.kartKayitlariGonder( kart_kayitlari, bayii.BAYI_LISANS_NO )                  
            if donen_kart_kayitlari != '':                   
                self.kartKayitlariDonenWsIncele(donen_kart_kayitlari,db)
        except:
            pass

        
        try:
            epdk_tablos = dict (
            EPDK_TABLO1 = db.get_raw_data_all(table_name="EPDK_TABLO1",selects="ID_TABLO1,TARIH,LISANS_NO,ID_YAKIT,TANK_DONEM_BASI,TANK_DOLUM,POMPA_SATIS,TANK_DONEM_SONU,KOY_POMPA_DOLUM,ACIKLAMA,OLUSTURMA_TARIHI",where="WEBE_ILETIM_TARIHI is Null",dict_mi ='dict'),
            EPDK_TABLO2 = db.get_raw_data_all(table_name="EPDK_TABLO2",selects="ID_TABLO2,TARIH,LISANS_NO,ID_YAKIT,TANKER_PLAKA,POMPA_SATIS,IST_YAPILAN_DOLUM,ACIKLAMA,OLUSTURMA_TARIHI",where="WEBE_ILETIM_TARIHI is Null",dict_mi ='dict'),
            EPDK_TABLO3 = db.get_raw_data_all(table_name="EPDK_TABLO3",selects="ID_TABLO3,TARIH,LISANS_NO,KOY_POMPASI_NO,ID_YAKIT,TANK_DONEM_BASI,TANK_DOLUM,POMPA_SATIS,TANK_DONEM_SONU,IST_YAPILAN_DOLUM,ACIKLAMA,OLUSTURMA_TARIHI",where="WEBE_ILETIM_TARIHI is Null",dict_mi ='dict'),
            EPDK_TABLO4 = db.get_raw_data_all(table_name="EPDK_TABLO4",selects="ID_TABLO4,DONEM,LISANS_NO,ID_YAKIT,TANK_DONEM_BASI,TANK_DOLUM,POMPA_SATIS,TANK_DONEM_SONU,KOY_YAPILAN_DOLUM,ACIKLAMA,OLUSTURMA_TARIHI",where="WEBE_ILETIM_TARIHI is Null",dict_mi ='dict'),
            EPDK_TABLO5 = db.get_raw_data_all(table_name="EPDK_TABLO5",selects="ID_TABLO5,BAYI_BELGE_TARIHI,BAYI_BELGE_NO,LISANS_NO,ID_YAKIT,SATIS_IL,DIS_SATIS_MIKTARI,DAG_SEVK_IRS_TAR,DAG_SEVK_IRS_NO,ACIKLAMA,OLUSTURMA_TARIHI",where="WEBE_ILETIM_TARIHI is Null",dict_mi ='dict'),
            EPDK_TABLO6 = db.get_raw_data_all(table_name="EPDK_TABLO6",selects="ID_TABLO6,BELGE_TANZIM_TARIHI,LISANS_NO,ID_YAKIT,DAG_SATIS_MIKTARI,DAG_SEVK_IRS_NO,ISTASYON_DOLUM,KOY_POMPASI_DOLUM,TAR_AMAC_TANKER_SATIS,DIS_SATIS_MIKTARI,IADE_MIKTAR,ACIKLAMA,OLUSTURMA_TARIHI",where="WEBE_ILETIM_TARIHI is Null",dict_mi ='dict'),
            EPDK_TABLO7 = db.get_raw_data_all(table_name="EPDK_TABLO7",selects="ID_TABLO7,MUTABAKAT_DONEMI,LISANS_NO,ID_YAKIT,DAG_SATIS_MIKTARI,OTO_SIS_GECEN_DOLUM,DIS_SATIS,MUTABAKAT_DURUMU,ACIKLAMA,OLUSTURMA_TARIHI",where="WEBE_ILETIM_TARIHI is Null",dict_mi ='dict'),
            EPDK_TABLO8 = db.get_raw_data_all(table_name="EPDK_TABLO8",selects="ID_ACIKLAMA_KODU,ACIKLAMA_KODU,LISANS_NO,ID_YAKIT,OZEL_DURUM_BASL_TAR_SAAT,OZEL_DURUM_BIT_TAR_SAAT,OZEL_DURUM_TURU,AKARYAKIT_KAYIP,AKARYAKIT_KAZANC,DURUM,ACIKLAMA,OLUSTURMA_TARIHI",where="WEBE_ILETIM_TARIHI is Null",dict_mi ='dict'),
            EPDK_TABLO9 = db.get_raw_data_all(table_name="EPDK_TABLO9",selects="ID_TABLO9,TARIH_SAAT,LISANS_NO,ID_YAKIT,GECERLI_FIYAT,OLUSTURMA_TARIHI",where="WEBE_ILETIM_TARIHI is Null",dict_mi ='dict'),
            EPDK_TABLO11 = db.get_raw_data_all(table_name="EPDK_TABLO11",selects="ID_TABLO11,BAYI_BELGE_TANZIM_TAR,BAYI_BELGE_NO,LISANS_NO,ID_YAKIT,DAG_SATIS_MIK,DAG_SEVK_IRS_TAR,DAG_SEVK_IRS_NO,BELGELENEN_DIS_SATIS_MIKTARI,SATIS_IL,ACIKLAMA,OLUSTURMA_TARIHI",where="WEBE_ILETIM_TARIHI is Null",dict_mi ='dict'),
            EPDK_TABLO12 = db.get_raw_data_all(table_name="EPDK_TABLO12",selects="ID_TABLO12,DONEM,LISANS_NO,ID_YAKIT,TANKER_PLAKA,POMPA_SATIS,IST_YAPILAN_DOLUM,ACIKLAMA,OLUSTURMA_TARIHI",where="WEBE_ILETIM_TARIHI is Null",dict_mi ='dict'),
            EPDK_TABLO13 = db.get_raw_data_all(table_name="EPDK_TABLO13",selects="ID_TABLO13,DONEM,LISANS_NO,KOY_POMPA_NO,ID_YAKIT,TANK_DONEM_BASI,TANK_DOLUM,POMPA_SATIS,TANK_DONEM_SONU,IST_YAPILAN_DOLUM,ACIKLAMA,OLUSTURMA_TARIHI",where="WEBE_ILETIM_TARIHI is Null",dict_mi ='dict'),
            EPDK_TABLO14 = db.get_raw_data_all(table_name="EPDK_TABLO14",selects="ID_TABLO14,BELGE_TANZIM_TAR,LISANS_NO,ID_YAKIT,DAG_SAT_MIKTARI,DAG_SEVK_IRS_NO,ACIKLAMA,OLUSTURMA_TARIHI",where="WEBE_ILETIM_TARIHI is Null",dict_mi ='dict'),
            EPDK_TABLO15 = db.get_raw_data_all(table_name="EPDK_TABLO15",selects="ID_TABLO15,LISANS_NO,BAYI_ENLEM,BAYI_BOYLAM,BAYI_TANK_SAYISI,BAYI_TOPLAM_KAPASITE,BAYI_POMPA_SAYISI,BAYI_TABANCA_SAYISI,BAYI_MARKA,OLUSTURMA_TARIHI",where="WEBE_ILETIM_TARIHI is Null",dict_mi ='dict'),
            EPDK_TABLO16 = db.get_raw_data_all(table_name="EPDK_TABLO16",selects="ID_TABLO16,LISANS_NO,KOY_POMPASI_NO,KOY_POMPASI_ENLEM,KOY_POMPASI_BOYLAM,KOY_POMPASI_TANK_SAYISI,KOY_POMPASI_TOPLAM_KAPASITE,KOY_POMPASI_POMPA_SAYISI,KOY_POMPASI_TABANCA_SAYISI,OLUSTURMA_TARIHI",where="WEBE_ILETIM_TARIHI is Null",dict_mi ='dict'),
            EPDK_TABLO17 = db.get_raw_data_all(table_name="EPDK_TABLO17",selects="ID_TABLO17,LISANS_NO,TANKER_PLAKA_NO,TOPLAM_KAPASITE,OLUSTURMA_TARIHI",where="WEBE_ILETIM_TARIHI is Null",dict_mi ='dict'),
            EPDK_TABLO18 = db.get_raw_data_all(table_name="EPDK_TABLO18",selects="ID_TABLO18,LISANS_NO,DOLUM_TARIHI,ID_YAKIT,DOLUM_MIKTARI,SATIS_BELGESI_NO,SATIS_BELGESI_TARIH,SAT_BEL_URUN_MIK,IADE_BAKIM_TRANSFER,IKMAL_DURUMU,ACIKLAMA,OLUSTURMA_TARIHI",where="WEBE_ILETIM_TARIHI is Null",dict_mi ='dict')
            )
            donen_ws = ''
            donen_dolu_tables = self.wsEpdkTabloAyarla(epdk_tablos)

            donen_ws = ws.EpdkTablolariWebeGonder( donen_dolu_tables, bayii.BAYI_LISANS_NO )
            if donen_ws != '':
                self.epdkTabloDonenWsIncele(donen_ws,db)
        
        except:
            pass
        '''
        try:
            ws_gelen_tasit_tanima_kayitlari = ''
            ws_gelen_tasit_tanima_kayitlari = ws.TasitTanimaKayitlariAl(bayii.BAYI_LISANS_NO )
            if ws_gelen_tasit_tanima_kayitlari !='':
                self.wsGelentasitTanimaIncele(ws_gelen_tasit_tanima_kayitlari,db,ws,bayii.BAYI_LISANS_NO)
        except:
            pass
        
        try:
            donen_irsaliye_kayit     = ''
            donen_irsaliye_kayit     = ws.irsaliyeKayitlariAl(bayii.BAYI_LISANS_NO )
            if donen_irsaliye_kayit != '':                   
                self.irsaliyeKayitlariDonenWsIncele(donen_irsaliye_kayit,db,ws,bayii.BAYI_LISANS_NO)
        except:
           pass
        '''''
        try:
            wsden_gelen_iskonto_kayitlari     = ''
            wsden_gelen_iskonto_kayitlari     = ws.iskontoKayitlariAl(bayii.BAYI_LISANS_NO )
            if wsden_gelen_iskonto_kayitlari != '':
                self.wsdenGelenIskontoIncele(wsden_gelen_iskonto_kayitlari,db)
        except:
            pass
        
        try:
            wsden_gelen_iskonto_yakit_kayitlari     = ''
            wsden_gelen_iskonto_yakit_kayitlari     = ws.iskontoYakitKayitlariAl(bayii.BAYI_LISANS_NO )
            if wsden_gelen_iskonto_yakit_kayitlari != '':
                self.wsdenGelenIskontoYakitIncele(wsden_gelen_iskonto_yakit_kayitlari,db)
        except:
            pass
        
        try:
            wsden_gelen_cari_hesap_kayitlari     = ''
            wsden_gelen_cari_hesap_kayitlari     = ws.cariHesaplariAl(bayii.BAYI_LISANS_NO )
            if wsden_gelen_cari_hesap_kayitlari != '':
                self.wsdenGelenCariDonenIncele(wsden_gelen_cari_hesap_kayitlari,db)
        except:
            pass
        
        try:
            wsden_gelen_kart_kayitlari     = ''
            wsden_gelen_kart_kayitlari     = ws.cariKartlariAl(bayii.BAYI_LISANS_NO )
            if wsden_gelen_kart_kayitlari != '':
                self.wsdenGelenKartDonenIncele(wsden_gelen_kart_kayitlari,db)
        except:
            pass
            
        '''''
             
    def wsGelentasitTanimaIncele(self,ws_gelen_tasit_tanima_kayitlari,db,ws,bayi_lisans_no):
        if ws_gelen_tasit_tanima_kayitlari:
            tasit_tanima_ok_kayitlari = {}
            donen_json_format = json.loads(ws_gelen_tasit_tanima_kayitlari)
            groupped_json = {}
            for x in donen_json_format:
                node_list = []
                if  groupped_json.has_key(x['fields']['TABLO_ADI']):
                    node_list = groupped_json[ x['fields']['TABLO_ADI'] ]
                x['fields']['pk'] = x['pk']
                node_list.append( x['fields'] )
                from operator import itemgetter
                node_list = sorted(node_list, key=itemgetter('ISLEM_TIPI'))
                groupped_json[ x['fields']['TABLO_ADI'] ] = node_list
            
            for key in sabitler.TASIT_TANIMA_PK_MAP.pk_sira_list:
                if groupped_json.has_key( key ):
                    element_dict_list = groupped_json[ key ]
                    for value in element_dict_list:
                        id_tasit_tanima_web = -1
                        tasit_tanima_var_mi = []
                        column_name_ = ''
                        values_      = ''
                        id_tasit_tanima_web = value['pk']
                        for k,v in value.iteritems():
                            if k=='TABLO_ADI':
                                table_name_plain = v
                                table_name_ = '"'+v+'"'
                                where_ = '%s = %s '%(sabitler.TASIT_TANIMA_PK_MAP.pk_map_dict[str(table_name_plain)],value[sabitler.TASIT_TANIMA_PK_MAP.pk_map_dict[str(table_name_plain)]])
                            elif k=='ISLEM_TIPI':
                                islem_tipi = v
                            elif k!='TABLO_ADI' and k!='ISLEM_TIPI' and k!='ISLEM_TARIHI' and k!='ID_BAYI_WEB' and k!= 'pk' and v:
                                column_name_ += '"'+ k +'"'+','
                                values_      += '"'+str(v)+'"'+','
                                
                        column_name_ = column_name_[:-1]
                        values_      = values_[:-1]
                        durum = 0
                        tasit_tanima_var_mi = db.get_raw_data_all (table_name = table_name_,dict_mi = 'dict', where=where_)
                        if islem_tipi == 1 and not tasit_tanima_var_mi:
                            #try:
                                durum = db.save_data(table_name=table_name_,column_name=column_name_,values = values_)
                            #except:
                            #    pass
                        elif tasit_tanima_var_mi and islem_tipi in (2,3):
                            #try:
                                durum = db.update_data(table_name=table_name_,column_name=column_name_,values = values_,where=where_)
                            #except:
                            #    pass
                        if durum == 1:
                            tasit_tanima_ok_kayitlari[str(id_tasit_tanima_web)] = 1
                        elif tasit_tanima_var_mi and durum == 0 and islem_tipi == 1 :
                            tasit_tanima_ok_kayitlari[str(id_tasit_tanima_web)] = 1
                                
            if tasit_tanima_ok_kayitlari :
                try :
                    tasit_tanima_ok_kayit_ = ws.tasitTanimaOkGonder( tasit_tanima_ok_kayitlari, bayi_lisans_no )
                except:
                    pass            
    
    def wsdenGelenIskontoIncele(self,wsden_gelen_iskonto_kayitlari,db):          
        if wsden_gelen_iskonto_kayitlari:
            donen_json_format = json.loads(wsden_gelen_iskonto_kayitlari)
            for donen in donen_json_format :
                id_iskonto_web = -1
                iskonto_var_mi = []
                for key,value in donen.iteritems():
                   
                    if key == 'pk' :
                        id_iskonto_web = value
                        iskonto_var_mi = db.get_raw_data_all (table_name = "ISKONTO",dict_mi = 'dict', where="ID_ISKONTO_WEB=%s"%id_iskonto_web)
                        
                    if  key == 'fields':
                        column_name_ = ''
                        values_      = ''
                        for k,v in value.iteritems():
                            if v:
                                column_name_ += '"'+ k +'"'+','
                                values_      += '"'+str(v)+'"'+','

                        column_name_ += '"'+'ID_ISKONTO_WEB'+'"'
                        values_      += '"'+str(id_iskonto_web)+'"'
                        if  iskonto_var_mi == [] :
                            try:
                                save = db.save_data(table_name='ISKONTO',column_name=column_name_,values = values_)
                            except:
                                pass
              
    def wsdenGelenIskontoYakitIncele(self,wsden_gelen_iskonto_yakit_kayitlari,db):  
        if wsden_gelen_iskonto_yakit_kayitlari:
            donen_json_format = json.loads(wsden_gelen_iskonto_yakit_kayitlari)
            for donen in donen_json_format :
                id_iskonto_yakit_web = -1
                iskonto_yakit_var_mi = []
                for key,value in donen.iteritems():
                   
                    if key == 'pk' :
                        id_iskonto_yakit_web = value
                        iskonto_yakit_var_mi = db.get_raw_data_all (table_name = "ISKONTO_YAKIT",dict_mi = 'dict', where="ID_ISKONTO_YAKIT_WEB=%s"%id_iskonto_yakit_web)
                        
                    if  key == 'fields':
                        column_name_ = ''
                        values_      = ''
                        for k,v in value.iteritems():
                            if v:
                                column_name_ += '"'+ k +'"'+','
                                values_      += '"'+str(v)+'"'+','

                        column_name_ += '"'+'ID_ISKONTO_YAKIT_WEB'+'"'
                        values_      += '"'+str(id_iskonto_yakit_web)+'"'
                        if  iskonto_yakit_var_mi == [] :
                            try:
                                save = db.save_data(table_name='ISKONTO_YAKIT',column_name=column_name_,values = values_)
                            except:
                                pass
    def wsdenGelenCariDonenIncele(self,wsden_gelen_cari_hesap_kayitlari,db):
        if wsden_gelen_cari_hesap_kayitlari:
            donen_json_format = json.loads(wsden_gelen_cari_hesap_kayitlari)
            for donen in donen_json_format :
                id_cari_hesap_web = -1
                cari_hesap_var_mi = []
                for key,value in donen.iteritems():
                   
                    if key == 'pk' :
                        id_cari_hesap_web = value
                        cari_hesap_var_mi = db.get_raw_data_all (table_name = "CARI_HESAPLAR",dict_mi = 'dict', where="ID_CARI_HESAP_WEB=%s"%id_cari_hesap_web)
                        
                    if  key == 'fields':
                        column_name_ = ''
                        values_      = ''
                        for k,v in value.iteritems():
                            if  v:
                                column_name_ += '"'+ k +'"'+','
                                values_      += '"'+str(v)+'"'+','

                        column_name_ += '"'+'ID_CARI_HESAP_WEB'+ '"'
                        values_      += '"'+str(id_cari_hesap_web)+'"'
                        if  cari_hesap_var_mi == [] :
                            try:
                                save = db.save_data(table_name='CARI_HESAPLAR',column_name=column_name_,values = values_)
                            except:
                                pass
    def wsdenGelenKartDonenIncele(self,wsden_gelen_kart_kayitlari,db):
        if wsden_gelen_kart_kayitlari:
            donen_json_format = json.loads(wsden_gelen_kart_kayitlari)
            for donen in donen_json_format :
                id_kart_web = -1
                kart_var_mi = []
                for key,value in donen.iteritems():
                    if key == 'pk' :
                        id_kart_web = value
                        kart_var_mi = db.get_raw_data_all (table_name = "KARTLAR",dict_mi = 'dict', where="ID_KART_WEB=%s"%id_kart_web)
                        
                    if  key == 'fields':
                        column_name_ = ''
                        values_      = ''
                        for k,v in value.iteritems():
                            if  v:
                                column_name_ += '"'+ k +'"'+','
                                values_      += '"'+str(v)+'"'+','

                        column_name_ += '"'+'ID_KART_WEB'+'"' 
                        values_      += '"'+str(id_kart_web)+'"'
                        if  kart_var_mi == [] :
                            try:
                                save = db.save_data(table_name='KARTLAR',column_name=column_name_,values = values_)
                            except:
                                pass
    '''###cari hesap ve kart kayitlarinin bayiden gonderilmesi su an icin iptal edildi###                        
    def cariHesapKayitlariDonenWsIncele(self,donen_cari_hesap_kayit,db):
        if donen_cari_hesap_kayit:
            donen_cari_json  = json.loads(donen_cari_hesap_kayit)
            for id_cari,id_cari_web in donen_cari_json.iteritems():
                update_cari = db.update_data(table_name = 'CARI_HESAPLAR',column_name='WS_DURUM,ID_CARI_HESAP_WEB', values = "%s,%s"%(1,id_cari_web), where='ID_CARI_HESAP=%s'%(id_cari) )
                update_kart = db.update_data(table_name = 'KARTLAR',column_name='ID_CARI_HESAP_WEB')
                
    def kartKayitlariDonenWsIncele(self,donen_kart_kayit,db):
        if donen_kart_kayit:
            donen_kart_kayit_json = json.loads(donen_kart_kayit)
            for id_kart,id_kart_web in donen_kart_kayit_json.iteritems():
                update = db.update_data(table_name = 'KARTLAR',column_name='WS_DURUM,ID_KART_WEB', values = "%s,%s"%(1,id_kart_web), where='ID_KART=%s'%(id_kart) )
    '''
    def satisKayitlariDonenWsIncele(self,donen_satis_kayit,db):
        if donen_satis_kayit:
            donen_satis_kayit_array = donen_satis_kayit.split(',')
            for id_satis in donen_satis_kayit_array:
                try:
                    update = db.update_data(table_name = 'POMPA_SATIS_KAYIT',column_name='DURUM', values = str(1), where='ID_SATIS=%s'%(id_satis) )
                except:
                    pass
    def servisKayitlariDonenWsIncele(self,donen_servis_ws_kayit,db):
        
        if donen_servis_ws_kayit:
            donen_json_format = json.loads(donen_servis_ws_kayit)
            for key,value in donen_json_format.iteritems():
                ##key - id_servis value - merkeze_iletim_tarihi,id_web_servis
                mer_ilet_tarihi         = ''
                id_web_servis           = ''
                web_servis_durum_id     = ''
                tamamlanma_tarihi       = ''
                isleme_alinma_tarihi    = ''
                servise_bildirim_tarihi = ''
                for k,v in value.iteritems():
                    if k == 'MERKEZE_ILETIM_TARIHI':                        
                        mer_ilet_tarihi = stringdatetime_formatla(v)
                    if k == 'ID_WEB_SERVIS':                        
                        id_web_servis = v
                    if k == 'ID_DURUM':
                        web_servis_durum_id = v
                    if k == 'TAMAMLANMA_TARIHI':
                        tamamlanma_tarihi = stringdatetime_formatla(v)
                    if k == 'ISLEME_ALINMA_TARIHI':
                        isleme_alinma_tarihi = stringdatetime_formatla(v)
                    if k == 'SERVISE_BILDIRIM_TARIHI':
                        servise_bildirim_tarihi = stringdatetime_formatla(v)
                if  web_servis_durum_id == '' :
                    web_servis_durum_id = 2
                try:
                    update = db.update_data(table_name = 'SERVIS',column_name='MERKEZE_ILETIM_TARIHI,ID_DURUM,ID_WEB_SERVIS,ISLEME_ALINMA_TARIHI,SERVISE_BILDIRIM_TARIHI,TAMAMLANMA_TARIHI', values = '"'+str(mer_ilet_tarihi)+'"'+','+'"'+str(web_servis_durum_id)+'"'+','+'"'+str(id_web_servis)+'"'+','+'"'+str(isleme_alinma_tarihi)+'"'+','+'"'+str(servise_bildirim_tarihi)+'"'+','+'"'+str(tamamlanma_tarihi)+'"', where='ID_SERVIS=%s'%key )
                except:
                    pass
    def irsaliyeKayitlariDonenWsIncele(self,donen_irsaliye_ws_kayit,db,ws,bayi_lisans_no):
        if donen_irsaliye_ws_kayit:
            donen_json_format = json.loads(donen_irsaliye_ws_kayit)
            irsaliye_ok_kayitlari = {}
            for donen in donen_json_format :
                id_irsaliye = -1
                irsaliye_var_mi = False
                save = 0
                for key,value in donen.iteritems():
                    if key == 'pk' :
                        id_irsaliye = value
                        irsaliye_var_mi = db.get_raw_data_all (table_name = "IRSALIYE",dict_mi = 'dict', where="ID_IRSALIYE=%s"%id_irsaliye)
                        if irsaliye_var_mi:
                            irsaliye_gonderildigi_tarih  = irsaliye_var_mi[0]['GONDERILDIGI_TARIH']
                            irsaliye_ok_kayitlari[str(id_irsaliye)] = irsaliye_gonderildigi_tarih

                    if key == 'fields':
                        irsaliye_no        = ''
                        id_belge_tipi      = ''
                        id_kdv_durum       = ''
                        sevk_tarihi        = ''
                        irsaliye_tarihi    = ''
                        irsaliye_seri      = ''
                        toplam_tutar       = ''
                        iskonto            = ''
                        kdv_tutar          = ''
                        genel_toplam       = ''
                        irsaliye_kalem     = ''
                        gonderildigi_tarih = ''

                        for k,v in value.iteritems():

                            if k == 'GENEL_TOPLAM':                        
                                genel_toplam = v
                            if k == 'IRSALIYE_KALEM':                        
                                irsaliye_kalem = v
                            if k == 'SEVK_TARIHI':
                                sevk_tarihi = stringdatetime_formatla(v)
                            if k == 'IRSALIYE_TARIHI':
                                irsaliye_tarihi = stringdatetime_formatla(v)
                            if k == 'IRSALIYE_NO':
                                irsaliye_no = v
                            if k == 'KDV_TUTAR':
                                kdv_tutar = v
                            if k == 'ISKONTO':
                                iskonto = v
                            if k == 'IRSALIYE_SERI':
                                irsaliye_seri = v
                            if k == 'ID_BELGE_TIPI':
                                id_belge_tipi = v
                            if k == 'ID_KDV_DURUM':
                                id_kdv_durum = v
                            if k == 'TOPLAM_TUTAR':
                                toplam_tutar = v
                        gonderildigi_tarih = datetime.datetime.now()
                        if not irsaliye_var_mi :
                            try:
                                save = db.save_data(table_name = 'IRSALIYE',column_name='ID_IRSALIYE,GENEL_TOPLAM,IRSALIYE_KALEM,SEVK_TARIHI,IRSALIYE_TARIHI,IRSALIYE_NO,GONDERILDIGI_TARIH,KDV_TUTAR,ISKONTO,IRSALIYE_SERI,ID_BELGE_TIPI,ID_KDV_DURUM,TOPLAM_TUTAR',
                                                      values = '"'+str(id_irsaliye)+'"'+','+'"'+str(genel_toplam)+'"'+','+'"'+str(irsaliye_kalem)+'"'+','+'"'+str(sevk_tarihi)+'"'+','+'"'+str(irsaliye_tarihi)+'"'+','+'"'+str(irsaliye_no)+'"'+','+'"'+str(gonderildigi_tarih)+'"'+','+'"'+str(kdv_tutar)+'"'+','+'"'+str(iskonto)
                                                      +'"'+','+'"'+str(irsaliye_seri)+'"'+','+'"'+str(id_belge_tipi)+'"'+','+'"'+str(id_kdv_durum)+'"'+','+'"'+str(toplam_tutar)+'"')
                            except:
                                pass
                        if save == 1:
                            irsaliye_ok_kayitlari[str(id_irsaliye)] = gonderildigi_tarih
            if irsaliye_ok_kayitlari :
                try :
                    irsaliye_ok_kayit = ws.irsaliyeOkGonder( irsaliye_ok_kayitlari, bayi_lisans_no )
                except:
                    pass
    def epdkTabloDonenWsIncele(self, donen_ws,db ):
        
        donen_json_format = json.loads(donen_ws)
        for key,value in donen_json_format.iteritems():
            if key == 'EPDK_Tablo8' :
                value_col = 'ID_ACIKLAMA_KODU'
            else:
                value_col = key.replace('EPDK','ID')
            for k,v in value.iteritems():
                update = db.update_data(table_name = str(key),column_name='WEBE_ILETIM_TARIHI', values = '"'+str(v)+'"', where='%s=%s'%(value_col,k) )

                    
    def wsEpdkTabloAyarla(self,dict_epdk_tablos_kayitlar):
        return_epdk_tables ={}
        for table_name,value in dict_epdk_tablos_kayitlar.iteritems():
            if value != []:
                return_epdk_tables[table_name]=value
        return return_epdk_tables
       
    def getPortProperties( self, db_conn, port_operation_object, port_ids, try_again = True ):
        where_str         = ','.join(port_ids)
        where_str         = 'ID_PORT IN (' + where_str + ')'
        self.port_list  = db_conn.get_raw_data_all(table_name = 'PORTLAR',
                                                   selects="ID_PORT,PORT_ADI,PARITY,STOP_BIT,BAUND_RATE,DATA_BIT,BEK_SURESI"
                                                   ,dict_mi='object'
                                                   ,where=where_str)
        port_operation_object.open_port( self.port_list, self.com_q, try_again )  
    def OnAbout(self, event):
        createAbout(self,'Otomasyon Hakk�nda' )
        self.SetTransparent(255)
    def OnCloseWindow(self,e):
        e.Veto()
    def onay_message( self,message,win_name):
        self.SetTransparent(0)
        dlg = wx.MessageDialog(self, message,
                               win_name,
                               wx.OK  |wx.CANCEL | wx.ICON_INFORMATION
                               )
        
        retCode = dlg.ShowModal()
        if (retCode == wx.ID_OK):
            self.Destroy()
        dlg.Destroy()
        self.SetTransparent(255)  
    def OnMainMenuPressed( self , event ):
        if wx.FindWindowById( id_e_about ):
            param.total_key_code = ''
            wx.FindWindowById( id_e_about ).Destroy()
        fullscreen = True
        id_  = event.GetEventObject().GetId()
        if id_ == id_otomasyon:
            winClass = win_Otomasyon
            title    = MESSAGES.otomasyon
        elif id_ == id_islemler:
            winClass = win_Islemler
            title    = MESSAGES.islemler
        elif id_ == id_vardiya:
            winClass = win_Vardiya
            title    = MESSAGES.vardiyalar
        elif id_ == id_raporlar:
            winClass = win_Raporlar
            title    = MESSAGES.raporlar
        elif id_   == id_ayarlar:
            winClass = win_Ayarlar
            if not self.login:
                self.lg = LoginDialog()
                self.lg.ShowModal()
                if self.lg.logged_in:
                    self.login = self.lg.logged_in
                    self.user  = self.lg.user.GetValue()
                else:
                    winClass =None
                    self.login = False
            title = 'Ayarlar'
        elif id_ == id_about:
            winClass = createAbout
            title    = ''
            fullscreen = False
        if winClass:
            O.newWindow(self,event,winClass,title,fullscreen)   
    def createAyarlar( self ):
        
        self.Freeze()
        self.ayarlar_pressed  =  wx.Image("%sprocess_accept.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_ayarlar = wx.Image("%sprocess.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_ayarlar  = wx.BitmapButton(self.panel1, id=id_ayarlar, bitmap=image_ayarlar,pos=(0,4*button_size),
        size = (image_ayarlar.GetWidth()+5, image_ayarlar.GetHeight()+10),style=wx.BORDER_NONE)
        
        self.btn_ayarlar.Bind(wx.EVT_BUTTON, self.OnMainMenuPressed)
        self.btn_ayarlar.SetToolTipString("Ayarlar")
        self.btn_ayarlar.SetBitmapSelected(self.ayarlar_pressed)
        s.setPanelListBg( self.btn_ayarlar )
        self.sizer.Add(self.btn_ayarlar, 1, wx.ALL)
        self.SetSize( (self.GetSize().width, self.GetSize().height +  image_ayarlar.GetHeight()+10))
        self.Layout()
        self.Thaw()

class creatShow( wx.Frame ):
    def __init__(self, parent):
        wx.Frame.__init__(self, parent,size=(37,37),
                          style= (  wx.FRAME_NO_TASKBAR|wx.FRAME_FLOAT_ON_PARENT))
        self.parent = parent
        self.LayoutItems()
        self.Show(True)
    def LayoutItems(self):
       
        mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.Freeze()
        self.SetSizer(mainSizer)
        
        self.SetPosition((self.GetParent().GetPosition()[0]+37,self.GetParent().GetPosition()[1] + (self.GetParent().GetSize().height)))
        winSize =  self.GetSize()
        self.top_panel = wx.Panel(self, wx.ID_ANY,size=winSize,style=wx.EXPAND)
        s.setPanelListBg ( self.top_panel )
        
        self.collapse_pressed  =  wx.Image("%sonceki_pressed.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_collapse = wx.Image("%sonceki.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_collapse  = wx.BitmapButton(self.top_panel, id=wx.NewId(), bitmap=image_collapse,pos=(0,0),
        size = (image_collapse.GetWidth()+5, image_collapse.GetHeight()+5),style=wx.BORDER_NONE)
        self.btn_collapse.Bind(wx.EVT_BUTTON, self.OnMainGoster)
        self.btn_collapse.SetToolTipString("G�ster")
        self.btn_collapse.SetBitmapSelected(self.collapse_pressed)
        s.setPanelListBg( self.btn_collapse )
        
        
        mainSizer.Add( self.btn_collapse, 0, wx.EXPAND)
        self.top_panel.SetSizerAndFit( mainSizer )
        self.Thaw()
    def OnMainGoster( self, event ):
        #self.parent.SetPosition(( self.parent.GetPosition()[0]- 71, self.parent.GetPosition()[1]))
        #self.parent.Refresh()
        self.parent.Show( True )
        self.Show( False )
class createAbout( wx.Frame ):
    def __init__(self, parent,id_, title):
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.FRAME_NO_TASKBAR|wx.FRAME_FLOAT_ON_PARENT))
        
        self.dagitici_   = param.get_dagitici_adi()
        self.bayi_       = param.get_bayi_adi()
        self.LayoutItems()
        self.Show(True)
    
    def LayoutItems(self):
       
        mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.Freeze()
        self.SetSizer(mainSizer)
        self.SetSize((400,self.GetParent().GetSize().height))
        self.SetPosition((self.GetParent().GetPosition()[0]-(self.GetSize().width),self.GetParent().GetPosition()[1]))
        winSize =  self.GetSize()
        self.top_panel = wx.Panel(self, wx.ID_ANY,size=winSize,style=wx.EXPAND)
        
        about_txt     = wx.StaticText(self.top_panel,pos=(30,70), label=sabitler.copyright_text )#%(self.dagitici_,self.bayi_))
        line_         = wx.StaticLine(self.top_panel, 1,pos=(0,self.GetParent().GetSize().height-100), style=wx.LI_HORIZONTAL)
        s.setStaticTextHeader( about_txt )
        s.setPanelListBg ( self.top_panel )
        self.btn_bin_pressed  =  wx.Image("%sdelete_green.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_bin = wx.Image("%sdelete.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_bin = LibButtons.ThemedGenBitmapTextButton(self.top_panel, pos=(250,self.GetParent().GetSize().height-100),size=(150,70), id=wx.NewId(), bitmap=image_bin,label=MESSAGES.kapat)
        self.btn_bin.Bind(wx.EVT_BUTTON, self.OnKapat)
        self.btn_bin.SetToolTipString(MESSAGES.kapat)
        self.btn_bin.SetBitmapSelected(self.btn_bin_pressed)
        
        s.setButtonFont ( self.btn_bin )

        mainSizer.Add( self.top_panel,0,wx.EXPAND)
        if not FindWindowById(id_ayarlar ):
            param.bindSettings( self.btn_bin )
        self.Thaw()
    def OnKapat(self, event ):
        param.total_key_code = ''
        self.Destroy()
app = wx.App(False)
frame = MenuFrame(None)
app.MainLoop()

