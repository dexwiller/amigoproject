#!/usr/bin/env python
# -*- coding: latin5 -*-
from datetime import datetime
from datetime import time
import sabitler
import time as ttime
import win32api
import win32con
import win32netcon
import win32security
import win32wnet
now = datetime.now()
import hashlib
from _winreg import *
import os
import crcmod
import netifaces as nif
import wx
class Dummy:
    pass
def setRegisty( value ):
    key = CreateKey(HKEY_LOCAL_MACHINE, "Software\\DGOto\\Oto")
    SetValueEx(key, 'License', 0, REG_SZ, str( value ))
    key.Close()
def getRegisty():
    key  = OpenKey(HKEY_LOCAL_MACHINE, "Software\\DGOto\\Oto",0 )
    return QueryValueEx(key, 'License')[0]
    
def mac_for_ip(ip):
    for i in nif.interfaces():
	addrs = nif.ifaddresses(i)
	try:
	    if_mac = addrs[nif.AF_LINK][0]['addr']
	    if_ip = addrs[nif.AF_INET][0]['addr']
	except IndexError, KeyError:
	    if_mac = if_ip = None
	if if_ip == ip:
	    return if_mac
    return None

def get_all_mac():
    return_mac= []
    try:
	ints        = nif.interfaces()
	for i in ints:
	    addrs = nif.ifaddresses(i)
	    try:
	        if_mac = addrs[nif.AF_LINK][0]['addr']
	        #if_ip  = addrs[nif.AF_INET][0]['addr']
	        return_mac.append (if_mac )  
	    except IndexError, KeyError:
	        pass
    except:
	pass
    return_mac.sort()
    
    #print return_mac
    return return_mac
    
def isDigit(gelen):
    try:
        res = int( gelen )
        return True
    except:
        return False
def setAttrObj(obj,label,value):#setattr none donderir, ondan lambda icinde kullanamiyoruz ondan �eyettik bunu
    setattr(obj,label,value)
    return obj
def setAttrDict(obj,label,value):#dict set none donderir, ondan lambda icinde kullanamiyoruz ondan �eyettik bunu
    obj[label] = value
    return obj
def get_ytl_format2dgt( gdeger ):
    #print 'gelen deger', gdeger
    if gdeger:
        if len(str(gdeger)) > 0:
            try:
                    gdegerx = str(float(gdeger))
            except:
                    gdegerx = str(float(str(gdeger).replace(',','.')))
            if str(gdegerx).find('.')>0:
                    try:
                            mf = str(float(gdegerx))
                            ndeger = str(mf).split('.')
                    except:
                            mf = str(float(gdegerx.replace('.',',')))
                            ndeger = str(mf).split(',')
            elif str(gdeger).find(',')>0:
                    try:
                            mf = str(float(gdegerx))
                            ndeger = str(mf).split(',')
                    except:
                            mf = str(float(gdegerx.replace(',','.')))
                            ndeger = str(mf).split('.')
            else:
                    ndeger = float(gdegerx)
                    
            
            kurus  = ndeger[1]
            ytl    = ndeger[0]
            
            a = ytl
            c = []
            for as_ in a:
                    c.append(as_)
            c.reverse()
            s = 1
            k=''
            f = []
            
            for cs in c:
                    k += cs
                    if s == 3:
                            f.append( k)
                            s = 1
                            k = ''
                    else:
                            s += 1
            f.append(k)
            
            final = []
            
            for fs in f:
                    if fs <> '':
                            xr = []
                            for fss in fs:
                                    xr.append(fss)
                            xr.reverse()
                            ye = ''
                            for xrs in xr:
                                    ye += xrs
                            final.append(ye)
            final.reverse()
            lasto = ''
            for fin in final:
                    lasto += fin + '.'
            lasto = lasto[:len(lasto)-1]
            
            if len(kurus) < 2:
                    fark = 2 - len(kurus)
                    kurus = kurus + str( '0' * fark )
            elif len(kurus) > 2:
                    kurus = kurus[:2]
                    
            ytl_kur = lasto + ',' + kurus
        else:
                ytl_kur = '0'
    else:
            ytl_kur = '0'
    
    return ytl_kur
def get_miktar_digits( gdeger ):
	
	if gdeger:
		if len(str(gdeger)) > 0:
			try:
				gdegerx = str(float(gdeger))
			except:
				gdegerx = str(float(str(gdeger).replace(',','.')))
			if str(gdegerx).find('.')>0:
				try:
					mf = str(float(gdegerx))
					ndeger = str(mf).split('.')
				except:
					mf = str(float(gdegerx.replace('.',',')))
					ndeger = str(mf).split(',')
			elif str(gdeger).find(',')>0:
				try:
					mf = str(float(gdegerx))
					ndeger = str(mf).split(',')
				except:
					mf = str(float(gdegerx.replace(',','.')))
					ndeger = str(mf).split('.')
			else:
				ndeger = float(gdegerx)
				
			
			kurus  = ndeger[1]
			ytl    = ndeger[0]
			
			a = ytl
			c = []
			for as_ in a:
				c.append(as_)
			c.reverse()
			s = 1
			k=''
			f = []
			
			for cs in c:
				k += cs
				if s == 3:
					f.append( k)
					s = 1
					k = ''
				else:
					s += 1
			f.append(k)
			
			final = []
			
			for fs in f:
				if fs <> '':
					xr = []
					for fss in fs:
						xr.append(fss)
					xr.reverse()
					ye = ''
					for xrs in xr:
						ye += xrs
					final.append(ye)
			final.reverse()
			lasto = ''
			for fin in final:
				lasto += fin + '.'
			lasto = lasto[:len(lasto)-1]
			
			if len(kurus) < 3:
				if int(kurus) > 0:
					fark = 3 - len(kurus)
					kurus = kurus# + str( '0' * fark )
					ytl_kur = lasto + ',' + kurus
				else:
					ytl_kur = lasto
			elif len(kurus) >= 3:
				if float(kurus) > 0:
					kurus = kurus[:3]
					ytl_kur = lasto + ',' + kurus
				else:
					ytl_kur = lasto
		else:
			ytl_kur = '0'
	else:
		ytl_kur = '0'
	
	return ytl_kur
def get_datetime_formatted(gstrd):
    #2014-05-12 00:00:00.0000
    if isinstance(gstrd, datetime):
	k = "%s/%s/%s %s:%s"%( tarih_sifir_ekle(gstrd.day),
			      tarih_sifir_ekle(gstrd.month),
			      tarih_sifir_ekle(gstrd.year),
			      tarih_sifir_ekle(gstrd.hour),
			      tarih_sifir_ekle(gstrd.minute))
	return k
    gtrh = gstrd.split(' ')
    trh  = gtrh[0].split('-')
    zam  = gtrh[1].split(':')
	
    k = "%s/%s/%s %s:%s"%( tarih_sifir_ekle(trh[2]),
			   tarih_sifir_ekle(trh[1]),
			   tarih_sifir_ekle(trh[0]),
			   tarih_sifir_ekle(zam[0]),
			   tarih_sifir_ekle(zam[1]))
	
    return k
def get_date_formatted( gstrd ):
    if isinstance(gstrd, datetime):
	k = "%s/%s/%s"%( tarih_sifir_ekle (gstrd.day),
			 tarih_sifir_ekle (gstrd.month),
			 tarih_sifir_ekle (gstrd.year))
	return k
    gtrh = gstrd.split(' ')
    trh  = gtrh[0].split('-')
    zam  = gtrh[1].split(':')
    
    k = "%s/%s/%s"%( tarih_sifir_ekle(trh[2]),
		     tarih_sifir_ekle(trh[1]),
		     tarih_sifir_ekle(trh[0]))
    
    return k
def get_time_formatted_from_time( gstrd ):
    if isinstance(gstrd, time):
	k = "%s:%s:%s"%( tarih_sifir_ekle(gstrd.hour),
		         tarih_sifir_ekle(gstrd.minute),
			 tarih_sifir_ekle(gstrd.second))
	return k
    zam  = gstrd.split(':')
    
    k = "%s:%s"%( tarih_sifir_ekle(zam[0]),
		 tarih_sifir_ekle(zam[1]))
    
    return k
def tarih_sifir_ekle( gstrd ):
    if len( str(gstrd) ) == 1:
	gstrd = '0%s'%gstrd
    return gstrd
def stringdatetime_formatla(datetimestr):
    if datetimestr  and datetimestr != 'None':
	if datetimestr.find('T') != -1 :
	    datetimestr = datetimestr.replace('T',' ')
	if datetimestr.find('Z') != -1:
	    datetimestr = datetimestr.replace('Z','')
	datetimestr_without_timezone = datetimestr[:datetimestr.find('.')]
	if datetimestr_without_timezone :
	    datetime_obj     = datetime.strptime( datetimestr_without_timezone,'%Y-%m-%d %H:%M:%S' )
	    datetime_tuple   = datetime_obj.timetuple()
	    datetimestr      = ttime.strftime('%Y-%m-%d %H:%M:%S',datetime_tuple)
    return datetimestr
def data_formatla( deger, format_type ):
    if deger and deger != 'None':
	if format_type     == 'currency':
	    formatted_data = get_ytl_format2dgt( deger )
	elif format_type   == 'digit':
	    formatted_data = get_miktar_digits ( deger )
	elif format_type   == 'datetime':
	    formatted_data = get_datetime_formatted( deger )
	elif format_type   == 'date':
	    formatted_data = get_date_formatted( deger )
	elif format_type   == 'time':
	    formatted_data = get_time_formatted_from_time( deger )
	else:
	    formatted_data = str(deger)
	return formatted_data
    else:
	try:
	    if int( deger ) == 0:
		return '--'
	except:
	    return ''
	
def is_odd(num):
    return num & 0x1
def hasmap(  str_ ):    
    m = hashlib.sha512()
    m.update(str_)
    return m.hexdigest()
def regKeySetREG_SZ_LOCAL( key, Value ):
    sub_key  = key[key.rfind('\\')+1:]
    mid_key  = key[:key.rfind('\\')]
    aReg = ConnectRegistry(None,HKEY_LOCAL_MACHINE)
    try:
	aKey = OpenKey(aReg, mid_key, 0, KEY_ALL_ACCESS)
    except:
	aKey = CreateKey(aReg, mid_key)
    SetValueEx(aKey,sub_key,1, REG_SZ, Value )
    CloseKey(aKey)
def regKeySetREG_SZ_USER( key, Value ):
    sub_key  = key[key.rfind('\\')+1:]
    mid_key  = key[:key.rfind('\\')]
    aReg = ConnectRegistry(None,HKEY_CURRENT_USER)
    try:
	aKey = OpenKey(aReg, mid_key, 0, KEY_ALL_ACCESS)
    except:
	aKey = CreateKey(aReg, mid_key)
    SetValueEx(aKey,sub_key,1, REG_SZ, Value )
    CloseKey(aKey)
def regKeySetREG_DWORD_USER( key, Value ):
    sub_key  = key[key.rfind('\\')+1:]
    mid_key  = key[:key.rfind('\\')]
    aReg = ConnectRegistry(None,HKEY_CURRENT_USER)
    try:
	aKey = OpenKey(aReg, mid_key, 0, KEY_ALL_ACCESS)
    except:
	aKey = CreateKey(aReg, mid_key)
    SetValueEx(aKey,sub_key,1, REG_DWORD, Value )
    CloseKey(aKey)
def regKeySetREG_DWORD_LOCAL( key, Value ):
    sub_key  = key[key.rfind('\\')+1:]
    mid_key  = key[:key.rfind('\\')]
    aReg = ConnectRegistry(None,HKEY_LOCAL_MACHINE)
    try:
	aKey = OpenKey(aReg, mid_key, 0, KEY_ALL_ACCESS)
    except:
	aKey = CreateKey(aReg, mid_key)
    SetValueEx(aKey,sub_key,1, REG_DWORD, Value )
    CloseKey(aKey)
def QClear( queue_object ):
    with queue_object.mutex:
	queue_object.queue.clear()	
def getTankYuzdeHesabi(total, yakit_lt, su_lt=0, print_=0 ):
    yakit_yuzde          = 0
    su_yuzde             = 0
    yakit_by_total_yuzde = 0
    
    total_kapasite       = total
    total_yakit_kapasite = total_kapasite - su_lt
    if total_yakit_kapasite != 0:
	yakit_yuzde = int((float(yakit_lt) / float(total_yakit_kapasite)) * 100)
	if yakit_yuzde < 0:
	    yakit_yuzde = 0
    if total_kapasite != 0:
	yakit_by_total_yuzde    = int((float(yakit_lt) / float(total_kapasite)) * 100)
	if yakit_by_total_yuzde < 0:
	    yakit_by_total_yuzde = 0
    if total_kapasite != 0:
	su_yuzde    = int((float(su_lt) / float(total_kapasite)) * 100)
	if su_yuzde < 0:
	    su_yuzde = 0
    
    return yakit_yuzde, su_yuzde, yakit_by_total_yuzde

def getTankMinMaxAlarmHesabi(su_mm,yakit_mm,yakit_tanki_id,min_max_QueueContainer):
    
    min_max_sabit_degerler = min_max_QueueContainer.get()
    min_max_QueueContainer.put(min_max_sabit_degerler)
    
    su_alarm          = Dummy()
    su_alarm.text     = ""
    su_alarm.rengi    = wx.Colour(255,255,255)
    yakit_alarm       = Dummy()
    yakit_alarm.text  = ""
    yakit_alarm.rengi = wx.Colour(255,255,255)
    sabitler_su_alarm_map    = sabitler.TANK_MIN_MAX_ALARM_MAP.su_min_max_alarm_map_dict
    sabitler_yakit_alarm_map = sabitler.TANK_MIN_MAX_ALARM_MAP.yakit_min_max_alarm_map_dict
    
    if min_max_sabit_degerler and min_max_sabit_degerler.has_key(yakit_tanki_id):
	min_max_sabit_degerler_by_tank = min_max_sabit_degerler[yakit_tanki_id]
    
	if su_mm>= min_max_sabit_degerler_by_tank.SU_COK_YUKSEK_SEVIYE:
	    su_alarm.text     = sabitler_su_alarm_map['cok_yuksek']['text']
	    su_alarm.rengi    = sabitler_su_alarm_map['cok_yuksek']['rengi']
	elif min_max_sabit_degerler_by_tank.SU_YUKSEK_SEVIYE and su_mm>= min_max_sabit_degerler_by_tank.SU_YUKSEK_SEVIYE and su_mm < min_max_sabit_degerler_by_tank.SU_COK_YUKSEK_SEVIYE:
	    su_alarm.text     = sabitler_su_alarm_map['yuksek']['text']
	    su_alarm.rengi    = sabitler_su_alarm_map['yuksek']['rengi']
	
	if yakit_mm <= min_max_sabit_degerler_by_tank.YAKIT_COK_DUSUK_SEVIYE:
	    yakit_alarm.text  = sabitler_yakit_alarm_map['cok_dusuk']['text']
	    yakit_alarm.rengi = sabitler_yakit_alarm_map['cok_dusuk']['rengi']
	elif min_max_sabit_degerler_by_tank.YAKIT_DUSUK_SEVIYE and yakit_mm > min_max_sabit_degerler_by_tank.YAKIT_COK_DUSUK_SEVIYE and yakit_mm <= min_max_sabit_degerler_by_tank.YAKIT_DUSUK_SEVIYE: 
	    yakit_alarm.text  = sabitler_yakit_alarm_map['dusuk']['text']
	    yakit_alarm.rengi = sabitler_yakit_alarm_map['dusuk']['rengi']
	elif yakit_mm >= min_max_sabit_degerler_by_tank.YAKIT_COK_YUKSEK_SEVIYE:
	    yakit_alarm.text  = sabitler_yakit_alarm_map['cok_yuksek']['text']
	    yakit_alarm.rengi = sabitler_yakit_alarm_map['cok_yuksek']['rengi']
	elif min_max_sabit_degerler_by_tank.YAKIT_YUKSEK_SEVIYE and yakit_mm >= min_max_sabit_degerler_by_tank.YAKIT_YUKSEK_SEVIYE and yakit_mm < min_max_sabit_degerler_by_tank.YAKIT_COK_YUKSEK_SEVIYE:
	    yakit_alarm.text  = sabitler_yakit_alarm_map['yuksek']['text']
	    yakit_alarm.rengi = sabitler_yakit_alarm_map['yuksek']['rengi']
	    
    return su_alarm,yakit_alarm

def get_uzun_str_bol_2_word( kelime, default = "---------" ):
    kelime_1    = kelime
    kelime_2    = default
    kelime_dizi = kelime.split(' ')
    if len( kelime_dizi )  > 2:
	kelime_2 = ""
	kelime_1 = kelime_dizi.pop( 0 ) + ' ' + kelime_dizi.pop( 0 )
	for f in kelime_dizi:
	    kelime_2 += f
	    kelime_2 += ' '
    
    return kelime_1,kelime_2   
def shutdown(host=None, user=None, passwrd=None, msg=None, timeout=0, force=1, reboot=0):
    connected = 0
    if user and passwrd:
        try:
            win32wnet.WNetAddConnection2(win32netcon.RESOURCETYPE_ANY, None,
                                         ''.join([r'\\', host]), None, user,
                                         passwrd)
        except:
            pass
        else:
            connected = 1
    p1 = win32security.LookupPrivilegeValue(host, win32con.SE_SHUTDOWN_NAME)
    p2 = win32security.LookupPrivilegeValue(host,
                                            win32con.SE_REMOTE_SHUTDOWN_NAME)
    newstate = [(p1, win32con.SE_PRIVILEGE_ENABLED),
                (p2, win32con.SE_PRIVILEGE_ENABLED)]
    htoken = win32security.OpenProcessToken(win32api.GetCurrentProcess(),
                                           win32con.TOKEN_ALL_ACCESS)
    win32security.AdjustTokenPrivileges(htoken, False, newstate)
    win32api.InitiateSystemShutdown(host, msg, timeout, force, reboot)
    if connected:
        win32wnet.WNetCancelConnection2(''.join([r'\\', host]), 0, 0)
    #kullan�m�
    '''

    # an�nda shutdown.
    shutdown('pcad�', 'none', 'none', None, 0)
    # 30 saniye gecikmeli shutdown.
    shutdown('pcad�', 'none', 'none', 'aciklama', 30)
    # Reboot
    shutdown('pcad�', 'none', 'none', None, 0, reboot=1)
    # Shutdown local pc
    shutdown(None, 'none', 'none', None, 0)
    '''
def restartPC():
    shutdown(os.environ['COMPUTERNAME'], None, None, None, 0, reboot=1)
def uppercaseTr(value):
    
    value_ = value.replace('i','�')
    value_last = value_.upper()

    return value_last
def getCRC16( hex_ , reverse=True, mode='crc-16' ):
    crc16 = crcmod.predefined.mkCrcFun( mode )
    
    v  = hex(crc16( hex_.decode('hex') ))[2:]
    
    if is_odd( len(v) ) == 1:
	v = '0' + v
    if reverse:
	v = v[2:4] + v[:2]
    
    return v
def get_bytearray_str( hex_str ):
    ba = []
    for i in range(0,len(hex_str),2):
	str_deger = hex_str[i:i+2]
	int_deger = int(str_deger,16)
	ba.append( int_deger )

    return bytearray( ba )
def com_port_req_and_return_answer_probe( port, string_query ):
    #try:
	port.write(string_query + '\r\n')
	out = ''
	ttime.sleep(sabitler.CihazBeklemeSuresi)
	while port.inWaiting() > 0:
	    out += port.read(1)
	if len( out ) > 0:
	    return out
	'''
	except:
	    print 'except'
	    ttime.sleep(sabitler.except_bekleme_suresi)
	    return 0
	'''
	return 0
def com_port_req_and_return_answer( port, string_query ):
    #try:
	
	port.write(string_query + '\r\n')
	out = ''
	ttime.sleep(sabitler.arabirimBeklemeSuresi)
	while port.inWaiting() > 0:
	    out += port.read(1)
	if len( out ) > 0:
	    return out
	'''
	except:
	    print 'except'
	    ttime.sleep(sabitler.except_bekleme_suresi)
	    return 0
	'''
	return 0
def com_port_byte_req_and_return_answer_probe(port, byte_query ):
    #try:
	port.write(byte_query)
	out = ''
	ttime.sleep(sabitler.CihazBeklemeSuresi)
	while port.inWaiting() > 0:
	    out += port.read(1)
	if len( out ) > 0:
	    out = [elem.encode("hex") for elem in out]
	    return out
	#except:
	#	print 'except'
	#	ttime.sleep(sabitler.except_bekleme_suresi)
	
	#return [0]
	return [0]
def com_port_byte_req_and_return_answer(port, byte_query ):
    #try:
	#print port, string_query
	port.write(byte_query)
	out = ''
	ttime.sleep(sabitler.arabirimBeklemeSuresi)
	while port.inWaiting() > 0:
	    #print 'inWaiting'
	    out += port.read(1)
	if len( out ) > 0:
	    out = [elem.encode("hex") for elem in out]
	    return out
	#except:
	#	print 'except'
	#	ttime.sleep(sabitler.except_bekleme_suresi)
	
	#return [0]
	return [0]
def com_port_byte_req_and_return_answer_card(port, byte_query ):
    #try:
	#print port, string_query
	port.write(byte_query)
	out = ''
	ttime.sleep(sabitler.cardReaderBeklemeSuresi)
	while port.inWaiting() > 0:
	    #print 'inWaiting'
	    out += port.read(1)
	if len( out ) > 0:
	    out = [elem.encode("hex") for elem in out]
	    return out
	#except:
	#	print 'except'
	#	ttime.sleep(sabitler.except_bekleme_suresi)
	
	#return [0]
	return [0]
def com_port_byte_req( port, byte_query ):
    #try:
	
	port.write(byte_query)
	ttime.sleep(sabitler.arabirimCevapsizBeklemeSuresi)
    #except:
    #	print 'except'
    #ttime.sleep(sabitler.except_bekleme_suresi)
        return [0]
def get_byte_request_by_query(query, port_ ):
        byte_query = get_bytearray_str( query  )
        if  byte_query == []:
            pass
            #pompa tanimlama hatasi ( protokol bulunamadi )
        
        donen = com_port_byte_req_and_return_answer( port_, byte_query )
        return donen

def get_byte_request_by_query_card(query, port_ ):
        byte_query = get_bytearray_str( query  )
        if  byte_query == []:
            pass
            #pompa tanimlama hatasi ( protokol bulunamadi )
        
        donen = com_port_byte_req_and_return_answer_card( port_, byte_query )
        return donen

def get_byte_request(query, port_ ):
    byte_query = get_bytearray_str( query  )
    if  byte_query == []:
	pass
	#pompa tanimlama hatasi ( protokol bulunamadi )
    com_port_byte_req( port_, byte_query )

def get_by_mod( value, bolum_char, mod ):
    final_str_value = ''
    for v in value:
	if v != bolum_char:
	    final_str_value += str(int( v ) % 30)
	else:
	    final_str_value += '.'
    
    return float( final_str_value )
def get_mod_by_value( value, mod ):
    result = ""
    for i in str(value):
	v = mod + int(i)
	result += str( v )
    
    return result
def get_decode_hex_by_2( value_list ):
    str_final  = ''
    for i in value_list:
	str_final += i.decode('hex')
    
    return str_final

	