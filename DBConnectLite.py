#!/usr/bin/env python
# -*- coding: latin5 -*-
from __future__ import generators
import sqlite3
import logging
import sys
from util import data_formatla
from sabitler import db_filename, CARI_TIPLER, ODEME_SEKILLERI
from itertools import groupby
from local import MESSAGES

def ResultIter(cursor, arraysize=50):
    while True:
        results = cursor.fetchmany(arraysize)
        if not results:
            break
        for result in results:
            yield result
class Struct:
    def __init__(self, **entries):
        self.__dict__.update(entries)
class Child:
    def __init__(self, **entries): 
        self.__dict__.update(entries)
class dataObj:
    def __init__( self ):
        self.id = None
        self.ad = None
class conect:
    def __init__(self):
        self.conn = sqlite3.connect(db_filename,timeout=2)
        self.conn.execute( 'PRAGMA foreign_keys = ON;')
	#print self.conn.execute('pragma integrity_check;').fetchall()
	#self.conn.execute('commit;')
	try:
	    self.conn.execute( 'PRAGMA auto_vacuum  = 1;' )
	    self.conn.execute( 'VACUUM;')
	except:
	    #print 'vacuum olmad� m�nasi'
	    pass
	
	#self.conn.isolation_level = 'EXCLUSIVE'
	#self.conn.execute( 'PRAGMA locking_mode= EXCLUSIVE')
	
        self.cur  = None
	#self.usr     = 'OtoDesk'
	#self.pas     = 'OtoDesk'
	#self.db_name = 'DunyaOtoDesk'

    def connect( self):
	pass

	#print usr, pas, schema
        #conn  = cx_Oracle.connect( "%s/%s@%s"%(usr,pas,schema))
        
	
        #conn = pyodbc.connect('DSN=asdad;UID=%s;PWD=%s'%(usr,pas))
    
    def run( self, sqls ):
        #print sqls
	logging.debug( "con run ------------------------- > ")
	#self.conn.execute('BEGIN EXCLUSIVE')
	cur  = self.conn.cursor()
	logging.debug( "cur run ------------------------- >")
	#try:
	cur.execute(sqls)
	don  = cur.fetchall()
	'''
	except:
	    logging.debug( str(sys.exc_info()[0]))
	    logging.debug ( 'sql hatasi -> %s'% sqls)
	    print str(sys.exc_info()[0])
	    don = [[]]
	'''
	    
	cur.close()
	self.conn.commit()
	#self.conn.close()
        return don
    
    def run_returns_as_dict( self, sqls ):
	
	logging.debug( "con run ------------------------- > ")
        cur  = self.conn.cursor()
	logging.debug( "cur run ------------------------- >")
        
        cur.execute(sqls)
	columns = [column[0] for column in cur.description]
        don = []
	for row in ResultIter( cur ):
	    r = map( lambda x:None if not x and x !=0 else x, row )
	    don.append(dict(zip(columns, r)))
        '''
        except:
            logging.debug( str(sys.exc_info()[0]))
            logging.debug ( 'sql hatasi -> %s'% sqls)
	    print str(sys.exc_info()[0])
            don = [[]]
        '''
        cur.close()
        return don
    
    def run_returns_as_object( self, sqls ):
	
	logging.debug( "con run ------------------------- > ")
        cur  = self.conn.cursor()
	logging.debug( "cur run ------------------------- >")
        #try:
	cur.execute(sqls)
	columns = [column[0] for column in cur.description]
	don = []
	for row in ResultIter( cur ):
	    r = map( lambda x:'' if not x and x !=0 else x, row )
	    don.append(Struct(**dict(zip(columns, r))))
        #except:
        #    print( str(sys.exc_info()[0]))
        #    print( 'sql hatasi -> %s'% sqls)
	#    print str(sys.exc_info()[0])
        #    don = [[]]
        cur.close()
        return don
    
    def run_returns_as_parent_child( self, sqls, child_parameters,data_types ):
	
        cur  = self.conn.cursor()
	cur2 = self.conn.cursor()
        #try:
	cur.execute(sqls)
	columns = [column[0] for column in cur.description]
	don = []
	for row in  ResultIter(cur):
	    parent_dict        = dict(zip(columns, row))
	    parent_obje        = Struct(**parent_dict)
	    parent_mapper      = {}    
	    parent_mapper[0]   = row[0]
	    data_formatla(row[0], data_types[0])
	    setattr(parent_obje,'mapper',parent_mapper)
	    #setattr(parent_obje,'children',children)
	    setattr(parent_obje,'base_attr',parent_dict[child_parameters[1]])
	     
	    don.append( parent_obje )
        #except:
        #    logging.debug( str(sys.exc_info()[0]))
        #    logging.debug ( 'sql hatasi -> %s'% sqls)
	#    print str(sys.exc_info()[0])
        #    don = [[]]
        cur.close()
        
        return don
    def get_children_by_parent( self, sqls, child_parameters , parent_paramters,data_types ):
	cur2 = self.conn.cursor()
	cur2.execute( sqls )
	columns2 = [column[0] for column in cur2.description]
	children = []
	for row2 in ResultIter(cur2):
	    row2 = map(lambda x:x, row2)
	    ch_dict = dict(zip(columns2,row2))
	    ch = Child(**ch_dict)
	    mapper_child = {0:parent_paramters[0]}
	    child_id = row2.pop(1)
	    for r in range( len(row2) ):
		if not mapper_child.has_key(r):
		    mapper_child[r] = data_formatla(row2[r], data_types[r])
	    setattr(ch,'base_attr',parent_paramters[1])
	    setattr(ch,'child_id',child_id) 
	    setattr(ch,'mapper',mapper_child)
	    children.append( ch )
	cur2.close()
	return children
    
    def dict_fetch_all( self, curs ):
	desc = curs.description
	return [
		dict( zip([col[0] for col in desc], row))
		for row in curs.fetchall()
		]
    
    def run_dml_return_id( self, sqls ):
        last_row_id = -1
	logging.debug( ' run_dml ----------> geldi' )
        cur       = self.conn.cursor()
        #try:
        logging.debug( ' run_dml ----------> insert' )
	#print sqls
        cur.execute(sqls)
	last_row_id = cur.lastrowid
        logging.debug( ' run_dml ----------> cur.execute ')
        logging.debug( ' run_dml ----------> curdon '  )
        self.conn.commit()
        cur.close()
        
        '''
        except:
            print ( str(sys.exc_info()[0]))
            print  ( 'sql hatasi -> %s'% sqls)
            cur.close()
            conn.close()
        '''
        return last_row_id

    def run_dml( self, sqls,type):
        logging.debug( ' run_dml ----------> geldi' )
        cur  = self.conn.cursor()
        don  = 1
        #try:
        #print sqls
        if type == 'insert':
            logging.debug( ' run_dml ----------> insert' )
            
            cur.execute(sqls)
            logging.debug( ' run_dml ----------> cur.execute ')
            logging.debug( ' run_dml ----------> curdon '  )
        elif type == 'update':
            logging.debug( ' run_dml ----------> sqls %s'%sqls )
            cur.execute(sqls)
        elif type == 'delete':
            logging.debug( ' run_dml ----------> sqls %s'%sqls )
            cur.execute(sqls)
        
	self.conn.commit()
        cur.close()
        '''
        #except:
            print ( str(sys.exc_info()[0]))
            print  ( 'sql hatasi -> %s'% sqls)
            don = 0
            cur.close()
            conn.close()
        '''
        return don
    

class DBObjects:
    def __init__( self ):
        self.object_list = []
	self.sq_ = conect()
	
    def get_raw_data_all(self, table_name, where="", order="",selects="",dict_mi = 0,child_parameters="",parent_parameters="",data_types="",group_by=""):
	#dict mi 0 ise normal, 1 ise dict 2 ise parent child
	if where != "":
            where = ' Where %s'%where
	if group_by !="":
	    group_by = ' GROUP BY %s'%group_by
        if order != "":
            order = ' ORDER BY %s'%order
        if selects == "":
            selects = '*'
        sqls = " Select %s from %s %s %s %s" %(selects,table_name,where,group_by,order)
	#print sqls
        logging.debug('get_raw_data_all ----------------> sqls %s' %sqls)
	
        if dict_mi == 'dict':
	    return self.sq_.run_returns_as_dict( sqls )
	elif dict_mi == 'object':
	    return self.sq_.run_returns_as_object( sqls )
	elif dict_mi == 'parent':
	    return self.sq_.run_returns_as_parent_child( sqls, child_parameters,data_types )
	elif dict_mi == 'child':
	    return self.sq_.get_children_by_parent( sqls, child_parameters,parent_parameters,data_types )
	
	else:
	    
	    data = self.sq_.run( sqls )
	    return data
    def get_parameters_all(self,table_name,where="",order="",selects = ""):
	self.object_list = []
        if where != "":
            where = ' Where %s'%where
        if order != "":
            order = ' ORDER BY %s'%order
        if selects == "":
            selects = '*'
        
        sqls = " Select %s from %s %s %s" %(selects,table_name,where,order)
        logging.debug('get_parameters_all ----------------> sqls %s' %sqls)
	#print sqls
        data = self.sq_.run( sqls )
        for d in data:
            obj = dataObj()
            obj.id = d[0]
            obj.ad = d[1]
            self.object_list.append( obj )
            
        return self.object_list   
    def get_parameters_all_with_id(self,table_name,where="",order="",selects = ""):
	self.object_list = []
        if where != "":
            where = ' Where %s'%where
        if order != "":
            order = ' ORDER BY %s'%order
        if selects == "":
            selects = '*'
        sqls = " Select %s from %s %s %s" %(selects,table_name,where,order)
        logging.debug('get_parameters_all ----------------> sqls %s' %sqls)
        data = self.sq_.run( sqls )
        for d in data:
            obj = dataObj()
            obj.id = d[0]
	    obj.ad = ''
	    i=0
	    for o in d:
		if i!= 0:
		    obj.ad += str(o)+' - '
		i += 1
	    obj.ad = obj.ad[:len(obj.ad)-3]
            self.object_list.append( obj )     
	return self.object_list
    def save_data ( self , table_name ,column_name, values,loop=True, user=None ):
        #### türkce karakter problemi var !!!!#########logging.debug( 'Save Parameters -------- >' + str(table_name) +' ::  ' + str(column_name)+' ::  ')
        sqls = '''insert into %s ( %s ) values ( %s )''' %(table_name,column_name,values)
        logging.debug( 'Save Parameters -------- > %s' %sqls )
        sqls.encode('latin5','ignore')
	#print sqls
        data = self.sq_.run_dml(sqls,'insert')
	if data == 1:
	    aciklama = 'Basarili --- >  ' +  sqls
	else:
	    aciklama = 'Hata --- > ' + sqls
	
	
        logging.debug( 'Save Parameters -------- > %s'%str(data) )
	if loop and user:
	    self.LOGSAVE(user, aciklama )
        return data
    def save_data_and_get_id ( self , table_name ,column_name, values , user=None):
        #### türkce karakter problemi var !!!!#########
        logging.debug( 'Save Parameters -------- >' + str(table_name) +' ::  ' + str(column_name)+' ::  ')
        sqls = '''insert into %s ( %s ) values ( %s )''' %(table_name,column_name,values)
        logging.debug( 'Save Parameters -------- > %s' %sqls )
        sqls.encode('latin5','ignore')
        scope = self.sq_.run_dml_return_id(sqls) 
        logging.debug( 'Save Parameters -------- > %s'%str(scope) )
	if scope != -1:
	    aciklama = '%s --- >  '%MESSAGES.basarili +  sqls
	else:
	    aciklama = '%s --- > '%MESSAGES.hata + sqls
	if user:
	    self.LOGSAVE(user, aciklama )
        return scope
    def update_data( self, table_name,column_name, values, where,user=None ):
	logging.debug( 'Save Parameters -------- >' + str(table_name) +' ::  ' + str(column_name)+' ::  ')
	c_list = column_name.split(',')
	v_list = values.split(',')
	set_str = ""
	for c in c_list:
	    set_str += c + ' = ' +  v_list[c_list.index(c)] 
	    set_str += ' , '
	set_str = set_str[:len(set_str)-2]
        sqls = '''update %s set %s where %s''' %(table_name,set_str,where)
	
        logging.debug( 'Save Parameters -------- > %s' %sqls )
        sqls.encode('latin5','ignore')
        data = self.sq_.run_dml(sqls,'update')
	if data == 1:
	   aciklama = '%s --- >  '%MESSAGES.basarili +  sqls
	else:
	    aciklama = '%s --- > '%MESSAGES.hata + sqls
	  
	if user:
	    self.LOGSAVE(user, aciklama )
        logging.debug( 'Save Parameters -------- > %s'%str(data) )
        return data  
    
    def delete_data( self, table_name,where,user=None ):
        sqls = '''delete from %s where %s''' %(table_name,where)
        sqls.encode('latin5','ignore')
	try:
	    data = self.sq_.run_dml(sqls,'delete')
	except:
	    data=0
	if data == 1:
	    aciklama = '%s --- >  '%MESSAGES.basarili +  sqls
	else:
	    aciklama = '%s --- > '%MESSAGES.hata + sqls
	if user:
	    self.LOGSAVE(user, aciklama )
        return data
    
    def widgetMaker(self, widget, objects):
	widget.Clear()
        for obj in objects:
	    #print obj.ad
            widget.Append(str(obj.ad), obj)
    def widgetMakerDict(self, widget, dict_):
	widget.Clear()
        for k,v in dict_.iteritems():
	    obj = dataObj()
	    obj.ad = k
	    obj.id = v
            widget.Append(str(obj.ad), obj)
	    obj = None
    def widgetMakerStr( self , widget , StrObjects ):
	widget.Clear()
        for obj in StrObjects:
	    #print obj
            widget.Append( str( obj))
    def widgetMakerConcate( self , widget , objects):
	widget.Clear()
        for obj in objects:
            widget.Append(str(obj.id) +'-'+ str(obj.ad) )
    def widgetMakerConcateObject( self , widget , objects):
	widget.Clear()
        for obj in objects:
            widget.Append((str(obj.id) +'-'+ str(obj.ad)),obj )
    def getIlceObjectListByIl( self, il_id ):
	ilceler = self.get_parameters_all( table_name="P_ILCELER", where="sehir=%s"%il_id ,order="ilce" )
	return ilceler
    def getTankParametersByProbe(self, probe_id):
	tank_object = self.get_raw_data_all(table_name = "VIEW_YAKIT_TANKI", where = "ID_PROBE=%s"%probe_id,dict_mi = 'object')
	return tank_object
    def getProbeByTank(self, tank_id):
	tank_object = self.get_raw_data_all(table_name = "VIEW_YAKIT_TANKI", where = "ID_YAKIT_TANKI=%s"%tank_id,dict_mi = 'object')
	return tank_object
    def getYakitAdiByYakitID( self, yakit_id):
	yakit_obj= self.get_raw_data_all(table_name = "YAKIT", where = "ID_YAKIT=%s"%yakit_id,dict_mi = 'object')
	yakit_adi = ""
	if len(yakit_obj) > 0:
	    yakit_adi = yakit_obj[0].YAKIT_ADI
	return yakit_adi
    def getTankHCesitByID( self , id_thc):
	thc_obj= self.get_raw_data_all(table_name = "P_TANK_H_CESIT", where = "ID_THC=%s"%id_thc,dict_mi = 'object')
        thc_adi = ""
	if len(thc_obj) > 0:
	    thc_adi = thc_obj[0].H_ACIKLAMA
	return thc_adi
    def get_bayi_object( self ):
        bayi =  self.get_raw_data_all(table_name = "P_BAYI",dict_mi = 'object' )
        if len( bayi ) > 0:
            bayi = bayi[0]
        else:
            bayi = None
        return bayi
    def getTankAdiByTankID(self, tank_id):
	thc_obj= self.get_raw_data_all(table_name = "YAKIT_TANKI", where = "ID_YAKIT_TANKI=%s"%tank_id,dict_mi = 'object')
        thc_adi = ""
	if len(thc_obj) > 0:
	    thc_adi = thc_obj[0].YAKIT_TANKI_ADI
	return thc_adi
    def getTanKapasiteByTankID(self, tank_id):
	thc_obj= self.get_raw_data_all(table_name = "YAKIT_TANKI", where = "ID_YAKIT_TANKI=%s"%tank_id,dict_mi = 'object')
        thc_kap = 0
	if len(thc_obj) > 0:
	    thc_kap = int(thc_obj[0].YAKIT_TANKI_KAPASITE)
	return thc_kap
    def getDurumAdiByDurumID( self, durum_id):
	durum_obj= self.get_raw_data_all(table_name = "P_DURUM", where = "ID_DURUM=%s"%durum_id,dict_mi = 'object')
	durum_aciklama = ""
	if len(durum_obj) > 0:
	    durum_aciklama = durum_obj[0].DURUM_ACIKLAMA
	return durum_aciklama    
    def getHataAciklamaHataID(self,hata_id):
	hata_obj = self.get_raw_data_all (table_name = "P_HATA_CESITLERI", where = "ID_HATA=%s"%hata_id,dict_mi="object")
	hata_aciklama = ""
	if len(hata_obj) > 0:
	    hata_aciklama = hata_obj[0].HATA_ACIKLAMA
	return hata_aciklama
    def getPumpOptionsByYakitID( self, yakit_id):
	pump_obj= self.get_raw_data_all(table_name = "VIEW_POMPA_TABANCALAR", where = "ID_YAKIT=%s"%yakit_id,dict_mi = 'object')
	return pump_obj
    def getLastTankValueByProbeId( self, tank_id ):
	tank_kayit_obj= self.get_raw_data_all(table_name = "TANK_KAYITLARI", where = "TANK_ID=%s AND POMPA_SATISI = 0 AND ID_TANK_KAYITLARI=(select MAX(ID_TANK_KAYITLARI) from TANK_KAYITLARI WHERE TANK_ID = %s AND POMPA_SATISI=0)"%(tank_id,tank_id),dict_mi = 'object')
	return tank_kayit_obj
    def getTabancaByTankId( self, tank_id ):
	tabanca_object = self.get_raw_data_all(table_name = "TABANCALAR", where = "TANK_ID=%s"%(tank_id),dict_mi = 'object')
	return tabanca_object
    def getBirimFiyatByTabancaID( self, tabanca_id ):
	birim_f_obj= self.get_raw_data_all(table_name = "VIEW_POMPA_TABANCALAR",selects="YAKIT_BIRIM_FIYAT", where = "ID_TABANCA=%s"%tabanca_id,dict_mi = 'object')
	return birim_f_obj
    def getLatestUndefinetPaymentSalebyPompaNo(self, pompa_no ):
	satis_obj = self.get_raw_data_all(table_name = "POMPA_SATIS_KAYIT", where = "POMPA_NO=%s AND ODEME_SEKLI=%s"%(pompa_no, ODEME_SEKILLERI.BELIRSIZ),order=" ID_SATIS DESC LIMIT 1 ",dict_mi = 'object')
	return satis_obj
    def getAracByCari( self, cari_id ):
	arac_obj_list = self.get_raw_data_all(table_name = "ARACLAR", where = "ID_CARI_HESAP=%s"%(cari_id),order=" PLAKA DESC ",dict_mi = 'object')
	return arac_obj_list
    def LOGSAVE(self,kisi,aciklama):
	self.save_data(table_name='LOG_TABLO',column_name='KISI,ISLEM',values='"%s","%s"'%(kisi,aciklama),loop=False)
    def getCardOptionsByRfID( self, rf_id ):
	
	kart_object = self.get_raw_data_all(table_name = "KART_LISTE_VIEW", where = "KART_SERI_NO='%s'"%rf_id,dict_mi = 'object')
	
	iskonto_bilgileri = None
	if len( kart_object ) > 0:
	    kart_object = kart_object[0]
	    if kart_object.ID_CARI_TIPI == CARI_TIPLER.MUSTERI:
		if kart_object.ID_ISKONTO and kart_object.ID_ISKONTO != "": 
		    iskonto_bilgileri = self.get_raw_data_all( table_name = "ISKONTO_YAKIT", where="ID_ISKONTO=%s"%kart_object.ID_ISKONTO, dict_mi='object')
	    setattr(kart_object,'iskonto_info',iskonto_bilgileri )
	else:
	    kart_object = None
	
	return kart_object
    def getActiveVardiaDefaultUser(self):
	aktif_vardiya = self.get_raw_data_all(table_name="VIEW_VARDIYA_ADI",
                                                         where="ID_VARDIYA_AKTIF = (select MAX(ID_VARDIYA_AKTIF) from VIEW_VARDIYA_ADI)",
                                                         dict_mi = 'object')
	if len(aktif_vardiya) > 0:
	    default_cari = self.get_raw_data_all( table_name="VARDIYA_CARI", where = "ID_ACTIVE=1 AND ID_VARDIYA=%s"% aktif_vardiya[0].ID_VARDIYA,
						 dict_mi = "object")
	    
	    if len( default_cari ) > 0:
		cari = default_cari[0]
		cari.ID_CARI_HESAP = cari.ID_CARI_HESAP
		cari.PLAKA         = ""
		return cari
	
	return None
    def getAktifVardiya ( self ):
	aktif_vardiya = self.get_raw_data_all(table_name="VIEW_VARDIYA_ADI",
                                                         where="ID_VARDIYA_AKTIF = (select MAX(ID_VARDIYA_AKTIF) from VIEW_VARDIYA_ADI)",
                                                         dict_mi = 'object')
	if len(aktif_vardiya) > 0:
	    return aktif_vardiya[0]
	
	else:
	    return None
    def getKalibrasyonDataAll(self,id_yakit_tanki = None):
	
	if not id_yakit_tanki:
	    kalibrasyon_cetvel = self.get_raw_data_all(table_name = "TANK_KALIBRASYON_TABLOSU",dict_mi= 'object')
	else:
	    kalibrasyon_cetvel = self.get_raw_data_all(table_name = "TANK_KALIBRASYON_TABLOSU",where="ID_YAKIT_TANKI=%s"%id_yakit_tanki,dict_mi= 'object')
	return_kalibrasyon = ''
	if len(kalibrasyon_cetvel) > 0 :
	    return_kalibrasyon = dict()
	    for k,v in groupby(kalibrasyon_cetvel,lambda x:x.ID_YAKIT_TANKI):
		hassasiyet = list(v)
		hassasiyet_ = hassasiyet[0].YUKSEKLIK
		
		return_kalibrasyon[k] = (hassasiyet,hassasiyet_,)
	    
	    return return_kalibrasyon
	
    def getTankMinMaxDataAll(self,id_yakit_tanki = None):
	
	if not id_yakit_tanki:
	    min_max_cetvel = self.get_raw_data_all(table_name = "YAKIT_TANKI_MIN_MAX",dict_mi= 'object')
	else:
	    min_max_cetvel = self.get_raw_data_all(table_name = "YAKIT_TANKI_MIN_MAX",where="ID_YAKIT_TANKI=%s"%id_yakit_tanki,dict_mi= 'object')
	return_kalibrasyon = ''
	if len(min_max_cetvel) > 0 :
	    return_kalibrasyon = dict()
	    for min_max_ in  min_max_cetvel:
		return_kalibrasyon[min_max_.ID_YAKIT_TANKI] = min_max_
		
	return return_kalibrasyon
	
    def select_insert(self,table_insert,column_name_insert,column_name_select,table_select,where ,user=None):
	
	sqls = '''insert into %s  ( %s )  select  %s  from %s where %s''' %(table_insert,column_name_insert,column_name_select,table_select,where)
	logging.debug( 'Save Parameters -------- > %s' %sqls )
        sqls.encode('latin5','ignore')
	#print sqls
        data = self.sq_.run_dml(sqls,'insert')
	if data == 1:
	    aciklama = '%s --- >  '%MESSAGES.basarili +  sqls
	else:
	    aciklama = '%s --- > '%MESSAGES.hata + sqls
	
	if user:
	    self.LOGSAVE(user, aciklama )
	
        logging.debug( 'Save Parameters -------- > %s'%str(data) )
	
	return data
    
    
	    