#!/usr/bin/env python
# -*- coding: latin5 -*-
from  style import uyari_message
class wsSonuc:
    def __init__(self,hata_code,parent,hata_oge,hata_msg,win_name):
        
        self.parent   = parent
        self.hata_oge = hata_oge
        self.win_name = win_name
        self.hata_msg = hata_msg
        
        if hata_code == 'bayi_404':
            self.BAYI_404( )
        elif hata_code =='int_bag_yok':
            self.INT_CHECK( )
        else:
            self.WS_SERVER_HATA( )
            
    def BAYI_404 (self):
        
        uyari_message(self.parent,'%s \n%s'%(self.hata_oge,self.hata_msg),self.win_name)
        
    def INT_CHECK (self):
        
        uyari_message(self.parent,'%s \n%s'%(self.hata_oge,self.hata_msg),self.win_name)
        
    def WS_SERVER_HATA(self):
        
        uyari_message(self.parent,'%s \n%s'%(self.hata_oge,self.hata_msg),self.win_name)