import suds
import urllib2
import base64
import logging
logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.INFO)
class HTTPSudsPreprocessor(urllib2.BaseHandler):

    def http_request(self, req):
        base64string = base64.encodestring('%s:%s' % ('otoweb', 'manager')).replace('\n', '')
        req.add_header('Content-Type', 'text/xml; charset=utf-8')
        req.add_header('TAURUS',"Basic %s" % base64string)
        return req

    https_request = http_request

URL = "http://localhost:8000/OtoTasitTanima/service.wsdl"
    
https = suds.transport.https.HttpTransport()
opener = urllib2.build_opener(HTTPSudsPreprocessor)
https.urlopener = opener
client = suds.client.Client(URL, transport = https)
client.options.cache.clear()
print client

