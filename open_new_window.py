from wx import FindWindowById
import sabitler
import style as s
import util
import wx
import  wx.lib.scrolledpanel as scrolled
class OpenNewWindow:
    
    def newWindow( self, parent, event, winClass, title, maximize=False ):
        
        window_id = sabitler.EVENT_EKRAN_BINDING[ event.GetEventObject().GetId() ]

        if  FindWindowById( window_id ):

            try:
                win = FindWindowById ( window_id )
                win.Raise()
                win.Restore()
                win.RequestUserAttention()
            except:
                win.Destroy()
                win = winClass( parent, window_id, title )
                win.thread_list = []
        else:
            win = winClass( parent, window_id, title )
            win.thread_list = []
        if maximize == True:
            win.Maximize()
        win.Show(True)

    def createStaticList( self, parent, data_headers ,dataResultSet ,item_id='',lineCheck=False, checked_set_method=None,checked_index=-1,lineRadio=False,radio_set_method=None,radio_index=-1):
        #p = wx.Panel(parent,-1, style = wx.TAB_TRAVERSAL
        #                 | wx.CLIP_CHILDREN
        #                 | wx.FULL_REPAINT_ON_RESIZE)
        if item_id == '':
            item_id = wx.NewId()
        p = scrolled.ScrolledPanel(parent,style = wx.TAB_TRAVERSAL
                      | wx.CLIP_CHILDREN | wx.EXPAND
                     ,id=item_id)
        text='<html><head></head><body><table border="2" width="600"><tr>'
        yatay_padding    = 8
        baslik_long_dict = {}
        
        #frame_color = wx.Colour(130,208,190)
        #p.SetBackgroundColour(  frame_color )
        gbs = wx.GridBagSizer(10,10 )
        col = 1
        type_dict = {}
        index = 0
        for h in data_headers:
            h_list = h.split(',')
            if index != checked_index and index != radio_index: 
                if index == 0:
                    if ( lineCheck or lineRadio ):
                        lbl_header        =  wx.StaticText(p, label=h_list[0],style=wx.ALIGN_CENTER)
                        s.setStaticTextHeader( lbl_header )
                        gbs.Add( lbl_header, (1,col) ,(1,1),  wx.ALL ,3)
                else:
                    lbl_header        =  wx.StaticText(p, label=h_list[0],style=wx.ALIGN_CENTER)
                    s.setStaticTextHeader( lbl_header )
                    
                    gbs.Add( lbl_header, (1,col) ,(1,1),  wx.ALL ,3)
                    text += '<th>'+h_list[0] + '</th>'
                    baslik_long_dict[index] = len( h_list[0])
                type_dict[ index ] = h_list[1]    
                index += 1
                col   += 1
        
        text = text.strip()
        text += '</tr>'
        

        line_num = 2
        self.line_dict = {}
        l_check = None
        l_radio = None
        self.last_selected = None
        
        for data in dataResultSet:
            text+='<tr align="center">'
            line_objects = []
            col_num  = 1
            index    = 0
            for d in data:
                if util.isDigit( d ):
                    alignment = wx.ALIGN_CENTER
                else:
                    alignment = wx.ALIGN_LEFT
                if index != checked_index and index != radio_index: 
                    if index == 0:
                        if lineCheck:
                            #eger bu true ise ilk degisken primary key olacak.
                            if not util.isDigit( d ):
                                d = line_num / 2
                            l_check = wx.CheckBox(p,d, "", style=alignment)
                            l_check.Bind(wx.EVT_CHECKBOX, lambda event: self.OnCheckClick(event, checked_set_method)  )
                            line_objects.append( l_check )
                            if checked_index != -1:
                                if int(data[ checked_index ]) == 1:
                                    l_check.SetValue( True )
                            gbs.Add( l_check, (line_num , col_num ),(1,1),wx.ALL, 3 )
                            
                            if lineRadio:
                                col_num += 1
                        if lineRadio:
                            #eger bu true ise ilk degisken primary key olacak.
                            if not util.isDigit( d ):
                                d = line_num / 2
                            l_radio = wx.RadioButton(p,d*100, "", style=alignment)
                            l_radio.Bind(wx.EVT_RADIOBUTTON, lambda event: self.OnRadioClick(event, radio_set_method )  )
                            line_objects.append( l_radio )
                            if radio_index != -1:
                                if int(data[ radio_index ]) == 1:
                                    l_radio.SetValue( True )
                            gbs.Add( l_radio, (line_num , col_num ),(1,1),wx.ALL, 3 )
                    else:
                        d_type = type_dict[index]
                        formatted_data = util.data_formatla( str(d), d_type )
                        lbl_data    =   wx.StaticText(p, label=formatted_data,style=alignment)
                        line_objects.append( lbl_data )
                        s.setStaticTextList( lbl_data )
                        '''
                        if util.is_odd( col_num ):
                            s.setStaticTextListBg( lbl_data )
                            print 'burada'
                        '''
                        gbs.Add( lbl_data, (line_num ,col_num ),(1,1),wx.ALL, 3 )
                        text +='<td>'+ formatted_data + '</td>'
                    col_num += 1
                    index += 1
                if l_check != None:
                    self.line_dict[ l_check.GetId() ] = line_objects
                if l_radio != None:
                    self.line_dict[ l_radio.GetId() ] = line_objects
            line_num += 1
            text+='</tr>'
                       
            line_                           =   wx.StaticLine(p, -1, style=wx.LI_HORIZONTAL)
            gbs.Add( line_,            (line_num,0 ),(1,len(data) + 2),flag=wx.EXPAND|wx.BOTTOM, border=10)
            line_num += 1
        
        s.setPanelListBg( p )
        p.SetSizerAndFit(gbs)
        p.SetupScrolling()
        #p.SetSize((300,300))
        p.SetAutoLayout(True)
        text+='</table></body></html>'
        setattr(p,'Text',text)
        #print text
        return p
    
   
    def OnCheckClick( self, event, checked_set_method ):
        
        items_to_paint = self.line_dict[ event.GetEventObject().GetId()]
        for item in items_to_paint:
            if isinstance( item, wx.CheckBox):
                if item.GetValue() == True:
                    boya = wx.RED
                else:
                    boya = s.default_list_text_color
            
            item.SetForegroundColour( boya)
            item.Refresh()
        checked_set_method(  event.GetEventObject().GetId(), event.GetEventObject().GetValue() )

        
    def createNullPanel( self,parent ):
        p = wx.Panel(parent,1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        
        return p

    
    def OnRadioClick( self, event, checked_set_method ):
        items_to_unpaint = []
        items_to_paint   = self.line_dict[ event.GetEventObject().GetId()]
        if self.last_selected:    
            items_to_unpaint =  self.line_dict[ self.last_selected ]
        for item in items_to_paint:
            boya = wx.RED
            item.SetForegroundColour( boya)
            item.Refresh()
        for item in items_to_unpaint:
            item.SetForegroundColour( s.default_list_text_color )
            item.Refresh()
        self.last_selected = event.GetEventObject().GetId()
        checked_set_method(  event.GetEventObject().GetId()/100 )

        