#!/usr/bin/env python
# -*- coding: latin5 -*-

import wx
import wx.dataview as dv
import style as s
import sabitler
from model import TreeListModel
from model import IndexListModel
from DBConnectLite import DBObjects
db = DBObjects()
import wx.lib.buttons as LibButtons
button_size  = sabitler.ana_button_size
image_path   = sabitler.image_path
import wx.combo
import wx.calendar
from local import MESSAGES
from datetime import date as dt
import util
class TreeListPanel(wx.Panel):
    def __init__(self, parent, model=None,guncelle_button=False,delete_button=False,
                 child_guncellemede_gidilecek_sayfa=None,parent_guncellemede_gidilecek_sayfa=None,
                 child_table_name=None,parent_table_name=None,parent_delete_where=None,child_delete_where=None,refresh_dict=None):
        wx.Panel.__init__(self, parent, 1)

        # Create a dataview control
        self.dvc = dv.DataViewCtrl(self,
                                   style= dv.DV_ROW_LINES # nice alternating bg colors
                                   | dv.DV_HORIZ_RULES
                                   | dv.DV_VERT_RULES
                                   | dv.DV_MULTIPLE
                                   )
        
        #self.SetSize((800,500) )
        self.model = model
        # Tel the DVC to use the model
        self.dvc.AssociateModel(self.model)
        self.child_guncellemede_gidilecek_sayfa  = child_guncellemede_gidilecek_sayfa
        self.parent_guncellemede_gidilecek_sayfa = parent_guncellemede_gidilecek_sayfa
        self.child_table_name                    = child_table_name
        self.parent_table_name                   = parent_table_name
        self.parent_                             = parent
        self.child_delete_where                  = child_delete_where
        self.parent_delete_where                 = parent_delete_where
        self.refresh_dict                        = refresh_dict
        
        # Define the columns that we want in the view.  Notice the 
        # parameter which tells the view which col in the data model to pull
        # values from for each view column.
        
        if 1:
            self.tr = tr = dv.DataViewTextRenderer()
            c0 = dv.DataViewColumn(self.model.headers[0],   # title
                                   tr,        # renderer
                                   0,         # data model column
                                   width=250)
            self.dvc.AppendColumn(c0)
        else:
            self.dvc.AppendTextColumn(self.model.headers[0],   0, width=80)
        i = 0
        for k in self.model.headers:
            if i != 0:#ilk kolonu atla
                c = self.dvc.AppendTextColumn(k,   i, mode=dv.DATAVIEW_CELL_SELECTED)
                c.Alignment = wx.ALIGN_RIGHT
            i += 1
        # Set some additional attributes for all the columns
        for c in self.dvc.Columns:
            c.Sortable = True
            c.Reorderable = True
            
        if delete_button==True:
            btn_delete_pressed  =  wx.Image("%sdelete_green.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
            image_delete = wx.Image("%sdelete.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
            btn_delete  = LibButtons.ThemedGenBitmapTextButton(self, id=wx.NewId(), bitmap=image_delete,label=MESSAGES.sil , style=wx.ALIGN_LEFT)
            btn_delete.Bind(wx.EVT_BUTTON, self.OnDeleteRows)
            btn_delete.SetToolTipString(MESSAGES.sil)
            btn_delete.SetBitmapSelected(btn_delete_pressed)
            s.setButtonFont( btn_delete )
        if guncelle_button==True:
            btn_update_pressed  =  wx.Image("%supdate_green_button_64.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
            image_update = wx.Image("%supdate_red_button_64.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
            btn_update  = LibButtons.ThemedGenBitmapTextButton(self, id=wx.NewId(), bitmap=image_update,label=MESSAGES.guncelle , style=wx.ALIGN_LEFT)
            btn_update.Bind(wx.EVT_BUTTON, self.OnUpdateRow)
            btn_update.SetToolTipString(MESSAGES.guncelle)
            btn_update.SetBitmapSelected(btn_update_pressed)
            s.setButtonFont( btn_update )
            
        self.Sizer = wx.BoxSizer(wx.VERTICAL)
        self.Sizer.Add(self.dvc, 1, wx.EXPAND)
        
        btnbox = wx.BoxSizer(wx.HORIZONTAL)
        #btnbox.Add(b1, 0, wx.LEFT|wx.RIGHT, 5)
        if guncelle_button == True:
            btnbox.Add(btn_update, 0, wx.LEFT|wx.RIGHT, 5)
        if delete_button == True:
            btnbox.Add(btn_delete, 0, wx.LEFT|wx.RIGHT, 5)
        self.Sizer.Add(btnbox, 0, wx.TOP|wx.BOTTOM, 5)

    def OnUpdateRow(self,event):
        
        #item           = self.dvc.GetSelection()
        #parent_mi = self.model.IsContainer(item)            
        #object_   = self.model.ItemToObject(item)

        if self.model.IsContainer(self.dvc.GetSelection())  :
            ###parent update olacakmis o halde selected_item ile guncellemede gidilecek book un page ini set edelim
            if self.parent_table_name and self.parent_guncellemede_gidilecek_sayfa>-1 :

                guncellemede_gidilecek_sayfa = self.parent_guncellemede_gidilecek_sayfa
                if isinstance(self.parent_.GetParent().selected_item,dict):
                    self.parent_.GetParent().selected_item[self.parent_guncellemede_gidilecek_sayfa] = self.model.ItemToObject(self.dvc.GetSelection()).base_attr
                else:
                    self.parent_.GetParent().selected_item = self.model.ItemToObject(self.dvc.GetSelection()).base_attr
        else:
            ###child update olacakmis o halde selected_item ile guncellemede gidilecek book un page ini set edelim
            if self.child_table_name and self.child_guncellemede_gidilecek_sayfa>-1 :
                guncellemede_gidilecek_sayfa = self.child_guncellemede_gidilecek_sayfa
                if isinstance(self.parent_.GetParent().selected_item,dict):
                    self.parent_.GetParent().selected_item[self.child_guncellemede_gidilecek_sayfa] = self.model.ItemToObject(self.dvc.GetSelection()).child_id
                else:
                    self.parent_.GetParent().selected_item = self.model.ItemToObject(self.dvc.GetSelection()).child_id

        self.parent_.SetSelection(guncellemede_gidilecek_sayfa)
        '''
        if guncellemede_gidilecek_sayfa>-1 and selected_item :
            ### simdi de selected_item i dondurelim
            ##simdi book un page ini set edelim
            print "00000000000000"
            print type(selected_item)
            if isinstance(selected_item,dict):
                self.parent_.GetParent().selected_item[guncellemede_gidilecek_sayfa] = selected_item
            else:
                self.parent_.GetParent().selected_item = selected_item      
            #self.parent_.GetParent().selected_item = selected_item       
            self.parent_.SetSelection(guncellemede_gidilecek_sayfa)
        '''
    def OnDeleteRows(self, evt):
        # Remove the selected row(s) from the model. The model will take care
        # of notifying the view (and any other observers) that the change has
        # happened.

        s.ok_cancel_message(parent=self, message=MESSAGES.sil_onay
                                      ,win_name="", ok_function=self.deleteKayit, cancel_function=None)
            
    def deleteKayit(self):
        items = self.dvc.GetSelections()
        delete = 0
        for item in items:
            parent_mi = self.model.IsContainer(item)            
            object_   = self.model.ItemToObject(item)
            
            if parent_mi :
            ###parent(s) silinecekmis  o halde silme fonksiyonuna degerleri hazirlayalim
                if self.parent_table_name and self.parent_delete_where :
                    delete_table_name = self.parent_table_name
                    delete_where      = self.parent_delete_where
                    delete_obj_id     = object_.base_attr
            else:
                ###child(s) slinecekmis  o halde silme fonksiyonuna degerleri hazirlayalim
                if self.child_table_name and self.child_delete_where :
                    delete_table_name = self.child_table_name
                    delete_where      = self.child_delete_where
                    delete_obj_id     = object_.child_id   
            try:
                delete = db.delete_data(table_name=delete_table_name,where='%s=%s'%(delete_where,delete_obj_id))           
            except:
                s.uyari_message(self,MESSAGES.sil_hata,'')
        if delete == 1:
            #rows = [self.model.GetRow(item) for item in items]
            #self.model.DeleteRows(rows)
            s.updateWindow(**self.refresh_dict)
        
class IndexListPanel(wx.Panel):
    def __init__(self, parent, model=None,
                 data=None,headers=None,
                 table_name=None,where=None,
                 delete_button=False,guncelle_button=False,
                 guncellemede_gidilecek_sayfa=None,
                 paginating_pane=False,cell_editable = None,
                 save_button = False,
                 save_method = None,
                 pasif_et    = False,
                 update_columns = '',
                 update_values  = ''):
        #self.log = log
        
        
        wx.Panel.__init__(self,parent, -1)
        self.parent_                      = parent
        self.delete_table_name            = table_name
        self.delete_where                 = where
        self.guncellemede_gidilecek_sayfa = guncellemede_gidilecek_sayfa
        self.save_button                  = save_button
        self.save_method                  = save_method
        self.pasif_et                     = pasif_et
        self.update_column                = update_columns
        self.update_value                 = update_values
        # Create a dataview control
        self.dvc = dv.DataViewCtrl(self,
                                   style=wx.BORDER_THEME
                                   | dv.DV_ROW_LINES # nice alternating bg colors
                                   | dv.DV_HORIZ_RULES
                                   | dv.DV_VERT_RULES
                                   | dv.DV_MULTIPLE                                   
                                   )
        
        # Create an instance of our simple model...
        header = map( lambda x:x.split(',')[0], headers )
        types  = map( lambda x:x.split(',')[1], headers )
        if model is None:
            self.model = IndexListModel(data,types)
        else:
            self.model = model            

        # ...and associate it with the dataview control.  Models can
        # be shared between multiple DataViewCtrls, so this does not
        # assign ownership like many things in wx do.  There is some
        # internal reference counting happening so you don't really
        # need to hold a reference to it either, but we do for this
        # example so we can fiddle with the model from the widget
        # inspector or whatever.
        self.dvc.AssociateModel(self.model)

        # Now we create some columns.  The second parameter is the
        # column number within the model that the DataViewColumn will
        # fetch the data from.  This means that you can have views
        # using the same model that show different columns of data, or
        # that they can be in a different order than in the model.
        i=0
        for k in header:
            if i>=1:
                if not cell_editable :
                    self.dvc.AppendTextColumn(k,  i, width=140, mode=dv.DATAVIEW_CELL_SELECTED)
                elif cell_editable == 'DATAVIEW_CELL_EDITABLE':
                    
                    self.dvc.AppendTextColumn(k,  i, width=140, mode=dv.DATAVIEW_CELL_EDITABLE)
                    self.Bind(dv.EVT_DATAVIEW_ITEM_EDITING_DONE,self.OnEditingDone, self.dvc)
                    self.Bind(dv.EVT_DATAVIEW_ITEM_VALUE_CHANGED,self.OnValueChanged, self.dvc)
                    

            i+=1
        
        #c0 = self.dvc.PrependTextColumn(header[0], 0, width=40)
        # There are Prepend methods too, and also convenience methods
        # for other data types but we are only using strings in this
        # example.  You can also create a DataViewColumn object
        # yourself and then just use AppendColumn or PrependColumn.
        c0 = self.dvc.PrependTextColumn(header[0], 0, width=40)

        # The DataViewColumn object is returned from the Append and
        # Prepend methods, and we can modify some of it's properties
        # like this.
        c0.Alignment = wx.ALIGN_RIGHT
        c0.Renderer.Alignment = wx.ALIGN_RIGHT
        c0.MinWidth = 30

        # Through the magic of Python we can also access the columns
        # as a list via the Columns property.  Here we'll mark them
        # all as sortable and reorderable.
        for c in self.dvc.Columns:
            c.Sortable = True
            c.Reorderable = True

        # Let's change our minds and not let the first col be moved.
        c0.Reorderable = False

        # set the Sizer property (same as SetSizer)
        self.Sizer = wx.BoxSizer(wx.VERTICAL) 
        self.Sizer.Add(self.dvc, 1, wx.EXPAND)
        
        # Add some buttons to help out with the tests
        #b1 = wx.Button(self, label="New View", name="newView")
        #self.Bind(wx.EVT_BUTTON, self.OnNewView, b1)
        #b2 = wx.Button(self, label="Add Row")
        #self.Bind(wx.EVT_BUTTON, self.OnAddRow, b2)
        if delete_button==True:
            btn_delete_pressed  = wx.Image("%sdelete_green.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
            image_delete        = wx.Image("%sdelete.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
            btn_delete          = LibButtons.ThemedGenBitmapTextButton(self, id=wx.NewId(), bitmap=image_delete,label=MESSAGES.sil, style=wx.ALIGN_LEFT)
            btn_delete.Bind(wx.EVT_BUTTON, self.OnDeleteRows)
            btn_delete.SetToolTipString(MESSAGES.sil)
            btn_delete.SetBitmapSelected(btn_delete_pressed)
            s.setButtonFont( btn_delete )
        if guncelle_button == True:
            btn_update_pressed  = wx.Image("%supdate_green_button_64.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
            image_update        = wx.Image("%supdate_red_button_64.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
            btn_update          = LibButtons.ThemedGenBitmapTextButton(self, id=wx.NewId(), bitmap=image_update,label=MESSAGES.guncelle, style=wx.ALIGN_LEFT)
            btn_update.Bind(wx.EVT_BUTTON, self.OnUpdateRow)
            btn_update.SetToolTipString(MESSAGES.guncelle)
            btn_update.SetBitmapSelected(btn_update_pressed)
            s.setButtonFont( btn_update )
        if save_button == True:
            btn_save_pressed = wx.Image("%ssave3_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
            image_update     = wx.Image("%ssave2_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
            btn_save         = LibButtons.ThemedGenBitmapTextButton(self, id=wx.NewId(), bitmap=image_update,label=MESSAGES.kaydet , style=wx.ALIGN_LEFT)
            btn_save.Bind(wx.EVT_BUTTON, self.OnSaveRows)
            btn_save.SetToolTipString(MESSAGES.kaydet)
            btn_save.SetBitmapSelected(btn_save_pressed)
            s.setButtonFont( btn_save )
        #b3 = wx.Button(self, label="Kay�t(lar�) Sil")
        #self.Bind(wx.EVT_BUTTON, self.OnDeleteRows, b3)

        btnbox  = wx.BoxSizer(wx.HORIZONTAL)
        pagebox = wx.BoxSizer(wx.HORIZONTAL)
        if paginating_pane:
            paginating_pane.Reparent( self )
            pagebox.Add( paginating_pane, 1, wx.EXPAND, 0)
        #btnbox.Add(b1, 0, wx.LEFT|wx.RIGHT, 5)
        if guncelle_button == True:
            btnbox.Add(btn_update, 0, wx.LEFT|wx.RIGHT, 5)
        if delete_button == True:
            btnbox.Add(btn_delete, 0, wx.LEFT|wx.RIGHT, 5)
        if save_button == True:
            btnbox.Add(btn_save,0,wx.LEFT|wx.RIGHT,5)
        self.Sizer.Add ( pagebox,0, wx.TOP|wx.BOTTOM, 5)
        self.Sizer.Add ( btnbox, 0, wx.TOP|wx.BOTTOM, 5)
    
        # Bind some events so we can see what the DVC sends us
        #self.Bind(dv.EVT_DATAVIEW_ITEM_EDITING_DONE, self.OnEditingDone, self.dvc)
        #self.Bind(dv.EVT_DATAVIEW_ITEM_VALUE_CHANGED, self.OnValueChanged, self.dvc)
        
    def OnNewView(self, evt):
        f = wx.Frame(None, title="New view, shared model", size=(600,400))
        TestPanel(f, self.log, self.model)
        b = f.FindWindowByName("newView")
        b.Disable()
        f.Show()

    def OnUpdateRow(self,event):

        if self.guncellemede_gidilecek_sayfa != None:
            item = self.dvc.GetSelection()
            if isinstance(self.parent_.GetParent().selected_item,dict):
                self.parent_.GetParent().selected_item[self.guncellemede_gidilecek_sayfa] = self.model.GetValue(item,0)
            else:
                self.parent_.GetParent().selected_item = self.model.GetValue(item,0)
            self.parent_.SetSelection(self.guncellemede_gidilecek_sayfa)
    def OnDeleteRows(self, evt):
        # Remove the selected row(s) from the model. The model will take care
        # of notifying the view (and any other observers) that the change has
        # happened.
        
        if self.delete_table_name and self.delete_where :
            s.ok_cancel_message(parent=self, message=MESSAGES.sil_onay
                                          ,win_name="", ok_function=self.deleteKayit, cancel_function=None)
        
    def deleteKayit(self):
        
        items = self.dvc.GetSelections()
        delete = 0
        for i in items:
            try:
                if self.pasif_et == False:
                    delete = db.delete_data(table_name=self.delete_table_name,where='%s=%s'%(self.delete_where,self.model.GetValue(i,0)))
                else:
                    delete = db.update_data(table_name=self.delete_table_name,column_name=self.update_column,values='%s'%self.update_value,where='%s=%s'%(self.delete_where,self.model.GetValue(i,0)))
            except:
                s.uyari_message(self,MESSAGES.sil_hata,MESSAGES.hata)
        if delete == 1:
            rows = [self.model.GetRow(item) for item in items]
            self.model.DeleteRows(rows)

        
    def OnAddRow(self, evt):
        # Add some bogus data to a new row in the model's data
        id = len(self.model.data) + 1
        value = [str(id),
                 'new artist %d' % id,
                 'new title %d' % id,
                 'genre %d' % id]
        self.model.AddRow(value)
                

    def OnEditingDone(self, evt):
        #self.log.write("OnEditingDone\n")
        pass

    def OnValueChanged(self, evt):
        #self.log.write("OnValueChanged\n")
        pass
    
    def OnSaveRows(self,evt):
       self.save_method(event=evt)

class PaginatingIndexPanel:
    def __init__(self, parent, table_name, selects="*",
                 where="", order="",parent_sizer=None,
                 grid_pane_id=0, headers=[],totalcoloum='',
                 ekran_orani=1,cell_editable=None):
        self.current_page = 1
        self.parent_sizer = parent_sizer
        self.parent = parent
        self.sayfa_sayisi = 10
        self.table_name   = table_name
        self.selects      = selects
        self.where        = where
        self.order        = order
        self.headers      = headers
        self.totalcoloum  = totalcoloum
        self.grid_pane_id = grid_pane_id
        self.orientation  = ekran_orani 
        self.paginating_bilgileri_set_et( table_name, where ) 
        self.p            = self.get_page_panel() 
        self.data_set     = self.GetDataSet( table_name, selects, where, order, self.current_page )
        self.cell_editable = cell_editable
        self.setIndexListPane ()
        
    def get_page_panel(self):
        
        p = wx.Panel(self.parent,-1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE | wx.EXPAND
                    )
        #p.PAINT_GRADIENTS = True
        #panelStyle = (p.PAINT_GRADIENTS and [wx.BORDER_NONE] or [0])[0]
        
        #p.endColour  = wx.Colour( 140,140,140 )
        #self.startColour =  wx.RED 
        #p.startColour  = wx.WHITE
        
        bs = wx.BoxSizer( wx.HORIZONTAL )
        
        btn_next_pressed  =  wx.Image("%ssonraki_pressed_16.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_next        = wx.Image("%ssonraki_16.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        btn_next          = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_next,label="" , style=wx.ALIGN_CENTER)
        btn_next.Bind(wx.EVT_BUTTON, self.OnNext )
        btn_next.SetToolTipString(MESSAGES.sonraki)
        btn_next.SetBitmapSelected(btn_next_pressed)
        s.setButtonFont( btn_next )
        
        btn_prev_pressed  =  wx.Image("%sonceki_pressed_16.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_prev        = wx.Image("%sonceki_16.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        btn_prev          = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_prev,label="" )
        btn_prev.Bind(wx.EVT_BUTTON, self.OnPrev )
        btn_prev.SetToolTipString(MESSAGES.onceki)
        btn_prev.SetBitmapSelected(btn_prev_pressed)
        s.setButtonFont( btn_prev )
        
        
        
        choices = list(range(1,self.sayfa_sayisi+ 1))
        choices = map(lambda x:str(x), choices)
        self.cmb_page    = wx.ComboBox(p,choices=choices, style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.cmb_page.Bind(wx.EVT_COMBOBOX,self.OnComboChange)
        self.cmb_page.SetStringSelection(str(1))
        bs.Add( btn_prev, 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL  )
        bs.Add( self.cmb_page , 0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL  )
        bs.Add( btn_next, 0,wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL  )
        if self.totalcoloum != '':
            self.get_total_set= self.GetTotalSet(self.table_name,self.selects,self.where,self.totalcoloum)
            if len(self.get_total_set)==1:
                for totalkey,total in self.get_total_set[0].iteritems():
                    lbl_toplam   = s.TransparentText(p,label=totalkey)
                    lbl_toplam_query   = s.TransparentText(p, label=util.data_formatla(total,'currency'))
                    #lbl_space=s.TransparentText(p,label="         ")
                    s.setStaticTextPompaHeader( lbl_toplam,font=14)
                    s.setStaticTextPompaPanelList( lbl_toplam_query,font=14)
                    bs.Add((25,1),0,wx.ALIGN_RIGHT)
                    bs.Add(lbl_toplam,0,wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL)
                    bs.Add(lbl_toplam_query,0,wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL)
        
        #s.BindEvents( p )
        p.SetSizerAndFit ( bs )
        return p
        
    def paginating_bilgileri_set_et( self, table_name, where ):
        
        total_kayit_s = db.get_raw_data_all(table_name,selects="count(*)", where=where )[0][0]
        mod_sayfa_sayisi  = total_kayit_s%sabitler.rapor_sayfa_kayit_sayisi
        son_sayfa_ekle    = 0
        if mod_sayfa_sayisi != 0:
            son_sayfa_ekle = 1
        
        self.sayfa_sayisi = total_kayit_s / sabitler.rapor_sayfa_kayit_sayisi + son_sayfa_ekle
        
    def GetDataSet( self,table_name, select="*", where="", order="", current_page=1 )   :
        
        order += " LIMIT %s,%s "%(sabitler.rapor_sayfa_kayit_sayisi*( current_page - 1), sabitler.rapor_sayfa_kayit_sayisi)
        data_query=db.get_raw_data_all(table_name,order =order,
                    selects=select,where=where)
        
        return data_query
    def GetTotalSet(self,table_name,select='',where="",totalcoloum="") :
        select='%s'%totalcoloum
        data_query=db.get_raw_data_all(table_name,
                    selects=select,where=where,dict_mi='dict')
        return data_query
    def OnNext( self, event ):
        if int(self.cmb_page.GetStringSelection()) < self.sayfa_sayisi:
            self.p.Reparent( self.parent )
            self.current_page = int(self.cmb_page.GetStringSelection()) + 1
            self.data_set     = self.GetDataSet(self.table_name, self.selects, self.where, self.order, self.current_page  )
            self.cmb_page.SetStringSelection(str(self.current_page))
            self.setIndexListPane()
        
    
    def OnPrev( self, event ):
        if int(self.cmb_page.GetStringSelection()) > 1:
            self.p.Reparent( self.parent )
            self.current_page = int(self.cmb_page.GetStringSelection()) - 1
            self.data_set     = self.GetDataSet(self.table_name, self.selects, self.where, self.order, self.current_page  )
            self.cmb_page.SetStringSelection(str(self.current_page))
            self.setIndexListPane()
    def OnComboChange( self, event ):
        self.p.Reparent( self.parent )
        self.current_page = int(self.cmb_page.GetStringSelection())
        self.data_set     = self.GetDataSet(self.table_name, self.selects, self.where, self.order, self.current_page  )
        self.cmb_page.SetStringSelection(str(self.current_page))
        self.setIndexListPane()
    
    def setIndexListPane( self ):
        list_pane = IndexListPanel( self.parent, data = self.data_set, headers= self.headers, paginating_pane = self.p,cell_editable = self.cell_editable)
        if self.grid_pane_id != 0:
            wx.FindWindowById( self.grid_pane_id ).Destroy()
        self.grid_pane_id = list_pane.GetId()
        #print self.grid_pane_id
        self.parent_sizer.Add( list_pane,self.orientation,wx.EXPAND )
        self.parent.rapor_panel_id = self.grid_pane_id
        self.parent.Layout()

def createSonSatislar( parent ):
        
        data      = db.get_raw_data_all( table_name = "VIEW_POMPA_SATIS_RAPOR",selects="ID_SATIS,TARIH,PLAKA,SATIS_LITRE,BIRIM_FIYAT,TUTAR,ODEME_SEKLI", order ="ID_SATIS DESC LIMIT 15" ) 
        headers   = ['ID,digit','%s,datetime'%MESSAGES.tarih,'%s,string'%MESSAGES.plaka,'%s,digit'%MESSAGES.litre,'%s,digit'%MESSAGES.br_fiyat,'%s,digit'%MESSAGES.tutar,'%s,text'%MESSAGES.odeme]
        list_pane = IndexListPanel( parent, data = data, headers= headers )
        list_pane.SetSizeHints(1360,400,1360,600)
        list_pane.SetId( sabitler.EKRAN_ID_MAP.OTOMASYON_EKRANLAR.id_e_otomasyon_total_satislar )
        return list_pane
    
def createCariSatislar( parent,selects_,where_,order_ ,group_by_,list_cari_hesaplar,cari_hesap,OnPrintPreview):
        #odeme_sekilleri = sabitler.ODEME_SEKILLERI.dict_values
        data_formatli = []
        if list_cari_hesaplar == []:
            data = db.get_raw_data_all( table_name = "VIEW_POMPA_SATIS_RAPOR",selects=selects_, where = where_, order = order_,group_by = group_by_)    
            if data !=[]:
                    belirsiz_toplam = filter(lambda x:x[2]==0,data)
                    if belirsiz_toplam == []:
                        belirsiz_toplam = float(0.0)
                    else:
                        belirsiz_toplam = belirsiz_toplam[0][4]
                    nakit_toplam = filter(lambda x:x[2]==1,data)
                    if nakit_toplam == []:
                        nakit_toplam = float(0.0)
                    else:
                        nakit_toplam = nakit_toplam[0][4]
                    kkart_toplam = filter(lambda x:x[2]==2,data)
                    if kkart_toplam == []:
                        kkart_toplam = float(0.0)
                    else:
                        kkart_toplam = kkart_toplam[0][4]
                    veresiye_toplam = filter(lambda x:x[2]==3,data)
                    if veresiye_toplam == []:
                        veresiye_toplam = float(0.0)
                    else:
                        veresiye_toplam = veresiye_toplam[0][4]
                    
                    toplam = belirsiz_toplam+nakit_toplam+kkart_toplam+veresiye_toplam
                    data_format = (cari_hesap.id,cari_hesap.ad,belirsiz_toplam,nakit_toplam,kkart_toplam,veresiye_toplam,toplam)
                    data_formatli.append(data_format)
        else:
            for musteri_cari in list_cari_hesaplar:
                data = db.get_raw_data_all( table_name = "VIEW_POMPA_SATIS_RAPOR",selects=selects_, where = where_%str(musteri_cari.id), order = order_,group_by = group_by_)       
                if data !=[]:
                    belirsiz_toplam = filter(lambda x:x[2]==0,data)
                    if belirsiz_toplam == []:
                        belirsiz_toplam = float(0.0)
                    else:
                        belirsiz_toplam = belirsiz_toplam[0][4]
                    nakit_toplam = filter(lambda x:x[2]==1,data)
                    if nakit_toplam == []:
                        nakit_toplam = float(0.0)
                    else:
                        nakit_toplam = nakit_toplam[0][4]
                    kkart_toplam = filter(lambda x:x[2]==2,data)
                    if kkart_toplam == []:
                        kkart_toplam = float(0.0)
                    else:
                        kkart_toplam = kkart_toplam[0][4]
                    veresiye_toplam = filter(lambda x:x[2]==3,data)
                    if veresiye_toplam == []:
                        veresiye_toplam = float(0.0)
                    else:
                        veresiye_toplam = veresiye_toplam[0][4]
                    
                    toplam = belirsiz_toplam+nakit_toplam+kkart_toplam+veresiye_toplam
                    data_format = (musteri_cari.id,musteri_cari.ad,belirsiz_toplam,nakit_toplam,kkart_toplam,veresiye_toplam,toplam)
                    data_formatli.append(data_format)

        headers   = ['ID,digit','%s, text'%(MESSAGES.sablon_iki_kelime%( MESSAGES.cari,MESSAGES.adi)),'%s,digit'%MESSAGES.belirsiz,'%s,digit'%MESSAGES.nakit,'%s,digit'%MESSAGES.kredi_karti,'%s,digit'%MESSAGES.veresiye,'%s,digit'%MESSAGES.tutar]
        if not OnPrintPreview :
            list_pane = IndexListPanel( parent, data = data_formatli, headers= headers )
            list_pane.SetSizeHints(1360,400,1360,600)
            list_pane.SetId( sabitler.EKRAN_ID_MAP.RAPORLAR_EKRANLAR.id_e_genel_toplam_satis_rapor )
            return list_pane
        else:
            return data_formatli,headers