#!/usr/bin/env python
# -*- coding: latin5 -*-

import wx
import wx.html2 as webview
import os
import sys
import time
import wx.lib.agw.gradientbutton as GB
import wx.grid as gridlib
import logging
from DBConnectLite import dataObj
import style
import model
from getGridDataList import IndexListPanel,PaginatingIndexPanel
param = model.Parameters()
from printPage import Printer, HtmlPrinter, PrintTablePreview
from getGridDataList import TreeListPanel
import wx.lib.agw.aui as aui
'''
import win32com.client
from win32api import GetSystemMetrics
try:
    from agw import flatnotebook as fnb
except ImportError:
    import wx.lib.agw.flatnotebook as fnb
'''  
import win32com.client
import sabitler
from win32api import GetSystemMetrics
from local import MESSAGES
import wx.lib.buttons as LibButtons
button_size  = sabitler.ana_button_size
image_path   = sabitler.image_path
ekstra_idler = sabitler.EKRAN_ID_MAP.ISKONTO_YAKIT_EKLE 

id_b_iskonto_tanim      = sabitler.EKRAN_ID_MAP.ISLEMLER_BUTONLAR.id_b_iskonto_tanim
id_b_cari_kartlar       = sabitler.EKRAN_ID_MAP.ISLEMLER_BUTONLAR.id_b_cari_kartlar
id_b_kart_tanimlama     = sabitler.EKRAN_ID_MAP.ISLEMLER_BUTONLAR.id_b_kart_tanimlama
id_b_kart_listesi       = sabitler.EKRAN_ID_MAP.ISLEMLER_BUTONLAR.id_b_kart_listesi
id_b_veresiye_islem     = sabitler.EKRAN_ID_MAP.ISLEMLER_BUTONLAR.id_b_veresiye_islem
id_b_servis_islem       = sabitler.EKRAN_ID_MAP.ISLEMLER_BUTONLAR.id_b_servis_islem
id_b_irsaliye_islem     = sabitler.EKRAN_ID_MAP.ISLEMLER_BUTONLAR.id_b_irsaliye_islem
id_b_kalibrasyon_islem  = sabitler.EKRAN_ID_MAP.ISLEMLER_BUTONLAR.id_b_kalibrasyon_islem
id_b_web_uygulamasi     = sabitler.EKRAN_ID_MAP.ISLEMLER_BUTONLAR.id_b_web_uygulamasi
class win_Islemler(wx.Frame):
    def __init__(self, parent, id_, title):
        
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR | wx.MAXIMIZE
                                  ))
        self.O  = param.get_root( self ).O
        self.thread_params = param.get_root( self ).thread_params
        self.GValuesDict = None
        self.SourceDataBase = None
        self.RS = None
        self.previousGValuesDict = None
        self.oncekiDeger = False
        #font1 = wx.Font(9, wx.SWISS, wx.NORMAL, wx.BOLD, False, u'Verdana')
        #self.CreateStatusBar() 
        #filemenu= wx.Menu()
        #aboutItem = filemenu.Append(wx.ID_ABOUT, "&Hakk�nda"," Program Hakk�nda Bilgi")
        #self.Bind(wx.EVT_MENU, self.OnAbout, aboutItem)
        #filemenu.AppendSeparator()
        #exitItem = filemenu.Append(wx.ID_EXIT,"&��k��"," Program� Kapat")
        #self.Bind(wx.EVT_MENU, self.OnExit, exitItem)
        
        #item = wx.MenuItem(filemenu, MENU_HIDE_X, "Hide X Button", "Hide X Button", wx.ITEM_CHECK)
        
        #self.Bind(wx.EVT_CLOSE, self.OnCloseWindow)

        #menuBar = wx.MenuBar()
        #menuBar.Append(filemenu,"&Dosya")
        #self.SetMenuBar(menuBar)
        #self.CreateRightClickMenu()
        self.LayoutItems()
        self.Centre()
        #icon1           = wx.Icon("buski.ico", wx.BITMAP_TYPE_ICO)
        #self.SetIcon(icon1)
        
        self.Show(True)
    
    def LayoutItems(self):
        mainSizer = wx.GridBagSizer(0,0)
        self.Freeze()
        self.SetSizer(mainSizer)
        winSize =  self.GetSize()
        self.top_panel = wx.Panel(self, wx.ID_ANY,size=(winSize[0],540))
        self.top_panel.SetBackgroundColour( wx.WHITE)
        self.top_sizer = wx.GridBagSizer(1,1)
        self.top_panel.SetSizer( self.top_sizer )
        
        self._islemler_iskonto_tanimi_pressed  =  wx.Image("%ssale_accepted_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_islemler_iskonto_tanimi = wx.Image(MESSAGES.iskonto_ekrani_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_islemler_iskonto_tanimi  = wx.BitmapButton(self.top_panel, id=id_b_iskonto_tanim, bitmap=image_islemler_iskonto_tanimi,pos=(0,0),
        size = (image_islemler_iskonto_tanimi.GetWidth()+5, image_islemler_iskonto_tanimi.GetHeight()+5))
        self.btn_islemler_iskonto_tanimi.Bind(wx.EVT_BUTTON, self.OnIslemlerMenuPressed)
        self.btn_islemler_iskonto_tanimi.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.iskonto, MESSAGES.tanimlari ))
        self.btn_islemler_iskonto_tanimi.SetBitmapSelected(self._islemler_iskonto_tanimi_pressed)
        self.top_sizer.Add(self.btn_islemler_iskonto_tanimi, (0,0),(1,1),wx.ALL)
        
        self.islemler_cari_kartlar_pressed  =  wx.Image("%scard_accepted_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_islemler_cari_kartlar = wx.Image(MESSAGES.cari_kart_ekrani_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_islemler_cari_kartlar  = wx.BitmapButton(self.top_panel, id=id_b_cari_kartlar, bitmap=image_islemler_cari_kartlar,pos=(0,1),
        size = (image_islemler_cari_kartlar.GetWidth()+5, image_islemler_cari_kartlar.GetHeight()+5))
        self.btn_islemler_cari_kartlar.Bind(wx.EVT_BUTTON, self.OnIslemlerMenuPressed)
        self.btn_islemler_cari_kartlar.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.cari, MESSAGES.kartlar ))
        self.btn_islemler_cari_kartlar.SetBitmapSelected(self.islemler_cari_kartlar_pressed)
        self.top_sizer.Add(self.btn_islemler_cari_kartlar,  (0,1),(1,1), wx.ALL)
        '''
        self.islemler_card_definitions_pressed  =  wx.Image("%scard_add_accepted_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_islemler_card_definitions = wx.Image("%scard_add_2_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_islemler_card_definitions  = wx.BitmapButton(self.top_panel, id=id_b_kart_tanimlama, bitmap=image_islemler_card_definitions,pos=(0,1),
        size = (image_islemler_card_definitions.GetWidth()+5, image_islemler_card_definitions.GetHeight()+5))
        self.btn_islemler_card_definitions.Bind(wx.EVT_BUTTON, self.OnIslemlerMenuPressed)
        self.btn_islemler_card_definitions.SetToolTipString("Kart Tan�mlamalar")
        self.btn_islemler_card_definitions.SetBitmapSelected(self.islemler_card_definitions_pressed)
        self.top_sizer.Add(self.btn_islemler_card_definitions,  (0,2),(1,1), wx.ALL)
        
        self.islemler_card_list_pressed  =  wx.Image("%scard_list_accepted_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_islemler_card_list = wx.Image("%scard_list_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_islemler_card_list  = wx.BitmapButton(self.top_panel, id=id_b_kart_listesi, bitmap=image_islemler_card_list,pos=(0,1),
        size = (image_islemler_card_list.GetWidth()+5, image_islemler_card_list.GetHeight()+5))
        #self.btn_islemler_card_list.Bind(wx.EVT_BUTTON, self.OnIslemlerMenuPressed)
        self.btn_islemler_card_list.SetToolTipString("Kart Listesi")
        self.btn_islemler_card_list.SetBitmapSelected(self.islemler_card_list_pressed)
        self.top_sizer.Add(self.btn_islemler_card_list,  (0,3),(1,1), wx.ALL)
        
        self.islemler_kalib_islemleri_pressed  =  wx.Image("%skalibrasyon_accepted_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_islemler_kalib_islemleri = wx.Image("%skalibrasyon_yazili_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_islemler_kalib_islemleri  = wx.BitmapButton(self.top_panel, id=id_b_kalibrasyon_islem, bitmap=image_islemler_kalib_islemleri,pos=(0,1),
        size = (image_islemler_kalib_islemleri.GetWidth()+5, image_islemler_kalib_islemleri.GetHeight()+5))
        self.btn_islemler_kalib_islemleri.Bind(wx.EVT_BUTTON, self.OnIslemlerMenuPressed)
        self.btn_islemler_kalib_islemleri.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.kalibrasyon, MESSAGES.islemleri ))
        self.btn_islemler_kalib_islemleri.SetBitmapSelected(self.islemler_kalib_islemleri_pressed)
        self.top_sizer.Add(self.btn_islemler_kalib_islemleri,  (1,0),(1,1), wx.ALL)
        
        self.islemler_veresiye_fis_pressed  =  wx.Image("%sveresiye_accepted_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_islemler_veresiye_fis = wx.Image("%sveresiye_yazili_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_islemler_veresiye_fis  = wx.BitmapButton(self.top_panel, id=id_b_veresiye_islem, bitmap=image_islemler_veresiye_fis,pos=(0,1),
        size = (image_islemler_veresiye_fis.GetWidth()+5, image_islemler_veresiye_fis.GetHeight()+5))
        #self.btn_islemler_veresiye_fis.Bind(wx.EVT_BUTTON, self.OnIslemlerMenuPressed)
        self.btn_islemler_veresiye_fis.SetToolTipString("Veresiye ��lemleri")
        self.btn_islemler_veresiye_fis.SetBitmapSelected(self.islemler_veresiye_fis_pressed)
        self.top_sizer.Add(self.btn_islemler_veresiye_fis,  (1,0),(1,1), wx.ALL)
        '''
        self.islemler_services_pressed  =  wx.Image("%sservice_accepted_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_islemler_services = wx.Image(MESSAGES.servis_ekrani_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_islemler_services  = wx.BitmapButton(self.top_panel, id=id_b_servis_islem, bitmap=image_islemler_services,pos=(0,1),
        size = (image_islemler_services.GetWidth()+5, image_islemler_services.GetHeight()+5))
        self.btn_islemler_services.Bind(wx.EVT_BUTTON, self.OnIslemlerMenuPressed)
        self.btn_islemler_services.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.servis, MESSAGES.islemleri ))
        self.btn_islemler_services.SetBitmapSelected(self.islemler_services_pressed)
        self.top_sizer.Add(self.btn_islemler_services,  (1,1),(1,1), wx.ALL)
        '''
        self.islemler_irs_islemleri_pressed  =  wx.Image("%sfis_accepted_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_islemler_irs_islemleri = wx.Image("%sfis_yazili_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_islemler_irs_islemleri  = wx.BitmapButton(self.top_panel, id=id_b_irsaliye_islem, bitmap=image_islemler_irs_islemleri,pos=(0,1),
        size = (image_islemler_irs_islemleri.GetWidth()+5, image_islemler_irs_islemleri.GetHeight()+5))
        #self.btn_islemler_irs_islemleri.Bind(wx.EVT_BUTTON, self.OnIslemlerMenuPressed)
        self.btn_islemler_irs_islemleri.SetToolTipString("�rsaliye ��lemleri")
        self.btn_islemler_irs_islemleri.SetBitmapSelected(self.islemler_irs_islemleri_pressed)
        self.top_sizer.Add(self.btn_islemler_irs_islemleri,  (1,2),(1,1), wx.ALL)     
        '''
        
        self.islemler_web_pressed  =  wx.Image("%sweb_accepted_256.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_islemler_web = wx.Image(MESSAGES.web_ekrani_%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_islemler_web  = wx.BitmapButton(self.top_panel, id=id_b_web_uygulamasi, bitmap=image_islemler_web,pos=(0,1),
        size = (image_islemler_web.GetWidth()+5, image_islemler_web.GetHeight()+5))
        self.btn_islemler_web.Bind(wx.EVT_BUTTON, self.OnIslemlerMenuPressed)
        self.btn_islemler_web.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.internet, MESSAGES.islemleri ))
        self.btn_islemler_web.SetBitmapSelected(self.islemler_web_pressed)
        self.top_sizer.Add(self.btn_islemler_web,  (1,0),(1,1), wx.ALL)
        if not self.thread_params.WS_AKTIF:
            self.btn_islemler_web.Disable()
        
        
        self.middle_panel =  wx.Panel(self, wx.ID_ANY ,size=(winSize[0],winSize[1]-140))
        middle_sizer=wx.FlexGridSizer(cols=1,hgap=10,vgap=10)
        self.middle_panel.SetSizer(middle_sizer)
        self.middle_panel.SetBackgroundColour( wx.Colour(254,254,254))
        middle_panel_image=wx.Image(MESSAGES.yazilim_image_%image_path,wx.BITMAP_TYPE_ANY)
        sb1 = wx.StaticBitmap(self.middle_panel, -1, wx.BitmapFromImage(middle_panel_image))
        
        middle_sizer.Add(sb1)      

        mainSizer.Add( self.top_panel,(0,0),(1,1),wx.ALL)
        mainSizer.Add( self.middle_panel,(1,0),(1,1),wx.ALL|wx.BOTTOM)
        self.Thaw()
    def OnAbout(self, event):
        self.SetTransparent(150)
        dlg = wx.MessageDialog( self, " ",'', wx.OK)
        dlg.ShowModal() 
        dlg.Destroy()
        self.SetTransparent(255)
    def OnExit(self,e):
        self.onay_message(MESSAGES.cik_onay,MESSAGES.kapat)
    def OnCloseWindow(self,e):
        self.onay_message(MESSAGES.cik_onay,MESSAGES.kapat)
    def onSelect(self, event):
        """"""
        #print dir( event )
        #print "You selected: " + self.isin_adi_cmb.GetStringSelection()
        obj = self.isin_adi_cmb.GetClientData(self.isin_adi_cmb.GetSelection())
        text = """
        The object's attributes are:
        %s  %s
 
        """ % (obj.id, obj.ad)
        #print text
    def onay_message( self,message,win_name):
        self.SetTransparent(150)
        dlg = wx.MessageDialog(self, message,
                               win_name,
                               wx.OK  |wx.CANCEL | wx.ICON_INFORMATION
                               )
        
        retCode = dlg.ShowModal()
        if (retCode == wx.ID_OK):
            self.Show( False )
        dlg.Destroy()
        self.SetTransparent(255)      
        
    def OnIslemlerMenuPressed( self , event ):
       
        id_  = event.GetEventObject().GetId()
        
        if id_ == id_b_iskonto_tanim:
            winClass = win_Iskonto_Tanim
            title    = MESSAGES.sablon_iki_kelime%(MESSAGES.iskonto, MESSAGES.tanimlari )
        elif id_ == id_b_cari_kartlar:
            winClass = win_Cari_Kartlar
            title    = MESSAGES.sablon_iki_kelime%(MESSAGES.cari, MESSAGES.kartlar )
        
        elif id_   == id_b_servis_islem:
            winClass = win_Servis_Islem
            title    = MESSAGES.sablon_iki_kelime%(MESSAGES.servis, MESSAGES.islemleri )        
        
        elif id_   == id_b_kalibrasyon_islem:
            winClass = win_Kalibrasyon_Islem
            title    = MESSAGES.sablon_iki_kelime%(MESSAGES.kalibrasyon, MESSAGES.islemleri )              
        elif id_   == id_b_web_uygulamasi:
            winClass = win_Web_View
            title    = MESSAGES.sablon_iki_kelime%(MESSAGES.internet, MESSAGES.islemleri )    
        '''
        elif id_ == id_b_kart_tanimlama:
            winClass = win_Kart_Tanimlama
            title    = 'Kart Tan�mlamalar'
        elif id_ == id_b_kart_listesi:
            winClass = win_Kart_Listesi
            title    = 'Kart Listesi'
        elif id_   == id_b_veresiye_islem:
            winClass = win_Veresiye_Islem
            title    = 'Veresiye ��lemleri'
        elif id_ == id_b_irsaliye_islem:
            winClass = win_Irsaliye_Islem
            title    = '�rsaliye ��lemleri'
        '''    
        self.O.newWindow(self,event,winClass,title,True)      
class win_Iskonto_Tanim( wx.Frame ):
    
    def __init__(self, parent, id_, title):
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR ))
        self.root = param.get_root( self )
        self.db = self.root.DB
        self.O  = self.root.O
        self.cancel = False
        self.LayoutItems()
    
    def LayoutItems(self):
        self.Freeze()
        self.scroll = wx.ScrolledWindow(self, 1, style=wx.RAISED_BORDER)
        self.scroll.SetScrollbars(1,1,600,400)
        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.scroll.SetSizer(self.mainSizer)
        self.book  = aui.AuiNotebook(self.scroll,1,agwStyle=(aui.AUI_NB_TOP|aui.AUI_NB_SCROLL_BUTTONS))
        self.book.SetArtProvider(aui.AuiDefaultTabArt())
        style.setPanelListBg( self.book )
        self.mainSizer.Add(self.book, 1,wx.ALL | wx.EXPAND)
        
        self.p  = self.createIskonto()
        style.setPanelListBg( self.p )
        self.p2 = self.createIskontoYakitEkleme()
        style.setPanelListBg( self.p2 )
        self.p3 = self.createIskontoListe()
        style.setPanelListBg( self.p3 )
        
        
        self.book.AddPage(self.p,  MESSAGES.sablon_iki_kelime%(MESSAGES.iskonto, MESSAGES.tanimlari ),True)
        self.book.AddPage(self.p2, MESSAGES.iskontoya_yakit_ekle )
        self.book.AddPage(self.p3, MESSAGES.sablon_iki_kelime%(MESSAGES.iskonto, MESSAGES.listesi ))
        
        self.book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED, self.OnBookPageChanged )
        
        il = wx.ImageList(64, 64)
        self.img2 = il.Add(wx.Bitmap('%siskontoya_yakit_ekle_2_64.png'%image_path,wx.BITMAP_TYPE_ANY))
        self.img1= il.Add(wx.Bitmap('%siskonto_list_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img0 = il.Add(wx.Bitmap('%siskonto_ekle_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        
        
        self.book.AssignImageList(il)
 
        self.book.SetPageImage(0, self.img0)
        self.book.SetPageImage(1, self.img2)
        self.book.SetPageImage(2, self.img1)
        
        
        self.Thaw()
        
    def OnBookPageChanged( self, event ):
        if self.book.GetSelection() == 0:
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.Freeze()
            self.book.SetSelection(1)
            p = self.createIskonto()
            self.book.RemovePage(0)
            self.book.InsertPage(0, p,MESSAGES.sablon_iki_kelime%(MESSAGES.iskonto, MESSAGES.tanimlari ))
            self.book.SetPageImage(0, self.img0)
            self.book.SetSelection(0)
            self.book.Bind(  aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.Thaw()
        
        elif self.book.GetSelection() == 1:
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.Freeze()
            self.book.SetSelection(0)
            p = self.createIskontoYakitEkleme()
            self.book.RemovePage(1)
            self.book.InsertPage(1, p,MESSAGES.iskontoya_yakit_ekle)
            self.book.SetPageImage(1, self.img2)
            self.book.SetSelection(1)
            self.book.Bind(  aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.Thaw()
        
        elif self.book.GetSelection() == 2:
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.Freeze()
            self.book.SetSelection(0)
            p = self.createIskontoListe()
            self.book.RemovePage(2)
            self.book.InsertPage(2, p,MESSAGES.sablon_iki_kelime%(MESSAGES.iskonto, MESSAGES.listesi ))
            self.book.SetPageImage(2, self.img1)
            self.book.SetSelection(2)
            self.book.Bind(  aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.Thaw()
            
        
            
    def createIskonto ( self ):
        
        p = wx.Panel(self.book,-1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        
        gbs = self.gbs = wx.GridBagSizer(10, 10)

        lbl_iskonto_adi   = wx.StaticText(p, label=MESSAGES.sablon_iki_kelime_dot%(MESSAGES.iskonto, MESSAGES.adi ))
        style.setStaticTextList( lbl_iskonto_adi )
        self.txt_Iskonto_adi      = wx.TextCtrl  (p, -1, "",size=(180,-1))

        self.btn_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        
        self.btn_save  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%(MESSAGES.iskonto, MESSAGES.kaydet ))
        self.btn_save.Bind(wx.EVT_BUTTON, self.OnIskontoSave)
        self.btn_save.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.iskonto, MESSAGES.kaydet ))
        self.btn_save.SetBitmapSelected(self.btn_save_pressed)
        style.setButtonFont( self.btn_save )
        line_ =   wx.StaticLine(p, -1, style=wx.LI_HORIZONTAL)
        
        gbs.Add( lbl_iskonto_adi, (1,1),(1,1),  wx.ALL,3)
        gbs.Add( self.txt_Iskonto_adi, (1,2) ,(1,2),  wx.ALL,3)
       
        gbs.Add(self.btn_save,  (2,2),(1,3),wx.ALL,3)        
        gbs.Add( line_, (3,0 ),(1,5),flag=wx.EXPAND|wx.BOTTOM, border=10)
        
        p.SetSizerAndFit(gbs)
        p.SetAutoLayout(True)    
        return p
    
    def createIskontoYakitEkleme ( self ):
        
        self.panel = wx.Panel(self.book,1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        
        self.gbs_yakit_ekrani =  wx.GridBagSizer(10, 10)

        lbl_iskonto_adi   = wx.StaticText(self.panel, label=MESSAGES.sablon_iki_kelime_dot%(MESSAGES.iskonto, MESSAGES.adi ))
        style.setStaticTextList( lbl_iskonto_adi )
        self.cmb_iskonto_adi = wx.ComboBox(self.panel,1,"", choices=[],style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY))#bind data veri tabanindan gelecek...
        self.cmb_iskonto_adi.Bind( wx.EVT_COMBOBOX , self.OnIskontoAdiChanged )
        iskonto_adi_parametric_objects = []
        iskonto_adi_parametric_objects = self.db.get_parameters_all("ISKONTO",order="ISKONTO_ADI",selects="ID_ISKONTO,ISKONTO_ADI",where='ID_ISKONTO_WEB is null')
        if iskonto_adi_parametric_objects != []:
            self.db.widgetMaker( self.cmb_iskonto_adi , iskonto_adi_parametric_objects)
        lbl_yakit_turu = wx.StaticText(self.panel,label=MESSAGES.sablon_iki_kelime_dot%(MESSAGES.yakit, MESSAGES.turu ))
        style.setStaticTextList( lbl_yakit_turu )
        self.cmb_yakit_turu = wx.ComboBox(self.panel,1,"", choices=[],style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY))#bind data veri tabanindan gelecek...
        self.cmb_yakit_turu.Bind( wx.EVT_COMBOBOX , self.OnYakitTuruChanged )  
        
        yakit_turu_parametric_objects = []
        yakit_turu_parametric_objects = self.db.get_parameters_all("YAKIT",where="YAKIT_DURUM=1",order="ID_YAKIT",selects="ID_YAKIT,YAKIT_ADI,YAKIT_BIRIM_FIYAT")

        if yakit_turu_parametric_objects != []:
            self.db.widgetMaker( self.cmb_yakit_turu , yakit_turu_parametric_objects)

        lbl_uygulama_turu = wx.StaticText(self.panel,label=MESSAGES.sablon_iki_kelime_dot%(MESSAGES.uygulama, MESSAGES.turu ))
        style.setStaticTextList( lbl_uygulama_turu )
        self.cmb_uygulama_turu = wx.ComboBox(self.panel,1,"", choices=[],style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY))#bind data veri tabanindan gelecek...
        #db.widgetMaker( self.cmb_uygulama_turu , dizi_uyg_turu)
        self.cmb_uygulama_turu.Bind( wx.EVT_COMBOBOX , self.OnUygulamaTuruChanged )
        uyg_turu_parametric_objects = []
        uyg_turu_parametric_objects = self.db.get_parameters_all("P_ISKONTO_UYG_TURU",order="UYG_TURU_ADI",selects="ID_UYG_TURU,UYG_TURU_ADI")

        if uyg_turu_parametric_objects != []:
            self.db.widgetMaker( self.cmb_uygulama_turu , uyg_turu_parametric_objects)

        lbl_uygulama_cinsi = wx.StaticText(self.panel,1,MESSAGES.sablon_iki_kelime_dot%(MESSAGES.uygulama, MESSAGES.cinsi ))
        style.setStaticTextList( lbl_uygulama_cinsi )
        self.cmb_uygulama_cinsi = wx.ComboBox(self.panel,1,"", choices=[],style=(wx.CB_DROPDOWN|wx.EXPAND |wx.CB_READONLY))#bind data veri tabanindan gelecek...
        self.cmb_uygulama_cinsi.Bind( wx.EVT_COMBOBOX , self.OnUygulamaCinsiChanged )
        #db.widgetMaker( self.cmb_uygulama_cinsi , dizi_uyg_cinsi)
        uyg_cinsi_parametric_objects = []
        uyg_cinsi_parametric_objects = self.db.get_parameters_all("P_ISKONTO_UYG_CINSI",order="UYG_CINSI_ADI",selects="ID_UYG_CINSI,UYG_CINSI_ADI")

        if uyg_cinsi_parametric_objects != []:
            self.db.widgetMaker( self.cmb_uygulama_cinsi , uyg_cinsi_parametric_objects)
               
        lbl_oran_miktar            = wx.StaticText(self.panel, label=MESSAGES.sablon_ayrac_dot%(MESSAGES.oran,MESSAGES.miktar))
        self.txt_Oran_miktar       = wx.TextCtrl  (self.panel, 1, "",size=(180,-1))
        self.txt_Oran_miktar.Bind( wx.EVT_CHAR, self.OnMiktarBindWithoutCins)
        self.txt_Oran_miktar_uyari = wx.StaticText(self.panel,label=MESSAGES.ornek_oran_miktar)
        style.setStaticTextList( self.txt_Oran_miktar_uyari )
        style.setUyariText(self.txt_Oran_miktar_uyari)
        style.setStaticTextList( lbl_oran_miktar )
        
        self.btn_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        
        self.btn_save  = LibButtons.ThemedGenBitmapTextButton(self.panel, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%(MESSAGES.iskonto, MESSAGES.kaydet ))
        self.btn_save.Bind(wx.EVT_BUTTON, self.OnIskontoYakitSave)
        self.btn_save.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.iskonto, MESSAGES.kaydet ))
        self.btn_save.SetBitmapSelected(self.btn_save_pressed)
        style.setButtonFont( self.btn_save )
        line_ =   wx.StaticLine(self.panel, -1, style=wx.LI_HORIZONTAL)
        
        self.gbs_yakit_ekrani.Add( lbl_iskonto_adi, (1,1),(1,1),  wx.ALL,3)
        self.gbs_yakit_ekrani.Add( self.cmb_iskonto_adi, (1,2) ,(1,2),  wx.ALL,3)
       
        self.gbs_yakit_ekrani.Add( lbl_yakit_turu , (2,1),(1,1), wx.ALL , 3)
        self.gbs_yakit_ekrani.Add( self.cmb_yakit_turu, (2,2) ,(1,2),  wx.ALL,3)
        
        self.gbs_yakit_ekrani.Add(lbl_uygulama_turu,(3,1),(1,1),wx.ALL, 3)
        self.gbs_yakit_ekrani.Add(self.cmb_uygulama_turu,(3,2),(1,2),wx.ALL, 3)
        
        self.gbs_yakit_ekrani.Add( lbl_uygulama_cinsi, (4,1),(1,1),  wx.ALL,3)
        self.gbs_yakit_ekrani.Add( self.cmb_uygulama_cinsi, (4,2) ,(1,1),  wx.ALL,3)
       
        self.gbs_yakit_ekrani.Add( lbl_oran_miktar, (5,1),(1,1),  wx.ALL,3)
        self.gbs_yakit_ekrani.Add( self.txt_Oran_miktar, (5,2) ,(1,2),  wx.ALL,3)        
        self.gbs_yakit_ekrani.Add( self.txt_Oran_miktar_uyari, (5,4) ,(1,3),  wx.ALL,3)

        
        self.gbs_yakit_ekrani.Add(self.btn_save,  (6,2),(1,1),wx.ALL,1)        
        self.gbs_yakit_ekrani.Add( line_, (7,0 ),(1,7),flag=wx.EXPAND|wx.BOTTOM, border=10)
        
        self.panel.SetSizerAndFit(self.gbs_yakit_ekrani)
        self.panel.SetAutoLayout(True)    
        return self.panel
    
    def createIskontoListe ( self ):
        
        child_parameters = ['ISKONTO_VIEW','ID_ISKONTO','ID_ISKONTO,ID_ISKONTO_YAKIT,YAKIT_ADI,UYGULAMA_TURU_ADI,UYGULAMA_CINSI_ADI,ORAN_MIKTAR']
        headers_model    = ['%s,string'%MESSAGES.sablon_iki_kelime%( MESSAGES.iskonto, MESSAGES.adi),
                            '%s,string'%MESSAGES.sablon_iki_kelime%( MESSAGES.yakit, MESSAGES.adi),
                            '%s,string'%MESSAGES.sablon_iki_kelime%( MESSAGES.uygulama, MESSAGES.turu),
                            '%s,string'%MESSAGES.sablon_iki_kelime%( MESSAGES.uygulama, MESSAGES.cinsi),
                            '%s,currency'%MESSAGES.sablon_ayrac_dot%(MESSAGES.oran,MESSAGES.miktar)]
        
        parent_childs  = self.db.get_raw_data_all(table_name = 'ISKONTO',order="ISKONTO_ADI",selects="ISKONTO_ADI,ID_ISKONTO",where='ID_ISKONTO_WEB is null',dict_mi='parent',child_parameters =child_parameters,data_types=map(lambda x:x.split(',')[1], headers_model) )
        mod            = model.TreeListModel(parent_childs,headers_model,child_parameters)
        tree_list_pane = TreeListPanel( self.book, mod )
        
        return tree_list_pane
    
    def createIskontonunChildleriniGetir ( self ):
        
        self.p2.Freeze()
        ekran = self.p2.FindWindowById(31)
        if ekran:
            ekran.Destroy()
        iskonto_id      = self.cmb_iskonto_adi.GetClientData(self.cmb_iskonto_adi.GetSelection()).id
        
        headers    =        ['ID,digit',
                            '%s,string'%MESSAGES.sablon_iki_kelime%( MESSAGES.iskonto, MESSAGES.adi),
                            '%s,string'%MESSAGES.sablon_iki_kelime%( MESSAGES.yakit, MESSAGES.adi),
                            '%s,string'%MESSAGES.sablon_iki_kelime%( MESSAGES.uygulama, MESSAGES.turu),
                            '%s,string'%MESSAGES.sablon_iki_kelime%( MESSAGES.uygulama, MESSAGES.cinsi),
                            '%s,currency'%MESSAGES.sablon_ayrac_dot%(MESSAGES.oran,MESSAGES.miktar)]
        
        
        data_history   = self.db.get_raw_data_all("ISKONTO_VIEW",where = "ID_ISKONTO="+str(iskonto_id) ,order = "ID_YAKIT",selects="ID_ISKONTO_YAKIT,ISKONTO_ADI,YAKIT_ADI,UYGULAMA_TURU_ADI,UYGULAMA_CINSI_ADI,ORAN_MIKTAR")  
        p_list = self.O.createStaticList( self.p2, headers, data_history,item_id = 31)
        p_list.SetPosition((0,300),wx.EXPAND|wx.ALL)
        p_list.SetSize((self.p2.GetSize().width -200, 300))
        self.p2.Layout()
        self.p2.Thaw()
  
    def OnIskontoAdiChanged(self,event):
        if self.cmb_iskonto_adi.GetSelection() != -1 :
            obj_iskonto_id     = self.cmb_iskonto_adi.GetClientData(self.cmb_iskonto_adi.GetSelection()).id
            aktif_mi = self.db.get_raw_data_all("ISKONTO",where="ID_ISKONTO="+str(obj_iskonto_id),selects="ID_ACTIVE")
            
            self.p2.Freeze()
            if self.p2.FindWindowById(32) :
                self.p2.FindWindowById(32).Destroy()
            if self.p2.FindWindowById(33) :
                self.p2.FindWindowById(33).Destroy()
            if aktif_mi[0][0] == 1:
                self.cb = wx.CheckBox(self.p2,32 , MESSAGES.pasif,style=wx.ALIGN_RIGHT)
            elif aktif_mi[0][0] == 2:
                self.cb = wx.CheckBox(self.p2,33, MESSAGES.aktif,style=wx.ALIGN_RIGHT)
            if self.cb :
                style.setStaticTextList(self.cb)
                self.gbs_yakit_ekrani.Add( self.cb, (1,4) ,(1,2),  wx.ALL,3)
            self.p2.Layout()
            self.p2.Thaw()
            self.createIskontonunChildleriniGetir()


    def OnYakitTuruChanged(self,event):
        
        obj_yakit_turu      = self.cmb_yakit_turu.GetClientData(self.cmb_yakit_turu.GetSelection())        
        obj_iskonto_adi     = self.cmb_iskonto_adi.GetSelection()

        if obj_iskonto_adi == -1 :
            #obj_yakit_turu = -1
            self.cmb_yakit_turu.SetSelection(-1)
            style.uyari_message(self,MESSAGES.once_iskonto_sec,MESSAGES.sablon_iki_kelime%( MESSAGES.iskonto, MESSAGES.tanimlari))
        self.btn_save.SetToolTipString(str(self.cmb_iskonto_adi.GetValue())+MESSAGES.iskonto + str(self.cmb_yakit_turu.GetValue())+ '%s %s %s '%(MESSAGES.yakit , MESSAGES.turu , MESSAGES.kaydet))
    
    def OnUygulamaCinsiChanged( self , event ):
        
        obj = dataObj()
        obj_uyg_turu = dataObj()
        obj = self.cmb_uygulama_cinsi.GetClientData(self.cmb_uygulama_cinsi.GetSelection())
        obj_uyg_turu = self.cmb_uygulama_turu.GetSelection()
        
        if self.cmb_uygulama_turu.GetSelection() == -1 :
            self.cmb_uygulama_cinsi.SetSelection(-1)         
            style.uyari_message(self,MESSAGES.once_uygulama_turu_sec,MESSAGES.sablon_iki_kelime%( MESSAGES.iskonto, MESSAGES.tanimlari))            
        if obj.id == 1:
            self.txt_Oran_miktar.Unbind( wx.EVT_CHAR)
            self.txt_Oran_miktar.Bind( wx.EVT_CHAR, lambda event: style.CheckNumberMinMax(event, (0,5)))
        elif obj.id == 2:
            self.txt_Oran_miktar.Unbind( wx.EVT_CHAR)
            self.txt_Oran_miktar.Bind( wx.EVT_CHAR, lambda event: style.CheckNumberMinMax(event, (0,70)))
        else:
            self.txt_Oran_miktar.Unbind( wx.EVT_CHAR)
            self.txt_Oran_miktar.Bind( wx.EVT_CHAR,self.OnMiktarBindWithoutCins)
            
    def OnUygulamaTuruChanged(self,event):
        
        obj_uyg_turu = self.cmb_uygulama_turu.GetClientData(self.cmb_uygulama_turu.GetSelection()).id        
        obj_yakit_turu = self.cmb_yakit_turu.GetSelection()
        if obj_yakit_turu == -1:
            self.cmb_uygulama_turu.SetSelection(-1)
            style.uyari_message(self,MESSAGES.once_yakit_turu_sec,MESSAGES.sablon_iki_kelime%( MESSAGES.iskonto, MESSAGES.tanimlari))
        
    def OnMiktarBindWithoutCins( self, event ):
        
        self.txt_Oran_miktar.SetValue("")
        style.uyari_message(self,MESSAGES.once_uygulama_cinsi_sec,MESSAGES.sablon_iki_kelime%( MESSAGES.iskonto, MESSAGES.tanimlari))
    
    def OnIskontoSave ( self, event ) :
        iskonto_adi = self.txt_Iskonto_adi.GetValue()
        if iskonto_adi != "":
            column_name = "ISKONTO_ADI"            
            table_name = "ISKONTO"
            ##ilk once eklenmek istenen iskonto var mi check et
            iskonto_adi = iskonto_adi.strip()     
            self.txt_Iskonto_adi.SetValue(iskonto_adi)      
            value       = "'" + iskonto_adi + "'"
            where   = "UPPER (ISKONTO_ADI) like UPPER('" + iskonto_adi+"')"
            order   = "ISKONTO_ADI"
            selects = "ID_ISKONTO,ISKONTO_ADI"
            var = self.db.get_raw_data_all( table_name,where,order,selects )
            if var == []:
                ##veritabaninda yokmus o zaman veritabanina kaydedelim
                data_id = self.db.save_data_and_get_id ( table_name ,column_name, value )
                if data_id !='' :                    
                    style.uyari_message(self,MESSAGES.sablon_iki_kelime%( MESSAGES.iskonto, MESSAGES.kaydedildi),MESSAGES.sablon_iki_kelime%( MESSAGES.iskonto, MESSAGES.tanimlari))    
                    ##uyaridan sonra iskontaya eklenecek yakit turleri ekleme ekranina gecelim
                    self.p2.Freeze()
                    self.cmb_iskonto_adi.Destroy()
                    try:
                        self.cb.Destroy()
                    except:
                        pass
                    self.cmb_iskonto_adi = wx.ComboBox(self.panel,1,"", choices=[],style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY))#bind data veri tabanindan gelecek...
                    self.cmb_iskonto_adi.Bind( wx.EVT_COMBOBOX , self.OnIskontoAdiChanged )
                    iskonto_adi_parametric_objects = []
                    iskonto_adi_parametric_objects = self.db.get_parameters_all("ISKONTO",order="ISKONTO_ADI",selects="ID_ISKONTO,ISKONTO_ADI")
              
                    if iskonto_adi_parametric_objects != []:
                        #print len(iskonto_adi_parametric_objects)
                        self.db.widgetMaker( self.cmb_iskonto_adi , iskonto_adi_parametric_objects)
                    self.cb = wx.CheckBox(self.p2,32 , MESSAGES.pasif,style=wx.ALIGN_RIGHT)
                    self.gbs_yakit_ekrani.Add( self.cmb_iskonto_adi, (1,2) ,(1,2),  wx.ALL,3)
                    self.gbs_yakit_ekrani.Add( self.cb, (1,4) ,(1,2),  wx.ALL,3)
                    self.book.SetSelection(1)
                    #self.cmb_iskonto_adi.SetSelection(len(iskonto_adi_parametric_objects)-1)
                    index_line = self.cmb_iskonto_adi.FindString(iskonto_adi)
                    self.cmb_iskonto_adi.SetSelection(index_line)
                    self.cmb_yakit_turu.SetSelection(-1)
                    self.cmb_uygulama_cinsi.SetSelection(-1)
                    self.cmb_uygulama_turu.SetSelection(-1)
                    self.txt_Oran_miktar.SetValue('')
                    try:
                        self.p2.FindWindowById(31).Destroy()
                    except:
                        pass
                    self.p2.Layout()
                    self.p2.Thaw()
                   
            else :
                style.uyari_message(self,MESSAGES.sablon_iki_kelime%( MESSAGES.iskonto, MESSAGES.zaten_var),MESSAGES.sablon_iki_kelime%( MESSAGES.iskonto, MESSAGES.tanimlari))
        
    def OnIskontoYakitSave( self, event ):
        check_aktif_pasif = False
        if self.cmb_iskonto_adi.GetSelection()!=-1:
            iskonto  = self.cmb_iskonto_adi.GetClientData(self.cmb_iskonto_adi.GetSelection())
            iskonto_id = iskonto.id
            check_aktif_pasif = self.cb.GetValue()
        if check_aktif_pasif == True :
            cb_id = self.cb.GetId()
            if cb_id == 32:
                data = self.db.update_data(table_name="ISKONTO",column_name=("ID_ACTIVE"),values=("2"),where="ID_ISKONTO="+str(iskonto_id))
            elif cb_id == 33:
                data = self.db.update_data(table_name="ISKONTO",column_name=("ID_ACTIVE"),values=("1"),where="ID_ISKONTO="+str(iskonto_id))
        oran_miktar = self.txt_Oran_miktar.GetValue()
        if oran_miktar == '' and check_aktif_pasif == False:
            style.uyari_message(self,MESSAGES.once_oran_miktar_gir,MESSAGES.sablon_iki_kelime%( MESSAGES.iskonto, MESSAGES.tanimlari))    
        column_name = ""
        values      = ""     
        if iskonto_id != "":
            column_name +="ID_ISKONTO,"
            values += "'"+str(iskonto_id)+"',"
        if self.cmb_yakit_turu.GetSelection() != -1:
            yakit_turu = self.cmb_yakit_turu.GetClientData(self.cmb_yakit_turu.GetSelection())
            yakit_turu_id = yakit_turu.id      
            column_name += "ID_YAKIT,"
            values +=str(yakit_turu_id)+","
        if self.cmb_uygulama_turu.GetSelection() != -1:
            uyg_turu    = self.cmb_uygulama_turu.GetClientData(self.cmb_uygulama_turu.GetSelection())
            uyg_turu_id = uyg_turu.id       
            column_name += "ID_UYG_TURU,"
            values += str(uyg_turu_id)+","
        if self.cmb_uygulama_cinsi.GetSelection() != -1:
            uyg_cinsi = self.cmb_uygulama_cinsi.GetClientData(self.cmb_uygulama_cinsi.GetSelection())
            uyg_cinsi_id = uyg_cinsi.id
            column_name += "ID_UYG_CINSI,"
            values += str(uyg_cinsi_id) + ","
        if oran_miktar != '':
            column_name += "ORAN_MIKTAR"
            values += str(oran_miktar)
            table_name = "ISKONTO_YAKIT"
            where = "ID_ISKONTO=%s AND ID_YAKIT=%s AND ID_ACTIVE = %s"%(iskonto_id,yakit_turu_id,1)
            order = "ID_ISKONTO_YAKIT"
            selects ="ID_ISKONTO_YAKIT,ORAN_MIKTAR"
            var = self.db.get_raw_data_all( table_name,where,order,selects )

            if var == []:
                data_ = self.db.save_data ( table_name ,column_name, values )
                if data_ == 1 :
                    style.uyari_message( self, MESSAGES.sablon_iki_kelime%(MESSAGES.iskonto,MESSAGES.kaydedildi),MESSAGES.sablon_iki_kelime%( MESSAGES.iskonto, MESSAGES.tanimlari))    
                    self.createIskontonunChildleriniGetir()
                    
            else:
                self.table_name_iskonto_yakit = table_name
                self.column_name_iskonto_yakit = "ID_UYG_TURU,ID_UYG_CINSI,ORAN_MIKTAR"
                self.values_iskonto_yakit = '%s,%s,%s'%(uyg_turu_id,uyg_cinsi_id,oran_miktar)
                self.where_iskonto_yakit="ID_ISKONTO = %s AND ID_YAKIT = %s"%(iskonto_id,yakit_turu_id)
                
                style.ok_cancel_message(self,
                                        '%s %s'%(MESSAGES.sablon_iki_kelime%(MESSAGES.iskonto,MESSAGES.zaten_var) ,MESSAGES.guncelle)
                                        ,MESSAGES.sablon_iki_kelime%( MESSAGES.iskonto, MESSAGES.tanimlari),ok_function = self.YakitTuruGuncelle,cancel_function=self.YakitTuruGuncelleme)
                
        else:
            self.cmb_iskonto_adi.SetSelection(-1)
            if self.cb:
                self.p2.Freeze()
                self.cb.Destroy()
                self.p2.Layout()
                self.p2.Thaw()
                
    def YakitTuruGuncelle (self) :
        data = self.db.update_data(self.table_name_iskonto_yakit,self.column_name_iskonto_yakit,self.values_iskonto_yakit,self.where_iskonto_yakit)
        if data == 1:
            style.uyari_message(self,MESSAGES.sablon_iki_kelime%(MESSAGES.iskonto,MESSAGES.guncellendi),MESSAGES.sablon_iki_kelime%( MESSAGES.iskonto, MESSAGES.tanimlari))
            self.createIskontonunChildleriniGetir()
    def YakitTuruGuncelleme(self):
        
        pass  
class win_Kalibrasyon_Islem( wx.Frame ):
    

    def __init__(self, parent, id_, title):
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR ))
        self.db = param.get_root( self ).DB
        self.O  = param.get_root( self ).O 
        self.LayoutItems()
        
    def LayoutItems( self ):
        
        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.mainSizer)
        self.Freeze()
        self.p      = self.KalibrasyonPanel()
    
        self.mainSizer.Add(self.p, 1,wx.ALL | wx.EXPAND)
        self.CreateKalibrasyon()
        self.Layout()
        self.SetSize((600,400))
        
        
        if self.GetSize().height > GetSystemMetrics(1) - 200:
            self.SetSize( (self.GetSize().width, GetSystemMetrics(1) - 200))
        
        if self.GetSize().width > GetSystemMetrics(0) - 200:
            self.SetSize( (GetSystemMetrics(0) - 200,self.GetSize().height )) 
        
        self.Thaw()
        
    def KalibrasyonPanel (self) :
        p = wx.Panel(self,1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE
                     | wx.DOUBLE_BORDER)
        style.setPanelListBg( p )
        gbs = self.gbs =wx.GridBagSizer(10,10)
        
        
        lbl_kalibrasyon_tank_adi   = wx.StaticText(p, label="Kalibre Edilecek Yak�t Tank�n� Se�iniz :  ")
        style.setStaticTextList( lbl_kalibrasyon_tank_adi )
        self.cmb_kalibrasyon_tank_adi = wx.ComboBox(p,1,"", choices=["Tank 1","Tank 2","Tank 3"],style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY))#bind data veri tabanindan gelecek...
        #self.cmb_iskonto_adi.Bind( wx.EVT_COMBOBOX , self.OnIskontoAdiChanged )
        '''
        kalibrasyon_tank_adi_parametric_objects = []
        kalibrasyon_tank_adi_parametric_objects = db.get_parameters_all("TANK",order="TANK_ADI",selects="ID_TANK,TANK_ADI")
  
        if kalibrasyon_tank_adi_parametric_objects != []:
            db.widgetMaker( self.cmb_kalibrasyon_tank_adi , kalibrasyon_tank_adi_parametric_objects)
        '''
        lbl_kaibrasyon_yakit_mik   = wx.StaticText(p, label="Tanktaki Yak�t Miktar� :  ")# giri� bi�imi saat �eklinde olacak
        style.setStaticTextList( lbl_kaibrasyon_yakit_mik )
        self.txt_kalibrasyon_yakit_mik = wx.TextCtrl  (p, -1, "",size=(180,-1))
        
        self.btn_kalibrasyon_start_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_kalibrasyon_start  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label="Kalibrasyonu Ba�lat")
        self.btn_kalibrasyon_start.Bind(wx.EVT_BUTTON, self.OnKalibrasyonStart)
        self.btn_kalibrasyon_start.SetToolTipString("Otomatik Tank Kalibrasyonunu Ba�lat")
        self.btn_kalibrasyon_start.SetBitmapSelected(self.btn_kalibrasyon_start_pressed)
        style.setButtonFont( self.btn_kalibrasyon_start )
        self.btn_kalibrasyon_finish_pressed  =  wx.Image("%sdelete_green.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_delete = wx.Image("%sdelete.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_kalibrasyon_finish  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_delete,label="Kalibrasyonu Bitir" , style=wx.ALIGN_LEFT)
        self.btn_kalibrasyon_finish.Bind(wx.EVT_BUTTON, self.OnKalibrasyonFinish)
        self.btn_kalibrasyon_finish.SetToolTipString("Otomatik Tank Kalibrasyonunu Bitir")
        self.btn_kalibrasyon_finish.SetBitmapSelected(self.btn_kalibrasyon_finish_pressed)
        style.setButtonFont( self.btn_kalibrasyon_finish )
        line_                           =   wx.StaticLine(p, -1, style=wx.LI_HORIZONTAL)
         
        gbs.Add( lbl_kalibrasyon_tank_adi, (1,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.cmb_kalibrasyon_tank_adi, (1,2) ,(1,1),  wx.EXPAND,3)
        
        gbs.Add( lbl_kaibrasyon_yakit_mik, (2,1),(1,1),  wx.EXPAND ,3)
        gbs.Add( self.txt_kalibrasyon_yakit_mik, (2,2) ,(1,1),  wx.EXPAND,3)
  
        gbs.Add( line_,  (3,0 ),(1,5),flag=wx.EXPAND|wx.BOTTOM, border=10)
         
        gbs.Add( self.btn_kalibrasyon_start, (4,1) ,(1,1),  wx.EXPAND,3)
        gbs.Add( self.btn_kalibrasyon_finish, (4,2) ,(1,2),  wx.EXPAND,3)
        #gbs.Add( self.btn_bin, (6,4) ,(1,6),  wx.EXPAND,3)
        
        p.SetSizerAndFit(gbs)
        p.SetAutoLayout(True)
        style.setPanelListBg( p )
        return p
        
    def CreateKalibrasyon( self ):
        pass
        '''
        self.data_all       = db.get_raw_data_all(table_name = 'VARDIYA',where="ID_ACTIVE=1",selects="ID_VARDIYA,VARDIYA_ADI,BASLANGIC_SAATI,BITIS_SAATI" )
        headers = ['Se�,digit','Vardiya Ad�,string','Ba�lang�� Saati,time','Biti� Saati,time']
        
        self.liste = O.createStaticList(self, headers, self.data_all)
        self.liste.SetPosition((0,0))
        self.mainSizer.Add(self.liste,1,wx.ALL | wx.EXPAND)
        self.Layout()
        '''
    def OnKalibrasyonStart ( self ) :
        
        pass
    
    def OnKalibrasyonFinish ( self ) :
        
        pass
class win_Cari_Kartlar(wx.Frame):
    def __init__(self, parent, id_, title):
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR ))
        self.styl=style
        self.root = param.get_root( self )
        self.db   = self.root.DB
        self.O    = self.root.O
        self.selected_item = None
        self.selected_kart = None
        self.dummy = sabitler.dummy()
        self.LayoutItems()
    def LayoutItems(self):
        self.Freeze()
        favicon = wx.Icon('%sserial_port.png'%image_path, wx.BITMAP_TYPE_ANY)
        self.SetIcon(favicon)
        self.check_list = []
        mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(mainSizer)
        #self.book = wx.Notebook(self, style=wx.NB_TOP)
        self.book  = aui.AuiNotebook(self,1,agwStyle=(aui.AUI_NB_TOP|aui.AUI_NB_SCROLL_BUTTONS))
        self.book.SetArtProvider(aui.AuiDefaultTabArt())
        mainSizer.Add(self.book, 1,wx.ALL | wx.EXPAND)
        
        self.p = self.createCari_Kart_Ekle()
        style.setPanelListBg(self.p)
        self.p2 = self.CreateCariListe()
        style.setPanelListBg(self.p2)
        self.p3 = self.CreatePersonelEkle()
        style.setPanelListBg(self.p3)
        self.p6 = self.CreatePersonelListe()
        style.setPanelListBg(self.p6)
        self.p4=self.createKartTanimla()
        style.setPanelListBg(self.p4)
        self.p5=self.createKartListesi()
        style.setPanelListBg(self.p5)
        self.p7 = self.CreateAracEkle()
        style.setPanelListBg( self.p7)
        self.p8 = self.CreateAracListe()
        style.setPanelListBg( self.p8 )
        
        self.book.AddPage( self.p,MESSAGES.sablon_iki_kelime%(MESSAGES.sirket, MESSAGES.ekle),True )
        self.book.AddPage( self.p2,MESSAGES.sablon_iki_kelime%(MESSAGES.sirket, MESSAGES.listesi) )
        self.book.AddPage( self.p7,MESSAGES.sablon_iki_kelime%(MESSAGES.arac, MESSAGES.ekle))
        self.book.AddPage( self.p8,MESSAGES.sablon_iki_kelime%(MESSAGES.arac, MESSAGES.listesi))
        self.book.AddPage( self.p3,MESSAGES.sablon_iki_kelime%(MESSAGES.personel, MESSAGES.ekle)  )
        self.book.AddPage( self.p6,MESSAGES.sablon_iki_kelime%(MESSAGES.personel, MESSAGES.listesi)  )
        self.book.AddPage( self.p4, MESSAGES.sablon_iki_kelime%(MESSAGES.kart, MESSAGES.ekle ))
        self.book.AddPage( self.p5, MESSAGES.sablon_iki_kelime%(MESSAGES.kart, MESSAGES.listesi ))
        
        il = wx.ImageList(64, 64)
        self.img0 = il.Add(wx.Bitmap('%simage_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        
        self.img1 = il.Add(wx.Bitmap('%simage_search_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img3 = il.Add(wx.Bitmap('%scard_add_2_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img4 = il.Add(wx.Bitmap('%scard_list_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img2 = il.Add(wx.Bitmap('%sbusiness_user.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img6 = il.Add(wx.Bitmap('%sbusiness_user_list.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img7 = il.Add(wx.Bitmap('%stasit_tanima_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img8 = il.Add(wx.Bitmap('%stasit_tanima_64_list.png'%image_path, wx.BITMAP_TYPE_ANY))
        
        self.book.AssignImageList(il)
        self.book.Bind(aui.EVT_AUINOTEBOOK_PAGE_CHANGED, self.OnBookPageChanged)
 
        self.book.SetPageImage(0, self.img0)
        self.book.SetPageImage(1, self.img1)
        self.book.SetPageImage(2, self.img7)
        self.book.SetPageImage(3, self.img8)
        self.book.SetPageImage(4, self.img2)
        self.book.SetPageImage(5, self.img6)
        self.book.SetPageImage(6, self.img3)
        self.book.SetPageImage(7, self.img4)
        
        self.Thaw()
        self.Show()
    def CreatePersonelEkle(self):
        
        selected_item             = self.selected_item
        self.dummy.CARI_ADI       = ''
        self.dummy.YETKILI_ADI    = ''
        self.dummy.ADRES_1        = ''
        self.dummy.TEL            = ''
        self.data_cari_hesap_personel      = self.dummy
        
        if selected_item :
            #data_probe = self.db.get_raw_data_all("CARI_LISTE_VIEW",selects="ID_PROBE,PROBE_ADRES,PROBE_MARKA,PROBE_UZUNLUK,SPEED_OF_WIRE",where="ID_PROBE=%s"%selected_item,dict_mi='object')[0]
            self.data_cari_hesap_personel = self.db.get_raw_data_all("CARI_LISTE_VIEW",selects="ID_CARI_HESAP,CARI_ADI,YETKILI_ADI,ADRES_1,TEL",where='ID_CARI_HESAP=%s'%selected_item,dict_mi='object')[0]  
        
        p = wx.Panel(self.book,1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        
        gbs = wx.GridBagSizer(10, 10)
        
        personel_adi_lbl   = wx.StaticText(p, label= MESSAGES.sablon_iki_kelime_dot%( MESSAGES.personel, MESSAGES.adi))
        self.personel_adi_txt      = wx.TextCtrl  (p, -1, self.data_cari_hesap_personel.CARI_ADI,size=(180,-1))
        self.styl.setStaticTextList( personel_adi_lbl,True)
        
        personel_Yetkili_Adi_lbl   = wx.StaticText(p, label=MESSAGES.sablon_iki_kelime%( MESSAGES.yetkili, MESSAGES.adi))
        self.personel_Yetkili_Adi_txt      = wx.TextCtrl  (p, -1, self.data_cari_hesap_personel.YETKILI_ADI,size=(180,-1))
        self.styl.setStaticTextList( personel_Yetkili_Adi_lbl,True)
        

        personel_Adres_lbl   = wx.StaticText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.personel, MESSAGES.adres))
        self.personel_Adres_txt      = wx.TextCtrl  (p, -1, self.data_cari_hesap_personel.ADRES_1,size=(280,-1),style= wx.TE_MULTILINE | wx.SUNKEN_BORDER)
        self.styl.setStaticTextList( personel_Adres_lbl)
        
        personel_Tel_lbl   = wx.StaticText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.personel, MESSAGES.telefon))
        #self.personel_Tel_txt      = wx.TextCtrl  (p, -1, "",size=(180,-1))
        self.personel_Tel_txt = self.styl.CheckTelFaxMasked ( p, 'personel_tel',11,self.data_cari_hesap_personel.TEL)
        self.styl.setStaticTextList( personel_Tel_lbl)
        
        uyari_lbl    = wx.StaticText(p, label=MESSAGES.zorunlu)
        self.ZorunluAlan(uyari_lbl)

        self.btn_personel_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_personel_save  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%( MESSAGES.personel, MESSAGES.kaydet))
        self.btn_personel_save.Bind(wx.EVT_BUTTON,lambda event: self.OnPersonelKaydet(event,selected_item))
        self.btn_personel_save.SetToolTipString(MESSAGES.sablon_iki_kelime%( MESSAGES.personel, MESSAGES.kaydet))
        self.btn_personel_save.SetBitmapSelected(self.btn_personel_save_pressed)
        style.setButtonFont( self.btn_personel_save)
        
        gbs.Add( personel_adi_lbl, (1,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.personel_adi_txt, (1,2) ,(1,1),  wx.EXPAND,3)
        
        gbs.Add( personel_Yetkili_Adi_lbl, (2,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.personel_Yetkili_Adi_txt, (2,2) ,(1,1),  wx.EXPAND,3)

        gbs.Add( personel_Adres_lbl, (3,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.personel_Adres_txt, (3,2) ,(1,1),  wx.EXPAND,3)
        
        gbs.Add( personel_Tel_lbl, (4,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.personel_Tel_txt, (4,2) ,(1,1),  wx.EXPAND,3)

    
        gbs.Add(self.btn_personel_save,  (6,2),(1,1),wx.ALL,1)        
        
        gbs.Add( uyari_lbl, (8,1),(1,3),  wx.ALL,3)
        
        
        p.SetSizerAndFit(gbs)
        p.SetAutoLayout(True)
        return p
    def CreatePersonelListe(self):
           
        headers        = ['ID,digit',
                          '%s,string'%MESSAGES.sablon_iki_kelime%(MESSAGES.cari, MESSAGES.adi),
                          '%s,string'%MESSAGES.sablon_iki_kelime%(MESSAGES.yetkili,MESSAGES.adi),
                          '%s,string'%MESSAGES.adres,
                          '%s,string'%MESSAGES.telefon
                          ]
        data_history   = self.db.get_raw_data_all("CARI_LISTE_VIEW" ,order = "ID_CARI_HESAP",selects="ID_CARI_HESAP,CARI_ADI,YETKILI_ADI,ADRES_1,TEL",where ='ID_CARI_TIPI=%s AND ID_CARI_HESAP != 1' % sabitler.CARI_TIPLER.PERSONEL)  
        p_list = IndexListPanel( self.book, headers=headers, data = data_history,table_name='CARI_HESAPLAR',where='ID_CARI_HESAP',delete_button=True,guncelle_button=True,guncellemede_gidilecek_sayfa=4,pasif_et=True,update_columns='ID_ACTIVE',update_values=2)
      
        return p_list
    def CreateAracEkle (self):
        
        selected_item           = self.selected_item
        self.dummy.ID_CARI_HESAP= ''
        self.dummy.DEPARTMAN    = ''
        self.dummy.PLAKA        = ''
        self.dummy.MARKA        = ''
        self.dummy.MODEL        = ''
        self.dummy.YILI         = ''
        self.dummy.ID_YAKIT     = ''
        self.dummy.ID_KIT_TIPI  = ''

        data_arac               = self.dummy
        p = wx.Panel(self.book,1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        
        gbs = wx.GridBagSizer(10, 10)
        if selected_item :
            data_arac = self.db.get_raw_data_all("ARACLAR",selects="ID_ARAC,ID_CARI_HESAP,DEPARTMAN,PLAKA,MARKA,MODEL,YILI,ID_YAKIT,ID_KIT_TIPI,ID_ACTIVE,ID_CARI_HESAP_WEB",where='ID_ARAC=%s'%selected_item,dict_mi='object')[0]  
        
        cari_listesi_parametric_objects = []
        cari_listesi_parametric_objects = self.db.get_parameters_all_with_id(table_name="CARI_LISTE_VIEW",order="CARI_ADI",selects = "ID_CARI_HESAP,CARI_ADI,CARI_TIP_ADI",where='ID_CARI_TIPI=%s AND ID_ACTIVE=%s'%(sabitler.CARI_TIPLER.MUSTERI,1))
        cari_list_lbl              = wx.StaticText(p, label= '%s :'%MESSAGES.sirket)
        self.cari_list_arac_cmb    = wx.ComboBox(p,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        style.setStaticTextList( cari_list_lbl, True)
        self.db.widgetMaker( self.cari_list_arac_cmb , cari_listesi_parametric_objects)
        
        if data_arac.ID_CARI_HESAP == '':
            arac_cari_hesap_index = -1
        else:
            arac_cari_hesap_index = cari_listesi_parametric_objects.index(filter(lambda x:  x.id==data_arac.ID_CARI_HESAP,cari_listesi_parametric_objects)[0])
        
        self.cari_list_arac_cmb.SetSelection(arac_cari_hesap_index)

        departman_lbl   = wx.StaticText(p, label= MESSAGES.departman)
        self.departman_txt      = wx.TextCtrl  (p, -1, data_arac.DEPARTMAN,size=(180,-1))
        self.styl.setStaticTextList( departman_lbl,False)
        
        plaka_lbl   = wx.StaticText(p, label="%s :"%MESSAGES.plaka)
        self.plaka_arac_txt      = wx.TextCtrl  (p, -1, data_arac.PLAKA,size=(180,-1))
        style.setStaticTextList( plaka_lbl,True)
        
        marka_lbl           = wx.StaticText(p, label="%s :"%MESSAGES.marka)
        self.marka_txt      = wx.TextCtrl  (p, -1, data_arac.MARKA,size=(180,-1))
        style.setStaticTextList( marka_lbl,False)
        
        model_lbl   = wx.StaticText(p, label="%s :"%MESSAGES.model)
        self.model_txt      = wx.TextCtrl  (p, -1, data_arac.MODEL,size=(180,-1))
        style.setStaticTextList( model_lbl,False)
        
        yili_lbl   = wx.StaticText(p, label="%s :"%MESSAGES.yili)
        #self.yili_txt      = wx.TextCtrl  (p, -1, "",size=(180,-1))
        self.yili_txt      = style.CheckNumberMasked( p, 'yili',4,0,data_arac.YILI )
        style.setStaticTextList( yili_lbl,False)
        
        lbl_yakit_turu = wx.StaticText(p, label=MESSAGES.sablon_iki_kelime%( MESSAGES.yakit, MESSAGES.turu))
        style.setStaticTextList( lbl_yakit_turu,True )
        self.cmb_yakit_turu  = wx.ComboBox(p,1,"", choices=[],style=(wx.CB_DROPDOWN|wx.EXPAND|wx.CB_READONLY)) 
        yakit_turu_parametric_objects = []
        yakit_turu_parametric_objects = self.db.get_parameters_all("YAKIT",where="YAKIT_DURUM=1",order="YAKIT_ADI",selects="ID_YAKIT,YAKIT_ADI")
        
        if yakit_turu_parametric_objects != [] :
            self.db.widgetMaker( self.cmb_yakit_turu , yakit_turu_parametric_objects)
            
        if data_arac.ID_YAKIT == '':
            arac_yakit_index = -1
        else:
            arac_yakit_index = yakit_turu_parametric_objects.index(filter(lambda x:  x.id==data_arac.ID_YAKIT,yakit_turu_parametric_objects)[0])
        
        self.cmb_yakit_turu.SetSelection(arac_yakit_index)
        
        kit_tipi_parametric_objects = []
        kit_tipi_parametric_objects = self.db.get_parameters_all_with_id(table_name="P_KIT_TIPI",order="ID_KIT_TIPI",selects = "ID_KIT_TIPI,KIT_TIPI_ADI",where='ID_ACTIVE=1')
        kit_tipi_lbl                = wx.StaticText(p, label= MESSAGES.sablon_iki_kelime%( MESSAGES.kit, MESSAGES.turu))
        self.kit_tipi_arac_cmb     = wx.ComboBox(p,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        style.setStaticTextList( kit_tipi_lbl, True)
        self.db.widgetMaker( self.kit_tipi_arac_cmb , kit_tipi_parametric_objects)
        
        if data_arac.ID_KIT_TIPI == '':
            arac_kit_index = -1
        else:
            arac_kit_index = kit_tipi_parametric_objects.index(filter(lambda x:  x.id==data_arac.ID_KIT_TIPI,kit_tipi_parametric_objects)[0])
        
        self.kit_tipi_arac_cmb.SetSelection(arac_kit_index)
        
        uyari_lbl    = wx.StaticText(p, label=MESSAGES.zorunlu)
        self.ZorunluAlan(uyari_lbl)

        self.arac_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_arac_save  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%( MESSAGES.arac, MESSAGES.kaydet))
        self.btn_arac_save.Bind(wx.EVT_BUTTON,lambda event: self.OnAracKaydet(event,selected_item))
        self.btn_arac_save.SetToolTipString(MESSAGES.sablon_iki_kelime%( MESSAGES.arac, MESSAGES.kaydet))
        self.btn_arac_save.SetBitmapSelected(self.arac_save_pressed)
        style.setButtonFont( self.btn_arac_save)
        
        gbs.Add( cari_list_lbl, (1,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.cari_list_arac_cmb, (1,2) ,(1,1),  wx.EXPAND,3)
        
        gbs.Add( departman_lbl, (2,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.departman_txt, (2,2) ,(1,1),  wx.EXPAND,3)

        gbs.Add( plaka_lbl, (3,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.plaka_arac_txt, (3,2) ,(1,1),  wx.EXPAND,3)
        
        gbs.Add( marka_lbl, (4,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.marka_txt, (4,2) ,(1,1),  wx.EXPAND,3)

        gbs.Add( model_lbl, (5,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.model_txt, (5,2) ,(1,1),  wx.EXPAND,3)

        gbs.Add( yili_lbl, (6,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.yili_txt, (6,2) ,(1,1),  wx.EXPAND,3)

        gbs.Add( lbl_yakit_turu, (7,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.cmb_yakit_turu, (7,2) ,(1,1),  wx.EXPAND,3)

        gbs.Add( kit_tipi_lbl, (8,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.kit_tipi_arac_cmb, (8,2) ,(1,1),  wx.EXPAND,3)

    
        gbs.Add(self.btn_arac_save,  (9,2),(1,1),wx.ALL,1)        
        
        gbs.Add( uyari_lbl, (11,1),(1,3),  wx.ALL,3)
        
        
        p.SetSizerAndFit(gbs)
        p.SetAutoLayout(True)
        return p
    def OnAracKaydet( self, event,update):
        if self.cari_list_arac_cmb.GetSelection() == -1 or self.plaka_arac_txt.GetValue().strip() == '' or self.kit_tipi_arac_cmb.GetSelection() == -1 or self.cmb_yakit_turu.GetSelection() == -1:
            style.ok_message(self,MESSAGES.zorunlu,MESSAGES.hata)
        else:
            cari_id = self.cari_list_arac_cmb.GetClientData(self.cari_list_arac_cmb.GetSelection()).id
            kit_id  = self.kit_tipi_arac_cmb.GetClientData(self.kit_tipi_arac_cmb.GetSelection()).id
            yakit_id= self.cmb_yakit_turu.GetClientData(self.cmb_yakit_turu.GetSelection()).id
            
            departman = self.departman_txt.GetValue().strip()
            marka     = self.marka_txt.GetValue().strip()
            model     = self.model_txt.GetValue().strip()
            yili      = self.yili_txt.GetValue().strip()
            plaka     = self.plaka_arac_txt.GetValue().strip()

            if update:
                donen = self.db.update_data(table_name="ARACLAR",where="ID_ARAC=%s"%update,
                                              column_name="DEPARTMAN,ID_CARI_HESAP, PLAKA,MARKA,MODEL,YILI,ID_YAKIT,ID_KIT_TIPI",
                                              values="'%s',%s,'%s','%s','%s','%s',%s,%s"%(departman,cari_id,plaka,marka,model,yili,yakit_id,kit_id))
                if donen == 1 :
                    style.ok_message(self,"G�ncellemek �stedi�iniz Ara� Bilgileri Sisteme Ba�ar�yla Kaydedildi.","Ara� Tan�mlama")    
                else:
                    style.uyari_message(self,"G�ncellemek �stedi�iniz Ara� Bilgileri Sisteme Kaydedilemedi. Sistem Y�neticiniz ile G�r���n�z.","Ara� Tan�mlama")
            
            else:   
                sonuc=self.db.save_data( table_name="ARACLAR" ,
                column_name="DEPARTMAN,ID_CARI_HESAP, PLAKA,MARKA,MODEL,YILI,ID_YAKIT,ID_KIT_TIPI,ID_ACTIVE",
                values="'%s',%s,'%s','%s','%s','%s',%s,%s,1"%(departman,
                                                              cari_id,
                                                              plaka,
                                                              marka,
                                                              model,
                                                              yili,
                                                              yakit_id,
                                                              kit_id
                                                              ))
                if sonuc :
                        style.ok_message(self,MESSAGES.kaydedildi,MESSAGES.basarili)
                else:
                    style.uyari_message(self,MESSAGES.sablon_iki_kelime%(MESSAGES.sirket, MESSAGES.hata),MESSAGES.sablon_iki_kelime%(MESSAGES.sirket, MESSAGES.tanimlari))
    def CreateAracListe(self):
           
        headers        = ['ID,digit',
                          '%s,string'%MESSAGES.sablon_iki_kelime%(MESSAGES.cari, MESSAGES.adi),
                          '%s,string'%MESSAGES.departman,
                          '%s,string'%MESSAGES.plaka,
                          '%s,string'%MESSAGES.marka,
                          '%s,string'%MESSAGES.model,
                          '%s,string'%MESSAGES.yili,
                          '%s,string'%MESSAGES.yakit,
                          '%s,string'%MESSAGES.sablon_iki_kelime%(MESSAGES.kit, MESSAGES.turu),

                          ]
        '''
        data_history   = self.db.get_raw_data_all("ARAC_LISTE_VIEW" ,order = "ID_ARAC",selects="ID_ARAC,CARI_ADI,DEPARTMAN,PLAKA,MARKA,MODEL,YILI,YAKIT_ADI,KIT_TIPI_ADI",where ='ID_ACTIVE=1' )
        p_list = IndexListPanel( self.book, headers=headers, data = data_history)
        
        return p_list
        '''
        
        data_history   = self.db.get_raw_data_all("ARAC_LISTE_VIEW" ,order = "ID_ARAC",selects="ID_ARAC,CARI_ADI,DEPARTMAN,PLAKA,MARKA,MODEL,YILI,YAKIT_ADI,KIT_TIPI_ADI",where='ID_ACTIVE=1' ) 
        p_list = IndexListPanel( self.book, headers=headers, data = data_history,table_name='ARACLAR',where='ID_ARAC',delete_button=True,guncelle_button=True,guncellemede_gidilecek_sayfa=2,pasif_et=True,update_columns='ID_ACTIVE',update_values=2)
        return p_list 
    def createCari_Kart_Ekle(self):
    
        selected_item             = self.selected_item
        self.dummy.CARI_ADI       = ''
        self.dummy.YETKILI_ADI    = ''
        self.dummy.VERGI_DAIRESI  = ''
        self.dummy.VERGI_NO       = ''
        self.dummy.sehir          = ''
        self.dummy.ilce           = ''
        self.dummy.ADRES_1        = ''
        self.dummy.ADRES_2        = ''
        self.dummy.TEL            = ''
        self.dummy.FAX            = ''
        self.data_cari_hesap      = self.dummy

        p = wx.Panel(self.book,1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        
        gbs = wx.GridBagSizer(10, 10)

        if selected_item :
            #data_probe = self.db.get_raw_data_all("CARI_LISTE_VIEW",selects="ID_PROBE,PROBE_ADRES,PROBE_MARKA,PROBE_UZUNLUK,SPEED_OF_WIRE",where="ID_PROBE=%s"%selected_item,dict_mi='object')[0]
            self.data_cari_hesap = self.db.get_raw_data_all("CARI_LISTE_VIEW",selects="ID_CARI_HESAP,CARI_ADI,YETKILI_ADI,VERGI_DAIRESI,VERGI_NO,sehir,ilce,ADRES_1,ADRES_2,TEL,FAX,ID_IL,ID_ILCE",where='ID_CARI_HESAP=%s'%selected_item,dict_mi='object')[0]  
        
        Cari_adi_lbl      = wx.StaticText(p, label= MESSAGES.sablon_iki_kelime_dot%( MESSAGES.sirket, MESSAGES.adi))
        self.Cari_adi_txt = wx.TextCtrl  (p, -1,self.data_cari_hesap.CARI_ADI ,size=(180,-1))
        self.styl.setStaticTextList( Cari_adi_lbl,True)
        
        Cari_Yetkili_Adi_lbl   = wx.StaticText(p, label=MESSAGES.sablon_iki_kelime%( MESSAGES.yetkili, MESSAGES.adi))
        self.Cari_Yetkili_Adi_txt      = wx.TextCtrl  (p, -1, self.data_cari_hesap.YETKILI_ADI,size=(180,-1))
        self.styl.setStaticTextList( Cari_Yetkili_Adi_lbl,True)
        
        '''
        cari_tipi_parametric_objects = []
        cari_tipi_parametric_objects = self.db.get_parameters_all("P_CARI_TIPI",order="CARI_TIP_ADI",selects="ID_CARI_TIPI,CARI_TIP_ADI")
        
        
        cari_tipi_lbl    = wx.StaticText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.cari, MESSAGES.turu))
        self.cari_tipi_cmb    = wx.ComboBox(p,  size=(180, -1),choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY)
        self.styl.setStaticTextList( cari_tipi_lbl,True)
        
        if cari_tipi_parametric_objects != []:
         self.db.widgetMaker( self.cari_tipi_cmb , cari_tipi_parametric_objects)
        '''
        Cari_Vergi_Dairesi_lbl   = wx.StaticText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.vergi, MESSAGES.dairesi))
        self.Cari_Vergi_Dairesi_txt      = wx.TextCtrl  (p, -1, self.data_cari_hesap.VERGI_DAIRESI,size=(180,-1))
        self.styl.setStaticTextList( Cari_Vergi_Dairesi_lbl)
                
        Cari_Vergi_No_lbl   = wx.StaticText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.vergi, MESSAGES.no))
        self.Cari_Vergi_No_txt      = wx.TextCtrl  (p, -1, self.data_cari_hesap.VERGI_NO,size=(180,-1))
        self.styl.setStaticTextList( Cari_Vergi_No_lbl)
        
        Cari_Adres_lbl   = wx.StaticText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.fatura, MESSAGES.adres))
        self.Cari_Adres_txt      = wx.TextCtrl  (p, -1, self.data_cari_hesap.ADRES_1,size=(280,-1),style= wx.TE_MULTILINE | wx.SUNKEN_BORDER)
        self.styl.setStaticTextList( Cari_Adres_lbl)
        
        Cari_Adres2_lbl   = wx.StaticText(p, label="%s" % MESSAGES.sablon_iki_kelime_dot%( MESSAGES.sevk, MESSAGES.adres))
        self.Cari_Adres2_txt      = wx.TextCtrl  (p, -1, self.data_cari_hesap.ADRES_2,size=(180,-1),style= wx.TE_MULTILINE | wx.SUNKEN_BORDER)
        self.styl.setStaticTextList( Cari_Adres2_lbl)
        
        Cari_Tel_lbl   = wx.StaticText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.sirket, MESSAGES.telefon))
        #self.Cari_Tel_txt      = wx.TextCtrl  (p, -1, "",size=(180,-1))
        self.Cari_Tel_txt = self.styl.CheckTelFaxMasked ( p, 'cari_tel',11,self.data_cari_hesap.TEL)
        self.styl.setStaticTextList( Cari_Tel_lbl)
        
        uyari_lbl    = wx.StaticText(p, label=MESSAGES.zorunlu)
        self.ZorunluAlan(uyari_lbl)

        Cari_Fax_lbl   = wx.StaticText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.sirket, MESSAGES.faks))
        self.Cari_Fax_txt      = self.styl.CheckTelFaxMasked ( p, 'cari_fax',11,self.data_cari_hesap.FAX)
        self.styl.setStaticTextList( Cari_Fax_lbl)
        
        il_adi_parametric_objects = []
        il_adi_parametric_objects = self.db.get_parameters_all("P_ILLER",order="id",selects="id,sehir")
        
        ilce_adi_parametric_objects=[]
        
        il_adi_lbl    = wx.StaticText(p, label=MESSAGES.il)
        self.il_adi_cmb    = wx.ComboBox(p,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.il_adi_cmb.Bind(wx.EVT_COMBOBOX,self.onilcelerigetir)
        self.styl.setStaticTextList( il_adi_lbl)
        if il_adi_parametric_objects != []:
         self.db.widgetMaker( self.il_adi_cmb , il_adi_parametric_objects)

        ilce_adi_lbl       = wx.StaticText(p, label=MESSAGES.ilce)
        self.ilce_adi_cmb  = wx.ComboBox(p,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND) 
        self.styl.setStaticTextList( ilce_adi_lbl)

        if self.data_cari_hesap.sehir == '':
            sehir_index = -1
        else:
            sehir_index = il_adi_parametric_objects.index(filter(lambda x:  x.id==self.data_cari_hesap.ID_IL,il_adi_parametric_objects)[0])
            
        self.il_adi_cmb.SetSelection(sehir_index)
        if self.data_cari_hesap.sehir != '':
            self.onilcelerigetir(None)
 
        
        self.btn_kart_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_kart_save  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%( MESSAGES.cari, MESSAGES.kaydet))
        self.btn_kart_save.Bind(wx.EVT_BUTTON,lambda event: self.OnCariKaydet(event,selected_item))
        self.btn_kart_save.SetToolTipString(MESSAGES.sablon_iki_kelime%( MESSAGES.cari, MESSAGES.kaydet))
        self.btn_kart_save.SetBitmapSelected(self.btn_kart_save_pressed)
        style.setButtonFont( self.btn_kart_save)
        
        gbs.Add( Cari_adi_lbl, (1,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.Cari_adi_txt, (1,2) ,(1,1),  wx.EXPAND,3)
        
        gbs.Add( Cari_Yetkili_Adi_lbl, (2,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.Cari_Yetkili_Adi_txt, (2,2) ,(1,1),  wx.EXPAND,3)
        
        gbs.Add( Cari_Vergi_Dairesi_lbl, (3,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.Cari_Vergi_Dairesi_txt, (3,2) ,(1,1),  wx.EXPAND,3)
        
        gbs.Add( Cari_Vergi_No_lbl, (4,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.Cari_Vergi_No_txt, (4,2) ,(1,1),  wx.EXPAND,3)
        
        gbs.Add( Cari_Adres_lbl, (5,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.Cari_Adres_txt, (5,2) ,(1,1),  wx.EXPAND,3)
        
        gbs.Add( Cari_Adres2_lbl, (6,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.Cari_Adres2_txt, (6,2) ,(1,1),  wx.EXPAND,3)
        
        gbs.Add( Cari_Tel_lbl, (7,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.Cari_Tel_txt, (7,2) ,(1,1),  wx.EXPAND,3)
        
        gbs.Add( Cari_Fax_lbl, (8,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.Cari_Fax_txt, (8,2) ,(1,1),  wx.EXPAND,3)
        
        gbs.Add( il_adi_lbl, (5,4),(1,1),  wx.ALL,3)
        gbs.Add( self.il_adi_cmb,(5,5) ,(1,2),  wx.ALL,3)
        
        gbs.Add( ilce_adi_lbl, (6,4),(1,1),  wx.ALL,3)
        gbs.Add( self.ilce_adi_cmb,(6,5) ,(1,2),  wx.ALL,3)
        
        '''
        gbs.Add( cari_tipi_lbl, (2,4),(1,1),  wx.ALL,3)
        gbs.Add( self.cari_tipi_cmb,(2,5) ,(1,2),  wx.ALL,3)
        '''
    
        gbs.Add(self.btn_kart_save,  (10,2),(1,1),wx.ALL,1)        
        
        gbs.Add( uyari_lbl, (12,1),(1,3),  wx.ALL,3)
        
        
        p.SetSizerAndFit(gbs)
        p.SetAutoLayout(True)
        return p
    def ZorunluAlan(self, widget):
     font  = wx.Font(12, wx.MODERN,wx.NORMAL, wx.BOLD )
     color = wx.Colour( 255,0,0 )
     widget.SetFont(font)
     widget.SetForegroundColour( color )
    def onilcelerigetir(self,event):
        if self.il_adi_cmb.GetSelection()==-1:
            
            style.ok_message(self,MESSAGES.once_il_sec,MESSAGES.hata)
        else :
            ilce_adi_parametric_objects=self.db.getIlceObjectListByIl(self.il_adi_cmb.GetClientData(self.il_adi_cmb.GetSelection()).id)
            self.db.widgetMaker( self.ilce_adi_cmb , ilce_adi_parametric_objects)
            if self.data_cari_hesap.ilce == '':
                ilce_index = -1
            else:
                ilce_index = ilce_adi_parametric_objects.index(filter(lambda x:  x.id==self.data_cari_hesap.ID_ILCE,ilce_adi_parametric_objects)[0])
            self.ilce_adi_cmb.SetSelection(ilce_index) 
    def OnCariKaydet(self,event,update):
        cari_adi = self.Cari_adi_txt.GetValue().strip()
        cari_yetkili_adi = self.Cari_Yetkili_Adi_txt.GetValue().strip()
        cari_vergi_daire=self.Cari_Vergi_Dairesi_txt.GetValue().strip()
        cari_vergi_no=self.Cari_Vergi_No_txt.GetValue().strip()
        cari_adres=self.Cari_Adres_txt.GetValue().strip()
        cari_adres2=self.Cari_Adres2_txt.GetValue().strip()
        cari_tel=self.Cari_Tel_txt.GetValue().strip()
        cari_fax=self.Cari_Fax_txt.GetValue().strip()
        
        if cari_adi == '' or cari_yetkili_adi == '':
            style.ok_message(self,MESSAGES.zorunlu,MESSAGES.hata)
        else:
            if self.il_adi_cmb.GetSelection() == -1:
                il_adi_id = 'null'
            else:
                il_adi_id=self.il_adi_cmb.GetClientData(self.il_adi_cmb.GetSelection()).id
            if self.ilce_adi_cmb.GetSelection() == -1:
                ilce_adi_id = 'null'
            else:
                ilce_adi_id=self.ilce_adi_cmb.GetClientData(self.ilce_adi_cmb.GetSelection()).id
            cari_tipi_id=sabitler.CARI_TIPLER.MUSTERI
            
            if update:
                donen = self.db.update_data(table_name="CARI_HESAPLAR",where="ID_CARI_HESAP=%s"%update,
                                              column_name="CARI_ADI,YETKILI_ADI, ID_CARI_TIPI,VERGI_DAIRESI,VERGI_NO,ID_IL,ID_ILCE,ADRES_1,ADRES_2,TEL,FAX",
                                              values="'%s','%s','%s','%s','%s',%s,%s,'%s','%s','%s','%s'"%(cari_adi,cari_yetkili_adi,cari_tipi_id,cari_vergi_daire,
                                                                                                           cari_vergi_no,il_adi_id,ilce_adi_id,cari_adres,cari_adres2,cari_tel,cari_fax))
                if donen == 1 :
                    style.ok_message(self,"G�ncellemek �stedi�iniz �irket Bilgileri Sisteme Ba�ar�yla Kaydedildi.","�irket Tan�mlama")    
                else:
                    style.uyari_message(self,"G�ncellemek �stedi�iniz �irket Bilgileri Sisteme Kaydedilemedi. Sistem Y�neticiniz ile G�r���n�z.","�irket Tan�mlama")
            
            else:
                
                sonuc=self.db.save_data( table_name="CARI_HESAPLAR" ,
                column_name="CARI_ADI,YETKILI_ADI, ID_CARI_TIPI,VERGI_DAIRESI,VERGI_NO,ID_IL,ID_ILCE,ADRES_1,ADRES_2,TEL,FAX,ID_ACTIVE",
                values="'%s','%s','%s','%s','%s',%s,%s,'%s','%s','%s','%s',1"%(cari_adi,cari_yetkili_adi,cari_tipi_id,cari_vergi_daire,
                                                                                 cari_vergi_no,il_adi_id,ilce_adi_id,cari_adres,cari_adres2,cari_tel,cari_fax) )
                if sonuc :
                    style.ok_message(self,MESSAGES.kaydedildi,MESSAGES.basarili)
                else:
                    style.uyari_message(self,MESSAGES.sablon_iki_kelime%(MESSAGES.sirket, MESSAGES.hata),MESSAGES.sablon_iki_kelime%(MESSAGES.sirket, MESSAGES.tanimlari))
    def OnPersonelKaydet(self,event, selected_item):
        personel_adi         = self.personel_adi_txt.GetValue().strip()
        personel_yetkili_adi = self.personel_Yetkili_Adi_txt.GetValue().strip()
        personel_adres       = self.personel_Adres_txt.GetValue().strip()
        personel_tel         = self.personel_Tel_txt.GetValue().strip()
        cari_tipi_id=sabitler.CARI_TIPLER.PERSONEL
        
        if personel_adi == '' or personel_yetkili_adi == '':
            style.ok_message(self,MESSAGES.zorunlu,MESSAGES.hata)
        else:
            
            if selected_item:
                donen = self.db.update_data(table_name="CARI_HESAPLAR",where="ID_CARI_HESAP=%s"%selected_item,
                                              column_name="CARI_ADI,YETKILI_ADI, ID_CARI_TIPI,ADRES_1,TEL",
                                              values="'%s','%s',%s,'%s','%s'"%(personel_adi,personel_yetkili_adi,cari_tipi_id,personel_adres,personel_tel))
                if donen == 1 :
                    style.ok_message(self,"G�ncellemek �stedi�iniz Personel Bilgileri Sisteme Ba�ar�yla Kaydedildi.","�irket Tan�mlama")    
                else:
                    style.uyari_message(self,"G�ncellemek �stedi�iniz Personel Bilgileri Sisteme Kaydedilemedi. Sistem Y�neticiniz ile G�r���n�z.","�irket Tan�mlama")
            else:
                
                
                sonuc=self.db.save_data( table_name="CARI_HESAPLAR" ,
                column_name="CARI_ADI,YETKILI_ADI, ID_CARI_TIPI,ADRES_1,TEL,ID_ACTIVE",
                values="'%s','%s','%s','%s','%s',1"%(personel_adi,personel_yetkili_adi,cari_tipi_id,
                                                                           personel_adres,personel_tel) )
                if sonuc :
                    style.ok_message(self,MESSAGES.kaydedildi,MESSAGES.basarili)
                else:
                    style.uyari_message(self,MESSAGES.sablon_iki_kelime%(MESSAGES.personel, MESSAGES.hata),MESSAGES.sablon_iki_kelime%(MESSAGES.personel, MESSAGES.tanimlari))
    def OnBookPageChanged( self, event ):
        if self.book.GetSelection() == 1:
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.book.Freeze()
            self.book.SetSelection(0)
            p_ = self.CreateCariListe()
            self.book.RemovePage(1)
            style.setPanelListBg(p_)
            self.book.InsertPage( 1,p_,MESSAGES.sablon_iki_kelime%( MESSAGES.sirket, MESSAGES.listesi))
            self.book.SetPageImage(1, self.img1)
            self.book.SetSelection(1)
            self.book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.book.Thaw()
            
        elif self.book.GetSelection() == 2:
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.book.Freeze()
            self.book.SetSelection(0)
            p_ = self.CreateAracEkle()
            style.setPanelListBg(p_)
            self.book.RemovePage(2)
            self.book.InsertPage( 2,p_,MESSAGES.sablon_iki_kelime%( MESSAGES.arac, MESSAGES.ekle))
            self.book.SetPageImage(2, self.img7)
            self.book.SetSelection(2)
            self.book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.book.Thaw()
        
        elif self.book.GetSelection() == 3:
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.book.Freeze()
            self.book.SetSelection(0)
            p_ = self.CreateAracListe()
            style.setPanelListBg(p_)
            self.book.RemovePage(3)
            self.book.InsertPage( 3,p_,MESSAGES.sablon_iki_kelime%( MESSAGES.arac, MESSAGES.listesi))
            self.book.SetPageImage(3, self.img8)
            self.book.SetSelection(3)
            self.book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.book.Thaw()
        
        elif self.book.GetSelection() == 5:
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.book.Freeze()
            self.book.SetSelection(0)
            p_ = self.CreatePersonelListe()
            style.setPanelListBg(p_)
            self.book.RemovePage(5)
            self.book.InsertPage( 5,p_,MESSAGES.sablon_iki_kelime%( MESSAGES.personel, MESSAGES.listesi))
            self.book.SetPageImage(5, self.img6)
            self.book.SetSelection(5)
            self.book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.book.Thaw()

        elif self.book.GetSelection() == 7:
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.Freeze()
            self.book.SetSelection(0)
            p = self.createKartListesi()
            style.setPanelListBg(p)
            self.book.RemovePage(7)
            self.book.InsertPage(7, p,MESSAGES.sablon_iki_kelime%(MESSAGES.kart, MESSAGES.listesi ))
            self.book.SetPageImage(7, self.img4)
            self.book.SetSelection(7)
            self.book.Bind(  aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.Thaw()
        elif self.book.GetSelection() == 0:
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.Freeze()
            #self.selected_item = None
            self.book.SetSelection(1)
            p = self.createCari_Kart_Ekle()
            style.setPanelListBg(p)
            self.book.RemovePage(0)
            self.book.InsertPage(0, p,MESSAGES.sablon_iki_kelime%(MESSAGES.sirket, MESSAGES.ekle ))
            self.book.SetPageImage(0, self.img0)
            self.book.SetSelection(0)
            self.book.Bind(  aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.Thaw()
        elif self.book.GetSelection() == 4:
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.Freeze()
            #self.selected_item = None
            self.book.SetSelection(0)
            p = self.CreatePersonelEkle()
            style.setPanelListBg(p)
            style.setPanelListBg( p )
            self.book.RemovePage(4)
            self.book.InsertPage(4, p,MESSAGES.sablon_iki_kelime%(MESSAGES.personel, MESSAGES.ekle ))
            self.book.SetPageImage(4, self.img2)
            self.book.SetSelection(4)
            self.book.Bind(  aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.Thaw()
        elif self.book.GetSelection() == 6:
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.Freeze()
            #self.selected_item = None
            self.book.SetSelection(0)
            p = self.createKartTanimla()
            self.selected_kart = self.selected_item
            style.setPanelListBg(p)
            style.setPanelListBg( p )
            self.book.RemovePage(6)
            self.book.InsertPage(6, p,MESSAGES.sablon_iki_kelime%(MESSAGES.kart, MESSAGES.ekle ))
            self.book.SetPageImage(6, self.img3)
            self.book.SetSelection(6)
            self.book.Bind(  aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.onTextChange(None)
            self.Thaw()
        self.selected_item = None
    def CreateCariListe(self):
           
        headers        = ['ID,digit',
                          '%s,string'%MESSAGES.sablon_iki_kelime%(MESSAGES.cari, MESSAGES.adi),
                          '%s,string'%MESSAGES.sablon_iki_kelime%(MESSAGES.yetkili,MESSAGES.adi),
                          '%s,string'%MESSAGES.sablon_iki_kelime%(MESSAGES.vergi,MESSAGES.dairesi),
                          '%s,string'%MESSAGES.sablon_iki_kelime%(MESSAGES.vergi,MESSAGES.no),
                          '%s,string'%MESSAGES.il,
                          '%s,string'%MESSAGES.ilce,
                          '%s,string'%MESSAGES.adres,
                          '%s (2),string'%MESSAGES.adres,
                          '%s,string'%MESSAGES.telefon,
                          '%s,string'%MESSAGES.faks]
        data_history   = self.db.get_raw_data_all("CARI_LISTE_VIEW" ,order = "ID_CARI_HESAP",selects="ID_CARI_HESAP,CARI_ADI,YETKILI_ADI,VERGI_DAIRESI,VERGI_NO,sehir,ilce,ADRES_1,ADRES_2,TEL,FAX",where='ID_CARI_TIPI=%s and ID_ACTIVE=%s'%(sabitler.CARI_TIPLER.MUSTERI,1) ) 
        #p_list = IndexListPanel( self.book, headers=headers, data = data_history)
        p_list = IndexListPanel( self.book, headers=headers, data = data_history,table_name='CARI_HESAPLAR',where='ID_CARI_HESAP',delete_button=True,guncelle_button=True,guncellemede_gidilecek_sayfa=0,pasif_et=True,update_columns='ID_ACTIVE',update_values=2)
        return p_list
    def createKartListesi(self):
        headers        = ['ID,digit',
                          '%s,string'%MESSAGES.sablon_iki_kelime%( MESSAGES.kart, MESSAGES.adi),
                          '%s,string'%MESSAGES.sablon_iki_kelime%( MESSAGES.cari, MESSAGES.adi),
                          '%s,string'%MESSAGES.sablon_iki_kelime%( MESSAGES.iskonto, MESSAGES.adi),
                          '%s,string'%MESSAGES.plaka,
                          '%s,integer'%MESSAGES.sablon_iki_kelime%( MESSAGES.hesap, MESSAGES.donemi),
                          '%s,string'%MESSAGES.sablon_iki_kelime%( MESSAGES.hesap,  MESSAGES.tarih),
                          '%s,integer'%MESSAGES.limit,
                          '%s,string'%MESSAGES.sablon_iki_kelime%( MESSAGES.limit,  MESSAGES.turu),
                          '%s,string'%MESSAGES.sablon_iki_kelime%( MESSAGES.cari,  MESSAGES.turu)]
        
        data_history   = self.db.get_raw_data_all("KART_LISTE_VIEW" ,order = "ID_KART",where='ID_KART_WEB is null AND ID_KART is not null AND ID_ACTIVE = 1',
                                                  selects="ID_KART,KART_ADI,CARI_ADI,ISKONTO_ADI,PLAKA,LIMIT_DONEM,H_TARIH,U_LIMIT,LIMIT_CESIT,CARI_TIP_ADI")  
        p_list = IndexListPanel( self.book, headers=headers, data = data_history,table_name='KARTLAR',where='ID_KART',delete_button=True,guncelle_button=True,guncellemede_gidilecek_sayfa=6,pasif_et=True,update_columns='ID_ACTIVE',update_values=2)
      
        return p_list
    def createKartTanimla(self):
        self.Freeze()
        self.p_kart_ = wx.Panel(self.book,1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        self.gridbagsiz=wx.GridBagSizer(5,5)
        self.plaka_list_cmb = None
        style.setPanelListBg(self.p_kart_)
        self.cari_listesi_parametric_objects = []
        self.cari_listesi_parametric_objects=self.db.get_parameters_all_with_id(table_name="CARI_LISTE_VIEW",order="CARI_ADI",selects = "ID_CARI_HESAP,CARI_ADI,CARI_TIP_ADI",where='ID_CARI_TIPI=1')
        
        self.cari_list_lbl    = wx.StaticText(self.p_kart_, label= '%s :'%MESSAGES.personel)
        self.cari_list_cmb    = wx.ComboBox(self.p_kart_,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.cari_list_cmb.Bind(wx.EVT_COMBOBOX,self.onCariChange)
        style.setStaticTextList( self.cari_list_lbl,True)
        self.db.widgetMaker( self.cari_list_cmb , self.cari_listesi_parametric_objects)

        #iskonto_listesi_parametric_objects = []
        #iskonto_listesi_parametric_objects=self.db.get_parameters_all_with_id(table_name="ISKONTO_VIEW",order="ISKONTO_ADI",selects = "ISKONTO_ADI")
        iskonto_list_lbl    = wx.StaticText(self.p_kart_, label='%s :'%MESSAGES.iskonto)
        self.iskonto_list_cmb    = wx.ComboBox(self.p_kart_,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        style.setStaticTextList( iskonto_list_lbl)
        self.iskonto_list_cmb.Disable()
        
        #self.db.widgetMaker( self.iskonto_list_cmb , iskonto_listesi_parametric_objects)
     
      
        kart_adi_lbl   = wx.StaticText(self.p_kart_, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.kart, MESSAGES.adi))
        self.kart_adi_txt      = wx.TextCtrl  (self.p_kart_, -1, "",size=(180,-1))
        style.setStaticTextList( kart_adi_lbl,True)
        #self.kart_adi_txt.SetValue('A1245647')
        
        
        ust_limit_lbl   = wx.StaticText(self.p_kart_, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.ust, MESSAGES.limit))
        self.ust_limit_txt      = wx.TextCtrl  (self.p_kart_, -1, "",size=(180,-1))
        style.setStaticTextList( ust_limit_lbl)
        
        self.ust_limit_cesit_parametric_objects = []
        self.ust_limit_cesit_parametric_objects=self.db.get_parameters_all_with_id(table_name="P_LIMIT_CESIT",order="ID_LIMIT_CESIT",selects = "ID_LIMIT_CESIT,LIMIT_CESIT ")       
        ust_limit_cesit_lbl    = wx.StaticText(self.p_kart_, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.limit, MESSAGES.turu))
        style.setStaticTextList( ust_limit_cesit_lbl)
        self.ust_limit_cesit_cmb    = wx.ComboBox(self.p_kart_,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.db.widgetMaker( self.ust_limit_cesit_cmb ,self.ust_limit_cesit_parametric_objects)
        
        self.ust_limit_donem_parametric_objects = []
        self.ust_limit_donem_parametric_objects=self.db.get_parameters_all_with_id(table_name="P_LIMIT_DONEM",order="ID_LIMIT_DONEM",selects = "ID_LIMIT_DONEM,LIMIT_DONEM ")
        ust_limit_donem_lbl    = wx.StaticText(self.p_kart_, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.hesap, MESSAGES.donemi))
        style.setStaticTextList( ust_limit_donem_lbl)
        
        self.ust_limit_donem_cmb    = wx.ComboBox(self.p_kart_,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.ust_limit_donem_cmb.Bind(wx.EVT_COMBOBOX,self.onLimitChange)
        self.db.widgetMaker( self.ust_limit_donem_cmb , self.ust_limit_donem_parametric_objects)
        
        self.pompa_ac_check       = wx.CheckBox(self.p_kart_,32 , '',style=wx.ALIGN_RIGHT)
        
        
        self.pompa_ac_check_label = wx.StaticText(self.p_kart_, label="%s :"%MESSAGES.pompa_acilsin_mi)
        style.setStaticTextList( self.pompa_ac_check_label,True)
        
        plaka_lbl   = wx.StaticText(self.p_kart_, label="%s :"%MESSAGES.plaka)
        self.plaka_txt      = wx.TextCtrl  (self.p_kart_, -1, "",size=(180,-1))
        style.setStaticTextList( plaka_lbl,True)
        self.plaka_check=wx.CheckBox(self.p_kart_, label=MESSAGES.sablon_iki_kelime%( MESSAGES.plaka, MESSAGES.aktif), pos=(20, 20))
        self.plaka_check.SetValue(False)
        self.plaka_check.Bind(wx.EVT_CHECKBOX, self.EnablePlaka)
        #self.plaka_check.Hide()
        
        lbl_tarih_dpc =style.TransparentText(self.p_kart_,label="%s :"%MESSAGES.tarih)
        style.setStaticTextList(lbl_tarih_dpc)
        '''
        self.tarih_dpc = wx.DatePickerCtrl(self.p_kart_, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )
        '''

        self.donem_tarih_cmb    = wx.ComboBox(self.p_kart_,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.donem_tarih_cmb.Disable()
        
        self.btn_card_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_card_save  = LibButtons.ThemedGenBitmapTextButton(self.p_kart_, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%(MESSAGES.kart,MESSAGES.kaydet))
        self.btn_card_save.Bind(wx.EVT_BUTTON, self.OnCardKaydet)
        self.btn_card_save.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.kart,MESSAGES.kaydet))
        self.btn_card_save.SetBitmapSelected(self.btn_card_save_pressed)
        style.setButtonFont( self.btn_card_save)
        
        line_                           =   wx.StaticLine(self.p_kart_, -1, style=wx.LI_HORIZONTAL)
        
        card_reader_parametric_objects = []
        card_reader_parametric_objects=self.db.get_parameters_all_with_id(table_name="VIEW_POMPA_TABANCALAR",order="POMPA_NO",selects = "distinct ( POMPA_NO ),POMPA_NO", where ="KART_OKUYUCU_ADRES is not null")
        kart_okuyucu_list_lbl    = wx.StaticText(self.p_kart_, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.pompa, MESSAGES.no))
        self.kart_okuyucu_list_cmb    = wx.ComboBox(self.p_kart_,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        style.setStaticTextList( kart_okuyucu_list_lbl)
        #self.kart_okuyucu_list_cmb.Bind(wx.EVT_COMBOBOX,self.onLimitChange)
        self.db.widgetMaker( self.kart_okuyucu_list_cmb , card_reader_parametric_objects)
        # oku butonuna bas�ld���nda gelen kart ID buraya yerle�ecek
        kart_seri_no_lbl   = wx.StaticText(self.p_kart_, label=MESSAGES.sablon_uc_kelime_dot%(MESSAGES.kart,MESSAGES.seri,MESSAGES.no))
        self.kart_seri_no_txt      = wx.TextCtrl  (self.p_kart_, -1, "",size=(180,-1),style=wx.TE_READONLY)
        style.setStaticTextList( kart_seri_no_lbl,True)
        #self.kart_seri_no_txt.Bind(wx.EVT_TEXT,self.onTextChange)
        
        
        self.btn_card_id_read_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_card_id_read  = LibButtons.ThemedGenBitmapTextButton(self.p_kart_, id=wx.NewId(), bitmap=image_save,label=MESSAGES.seri_no_bekle)
        self.btn_card_id_read.Bind(wx.EVT_BUTTON, self.OnCardRfIDSet )
        self.btn_card_id_read.SetToolTipString(MESSAGES.seri_no_bekle)
        self.btn_card_id_read.SetBitmapSelected(self.btn_card_id_read_pressed)
        style.setButtonFont( self.btn_card_id_read)
        
        self.personel_radio=wx.RadioButton(self.p_kart_,-1,MESSAGES.sablon_iki_kelime%(MESSAGES.personel,MESSAGES.kart),style=wx.ALL)
        self.personel_radio.Bind(wx.EVT_RADIOBUTTON,self.onPersonelRadio)
        self.personel_radio.Value=True
        
        self.musteri_radio=wx.RadioButton(self.p_kart_,1,MESSAGES.sablon_ayrac%(MESSAGES.musteri,MESSAGES.arac) +' '+ MESSAGES.kart,style=wx.ALL)
        self.musteri_radio.Bind(wx.EVT_RADIOBUTTON,self.onMusteriRadio)

        self.iskonto_listesi_parametric_objects = []
        self.iskonto_listesi_parametric_objects=self.db.get_parameters_all(table_name="ISKONTO",order="ISKONTO_ADI",selects = "ID_ISKONTO,ISKONTO_ADI")
                
        
        self.gridbagsiz.Add(kart_okuyucu_list_lbl,(1,1),(1,1),wx.ALL,3)
        self.gridbagsiz.Add(self.kart_okuyucu_list_cmb,(1,2),(1,1),wx.ALL,3)
        self.gridbagsiz.Add(self.btn_card_id_read,  (1,3),(2,2),wx.ALL,1)
        
        self.gridbagsiz.Add( kart_seri_no_lbl, (2,1),(1,1),  wx.ALL,3)
        self.gridbagsiz.Add( self.kart_seri_no_txt,(2,2) ,(1,1),  wx.ALL,3)  
        
        self.gridbagsiz.Add( line_,  (3,0 ),(1,5),flag=wx.EXPAND|wx.BOTTOM, border=10)
        
        self.gridbagsiz.Add(self.personel_radio,(4,1),(1,1),wx.ALL,3)
        self.gridbagsiz.Add(self.musteri_radio,(4,2),(1,1),wx.ALL,3)
        
        self.gridbagsiz.Add( kart_adi_lbl, (5,1),(1,1),  wx.ALL,3)
        self.gridbagsiz.Add( self.kart_adi_txt,(5,2) ,(1,1),  wx.ALL,3)
        
        self.gridbagsiz.Add( self.cari_list_lbl, (6,1),(1,1),  wx.ALL,3)
        self.gridbagsiz.Add( self.cari_list_cmb,(6,2) ,(1,1),  wx.ALL,3)
        
        self.gridbagsiz.Add( plaka_lbl, (7,1),(1,1),  wx.ALL,3)
        self.gridbagsiz.Add( self.plaka_txt,(7,2) ,(1,1),  wx.ALL,3)
        self.gridbagsiz.Add(self.plaka_check,(7,3),(1,1),  wx.ALL,3)
        
        self.gridbagsiz.Add( ust_limit_donem_lbl, (8,1),(1,1),  wx.ALL,3)
        self.gridbagsiz.Add( self.ust_limit_donem_cmb,(8,2) ,(1,1),  wx.ALL,3)
        
        self.gridbagsiz.Add(lbl_tarih_dpc,(8,3),(1,1),wx.ALL,3)
        self.gridbagsiz.Add(self.donem_tarih_cmb,(8,4),(1,1),wx.ALL,3)
        
        self.gridbagsiz.Add( ust_limit_lbl, (9,1),(1,1),  wx.ALL,3)
        self.gridbagsiz.Add( self.ust_limit_txt,(9,2) ,(1,1),  wx.ALL,3)
        
        self.gridbagsiz.Add( ust_limit_cesit_lbl, (9,3),(1,1),  wx.ALL,3)
        self.gridbagsiz.Add( self.ust_limit_cesit_cmb,(9,4) ,(1,1),  wx.ALL,3)
      
        self.gridbagsiz.Add( iskonto_list_lbl, (10,1),(1,1),  wx.ALL,3)
        self.gridbagsiz.Add( self.iskonto_list_cmb,(10,2) ,(1,1),  wx.ALL,3)
        
        
        self.gridbagsiz.Add(self.pompa_ac_check_label, (11,1),(1,1), wx.ALL, 3)
        self.gridbagsiz.Add(self.pompa_ac_check, (11,2),(1,1), wx.ALL, 3)
        self.gridbagsiz.Add(self.btn_card_save,  (12,2),(1,1),wx.ALL,1)
        
        self.p_kart_.SetSizerAndFit(self.gridbagsiz)
        self.p_kart_.SetAutoLayout(True)
        
        self.plaka_txt.Disable()
        self.plaka_txt.SetValue(MESSAGES.personel)
        self.plaka_check.Show()
        self.ust_limit_donem_cmb.Disable()
        self.ust_limit_donem_cmb.SetSelection(-1)
        self.donem_tarih_cmb.Disable()
        self.donem_tarih_cmb.SetSelection(-1)
        self.ust_limit_txt.Disable()
        self.ust_limit_txt.SetValue(MESSAGES.personel)
        self.ust_limit_cesit_cmb.Disable()
        self.ust_limit_cesit_cmb.SetSelection(-1)
        self.iskonto_list_cmb.Disable()
        self.iskonto_list_cmb.SetSelection(-1)
        self.pompa_ac_check.Disable()
        self.pompa_ac_check.SetValue( 1 )
        self.Refresh()

        self.Thaw()
        
        return self.p_kart_
    def EnablePlaka(self,event ):
        if self.plaka_check.IsChecked:
            self.plaka_txt.Clear()
            self.plaka_txt.Enable()
        if self.plaka_check.Value==False:
            self.plaka_txt.Disable()
            self.plaka_txt.SetValue(MESSAGES.personel)
    def onMusteriRadio(self,event):
        self.cari_listesi_parametric_objects=self.db.get_parameters_all_with_id(table_name="CARI_LISTE_VIEW",order="CARI_ADI",selects = "ID_CARI_HESAP,CARI_ADI,CARI_TIP_ADI",where='ID_CARI_TIPI=2')
        self.db.widgetMaker( self.cari_list_cmb , self.cari_listesi_parametric_objects)
        self.cari_list_cmb.Refresh()
        if self.plaka_txt:
            self.plaka_txt.Destroy()
            self.plaka_txt = None
        self.plaka_parametric_objects  = [MESSAGES.sablon_iki_kelime%( MESSAGES.sirket, MESSAGES.sec)]
        if not self.plaka_list_cmb:
            self.plaka_list_cmb            = wx.ComboBox(self.p_kart_,choices=self.plaka_parametric_objects, style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
            self.gridbagsiz.Add( self.plaka_list_cmb,(7,2) ,(1,1),  wx.ALL,3)
        self.ust_limit_donem_cmb.Enable()
        self.ust_limit_txt.Enable()
        self.ust_limit_txt.Clear()
        self.ust_limit_cesit_cmb.Enable()
        self.iskonto_list_cmb.Enable()
        self.plaka_check.Hide()
        self.pompa_ac_check.Enable()
        self.pompa_ac_check.SetValue( 0 )
        self.cari_list_lbl.SetLabel('* ' + MESSAGES.sirket)
        self.gridbagsiz.Layout()
        
        self.Refresh()
    def onPersonelRadio(self,event):
        self.cari_listesi_parametric_objects=self.db.get_parameters_all_with_id(table_name="CARI_LISTE_VIEW",order="CARI_ADI",selects = "ID_CARI_HESAP,CARI_ADI,CARI_TIP_ADI",where='ID_CARI_TIPI=1')
        self.db.widgetMaker( self.cari_list_cmb , self.cari_listesi_parametric_objects)
        self.cari_list_cmb.Refresh()
        if self.plaka_list_cmb:
            self.plaka_list_cmb.Destroy()
            self.plaka_list_cmb = None
        if not self.plaka_txt:
            self.plaka_txt = wx.TextCtrl  (self.p_kart_, -1, "",size=(180,-1))
            self.gridbagsiz.Add( self.plaka_txt,(7,2) ,(1,1),  wx.ALL,3)
        self.plaka_txt.Disable()
        self.plaka_txt.SetValue(MESSAGES.personel)
        self.plaka_check.Show()
        self.ust_limit_donem_cmb.Disable()
        self.ust_limit_donem_cmb.SetSelection(-1)
        self.donem_tarih_cmb.Disable()
        self.donem_tarih_cmb.SetSelection(-1)
        self.ust_limit_txt.Disable()
        self.ust_limit_txt.SetValue(MESSAGES.personel)
        self.ust_limit_cesit_cmb.Disable()
        self.ust_limit_cesit_cmb.SetSelection(-1)
        self.iskonto_list_cmb.Disable()
        self.iskonto_list_cmb.SetSelection(-1)
        self.pompa_ac_check.Disable()
        self.pompa_ac_check.SetValue( 1 )
        self.cari_list_lbl.SetLabel('* ' + MESSAGES.personel)
        
        self.gridbagsiz.Layout()
        self.Refresh()    
    def onAracRadio( self, event ):
        pass
    def onLimitChange(self,event):
        self.aylik_choose=['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']
        self.haftalik_choose=[MESSAGES.pazartesi,MESSAGES.sali,MESSAGES.carsamba,MESSAGES.persembe,MESSAGES.cuma,MESSAGES.cumartesi,MESSAGES.pazar]
        #print self.ust_limit_donem_cmb.Value
        if self.ust_limit_donem_cmb.Value==MESSAGES.aylik:
            self.donem_tarih_cmb.Enable()
            self.db.widgetMakerStr( self.donem_tarih_cmb , self.aylik_choose)
            self.donem_tarih_cmb.Update()
        elif self.ust_limit_donem_cmb.Value==MESSAGES.haftalik:
            self.donem_tarih_cmb.Enable()
            self.db.widgetMakerStr( self.donem_tarih_cmb , self.haftalik_choose)
            self.donem_tarih_cmb.Update()
        else:
            self.donem_tarih_cmb.Disable()
        self.Refresh()  
    def onCariChange(self,event):
        if self.cari_list_cmb.GetSelection()!=-1 :
            cari_id=self.cari_list_cmb.GetClientData(self.cari_list_cmb.GetSelection()).id
            cari_tipi=self.db.get_raw_data_all(table_name="CARI_HESAPLAR",order="CARI_ADI",selects = "ID_CARI_TIPI",where="ID_CARI_HESAP=%s"%cari_id)
            cari_tipi_id=cari_tipi[0][0]
            if cari_tipi_id==sabitler.CARI_TIPLER.PERSONEL:
                self.iskonto_list_cmb.SetSelection(-1)
                self.iskonto_list_cmb.Disable()
            elif cari_tipi_id==sabitler.CARI_TIPLER.MUSTERI:
                #plaka_listesi            = self.db.getAracByCari( cari_id )
                self.plaka_parametric_objects  = self.db.get_parameters_all(table_name="ARACLAR",order="PLAKA",selects = "ID_ARAC,PLAKA", where="ID_CARI_HESAP = %s" % cari_id)
                self.db.widgetMaker( self.plaka_list_cmb , self.plaka_parametric_objects)
                self.iskonto_list_cmb.Enable()
                self.db.widgetMaker( self.iskonto_list_cmb , self.iskonto_listesi_parametric_objects)
    def OnCardRfIDSet( self, event ):
        card_container = self.root.cardReaderQueueContainer
        RF_ID_q_object = None
        if self.kart_okuyucu_list_cmb.GetSelection() ==-1:
            style.uyari_message(self,MESSAGES.once_kart_okuyucu_sec,MESSAGES.uyari)
        else:
            selected_card_reader = self.kart_okuyucu_list_cmb.GetClientData(self.kart_okuyucu_list_cmb.GetSelection())
            selected_reader_id   = selected_card_reader.id
            #print selected_reader_id
            count = 0
            while count < 25:
                if not card_container.empty():
                    RF_ID_q_object = card_container.get()
                    card_container.put( RF_ID_q_object )
                    #print '*********'
                    #print RF_ID_q_object
                    if RF_ID_q_object.has_key( selected_reader_id ):
                        if not RF_ID_q_object[ selected_reader_id ]:
                            style.uyari_message(self,MESSAGES.kart_okuyucu_iletisim_yok,MESSAGES.uyari)
                            break
                        if RF_ID_q_object[ selected_reader_id ].RF_ID:
                            self.kart_seri_no_txt.SetValue( RF_ID_q_object[ selected_reader_id ].RF_ID )
                            break
                        
                        #print RF_ID_q_object[ selected_reader_id ].card_state
                        if RF_ID_q_object[ selected_reader_id ].card_state not in RF_ID_q_object[ selected_reader_id ].card_base_object.idle_list:
                            style.uyari_message(self,MESSAGES.kart_okuyucu_mesgul,MESSAGES.uyari)
                            break
                count +=1
                time.sleep( sabitler.cardReaderSorguSuresi )
    def onTextChange(self,event):
        if self.selected_item:
            sorgula=self.db.get_raw_data_all( table_name="KART_LISTE_VIEW",where="ID_KART='%s'"%self.selected_item,selects="*",dict_mi='object' )
        else:    
            txt_kart_seri_no=self.kart_seri_no_txt.Value
            sorgula=self.db.get_raw_data_all( table_name="KART_LISTE_VIEW",where="KART_SERI_NO='%s'"%txt_kart_seri_no,selects="*",dict_mi='object' )
        if sorgula!=[]:
            
            if sorgula[0].CARI_TIP_ADI==MESSAGES.personel:
                self.personel_radio.SetValue(True)
                self.onPersonelRadio( event )
                cari_index_ = self.cari_listesi_parametric_objects.index(filter(lambda x:  x.id==sorgula[0].ID_CARI_HESAP,self.cari_listesi_parametric_objects)[0])
                self.cari_list_cmb.SetSelection(cari_index_)
                self.onCariChange(event)
                if sorgula[0].PLAKA !='' and sorgula[0].PLAKA != MESSAGES.personel:
                    self.plaka_txt.Enable()
                    self.plaka_check.Enable()
                    self.plaka_txt.SetValue(sorgula[0].PLAKA)
            
            elif sorgula[0].CARI_TIP_ADI==MESSAGES.musteri:
                self.musteri_radio.SetValue(True)
                self.onMusteriRadio( event )
                
                if str(sorgula[0].U_LIMIT)!='':
                    self.ust_limit_txt.SetValue(str(sorgula[0].U_LIMIT))
                cari_index_ = self.cari_listesi_parametric_objects.index(filter(lambda x:  x.id==sorgula[0].ID_CARI_HESAP,self.cari_listesi_parametric_objects)[0])
                self.cari_list_cmb.SetSelection(cari_index_)
                self.onCariChange(event)
                
                plaka_index = self.plaka_parametric_objects.index( filter(lambda x:  x.ad==sorgula[0].PLAKA,self.plaka_parametric_objects)[0])
                self.plaka_list_cmb.SetSelection(plaka_index)
                if sorgula[0].ID_LIMIT_DONEM !='':
                    ust_limit_index_ = self.ust_limit_donem_parametric_objects.index(filter(lambda x:  x.id==sorgula[0].ID_LIMIT_DONEM,self.ust_limit_donem_parametric_objects)[0])
                    self.ust_limit_donem_cmb.SetSelection(ust_limit_index_)
                    self.onLimitChange(event)
                if sorgula[0].ID_LIMIT_CESIT != '' :
                    ust_limit_cesit_index_ = self.ust_limit_cesit_parametric_objects.index(filter(lambda x:  x.id==sorgula[0].ID_LIMIT_CESIT,self.ust_limit_cesit_parametric_objects)[0])
                    self.ust_limit_cesit_cmb.SetSelection(ust_limit_cesit_index_)
                    self.donem_tarih_cmb.SetValue(sorgula[0].H_TARIH)       
                if sorgula[0].ID_ISKONTO !='':
                    isk_index_ = self.iskonto_listesi_parametric_objects.index(filter(lambda x:  x.id==sorgula[0].ID_ISKONTO,self.iskonto_listesi_parametric_objects)[0])
                    self.iskonto_list_cmb.SetSelection(isk_index_)
                if sorgula[0].POMPA_ACABILIR_MI and sorgula[0].POMPA_ACABILIR_MI == 1:
                    self.pompa_ac_check.SetValue( 1 )
            if sorgula[0].KART_ADI:
                self.kart_adi_txt.SetValue(sorgula[0].KART_ADI)
            else:
                self.kart_adi_txt.SetValue('')
            if sorgula[0].KART_SERI_NO:
                self.kart_seri_no_txt.SetValue( sorgula[0].KART_SERI_NO)
        else:
            if not self.selected_item:
                self.Freeze()
                self.personel_radio.SetValue(True)
                self.onPersonelRadio( event )
                self.plaka_txt.Disable()
                self.plaka_txt.SetValue(MESSAGES.personel)
                self.plaka_check.Show()
                self.ust_limit_donem_cmb.Disable()
                self.ust_limit_donem_cmb.SetSelection(-1)
                self.donem_tarih_cmb.Disable()
                self.donem_tarih_cmb.SetSelection(-1)
                self.ust_limit_txt.Disable()
                self.ust_limit_txt.SetValue(MESSAGES.personel)
                self.ust_limit_cesit_cmb.Disable()
                self.ust_limit_cesit_cmb.SetSelection(-1)
                self.iskonto_list_cmb.Disable()
                self.iskonto_list_cmb.SetSelection(-1)
                self.Refresh()
                self.Thaw()
    def OnCardKaydet(self,event):
        self.kart_adi = self.kart_adi_txt.GetValue().strip()
        self.kart_seri=self.kart_seri_no_txt.GetValue()
        donen = 0
        if self.selected_kart:
            var = self.db.get_raw_data_all( table_name="KARTLAR",where="ID_KART=%s"%self.selected_kart,selects="count(ID_KART)" )
            donen= var[0][0]
        yeni_gelen_var = self.db.get_raw_data_all( table_name="KARTLAR",where="KART_SERI_NO='%s'"%self.kart_seri,selects="count(ID_KART)" )
        if yeni_gelen_var[0][0] > 0:
            style.ok_message(self,MESSAGES.sablon_iki_kelime%(MESSAGES.kart, MESSAGES.zaten_var),MESSAGES.sablon_iki_kelime%(MESSAGES.kart, MESSAGES.tanimlari))
        else:
            message=''
                
            if self.kart_adi=="":
                message=MESSAGES.kart_adi_doldur
                
            if self.cari_list_cmb.GetSelection()==-1:
                message+=MESSAGES.cari_sec
            if self.kart_seri=="":
                message+=MESSAGES.kart_okut
            if self.ust_limit_cesit_cmb.GetSelection()==-1:
                ust_limit_cesit='NULL'
            else:
                ust_limit_cesit=self.ust_limit_cesit_cmb.GetClientData(self.ust_limit_cesit_cmb.GetSelection()).id
            if self.ust_limit_donem_cmb.GetSelection()==-1:
                ust_limit_donem='NULL'
            else:
                ust_limit_donem=self.ust_limit_donem_cmb.GetClientData(self.ust_limit_donem_cmb.GetSelection()).id
            pompa_acar = 0
            if self.pompa_ac_check.GetValue():
                pompa_acar = 1
                
            '''
            if self.ust_limit_txt.Disable:
                ust_limit_txt='Personel'
                print ust_limit_txt
            else:
                ust_limit_txt=self.ust_limit_txt.GetValue().strip()
            
            if self.donem_tarih_cmb.Disable==True:
                donem_tarih='Personel'
            else:
                donem_tarih=self.donem_tarih_cmb.GetValue()
            '''
            if self.personel_radio.Value==True:
                ID_CARI_TIP=1
                donem_tarih=MESSAGES.personel
            if self.musteri_radio.Value==True:
                ID_CARI_TIP=2
                donem_tarih=self.donem_tarih_cmb.Value
                
            ust_limit_txt=self.ust_limit_txt.GetValue().strip()
            if self.plaka_txt :
                plaka=self.plaka_txt.GetValue().strip()
            else:
                plaka=self.plaka_list_cmb.GetClientData(self.plaka_list_cmb.GetSelection()).ad
            '''
            if self.plaka_txt.Disable:
                plaka='NULL'
            else:
                plaka=self.plaka_txt.GetValue().strip()
            '''
            
            if message != '' :
                style.uyari_message(self,message,MESSAGES.uyari)
            else:
                if donen !=0:     
                    style.ok_cancel_message(parent=self, message=MESSAGES.sablon_uc_kelime%( MESSAGES.kart, MESSAGES.zaten_var,MESSAGES.guncelle)
                                              ,win_name=MESSAGES.sablon_iki_kelime%(MESSAGES.kart, MESSAGES.tanimlari), ok_function=self.OnCardGuncelle, cancel_function=None)
                else:
                    cari_list_id=self.cari_list_cmb.GetClientData(self.cari_list_cmb.GetSelection()).id
                
                    if self.iskonto_list_cmb.GetSelection()!=-1:
                        iskonto_list_id=self.iskonto_list_cmb.GetClientData(self.iskonto_list_cmb.GetSelection()).id
                    else:
                        iskonto_list_id='NULL'
                    kaydet=self.db.save_data(table_name = 'KARTLAR',
                             column_name='KART_ADI,ID_CARI_HESAP,ID_ISKONTO,KART_SERI_NO,PLAKA,H_DONEMI,H_TARIH,U_LIMIT,LIMIT_TUR,ID_CARI_TIPI,ID_ACTIVE,POMPA_ACABILIR_MI',
                             values="'%s',%s,%s,'%s','%s',%s,'%s','%s',%s,%s,1,%s"%(self.kart_adi,cari_list_id,iskonto_list_id,self.kart_seri,
                                                                            plaka,ust_limit_donem,donem_tarih,ust_limit_txt,
                                                                            ust_limit_cesit,ID_CARI_TIP,pompa_acar))
                    if kaydet:
                        style.ok_message(self,MESSAGES.sablon_iki_kelime%(MESSAGES.kart, MESSAGES.kaydedildi),MESSAGES.sablon_iki_kelime%(MESSAGES.kart, MESSAGES.tanimlari))
                    else:
                        style.uyari_message(self,MESSAGES.sablon_iki_kelime%(MESSAGES.kart, MESSAGES.hata),MESSAGES.sablon_iki_kelime%(MESSAGES.kart, MESSAGES.tanimlari))
                self.selected_kart = None
    def OnCardGuncelle(self):
        if self.personel_radio.Value==True:
            ID_CARI_TIP=1
            donem_tarih=MESSAGES.personel
            plaka=self.plaka_txt.Value
            iskonto_id='NULL'
            ust_limit_donem='NULL'
            ust_limit_cesit='NULL'
            ust_limit_txt='NULL'
            pompa_acar = 1
        if self.musteri_radio.Value==True:
            
            ID_CARI_TIP=2
            plaka=self.plaka_list_cmb.GetClientData(self.plaka_list_cmb.GetSelection()).ad
            try:
                iskonto_id=self.iskonto_list_cmb.GetClientData(self.iskonto_list_cmb.GetSelection()).id
            except:
                iskonto_id = 'NULL'
            try:
                ust_limit_cesit=self.ust_limit_cesit_cmb.GetClientData(self.ust_limit_cesit_cmb.GetSelection()).id
            except:
                ust_limit_cesit= 'NULL'
            try:
                ust_limit_donem=self.ust_limit_donem_cmb.GetClientData(self.ust_limit_donem_cmb.GetSelection()).id
            except:
                ust_limit_donem = 'NULL'
            try:    
                donem_tarih=self.donem_tarih_cmb.Value
            except:
                donem_tarih = 'NULL'
            try:
                ust_limit_txt=self.ust_limit_txt.Value
            except:
                ust_limit_txt = ''
            pompa_acar = 0
            if self.pompa_ac_check.GetValue():
                pompa_acar = 1
           
        kart_seri=self.kart_seri_no_txt.Value
        donen=self.db.update_data( table_name='KARTLAR',
                        column_name='KART_SERI_NO,KART_ADI,ID_CARI_HESAP,ID_ISKONTO,PLAKA,H_DONEMI,H_TARIH,U_LIMIT,LIMIT_TUR,ID_CARI_TIPI,POMPA_ACABILIR_MI',
                        values=("'%s','%s',%s,%s,'%s',%s,'%s',%s,%s,%s,%s"%((self.kart_seri,self.kart_adi,
                                                                       self.cari_list_cmb.GetClientData(self.cari_list_cmb.GetSelection()).id,
                                                                       iskonto_id,
                                                                       plaka,
                                                                       ust_limit_donem,
                                                                       donem_tarih,
                                                                       ust_limit_txt,
                                                                       ust_limit_cesit,ID_CARI_TIP,pompa_acar))),where="ID_KART=%s"%self.selected_kart)
                                                                            
        if donen ==1 :
            style.ok_message(self,MESSAGES.sablon_iki_kelime%(MESSAGES.kart, MESSAGES.guncellendi),MESSAGES.sablon_iki_kelime%(MESSAGES.kart, MESSAGES.tanimlari))
        else:
            style.uyari_message(self,MESSAGES.sablon_iki_kelime%(MESSAGES.kart, MESSAGES.hata),MESSAGES.sablon_iki_kelime%(MESSAGES.kart, MESSAGES.tanimlari))   
class win_Servis_Islem(wx.Frame):
    def __init__(self, parent, id_, title):
       
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR|wx.EXPAND ))
        self.styl=style 
        self.db = param.get_root( self ).DB
        self.O  = param.get_root( self ).O
        self.rapor_panel_id=0
        self.LayoutItems()
        self.where_query = ""
    def LayoutItems(self):
        self.Freeze()
        #self.check_list = []
        mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(mainSizer)
        #self.book = wx.Notebook(self, style=wx.NB_TOP)
        self.book  = aui.AuiNotebook(self,1,agwStyle=(aui.AUI_NB_TOP|aui.AUI_NB_SCROLL_BUTTONS))
        self.book.SetArtProvider(aui.AuiDefaultTabArt())
        style.setPanelListBg(self)
        style.setPanelListBg( self.book )
        mainSizer.Add(self.book, 1,wx.ALL | wx.EXPAND)
        self.p = self.CreateArizaBildir() 
        style.setPanelListBg(self.p)
        self.p2 = self.CreateServisGecmisi()
        style.setPanelListBg(self.p2)
        
        self.book.AddPage( self.p,MESSAGES.ariza_bildir,True )
        self.book.AddPage( self.p2,MESSAGES.servis_gecmisi )
    
        
        il = wx.ImageList(64, 64)
        img0 = il.Add(wx.Bitmap('%sariza_bildir_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        self.img1 = il.Add(wx.Bitmap('%sservis_gecmisi_64.png'%image_path, wx.BITMAP_TYPE_ANY))
        
        self.book.AssignImageList(il)
        self.book.Bind(aui.EVT_AUINOTEBOOK_PAGE_CHANGED, self.OnBookPageChanged)
 
        self.book.SetPageImage(0, img0)
        self.book.SetPageImage(1, self.img1)
        self.Layout()
        self.Thaw()
        self.Show()
    def CreateArizaBildir(self):
        p = wx.Panel(self.book,-1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        
        gbs = wx.GridBagSizer(10, 10)
        
        Aciklama_lbl   = wx.StaticText(p, label=MESSAGES.aciklama)
        self.Aciklama_txt      = wx.TextCtrl  (p, -1, "",size=(280,-1),style= wx.TE_MULTILINE | wx.SUNKEN_BORDER)
        self.styl.setStaticTextList( Aciklama_lbl)
        '''
        bayi_bilgileri=[]
        bayi_bilgileri=self.db.get_raw_data_all(table_name='P_BAYI',selects="BAYI_LISANS_NO,BAYI_POMPA_SAYISI,BAYI_TANK_SAYISI",dict_mi='object')
        print bayi_bilgileri[0].BAYI_LISANS_NO
        print bayi_bilgileri[1].BAYI_POMPA_SAYISI
        print bayi_bilgileri[2].BAYI_TANK_SAYISI
        '''
        lbl_lisans_no   = wx.StaticText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.lisans, MESSAGES.no))
        self.txt_bayi_lisans      = wx.TextCtrl  (p, -1, "",size=(180,-1), style=wx.TE_READONLY)
        style.setStaticTextList( lbl_lisans_no,True)
        
        lbl_bayi_adi   = wx.StaticText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.bayi, MESSAGES.adi))
        self.txt_bayi_adi      = wx.TextCtrl  (p, -1, "",size=(180,-1), style=wx.TE_READONLY)
        style.setStaticTextList( lbl_bayi_adi,True)
        
        lbl_tank_sayisi   = wx.StaticText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.tank, MESSAGES.sayisi))
        self.txt_tank_sayisi      = wx.TextCtrl  (p, -1, "",size=(180,-1), style=wx.TE_READONLY)
        style.setStaticTextList( lbl_tank_sayisi,True)
        
        lbl_pompa_sayisi   = wx.StaticText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.pompa, MESSAGES.sayisi))
        self.txt_pompa_sayisi      = wx.TextCtrl  (p, -1, "",size=(180,-1), style=wx.TE_READONLY)
        style.setStaticTextList( lbl_pompa_sayisi,True)
        
        
        hata_adi_parametric_objects = []
        hata_adi_parametric_objects = self.db.get_parameters_all("P_HATA_CESITLERI",order="ID_HATA",selects="ID_HATA,HATA_ACIKLAMA")
        hata_adi_lbl    = wx.StaticText(p, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.ariza, MESSAGES.turu))
        self.hata_adi_cmb    = wx.ComboBox(p,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        self.styl.setStaticTextList( hata_adi_lbl)
        if hata_adi_parametric_objects != []:
         self.db.widgetMaker( self.hata_adi_cmb , hata_adi_parametric_objects)
        mevcut_bayi  = param.get_bayi_object()
        if mevcut_bayi:
            self.bayi_id=mevcut_bayi.ID_BAYI
            self.txt_bayi_adi.SetValue(  mevcut_bayi.BAYI_ADI )
            self.txt_bayi_lisans.SetValue(  mevcut_bayi.BAYI_LISANS_NO )   
            self.txt_tank_sayisi.SetValue(  str(mevcut_bayi.BAYI_TANK_SAYISI) )
            self.txt_pompa_sayisi.SetValue(  str(mevcut_bayi.BAYI_POMPA_SAYISI) )
        else:
            self.txt_bayi_adi.SetValue(     MESSAGES.bayi_tanimli_degil )
            self.txt_bayi_lisans.SetValue(  MESSAGES.bayi_tanimli_degil )   
            self.txt_tank_sayisi.SetValue(  MESSAGES.bayi_tanimli_degil )
            self.txt_pompa_sayisi.SetValue( MESSAGES.bayi_tanimli_degil)
            
        self.btn_servis_save_pressed  =  wx.Image("%ssave3.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%ssave2.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_servis_save  = LibButtons.ThemedGenBitmapTextButton(p, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%( MESSAGES.ariza,MESSAGES.kaydet))
        self.btn_servis_save.Bind(wx.EVT_BUTTON, self.onServisKaydet)
        self.btn_servis_save.SetToolTipString(MESSAGES.sablon_iki_kelime%( MESSAGES.ariza,MESSAGES.kaydet))
        self.btn_servis_save.SetBitmapSelected(self.btn_servis_save_pressed)
        style.setButtonFont( self.btn_servis_save)
            
        gbs.Add( hata_adi_lbl, (1,1),(1,1),  wx.ALL,3)
        gbs.Add( self.hata_adi_cmb,(1,2) ,(1,1),  wx.ALL,3)
        
        gbs.Add( Aciklama_lbl, (2,1),(1,1),  wx.EXPAND,3)
        gbs.Add( self.Aciklama_txt, (2,2) ,(2,1),  wx.EXPAND,3)
        
        gbs.Add( lbl_bayi_adi, (5,1),(1,1),  wx.ALL,3)
        gbs.Add( self.txt_bayi_adi,(5,2) ,(1,1),  wx.ALL,3)
        
        gbs.Add( lbl_lisans_no, (6,1),(1,1),  wx.ALL,3)
        gbs.Add( self.txt_bayi_lisans,(6,2) ,(1,1),  wx.ALL,3)
        
        gbs.Add( lbl_pompa_sayisi, (7,1),(1,1),  wx.ALL,3)
        gbs.Add( self.txt_pompa_sayisi,(7,2) ,(1,1),  wx.ALL,3)
        
        gbs.Add( lbl_tank_sayisi, (8,1),(1,1),  wx.ALL,3)
        gbs.Add( self.txt_tank_sayisi,(8,2) ,(1,1),  wx.ALL,3)
        
        gbs.Add(self.btn_servis_save,  (9,2),(1,1),wx.ALL,1)
        
        p.SetSizerAndFit(gbs)
        p.SetAutoLayout(True)
        
        return p
    def onServisKaydet(self,event):
        if self.txt_bayi_adi.GetValue()== MESSAGES.bayi_tanimli_degil or self.txt_bayi_lisans.GetValue()==MESSAGES.bayi_tanimli_degil:
            style.ok_message(self,MESSAGES.once_bayi_tanimla,MESSAGES.hata)
        else:
            if self.hata_adi_cmb.GetSelection()==-1:
                style.ok_message(self,MESSAGES.once_ariza_durumu_sec,MESSAGES.hata)
            else:
                tarih=wx.DateTime.Now()
                tank_sayisi=self.txt_tank_sayisi.GetValue().strip()
                pompa_sayisi=self.txt_pompa_sayisi.GetValue().strip()
                aciklama=self.Aciklama_txt.GetValue().strip()
                hata_tipi_id=self.hata_adi_cmb.GetClientData(self.hata_adi_cmb.GetSelection()).id
                sonuc=self.db.save_data( table_name="SERVIS" ,
                column_name="ID_HATA,ID_DURUM,ID_BAYI,TANK_SAYISI,POMPA_SAYISI,ACIKLAMA,TALEP_TARIHI",
                values="'%s','%s','%s','%s','%s','%s',%s"%(hata_tipi_id,1,self.bayi_id,tank_sayisi,pompa_sayisi,aciklama,"DateTime('now','localtime')") )
                if sonuc==1:
                    style.ok_message(self,MESSAGES.sablon_iki_kelime%( MESSAGES.servis, MESSAGES.kaydedildi),MESSAGES.basarili)
                else:
                    style.ok_message(self,MESSAGES.sablon_iki_kelime%( MESSAGES.servis, MESSAGES.hata),MESSAGES.hata)
    def CreateServisGecmisi(self):
        self.p_servis_list = wx.Panel(self.book,1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        
        p_select = wx.Panel(self.p_servis_list,1, style = wx.TAB_TRAVERSAL
                     | wx.CLIP_CHILDREN
                     | wx.FULL_REPAINT_ON_RESIZE)
        bs_select = wx.GridSizer(0,14)
        
        self.bs = wx.BoxSizer(wx.VERTICAL)
        self.p_servis_list.SetSizer( self.bs )
        
        p_select.SetSizerAndFit(bs_select)
        
        lbl_tarih_turu    = style.TransparentText(p_select, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.tarih, MESSAGES.turu))
        choices           = map(lambda x:x[0],sabitler.servis_cesitleri.itervalues())
        self.cmb_tarih_turu    = wx.ComboBox(p_select,choices=choices, style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        #self.cmb_tarih_turu.Bind( wx.EVT_COMBOBOX,self.print_ch)
        style.setStaticTextList(lbl_tarih_turu)
        self.cmb_tarih_turu.SetSelection(0)
        
        hata_adi_parametric_objects = []
        hata_adi_parametric_objects = self.db.get_parameters_all("P_HATA_CESITLERI",order="ID_HATA",selects="ID_HATA,HATA_ACIKLAMA")
        lbl_hata_turu    = style.TransparentText(p_select, label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.ariza, MESSAGES.turu))
        self.cmb_hata_turu    = wx.ComboBox(p_select,choices=[], style=wx.CB_DROPDOWN|wx.CB_READONLY|wx.EXPAND)
        #self.cmb_hata_turu.Bind( wx.EVT_COMBOBOX,self.print_ch)
        style.setStaticTextList(lbl_hata_turu)
        if hata_adi_parametric_objects != []:
            a=dataObj()
            a.id=-1
            a.ad=MESSAGES.tumu
            hata_adi_parametric_objects.append(a)
            self.db.widgetMaker( self.cmb_hata_turu , hata_adi_parametric_objects)
                        
        lbl_dpc_tarih = style.TransparentText(p_select,label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.baslangic, MESSAGES.tarihi))
        style.setStaticTextList(lbl_dpc_tarih)
        self.dpc_tarih = wx.DatePickerCtrl(p_select, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )
        lbl_dpc_bit_tarih = style.TransparentText(p_select,label=MESSAGES.sablon_iki_kelime_dot%( MESSAGES.bitis, MESSAGES.tarihi))
        style.setStaticTextList(lbl_dpc_bit_tarih)
        self.dpc_bitis_tarih = wx.DatePickerCtrl(p_select, size=(100,-1),
                                       style = wx.TAB_TRAVERSAL
                                       | wx.DP_DROPDOWN
                                       | wx.DP_SHOWCENTURY
                                       )
        self.btn_search_pressed  =  wx.Image("%sdb_search_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sdb_search_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_search  = LibButtons.ThemedGenBitmapTextButton(p_select, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%(MESSAGES.ariza,MESSAGES.rapor))
        self.btn_search.Bind(wx.EVT_BUTTON, self.RaporuHazirla)
        self.btn_search.SetToolTipString(MESSAGES.rapor)
        self.btn_search.SetBitmapSelected(self.btn_search_pressed)
        style.setButtonFont( self.btn_search)
        
        self.btn_print_preview_pressed  =  wx.Image("%sdb_search_pressed_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        image_save = wx.Image("%sdb_search_32.png"%image_path, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.btn_print_preview  = LibButtons.ThemedGenBitmapTextButton(p_select, id=wx.NewId(), bitmap=image_save,label=MESSAGES.sablon_iki_kelime%(MESSAGES.yazdir,MESSAGES.onizleme))
        self.btn_print_preview.Bind(wx.EVT_BUTTON, self.OnPrintPreview)
        self.btn_print_preview.SetToolTipString(MESSAGES.sablon_iki_kelime%(MESSAGES.yazdir,MESSAGES.onizleme))
        self.btn_print_preview.SetBitmapSelected(self.btn_print_preview_pressed)
        style.setButtonFont( self.btn_print_preview)
        
        
        bs_select.AddMany( [(lbl_hata_turu,0,wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.cmb_hata_turu,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((5,40)),
                        (lbl_tarih_turu ,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.cmb_tarih_turu,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((5,5)),
                        (lbl_dpc_tarih,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        (self.dpc_tarih,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        (lbl_dpc_bit_tarih,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ), 
                        (self.dpc_bitis_tarih,0, wx.ALIGN_LEFT|wx.ALIGN_CENTER_VERTICAL ),
                        ((5,5)),
                        (self.btn_search,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL ),
                        ((5,5)),
                        (self.btn_print_preview,0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL )
                        ])
        
        
        
        headers        = ['ID,digit',
                          '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.ariza, MESSAGES.turu)),
                          '%s,string'%MESSAGES.aciklama,
                          '%s,string'%MESSAGES.durum,
                          '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.lisans, MESSAGES.no)),
                          '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tank, MESSAGES.sayisi)),
                          '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.pompa, MESSAGES.sayisi)),
                          '%s,datetime'%(MESSAGES.sablon_iki_kelime%( MESSAGES.olusturma, MESSAGES.tarihi)),
                          '%s,datetime'%(MESSAGES.sablon_iki_kelime%( MESSAGES.iletim, MESSAGES.tarihi)),
                          '%s,datetime'%(MESSAGES.sablon_iki_kelime%( MESSAGES.islem, MESSAGES.tarihi)),
                          '%s,datetime'%(MESSAGES.sablon_iki_kelime%( MESSAGES.servise_bildirim, MESSAGES.tarihi)),
                          '%s,datetime'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tamamlanma, MESSAGES.tarihi))]
        
        data_history   = self.db.get_raw_data_all("SERVIS_VIEW" ,order = "ID_SERVIS",selects="ID_SERVIS,HATA_ACIKLAMA,ACIKLAMA,DURUM_ACIKLAMA,BAYI_LISANS_NO,BAYI_TANK_SAYISI,BAYI_POMPA_SAYISI,TALEP_TARIHI,MERKEZE_ILETIM_TARIHI,ISLEME_ALINMA_TARIHI,SERVISE_BILDIRIM_TARIHI,TAMAMLANMA_TARIHI")  
        selects="ID_SERVIS,HATA_ACIKLAMA,ACIKLAMA,DURUM_ACIKLAMA,BAYI_LISANS_NO,BAYI_TANK_SAYISI,BAYI_POMPA_SAYISI,TALEP_TARIHI,MERKEZE_ILETIM_TARIHI,ISLEME_ALINMA_TARIHI,SERVISE_BILDIRIM_TARIHI,TAMAMLANMA_TARIHI"
        tarih=self.dpc_tarih.GetValue()
        self.format_tarih=tarih.Format('%Y-%m-%d')
        where_query="strftime('%%Y-%%m-%%d', %s)='%s'"%(sabitler.servis_cesitleri[self.cmb_tarih_turu.GetSelection()][1],self.format_tarih)
        #self.p_list = IndexListPanel( p, headers=headers, data = data_history)
        
        self.bs.Add(p_select,1,wx.EXPAND)
        PaginatingIndexPanel( self.p_servis_list,"SERVIS_VIEW",
                                    selects = selects,
                                    where=where_query,
                                    order = "ID_SERVIS ASC",
                                    headers = headers,
                                    parent_sizer = self.bs,
                                    ekran_orani  = 12)
        
        
        return self.p_servis_list
    def OnPrintPreview(self, event):
        self.printTitle=MESSAGES.sablon_iki_kelime%(MESSAGES.servis, MESSAGES.raporu)
        headers        = ['ID,digit',
                          '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.ariza, MESSAGES.turu)),
                          '%s,string'%MESSAGES.aciklama,
                          '%s,string'%MESSAGES.durum,
                          '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.lisans, MESSAGES.no)),
                          '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tank, MESSAGES.sayisi)),
                          '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.pompa, MESSAGES.sayisi)),
                          '%s,datetime'%(MESSAGES.sablon_iki_kelime%( MESSAGES.olusturma, MESSAGES.tarihi)),
                          '%s,datetime'%(MESSAGES.sablon_iki_kelime%( MESSAGES.iletim, MESSAGES.tarihi)),
                          '%s,datetime'%(MESSAGES.sablon_iki_kelime%( MESSAGES.islem, MESSAGES.tarihi)),
                          '%s,datetime'%(MESSAGES.sablon_iki_kelime%( MESSAGES.servise_bildirim, MESSAGES.tarihi)),
                          '%s,datetime'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tamamlanma, MESSAGES.tarihi))]
        selects="ID_SERVIS,HATA_ACIKLAMA,ACIKLAMA,DURUM_ACIKLAMA,BAYI_LISANS_NO,BAYI_TANK_SAYISI,BAYI_POMPA_SAYISI,TALEP_TARIHI,MERKEZE_ILETIM_TARIHI,ISLEME_ALINMA_TARIHI,SERVISE_BILDIRIM_TARIHI,TAMAMLANMA_TARIHI"
        
        column_spaces = [0.4,0.6,0.8,1.0,1.0,1.0,1.0,1.0,0.8,0.8,0.8,0.8]
        #print self.where_query
        dataset = self.db.get_raw_data_all(table_name = "SERVIS_VIEW",
                                 where=self.where_query,
                                 order="ID_Servis ASC",
                                 selects=selects)
                
        if dataset==[]:
            style.uyari_message(self,MESSAGES.bos_rapor,self.printTitle)
        else:
            PrintTablePreview( self, dataset,headers,column_spaces, self.printTitle )
        '''
    def print_ch( self, event ):
        #print sabitler.servis_cesitleri[self.cmb_tarih_turu.GetSelection()][1]
        print self.cmb_hata_turu.GetClientData(self.cmb_hata_turu.GetSelection()).id
        '''

    def RaporuHazirla(self,event ):
        self.Freeze()
        
        tarih=self.dpc_tarih.GetValue()
        self.format_tarih=tarih.Format('%Y-%m-%d')
        self.bitis_tarih=self.dpc_bitis_tarih.GetValue().Format('%Y-%m-%d')
        where_query="strftime('%%Y-%%m-%%d', %s) BETWEEN '%s' and '%s' "%(sabitler.servis_cesitleri[self.cmb_tarih_turu.GetSelection()][1],self.format_tarih,self.bitis_tarih)
        if self.p_servis_list.rapor_panel_id != 0:
            wx.FindWindowById(self.p_servis_list.rapor_panel_id ).Destroy()
            
        headers        = ['ID,digit',
                          '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.ariza, MESSAGES.turu)),
                          '%s,string'%MESSAGES.aciklama,
                          '%s,string'%MESSAGES.durum,
                          '%s,string'%(MESSAGES.sablon_iki_kelime%( MESSAGES.lisans, MESSAGES.no)),
                          '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tank, MESSAGES.sayisi)),
                          '%s,digit'%(MESSAGES.sablon_iki_kelime%( MESSAGES.pompa, MESSAGES.sayisi)),
                          '%s,datetime'%(MESSAGES.sablon_iki_kelime%( MESSAGES.olusturma, MESSAGES.tarihi)),
                          '%s,datetime'%(MESSAGES.sablon_iki_kelime%( MESSAGES.iletim, MESSAGES.tarihi)),
                          '%s,datetime'%(MESSAGES.sablon_iki_kelime%( MESSAGES.islem, MESSAGES.tarihi)),
                          '%s,datetime'%(MESSAGES.sablon_iki_kelime%( MESSAGES.servise_bildirim, MESSAGES.tarihi)),
                          '%s,datetime'%(MESSAGES.sablon_iki_kelime%( MESSAGES.tamamlanma, MESSAGES.tarihi))]
        selects="ID_SERVIS,HATA_ACIKLAMA,ACIKLAMA,DURUM_ACIKLAMA,BAYI_LISANS_NO,BAYI_TANK_SAYISI,BAYI_POMPA_SAYISI,TALEP_TARIHI,MERKEZE_ILETIM_TARIHI,ISLEME_ALINMA_TARIHI,SERVISE_BILDIRIM_TARIHI,TAMAMLANMA_TARIHI"

        if self.cmb_hata_turu.GetSelection() != -1 and self.cmb_hata_turu.Value != MESSAGES.tumu:
            where_query=where_query + " and ID_HATA=%s"%(self.cmb_hata_turu.GetClientData(self.cmb_hata_turu.GetSelection()).id)
            #where_query=where_query+ " and HATA_ACIKLAMA='%s'"%self.cmb_hata_turu.Value
        #print where_query
        self.print_select=selects
        self.where_query=where_query
        self.print_headers=headers
        PaginatingIndexPanel( self.p_servis_list,"SERVIS_VIEW",
                                    selects = selects,
                                    where=where_query,
                                    order = "ID_SERVIS ASC",
                                    headers = headers,
                                    parent_sizer = self.bs,
                                    ekran_orani  = 12)
        
        
        self.Layout()
        self.Thaw()
    def OnBookPageChanged( self, event ):
        if self.book.GetSelection() == 1:
            self.book.Unbind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED)
            self.book.Freeze()
            self.book.SetSelection(0)
            p_ = self.CreateServisGecmisi()
            self.book.RemovePage(1)
            self.book.InsertPage( 1,p_,MESSAGES.servis_gecmisi)
            self.book.SetPageImage(1, self.img1)
            self.book.SetSelection(1)
            self.book.Bind( aui.EVT_AUINOTEBOOK_PAGE_CHANGED ,self.OnBookPageChanged)
            self.book.Thaw()   
class win_Web_View( wx.Frame ):
    def __init__(self, parent, id_, title):
        
        wx.Frame.__init__(self, parent,id_, title=title,
                          style= (  wx.DEFAULT_FRAME_STYLE| wx.FRAME_NO_TASKBAR|wx.EXPAND ))

        self.styl=style 
        self.db = param.get_root( self ).DB
        self.O  = param.get_root( self ).O
        
        prefix       = "OtoDeskSync"
        
        self.current = sabitler.web_soap_url.split( prefix )[0]
        
        self.LayoutItems()
    def LayoutItems(self):
            sizer = wx.BoxSizer(wx.VERTICAL)
            #btnSizer = wx.BoxSizer(wx.HORIZONTAL)
            self.wv = webview.WebView.New(self)
            self.Bind(webview.EVT_WEBVIEW_NAVIGATING, self.OnWebViewNavigating, self.wv)
            self.Bind(webview.EVT_WEBVIEW_LOADED, self.OnWebViewLoaded, self.wv)
        
            '''     
            btn = wx.Button(self, -1, "Git", style=wx.BU_EXACTFIT)
            self.Bind(wx.EVT_BUTTON, self.OnOpenButton, btn)
            btnSizer.Add(btn, 0, wx.EXPAND|wx.ALL, 2)
    
            btn = wx.Button(self, -1, "<--", style=wx.BU_EXACTFIT)
            self.Bind(wx.EVT_BUTTON, self.OnPrevPageButton, btn)
            btnSizer.Add(btn, 0, wx.EXPAND|wx.ALL, 2)
            self.Bind(wx.EVT_UPDATE_UI, self.OnCheckCanGoBack, btn)

            btn = wx.Button(self, -1, "-->", style=wx.BU_EXACTFIT)
            self.Bind(wx.EVT_BUTTON, self.OnNextPageButton, btn)
            btnSizer.Add(btn, 0, wx.EXPAND|wx.ALL, 2)
            self.Bind(wx.EVT_UPDATE_UI, self.OnCheckCanGoForward, btn)

            btn = wx.Button(self, -1, "Dur", style=wx.BU_EXACTFIT)
            self.Bind(wx.EVT_BUTTON, self.OnStopButton, btn)
            btnSizer.Add(btn, 0, wx.EXPAND|wx.ALL, 2)

            btn = wx.Button(self, -1, "Yenile", style=wx.BU_EXACTFIT)
            self.Bind(wx.EVT_BUTTON, self.OnRefreshPageButton, btn)
            btnSizer.Add(btn, 0, wx.EXPAND|wx.ALL, 2)

            txt = wx.StaticText(self, -1, "Adres:")
            btnSizer.Add(txt, 0, wx.CENTER|wx.ALL, 2)
    
            self.location = wx.ComboBox(
                self, -1, "", style=wx.CB_DROPDOWN|wx.TE_PROCESS_ENTER)
            self.location.AppendItems(['http://localhost:8000',
                                   'http://wxwidgets.org',
                                   'http://google.com'])
            self.Bind(wx.EVT_COMBOBOX, self.OnLocationSelect, self.location)
            self.location.Bind(wx.EVT_TEXT_ENTER, self.OnLocationEnter)
            btnSizer.Add(self.location, 1, wx.EXPAND|wx.ALL, 2)


            sizer.Add(btnSizer, 0, wx.EXPAND)
            '''
            sizer.Add(self.wv, 1, wx.EXPAND)
            self.SetSizer(sizer)
            #print self.current
            self.wv.LoadURL(self.current)
            
            self.Layout()
            
            self.Show()


    # WebView events
    def OnWebViewNavigating(self, evt):
        # this event happens prior to trying to get a resource
        if evt.GetURL() == 'http://www.microsoft.com/':
            if wx.MessageBox("Are you sure you want to visit Microsoft?",
                             style=wx.YES_NO|wx.ICON_QUESTION) == wx.NO:
                # This is how you can cancel loading a page.
                evt.Veto()

    def OnWebViewLoaded(self, evt):
        # The full document has loaded
        self.current = evt.GetURL()
        #self.wv.LoadURL(self.current)
        #self.location.SetValue(self.current)
        

    # Control bar events
    def OnLocationSelect(self, evt):
        url = self.location.GetStringSelection()
        #self.wv.LoadURL(url)

    def OnLocationEnter(self, evt):
        url = self.location.GetValue()
        self.location.Append(url)
        self.wv.LoadURL(url)


    def OnOpenButton(self, event):
        dlg = wx.TextEntryDialog(self, "Open Location",
                                "Enter a full URL or local path",
                                self.current, wx.OK|wx.CANCEL)
        dlg.CentreOnParent()

        if dlg.ShowModal() == wx.ID_OK:
            self.current = dlg.GetValue()
            self.wv.LoadURL(self.current)

        dlg.Destroy()

    def OnPrevPageButton(self, event):
        self.wv.GoBack()

    def OnNextPageButton(self, event):
        self.wv.GoForward()

    def OnCheckCanGoBack(self, event):
        event.Enable(self.wv.CanGoBack())
        
    def OnCheckCanGoForward(self, event):
        event.Enable(self.wv.CanGoForward())

    def OnStopButton(self, evt):
        self.wv.Stop()

    def OnRefreshPageButton(self, evt):
        self.wv.Reload()