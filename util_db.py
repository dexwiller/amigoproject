#!/usr/bin/env python
# -*- coding: latin5 -*-

from datetime import datetime, timedelta
import time
from collections import defaultdict
from sabitler import HATA_ACIKLAMA, ARABIRIM_ID_MARKA,ODEME_SEKILLERI, dummy, acilis_dolum_farki, TANK_H_ACIKLAMA,KART_OKUYUCU_MARKA,satis_sonu_odeme_methodu_sor, param_threads, cardReaderBeklemeSuresi
from util import getCRC16,is_odd,QClear,get_by_mod,get_mod_by_value, get_decode_hex_by_2
from DBConnectLite import DBObjects
from sabitler import TANK_H_ACIKLAMA
from local import MESSAGES
import sys
class Dummy():
    pass
def gun_sonu_rapor_ayarla(db, stop_date ):
    
    
    
    cur_date         = stop_date
    temp_st_date     = cur_date - timedelta ( days = 1) 
    cur_date_str     = cur_date.strftime('%Y-%m-%d')
    temp_st_date_str = temp_st_date.strftime('%Y-%m-%d')
    
    
    select = '''
            *, ( SATIS_KALAN - BASLANGIC_YAKIT ) as GUN_SONU_FARKI '''
    table = '''
            (
               select  *,
               (OKUNAN_YAKIT    - (BASLANGIC_YAKIT + TOTAL_DOLUM)  ) as PROBE_KALAN,
               ((BASLANGIC_YAKIT + TOTAL_DOLUM)     - TOTAL_SATIS  ) as SATIS_KALAN
               from
               (
                   select t1.DOLUM as TOTAL_DOLUM ,t1.SATIS as TOTAL_SATIS,t2.*,
                      (CASE WHEN t3.OKUNAN_YAKIT is NULL THEN 0 ELSE t3.OKUNAN_YAKIT END ) as BASLANGIC_YAKIT
            
            
            
                   from [VIEW_TANK_HRKT_RAPOR] t2,
                        (SELECT ID_YAKIT_TANKI,TARIH,sum(TANK_DOLUMU) as DOLUM,sum(POMPA_SATISI) as SATIS
                         FROM [VIEW_TANK_HRKT_RAPOR] 
                         WHERE TARIH >= '%s' and TARIH <= '%s'
                   GROUP BY ID_YAKIT_TANKI) t1 
                   LEFT OUTER JOIN 
                      (
                        SELECT OKUNAN_YAKIT from VIEW_TANK_HRKT_RAPOR WHERE TARIH = 
                          (SELECT MAX(TARIH) from VIEW_TANK_HRKT_RAPOR WHERE  TARIH < '%s')
                       ) t3 
                   where t1.TARIH = t2.TARIH )
            
            )
            '''%( temp_st_date_str, cur_date_str, temp_st_date_str )
        
    donen_gunsonu_by_tank = db.get_raw_data_all( table_name = table, selects=select, dict_mi='dict' )
    for r in donen_gunsonu_by_tank:
        column_names='''
            ID_TANK_KAYITLARI,
            TOTAL_DOLUM,
            TOTAL_SATIS,
            ID_YAKIT_TANKI,
            YAKIT_TANKI_ADI,
            TARIH,
            YAKIT_ADI,
            OKUNAN_YAKIT,
            OKUNAN_SU,
            POMPA_SATISI,
            TANK_DOLUMU,
            ID_THC,
            H_ACIKLAMA,
            DURUM,
            DOLUM_TIPI,
            BASLANGIC_YAKIT,
            PROBE_KALAN,
            SATIS_KALAN,
            GUN_SONU_FARKI,
            GUN_SONU
          '''
        
        values = '''%(ID_TANK_KAYITLARI)s,
                    %(TOTAL_DOLUM)s,
                    %(TOTAL_SATIS)s,
                    %(ID_YAKIT_TANKI)s,
                    '%(YAKIT_TANKI_ADI)s',
                    '%(TARIH)s',
                    '%(YAKIT_ADI)s',
                     %(OKUNAN_YAKIT)s,
                     %(OKUNAN_SU)s,
                     %(POMPA_SATISI)s,
                     %(TANK_DOLUMU)s,
                     %(ID_THC)s,
                     '%(H_ACIKLAMA)s',0,
                     '%(DOLUM_TIPI)s',
                     %(BASLANGIC_YAKIT)s,
                     %(PROBE_KALAN)s,
                     %(SATIS_KALAN)s,
                     %(GUN_SONU_FARKI)s,
                     '%(GUN_SONU)s'
                 '''%r 
        db.save_data( table_name='GUNSONU_RAPORLARI',column_name=column_names, values=values )
        
def gun_sonu_rapor_ayarla_son_30_gun(db):
    
    
    
    cur_date         = datetime.now() - timedelta ( days = 1)
    
    delta = timedelta ( days = 30 )
    
    temp_st_date     = cur_date - delta
    
    for i in range( delta.days ):
        cur_date = temp_st_date + timedelta( days=1 )
        print  temp_st_date,cur_date
    
        cur_date_str     = cur_date.strftime('%Y-%m-%d')
        temp_st_date_str = temp_st_date.strftime('%Y-%m-%d')
        
        select = '''
            *, ( SATIS_KALAN - BASLANGIC_YAKIT ) as GUN_SONU_FARKI '''
        table = '''
            (
               select  *,
               (OKUNAN_YAKIT    - BASLANGIC_YAKIT + TOTAL_DOLUM  ) as PROBE_KALAN,
               (BASLANGIC_YAKIT + TOTAL_DOLUM     - TOTAL_SATIS  ) as SATIS_KALAN
               from
               (
                   select t1.DOLUM as TOTAL_DOLUM ,t1.SATIS as TOTAL_SATIS,t2.*,
                      (CASE WHEN t3.OKUNAN_YAKIT is NULL THEN 0 ELSE t3.OKUNAN_YAKIT END ) as BASLANGIC_YAKIT
            
            
            
                   from [VIEW_TANK_HRKT_RAPOR] t2,
                        (SELECT ID_YAKIT_TANKI,TARIH,sum(TANK_DOLUMU) as DOLUM,sum(POMPA_SATISI) as SATIS
                         FROM [VIEW_TANK_HRKT_RAPOR] 
                         WHERE TARIH >= '%s' and TARIH <= '%s'
                   GROUP BY ID_YAKIT_TANKI) t1 
                   LEFT OUTER JOIN 
                      (
                        SELECT OKUNAN_YAKIT from VIEW_TANK_HRKT_RAPOR WHERE TARIH = 
                          (SELECT MAX(TARIH) from VIEW_TANK_HRKT_RAPOR WHERE  TARIH < '%s')
                       ) t3 
                   where t1.TARIH = t2.TARIH )
            
            )
            '''%( temp_st_date_str, cur_date_str, temp_st_date_str )
            
        donen_gunsonu_by_tank = db.get_raw_data_all( table_name = table, selects=select, dict_mi='dict' )
        for r in donen_gunsonu_by_tank:
            column_names='''
                ID_TANK_KAYITLARI,
                TOTAL_DOLUM,
                TOTAL_SATIS,
                ID_YAKIT_TANKI,
                YAKIT_TANKI_ADI,
                TARIH,
                YAKIT_ADI,
                OKUNAN_YAKIT,
                OKUNAN_SU,
                POMPA_SATISI,
                TANK_DOLUMU,
                ID_THC,
                H_ACIKLAMA,
                DURUM,
                DOLUM_TIPI,
                BASLANGIC_YAKIT,
                PROBE_KALAN,
                SATIS_KALAN,
                GUN_SONU_FARKI,
                GUN_SONU
              '''
            
            values = '''%(ID_TANK_KAYITLARI)s,
                        %(TOTAL_DOLUM)s,
                        %(TOTAL_SATIS)s,
                        %(ID_YAKIT_TANKI)s,
                        '%(YAKIT_TANKI_ADI)s',
                        '%(TARIH)s',
                        '%(YAKIT_ADI)s',
                         %(OKUNAN_YAKIT)s,
                         %(OKUNAN_SU)s,
                         %(POMPA_SATISI)s,
                         %(TANK_DOLUMU)s,
                         %(ID_THC)s,
                         '%(H_ACIKLAMA)s',0,
                         '%(DOLUM_TIPI)s',
                         %(BASLANGIC_YAKIT)s,
                         %(PROBE_KALAN)s,
                         %(SATIS_KALAN)s,
                         %(GUN_SONU_FARKI)s,
                         '%(GUN_SONU)s'
                     '''%r 
            db.save_data( table_name='GUNSONU_RAPORLARI',column_name=column_names, values=values )
        
        temp_st_date =  cur_date     
        
def get_thread_params():
    param_abj = Dummy()
    param_abj.POMPA_AKTIF = False
    param_abj.TANK_AKTIF  = False
    param_abj.WS_AKTIF    = False
    param_abj.TANK_PROBESUZ_AKTIF = False
    db = DBObjects()
    pompa = db.get_raw_data_all("P_OTOMASYON_PARAMETRE",selects="PARAMETRE_DEGER",dict_mi="object",where="ID_PARAMETRE=%s"%param_threads.POMPA)[0]
    tank  = db.get_raw_data_all("P_OTOMASYON_PARAMETRE",selects="PARAMETRE_DEGER",dict_mi="object",where="ID_PARAMETRE=%s"%param_threads.TANK)[0]
    ws    = db.get_raw_data_all("P_OTOMASYON_PARAMETRE",selects="PARAMETRE_DEGER",dict_mi="object",where="ID_PARAMETRE=%s"%param_threads.WS)[0]
    tank_probesuz =  db.get_raw_data_all("P_OTOMASYON_PARAMETRE",selects="PARAMETRE_DEGER",dict_mi="object",where="ID_PARAMETRE=%s"%param_threads.TANK_PROBESUZ)[0]
    
    if int(pompa.PARAMETRE_DEGER) == 1:
        param_abj.POMPA_AKTIF = True
    if int(tank.PARAMETRE_DEGER) == 1:
        param_abj.TANK_AKTIF = True
    if int(ws.PARAMETRE_DEGER) == 1:
        param_abj.WS_AKTIF = True
    if int(tank_probesuz.PARAMETRE_DEGER) == 1:
        param_abj.TANK_PROBESUZ_AKTIF = True
    return param_abj
class ariza():
    
    def __init__(self):
        self.bayii                         = None
        self.check_probe_port_ariza        = []
        self.check_probe_ariza             = []
        self.check_arabirim_port_ariza     = []
        self.check_arabirim_iletisim_ariza = []
        self.check_pompa_beyin_ariza       = []
        self.db = DBObjects()
        self.bayi_bilgisi      = self.db.get_raw_data_all("P_BAYI",selects="ID_BAYI,BAYI_TANK_SAYISI,BAYI_POMPA_SAYISI",dict_mi="object")
        if  len(self.bayi_bilgisi)>0:
            self.bayii =  self.bayi_bilgisi[0]
            
    def arizaPortformatla(self,probe_property,probe_port = True):
        try:
            flag_save         = 0
            aciklama          = ''
            check_hata_var_mi = []
            now_minute        = datetime.now().minute
            
            hata_turu         = 1
            if self.bayii:
                if probe_port == True:
                    self.check_probe_port_ariza.append(probe_property.PORT_ADI)
                    ###PROBE_ADRES YAKIT_TANKI_ADI eklenecek##############
                    if now_minute == 0:
                        self.check_probe_port_ariza = []
                    if self.check_probe_port_ariza.count(probe_property.PORT_ADI) == 20:
                        flag_save         = 1
                        message           = HATA_ACIKLAMA.PORT_HATA%probe_property.PORT_ADI
                    
                        #check_hata_var_mi = self.db.get_raw_data_all("SERVIS",selects="*",
                        #                                           where="ACIKLAMA like '%s' AND ID_DURUM !=5"%message)
                        
                        
                    elif self.check_probe_port_ariza.count(probe_property.PORT_ADI) > 20:
                        self.check_probe_port_ariza = filter ( lambda x: x!= probe_property.PORT_ADI, self.check_probe_port_ariza )
                else:
                
                    #arabirim port arizasi olmaktadir
                    arabirim_base_object = probe_property
                    arabirim_marka_adi   = arabirim_base_object.ad
                    arabirim_adres       = arabirim_base_object.arabirim_adres
                    arabirim_pompa_no    = arabirim_base_object.pompa_no
                    self.check_arabirim_port_ariza.append(arabirim_adres)
                    hata_turu = 3
                    if now_minute == 0:
                        self.check_arabirim_port_ariza = []
                  
                    if self.check_arabirim_port_ariza.count(arabirim_adres) == 20:
                
                        flag_save         = 1
                        message           = HATA_ACIKLAMA.PORT_ARABIRIM_HATA%(str(arabirim_marka_adi),str(arabirim_adres),str(arabirim_pompa_no))
                        
                    elif self.check_arabirim_port_ariza.count(arabirim_adres) > 20:
                            self.check_arabirim_port_ariza = filter ( lambda x: x!= arabirim_adres, self.check_arabirim_port_ariza )
    
                if  flag_save == 1:
                    check_hata_var_mi = self.db.get_raw_data_all("SERVIS",selects="*",
                                                                    where="ACIKLAMA like '%s' AND ID_DURUM !=5"%message)
                    if not check_hata_var_mi :
                        insert_hata = self.db.save_data(table_name="SERVIS" ,column_name="ID_HATA,ID_DURUM,ID_BAYI,TANK_SAYISI,POMPA_SAYISI,ACIKLAMA,TALEP_TARIHI",
                                                   values="%s,1,%s,%s,%s,'%s',datetime('now', 'localtime')"%(hata_turu,self.bayii.ID_BAYI,self.bayii.BAYI_TANK_SAYISI,self.bayii.BAYI_POMPA_SAYISI,message))
        except:
            pass
    def arizaProbeformatla(self,probe_property,probe_object):
        try:
            now_minute = datetime.now().minute
            if now_minute == 0:
                self.check_pompa_ariza = []
            
            aciklama = ''
            if probe_object.yakit_mm == 0 and probe_object.su_mm == 0:
                self.check_probe_ariza.append(probe_object.yakit_mm)    
                   
            else:
                check_probe_ariza = []
            if self.bayii:
                if self.check_probe_ariza.count(probe_property.PROBE_ADRES) == 20:
                    aciklama = str(probe_property.PROBE_ADRES)+'%'
                    
                    check_hata_var_mi = self.db.get_raw_data_all("SERVIS",selects="*",
                                                                where="ACIKLAMA like '%s' AND ID_DURUM !=5"%aciklama)
                    if not check_hata_var_mi:
                        message = HATA_ACIKLAMA.PROBE_HATA%probe_property.PROBE_ADRES
                        
                        
                        insert_hata = self.db.save_data(table_name="SERVIS" ,column_name="ID_HATA,ID_DURUM,ID_BAYI,TANK_SAYISI,POMPA_SAYISI,ACIKLAMA,TALEP_TARIHI",
                                                       values="1,1,%s,%s,%s,'%s',datetime('now', 'localtime')"%(self.bayii.ID_BAYI,self.bayii.BAYI_TANK_SAYISI,self.bayii.BAYI_POMPA_SAYISI,message))
                elif self.check_probe_ariza.count(probe_property.PROBE_ADRES) > 20:
                    self.check_probe_ariza = filter (lambda x: x!= probe_property.PROBE_ADRES , self.check_probe_ariza)
        except:
            pass
    def arizaArabirimformatla(self,arabirim_base_object,iletisim_var_mi = True):
        try:
            now_minute   = datetime.now().minute
            aciklama     = ''
            insert_flag = 0
            
            if  self.bayii:            
                if iletisim_var_mi == False:                
                    arabirim_marka_adi   = arabirim_base_object.ad
                    arabirim_adres       = arabirim_base_object.arabirim_adres
                    arabirim_pompa_no    = arabirim_base_object.pompa_no
                    message             = HATA_ACIKLAMA.ARABIRIM_ILETISIM_HATA%(arabirim_marka_adi,arabirim_adres,arabirim_pompa_no)     
                    self.check_arabirim_iletisim_ariza.append(arabirim_adres)
                    
                    if self.check_arabirim_iletisim_ariza.count(arabirim_adres) == 20:
                        insert_flag = 1
                        
                    elif self.check_arabirim_iletisim_ariza.count(arabirim_adres) > 20:
                       self.check_arabirim_iletisim_ariza = filter( lambda x: x!= arabirim_adres ,self.check_arabirim_iletisim_ariza )
                else:                
                    arabirim_pompa_adi = arabirim_base_object.pompa_adi               
                    arabirim_beyin_no = arabirim_base_object.beyin_no                
                    message            = HATA_ACIKLAMA.POMPA_BEYIN_HATA%(arabirim_pompa_adi,arabirim_beyin_no)
                    self.check_pompa_beyin_ariza.append([arabirim_pompa_adi,arabirim_beyin_no])
                    if self.check_pompa_beyin_ariza.count([arabirim_pompa_adi,arabirim_beyin_no]) == 20:
                        insert_flag = 1
                    
                    elif self.check_pompa_beyin_ariza.count([arabirim_pompa_adi,arabirim_beyin_no]) > 20:
                        self.check_pompa_beyin_ariza = filter(lambda x : x != [arabirim_pompa_adi,arabirim_beyin_no] , self.check_pompa_beyin_ariza )
                
                            
                if now_minute == 0:
                    self.check_arabirim_port_ariza = []
                    self.check_pompa_beyin_ariza   = []
                    
                if insert_flag == 1:
                    check_hata_var_mi = self.db.get_raw_data_all("SERVIS",selects="*",
                                                                where="ACIKLAMA like '%s' AND ID_DURUM !=5"%message)
                    if not check_hata_var_mi:
                        insert_hata = self.db.save_data(table_name="SERVIS" ,column_name="ID_HATA,ID_DURUM,ID_BAYI,TANK_SAYISI,POMPA_SAYISI,ACIKLAMA,TALEP_TARIHI",
                                                   values="3,1,%s,%s,%s,'%s',datetime('now', 'localtime')"%(self.bayii.ID_BAYI,self.bayii.BAYI_TANK_SAYISI,self.bayii.BAYI_POMPA_SAYISI,message))
        except:
            pass
class ProbeOperations:
    def __init__(self,db):
        self.db = db
    def getDolumMiktar( self, tank_id, scope, dolum_bitis ):
        dolum_baslangic = self.db.get_raw_data_all(selects='OKUNAN_YAKIT,TARIH', table_name='TANK_KAYITLARI',
                                                   where='TANK_ID = %s AND KAYIT_KODU = %s AND DOLUM_SCOPE = %s'%(tank_id,TANK_H_ACIKLAMA.DOLUM_BASLANGIC,scope ),
                                                   dict_mi = 'object' )
        if len(dolum_baslangic) == 1:
            dolum_baslangic_object = dolum_baslangic[0]
        else:
            return 0
        
        ara_satislar = self.db.get_raw_data_all(selects='sum(POMPA_SATISI)', table_name='TANK_KAYITLARI',
                                                   where='''TANK_ID = %s AND TARIH BETWEEN '%s' AND datetime('now','localtime') '''%(tank_id,
                                                                                                              dolum_baslangic_object.TARIH  ),
                                                    )[0][0]
        if not ara_satislar:
            ara_satislar = 0
        dolum_miktari = float( dolum_bitis ) - float( dolum_baslangic_object.OKUNAN_YAKIT) + float( ara_satislar )
        
        return dolum_miktari
        
    def getDolumScopebyTank( self, tank_id ):
        
        scope       = self.db.get_raw_data_all(selects = ' MAX (DOLUM_SCOPE)',table_name = 'TANK_KAYITLARI', where = 'TANK_ID = %s'%tank_id )
        scope       = scope[0][0]
        if not scope:
            scope = 0
        previous_scope = scope
        scope          = int(scope) + 1
        
        return previous_scope,scope
    
    def GetDolumBul ( self, tank_id ):
        scope_by_tank,next_scope = self.getDolumScopebyTank( tank_id )
        dolum_tipi    = 0
        dolum_kayit   = self.db.get_raw_data_all( table_name='TANK_KAYITLARI', where='DOLUM_SCOPE=%s AND TANK_ID=%s'%(scope_by_tank,tank_id),dict_mi = 'object')
        basla_var = filter( lambda x:x.KAYIT_KODU == TANK_H_ACIKLAMA.DOLUM_BASLANGIC,dolum_kayit)
        if len( basla_var ) > 0:
            dolum_tipi = basla_var[0].MANUAL_DOLUM
            bit_var = filter( lambda x:x.KAYIT_KODU == TANK_H_ACIKLAMA.DOLUM_BITIS,dolum_kayit)
            if len( bit_var ) == 0:
                #baslat var bitis yok, aktif dolum var
                return True, dolum_tipi
        
        return False, dolum_tipi
    
    def save_tank_kayit( self, tank_params_obj,v ,basilcak_yakit = 0,dolum_deger=0, satis_deger = 0,manual_dolum=0,islem_type = -1,irsaliye_id = 'NULL',gun_sonu = 'NULL' ):
        if irsaliye_id == 0:
            irsaliye_id = 'NULL'
        if basilcak_yakit == 0:
            basilcak_yakit = v.yakit_litre
        if islem_type == -1:
            islem_type = v.islem_type[0]
        tarih = "datetime('now', 'localtime')"
        if gun_sonu == 1:#gunsonu
            gun_sonu = 1
            tarih    = datetime.now()
            tarih    = '\'%s-%s-%s 23:59:59\''%(tarih.year,tarih.month,tarih.day)
        if gun_sonu == 2:#gunbasi
            gun_sonu = 2
            tarih    = datetime.now() + timedelta( days = 1 )
            tarih    = '\'%s-%s-%s 00:00:00\''%(tarih.year,tarih.month,tarih.day)
            #print 'save_tank_kayit ', gun_sonu
        column_names    = """
                             TANK_ID,
                             TARIH,
                             ID_YAKIT,
                             OKUNAN_YAKIT,
                             OKUNAN_SU,
                             KAYIT_KODU,
                             DURUM,
                             TANK_DOLUMU,
                             POMPA_SATISI,
                             DOLUM_SCOPE,
                             MANUAL_DOLUM,
                             ID_IRSALIYE,
                             SICAKLIK,
                             SU_ALARM_TEXT,
                             YAKIT_ALARM_TEXT,
                             GUN_SONU
                        """
        
        if float( basilcak_yakit ) != 0:
            values          = """%s,
                                %s,
                                %s,
                                %s,
                                %s,
                                %s,
                                0,
                                %s,
                                %s,
                                %s,
                                %s,
                                %s,
                                %s,
                                '%s',
                                '%s',
                                %s"""%(tank_params_obj.ID_YAKIT_TANKI,
                                       tarih,
                                        tank_params_obj.ID_YAKIT,
                                        basilcak_yakit,
                                        v.su_lt,
                                        islem_type,
                                        dolum_deger,
                                        satis_deger,
                                        v.dolum_scope,
                                        manual_dolum,
                                        irsaliye_id,
                                        v.yakit_sicaklik,
                                        v.su_alarm.text,
                                        v.yakit_alarm.text,
                                        gun_sonu)
            donen = self.db.save_data( "TANK_KAYITLARI",column_names,values  )
class CardRequestFormatla:
    def __init__( self, db=None, main=None ):
        self.pompa_group = {}
        self.db          = db
        self.main        = main
    
    def pooling_request( self, records ):
        
        groups= defaultdict( list )
        for obj in records:
            groups[obj.POMPA_NO].append( obj )
        pompa_list = groups.values()
        
        final_pool_requests = {}
        for adres in pompa_list :
            base             = Dummy()
            sub_pool_request = {}
            if len( adres) > 0:
                r     = adres[0]
                
                request_base           = KART_OKUYUCU_MARKA.card_reader_id_requset_dict[ r.KART_OKUYUCU_MARKA ]
                
                if request_base.AD     == 'best':
                    
                    arrt_dict = dict((name, getattr(request_base, name)) for name in dir(request_base) if  str(getattr(request_base, name)).startswith('%s'))
                    for k,v in arrt_dict.iteritems():
                        v = v % hex(int(r.KART_OKUYUCU_ADRES))[2:]
                        if is_odd ( len( v )) == 1:
                            v = '0' + v
                        setattr ( base, k , v + getCRC16( v, True, 'crc-ccitt-false' ))
                    
                    
                    base.ad                = request_base.AD
                    base.port              = r.KART_OKUYUCU_PORT
                    base.card_reader_adres = r.ARABIRIM_ADRESI
                    base.pompa_no          = r.POMPA_NO
                    base.card_reader_marka = request_base.ID
                    base.status_resp_dict     = request_base.status_resp_dict
                    base.status_resp_dict_org = request_base.status_resp_dict
                    base.idle_list            = request_base.idle_list
                
                if request_base.AD     == 'rfid':
                    
                    arrt_dict = dict((name, getattr(request_base, name)) for name in dir(request_base) if  str(getattr(request_base, name)).startswith('02'))
                    end_byte  = request_base.crc_end
                    for k,v in arrt_dict.iteritems():
                        adres = hex(int(r.KART_OKUYUCU_ADRES))[2:]
                        if is_odd ( len( adres )) == 1:
                            adres = '0' + adres
                        v = v % adres
                        if is_odd ( len( v )) == 1:
                            v = '0' + v
                        if v.find('basenumber') == -1:
                            setattr ( base, k , v + end_byte%getCRC16( v, True, 'xmodem' ))
                        else:
                            v = v.replace( 'basenumber', '%s')
                            setattr (base, k, v )
                    
                    
                    base.ad                = request_base.AD
                    base.port              = r.KART_OKUYUCU_PORT
                    base.card_reader_adres = r.ARABIRIM_ADRESI
                    base.pompa_no          = r.POMPA_NO
                    base.card_reader_marka = request_base.ID
                    base.status_resp_dict     = request_base.status_resp_dict
                    base.status_resp_dict_org = request_base.status_resp_dict
                    base.idle_list            = request_base.idle_list
                    base.card_id_resp         = request_base.card_id_resp
                    base.payment_dict         = request_base.payment_dict
                    base.crc_end              = end_byte
                
            final_pool_requests[ r.POMPA_NO ] = base
    
        return final_pool_requests
    def card_donen_formatla(self, card_donen, ariza_obj, simulation = False ):
        
        card_status = {}
        for pompa_no, donen_torba in card_donen.iteritems():
            if len(donen_torba) == 3:
                donen            = donen_torba[0]
                card_base_obj    = donen_torba[1]
                state            = donen_torba[2]
  
            
                if not simulation:
                    if card_base_obj.card_reader_marka == KART_OKUYUCU_MARKA.MARKALAR.BEST:
                        RF_ID = None
                        kart_object = None
                        #print donen, state
                        if state == '03':
                            RF_ID = ''.join(donen[2:6])
                            kart_object = self.db.getCardOptionsByRfID( RF_ID )
                            
                        elif state == '05':
                            RF_ID = ''.join(donen[2:6])
                            if len( donen ) < 7:
                                raise Exception(' Pespese kart ')
                            kart_object = self.db.getCardOptionsByRfID( RF_ID )
                            

                        status_ojb = dummy()
                        status_ojb.RF_ID            = RF_ID
                        status_ojb.kart_object      = kart_object
                        status_ojb.card_state       = state
                        status_ojb.raw_data         = donen
                        status_ojb.card_base_object = card_base_obj
                        
                    if card_base_obj.card_reader_marka == KART_OKUYUCU_MARKA.MARKALAR.RFREADERV1:
                        RF_ID = None
                        kart_object = None
                        #print donen, state
                        if len (donen) > 3 and donen[3] == '62':
                            final_rf_id_list = donen[ donen.index('62') +1 :  donen.index('03') - 2]
                            RF_ID = get_decode_hex_by_2( final_rf_id_list )
                            kart_object = self.db.getCardOptionsByRfID( RF_ID )
                       
                        status_ojb = dummy()
                        status_ojb.RF_ID            = RF_ID
                        status_ojb.kart_object      = kart_object
                        status_ojb.card_state       = state
                        status_ojb.raw_data         = donen
                        status_ojb.card_base_object = card_base_obj
            else:
                status_ojb = None
                    
            card_status[ pompa_no] =  status_ojb
        
        return card_status 
class PompaRequestFormatla:
    def __init__( self ):
        self.pompa_group = {}
        self.final_endeks_dict = {}
        self.prev_plate_number = ''
        self.simulation_litre    = 0
        self.prev_status         = {}
    def orpak_pir_scu_crc( self,query,scuid,pirid,base,transaction_id = {}, beyin_no = None ):
        
        if not scuid.has_key( base.arabirim_adres ):
            scu_id = '01'
        else:
            scu_id = scuid[base.arabirim_adres ]
        
        
        if not pirid.has_key( base.arabirim_adres ):
            pir_id = '01'
        else:
            pir_id = pirid[base.arabirim_adres ]
        
        if  transaction_id == {}:
            query     = query%(scu_id,pir_id)
            valid_crc = getCRC16( query )
            query = base.start + query + base.crc_end % valid_crc
        else:
            trans_list        = transaction_id[str(base.arabirim_adres) + '_' + beyin_no ]
            trans_list_to_str = ''.join( trans_list )
            query     = query%(scu_id,pir_id, trans_list_to_str )
            valid_crc = getCRC16( query )
            query = base.start + query + base.crc_end % valid_crc
        return query
        
    def pooling_request_by_tabanca( self, records, value='' ):
        final_pool_requests = {}
        
        for r in records :
            
            request_base  = ARABIRIM_ID_MARKA.arabirim_id_requset_dict[ r.ID_ARABIRIM_MARKA ]
             
            
            if request_base.AD == 'teosis':
                r.pool        = request_base.pool%(hex(r.ARABIRIM_ADRESI)[2:] )
                r.pool_end    = request_base.pool_end%(hex(r.ARABIRIM_ADRESI)[2:] )
                
                r.pool_resp   = request_base.pool_resp
                r.ad          = request_base.AD
                
                r.autorize    = request_base.autorize%(hex(r.ARABIRIM_ADRESI)[2:], r.BEYIN_NO, r.TABANCA_NO )
                r.autorize    = r.autorize + request_base.crc_end % getCRC16( r.autorize )
                
                r.pump_st_req = request_base.pump_st_req%( hex(r.ARABIRIM_ADRESI)[2:], r.BEYIN_NO  )
                r.pump_st_req = r.pump_st_req + request_base.crc_end % getCRC16( r.pump_st_req )
                
                r.error            = request_base.error%(hex(r.ARABIRIM_ADRESI)[2:] )
                r.status_resp_dict = request_base.status_resp_dict
                
                r.req_paid   =   request_base.req_paid%(hex(r.ARABIRIM_ADRESI)[2:], r.BEYIN_NO, r.TABANCA_NO )
                r.req_paid   =   r.req_paid + request_base.crc_end % getCRC16( r.req_paid )
                
                r.req_ecr_plate   =   request_base.req_ecr_plate%(hex(r.ARABIRIM_ADRESI)[2:], r.BEYIN_NO, r.TABANCA_NO )
                r.req_ecr_plate   =   r.req_ecr_plate + request_base.crc_end % getCRC16( r.req_ecr_plate )
                
                r.req_eof   =   request_base.req_eof%(hex(r.ARABIRIM_ADRESI)[2:], r.BEYIN_NO, r.TABANCA_NO )
                r.req_eof   =   r.req_eof + request_base.crc_end % getCRC16( r.req_eof )
                
                r.req_unpause   =   request_base.req_unpause%(hex(r.ARABIRIM_ADRESI)[2:], r.BEYIN_NO, r.TABANCA_NO )
                r.req_unpause   =   r.req_unpause + request_base.crc_end % getCRC16( r.req_unpause )
                
                r.req_totalizer   =   request_base.req_totalizer%(hex(r.ARABIRIM_ADRESI)[2:], r.BEYIN_NO, r.TABANCA_NO )
                r.req_totalizer   =   r.req_totalizer + request_base.crc_end % getCRC16( r.req_totalizer )
                value = value.replace('.','')
                if len( value ) < 4:
                    value = value + '0'*( 4 - len( value ) )
                value = '00' + value
                r.req_ppu         = request_base.req_update_ppu%( hex(r.ARABIRIM_ADRESI)[2:] , r.BEYIN_NO, r.TABANCA_NO, value )
                r.req_ppu         = r.req_ppu + request_base.crc_end % getCRC16( r.req_ppu )
                
                r.pump_beyin_no    = '%s'%(r.BEYIN_NO)
            
            if request_base.AD == 'orpak':
                arabirim_adres  = hex(r.ARABIRIM_ADRESI)[2:]
                if len( arabirim_adres ) == 1:
                    arabirim_adres = '0' + arabirim_adres
                r.arabirim_adres = arabirim_adres
                r.pool        = request_base.pool%(arabirim_adres )
                r.pool        = r.pool.replace( 'scumesajpirmesaj','%s%s')
                
                r.pool_end    = request_base.pool_end%(arabirim_adres )
                r.pool_end    = r.pool_end.replace( 'scumesajpirmesaj','%s%s')
                
                r.pool_resp   = request_base.pool_resp
                r.ad          = request_base.AD
                
                r.start       = request_base.start
                r.crc_end  = request_base.crc_end
                
                
                r.autorize         = request_base.autorize%(arabirim_adres, r.BEYIN_NO, r.TABANCA_NO  )
                r.autorize         = r.autorize.replace( 'scumesajpirmesaj','%s%s')
                r.unautorize       = request_base.unautorize%(arabirim_adres, r.BEYIN_NO,  r.TABANCA_NO   )
                r.status_resp_dict = request_base.status_resp_dict
                
                r.req_paid   =   request_base.req_paid%(arabirim_adres, r.BEYIN_NO)
                r.req_paid   =   r.req_paid.replace( 'scumesajpirmesaj','%s%s')
                
                r.req_ecr_plate   =   request_base.req_ecr_plate%(arabirim_adres, r.BEYIN_NO, r.TABANCA_NO  )
                
                
                r.req_eof   =   request_base.req_eof%(arabirim_adres, r.BEYIN_NO, r.TABANCA_NO   )
                r.req_eof   =   r.req_eof.replace( 'scumesajpirmesaj','%s%s')
                
                r.req_clear =   request_base.req_clear%(arabirim_adres, r.BEYIN_NO,  r.TABANCA_NO  )
                r.req_clear =   r.req_clear.replace( 'scumesajpirmesaj','%s%s').replace( 'transactionlist','%s')
                
                r.req_unpause   =   request_base.req_unpause%(arabirim_adres, r.BEYIN_NO,  r.TABANCA_NO   )

                r.req_totalizer   =   request_base.req_totalizer%(arabirim_adres, r.BEYIN_NO,  r.TABANCA_NO   )

                r.req_real_totalizer   =   request_base.req_totalizer%(arabirim_adres, int(r.BEYIN_NO) + 4,  r.TABANCA_NO   )
                r.final_formatter      = self.orpak_pir_scu_crc
                value = value.replace('.','')
                value = '0'+ value
                if len( value ) < 5:
                    value = value + '0'*( 5 - len( value ) )
                
                value = get_mod_by_value( value, 30 )
                
                r.req_ppu         = request_base.req_update_ppu%( arabirim_adres , r.BEYIN_NO, r.TABANCA_NO, value )
                r.req_ppu         = r.req_ppu.replace( 'scumesajpirmesaj','%s%s')
                
                r.pump_beyin_no    = '%s'%(r.BEYIN_NO)
               
            final_pool_requests[ r.ID_TABANCA ] = r
        return final_pool_requests

    def pooling_request_by_arabirim( self,records ):
        groups= defaultdict( list )
        for obj in records:
            groups[obj.ARABIRIM_ADRESI].append( obj )
        pompa_list = groups.values()
        final_pool_requests = {}
        for adres in pompa_list :
            base             = Dummy()
            if len( adres) > 0:
                r     = adres[0]
                request_base        = ARABIRIM_ID_MARKA.arabirim_id_requset_dict[ r.ID_ARABIRIM_MARKA ]
                base.pool           = request_base.pool%(hex(r.ARABIRIM_ADRESI)[2:] )
                base.pool_end       = request_base.pool_end%(hex(r.ARABIRIM_ADRESI)[2:] )
                base.pool_resp      = request_base.pool_resp
                base.ad             = request_base.AD
                base.error          = request_base.error%(hex(r.ARABIRIM_ADRESI)[2:] )
                base.port           = r.ID_PORT
                base.arabirim_adres = r.ARABIRIM_ADRESI
                base.pompa_no       = r.POMPA_NO
                base.arabirim_marka = request_base.ID
                base.crc_end        = request_base.crc_end
                base.req_sale_records  =   request_base.req_sale_records
                base.req_update_time   = request_base.req_update_time
                final_pool_requests [ base ] =   adres[0]
        
        return final_pool_requests
    def pooling_request( self, records  ):
        c                = CardRequestFormatla()
        c_reader_records = filter( lambda x:x.KART_OKUYUCU_ADRES != '' , records)
        card_formatter   = c.pooling_request( c_reader_records )
        groups= defaultdict( list )
        for obj in records:
            groups[obj.POMPA_NO].append( obj )
        pompa_list = groups.values()
        
        final_pool_requests = {}
        for adres in pompa_list :
            
            base             = Dummy()
            sub_pool_request = {}
            if len( adres) > 0:
                r     = adres[0]
                
                request_base        = ARABIRIM_ID_MARKA.arabirim_id_requset_dict[ r.ID_ARABIRIM_MARKA ]
                if request_base.AD == 'teosis':
                    base.pool           = request_base.pool%(hex(r.ARABIRIM_ADRESI)[2:] )
                    base.pool_end       = request_base.pool_end%(hex(r.ARABIRIM_ADRESI)[2:] )
                    base.pool_resp      = request_base.pool_resp
                    base.ad             = request_base.AD
                    base.error          = request_base.error%(hex(r.ARABIRIM_ADRESI)[2:] )
                    base.port           = r.ID_PORT
                    base.arabirim_adres = r.ARABIRIM_ADRESI
                    base.pompa_no       = r.POMPA_NO
                    base.arabirim_marka = request_base.ID
                    base.crc_end        = request_base.crc_end
                    base.req_sale_records  = request_base.req_sale_records
                    base.req_update_time   = request_base.req_update_time
                    base.pump_st_req       = request_base.pump_st_req%( hex(r.ARABIRIM_ADRESI)[2:], r.BEYIN_NO )
                    base.pump_st_req       = base.pump_st_req + request_base.crc_end % getCRC16( base.pump_st_req )
                    #print base.pump_st_req
                    base.card_reader       = None
                    base.call_resp         = request_base.call_resp
                    base.paid_resp         = request_base.paid_resp
                    if r.KART_OKUYUCU_ADRES and r.KART_OKUYUCU_ADRES != '':
                        base.card_reader        = r.POMPA_NO
                        base.card_formatter    = card_formatter[ r.POMPA_NO ]
                    base.pump_beyin_no = str(r.BEYIN_NO)
                    
                    for r in adres:
        
                        r.autorize    = request_base.autorize%(hex(r.ARABIRIM_ADRESI)[2:], r.BEYIN_NO, r.TABANCA_NO )
                        r.autorize    = r.autorize + request_base.crc_end % getCRC16( r.autorize )
                       
                        r.unautorize    = request_base.unautorize%(hex(r.ARABIRIM_ADRESI)[2:], r.BEYIN_NO, r.TABANCA_NO )
                        r.unautorize    = r.unautorize + request_base.crc_end % getCRC16( r.unautorize )
                       
                       
                        r.status_resp_dict = request_base.status_resp_dict
                        
                        r.req_paid   =   request_base.req_paid%(hex(r.ARABIRIM_ADRESI)[2:], r.BEYIN_NO, r.TABANCA_NO )
                        r.req_paid   =   r.req_paid + request_base.crc_end % getCRC16( r.req_paid )
                        
                        r.req_ecr_plate   =   request_base.req_ecr_plate%(hex(r.ARABIRIM_ADRESI)[2:], r.BEYIN_NO, r.TABANCA_NO )
                        r.req_ecr_plate   =   r.req_ecr_plate + request_base.crc_end % getCRC16( r.req_ecr_plate )
                        
                        r.req_eof   =   request_base.req_eof%(hex(r.ARABIRIM_ADRESI)[2:], r.BEYIN_NO, r.TABANCA_NO )
                        r.req_eof   =   r.req_eof + request_base.crc_end % getCRC16( r.req_eof )
                        
                        r.req_unpause   =   request_base.req_unpause%(hex(r.ARABIRIM_ADRESI)[2:], r.BEYIN_NO, r.TABANCA_NO )
                        r.req_unpause   =   r.req_unpause + request_base.crc_end % getCRC16( r.req_unpause )
                        
                        r.req_totalizer   =   request_base.req_totalizer%(hex(r.ARABIRIM_ADRESI)[2:], r.BEYIN_NO, r.TABANCA_NO )
                        r.req_totalizer   =   r.req_totalizer + request_base.crc_end % getCRC16( r.req_totalizer )
                        
                        r.req_real_totalizer   =   request_base.req_totalizer%(hex(r.ARABIRIM_ADRESI)[2:], int(r.BEYIN_NO) + 4, r.TABANCA_NO )
                        r.req_real_totalizer   =   r.req_real_totalizer + request_base.crc_end % getCRC16( r.req_real_totalizer )
                        
                        '''
                        r.req_ppu         = request_base.req_update_ppu%( hex(r.ARABIRIM_ADRESI)[2:] , r.BEYIN_NO, r.TABANCA_NO, value )
                        r.req_ppu         = r.req_ppu + request_base.crc_end % getCRC16( r.req_ppu )
                        '''
                        
                       
                        sub_pool_request[ r.TABANCA_NO ] = r
                    
                if request_base.AD == 'orpak':
                    arabirim_adres  = hex(r.ARABIRIM_ADRESI)[2:]
                    if len( arabirim_adres ) == 1:
                        arabirim_adres = '0' + arabirim_adres
                    base.pool           = request_base.pool%(arabirim_adres )
                    base.pool           = base.pool.replace( 'scumesajpirmesaj','%s%s') 
                    base.pool_end       = request_base.pool_end%(arabirim_adres )
                    base.pool_end       = base.pool_end.replace( 'scumesajpirmesaj','%s%s')
                    base.pool_resp      = request_base.pool_resp
                    base.ad             = request_base.AD
                    base.start          = request_base.start
                    base.error          = request_base.error%(arabirim_adres )
                    base.port           = r.ID_PORT
                    base.arabirim_adres = arabirim_adres
                    base.pompa_no       = r.POMPA_NO
                    base.arabirim_marka = request_base.ID
                    base.crc_end        = request_base.crc_end
                    base.req_sale_records  = request_base.req_sale_records
                    base.req_update_time   = request_base.req_update_time
                    base.pump_st_req       = request_base.pump_st_req%( arabirim_adres, r.BEYIN_NO )
                    base.pump_st_req       = base.pump_st_req.replace( 'scumesajpirmesaj','%s%s')
                    #print base.pump_st_req
                    base.card_reader       = None
                    base.call_resp         = request_base.call_resp
                    base.paid_resp         = request_base.paid_resp
                    base.final_formatter   = self.orpak_pir_scu_crc
                    if r.KART_OKUYUCU_ADRES and r.KART_OKUYUCU_ADRES != '':
                        base.card_reader        = r.POMPA_NO
                        base.card_formatter    = card_formatter[ r.POMPA_NO ]
                    base.pump_beyin_no    = str(r.BEYIN_NO)
                    
                    for r in adres:

                        r.autorize         = request_base.autorize%(arabirim_adres, r.BEYIN_NO, r.TABANCA_NO  )
                        r.autorize         = r.autorize.replace( 'scumesajpirmesaj','%s%s')
                        r.unautorize       = request_base.unautorize%(arabirim_adres, r.BEYIN_NO,  r.TABANCA_NO   )
                        r.status_resp_dict = request_base.status_resp_dict
                        
                        r.req_paid   =   request_base.req_paid%(arabirim_adres, r.BEYIN_NO)
                        r.req_paid   =   r.req_paid.replace( 'scumesajpirmesaj','%s%s')
                        
                        r.req_ecr_plate   =   request_base.req_ecr_plate%(arabirim_adres, r.BEYIN_NO, r.TABANCA_NO  )
                        
                        
                        r.req_eof   =   request_base.req_eof%(arabirim_adres, r.BEYIN_NO, r.TABANCA_NO   )
                        r.req_eof   =   r.req_eof.replace( 'scumesajpirmesaj','%s%s')
                        
                        r.req_clear =   request_base.req_clear%(arabirim_adres, r.BEYIN_NO,  r.TABANCA_NO  )
                        r.req_clear =   r.req_clear.replace( 'scumesajpirmesaj','%s%s').replace( 'transactionlist','%s')
                        
                        r.req_unpause   =   request_base.req_unpause%(arabirim_adres, r.BEYIN_NO,  r.TABANCA_NO   )
                        
                        
                        r.req_totalizer   =   request_base.req_totalizer%(arabirim_adres, r.BEYIN_NO,  r.TABANCA_NO   )
                        
                        
                        r.req_real_totalizer   =   request_base.req_totalizer%(arabirim_adres, int(r.BEYIN_NO) + 4,  r.TABANCA_NO   )
                        
                        
                        '''
                        r.req_ppu         = request_base.req_update_ppu%( arabirim_adres , r.BEYIN_NO, r.TABANCA_NO, value )
                        r.req_ppu         = r.req_ppu + request_base.crc_end % getCRC16( r.req_ppu )
                        '''
            
                       
                        sub_pool_request[ r.TABANCA_NO  ] = r
 
            final_pool_requests[ base ] = sub_pool_request
    
        return final_pool_requests
    
    def arabirim_donen_formatla(self, arabirim_donen ,ariza_obj, simulation = False, card_req_q = None, cari_obj=None, port_ops_object = None ):
        
        final_pump_status = {}
        endeks_dict = {}
        for pompa_no, donen_torba in arabirim_donen.iteritems():
            
            donen        = donen_torba[0]
            if not donen:
                donen = []
            pompa_obj    = donen_torba[1]
            tabanca_obj  = donen_torba[2]
            endeks_donen = donen_torba[3]
            state        = donen_torba[4]
            musteri_cari  = None
            personel_cari = None
            if cari_obj:
                musteri_cari  = cari_obj[0]
                personel_cari = cari_obj[1]
            
            tabanca_id   = tabanca_obj.ID_TABANCA 
  
            #pompa_status = {}
            '''
            if self.pompa_group.has_key( pompa_obj.pompa_no ):
                pompa_status = self.pompa_group[ pompa_obj.pompa_no ]
            '''
            #print endeks_donen
            #print pompa_obj.arabirim_marka
            #final_pump_status = {}
            status = ARABIRIM_ID_MARKA.status_response_dict[ '05' ]
            if not simulation:
                if pompa_obj.arabirim_marka == ARABIRIM_ID_MARKA.TEOSIS:
                    #print donen
                    if len(donen) > 5:
                        #print donen
                        if donen[2] == '80':
                            
                            status = ARABIRIM_ID_MARKA.status_response_dict[ donen[5] ]
                            if self.prev_status.has_key( pompa_no ) and self.prev_status[pompa_no] == 'filling':
                                QClear( card_req_q )
                                if satis_sonu_odeme_methodu_sor:
                                    card_req_q.put( {pompa_no:['final_payment']} )
                                else:
                                    card_req_q.put( {pompa_no:['filling_sum']} )
                            
                            if donen[5]=='00':
            
                                if (port_ops_object.musteri_cari.has_key( pompa_no ) and port_ops_object.musteri_cari[ pompa_no ]):
                                    port_ops_object.musteri_cari[ pompa_no ]  = None
                                if   ( port_ops_object.personel_cari.has_key( pompa_no ) and port_ops_object.personel_cari[ pompa_no ]):    
                                    port_ops_object.personel_cari[ pompa_no ] = None 
                            
                            self.prev_status[ pompa_no ] = None
                        elif donen[2] == '97':
                            status = ARABIRIM_ID_MARKA.status_response_dict[ '09' ]
                            card_req_q.put( {pompa_no:['start_filling']} )
                        elif donen[2] == '83':
                            self.prev_status[ pompa_no ] = 'filling'
                            status = ARABIRIM_ID_MARKA.status_response_dict[ '02' ]
                            card_req_q.put( {pompa_no:['filling_progres']} )
                            endeks_dict  = self.endeksFormatla( tabanca_id, endeks_donen, tabanca_obj)
                            #print donen, tabanca_id
                        elif donen[2] == '82':
                            status = ARABIRIM_ID_MARKA.status_response_dict[ '02' ]
                        else:
                            status = ARABIRIM_ID_MARKA.status_response_dict[ '00' ]
                    else:
                        status = ARABIRIM_ID_MARKA.status_response_dict[ '00' ]
                    
                    status_ojb = dummy()
                    status_ojb.color          = status[1]
                    status_ojb.status         = status[0]
                    status_ojb.yakit_adi      = tabanca_obj.YAKIT_ADI
                    status_ojb.yakit_id       = tabanca_obj.ID_YAKIT
                    status_ojb.pompa_no       = tabanca_obj.POMPA_NO
                    status_ojb.pompa_adi      = tabanca_obj.POMPA_ADI
                    status_ojb.beyin_no       = tabanca_obj.BEYIN_NO
                    status_ojb.tabanca_no     = tabanca_obj.TABANCA_NO
                    status_ojb.yakit_tank_id  = tabanca_obj.ID_YAKIT_TANKI
                    status_ojb.tabanca_id     = tabanca_obj.ID_TABANCA
                    status_ojb.pump_state     = state
                    status_ojb.raw_data       = donen
                elif   pompa_obj.arabirim_marka == ARABIRIM_ID_MARKA.ORPAK:
                    #print donen
                    
                    if len(donen) > 13:
                        """
                        print ''' ******************************** '''
                        print donen
                        print endeks_donen
                        print '-----------------------------------'
                        """
                        if donen[5] == '28':
                            
                            status = ARABIRIM_ID_MARKA.status_response_dict_orpak[ donen[13] ]
                            if donen[13]=='31' and endeks_donen == []:
                                if self.prev_status.has_key( pompa_no ) and self.prev_status[pompa_no] == 'filling':
                                
                                    QClear( card_req_q )
                                    card_req_q.put( {pompa_no:['filling_sum']} )
                                else:
                                    if (port_ops_object.musteri_cari.has_key( pompa_no ) and port_ops_object.musteri_cari[ pompa_no ]):
                                        port_ops_object.musteri_cari[ pompa_no ]  = None
                                    if   ( port_ops_object.personel_cari.has_key( pompa_no ) and port_ops_object.personel_cari[ pompa_no ]):    
                                        port_ops_object.personel_cari[ pompa_no ] = None 
                                        
                            self.prev_status[ pompa_no ] = None
                        elif donen[5] == '22':
                            status = ARABIRIM_ID_MARKA.status_response_dict_orpak[ '32' ]
                            card_req_q.put( {pompa_no:['start_filling']} )
                        elif donen[5] == '26':
                            self.prev_status[ pompa_no ] = 'filling'
                            status = ARABIRIM_ID_MARKA.status_response_dict_orpak[ '32' ]
                            card_req_q.put( {pompa_no:['filling_progres']} )
                            
                            #endeks_dict  = self.endeksFormatla( tabanca_id, endeks_donen, tabanca_obj)
                            #print donen, tabanca_id
                        elif donen[5] == '24':
                            
                            status = ARABIRIM_ID_MARKA.status_response_dict_orpak[ '32' ]
                        else:
                            status = ARABIRIM_ID_MARKA.status_response_dict_orpak[ '37' ]
                        
                    
                    status_ojb = dummy()
                    status_ojb.color          = status[1]
                    status_ojb.status         = status[0]
                    status_ojb.yakit_adi      = tabanca_obj.YAKIT_ADI
                    status_ojb.yakit_id       = tabanca_obj.ID_YAKIT
                    status_ojb.pompa_no       = tabanca_obj.POMPA_NO
                    status_ojb.pompa_adi      = tabanca_obj.POMPA_ADI
                    status_ojb.beyin_no       = tabanca_obj.BEYIN_NO
                    status_ojb.tabanca_no     = tabanca_obj.TABANCA_NO
                    status_ojb.yakit_tank_id  = tabanca_obj.ID_YAKIT_TANKI
                    status_ojb.tabanca_id     = tabanca_obj.ID_TABANCA
                    status_ojb.pump_state     = state
                    status_ojb.raw_data       = donen
                    
                    status_ojb.endeks_donen   = endeks_donen
                    
                    ########## KART OKUYUCUDAN GELMEL�
                    #print 'self.pompa_group = ', self.pompa_group
            else:
                #simulation endeks
                endeks_dict = self.endeksFormatla( tabanca_id, endeks_donen, tabanca_obj, simulation )
                status_ojb = dummy()
                
                if pompa_obj.pompa_no % 3 == 1:
                    status  = ARABIRIM_ID_MARKA.status_response_dict[ '00' ]
                elif pompa_obj.pompa_no %3 == 2:
                    status  = ARABIRIM_ID_MARKA.status_response_dict[ '02' ]
                else:
                    status  = ARABIRIM_ID_MARKA.status_response_dict[ '05' ]
                status_ojb.color          = status[1]
                status_ojb.status         = status[0]
                status_ojb.yakit_adi      = tabanca_obj.YAKIT_ADI
                status_ojb.yakit_id       = tabanca_obj.ID_YAKIT
                status_ojb.pompa_no       = tabanca_obj.POMPA_NO
                status_ojb.pompa_adi      = tabanca_obj.POMPA_ADI
                status_ojb.beyin_no       = tabanca_obj.BEYIN_NO
                status_ojb.tabanca_no     = tabanca_obj.TABANCA_NO
                status_ojb.yakit_tank_id  = tabanca_obj.ID_YAKIT_TANKI
                status_ojb.tabanca_id     = tabanca_obj.ID_TABANCA
                status_ojb.pump_state     = state
                status_ojb.raw_data       = donen
                status_ojb.endeks_donen   = endeks_donen
                #print pompa_no
                ##### CARI HESAP EKLEME ELLE YAPILDI ######
                if status_ojb.status != 'error':
                    if status_ojb.status == 'busy':
                        status_ojb.cari_hesap_id     = 2
                    else:
                        status_ojb.cari_hesap_id     = 1
                    status_ojb.musteri_cari_id = 'Null'
                else: 
                    ariza_obj.arizaArabirimformatla( status_ojb,iletisim_var_mi = True )
            
           
            '''
            #print tabanca_id, pompa_status[ 1 ].status 
            self.pompa_group[ pompa_obj.pompa_no ] = pompa_status
            #print self.pompa_group
            for k, v in self.pompa_group.iteritems():
            
                for status_ in v.itervalues():
                    final_status  = status_ 
                    if status_.status != 'idle':
                        final_status = status_
                        break
            '''
            if pompa_obj.arabirim_marka == ARABIRIM_ID_MARKA.TEOSIS:
                final_status         = self.getFullfilledStatusObjTeosis( status_ojb, simulation  )
            elif pompa_obj.arabirim_marka == ARABIRIM_ID_MARKA.ORPAK:
                final_status         = self.getFullfilledStatusObjOrpak( status_ojb, simulation )
            
            if not simulation:
                
                if final_status.status != 'error':
                    
                    if musteri_cari and musteri_cari.has_key( pompa_no ) and musteri_cari[pompa_no] :
                        final_status.musteri_cari_id = musteri_cari[ pompa_no ].ID_CARI_HESAP
                        if not final_status.__dict__.has_key( 'plate_number' ) or final_status.plate_number == '':#yazarkasa plaka set etmemis
                            final_status.plate_number = musteri_cari[ pompa_no ].PLAKA
                    else:
                        final_status.musteri_cari_id = 'Null'
                    
                    if personel_cari and personel_cari.has_key( pompa_no ) and personel_cari[pompa_no]:
                        final_status.cari_hesap_id  = personel_cari[ pompa_no ].ID_CARI_HESAP
                        if not final_status.__dict__.has_key( 'plate_number' ) or final_status.plate_number == '':#yazarkasa ve musteri plaka set etmemis
                            #print dir( personel_cari[ pompa_no ])
                            if personel_cari[ pompa_no ].PLAKA != MESSAGES.personel:
                                final_status.plate_number = personel_cari[ pompa_no ].PLAKA
                                
                    else:
                        final_status.cari_hesap_id  = 1#default cari
                    
                else:
                    ariza_obj.arizaArabirimformatla( final_status,iletisim_var_mi = True )
            
            
            #print pompa_no
            final_status.payment_method   = ODEME_SEKILLERI.DEFAULT
            final_pump_status[ pompa_no ] = final_status
            
        #print final_pump_status
        return final_pump_status, endeks_dict
    
    def endeksFormatla(self, tabanca_id, endeks_donen_list , tabanca_obj, simulation=False ):
        endeks_list_by_tabanca = []
        for endeks_donen in endeks_donen_list:
            final_endex_object      = dummy()
            final_endex_object.total_volume   = 0
            final_endex_object.total_amount   = 0
            final_endex_object.sale_quantity  = 0
            final_endex_object.pompa_adi      = tabanca_obj.POMPA_ADI
            final_endex_object.pompa_no       = tabanca_obj.POMPA_NO
            final_endex_object.beyin_no       = tabanca_obj.BEYIN_NO
            final_endex_object.yakit_adi      = tabanca_obj.YAKIT_ADI
            final_endex_object.yakit_id       = tabanca_obj.ID_YAKIT
            if endeks_donen.count( '87' ) > 0:
                raw_data = endeks_donen[endeks_donen.index('87'):]
                try:
                    final_endex_object.total_volume      = float(''.join( raw_data[3:6]) + '.' + raw_data[6])
                    final_endex_object.total_amount      = float(''.join( raw_data[7:12]) + '.' + raw_data[12])
                    final_endex_object.sale_quantity     = float(''.join( raw_data[13:17]))
                except:
                    #print 'endeks :', raw_data
                    print ( str(sys.exc_info()[0]))
                endeks_list_by_tabanca.append( final_endex_object )
                self.final_endeks_dict[ tabanca_id ] = endeks_list_by_tabanca
            
        return self.final_endeks_dict    
            
    def getFullfilledStatusObjTeosis( self, status_obj, simulation = False ):
        #print status_obj.raw_data
        #print '************************************************'
        plate_number = ''
        status_obj.plate_number   = plate_number 
        status_obj.satis_db_basilacak = 0
        
        if status_obj.pump_state == '04':
            
            status_obj.satis_db_basilacak  = 1
            
        if status_obj.status == 'plate':
           if status_obj.raw_data.count('20') > 0:
                status_obj.plate_number = ''.join( status_obj.raw_data[5:status_obj.raw_data.index('20')]).decode('hex')

        elif status_obj.status == 'busy':
            if not simulation :
                if status_obj.raw_data.count( '83' ) > 0:
                    try:
                        raw_data = status_obj.raw_data[status_obj.raw_data.index('83'):]
                    
                        filling_amount = float(''.join( raw_data[4:7]) + '.' + raw_data[7])
                        filling_volume = float(''.join( raw_data[8:11]) + '.' + raw_data[11])
                        unit_price     = float(''.join( raw_data[12:14]) + '.' + raw_data[14])
                        if len( raw_data ) > 25 :
                        
                            plate_number   = ''.join( raw_data[15:47]).decode('hex').strip()
                            self.prev_plate_number = plate_number
                    
                        unit_price     = unit_price / 10 #basamak kaydirma bu bok olmal�
                        status_obj.filling_amount = filling_amount
                        status_obj.filling_volume = filling_volume
                        status_obj.unit_price     = unit_price
                        status_obj.plate_number   = plate_number
                    except:
                        print 'busy :', raw_data
            else:
                status_obj = self.getFullfilledStatusObjBySim( status_obj )
        return status_obj
    
    def getFullfilledStatusObjOrpak( self, status_obj, simulation = False ):
        #print status_obj.raw_data
        #print status_obj.pump_state
        #print '************************************************'
        #plate_number = ''
        #status_obj.plate_number   = plate_number 
        status_obj.satis_db_basilacak = 0
        set_continue                  = True
        if not simulation :
            for e in status_obj.endeks_donen:
                if len(e) > 5:
                    if e[5] == '24':
                        set_continue = False 
                        filling_volume_list = e[17: 25]
                        filling_amount_list = e[25: 33]
                        unit_price_list     = e[33: 38]
                    
                        filling_volume      = get_by_mod(filling_volume_list,'2e',30)
                        filling_amount      = get_by_mod(filling_amount_list,'2e',30)
                        unit_price          = get_by_mod(unit_price_list,'2e',30)
                       
                        if len(e) > 51:
                            plate_number   = ''.join( e[38:53]).decode('hex').strip()
                            
                            self.prev_plate_number = plate_number
                    
                        status_obj.filling_amount = filling_amount
                        status_obj.filling_volume = filling_volume
                        status_obj.unit_price     = unit_price
                        status_obj.plate_number   = plate_number
                        status_obj.satis_db_basilacak  = 1
                    
                    if e[5] == '26' and set_continue:
                        filling_volume_list = e[13: 21]
                        filling_amount_list = e[21: 29]    
                        
                        filling_volume      = get_by_mod(filling_volume_list,'2e',30)
                        filling_amount      = get_by_mod(filling_amount_list,'2e',30)
                        if int(filling_volume) != 0:
                            unit_price          = round(filling_amount / filling_volume , 2)
                        else:
                            unit_price          = float(0.0)
                        if len(e) > 45:
                            plate_number   = ''.join( e[32:47]).decode('hex').strip()
                            self.prev_plate_number = plate_number
                        
                        status_obj.filling_amount = filling_amount
                        status_obj.filling_volume = filling_volume
                        status_obj.unit_price     = unit_price
                        if len( plate_number ) > 0 :
                            status_obj.plate_number        = plate_number
                
        else:
            status_obj = self.getFullfilledStatusObjBySim( status_obj )
        
        

                
        return status_obj
    def getFullfilledStatusObjBySim( self, status_obj ):
    
        filling_volume = float( self.simulation_litre ) + 0.3
        if filling_volume > 200:
            filling_volume = 0
        self.simulation_litre  = filling_volume
        
        unit_price     = 5.03 #db.get_raw_data_all( table_name='YAKIT' , selects='YAKIT_BIRIM_FIYAT', where='ID_YAKIT=%s'%status_obj.yakit_id,dict_mi='object')[0].YAKIT_BIRIM_FIYAT
        filling_amount = round(filling_volume * float( unit_price ),2)
        
        plate_number   = '06FKR78'
        
        status_obj.filling_amount      = filling_amount
        status_obj.filling_volume      = filling_volume
        status_obj.unit_price          = unit_price
        status_obj.plate_number        = plate_number
        
        return status_obj
    
    def manuel_satis_formatla ( self, gelen_list, pompa_tabanca_obj_list ):
        donen_obj  = Dummy()
        donen      = False
        
        if gelen_list[2] == '8b':
            try:
                raw_data           = gelen_list[gelen_list.index('8b')+2:]
                beyin_tabanca        = raw_data.pop( 0 ) 
                donen_obj.beyin_no   = int(beyin_tabanca[0])
                donen_obj.tabanca_no = int(beyin_tabanca[1])
                
                pompa_tabanca_obj    = filter( lambda x:x if x.BEYIN_NO == donen_obj.beyin_no and x.TABANCA_NO == donen_obj.tabanca_no else None , pompa_tabanca_obj_list) 
                
                record_number = int(raw_data.pop(0) + raw_data.pop(0))
                donen_obj.record_number = record_number
                donen_obj.filling_type  = int(raw_data.pop( 0 ))
                donen_obj.product_num   = int(raw_data.pop( 0 ))
                donen_obj.unit_price    = float(''.join( raw_data[0:3]) + '.' + raw_data[3]) / 10
                donen_obj.litre         = float(''.join( raw_data[4:7]) + '.' + raw_data[7])
                donen_obj.tutar         = float(''.join( raw_data[8:11]) + '.' + raw_data[11])
                
                donen_obj.tarih         = '20'+raw_data[12] +'-'+raw_data[13]+'-'+raw_data[14]
                fmt                     = '%Y%m%d%H%M%S' 
                donen_obj.full_tarih    = datetime.strptime('20' + ''.join(raw_data[12:15]) + ''.join(raw_data[15:18]),fmt)
                plaka                   = ''.join( raw_data[18:50]).decode('hex').strip()
                plaka                   = ''.join([x for x in plaka if ord(x) > 0])
                if len( plaka ) == 0:
                    plaka = 'YOK'
                donen_obj.plaka         = plaka
                
                donen  = self.manual_satis_check ( donen_obj, pompa_tabanca_obj )
            except:
                #print 'except format'
                pass
        return donen        
    def manual_satis_check( self, manual_satis_object, pompa_tabanca_obj ):
        db = DBObjects()
        check_sqls = db.get_raw_data_all( table_name = 'POMPA_SATIS_KAYIT', selects=" count (*) ",
                                          where="""PLAKA='%s' AND strftime('%%Y-%%m-%%d', TARIH)='%s' AND
                                              TUTAR=%s AND SATIS_LITRE=%s""" %(manual_satis_object.plaka,
                                                                               manual_satis_object.tarih,
                                                                               manual_satis_object.tutar,
                                                                               manual_satis_object.litre))[0][0]
        if check_sqls == 0:
            if len(pompa_tabanca_obj) > 0:
                pompa_tabanca_obj = pompa_tabanca_obj[0]
                now              =   datetime.now()
                str_now          =   time.strftime('%Y-%m-%d %H:%M:%S', now.timetuple())
                column_names    = "POMPA_NO,POMPA_ADI,ID_YAKIT,BEYIN_NO,TABANCA_NO,SATIS_LITRE,BIRIM_FIYAT,TUTAR,TARIH,PLAKA,ODEME_SEKLI,DURUM,ID_TABANCA"
                values          = "%s,'%s',%s,%s,%s,%s,%s,%s,'%s','%s',1,0,%s"%(  pompa_tabanca_obj.POMPA_NO,
                                                                                    pompa_tabanca_obj.POMPA_ADI,
                                                                                    pompa_tabanca_obj.ID_YAKIT,
                                                                                    pompa_tabanca_obj.BEYIN_NO,
                                                                                    pompa_tabanca_obj.TABANCA_NO,
                                                                                    manual_satis_object.litre,
                                                                                    manual_satis_object.unit_price,
                                                                                    manual_satis_object.tutar,
                                                                                    manual_satis_object.full_tarih,
                                                                                    manual_satis_object.plaka,
                                                                                    pompa_tabanca_obj.ID_TABANCA
                                                                                    )
                donen = db.save_data( "POMPA_SATIS_KAYIT",column_names,values  )
            return False
        else:
            return True

def get_manual_satislar_by_tank( db, tank_params_obj ):
    tabancalar = db.getTabancaByTankId( tank_params_obj.ID_YAKIT_TANKI )
    counter = 0
    Record_obj = iletisim.Records()
    for tabanca in tabancalar:
        satis_record = Record_obj.get_satis_record_by_tabanca( tabanca )
        
def acilis_dolum_kontrol( db, tank_params_obj, v ):
    db_last_value = db.getLastTankValueByProbeId( tank_params_obj.ID_YAKIT_TANKI )
    #print db_last_value
    if len( db_last_value ) > 0:
        db_last_value = db_last_value[0]
        yakit_litre   = v.yakit_litre
        islem         = TANK_H_ACIKLAMA.OKUNAN_DEGER
        #print db_last_value.OKUNAN_YAKIT, ' db den gelen'
        #print yakit_litre,' probedan gelen'
        fark          =  yakit_litre - db_last_value.OKUNAN_YAKIT
        #print fark
        if fark > acilis_dolum_farki:
            islem =  TANK_H_ACIKLAMA.OTO_AC_FARK
        return islem, fark
    else:
        return None,None